package com.mahindra.mitra20.constants;

public interface AuctionType {
    int LIVE = 0;
    int PENDING = 1;
    int COMPLETED = 2;
}