package com.mahindra.mitra20.constants;

import com.mahindra.mitra20.BuildConfig;

public class GeneralConstants {

/*    //    UAT
//   public static final String BASE_URL = "https://mdwaccess.com/DMSContextREST/rest/";
//    Production
    public static final String BASE_URL = "https://mahindradealerworld.com/DMSContextREST/rest/";*/

    //UAT
    //public static final String BASE_URL = BuildConfig.Base_URL;

    //Production
    public static final String BASE_URL = BuildConfig.Base_URL;

    public static final String BROADCAST_RECEIVER_KEY = "Receiver";

    public static String DATA_FETCHED = "false";

    public static int MINIMUM_PROCUREMENT_PRICE = 999;

}
