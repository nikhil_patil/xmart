package com.mahindra.mitra20.constants;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by PATINIK-CONT on 23-May-19.
 */
public interface SpinnerConstants {
    Map<String, String> mapRcRemarks = new LinkedHashMap<String, String>() {
    {
        put(null, "NA");
        put("RCMIS", "RC Mismatch");
        put("FADED", "Faded");
        put("CLER", "Clear");
    }};
    Map<String, String> mapCNGKit = new LinkedHashMap<String, String>() {
    {
        put(null, "NA");
        put("AFTMKT", "After Market");
        put("CMPFTD", "Company Fitted");
    }};
    Map<String, String> mapAccidentalType = new LinkedHashMap<String, String>() {
    {
        put(null, "NA");
        put("MI", "Minor");
        put("SD", "Structural Damage");
        put("MJ", "Major");
    }};

    Map<String, String> mapBodyRemarks = new LinkedHashMap<String, String>() {
        {
            put("RP", "Repainted");
            put("SD", "Scratches / Dents");
            put("FE", "Flood Effected");
            put("RT", "Rusted");
            put("MDSB", "Missing / Damaged Seat Belt");
            put("CDD", "Crack / Damaged Dashboard");
            put("SSFF", "Spoiled Seat fabric / foam");
            put("DSR", "Damaged Seat Recliner");
            put("OEM", "OEM Mascot / Logo missing");
            put("OTH", "Others");
        }
    };

    Map<String, String> mapEngineRemarks = new LinkedHashMap<String, String>() {
        {
            put("EOL", "Engine Oil Leakage");
            put("CFRL", "Crank front / rear leakage");
            put("OSLD", "Oil sump leakage / dent");
            put("OLNWC", "Oil Leakage near Wall Cover");
            put("AEN", "Abnormal engine noise (Bearing, Turbo, Metallic)");
            put("EMBS", "Engine misfiring / black smoke");
            put("CL", "Coolant Leakage");
            put("WPNL", "Water Pump noise/leakage");
            put("EO", "Engine Overheat");
            put("RBL", "Radiator Broken / Leakage");
            put("BN", "Belt Noise");
            put("ECWB", "Engine Compression Weak / Blow by");
            put("EOCR", "Engine Oil/Coolant replacement");
            put("OTH1", "Others");
        }
    };

    Map<String, String> mapFuelAndIgnitionRemarks = new LinkedHashMap<String, String>() {
        {
            put("STRD", "Starting Delay");
            put("FPLN", "Fuel Pump Leakage/Noisy");
            put("NLG", "Nozzle Leakage");
            put("BSFE", "Black Smoke from exhaust");
            put("FLL", "Fuel line leakage");
            put("SMNS", "Starter Motor Noisy/slippage");
            put("ISS", "Ignition switch spoiled");
            put("OTH3", "Others");
        }
    };

    Map<String, String> mapTransmissionRemarks = new LinkedHashMap<String, String>() {
        {
            put("GBLG", "Gear Box Leakage");
            put("GBCB", "Gear Box case broken ( bell house, gear box cover)");
            put("GSNS", "Gear Slippage / Noisy");
            put("GLPH", "Gear Lever play/hard");
            put("DSNS", "Drive shaft noise");
            put("WBNS", "Wheel bearing noise");
            put("HCH", "Hard clutch");
            put("CFLG", "Clutch fluid leakage");
            put("HGSG", "Hard Gear shifting");
            put("HNRA", "Humming noise in rear axle");
            put("DLG1", "Differential Leakage");
            put("NW4", "4X4 not working");
            put("GDORT", "Gearbox/Differential Oil Replacement");
            put("OTH4", "Others");
        }
    };

    Map<String, String> mapBrakeSystemRemarks = new LinkedHashMap<String, String>() {
        {
            put("BRFDLG", "Brake fluid leakage");
            put("HBPDL", "Hard Brake Peddle");
            put("MNFWS", "Metallic Noise from wheels");
            put("BDWOT", "Brake disk worn out");
            put("BLCLKG", "Brake line/cylinder leakage");
            put("BKJG", "Brake juddering");
            put("HBNWCB", "Hand brake not working / cable broken");
            put("BKFDRT", "Brake fluid replacement");
            put("OTH5", "Others");
        }
    };

    Map<String, String> mapSuspensionRemarks = new LinkedHashMap<String, String>() {
        {
            put("SBRPMT", "Suspension Bush replacement");
            put("SBCLSU", "Suspension Broken(Coil Spring,Leaf spring,shackle,U Clamp)");
            put("CSPBR", "Coil spring broken");
            put("OTH6", "Others");
        }
    };

    Map<String, String> mapSteeringRemarks = new LinkedHashMap<String, String>() {
        {
            put("PSNW", "Power Stearing not working");
            put("HPSR", "Hard Power Stearing");
            put("STRNS", "Stearing Noise");
            put("DSASM", "Damaged Steering Stalk assembly");
            put("SP1", "Stearing Play (Rack  pinnion,Stearing wheel,Stearing Joints)");
            put("SCCPS", "Stearing column collapsed");
            put("OTH7", "Others");
        }
    };

    Map<String, String> mapACRemarks = new LinkedHashMap<String, String>() {
        {
            put("GLG1", "Gas Leakage");
            put("LAC1", "Low AC cooling");
            put("ACEL1", "AC condensor/ evaporator leakage");
            put("ACPL1", "AC pipe leakage");
            put("ACBNW1", "AC Blower not working");
            put("ACSSNW1", "AC switch spoilt / not working");
            put("HNW1", "Heater not working");
            put("ACDSNW1", "AC direction/recirculation system not working/spoilt");
            put("ACNL1", "AC compresor noisy / leakage");
            put("ACVBM1", "AC vents broken/missing");
            put("RACNW1", "Rear AC not working");
            put("OTH8", "Others");
        }
    };

    Map<String, String> mapElectricalRemarks = new LinkedHashMap<String, String>() {
        {
            put("BTDC", "Battery discharge");
            put("BTDMG", "Battery damage");
            put("ATRNS", "Alternator noisy");
            put("ATRNW", "Alternator not working");
            put("OLNATR", "Oil leakage near alternator");
            put("ATRBR", "Alternator broken");
            put("ATRFBR", "Alternator fins broken");
            put("CEWLGP", "Check engine warning lamp glowing permanently");
            put("FCLGT", "Faulty Cluster light");
            put("WRHDMG", "Wiring harness damaged");
            put("OTH9", "Others");
        }
    };

    Map<String, String> mapRoadTax = new LinkedHashMap<String, String>() {
        {
            put("V", "Valid");
            put("E", "Expired");
        }
    };

    Map<String, String> mapCondition = new LinkedHashMap<String, String>() {
        {
            put("1", "Good");
            put("2", "Scratch");
            put("3", "Dent");
            put("4", "Damage");
            put("5", "Rusted");
            put("6", "Repainted");
            put("7", "Replaced");
        }
    };

    Map<String, String> mapFrontBumper = new LinkedHashMap<String, String>() {
        {
            put("DMGD", "Damaged");
            put("DNT", "Dent");
            put("GUD", "Good");
            put("REPNT", "Repainted");
            put("RPLCD", "Replaced");
            put("SCRH", "Scratch");
        }
    };

    Map<String, String> mapOutsideRearViewMirrors = new LinkedHashMap<String, String>() {
        {
            put("CBLBRK", "cable broken");
            put("FAD", "Faded");
            put("GLSBRK", "Glass Broken");
            put("GUD", "Good");
            put("MTRNT", "Electric motor not working");
        }
    };

    Map<String, String> mapPillarType = new LinkedHashMap<String, String>() {
        {
            put("A", "A");
            put("B", "B");
            put("C", "C");
            put("D", "D");
        }
    };

    Map<String, String> mapDickyDoor = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("SCRH", "Scratch");
            put("DNT", "Dent");
            put("DMGD", "Damaged");
            put("REPNT", "Repainted");
            put("RPLCD", "Replaced");
            put("RESTD", "Rusted");
        }
    };

    Map<String, String> mapBodyShell = new LinkedHashMap<String, String>() {
        {
            put("ORGNL", "Original");
            put("REPLCD", "Replaced");
        }
    };

    Map<String, String> mapChassis = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("BND", "Bend");
            put("CRK", "Crack");
            put("RUST", "Rusted");
        }
    };

    Map<String, String> mapApronFront = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("DMGD", "Damaged");
            put("REPRD", "Repaired");
            put("RUST", "Rusted");
            put("REPNTD", "Repainted");
        }
    };

    Map<String, String> mapStrutMountingArea = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("DMGD", "Damaged");
            put("RUST", "Rusted");
        }
    };

    Map<String, String> mapFirewall = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("INLNDMGD", "Insulation Damage");
            put("RUST", "Rusted");
        }
    };

    Map<String, String> mapCowlTop = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("FADED", "Faded");
            put("BROKN", "Broken");
        }
    };

    Map<String, String> mapUpperLowerCrossMember = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("DENT", "Dent");
            put("RESTD", "Rusted");
        }
    };

    Map<String, String> mapRadiatorSupport = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("DENT", "Dent");
            put("RESTD", "Rusted");
            put("BROKN", "Broken");
        }
    };

    Map<String, String> mapEngineRoomCarrierAssembly = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("BROKN", "Broken");
        }
    };

    Map<String, String> mapHeadTailLight = new LinkedHashMap<String, String>() {
        {
            put("FADED", "Faded");
            put("UNTBRKN", "Unit Broken");
            put("GUD", "Good");
            put("BRKTBRKN", "Bracket Broken");
        }
    };

    Map<String, String> mapRunningBoard = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("SCRH", "Scratch");
            put("DNT", "Dent");
            put("DMGD", "Damaged");
            put("REPNT", "Repainted");
            put("RSTED", "Rusted");
        }
    };

    Map<String, String> mapPowerWindowType = new LinkedHashMap<String, String>() {
        {
            put("FRNTRH", "Front RH");
            put("FRNTLH", "Front LH");
            put("ALL", "All");
        }
    };

    Map<String, String> mapAllWindowCondition = new LinkedHashMap<String, String>() {
        {
            put("GOOD", "Good");
            put("BAD", "Bad");
        }
    };

    Map<String, String> mapFrontRearRLWindow = new LinkedHashMap<String, String>() {
        {
            put("GLSBRKN", "Glass Broken");
            put("HNDLRBRKN", "Handle Broken");
            put("NTWORKNG", "Not Working");
            put("SWNTWRKNG", "Switch Not Working");
            put("WORKNG", "Working");
        }
    };

    Map<String, String> mapPlatformBootSpaceAndPassengerCabin = new LinkedHashMap<String, String>() {
        {
            put("GUD", "Good");
            put("REPRD", "Repaired");
            put("RUST", "Rusted");
        }
    };

    Map<String, String> mapFueltype = new LinkedHashMap<String, String>() {
        {
            put("PET", "Petrol");
            put("DIE", "Diesel");
            put("LPG", "Liquefied Petroleum Gas");
            put("CNG", "Compressed Natural Gas");
            put("ELC", "Electric/Battery");
        }
    };

    Map<String, String> mapVehicleUsage = new LinkedHashMap<String, String>() {
        {
            put("PER", "Personal");
            put("COM", "Commercial");
            put("TAXI", "Taxi");
        }
    };
}