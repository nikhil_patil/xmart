package com.mahindra.mitra20.presenters;

import com.mahindra.mitra20.models.SessionUserDetails;

/**
 * Created by BADHAP-CONT on 7/18/2018.
 */

public interface LoginActivityIn {

    void onLoginSuccess(SessionUserDetails sessionUserDetails);

    void onLoginFailure(String response);

    void settingLoginElementsClicks();

}
