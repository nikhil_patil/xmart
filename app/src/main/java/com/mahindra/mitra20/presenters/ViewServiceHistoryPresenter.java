package com.mahindra.mitra20.presenters;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.interfaces.ViewServiceHistoryIn;
import com.mahindra.mitra20.models.RoDetail;
import com.mahindra.mitra20.models.ServiceHistory;
import com.mahindra.mitra20.models.ServiceHistoryStats;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 6/26/2019.
 */

public class ViewServiceHistoryPresenter implements VolleySingleTon.VolleyInteractor {

    private ViewServiceHistoryIn viewServiceHistoryIn;
    private Context context;

    public ViewServiceHistoryPresenter(Context context) {
        this.context = context;
        this.viewServiceHistoryIn = (ViewServiceHistoryIn) context;
    }

    public void getServiceHistory(String chassisNo, String regNo) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put(WebConstants.ServiceHistory.CHASIS_NUMBER, (chassisNo == null) ? "" : chassisNo);
            params.put(WebConstants.ServiceHistory.REG_NO, (regNo == null) ? "" : regNo);
            JSONObject jsonObject = new JSONObject(params);

            VolleySingleTon.getInstance().connectToPostUrl3(context, this,
//                    "https://mahindradealerworld.com/DMSContextREST/rest/WYH/ROHistory", jsonObject,
                    WebConstants.ServiceHistory.URL, jsonObject,
                    WebConstants.ServiceHistory.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (requestId == WebConstants.ServiceHistory.REQUEST_CODE) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("IsSuccessful").equals("1")) {
                        ServiceHistory serviceHistory = new Gson().fromJson(response, ServiceHistory.class);
                        if (null != serviceHistory) {
                            viewServiceHistoryIn.onServiceHistory(serviceHistory);
                        } else {
                            viewServiceHistoryIn.onDownloadFail(jsonObject.getString("message"));
                        }
                    } else {
                        viewServiceHistoryIn.onDownloadFail(jsonObject.getString("message"));
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    viewServiceHistoryIn.onDownloadFail("Something went wrong. Please try again.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            viewServiceHistoryIn.onDownloadFail(jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ServiceHistoryStats calculateAvgRunDay(List<RoDetail> roDetailList, String soldDateStr) {
        List<Date> roDatesArray = new ArrayList<>();
        List<Float> roKmsArray = new ArrayList<>();
        Date soldDate = null;
        try {
            soldDate = new SimpleDateFormat("dd/MM/yyyy",
                    Locale.ENGLISH).parse(soldDateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ServiceHistoryStats serviceHistoryStats = null;

        try {
            serviceHistoryStats = new ServiceHistoryStats();
            for (int i = 0; i < roDetailList.size(); i++) {
                switch (roDetailList.get(i).getServiceType()) {
                    case "PRSL":
                        serviceHistoryStats.setServiceTypePRSLCount(
                                serviceHistoryStats.getServiceTypePRSLCount() + 1);
                        serviceHistoryStats.setServiceTypePRSLTotalAmount(
                                serviceHistoryStats.getServiceTypePRSLTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    case "RR":
                        serviceHistoryStats.setServiceTypeRRCount(
                                serviceHistoryStats.getServiceTypeRRCount() + 1);
                        serviceHistoryStats.setServiceTypeRRTotalAmount(
                                serviceHistoryStats.getServiceTypeRRTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    case "SVC":
                        serviceHistoryStats.setServiceTypeSVCCount(
                                serviceHistoryStats.getServiceTypeSVCCount() + 1);
                        serviceHistoryStats.setServiceTypeSVCTotalAmount(
                                serviceHistoryStats.getServiceTypeSVCTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    case "SAC":
                        serviceHistoryStats.setServiceTypeSACCount(
                                serviceHistoryStats.getServiceTypeSACCount() + 1);
                        serviceHistoryStats.setServiceTypeSACTotalAmount(
                                serviceHistoryStats.getServiceTypeSACTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    case "ACC":
                        serviceHistoryStats.setServiceTypeACCCount(
                                serviceHistoryStats.getServiceTypeACCCount() + 1);
                        serviceHistoryStats.setServiceTypeACCTotalAmount(
                                serviceHistoryStats.getServiceTypeACCTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    case "PDS":
                        serviceHistoryStats.setServiceTypeScheduleCount(
                                serviceHistoryStats.getServiceTypeScheduleCount() + 1);
                        serviceHistoryStats.setServiceTypeScheduleTotalAmount(
                                serviceHistoryStats.getServiceTypeScheduleTotalAmount()
                                        + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        break;
                    default:
                        //  all the other codes which are not numbers will come under others
                        if (!TextUtils.isDigitsOnly(roDetailList.get(i).getServiceType().trim())) {
                            serviceHistoryStats.setServiceTypeOthersCount(
                                    serviceHistoryStats.getServiceTypeOthersCount() + 1);
                            serviceHistoryStats.setServiceTypeOthersTotalAmount(
                                    serviceHistoryStats.getServiceTypeOthersTotalAmount()
                                            + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        } else {
                            serviceHistoryStats.setServiceTypeScheduleCount(
                                    serviceHistoryStats.getServiceTypeScheduleCount() + 1);
                            serviceHistoryStats.setServiceTypeScheduleTotalAmount(
                                    serviceHistoryStats.getServiceTypeScheduleTotalAmount()
                                            + Float.parseFloat(roDetailList.get(i).getTotalAmount()));
                        }
                        break;
                }
                try {
                    roDatesArray.add(new SimpleDateFormat("dd/MM/yyyy",
                            Locale.ENGLISH).parse(roDetailList.get(i).getRoDate()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                roKmsArray.add(Float.parseFloat(roDetailList.get(i).getKm()));
            }

            List<Date> sampleArr = new ArrayList<>(roDatesArray);
            Collections.sort(sampleArr);
            int indexOfMaxDate = roDatesArray.indexOf(sampleArr.get(sampleArr.size() - 1));

            serviceHistoryStats.setLastReportedKms(roKmsArray.get(indexOfMaxDate));
            serviceHistoryStats.setAverageRun(roKmsArray.get(indexOfMaxDate) /
                    (getNumberOfDaysInBetween(soldDate, roDatesArray.get(indexOfMaxDate))));

            serviceHistoryStats.setServiceTypeRsPerKm((serviceHistoryStats.getServiceTypeRRTotalAmount()
                    + serviceHistoryStats.getServiceTypeScheduleTotalAmount()) / roKmsArray.get(indexOfMaxDate));
            serviceHistoryStats.setServiceTypeMTV(Math.round(Math.abs((
                    roKmsArray.get(indexOfMaxDate) / (serviceHistoryStats.getServiceTypeRRCount()
                            + serviceHistoryStats.getServiceTypeScheduleCount())))));

            serviceHistoryStats.setServiceTypeAllCount(
                    serviceHistoryStats.getServiceTypeRRCount()
                            + serviceHistoryStats.getServiceTypeScheduleCount()
                            + serviceHistoryStats.getServiceTypePRSLCount()
                            + serviceHistoryStats.getServiceTypeSVCCount()
                            + serviceHistoryStats.getServiceTypeSACCount()
                            + serviceHistoryStats.getServiceTypeACCCount()
                            + serviceHistoryStats.getServiceTypeOthersCount());

            serviceHistoryStats.setServiceTypeAllTotalAmount(
                    serviceHistoryStats.getServiceTypeRRTotalAmount()
                            + serviceHistoryStats.getServiceTypeScheduleTotalAmount()
                            + serviceHistoryStats.getServiceTypePRSLTotalAmount()
                            + serviceHistoryStats.getServiceTypeSVCTotalAmount()
                            + serviceHistoryStats.getServiceTypeSACTotalAmount()
                            + serviceHistoryStats.getServiceTypeACCTotalAmount()
                            + serviceHistoryStats.getServiceTypeOthersTotalAmount());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serviceHistoryStats;
    }

    private int getNumberOfDaysInBetween(Date date1, Date date2) {
        int oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        return Math.round(Math.abs((date1.getTime() - date2.getTime()) / (oneDay)));
    }
}