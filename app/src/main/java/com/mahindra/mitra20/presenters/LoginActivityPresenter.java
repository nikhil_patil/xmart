package com.mahindra.mitra20.presenters;

import android.content.Context;
import android.util.Log;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.helper.PasswordEncryption;
import com.mahindra.mitra20.module_broker.models.Dealer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by BADHAP-CONT on 7/18/2018.
 */

public class LoginActivityPresenter implements VolleySingleTon.VolleyInteractor {

    private LoginActivityIn mLoginActivityIn;
    private Context mContext;
    private int currentUser = 0;
    private String mitraMobileNo;

    public LoginActivityPresenter(LoginActivityIn loginActivityIn, Context context) {
        mLoginActivityIn = loginActivityIn;
        mContext = context;
    }

    public void setLoginTabsClicks() {
        mLoginActivityIn.settingLoginElementsClicks();
    }

    private void userLoggedIn(int currentUser, JSONObject jsonObject) {
        String parentName = jsonObject.optString("ParentName");
        String profileUrl = jsonObject.optString("ProfileUrl");
        try {
            if (currentUser == 0) {
                String userType = jsonObject.getString("User Type");
                String dealerName = jsonObject.getString("DealerName");
                String username = jsonObject.getString("UserName");
                String userId = jsonObject.getString("UserID");
                String employeeCode = jsonObject.getString("dmsEmpcode");
                String phoneNumber = jsonObject.getString("dmsEmpcode");

                int ut = 0;
                if (userType.equalsIgnoreCase("CHAMP")) {
                    ut = 0;
                } else if (userType.equalsIgnoreCase("EVAL")) {
                    ut = 1;
                }

                SessionUserDetails.getInstance().setUserType(ut);
                SessionUserDetails.getInstance().setCurrentUser(ut);
                SessionUserDetails.getInstance().setUserName(username);
                SessionUserDetails.getInstance().setDealerName(dealerName);
                if (ut == 1) {
                    SessionUserDetails.getInstance().setUserID(userId);
                } else if (ut == 0) {
                    SessionUserDetails.getInstance().setUserID(employeeCode);
                }

                SessionUserDetails.getInstance().setActualUserID(userId);
                SessionUserDetails.getInstance().setEmployeeCode(employeeCode);
                SessionUserDetails.getInstance().setParentName(parentName);
                SessionUserDetails.getInstance().setUserProfilePic(profileUrl);
                mLoginActivityIn.onLoginSuccess(SessionUserDetails.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (currentUser == 1) {
                String mitraCode = jsonObject.getString("MitraCode");
                String MitraName = jsonObject.getString("MitraName");
                String MitraType = jsonObject.getString("MitraType");
                String ParentCode = jsonObject.getString("ParentCode");
                String ParentName = jsonObject.getString("ParentName");
                JSONArray locationListArray = jsonObject.getJSONArray("ParentLocationList");

                SessionUserDetails.getInstance().setCurrentUser(2);
                SessionUserDetails.getInstance().setMitraCode(mitraCode);
                SessionUserDetails.getInstance().setMitraName(MitraName);
                SessionUserDetails.getInstance().setMitraType(MitraType);
                SessionUserDetails.getInstance().setParentCode(ParentCode);
                SessionUserDetails.getInstance().setParentName(ParentName);
                SessionUserDetails.getInstance().setUserType(2);
                SessionUserDetails.getInstance().setMitraMobileNum(mitraMobileNo);

                ArrayList<Dealer> locationList = new ArrayList<>();
                for (int i = 0; i < locationListArray.length(); i++) {
                    JSONObject jsonObject1 = (JSONObject) locationListArray.get(i);
                    Dealer dealer = new Dealer();
                    dealer.setLocationCode(jsonObject1.optString("LocationCode"));
                    dealer.setLocationName(jsonObject1.optString("LocationName"));
                    dealer.setParentCode(jsonObject1.optString("ParentCode"));
                    dealer.setParentName(jsonObject1.optString("ParentName"));
                    locationList.add(dealer);
                }
                SessionUserDetails.getInstance().setParentName(parentName);
                SessionUserDetails.getInstance().setUserProfilePic(profileUrl);
                SessionUserDetails.getInstance().setLocationList(locationList);
                mLoginActivityIn.onLoginSuccess(SessionUserDetails.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loginForChamp(int currentUser, String username, String password, String authCode, String FCMTokenId) {
        this.currentUser = currentUser;
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.EvalChampLoginInputKeys.UserID, username);
            String encrypted = PasswordEncryption.getEncryptedPassword(password);
            jsonObj.put(RestInputKeys.EvalChampLoginInputKeys.password, encrypted);
            jsonObj.put(RestInputKeys.EvalChampLoginInputKeys.authCode, authCode);
            jsonObj.put(RestInputKeys.EvalChampLoginInputKeys.FCMTokenId, FCMTokenId);

            String url = UrlConstants.BASE_URL + UrlConstants.EVAL_CHAM_LOGIN;
            VolleySingleTon.getInstance().connectToPostUrl3(mContext, this, url, jsonObj, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loginForMitra(int currentUser, String MitraCode, String MitraMobile, String FCMTokenId) {
        this.currentUser = currentUser;
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("MitraCode", MitraCode.toUpperCase());
            jsonObj.put("MitraMobile", MitraMobile);
            jsonObj.put("FCMTokenId", FCMTokenId);

            mitraMobileNo = MitraMobile;

            String url = UrlConstants.BASE_URL + UrlConstants.MITRA_LOGIN;
            VolleySingleTon.getInstance().connectToPostUrl2(mContext, this, url, jsonObj, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        if (currentUser == 0) {
            Log.e("Response", "Response is " + response + " " + currentUser);
            try {
                JSONObject jsonObject = new JSONObject(response);
                String isSuccess = jsonObject.getString("IsSuccessful");
                Log.e("Response", "Response is " + isSuccess);
                if (isSuccess.equalsIgnoreCase("1")) {
                    userLoggedIn(currentUser, jsonObject);
                } else {
                    mLoginActivityIn.onLoginFailure(response);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (currentUser == 1) {
            Log.e("Response", "Response is " + response + " " + currentUser);
            try {
                JSONObject jsonObject = new JSONObject(response);
                String isSuccess = jsonObject.getString("messagetext");
                Log.e("Response", "Response is " + isSuccess);
                if (isSuccess.equalsIgnoreCase("SUCCESS")) {
                    userLoggedIn(currentUser, jsonObject);
                } else {
                    mLoginActivityIn.onLoginFailure(response);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        Log.e("Response", "Failed Response is " + response + " " + currentUser);
        mLoginActivityIn.onLoginFailure(response);
    }
}