package com.mahindra.mitra20.helper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.models.SessionUserDetails;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.XMART;

/**
 * Created by user on 8/11/2018.
 */

public class CommonHelper {

    public static String PAPER_DB_SESSION_USER_DETAILS = "sessionuserdetails";
    private static final String authCd = "YWRtaW46UHJvZDEyMyE=";
    public static final String EXTRA_EVALUATION = "evaluation";
    public static final String EXTRA_ARE_IMAGES_ONLINE = "online_images";
    public static final String EXTRA_IS_BROKER = "is_broker";
    public static final String EXTRA_AUCTION = "Auction";
    public static final String EXTRA_AUCTION_DB = "AuctionDB";
    public static final String EXTRA_CUSTOMER_EXPECTED_PRICE = "CustomerExpectedPrice";
    public static final String EXTRA_EVALUATION_STATUS = "EvaluationStatus";

    public static boolean isConnectingToInternet(Context appContext) {
        ConnectivityManager cm = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void toast(String txt, Context appContext) {

        Toast.makeText(appContext, txt, Toast.LENGTH_SHORT).show();
    }

    public static void toast(int txtId, Context appContext) {

        Toast.makeText(appContext, appContext.getResources().getText(txtId), Toast.LENGTH_SHORT).show();
    }

    public static void toast_long(String txt, Context appContext) {
        Toast.makeText(appContext, txt, Toast.LENGTH_LONG).show();
    }

    public static String hasJSONKey(JSONObject jsonObject, String key) {
        try {
            if (jsonObject.has(key)) {
                return jsonObject.getString(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static int parseLeadPriorityKey(JSONObject jsonObject, String key) {
        int leadPriority = 3;
        try {
            if (jsonObject.has(key)) {
                switch (jsonObject.getString(key)) {
                    case "P1":
                        leadPriority = 1;
                        break;
                    case "P2":
                        leadPriority = 2;
                        break;
                    case "P3":
                    default:
                        leadPriority = 3;
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leadPriority;
    }

        public static void settingCustomToolBar(final AppCompatActivity activity, String title) {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        activity.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        activity.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        View customView = activity.getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText(title);

        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }

    public static void settingCustomToolBarEvaluator(final AppCompatActivity activity,
                                                     String title, int visibility) {

        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setCustomView(R.layout.temp_tool_bar_evaluator);
        activity.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        activity.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        View customView = activity.getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        ImageView imageViewSave = customView.findViewById(R.id.imageViewSave);
        headingTv.setText(title);
        imageViewSave.setVisibility(visibility);
        imageViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewEvaluationActivity) activity).saveDraft();
            }
        });

        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }

    public static void makeCall(String strMobileNo, Context context) {
        try {
            if (strMobileNo == null || strMobileNo.isEmpty()) {
                CommonHelper.toast(context.getResources().getString(R.string.no_contact_available),
                        context);
            } else {
                Intent intentCall = new Intent(Intent.ACTION_DIAL,
                        Uri.parse("tel:" + strMobileNo));
                context.startActivity(intentCall);
            }
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void sendMessage(String strMobileNo, Context context) {
        try {
            if (strMobileNo == null || strMobileNo.isEmpty()) {
                CommonHelper.toast(context.getResources().getString(R.string.no_contact_available),
                        context);
            } else {
                Intent intentMessage = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms",
                        strMobileNo, null));
                context.startActivity(intentMessage);
            }
        } catch (ActivityNotFoundException e) {
            CommonHelper.toast(context.getResources().getString(R.string.no_sms_app_available),
                    context);
            e.printStackTrace();
        }
    }

    /**
     * Method to create url for downloading image through glide which have authentication params in header
     *
     * @param url url of image to be loaded
     * @return returns glide url to be put in .load() function of Glide
     */
    public static GlideUrl getAuthenticatedUrlForGlide(String url) {
        if (url != null && !url.isEmpty())
            return new GlideUrl(url, new LazyHeaders.Builder()
                    .addHeader("authCd", authCd)
                    .addHeader("Content-Type", "application/octet-stream")
                    .build());
        return null;
    }

    public static RequestOptions getGlideErrorImage() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(R.mipmap.mahindra_vehicle);
        return requestOptions;
    }

    public static RequestOptions getGlideProfileErrorImage() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(R.mipmap.ic_profile);
        return requestOptions;
    }

    public static boolean isChampionExistInList(List<AuctionLIST> listList) {

        for (int i = 0; i < listList.size(); i++) {
            AuctionLIST auctionLIST = listList.get(i);

            if (auctionLIST.getMITRAId().equalsIgnoreCase(SessionUserDetails.getInstance().getUserID())) {
                return true;
            }
        }

        return false;
    }

    public static Pair<String, String> getMakeModelSpinnerData(ArrayList<DmsMakeMasterModel> list,
                                                               String makeValue, String modelValue) {
        String makeStr = "", modelStr = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).getMake().equalsIgnoreCase(makeValue)) || (
                        list.get(i).getMakeCode().equalsIgnoreCase(makeValue))) {
                    makeStr = list.get(i).getMake();
                    if ((list.get(i).getModelCode().equalsIgnoreCase(modelValue)) || (
                            list.get(i).getModelDesc().equalsIgnoreCase(modelValue))) {
                        modelStr = list.get(i).getModelDesc();
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return new Pair<>(makeStr, modelStr);
    }

    public static String getHighestPrice(AuctionDetail auctionDetail) {
        String highestPrice = "";
        try {
            List<AuctionLIST> list = auctionDetail.getLIST();

            if (list.size() == 1 && list.get(0).getMITRAId().equalsIgnoreCase(
                    SessionUserDetails.getInstance().getUserID())) {

                highestPrice = auctionDetail.getMITRAPriceBasePrice();

                return highestPrice;
            }

            int[] listhighestPrice = new int[list.size()];
            try {

                for (int i = 0; i < list.size(); i++) {

                    AuctionLIST auctionLIST = list.get(i);
                    listhighestPrice[i] = Integer.parseInt(auctionLIST.getDeductedPrice());
                }

                highestPrice = String.valueOf(getHighestValue(listhighestPrice));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return highestPrice;
    }

    public static String replaceCars24MFCWChampion(String mitraName, String mitraID,
                                                   String NegotiatorContactName) {
        if (mitraName.contains("CARS TWENTY FOUR")) {

            return "Cars 24";
        } else if (mitraName.contains("MFCW")) {

            if (!NegotiatorContactName.isEmpty()) {

                return "Mahindra First Choice (" + NegotiatorContactName + ")";
            } else {

                return "Mahindra First Choice";
            }
        } else if (mitraID.equalsIgnoreCase(SessionUserDetails.getInstance().getUserID())) {
            return XMART;
        } else {
            return mitraName;
        }
    }

    public static int getHighestValue(int[] listhighestPrice) {
        int max = listhighestPrice[0];
        for (int i = 1; i < listhighestPrice.length; i++) {
            if (listhighestPrice[i] > max) {
                max = listhighestPrice[i];
            }
        }

        return max;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}