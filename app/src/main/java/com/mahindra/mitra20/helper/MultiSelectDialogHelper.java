package com.mahindra.mitra20.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.mahindra.mitra20.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by PATINIK-CONT on 18-Jun-19.
 */
public class MultiSelectDialogHelper {

    public static final String COMMA = ",";
    public static final String PIPE = "|";

    public static void showPicker(Context context, final EditText editText,
                                  Map<String, String> options, String title,
                                  final EditText editTextOther, final String separator) {
        final String[] valueList = options.values().toArray(new String[0]);
        final boolean[] selected = new boolean[valueList.length];
        if (null != editText && !editText.getText().toString().isEmpty()) {
            for (int i = 0; i < valueList.length; i++) {
                if (editText.getText().toString().contains(valueList[i])) {
                    selected[i] = true;
                }
            }
        }
        android.view.ContextThemeWrapper cw = new android.view.ContextThemeWrapper(context,
                R.style.AlertDialogTheme);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(cw);
        mBuilder.setTitle(title);
        mBuilder.setMultiChoiceItems(valueList, selected, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                selected[position] = isChecked;
            }
        });

        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String items = "";
                for (int i = 0; i < valueList.length; i++) {
                    if (selected[i]) {
                        items = items + valueList[i] + separator;
                    }
                }
                if (items.contains(separator)) {
                    items = items.substring(0, items.length() - 1);
                }
                if (null != editText) {
                    editText.setText(items);
                }
                if (null != editTextOther) {
                    if (items.contains("Others")) {
                        editTextOther.setVisibility(View.VISIBLE);
                    } else {
                        editTextOther.setVisibility(View.GONE);
                    }
                }
            }
        });

        mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.setNeutralButton("Clear all", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (null != editText) {
                    editText.setText("");
                }
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    public static String getKeysFromValues(String values, Map<String, String> options,
                                           String separator) {
        String items = "";
        try {
            List<String> valueList = new ArrayList<>(options.values());
            List<String> keyList = new ArrayList<>(options.keySet());
            for (int i = 0; i < valueList.size(); i++) {
                if (values.contains(valueList.get(i)))
                    items = items + keyList.get(i) + separator;
            }
            if (items.contains(separator)) {
                items = items.substring(0, items.length() - 1);
            }
            if (items.isEmpty()) {
                items = values;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public static String getValuesFromKeys(String keys, Map<String, String> options,
                                           String separator) {
        String items = "";
        try {
            List<String> valueList = new ArrayList<>(options.values());
            List<String> keyList = new ArrayList<>(options.keySet());
            for (int i = 0; i < keyList.size(); i++) {
                if (keys.contains(keyList.get(i)))
                    items = items + valueList.get(i) + separator;
            }
            if (items.contains(separator)) {
                items = items.substring(0, items.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }
}