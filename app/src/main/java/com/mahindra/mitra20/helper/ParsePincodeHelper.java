package com.mahindra.mitra20.helper;

import com.mahindra.mitra20.models.MasterDistrict;
import com.mahindra.mitra20.models.MasterPincode;
import com.mahindra.mitra20.models.MasterState;
import com.mahindra.mitra20.models.MasterTehsil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ParsePincodeHelper<T> {

    public List<T> parseList(JSONArray jsonArray, Class<?> c) {
        if (jsonArray.length() == 0)
            return null; //RETURN NULL

        if (c.getSimpleName().equals(MasterPincode.class.getSimpleName()))
            return (List<T>) parsePincodeList(jsonArray);
        else if (c.getSimpleName().equals(MasterState.class.getSimpleName()))
            return (List<T>) parseStateList(jsonArray);
        else if (c.getSimpleName().equals(MasterDistrict.class.getSimpleName()))
            return (List<T>) parseDistrictList(jsonArray);
        else if (c.getSimpleName().equals(MasterTehsil.class.getSimpleName()))
            return (List<T>) parseTehsiltList(jsonArray);
        else
            return null;

    }

    public List<MasterPincode> parsePincodeList(JSONArray jsonArray) {

        try {
            List<MasterPincode> pincodeList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                pincodeList.add(new MasterPincode(jsonArray.getJSONObject(i)));
            }

            return pincodeList; //RETURN LIST

        } catch (JSONException e) {
            e.printStackTrace();
            return null;//RETURN NULL
        }
    }

    public List<MasterState> parseStateList(JSONArray jsonArray) {


        try {
            List<MasterState> stateList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                stateList.add(new MasterState(jsonArray.getJSONObject(i)));
            }

            return stateList; //RETURN LIST

        } catch (JSONException e) {
            e.printStackTrace();
            return null;//RETURN NULL
        }
    }

    public List<MasterDistrict> parseDistrictList(JSONArray jsonArray) {


        try {
            List<MasterDistrict> districtList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                districtList.add(new MasterDistrict(jsonArray.getJSONObject(i)));
            }

            return districtList; //RETURN LIST

        } catch (JSONException e) {
            e.printStackTrace();
            return null;//RETURN NULL
        }
    }

    public List<MasterTehsil> parseTehsiltList(JSONArray jsonArray) {


        try {
            List<MasterTehsil> tehsilList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                tehsilList.add(new MasterTehsil(jsonArray.getJSONObject(i)));
            }

            return tehsilList; //RETURN LIST

        } catch (JSONException e) {
            e.printStackTrace();
            return null;//RETURN NULL
        }
    }
}
