package com.mahindra.mitra20.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.mahindra.mitra20.MyApplication;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.models.MasterDistrict;
import com.mahindra.mitra20.models.MasterPincode;
import com.mahindra.mitra20.models.MasterState;
import com.mahindra.mitra20.models.MasterTehsil;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.utils.LogUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by PATINIK-CONT on 31-Jul-19.
 */

public class MasterSyncHelper implements VolleySingleTon.VolleyInteractor {

    private Context context;
    private ProgressDialog dialog;
    private MasterDatabaseHelper masterDatabaseHelper;

    public MasterSyncHelper(Context context) {
        this.context = context;
        masterDatabaseHelper = new MasterDatabaseHelper(context);
    }

    public void syncMasters() {
        colorMaster();
    }

    private void colorMaster() {
        showProgressDialog("Downloading Masters (1/7)");
        HashMap<String, String> params = new HashMap<>();
        params.put(WebConstants.masterSync.RequestType, "COLOR_MST");//evaluatorId
        params.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
        params.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL, params, WebConstants.masterSync.REQUEST_CODE_COLOR_MST);
    }
//COMMENTED BY PRAVIN DHARAM ON 13-12-2019
//    private void stateMaster() {
//        showProgressDialog("Downloading Masters (2/8)");
//        //State Master
//        HashMap<String, String> paramsState = new HashMap<>();
//        paramsState.put(WebConstants.masterSync.RequestType, "STATE_MST");//evaluatorId
//        paramsState.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
//        paramsState.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime
//
//        VolleySingleTon.getInstance().connectToPostUrl(context, this,
//                WebConstants.masterSync.URL, paramsState, WebConstants.masterSync.REQUEST_CODE_STATE_MST);
//    }
//
//    private void cityMaster() {
//        showProgressDialog("Downloading Masters (3/8)");
//        //City Master
//        HashMap<String, String> paramsCity = new HashMap<>();
//        paramsCity.put(WebConstants.masterSync.RequestType, "CITY_MST");//evaluatorId
//        paramsCity.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
//        paramsCity.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime
//
//        VolleySingleTon.getInstance().connectToPostUrl(context, this,
//                WebConstants.masterSync.URL, paramsCity, WebConstants.masterSync.REQUEST_CODE_CITY_MST);
//    }

    //ADDED BY PRAVIN DHARAM ON 14-12-2019
    private void call_Pincode_State_District_Tehsil_API() {
        showProgressDialog("Downloading Masters (7/7)");

        SharedPreferences pref = MyApplication.getMyApplicationContext().getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
        String lastPincodeSyncDate = pref.getString(SharedPreferenceKeys.LAST_PINCODE_SYNC_DATE, "18/12/2019");
        //City Master
        HashMap<String, String> paramsCity = new HashMap<>();
        paramsCity.put(WebConstants.masterSync.FIELD_IMEI_NUMBER, "");
        paramsCity.put(WebConstants.masterSync.FIELD_DATE, lastPincodeSyncDate);//appointmentStatus

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL_PINCODE, paramsCity, WebConstants.masterSync.REQUEST_CODE_PINCODE_STATE_DIST_TEHSIL);
    }

    private void variantMaster() {
        showProgressDialog("Downloading Masters (2/7)");
        //VARIANT Master
        HashMap<String, String> paramsVARIANT = new HashMap<>();
        paramsVARIANT.put(WebConstants.masterSync.RequestType, "VARIANT_MST");//evaluatorId
        paramsVARIANT.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
        paramsVARIANT.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL, paramsVARIANT, WebConstants.masterSync.REQUEST_CODE_VARIANT_MST);
    }

    private void makeMaster() {
        showProgressDialog("Downloading Masters (3/7)");
        //Make Master
        HashMap<String, String> paramsMake = new HashMap<>();
        paramsMake.put(WebConstants.masterSync.RequestType, "MAKE_MST");//evaluatorId
        paramsMake.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
        paramsMake.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL, paramsMake, WebConstants.masterSync.REQUEST_CODE_MAKE_MST);
    }

    private void vehicleSegmentMaster() {
        showProgressDialog("Downloading Masters (4/7)");
        //Vehicle Segment Master
        HashMap<String, String> paramsVehicleSegment = new HashMap<>();
        paramsVehicleSegment.put(WebConstants.masterSync.RequestType, "VEHCL_SEGMNT_MST");//evaluatorId
        paramsVehicleSegment.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
        paramsVehicleSegment.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL,
                paramsVehicleSegment, WebConstants.masterSync.REQUEST_CODE_VEHCL_SEGMNT_MST);
    }

    private void modelMaster() {
        showProgressDialog("Downloading Masters (5/7)");
        //Model Master
        HashMap<String, String> paramsModel = new HashMap<>();
        paramsModel.put(WebConstants.masterSync.RequestType, "MODL_MST");//evaluatorId
        paramsModel.put(WebConstants.masterSync.ImeiNo, "");//appointmentStatus
        paramsModel.put(WebConstants.masterSync.LastUpdatedDate, "");//appointmentDateTime

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.masterSync.URL, paramsModel, WebConstants.masterSync.REQUEST_CODE_MODEL_MST);
    }

    private void dmsMakeModelMaster() {
        showProgressDialog("Downloading Masters (6/7)");
        //Vehicle Segment Master
        HashMap<String, String> paramsDmsMakeMaster = new HashMap<>();
        paramsDmsMakeMaster.put(WebConstants.dmsMakeMaster.IMEI_NO, "123");
        paramsDmsMakeMaster.put(WebConstants.dmsMakeMaster.REQUEST_TYPE, "");
        paramsDmsMakeMaster.put(WebConstants.dmsMakeMaster.LAST_UPDATED_DATE, "");

        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.dmsMakeMaster.URL,
                paramsDmsMakeMaster, WebConstants.dmsMakeMaster.REQUEST_CODE);
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case WebConstants.masterSync.REQUEST_CODE_COLOR_MST:
                try {
                    ArrayList<MasterModel> listColorMaster = new ArrayList<>();

                    JSONObject jsonObjectParent = new JSONObject(response);
                    JSONArray jsonArrayColorMasterList = jsonObjectParent.getJSONArray("COLOR_MST");
                    for (int i = 0; i < jsonArrayColorMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayColorMasterList.getJSONObject(i);
                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");

                        for (int j = 0; j < jsonarrayROW.length(); j++) {
                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);

                            MasterModel masterModel = new MasterModel();
                            masterModel.setCode(jsonnewtwo.getString("Code"));
                            masterModel.setDescription(jsonnewtwo.getString("Description"));
                            listColorMaster.add(masterModel);
                        }
                    }
                    masterDatabaseHelper.insertColorMaster(listColorMaster);

                    variantMaster();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            //COMMENTED BY PRAVIN DHARAM ON 13-12-2019
//            case WebConstants.masterSync.REQUEST_CODE_STATE_MST:
//                try {
//                    ArrayList<MasterModel> listStateMaster = new ArrayList<>();
//
//                    JSONObject jsonObjectStateParent = new JSONObject(response);
//                    JSONArray jsonArrayStateMasterList = jsonObjectStateParent.getJSONArray("STATE_MST");
//                    for (int i = 0; i < jsonArrayStateMasterList.length(); i++) {
//
//                        JSONObject jsonnew = jsonArrayStateMasterList.getJSONObject(i);
//                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");
//
//                        for (int j = 0; j < jsonarrayROW.length(); j++) {
//                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);
//
//                            MasterModel masterModel = new MasterModel();
//                            masterModel.setCode(jsonnewtwo.getString("Code"));
//                            masterModel.setDescription(jsonnewtwo.getString("Description"));
//                            listStateMaster.add(masterModel);
//                        }
//                    }
////                    masterDatabaseHelper.insertStateMaster(listStateMaster);
//                    cityMaster();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    hideProgressDialog();
//                }
//                break;
//            case WebConstants.masterSync.REQUEST_CODE_CITY_MST:
//                try {
//                    ArrayList<MasterModel> listCityMaster = new ArrayList<>();
//
//                    JSONObject jsonObjectCityParent = new JSONObject(response);
//                    JSONArray jsonArrayCityMasterList = jsonObjectCityParent.getJSONArray("CITY_MST");
//                    for (int i = 0; i < jsonArrayCityMasterList.length(); i++) {
//
//                        JSONObject jsonnew = jsonArrayCityMasterList.getJSONObject(i);
//                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");
//
//
//                        MasterModel masterModel_header = new MasterModel();
//                        masterModel_header.setCode("Select City");
//                        masterModel_header.setDescription("Select City");
//                        listCityMaster.add(0, masterModel_header);
//
//
//                        for (int j = 0; j < jsonarrayROW.length(); j++) {
//                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);
//
//                            MasterModel masterModel = new MasterModel();
//                            masterModel.setCode(jsonnewtwo.getString("Code"));
//                            masterModel.setDescription(jsonnewtwo.getString("Description"));
//                            listCityMaster.add(masterModel);
//                        }
//                    }
//
//                    // masterDatabaseHelper.insertCityMaster(listCityMaster);
//                    variantMaster();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    hideProgressDialog();
//                }
//                break;
            case WebConstants.masterSync.REQUEST_CODE_VARIANT_MST:
                try {
                    ArrayList<MasterModel> listVARIANTMaster = new ArrayList<>();

                    JSONObject jsonObjectVARIANTParent = new JSONObject(response);
                    JSONArray jsonArrayVARIANTMasterList =
                            jsonObjectVARIANTParent.getJSONArray("VARIANT_MST");
                    for (int i = 0; i < jsonArrayVARIANTMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayVARIANTMasterList.getJSONObject(i);
                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");

                        for (int j = 0; j < jsonarrayROW.length(); j++) {
                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);

                            MasterModel masterModel = new MasterModel();
                            masterModel.setCode(jsonnewtwo.getString("Code"));
                            masterModel.setDescription(jsonnewtwo.getString("Description"));
                            listVARIANTMaster.add(masterModel);
                        }
                    }
                    masterDatabaseHelper.insertVariantMaster(listVARIANTMaster);
                    makeMaster();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            case WebConstants.masterSync.REQUEST_CODE_MAKE_MST:
                try {
                    ArrayList<MasterModel> listMakeMaster = new ArrayList<>();

                    JSONObject jsonObjectMakeParent = new JSONObject(response);
                    JSONArray jsonArrayMakeMasterList = jsonObjectMakeParent.getJSONArray("MAKE_MST");
                    for (int i = 0; i < jsonArrayMakeMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayMakeMasterList.getJSONObject(i);
                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");

                        for (int j = 0; j < jsonarrayROW.length(); j++) {
                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);

                            MasterModel masterModel = new MasterModel();
                            masterModel.setCode(jsonnewtwo.getString("Code"));
                            masterModel.setDescription(jsonnewtwo.getString("Description"));
                            listMakeMaster.add(masterModel);
                        }
                    }

                    masterDatabaseHelper.insertMakeMaster(listMakeMaster);
                    vehicleSegmentMaster();

                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            case WebConstants.masterSync.REQUEST_CODE_VEHCL_SEGMNT_MST:
                try {
                    ArrayList<MasterModel> listVehicleSegmentMaster = new ArrayList<>();

                    JSONObject jsonObjectVehicleSegmentParent = new JSONObject(response);
                    JSONArray jsonArrayVehicleSegmentMasterList =
                            jsonObjectVehicleSegmentParent.getJSONArray("VEHCL_SEGMNT_MST");
                    for (int i = 0; i < jsonArrayVehicleSegmentMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayVehicleSegmentMasterList.getJSONObject(i);
                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");

                        MasterModel masterModel_header = new MasterModel();
                        masterModel_header.setCode("Select Vehicle Segment");
                        masterModel_header.setDescription("Select Vehicle Segment");
                        listVehicleSegmentMaster.add(0, masterModel_header);

                        for (int j = 0; j < jsonarrayROW.length(); j++) {
                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);

                            MasterModel masterModel = new MasterModel();
                            masterModel.setCode(jsonnewtwo.getString("Code"));
                            masterModel.setDescription(jsonnewtwo.getString("Description"));
                            listVehicleSegmentMaster.add(masterModel);
                        }
                    }
                    masterDatabaseHelper.insertVehicleSegmentMaster(listVehicleSegmentMaster);
                    modelMaster();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            case WebConstants.masterSync.REQUEST_CODE_MODEL_MST:
                try {
                    ArrayList<MasterModel> listModelMaster = new ArrayList<>();

                    JSONObject jsonObjectModelParent = new JSONObject(response);
                    JSONArray jsonArrayModelMasterList =
                            jsonObjectModelParent.getJSONArray("MODL_MST");
                    for (int i = 0; i < jsonArrayModelMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayModelMasterList.getJSONObject(i);
                        JSONArray jsonarrayROW = jsonnew.getJSONArray("MasterRecord");

                        for (int j = 0; j < jsonarrayROW.length(); j++) {
                            JSONObject jsonnewtwo = jsonarrayROW.getJSONObject(j);

                            MasterModel masterModel = new MasterModel();
                            masterModel.setCode(jsonnewtwo.getString("Code"));
                            masterModel.setDescription(jsonnewtwo.getString("Description"));
                            listModelMaster.add(masterModel);
                        }
                    }
                    masterDatabaseHelper.insertModelMaster(listModelMaster);
                    dmsMakeModelMaster();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            case WebConstants.dmsMakeMaster.REQUEST_CODE:
                try {
                    ArrayList<DmsMakeMasterModel> listModelMaster = new ArrayList<>();

                    JSONObject jsonObjectModelParent = new JSONObject(response);
                    JSONArray jsonArrayModelMasterList =
                            jsonObjectModelParent.getJSONArray("ExchangeMakeModelDetails");
                    for (int i = 0; i < jsonArrayModelMasterList.length(); i++) {

                        JSONObject jsonnew = jsonArrayModelMasterList.getJSONObject(i);
                        DmsMakeMasterModel dmsMakeMasterModel = new DmsMakeMasterModel();
                        dmsMakeMasterModel.setMake(jsonnew.getString("Make"));
                        dmsMakeMasterModel.setMakeCode(jsonnew.getString("MakeCode"));
                        dmsMakeMasterModel.setModelCode(jsonnew.getString("ModelCode"));
                        dmsMakeMasterModel.setModelDesc(jsonnew.getString("ModelDesc"));
                        dmsMakeMasterModel.setSortOrder(jsonnew.getString("sortOrder"));
                        listModelMaster.add(dmsMakeMasterModel);
                    }
                    masterDatabaseHelper.putDmsMasterModel(listModelMaster);
                    call_Pincode_State_District_Tehsil_API();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }
                break;
            case WebConstants.masterSync.REQUEST_CODE_PINCODE_STATE_DIST_TEHSIL:
                try {
                    JSONObject jObj = new JSONObject(response);
                    LogUtil.getInstance().logE("Master", "IsSuccessfull = " + jObj.getString("IsSuccessfull"));
                    ParsePincodeHelper parsePincodeHelper = new ParsePincodeHelper();
                    if (jObj.has("IsSuccessfull") && jObj.getString("IsSuccessfull").equals("1")) {

                        //SET LAST SYNC DATE IN PREFERENCE
                        SharedPreferences pref = MyApplication.getMyApplicationContext().getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        String strDate = formatter.format(date);
                        pref.edit().putString(SharedPreferenceKeys.LAST_PINCODE_SYNC_DATE, strDate).commit();

                        LogUtil.getInstance().logE("Master", "Pincode LIST = " + jObj.getJSONArray("Pincode").length());
                        //Pincode

                        List<MasterPincode> pincodeList = parsePincodeHelper.parseList(jObj.getJSONArray("Pincode"), MasterPincode.class);
                        if (pincodeList != null && pincodeList.size() > 0) {
                            masterDatabaseHelper.insertUpdatePincodeList(pincodeList);
                        }

                        LogUtil.getInstance().logE("Master", "State LIST = " + jObj.getJSONArray("State").length());
                        //State

                        List<MasterState> stateList = parsePincodeHelper.parseList(jObj.getJSONArray("State"), MasterState.class);
                        if (stateList != null && stateList.size() > 0) {
                            masterDatabaseHelper.insertUpdateStateList(stateList);
                        }

                        LogUtil.getInstance().logE("Master", "District LIST = " + jObj.getJSONArray("District").length());
                        //District

                        List<MasterDistrict> districtList = parsePincodeHelper.parseList(jObj.getJSONArray("District"), MasterDistrict.class);
                        if (districtList != null && districtList.size() > 0) {
                            masterDatabaseHelper.insertUpdateDistrictList(districtList);
                        }

                        LogUtil.getInstance().logE("Master", "Tehsil LIST = " + jObj.getJSONArray("Tehsil").length());
                        //Tehsil

                        List<MasterTehsil> tehsilList = parsePincodeHelper.parseList(jObj.getJSONArray("Tehsil"), MasterTehsil.class);
                        if (tehsilList != null && tehsilList.size() > 0) {
                            masterDatabaseHelper.insertUpdateTehsilList(tehsilList);
                        }


                    }

                } catch (Exception e) {
                    LogUtil.getInstance().logE("Master", "JSONException = " + e.getMessage());
                    e.printStackTrace();
                }
                break;
        }

        if (requestId == WebConstants.masterSync.REQUEST_CODE_PINCODE_STATE_DIST_TEHSIL) {
            hideProgressDialog();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        CommonHelper.toast(response, context);

        hideProgressDialog();
    }

    private void showProgressDialog(String msg) {
        if (null == dialog) {
            dialog = new ProgressDialog(context);
        }
        dialog.setCancelable(false);
        if (null != msg && !msg.isEmpty()) {
            dialog.setMessage(msg);
        } else {
            dialog.setMessage("Wait while loading");
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}