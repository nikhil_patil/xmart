package com.mahindra.mitra20;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


public class Activity_Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_splash);

        moveToNextPage();
    }

    private void moveToNextPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Activity_Splash.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }
}