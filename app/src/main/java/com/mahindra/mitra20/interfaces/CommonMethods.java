package com.mahindra.mitra20.interfaces;

import com.mahindra.mitra20.evaluator.models.NewEvaluation;

/**
 * Created by PATINIK-CONT on 31-Dec-19.
 */
public interface CommonMethods {

    void init();
    void displayData();
    NewEvaluation getData();
    void saveData();
    boolean isDataValid();
    void fillTestDataInForm();
}
