package com.mahindra.mitra20.interfaces;

/**
 * Created by user on 8/26/2018.
 */

public interface ContactIn {
    void onCallPress(String contactNo);
    void onMessagePress(String contactNo);
}