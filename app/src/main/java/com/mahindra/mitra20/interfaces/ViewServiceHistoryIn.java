package com.mahindra.mitra20.interfaces;

import com.mahindra.mitra20.models.ServiceHistory;

/**
 * Created by user on 6/26/2019.
 */

public interface ViewServiceHistoryIn {
    void onServiceHistory(ServiceHistory serviceHistory);
    void onDownloadFail(String msg);
}
