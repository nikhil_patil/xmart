package com.mahindra.mitra20.utils;

import android.content.Context;

import com.mahindra.mitra20.MyApplication;

import java.io.IOException;
import java.io.InputStream;

public class AssetUtils {

    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = MyApplication.getMyApplicationContext().getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }



}
