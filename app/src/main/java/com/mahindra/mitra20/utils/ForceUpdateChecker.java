package com.mahindra.mitra20.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mahindra.mitra20.BuildConfig;


/**
 * Created by GAVHPR-CONT on 12/15/2017.
 */

public class ForceUpdateChecker {

    public static final String KEY_UPDATE_REQUIRED = "force_update_required";
    public static final String KEY_CURRENT_VERSION = "force_update_current_version";
    public static final String KEY_UPDATE_URL = "force_update_store_url";
    private static final String TAG = ForceUpdateChecker.class.getSimpleName();
    private OnUpdateNeededListener onUpdateNeededListener;
    private Context context;

    public ForceUpdateChecker(@NonNull Context context,
                              OnUpdateNeededListener onUpdateNeededListener) {
        this.context = context;
        this.onUpdateNeededListener = onUpdateNeededListener;
    }

    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }

    public void check() {
        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();

        LogUtil.getInstance().logE("ForceUpdate", "\n" +
                "IS UPDATE MAJOR: " + remoteConfig.getBoolean(KEY_UPDATE_REQUIRED) + "\n" +
                "PLAY STORE CURRENT VERSION: " + remoteConfig.getString(KEY_CURRENT_VERSION) + "\n" +
                "APP VERSION CODE: " + BuildConfig.VERSION_CODE + "\n" +
                "PLAY STORE URL: " + remoteConfig.getString(KEY_UPDATE_URL));


        String strCurrentVersion = remoteConfig.getString(KEY_CURRENT_VERSION);
        int currentVersion = Integer.parseInt(strCurrentVersion);
        String updateUrl = remoteConfig.getString(KEY_UPDATE_URL);
        boolean isMajorUpdate = remoteConfig.getBoolean(KEY_UPDATE_REQUIRED);

        if (currentVersion > BuildConfig.VERSION_CODE) {
            onUpdateNeededListener.onUpdateNeeded(updateUrl, isMajorUpdate);
        } else {
            onUpdateNeededListener.updateNotFound();
        }
    }


    public interface OnUpdateNeededListener {
        void onUpdateNeeded(String updateUrl, boolean isMajorUpdate);

        void updateNotFound();
    }

    public static class Builder {

        private Context context;
        private OnUpdateNeededListener onUpdateNeededListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateNeeded(OnUpdateNeededListener onUpdateNeededListener) {
            this.onUpdateNeededListener = onUpdateNeededListener;
            return this;
        }

        public ForceUpdateChecker build() {
            return new ForceUpdateChecker(context, onUpdateNeededListener);
        }

        public ForceUpdateChecker check() {
            ForceUpdateChecker forceUpdateChecker = build();
            forceUpdateChecker.check();
            return forceUpdateChecker;
        }
    }


}
