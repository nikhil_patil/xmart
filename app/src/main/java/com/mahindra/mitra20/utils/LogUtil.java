package com.mahindra.mitra20.utils;

import android.util.Log;

import com.mahindra.mitra20.BuildConfig;

public class LogUtil {
    private static final LogUtil ourInstance = new LogUtil();

    public static LogUtil getInstance() {
        return ourInstance;
    }

    private LogUtil() {

    }

    public void logE(String tag, String msg) {
        if (BuildConfig.DEBUG)
            Log.e(tag, msg);
    }
}