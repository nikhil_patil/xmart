package com.mahindra.mitra20.utils;

import com.mahindra.mitra20.constants.AuctionType;

public class AuctionIntervalUtils implements AuctionType {

    public static final int AUCTION_API_INTERVAL = 60000;//1 minuets in milliseconds
    private long liveAuctionAPiInterval;
    private long completedAuctionAPiInterval;
    private long pendingAuctionAPiInterval;
    private boolean liveAuctionFirstAPICall = true;
    private boolean completedAuctionFirstAPICall = true;
    private boolean pendingAuctionFirstAPICall = true;

    /**
     * @param type Auction Type which auction want to call API
     * @return true when it's first API Call or if @AUCTION_API_INTERVAL is passed for particular type
     */
    public boolean isFirstAPIOrValidInterval(int type) {
        switch (type) {
            case LIVE:
                return checkLive();
            case PENDING:
                return checkPending();
            case COMPLETED:
                return checkCompleted();
            default:
                return false;
        }

    }

    private boolean checkLive() {
        if (liveAuctionFirstAPICall || isIntervalPassed(liveAuctionAPiInterval)) {
            liveAuctionFirstAPICall = false;
            liveAuctionAPiInterval = System.currentTimeMillis();
            return true;
        } else return false;
    }

    private boolean checkPending() {
        if (pendingAuctionFirstAPICall || isIntervalPassed(pendingAuctionAPiInterval)) {
            pendingAuctionFirstAPICall = false;
            pendingAuctionAPiInterval = System.currentTimeMillis();
            return true;
        } else return false;
    }

    private boolean checkCompleted() {
        if (completedAuctionFirstAPICall || isIntervalPassed(completedAuctionAPiInterval)) {
            completedAuctionFirstAPICall = false;
            completedAuctionAPiInterval = System.currentTimeMillis();
            return true;
        } else return false;
    }

    /**
     * @param auctionAPIInterval last capture time in milliseconds for particular auction type
     * @return true if interval is passed 1 minute difference from last capture time
     */
    private boolean isIntervalPassed(long auctionAPIInterval) {
        int result = (int) (System.currentTimeMillis() - auctionAPIInterval);
        return result > AUCTION_API_INTERVAL;
    }

    public void resetValues() {
        liveAuctionFirstAPICall = true;
        completedAuctionFirstAPICall = true;
        pendingAuctionFirstAPICall = true;
        liveAuctionAPiInterval = 0L;
        completedAuctionAPiInterval = 0L;
        pendingAuctionAPiInterval = 0L;
    }
}
