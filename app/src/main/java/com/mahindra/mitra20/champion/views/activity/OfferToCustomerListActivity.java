package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.EnterXmartPrice;
import com.mahindra.mitra20.champion.presenter.MyLiveAuctionIn;
import com.mahindra.mitra20.champion.presenter.MyLiveAuctionPresenter;
import com.mahindra.mitra20.champion.views.adapters.EnterXMartPriceAdapter;
import com.mahindra.mitra20.databinding.ActivityOfferToCustomerListBinding;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;

public class OfferToCustomerListActivity extends AppCompatActivity implements ItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, MyLiveAuctionIn {

    private ActivityOfferToCustomerListBinding binding;
    private ArrayList<EnterXmartPrice> xmartPriceContents = new ArrayList<>();
    private MyLiveAuctionPresenter myLiveAuctionPresenter;
    private ArrayList<AuctionDetail> auctionDetailArrayList;
    private EnterXMartPriceAdapter enterXMartPriceAdapter;
    private ArrayList<String> evaluationReportIDsOFFR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer_to_customer_list);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Offer to Customer");

        myLiveAuctionPresenter = new MyLiveAuctionPresenter(this, this);
        ChampionHelper championHelper = new ChampionHelper(getBaseContext());

        auctionDetailArrayList = new ArrayList<>();
        evaluationReportIDsOFFR = championHelper.getEvaluationReportIDsOFFR();

        binding.swipeContainer.setOnRefreshListener(this);
        if (auctionDetailArrayList.size() == 0) {
            binding.swipeContainer.setRefreshing(true);
            onRefresh();
        }

        for (int i = 0; i < auctionDetailArrayList.size(); i++) {
            AuctionDetail auctionDetail = auctionDetailArrayList.get(i);

            EnterXmartPrice enterXmartPrice = new EnterXmartPrice();
            enterXmartPrice.setRequestID(auctionDetail.getRequestID());
            enterXmartPrice.setStrName(auctionDetail.getVehOwnerName());
            enterXmartPrice.setStrVehicleName(auctionDetail.getVehicleName());
            enterXmartPrice.setStrHighestPrice(CommonHelper.getHighestPrice(auctionDetail));

            xmartPriceContents.add(enterXmartPrice);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        binding.recyclerViewXmartPendingPrice.setLayoutManager(linearLayoutManager);
        binding.recyclerViewXmartPendingPrice.setEmptyView(binding.textViewEmpty);
        enterXMartPriceAdapter = new EnterXMartPriceAdapter(getBaseContext(), xmartPriceContents);
        binding.recyclerViewXmartPendingPrice.setAdapter(enterXMartPriceAdapter);
        enterXMartPriceAdapter.setmonClick(this);
    }

    @Override
    public void onClick(View view, int position) {
        AuctionDetail auctionDetail = getHighestBidderBOByAuctionDetailsList(
                xmartPriceContents.get(position));
        Intent intentHighestBidder = new Intent(this, OfferToCustomerActivity.class);
        intentHighestBidder.putExtra(ChampionConstants.SELECTED_AUCTION_DATA, auctionDetail);
        startActivity(intentHighestBidder);
    }

    private AuctionDetail getHighestBidderBOByAuctionDetailsList(EnterXmartPrice enterXmartPrice) {
        try {
            for (int i = 0; i < auctionDetailArrayList.size(); i++) {
                if (enterXmartPrice.getRequestID().equalsIgnoreCase(
                        auctionDetailArrayList.get(i).getRequestID())) {
                    return auctionDetailArrayList.get(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            myLiveAuctionPresenter.downloadLiveAuctionMaster(ChampionConstants.COMPLETED_STATUS);
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<AuctionDetail> auctionDetailArrayList) {
        if (auctionDetailArrayList != null && !auctionDetailArrayList.isEmpty()) {
            this.auctionDetailArrayList = auctionDetailArrayList;
            xmartPriceContents.clear();
            for (int i = 0; i < auctionDetailArrayList.size(); i++) {
                AuctionDetail auctionDetail = auctionDetailArrayList.get(i);

                if (!evaluationReportIDsOFFR.contains(auctionDetail.getRequestID())) {
                    EnterXmartPrice enterXmartPrice = new EnterXmartPrice();
                    enterXmartPrice.setRequestID(auctionDetail.getRequestID());
                    enterXmartPrice.setStrName(auctionDetail.getVehOwnerName());
                    enterXmartPrice.setStrVehicleName(auctionDetail.getVehicleName());
                    enterXmartPrice.setStrHighestPrice(CommonHelper.getHighestPrice(auctionDetail));

                    xmartPriceContents.add(enterXmartPrice);
                }
            }

            this.enterXMartPriceAdapter.updateList(xmartPriceContents);
            enterXMartPriceAdapter.notifyDataSetChanged();
        }

        binding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        binding.swipeContainer.setRefreshing(false);
        CommonHelper.toast(message, getBaseContext());
    }
}