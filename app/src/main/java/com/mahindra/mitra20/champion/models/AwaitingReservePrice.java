package com.mahindra.mitra20.champion.models;

/**
 * Created by BADHAP-CONT on 7/24/2018.
 */

public class AwaitingReservePrice {
    String strName;
    String strVehicleName;
    String strLocation;

    public AwaitingReservePrice(String strName, String strVehicleName, String strLocation) {
        this.strName = strName;
        this.strVehicleName = strVehicleName;
        this.strLocation = strLocation;
    }

    public String getStrName() {

        return strName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public String getStrLocation() {
        return strLocation;
    }
}
