package com.mahindra.mitra20.champion.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.views.activity.CancelRequestDetailsActivity;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.ASSIGN_REQUEST_MODEL_KEY;


/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class ChampionCancelledAppointmentAdapter extends RecyclerView.Adapter<ChampionCancelledAppointmentAdapter.ViewHolder> {

    private ArrayList<EnquiryRequest> CancelAppointmentModelContents;
    Context context;
    private String type;
    private DateHelper dateHelper;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewBackground, textViewMonth, textViewDate, textViewTime, tvEnquirySource,
                textViewCustomerName, textViewVehicleName, textViewLocation, textViewLeadPriority, tvEnquiryType;
        LinearLayout linearLayoutOverdueVisits;
        public View layout;
        public ImageView imageViewCall, imageViewMessage;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewMonth = v.findViewById(R.id.textViewMonth);
            textViewDate = v.findViewById(R.id.textViewDate);
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewBackground = v.findViewById(R.id.textViewBackground);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewMessage = v.findViewById(R.id.imageViewMessage);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);
            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);

            linearLayoutOverdueVisits = v.findViewById(R.id.linearLayoutOverdueVisits);
        }
    }

    public void updateList(ArrayList<EnquiryRequest> CancelAppointmentModelContents) {
        this.CancelAppointmentModelContents = CancelAppointmentModelContents;
    }

    public ChampionCancelledAppointmentAdapter(Context _context, ArrayList<EnquiryRequest> _CancelAppointmentModelContents, String _type) {
        CancelAppointmentModelContents = _CancelAppointmentModelContents;
        context = _context;
        type = _type;
        dateHelper = new DateHelper();
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper = new MasterDatabaseHelper(context);
    }

    @Override
    public ChampionCancelledAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                             int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_overdue_visits, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String strDate = CancelAppointmentModelContents.get(position).getDateOfAppointment();
        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        String strMonth = dateHelper.getMonthName(appointmentDate);
        String strAppointmentDate = dateHelper.getDate(appointmentDate);
        String strTime = dateHelper.convertTwentyFourHourToTweleveHour(CancelAppointmentModelContents.get(position).getTimeOfAppointment());

        holder.textViewMonth.setText(strMonth);
        holder.textViewDate.setText(strAppointmentDate);
        holder.textViewTime.setText(strTime);
        holder.textViewCustomerName.setText(CancelAppointmentModelContents.get(position).getNameOfEnquiry());
        holder.textViewLeadPriority.setVisibility(View.INVISIBLE);
        holder.textViewLocation.setText(CancelAppointmentModelContents.get(position).getLocation());

        String strMake = CancelAppointmentModelContents.get(position).getMake();
        String strModel = CancelAppointmentModelContents.get(position).getModel();
        holder.textViewVehicleName.setText(String.format("%s %s", strMake, strModel));
        holder.tvEnquirySource.setVisibility(View.GONE);
        holder.tvEnquiryType.setVisibility(View.GONE);

        if (type.equals("Overdue") || type.equals("Draft")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorOrange));
        } else if (type.equals("Upcoming")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        } else if (type.equals("Completed")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
        } else {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        }

        holder.linearLayoutOverdueVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masterDatabaseHelper.isMasterSync()) {
                    EnquiryRequest scheduleAppoinmet = CancelAppointmentModelContents.get(position);
                    Intent intent = new Intent(context, CancelRequestDetailsActivity.class);
                    intent.putExtra(ASSIGN_REQUEST_MODEL_KEY, scheduleAppoinmet);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
            }
        });

        holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.makeCall(CancelAppointmentModelContents.get(position).getContactNo(), context);
            }
        });

        holder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.
                        sendMessage(CancelAppointmentModelContents.get(position).getContactNo(), context);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return CancelAppointmentModelContents.size();
    }


    String getSpinnerData(ArrayList<MasterModel> list, String value) {
        String strValue = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).getDescription().equalsIgnoreCase(value)) || (list.get(i).getCode().equalsIgnoreCase(value))) {
                    strValue = list.get(i).getDescription();
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return strValue;
    }
}