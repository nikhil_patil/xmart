package com.mahindra.mitra20.champion.presenter;

/**
 * Created by user on 8/18/2018.
 */

public interface PlaceBidIn {

    void uploadSuccess(int requestID, String data);

    void uploadFailed(int requestID, String data);

}
