package com.mahindra.mitra20.champion.helper;

import android.widget.Spinner;
import android.widget.TextView;

import com.mahindra.mitra20.evaluator.models.MasterModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by PATINIK-CONT on 23-May-19.
 */
public class SpinnerHelper {
    public static void setSpinnerValueFromMap(String[] list, String valueKey, Spinner spinner,
                                              Map<String, String> pairs) {
        try {
            String value = "";
            if (pairs.containsKey(valueKey)) {
                value = pairs.get(valueKey);
            }
            for (int i = 0; i < list.length; i++) {
                if ((list[i].equalsIgnoreCase(value)) || (String.valueOf(i).equalsIgnoreCase(value))
                        || (list[i].substring(0, 1).equalsIgnoreCase(value))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void setTextValueFromMap(String[] list, String valueKey, TextView textView,
                                           Map<String, String> pairs) {
        try {
            String value = "";
            if (pairs.containsKey(valueKey)) {
                value = pairs.get(valueKey);
            }
            for (int i = 0; i < list.length; i++) {
                if ((list[i].equalsIgnoreCase(value)) || (String.valueOf(i).equalsIgnoreCase(value))
                        || (list[i].substring(0, 1).equalsIgnoreCase(value))) {
                    textView.setText(list[i]);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static String getKeyFromMapValues(String value, Map<String, String> stringMap) {
        for (Map.Entry<String, String> entry : stringMap.entrySet()) {
            if (entry.getValue().equalsIgnoreCase(value)) {
                return entry.getKey();
            }
        }
        return "";
    }

    public static void setSpinnerValue(ArrayList<MasterModel> list, String value, Spinner spinner) {
        try {
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).getDescription().equalsIgnoreCase(value)) || (list.get(i).getCode().equalsIgnoreCase(value))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static int getSpinnerPosition(Spinner spinner, int count) {
        if (spinner.getSelectedItemPosition() > 0) {
            count += +1;
        }
        return count;
    }

    public static void setSpinnerValue(String[] list, String value, Spinner spinner) {
        try {
            for (int i = 0; i < list.length; i++) {
                if ((list[i].equalsIgnoreCase(value)) || (String.valueOf(i).equalsIgnoreCase(value))
                        || (list[i].substring(0, 1).equalsIgnoreCase(value))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void setSpinnerValueAirBag(String[] list, String value, Spinner spinner) {
        try {
            for (int i = 0; i < list.length; i++) {
                if (list[i].equalsIgnoreCase(value)) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void setSpinnerValue(String[] list, String[] listCode, String value, Spinner spinner) {
        try {
            for (int i = 0; i < list.length; i++) {
                if ((listCode[i].equalsIgnoreCase(value)) ||
                        (String.valueOf(i).equalsIgnoreCase(value)) ||
                        (list[i].substring(0, 1).equalsIgnoreCase(value))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void setSpinnerValueByCode(String[] listCode, String value, Spinner spinner) {
        try {
            for (int i = 0; i < listCode.length; i++) {
                if ((listCode[i].equalsIgnoreCase(value)) ||
                        (String.valueOf(i).equalsIgnoreCase(value)) ||
                        (listCode[i].substring(0, 1).equalsIgnoreCase(value))) {
                    spinner.setSelection(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void setSpinnerValueByCode(List<String> listCode, String value, Spinner spinner) {
        if (null != value) {
            for (int i = 0; i < listCode.size(); i++) {
                if ((listCode.get(i).equalsIgnoreCase(value))) {
                    spinner.setSelection(i + 1);
                    break;
                }
            }
        }
    }

    public static String getValueByCodeFromMap(Map<String, String> map, String value) {
        if (null != value) {
            for (Map.Entry<String, String> entry : map.entrySet()
            ) {
                if (entry.getKey().equalsIgnoreCase(value)) {
                    return entry.getValue();
                }
            }
        }
        return "";
    }

    public static String getSpinnerCodeFromArrayList(Spinner spinner, ArrayList<MasterModel> arrList) {
        int spinnerPos = spinner.getSelectedItemPosition();
        String strCode = "";
        try {
            if (arrList.size() > 0) {
                strCode = arrList.get(spinnerPos).getCode();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return strCode;
    }

    public static String getSpinnerValue(Spinner spinner, String[] arrList) {
        int spinnerPos = spinner.getSelectedItemPosition();
        String strCode = "";
        if (arrList.length > 0) {
            strCode = arrList[spinnerPos];
        }
        return strCode;
    }

    public static String checkValueInArrayList(ArrayList<MasterModel> list, String value) {
        String strValue = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).getDescription().equalsIgnoreCase(value))
                        || (list.get(i).getCode().equalsIgnoreCase(value))) {
                    strValue = list.get(i).getDescription();
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return strValue;
    }
}
