package com.mahindra.mitra20.champion.views.adapters.myauctions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListenerDashboard;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.evaluator.helper.ViewHelper;
import com.mahindra.mitra20.interfaces.ContactIn;

import java.util.ArrayList;

public class AssignRequestDashboardAdapter extends RecyclerView.Adapter<AssignRequestDashboardAdapter.ViewHolder> {

    private ArrayList<EnquiryRequest> assignRequestContents;
    private Context context;
    private ItemClickListenerDashboard clickListener;
    private ContactIn contactIn;

    public void updateList(ArrayList<EnquiryRequest> assignRequestContents){
        this.assignRequestContents = assignRequestContents;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView tvName, tvVehicleName, textViewLeadPriority, textViewLocation,
                textViewSalesPersonName, textViewSalesPersonContactNo, textViewSource, textViewType;
        LinearLayout layoutItemAssignRequest;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            tvName = v.findViewById(R.id.tvName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewSource = v.findViewById(R.id.textViewEnqSource);
            textViewType = v.findViewById(R.id.textViewEnqType);
            textViewSalesPersonName = v.findViewById(R.id.textViewSalesPersonName);
            textViewSalesPersonContactNo = v.findViewById(R.id.textViewSalesPersonContactNo);
            tvVehicleName = v.findViewById(R.id.tvBrokerId);
            ImageView imageViewCall = v.findViewById(R.id.imageViewCall);
            ImageView imageViewMessage = v.findViewById(R.id.imageViewMessage);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);
            layoutItemAssignRequest = v.findViewById(R.id.layout_item_assign_request);

            layoutItemAssignRequest.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);
            imageViewMessage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.layout_item_assign_request:
                    if (clickListener != null) {
                        clickListener.onClick(view, getAdapterPosition(), 0);
                    }
                    break;
                case R.id.imageViewCall:
                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onCallPress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
                case R.id.imageViewMessage:
                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onMessagePress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
            }
        }
    }

    public void setClickListener(ItemClickListenerDashboard monClick) {
        this.clickListener = monClick;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AssignRequestDashboardAdapter(Context _context, ArrayList<EnquiryRequest> _assignRequestContents, ContactIn contactIn) {
        assignRequestContents = _assignRequestContents;
        context = _context;
        this.contactIn = contactIn;
    }

    @Override
    public AssignRequestDashboardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_assign_request_sales, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvName.setText(assignRequestContents.get(position).getNameOfEnquiry());
        holder.textViewLocation.setText(assignRequestContents.get(position).getLocation());
        String strMake = assignRequestContents.get(position).getMake();
        String strModel = assignRequestContents.get(position).getModel();
        holder.tvVehicleName.setText(String.format("%s %s", strMake, strModel));
        ViewHelper.populateLeadView(context, holder.textViewLeadPriority,
                assignRequestContents.get(position).getLeadPriority());
        holder.textViewSalesPersonName.setText(assignRequestContents.get(position).getSalesconsultantname());
        holder.textViewSalesPersonContactNo.setText(assignRequestContents.get(position).getSalesconsultantContactnum());
        String source = assignRequestContents.get(position).getEnquirySource();
        holder.textViewSource.setText(String.format("Source: %s", (null != source) ? source : ""));
        String type = assignRequestContents.get(position).getEnquiryType();
        holder.textViewType.setText(String.format("Type:  %s", (null != type) ? type : ""));
    }

    @Override
    public int getItemCount() {
        return assignRequestContents.size();
    }
}