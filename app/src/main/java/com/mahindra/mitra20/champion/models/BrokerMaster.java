package com.mahindra.mitra20.champion.models;

import java.util.List;

public class BrokerMaster {
    private List<BrokerDetails> BrokerDetails;

    private String IsSuccessful;

    public List<BrokerDetails> getBrokerDetails() {
        return BrokerDetails;
    }

    public void setBrokerDetails(List<BrokerDetails> BrokerDetails) {
        this.BrokerDetails = BrokerDetails;
    }

    public String getIsSuccessful() {
        return IsSuccessful;
    }

    public void setIsSuccessful(String IsSuccessful) {
        this.IsSuccessful = IsSuccessful;
    }

}