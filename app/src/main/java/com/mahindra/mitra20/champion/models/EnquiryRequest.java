package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

public class EnquiryRequest implements Parcelable {

    private String contactNo;
    private String dateOfAppointment;
    private String make;
    private String model;
    private String nameOfEnquiry;
    private String requestID;
    private String requestStatus;
    private String location;
    private String PinCode;
    private String evaluationReportID;
    private String evaluatorID;
    private String auctionID;
    private String timeOfAppointment;
    private String yearOfManifacturing;
    private String customerResponse;
    private String appointmentStatus;
    private String customerExpectedPrice;
    private String optedNewVehicle;
    private String salesconsultantname;
    private String salesconsultantContactnum;
    private boolean selected;
    private int leadPriority;
    private String enquiryLocation;
    private String enquirySource;
    private String enquiryType;

    protected EnquiryRequest(Parcel in) {
        contactNo = in.readString();
        dateOfAppointment = in.readString();
        make = in.readString();
        model = in.readString();
        nameOfEnquiry = in.readString();
        requestID = in.readString();
        requestStatus = in.readString();
        location = in.readString();
        PinCode = in.readString();
        evaluationReportID = in.readString();
        evaluatorID = in.readString();
        auctionID = in.readString();
        timeOfAppointment = in.readString();
        yearOfManifacturing = in.readString();
        customerResponse = in.readString();
        appointmentStatus = in.readString();
        customerExpectedPrice = in.readString();
        optedNewVehicle = in.readString();
        salesconsultantname = in.readString();
        salesconsultantContactnum = in.readString();
        selected = in.readByte() != 0;
        leadPriority = in.readInt();
        enquiryLocation = in.readString();
        enquirySource = in.readString();
        enquiryType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contactNo);
        dest.writeString(dateOfAppointment);
        dest.writeString(make);
        dest.writeString(model);
        dest.writeString(nameOfEnquiry);
        dest.writeString(requestID);
        dest.writeString(requestStatus);
        dest.writeString(location);
        dest.writeString(PinCode);
        dest.writeString(evaluationReportID);
        dest.writeString(evaluatorID);
        dest.writeString(auctionID);
        dest.writeString(timeOfAppointment);
        dest.writeString(yearOfManifacturing);
        dest.writeString(customerResponse);
        dest.writeString(appointmentStatus);
        dest.writeString(customerExpectedPrice);
        dest.writeString(optedNewVehicle);
        dest.writeString(salesconsultantname);
        dest.writeString(salesconsultantContactnum);
        dest.writeByte((byte) (selected ? 1 : 0));
        dest.writeInt(leadPriority);
        dest.writeString(enquiryLocation);
        dest.writeString(enquirySource);
        dest.writeString(enquiryType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EnquiryRequest> CREATOR = new Creator<EnquiryRequest>() {
        @Override
        public EnquiryRequest createFromParcel(Parcel in) {
            return new EnquiryRequest(in);
        }

        @Override
        public EnquiryRequest[] newArray(int size) {
            return new EnquiryRequest[size];
        }
    };

    public String getSalesconsultantname() {
        return salesconsultantname;
    }

    public void setSalesconsultantname(String salesconsultantname) {
        this.salesconsultantname = salesconsultantname;
    }

    public String getSalesconsultantContactnum() {
        return salesconsultantContactnum;
    }

    public void setSalesconsultantContactnum(String salesconsultantContactnum) {
        this.salesconsultantContactnum = salesconsultantContactnum;
    }

    public String getOptedNewVehicle() {
        return optedNewVehicle;
    }

    public void setOptedNewVehicle(String optedNewVehicle) {
        this.optedNewVehicle = optedNewVehicle;
    }

    public String getCustomerExpectedPrice() {
        return customerExpectedPrice;
    }

    public void setCustomerExpectedPrice(String customerExpectedPrice) {
        this.customerExpectedPrice = customerExpectedPrice;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getAuctionID() {
        return auctionID;
    }

    public void setAuctionID(String auctionID) {
        this.auctionID = auctionID;
    }

    public String getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(String customerResponse) {
        this.customerResponse = customerResponse;
    }

    public String getEvaluationReportID() {
        return evaluationReportID;
    }

    public void setEvaluationReportID(String evaluationReportID) {
        this.evaluationReportID = evaluationReportID;
    }

    public String getEvaluatorID() {
        return evaluatorID;
    }

    public void setEvaluatorID(String evaluatorID) {
        this.evaluatorID = evaluatorID;
    }

    public String getPinCode() {
        return PinCode;
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getTimeOfAppointment() {
        return timeOfAppointment;
    }

    public void setTimeOfAppointment(String timeOfAppointment) {
        this.timeOfAppointment = timeOfAppointment;
    }

    public String getYearOfManifacturing() {
        return yearOfManifacturing;
    }

    public void setYearOfManifacturing(String yearOfManifacturing) {
        this.yearOfManifacturing = yearOfManifacturing;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public EnquiryRequest() {
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getDateOfAppointment() {
        return dateOfAppointment;
    }

    public void setDateOfAppointment(String dateOfAppointment) {
        this.dateOfAppointment = dateOfAppointment;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNameOfEnquiry() {
        return nameOfEnquiry;
    }

    public void setNameOfEnquiry(String nameOfEnquiry) {
        this.nameOfEnquiry = nameOfEnquiry;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public int getLeadPriority() {
        return leadPriority;
    }

    public void setLeadPriority(int leadPriority) {
        this.leadPriority = leadPriority;
    }

    public String getEnquiryLocation() {
        return enquiryLocation;
    }

    public void setEnquiryLocation(String enquiryLocation) {
        this.enquiryLocation = enquiryLocation;
    }

    public String getEnquirySource() {
        return enquirySource;
    }

    public void setEnquirySource(String enquirySource) {
        this.enquirySource = enquirySource;
    }

    public String getEnquiryType() {
        return enquiryType;
    }

    public void setEnquiryType(String enquiryType) {
        this.enquiryType = enquiryType;
    }
}