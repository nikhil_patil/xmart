package com.mahindra.mitra20.champion.interfaces;

import com.mahindra.mitra20.champion.models.EvaluatorWiseEnquiry;

import java.util.List;

/**
 * Created by PATINIK-CONT on 01-Oct-19.
 */
public interface OnEvaluatorwiseEnquiryListItemClickListener {
    void onAnyCountClick(List<EvaluatorWiseEnquiry> brokerDetails, int code);

    void onCallPress(String number);
}
