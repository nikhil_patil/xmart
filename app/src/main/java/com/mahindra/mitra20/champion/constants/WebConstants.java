package com.mahindra.mitra20.champion.constants;

import com.mahindra.mitra20.constants.GeneralConstants;

/**
 * Created by user on 8/11/2018.
 */

public interface WebConstants {

    String BASE_URL = GeneralConstants.BASE_URL;

    String UserID = "UserID";
    String IsSuccessful = "IsSuccessful";
    String IsSuccessfull = "IsSuccessfull";

    interface EnquiryRequestList {

        int REQUEST_CODE = 1;
        String URL = BASE_URL + "xmart/EnquiryRequestList";
        String USER_ID = "UserID";
        String REQUEST_STATUS = "RequestStatus";
        String EVALUATION_REPORT_ID = "EvaluationReportID";
        String COUNT_TO_DISPLAY_DASHBOARD = "CountToDisplayOnDashboard";
        String PINCODE = "Pincode";
    }

    interface EvaluatorMaster {
        int REQUEST_CODE = 2;
        String URL = BASE_URL + "xmart/evaluatorMaster";
        String ALLOCATED_REQ_COUNT = "ALLOCATED_REQ_COUNT";
        String EVALUATOR_CONTACT_NUMBER = "Evaluator_Contact_Number";
        String EVALUATOR_ID = "Evaluator_ID";
        String EVALUATOR_NAME = "Evaluator_Name";
    }

    interface AssignRequestToEvaluator {
        int REQUEST_CODE = 3;
        String URL = BASE_URL + "xmart/AssignRequestToEvaluator";
        String AssignRequestDetails = "AssignRequestDetails";
        String UserID = "UserID";
        String RequestID = "RequestID";
        String ScheduledStartDateTime = "ScheduledStartDateTime";
        String EvaluatorID = "EvaluatorID";
    }

    interface LiveAuctionMaster {
        int REQUEST_CODE = 4;
        int REQUEST_CODE_LIVE = 44;
        int REQUEST_CODE_UPCOMING = 45;
        int REQUEST_CODE_COMPLETED = 46;
        String URL = BASE_URL + "xmart/LiveAuctionMaster";
        String FromDate = "FromDate";
        String toDate = "toDate";
        String AuctionStatus = "AuctionStatus";
        String Make = "Make";
        String Model = "Model";
        String countToDisplay = "countToDisplay";
        String Sorting = "Sorting";
        String UserID  = "UserID";
    }

    interface LiveAuctionMasterByID {
        int REQUEST_CODE = 9;
        String URL = BASE_URL + "xmart/LiveAuctionMasterByID";
        String UserID  = "UserID";
        String AuctionID  = "AuctionID";
    }

    interface PlaceBid {
        int REQUEST_CODE = 5;
        String URL = BASE_URL + "xmart/PlaceBid";
        String UserID = "UserID";
        String AuctionID = "AuctionID";
        String BIDAmount = "BIDAmount";
    }

    interface SendToAuction {
        int REQUEST_CODE = 6;
        String URL = BASE_URL + "xmart/SendToAuction";
        String UserID = "UserID";
        String RequestID = "RequestID";
        String auctionStartDateTime = "auctionStartDateTime";
        String auctionEndDateTime = "auctionEndDateTime";
        String reservedPrice = "reservedPrice";
        String resendAuctionFlag = "ResendAuctionFlag";
    }

    interface SetOfferprice {
        int REQUEST_CODE = 7;
        String URL = BASE_URL + "xmart/SetOfferprice";
        String UserID = "UserID";
        String RequestID = "RequestID";
        String CustomerResponse = "CustomerResponse";
        String OfferPrice = "OfferPrice";
        String CustomerExpectedPrice = "CustomerExpectedPrice";
        String HighestBidderResponse = "HighestBidderResponse";
        String ProcuredBy = "ProcuredBy";
    }

    interface SubmitProcurementPrice {
        int REQUEST_CODE = 7;
        String URL = BASE_URL + "xmart/SubmitProcurementPrice";
        String UserID = "UserID";
        String AuctionID = "AuctionID";
        String CustomerResponse = "CustomerResponse";
        String ProcurementPrice = "ProcurementPrice";
        String ReasonForLowerPriceThanHighest = "ReasonForLowerPriceThanHighest";
        String ModeOfSale = "ModeofSale";
    }

    interface GetEvaluationReport {
        int REQUEST_CODE = 8;
        String URL = BASE_URL + "xmart/EvaluationReportMasterByID";
        String EvaluationReportID = "EvaluationReportID";
        String RequestID = "RequestID";
        String UserID = "UserID";
    }

    interface CancelAppointmentApproval {
        int REQUEST_CODE = 9;
        String URL = BASE_URL + "xmart/CancelAppointmentApproval";
        String RequestID = "RequestID";
        String EvaluatorID = "EvaluatorID";
        String ApprovalStatus = "approvalStatus";
    }

    interface SubmitOffloadDetails {
        int REQUEST_CODE = 10;
        String URL = BASE_URL + "xmart/UpdateOffLoadDetails";
    }

    interface BrokersMaster {
        int REQUEST_CODE = 12;
        String URL = BASE_URL + "xmart/fetchBrokerDetails";
        String UserID  = "UserID";
        String RequestCount = "RequestCount";
    }
}