package com.mahindra.mitra20.champion.views.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.BrokerMaster;
import com.mahindra.mitra20.champion.presenter.BrokersListIn;
import com.mahindra.mitra20.champion.presenter.BrokersListPresenter;
import com.mahindra.mitra20.champion.views.adapters.BrokersListRVAdapter;
import com.mahindra.mitra20.databinding.ActivityBrokersListBinding;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.helper.MyLeadsItemDecoration;

public class BrokersListActivity extends AppCompatActivity implements BrokersListIn,
        SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    ActivityBrokersListBinding binding;
    private BrokersListRVAdapter brokersListRVAdapter;
    private BrokersListPresenter brokersListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_brokers_list);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Select Broker for Procurement");
        brokersListPresenter = new BrokersListPresenter(this);

        binding.recyclerViewBrokers.setEmptyView(findViewById(R.id.textViewEmpty));
        MyLeadsItemDecoration dividerItemDecoration = new MyLeadsItemDecoration(
                ScreenDensityHelper.dpToPx(this, 15),
                ScreenDensityHelper.dpToPx(this, 10),
                ScreenDensityHelper.dpToPx(this, 10));
        binding.recyclerViewBrokers.addItemDecoration(dividerItemDecoration);
        binding.swipeContainer.setOnRefreshListener(this);

        onRefresh();
        setupSearchView();
    }

    private void setupSearchView() {
        binding.searchViewFilter.setIconifiedByDefault(false);
        binding.searchViewFilter.setOnQueryTextListener(this);
        binding.searchViewFilter.setSubmitButtonEnabled(false);
        binding.searchViewFilter.setFocusable(false);
        binding.searchViewFilter.setQueryHint("Search here by name...");
    }

    @Override
    public void onDownloadSuccess(int requestId, BrokerMaster brokerMaster) {
        if (requestId == WebConstants.BrokersMaster.REQUEST_CODE) {
            if (brokerMaster != null && !brokerMaster.getBrokerDetails().isEmpty()) {
                brokersListRVAdapter = new BrokersListRVAdapter(this, brokerMaster.getBrokerDetails());
                binding.recyclerViewBrokers.setAdapter(brokersListRVAdapter);
            }
        }
        binding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        CommonHelper.toast(message, this);
    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            brokersListPresenter.downloadBrokersMaster();
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (brokersListRVAdapter != null)
            brokersListRVAdapter.filter(newText);
        return false;
    }
}