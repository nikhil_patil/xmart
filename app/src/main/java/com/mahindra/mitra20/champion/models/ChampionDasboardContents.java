package com.mahindra.mitra20.champion.models;

/**
 * Created by BADHAP-CONT on 7/19/2018.
 */

public class ChampionDasboardContents {

    public ChampionDasboardContents(String strTitle, String strCount) {
        this.strTitle = strTitle;
        this.strCount = strCount;
    }

    String strTitle;
    String strCount;

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrCount() {
        return strCount;
    }

    public void setStrCount(String strCount) {
        this.strCount = strCount;
    }
}
