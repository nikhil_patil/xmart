package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 8/4/2018.
 */

public class EvaluatorMaster implements Parcelable {

    String AllocatedReqCount, EvaluatorContactNo, EvaluatorID, EvaluatorName;

    public String getAllocatedReqCount() {
        return AllocatedReqCount;
    }

    public void setAllocatedReqCount(String allocatedReqCount) {
        AllocatedReqCount = allocatedReqCount;
    }

    public String getEvaluatorContactNo() {
        return EvaluatorContactNo;
    }

    public void setEvaluatorContactNo(String evaluatorContactNo) {
        EvaluatorContactNo = evaluatorContactNo;
    }

    public String getEvaluatorID() {
        return EvaluatorID;
    }

    public void setEvaluatorID(String evaluatorID) {
        EvaluatorID = evaluatorID;
    }

    public String getEvaluatorName() {
        return EvaluatorName;
    }

    public void setEvaluatorName(String evaluatorName) {
        EvaluatorName = evaluatorName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.AllocatedReqCount);
        dest.writeString(this.EvaluatorContactNo);
        dest.writeString(this.EvaluatorID);
        dest.writeString(this.EvaluatorName);
    }

    public EvaluatorMaster() {
    }

    protected EvaluatorMaster(Parcel in) {
        this.AllocatedReqCount = in.readString();
        this.EvaluatorContactNo = in.readString();
        this.EvaluatorID = in.readString();
        this.EvaluatorName = in.readString();
    }

    public static final Parcelable.Creator<EvaluatorMaster> CREATOR = new Parcelable.Creator<EvaluatorMaster>() {
        @Override
        public EvaluatorMaster createFromParcel(Parcel source) {
            return new EvaluatorMaster(source);
        }

        @Override
        public EvaluatorMaster[] newArray(int size) {
            return new EvaluatorMaster[size];
        }
    };
}
