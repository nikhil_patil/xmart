package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

public class EvaluatorWiseEnquiry implements Parcelable {
    private String EnquiryDate;
    private String Variant;
    private String ContactNo;
    private String Model;
    private String TimeOfAppointment;
    private String EnquiryID;
    private String Make;
    private String DateOfAppointment;
    private String NameOfEnquiry;
    private String EmailAddress;
    private String AuctionDate;
    private String AuctionID;
    private String City;
    private String CustomerExpectedPrice;
    private String EvaluationDate;
    private String EvaluationID;
    private String EvaluationReportID;
    private String EvaluatorID;
    private String FinalProcurementPrice;
    private String OptedNewVehicle;
    private String State;
    private String EnquirySource;
    private String EnquiryType;

    public EvaluatorWiseEnquiry() {

    }

    protected EvaluatorWiseEnquiry(Parcel in) {
        EnquiryDate = in.readString();
        Variant = in.readString();
        ContactNo = in.readString();
        Model = in.readString();
        TimeOfAppointment = in.readString();
        EnquiryID = in.readString();
        Make = in.readString();
        DateOfAppointment = in.readString();
        NameOfEnquiry = in.readString();
        EmailAddress = in.readString();
        AuctionDate = in.readString();
        AuctionID = in.readString();
        City = in.readString();
        CustomerExpectedPrice = in.readString();
        EvaluationDate = in.readString();
        EvaluationID = in.readString();
        EvaluationReportID = in.readString();
        EvaluatorID = in.readString();
        FinalProcurementPrice = in.readString();
        OptedNewVehicle = in.readString();
        State = in.readString();
        EnquirySource = in.readString();
        EnquiryType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(EnquiryDate);
        dest.writeString(Variant);
        dest.writeString(ContactNo);
        dest.writeString(Model);
        dest.writeString(TimeOfAppointment);
        dest.writeString(EnquiryID);
        dest.writeString(Make);
        dest.writeString(DateOfAppointment);
        dest.writeString(NameOfEnquiry);
        dest.writeString(EmailAddress);
        dest.writeString(AuctionDate);
        dest.writeString(AuctionID);
        dest.writeString(City);
        dest.writeString(CustomerExpectedPrice);
        dest.writeString(EvaluationDate);
        dest.writeString(EvaluationID);
        dest.writeString(EvaluationReportID);
        dest.writeString(EvaluatorID);
        dest.writeString(FinalProcurementPrice);
        dest.writeString(OptedNewVehicle);
        dest.writeString(State);
        dest.writeString(EnquirySource);
        dest.writeString(EnquiryType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EvaluatorWiseEnquiry> CREATOR = new Creator<EvaluatorWiseEnquiry>() {
        @Override
        public EvaluatorWiseEnquiry createFromParcel(Parcel in) {
            return new EvaluatorWiseEnquiry(in);
        }

        @Override
        public EvaluatorWiseEnquiry[] newArray(int size) {
            return new EvaluatorWiseEnquiry[size];
        }
    };

    public String getEnquiryDate() {
        return EnquiryDate;
    }

    public void setEnquiryDate(String EnquiryDate) {
        this.EnquiryDate = EnquiryDate;
    }

    public String getVariant() {
        return Variant;
    }

    public void setVariant(String Variant) {
        this.Variant = Variant;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getTimeOfAppointment() {
        return TimeOfAppointment;
    }

    public void setTimeOfAppointment(String TimeOfAppointment) {
        this.TimeOfAppointment = TimeOfAppointment;
    }

    public String getEnquiryID() {
        return EnquiryID;
    }

    public void setEnquiryID(String EnquiryID) {
        this.EnquiryID = EnquiryID;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public String getDateOfAppointment() {
        return DateOfAppointment;
    }

    public void setDateOfAppointment(String DateOfAppointment) {
        this.DateOfAppointment = DateOfAppointment;
    }

    public String getNameOfEnquiry() {
        return NameOfEnquiry;
    }

    public void setNameOfEnquiry(String NameOfEnquiry) {
        this.NameOfEnquiry = NameOfEnquiry;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String EmailAddress) {
        this.EmailAddress = EmailAddress;
    }

    public String getAuctionDate() {
        return AuctionDate;
    }

    public void setAuctionDate(String auctionDate) {
        AuctionDate = auctionDate;
    }

    public String getAuctionID() {
        return AuctionID;
    }

    public void setAuctionID(String auctionID) {
        AuctionID = auctionID;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCustomerExpectedPrice() {
        return CustomerExpectedPrice;
    }

    public void setCustomerExpectedPrice(String customerExpectedPrice) {
        CustomerExpectedPrice = customerExpectedPrice;
    }

    public String getEvaluationDate() {
        return EvaluationDate;
    }

    public void setEvaluationDate(String evaluationDate) {
        EvaluationDate = evaluationDate;
    }

    public String getEvaluationReportID() {
        return EvaluationReportID;
    }

    public void setEvaluationReportID(String evaluationReportID) {
        EvaluationReportID = evaluationReportID;
    }

    public String getEvaluatorID() {
        return EvaluatorID;
    }

    public void setEvaluatorID(String evaluatorID) {
        EvaluatorID = evaluatorID;
    }

    public String getFinalProcurementPrice() {
        return FinalProcurementPrice;
    }

    public void setFinalProcurementPrice(String finalProcurementPrice) {
        FinalProcurementPrice = finalProcurementPrice;
    }

    public String getOptedNewVehicle() {
        return OptedNewVehicle;
    }

    public void setOptedNewVehicle(String optedNewVehicle) {
        OptedNewVehicle = optedNewVehicle;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getEvaluationID() {
        return EvaluationID;
    }

    public void setEvaluationID(String evaluationID) {
        EvaluationID = evaluationID;
    }

    public String getEnquirySource() {
        return EnquirySource;
    }

    public void setEnquirySource(String enquirySource) {
        EnquirySource = enquirySource;
    }

    public String getEnquiryType() {
        return EnquiryType;
    }

    public void setEnquiryType(String enquiryType) {
        EnquiryType = enquiryType;
    }
}