package com.mahindra.mitra20.champion.models.comparator;

import com.mahindra.mitra20.champion.models.AuctionLIST;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by user on 9/15/2018.
 */

public class BidDateTimeComparator implements Comparator<AuctionLIST> {

    @Override
    public int compare(AuctionLIST auctionLIST, AuctionLIST t1) {

        String sDate1 = auctionLIST.getBidingDateTime();
        String sDate2 = t1.getBidingDateTime();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date date1 = null;
        try {
            date1 = format.parse(sDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = null;
        try {
            date2 = format.parse(sDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date1.compareTo(date2);
    }
}