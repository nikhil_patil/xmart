package com.mahindra.mitra20.champion.views.adapters.myauctions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.ContactIn;

import java.util.ArrayList;


/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class CompletedAuctionsAdapter extends RecyclerView.Adapter<CompletedAuctionsAdapter.ViewHolder> {

    private ArrayList<AuctionDetail> myAuctionsContents;
    private Context context;
    private ItemClickListener clickListener;
    private ContactIn contactIn;

    public void setContactIn(ContactIn contactIn) {
        this.contactIn = contactIn;
    }

    public void updateList(ArrayList<AuctionDetail> myAuctionsContents){
        this.myAuctionsContents = myAuctionsContents;
    }

    @Override
    public CompletedAuctionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_my_completed_auctions, parent, false);
        return new ViewHolder(v);
    }

    public void setmonClick(ItemClickListener monClick) {
        this.clickListener = monClick;
    }

    public CompletedAuctionsAdapter(Context _context, ArrayList<AuctionDetail> _myAuctionsContents) {
        myAuctionsContents = _myAuctionsContents;
        context = _context;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (myAuctionsContents.get(position).getHighestBidder().equalsIgnoreCase(ChampionConstants.Y)) {
            holder.textViewWinStatus.setText("You Won");
            holder.tableRowResult.setBackground(context.getDrawable(R.drawable.green_bg_right_corner_rounded));
            holder.arrow.setImageDrawable(context.getDrawable(R.mipmap.ic_arrow_up));
        } else {
            holder.textViewWinStatus.setText("You Lost");
            holder.tableRowResult.setBackground(context.getDrawable(R.drawable.orange_bg_right_corner_rounded));
            holder.arrow.setImageDrawable(context.getDrawable(R.mipmap.ic_arrow_down));
        }

        holder.textViewHighestBid.setText(String.format("Rs. %s", myAuctionsContents.get(position).getHighestPrice()));
        holder.textViewXmartPrice.setText(String.format("Rs. %s", myAuctionsContents.get(position).getMITRAPriceBasePrice()));
        holder.textViewCustomerName.setText(myAuctionsContents.get(position).getVehOwnerName());
        holder.textViewVehicleName.setText(myAuctionsContents.get(position).getVehicleName());
        holder.textViewVehicleUsage.setText(String.format("Vehicle Usage - %s",
                myAuctionsContents.get(position).getVehicleUsage()));

        if (null != myAuctionsContents.get(position).getVehiclePicURL()) {
            if (!myAuctionsContents.get(position).getVehiclePicURL().isEmpty()) {
                Glide.with(holder.imageViewVehicle.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(myAuctionsContents.get(position).getVehiclePicURL()))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.imageViewVehicle);
            } else {
                Glide.with(holder.imageViewVehicle.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.imageViewVehicle);
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTime, textViewHighestBid, textViewXmartPrice, textViewCustomerName,
                textViewVehicleName, textViewWinStatus, textViewVehicleUsage;
        TableRow tableRowResult;
        ImageView imageViewCall, imageViewDetails, arrow, imageViewVehicle;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewHighestBid = v.findViewById(R.id.textViewHighestBid);
            textViewXmartPrice = v.findViewById(R.id.textViewXmartPrice);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewWinStatus = v.findViewById(R.id.textViewWinStatus);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewDetails = v.findViewById(R.id.imageViewDetails);
            arrow = v.findViewById(R.id.arrow);
            imageViewVehicle = v.findViewById(R.id.imageViewVehicle);
            tableRowResult = v.findViewById(R.id.tableRowResult);
            textViewVehicleUsage = v.findViewById(R.id.textViewVehicleUsage);

            imageViewCall.setOnClickListener(this);
            imageViewDetails.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imageViewDetails:
                    if (clickListener != null) {
                        clickListener.onClick(view, getAdapterPosition());
                    }
                    break;
                case R.id.imageViewCall:
                    contactIn.onCallPress(myAuctionsContents.get(getAdapterPosition()).getContactNo());
                    break;
                case R.id.imageViewMessage:
                    contactIn.onMessagePress(myAuctionsContents.get(getAdapterPosition()).getContactNo());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return myAuctionsContents.size();
    }
}