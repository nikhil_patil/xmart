package com.mahindra.mitra20.champion.views.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.Activity_Profile_Pic;
import com.mahindra.mitra20.BuildConfig;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.customeWidgets.ItemDecorationAlbumColumns;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.helper.LoginHelper;
import com.mahindra.mitra20.champion.helper.UpdateAppHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.interfaces.ItemClickListenerDashboard;
import com.mahindra.mitra20.champion.models.ChampionDasboardContents;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AssignRequestListIn;
import com.mahindra.mitra20.champion.presenter.AssignRequestListMasterPresenter;
import com.mahindra.mitra20.champion.views.adapters.CustomGridAdapter;
import com.mahindra.mitra20.champion.views.adapters.myauctions.AssignRequestDashboardAdapter;
import com.mahindra.mitra20.constants.GeneralConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.views.activity.EvaluationCompletedDraftActivity;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.evaluator.views.activity.NotificationsActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.MasterSyncHelper;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.sqlite.BrokerDbHelper;
import com.mahindra.mitra20.sqlite.ChampionHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.utils.ForceUpdateChecker;
import com.mahindra.mitra20.utils.LogUtil;
import com.mahindra.mitra20.utils.NetworkUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.mahindra.mitra20.champion.constants.ChampionConstants.ASSIGN_REQUEST_MODEL_KEY;
import static com.mahindra.mitra20.champion.constants.ChampionConstants.DASHBOARD_DATA_LIMIT;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.DATABASE_NAME;

public class ChampionDashboardActivity extends AppCompatActivity
        implements ItemClickListener, View.OnClickListener, AssignRequestListIn,
        SwipeRefreshLayout.OnRefreshListener, ContactIn, ItemClickListenerDashboard,
        ForceUpdateChecker.OnUpdateNeededListener {

    private ArrayList<ChampionDasboardContents> championDashboardContents = new ArrayList<>();
    private ChampionHelper championHelper;
    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private TextView textViewDraftCount, textViewCompletedScheduledCount,
            textViewOverdueScheduledCount, textViewUpcomingScheduledCount,
            textViewTodaysScheduledCount, textViewCancellationCount, textViewUPCOMINGCount,
            textViewLIVECount, textViewCOMPLETEDCount;
    private DrawerLayout drawer;
    private ImageView ivProfile;
    private SwipeRefreshLayout swipeContainer;
    private AssignRequestListMasterPresenter assignRequestListPresenter;
    private AssignRequestDashboardAdapter assignRequestAdapter;
    private CustomGridAdapter customGridAdapter;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private Context context;
    private ProgressDialog dialog;
    private boolean isEnquiryAPICalled = false;

    private static final int PERMISSION_REQUEST_CODE = 200;

    private ProgressBar progressProfileImage, progressProfileNavigationImage;
    private ImageView imageViewProfilePic;
    private SharedPreferences sharedPref;
    private static final String TAG = "ChampionDashboardActivity";
    private String DirectoryName = Environment.getExternalStorageDirectory() + "/X-Mart-DB";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_champion_dashboard);
        LogUtil.getInstance().logE(TAG, "onCreate()");
        initUI();
        displayUnreadNotificationIcon();

        sharedPref = getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
        boolean isLoginHelpDisplayed = sharedPref.getBoolean(
                SharedPreferenceKeys.CHAMPION_DASHBOARD_HELP_DISPLAYED, false);
        if (!isLoginHelpDisplayed) {
            showHelp();
        }

        if (BuildConfig.DEBUG)
            exportDB();

        if (NetworkUtilities.isNetworkConnected(this))
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
    }

    private void autoDownloadOpenEnquiries() {
        LogUtil.getInstance().logE(TAG, "autoDownloadOpenEnquiries()");
        try {
            LogUtil.getInstance().logE("autoDownloadOpenEnquiries",
                    "autoDownloadOpenEnquiries()->isMasterSync = " + masterDatabaseHelper.isMasterSync());
            if (CommonHelper.isConnectingToInternet(getBaseContext()) && masterDatabaseHelper.isMasterSync()) {
                LogUtil.getInstance().logE("autoDownloadOpenEnquiries", "autoRefreshDownloadMasters()");
                assignRequestListPresenter.downloadEnquiryMaster(
                        ChampionConstants.OPEN_STATUS, 13);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.getInstance().logE(TAG, "onResume()");
        try {
            LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                    "onResume()->NOTIFICATION_RECEIVED FLAG = " +
                            sharedPref.getBoolean(SharedPreferenceKeys.NOTIFICATION_RECEIVED, false));
            if (sharedPref.getBoolean(SharedPreferenceKeys.NOTIFICATION_RECEIVED, false)) {
                LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                        "onResume()->autoRefreshDownloadMasters()");
                autoRefreshDownloadMasters();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void autoRefreshDownloadMasters() {
        LogUtil.getInstance().logE(TAG, "autoRefreshDownloadMasters()");
        LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                "autoRefreshDownloadMasters()->isMasterSync = " + masterDatabaseHelper.isMasterSync());
        if (CommonHelper.isConnectingToInternet(getBaseContext()) && masterDatabaseHelper.isMasterSync()) {
            LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                    "autoRefreshDownloadMasters()->downloadEnquiryMaster(OPEN_STATUS,REQUEST_ID=1)");
//            showProgressDialog("Downloading dashboard data... Please wait...");
            //ADDED BY PRAVIN DHARAM ON 26-11-2019
            if (!isEnquiryAPICalled) {
                isEnquiryAPICalled = true;
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.OPEN_STATUS, 1);
            }
        }
    }

    @Override
    protected void onDestroy() {
        LogUtil.getInstance().logE(TAG, "onDestroy()");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private void showHelp() {
        LogUtil.getInstance().logE(TAG, "showHelp()");
        try {
            openDrawer();
            findViewById(R.id.cl_overlay_sync).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_sync_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeDrawer();
                    findViewById(R.id.cl_overlay_sync).setVisibility(View.GONE);
                    findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.VISIBLE);

                    findViewById(R.id.tv_pull_to_refresh_next).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.GONE);
                            findViewById(R.id.cl_overlay_notification).setVisibility(View.VISIBLE);

                            findViewById(R.id.tv_notification_done).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    findViewById(R.id.cl_overlay_notification).setVisibility(View.GONE);

                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putBoolean(
                                            SharedPreferenceKeys.CHAMPION_DASHBOARD_HELP_DISPLAYED, true);
                                    editor.apply();
                                }
                            });
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayUnreadNotificationIcon() {
        LogUtil.getInstance().logE(TAG, "displayUnreadNotificationIcon()");
        BrokerDbHelper brokerDbHelper = new BrokerDbHelper(this);
        if (brokerDbHelper.checkUnreadNotifications()) {
            findViewById(R.id.iv_notification_available).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.iv_notification_available).setVisibility(View.GONE);
        }
    }

    private void openDrawer() {

        if (null != drawer && !drawer.isDrawerOpen(Gravity.START)) {
            drawer.openDrawer(Gravity.START);
        }
    }

    private void closeDrawer() {
        if (null != drawer && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void initUI() {
        LogUtil.getInstance().logE(TAG, "initUI()");
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE,
                    READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);
        }

        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                    new IntentFilter(GeneralConstants.BROADCAST_RECEIVER_KEY));

        } catch (Exception e) {
            e.printStackTrace();
        }

        evaluatorHelper = new EvaluatorHelper(this.getBaseContext());
        masterDatabaseHelper = new MasterDatabaseHelper(this.getBaseContext());
        this.context = getApplicationContext();

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        assignRequestListPresenter = new AssignRequestListMasterPresenter(this, this);

        ivProfile = findViewById(R.id.iv_profile);
        progressProfileNavigationImage = findViewById(R.id.progressProfileNavigationImage);
        ImageView imageViewNotification = findViewById(R.id.imageViewNotification);
        imageViewNotification.setOnClickListener(this);

        drawer = findViewById(R.id.drawer_layout);
        ImageView imageViewMenu = findViewById(R.id.imageViewMenu);
        ImageView imageViewNewEvaluation = findViewById(R.id.imageViewNewEvaluation);
        imageViewMenu.setOnClickListener(this);
        imageViewNewEvaluation.setOnClickListener(this);

        TableRow tableRowRequests = findViewById(R.id.tableRowRequests);
        TableRow tableRowAuctions = findViewById(R.id.tableRowAuctions);
        TableRow tableRowReservePrice = findViewById(R.id.tableRowReservePrice);
        TableRow tableRowOfferPrice = findViewById(R.id.tableRowOfferPrice);
        TableRow tableRowFollowupCustomer = findViewById(R.id.tableRowFollowupCustomer);
        TableRow tableRowSyncMaster = findViewById(R.id.tableRowSyncMaster);
        TextView textViewLogout = findViewById(R.id.textViewLogout);
        textViewCancellationCount = findViewById(R.id.textViewCancellationCount);

        TextView profile_name_tv = findViewById(R.id.profile_name_tv);
        TextView profile_team_tv = findViewById(R.id.profile_team_tv);

        textViewUPCOMINGCount = findViewById(R.id.textViewUPCOMING);
        textViewLIVECount = findViewById(R.id.textViewLIVE);
        textViewCOMPLETEDCount = findViewById(R.id.textViewCOMP);

        textViewDraftCount = findViewById(R.id.textViewDraftCount);
        textViewCompletedScheduledCount = findViewById(R.id.tvCount);
        textViewOverdueScheduledCount = findViewById(R.id.textViewOverdueScheduledCount);
        textViewUpcomingScheduledCount = findViewById(R.id.textViewUpcomingScheduledCount);
        textViewTodaysScheduledCount = findViewById(R.id.textViewTodaysScheduledCount);

        LinearLayout linearLayoutScheduleForTheDay = findViewById(R.id.linearLayoutScheduleForTheDay);
        LinearLayout linearLayoutCancellationRequest = findViewById(R.id.linearLayoutCancellationRequest);
        LinearLayout linearLayoutUpcoming = findViewById(R.id.linearLayoutUpcoming);
        LinearLayout linearLayoutOverdue = findViewById(R.id.linearLayoutOverdue);
        TableRow tableRowCompletedEvaluation = findViewById(R.id.tableRowCompltetedEvaluation);
        TableRow tableRowDraftEvaluation = findViewById(R.id.tableRowDraftEvaluation);
        TableRow tableRowEvaluatorwiseStatus = findViewById(R.id.tableRowEvaluatorwiseStatus);

        TextView textUserName = findViewById(R.id.textUserName);
        TextView textDealerName = findViewById(R.id.textDealerName);

        TextView textView_ViewAll_Enquiry = findViewById(R.id.textView_ViewAll_Enquiry);
        LinearLayout linearLayoutLiveAuction = findViewById(R.id.linearLayoutLiveAuction);
        LinearLayout linearLayoutCompletedAuction = findViewById(R.id.linearLayoutCompletedAuction);
        LinearLayout linearLayoutUpcomingAuction = findViewById(R.id.linearLayoutUpcomingAuction);

        championHelper = new ChampionHelper(getBaseContext());

        RecyclerView recyclerViewChampionDashboard = findViewById(R.id.recyclerViewChampionDashboard);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerViewChampionDashboard.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewChampionDashboard.addItemDecoration(new ItemDecorationAlbumColumns(getResources().getDimensionPixelSize(R.dimen.one_sp), 2));
        customGridAdapter = new CustomGridAdapter(getBaseContext(), championDashboardContents);
        customGridAdapter.setmonClick(this);
        recyclerViewChampionDashboard.setAdapter(customGridAdapter);

        textUserName.setText(SessionUserDetails.getInstance().getUserName());
        textDealerName.setText(SessionUserDetails.getInstance().getDealerName());

        profile_name_tv.setText(SessionUserDetails.getInstance().getUserName());
        profile_team_tv.setText(SessionUserDetails.getInstance().getDealerName());

        enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(
                ChampionConstants.DASHBOARD_DATA_LIMIT, ChampionConstants.OPEN_STATUS);
        RecyclerView recyclerViewEnquiryList = findViewById(R.id.recyclerViewEnquiryList);
        assignRequestAdapter = new AssignRequestDashboardAdapter(getApplicationContext(),
                enquiryRequestArrayList, this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerViewEnquiryList.setLayoutManager(mLayoutManager);
        recyclerViewEnquiryList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewEnquiryList.setAdapter(assignRequestAdapter);

        assignRequestAdapter.setClickListener(this);
        assignRequestAdapter.notifyDataSetChanged();

        textView_ViewAll_Enquiry.setOnClickListener(this);
        linearLayoutLiveAuction.setOnClickListener(this);
        linearLayoutCompletedAuction.setOnClickListener(this);
        linearLayoutUpcomingAuction.setOnClickListener(this);
        linearLayoutScheduleForTheDay.setOnClickListener(this);
        linearLayoutCancellationRequest.setOnClickListener(this);
        linearLayoutUpcoming.setOnClickListener(this);
        linearLayoutOverdue.setOnClickListener(this);
        tableRowCompletedEvaluation.setOnClickListener(this);
        tableRowDraftEvaluation.setOnClickListener(this);
        tableRowEvaluatorwiseStatus.setOnClickListener(this);

        tableRowRequests.setOnClickListener(this);
        tableRowAuctions.setOnClickListener(this);
        tableRowReservePrice.setOnClickListener(this);
        tableRowOfferPrice.setOnClickListener(this);
        tableRowFollowupCustomer.setOnClickListener(this);
        tableRowSyncMaster.setOnClickListener(this);
        textViewLogout.setOnClickListener(this);

        LogUtil.getInstance().logE("Champ-DownloadSuccess", "initUI()->setDashboardUpdatedCount()");
        setDashboardUpdatedCount();

        progressProfileImage = findViewById(R.id.progressProfileImage);
        imageViewProfilePic = findViewById(R.id.imageViewProfilePic);
        imageViewProfilePic.setOnClickListener(this);

        //Show Profile Pic
        String strProfilePic = SessionUserDetails.getInstance().getUserProfilePic();
        if (null != strProfilePic) {
            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(
                            SessionUserDetails.getInstance().getUserProfilePic()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            imageViewProfilePic.setImageDrawable(
                                    getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(imageViewProfilePic);

            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(
                            SessionUserDetails.getInstance().getUserProfilePic()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            progressProfileNavigationImage.setVisibility(View.GONE);
                            ivProfile.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            progressProfileNavigationImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(ivProfile);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.getInstance().logE(TAG, "onNewIntent()");
        try {
            LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                    "onNewIntent()->autoRefreshDownloadMasters()");
            //ADDED BY PRAVIN DHARAM ON 26-1-2019
            if (intent.getBooleanExtra(ChampionConstants.FROM_NEW_EVALUTION_SCREEN, false)) {
                if (intent.getBooleanExtra(ChampionConstants.IS_DRAFT_SAVED, false))
                    setDashboardUpdatedCount();
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_APPOINTMENT_DETAILS_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_SCHEDULE_APPOINTMENT_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_LOGIN_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_DRAFT_LIST_ACTIVITY, false)) {
                setDashboardUpdatedCount();
            } else
                autoRefreshDownloadMasters();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtil.getInstance().logE(TAG, "BroadcastReceiver()");
            try {
                LogUtil.getInstance().logE("autoRefreshDownloadMasters",
                        "BroadcastReceiver()->autoRefreshDownloadMasters()");
                autoRefreshDownloadMasters();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setDashboardUpdatedCount() {
        LogUtil.getInstance().logE(TAG, "setDashboardUpdatedCount()");

        championDashboardContents.clear();

        ArrayList<String> evaluationReportIDsOFFR = championHelper.getEvaluationReportIDsOFFR();

        HashMap<String, Integer> hashMapDashboardCount = championHelper.getDashboardCount(
                evaluationReportIDsOFFR);

        LogUtil.getInstance().logE("Champ-DownloadSuccess",
                "setDashboardUpdatedCount() DashboardCount = " + hashMapDashboardCount.toString());

        championDashboardContents.add(new ChampionDasboardContents(
                getResources().getString(R.string.assign_open_enquiries),
                String.valueOf(hashMapDashboardCount.get("AssignOpen"))));

        championDashboardContents.add(new ChampionDasboardContents(
                getResources().getString(R.string.enter_reserve_price),
                String.valueOf(hashMapDashboardCount.get("XMartPrice"))));

        championDashboardContents.add(new ChampionDasboardContents(
                getResources().getString(R.string.enter_offer_price),
                String.valueOf(hashMapDashboardCount.get("ProcurementDetails"))));

        championDashboardContents.add(new ChampionDasboardContents(
                getResources().getString(R.string.follow_up_with_customers),
                String.valueOf(hashMapDashboardCount.get("OfferCustomer"))));

        textViewCancellationCount.setText(
                String.valueOf(hashMapDashboardCount.get("CancellationDetails")));

        textViewLIVECount.setText(String.valueOf(hashMapDashboardCount.get("LIVE")));
        textViewCOMPLETEDCount.setText(String.valueOf(hashMapDashboardCount.get("COMPLETED")));
        textViewUPCOMINGCount.setText(String.valueOf(hashMapDashboardCount.get("BIDDING")));

        customGridAdapter.notifyDataSetChanged();

        int scheduledAppoinmentCount = evaluatorHelper.getScheduledAppointmentCount(
                EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        textViewTodaysScheduledCount.setText(String.valueOf(scheduledAppoinmentCount));

        int overdueAppoinmentCount = evaluatorHelper.getOverdueAppointmentCount();
        textViewOverdueScheduledCount.setText(String.valueOf(overdueAppoinmentCount));

        int upcomingAppointmentCount = evaluatorHelper.getUpcomingAppointmentCount();
        textViewUpcomingScheduledCount.setText(String.valueOf(upcomingAppointmentCount));

        int draftEvaluationsCount = evaluatorHelper.getCountOfEvaluations("Draft");
        textViewDraftCount.setText(String.valueOf(draftEvaluationsCount));

        int completedEvaluationsCount = evaluatorHelper.getCountOfEvaluations("Completed");
        textViewCompletedScheduledCount.setText(String.valueOf(completedEvaluationsCount));
    }

    private boolean checkPermission() {
        LogUtil.getInstance().logE(TAG, "checkPermission()");
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onBackPressed() {
        LogUtil.getInstance().logE(TAG, "onBackPressed()");
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //System.exit(0);
                            try {
                                GeneralConstants.DATA_FETCHED = "false";
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    }).setNegativeButton("No", null).show();
        }
    }

    @Override
    public void onClick(View view, int position) {
        LogUtil.getInstance().logE(TAG, "onClick(View view, int position) position = " + position);
        if (masterDatabaseHelper.isMasterSync()) {
            switch (position) {
                case 0:
                    Intent intentAssignReqList = new Intent(
                            ChampionDashboardActivity.this,
                            AssignRequestListActivity.class);
                    startActivity(intentAssignReqList);
                    break;
                case 1:
                    Intent intentReservePrice = new Intent(this,
                            EnterReservePriceListActivity.class);
                    startActivity(intentReservePrice);
                    break;
                case 2:
                    Intent intentXMartPendingPrice = new Intent(this,
                            OfferToCustomerListActivity.class);
                    startActivity(intentXMartPendingPrice);
                    break;
                case 3:
                    Intent intentFinalOfferAndProcurement = new Intent(this,
                            FinalProcurementListActivity.class);
                    startActivity(intentFinalOfferAndProcurement);
                    break;
            }
        } else {
            CommonHelper.toast(R.string.master_sync_request, context);
        }
    }

    @Override
    public void onLongClick(View view, int position) {


    }

    @Override
    public void onClick(View view) {
        LogUtil.getInstance().logE(TAG, "onClick(view)");
        boolean isMasterSync = masterDatabaseHelper.isMasterSync();
        switch (view.getId()) {
            case R.id.imageViewMenu:
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.imageViewNotification:
                Intent intentNotif = new Intent(ChampionDashboardActivity.this,
                        NotificationsActivity.class);
                startActivityForResult(intentNotif, 300);
                break;
            case R.id.imageViewNewEvaluation:
                if (masterDatabaseHelper.isMasterSync()) {
                    String id = assignRequestListPresenter.getNextEvaluationId();
                    Intent intentEvaluationActivity2 = new Intent(
                            ChampionDashboardActivity.this, NewEvaluationActivity.class);
                    intentEvaluationActivity2.putExtra("status", "newEvaluationWithoutEnquiry");
                    intentEvaluationActivity2.putExtra("purpose", "Direct Buy");
                    intentEvaluationActivity2.putExtra("NewEvaluationId", id);
                    intentEvaluationActivity2.setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentEvaluationActivity2);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.textView_ViewAll_Enquiry:
            case R.id.tableRowRequests:
                if (isMasterSync) {
                    Intent intentAssignReqList = new Intent(
                            ChampionDashboardActivity.this, AssignRequestListActivity.class);
                    startActivity(intentAssignReqList);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutLiveAuction:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentMyAuctions = new Intent(
                            ChampionDashboardActivity.this, MyAuctionsActivity.class);
                    intentMyAuctions.putExtra(ChampionConstants.AUCTION_STATUS_KEY,
                            ChampionConstants.LIVE_STATUS);
                    startActivity(intentMyAuctions);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutCompletedAuction:
                if (isMasterSync) {
                    Intent intentMyAuctions = new Intent(
                            this, MyAuctionsActivity.class);
                    intentMyAuctions.putExtra(ChampionConstants.AUCTION_STATUS_KEY,
                            ChampionConstants.COMPLETED_STATUS);
                    startActivity(intentMyAuctions);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutUpcomingAuction:
                if (isMasterSync) {
                    Intent intentMyAuctions = new Intent(
                            this, MyAuctionsActivity.class);
                    intentMyAuctions.putExtra(ChampionConstants.AUCTION_STATUS_KEY,
                            ChampionConstants.BIDDING_STATUS);
                    startActivity(intentMyAuctions);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutScheduleForTheDay:
                if (isMasterSync) {
                    Intent intentScheduleAppointment = new Intent(
                            this, ChampionScheduleAppointmentActivity.class);
                    intentScheduleAppointment.putExtra("appointmentType", "Today");
                    startActivity(intentScheduleAppointment);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutCancellationRequest:
                if (isMasterSync) {
                    Intent intentScheduleAppointment = new Intent(
                            this, ChampionScheduleAppointmentActivity.class);
                    intentScheduleAppointment.putExtra("appointmentType", "Cancellation");
                    startActivity(intentScheduleAppointment);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutUpcoming:
                if (isMasterSync) {
                    Intent intentUpcomingAppointment = new Intent(
                            this, ChampionScheduleAppointmentActivity.class);
                    intentUpcomingAppointment.putExtra("appointmentType", "Upcoming");
                    startActivity(intentUpcomingAppointment);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutOverdue:
                if (isMasterSync) {
                    Intent intentPendingAppointment = new Intent(
                            this, ChampionScheduleAppointmentActivity.class);
                    intentPendingAppointment.putExtra("appointmentType", "Pending");
                    startActivity(intentPendingAppointment);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowCompltetedEvaluation:
                if (isMasterSync) {
                    Intent intentCompletedEvaluation = new Intent(
                            this, EvaluationCompletedDraftActivity.class);
                    intentCompletedEvaluation.putExtra("appointmentType", "Completed");
                    startActivity(intentCompletedEvaluation);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowDraftEvaluation:
                if (isMasterSync) {
                    Intent intentDraftEvaluation = new Intent(
                            this, EvaluationCompletedDraftActivity.class);
                    intentDraftEvaluation.putExtra("appointmentType", "Draft");
                    startActivity(intentDraftEvaluation);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowEvaluatorwiseStatus:
                if (isMasterSync) {
                    Intent intentEvaluatorwiseStatus = new Intent(
                            this, EvaluatorwiseStatusActivity.class);
                    startActivity(intentEvaluatorwiseStatus);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowDshboard:
                Intent intentDashboard = new Intent(
                        this, ChampionDashboardActivity.class);
                intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentDashboard);
                break;
            case R.id.tableRowAuctions:
                if (isMasterSync) {
                    Intent intentMyAuctions = new Intent(
                            this, MyAuctionsActivity.class);
                    intentMyAuctions.putExtra(ChampionConstants.AUCTION_STATUS_KEY,
                            ChampionConstants.LIVE_STATUS);
                    startActivity(intentMyAuctions);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowReservePrice:
                if (isMasterSync) {
                    Intent intentReservePrice = new Intent(this,
                            EnterReservePriceListActivity.class);
                    startActivity(intentReservePrice);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowOfferPrice:
                if (isMasterSync) {
                    Intent intentXMartPendingPrice = new Intent(this,
                            OfferToCustomerListActivity.class);
                    startActivity(intentXMartPendingPrice);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowFollowupCustomer:
                if (masterDatabaseHelper.isMasterSync()) {

                    Intent intentFinalOfferAndProcurement = new Intent(this,
                            FinalProcurementListActivity.class);
                    startActivity(intentFinalOfferAndProcurement);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowSyncMaster:
                if (CommonHelper.isConnectingToInternet(getApplicationContext())) {
                    masterDatabaseHelper.truncateMasterTable();
                    new MasterSyncHelper(this).syncMasters();
                } else
                    CommonHelper.toast(R.string.internet_error, this);
                drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.textViewLogout:
                LoginHelper.logoutUser(this);
                break;
            case R.id.imageViewProfilePic:
                startActivity(new Intent(ChampionDashboardActivity.this,
                        Activity_Profile_Pic.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRefresh() {
        LogUtil.getInstance().logE(TAG, "onRefresh()");
        swipeContainer.setRefreshing(false);
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            if (masterDatabaseHelper.isMasterSync()) {
                if (!isEnquiryAPICalled) {
                    isEnquiryAPICalled = true;
                    assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.OPEN_STATUS, 1);
                    showProgressDialog("Downloading dashboard data... Please wait...");
                }
            } else {
                CommonHelper.toast(R.string.master_sync_request, context);
            }
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> _enquiryRequestArrayList) {
        LogUtil.getInstance().logE(TAG, "onDownloadSuccess()");
        LogUtil.getInstance().logE("Champ-DownloadSuccess", "Request ID: " + requestId);
        if (_enquiryRequestArrayList != null && _enquiryRequestArrayList.size() > 0) {
            LogUtil.getInstance().logE("Champ-DownloadSuccess", " Request Status : "
                    + _enquiryRequestArrayList.get(0).getRequestStatus() + " List Size: "
                    + _enquiryRequestArrayList.size());
        }
        switch (requestId) {
            case com.mahindra.mitra20.champion.constants.WebConstants.EnquiryRequestList.REQUEST_CODE:
                if (_enquiryRequestArrayList != null && !_enquiryRequestArrayList.isEmpty()) {

                    enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(DASHBOARD_DATA_LIMIT,
                            ChampionConstants.OPEN_STATUS);

                    this.assignRequestAdapter.updateList(this.enquiryRequestArrayList);
                    assignRequestAdapter.notifyDataSetChanged();
                }
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.EVALUATION_DONE, 2);
                break;
            case 2:
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.RESERVE_PRICE, 3);
                break;
            case 3:
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.CANCELLATION_DONE, 4);
                break;
            case 4:
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.FOLLOWUP_CUSTOMER, 5);
                break;
            case 5:
                assignRequestListPresenter.downloadLiveAuctionMaster(ChampionConstants.LIVE_STATUS, 6);
                break;
            case 6:
                assignRequestListPresenter.downloadLiveAuctionMaster(ChampionConstants.BIDDING_STATUS, 7);
                break;
            case 7:
                assignRequestListPresenter.downloadLiveAuctionMaster(ChampionConstants.COMPLETED_STATUS, 8);
                break;
            case 8:
                assignRequestListPresenter.downloadEvaluatorMaster(9);
                break;
            case 9:
                assignRequestListPresenter.downloadEnquiryMaster(10);
                break;
            case 10:
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.OFFER_PRICE, 11);
                break;
            case 11:
                assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.PROCURE_PRICE, 12);
                break;
            case 12:
                isEnquiryAPICalled = false;
                if (null != dialog && dialog.isShowing()) {
                    dialog.dismiss();
                }
                try {
                    if (sharedPref.getBoolean(SharedPreferenceKeys.NOTIFICATION_RECEIVED, false)) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean(SharedPreferenceKeys.NOTIFICATION_RECEIVED, false);
                        editor.apply();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                swipeContainer.setRefreshing(false);
                LogUtil.getInstance().logE("Champ-DownloadSuccess",
                        "onDownloadSuccess(case 12)->setDashboardUpdatedCount()");
                setDashboardUpdatedCount();
                break;
            case 13:
                if (_enquiryRequestArrayList != null && !_enquiryRequestArrayList.isEmpty()) {
                    enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(
                            DASHBOARD_DATA_LIMIT, ChampionConstants.OPEN_STATUS);
                    this.assignRequestAdapter.updateList(this.enquiryRequestArrayList);
                    assignRequestAdapter.notifyDataSetChanged();
                    try {
                        GeneralConstants.DATA_FETCHED = "true";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        LogUtil.getInstance().logE(TAG, "onDownloadFail()");
        swipeContainer.setRefreshing(false);
        isEnquiryAPICalled = false;
        hideProgressDialog();
    }

    @Override
    public void onCallPress(String contactNo) {
        LogUtil.getInstance().logE(TAG, "onCallPress()");
        if (contactNo != null && !contactNo.isEmpty()) {
            new DialogsHelper(ChampionDashboardActivity.this).callDialog(contactNo);
        }
    }

    @Override
    public void onMessagePress(String contactNo) {
        LogUtil.getInstance().logE(TAG, "onMessagePress()");
        CommonHelper.sendMessage(contactNo, ChampionDashboardActivity.this);
    }

    public void showProgressDialog(String msg) {
        LogUtil.getInstance().logE(TAG, "showProgressDialog() msg " + msg);
        if (null == dialog) {
            dialog = new ProgressDialog(ChampionDashboardActivity.this);
        }
        dialog.setCancelable(false);
        if (null != msg && !msg.isEmpty()) {
            dialog.setMessage(msg);
        } else {
            dialog.setMessage("Wait while loading");
        }
        LogUtil.getInstance().logE(TAG, "showProgressDialog() Dialog isShowing " + dialog.isShowing());
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void hideProgressDialog() {
        LogUtil.getInstance().logE(TAG, "hideProgressDialog()");
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view, int position, int x) {
        LogUtil.getInstance().logE(TAG, "onClick(View view, int position, int x)");
        try {
            if (view.getId() == R.id.layout_item_assign_request) {
                if (enquiryRequestArrayList != null && !enquiryRequestArrayList.isEmpty()) {

                    Intent intentAssignRequest = new Intent(
                            ChampionDashboardActivity.this, AppointmentDetailsActivity.class);
                    intentAssignRequest.putExtra(ASSIGN_REQUEST_MODEL_KEY,
                            enquiryRequestArrayList.get(position));
                    startActivity(intentAssignRequest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.getInstance().logE(TAG, "onActivityResult()");
        if (requestCode == 300) {
            displayUnreadNotificationIcon();
        }
    }

    private void showAlertForAppUpdate() {
        AlertDialog.Builder msg = new AlertDialog.Builder(ChampionDashboardActivity.this);
        msg.setMessage(getString(R.string.text_update_available));
        msg.setCancelable(false);
        msg.setPositiveButton(R.string.text_update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                            + BuildConfig.APPLICATION_ID)));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id="
                                    + BuildConfig.APPLICATION_ID)));
                }
                dialog.cancel();
                finish();
            }
        });
    /*msg.setNegativeButton(R.string.text_update_continue, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            moveToNextScreen();
            dialog.cancel();
        }
    });*/
        msg.setCancelable(false);
        msg.show();
    }


    //ADDED BY PRAVIN DHARAM
    private void exportDB() {
        try {
            File dbFile = new File(this.getDatabasePath(DATABASE_NAME).getAbsolutePath());
            FileInputStream fis = new FileInputStream(dbFile);

            File direct = new File(DirectoryName);

            if (!direct.exists()) {
                if (direct.mkdir()) {
                    //directory is created;
                }

            }

            String outFileName = DirectoryName + File.separator +
                    DATABASE_NAME + ".db";

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);

            // Transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            // Close the streams
            output.flush();
            output.close();
            fis.close();


        } catch (IOException e) {
            Log.e("dbBackup:", e.getMessage());
        }
    }

    @Override
    public void onUpdateNeeded(String updateUrl,boolean isMajorUpdate) {
        new UpdateAppHelper(this, updateUrl,isMajorUpdate).showUpdateDialog();
    }

    @Override
    public void updateNotFound() {
        //TODO
    }

}