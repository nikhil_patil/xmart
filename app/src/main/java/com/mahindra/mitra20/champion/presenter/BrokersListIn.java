package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.BrokerMaster;

/**
 * Created by user on 8/11/2018.
 */

public interface BrokersListIn {

    void onDownloadSuccess(int requestId, BrokerMaster brokerMaster);

    void onDownloadFail(int requestId, String message);

}
