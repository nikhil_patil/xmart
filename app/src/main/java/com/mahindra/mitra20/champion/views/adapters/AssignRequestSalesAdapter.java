package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.evaluator.helper.ViewHelper;
import com.mahindra.mitra20.interfaces.ContactIn;

import java.util.ArrayList;

public class AssignRequestSalesAdapter extends RecyclerView.Adapter<AssignRequestSalesAdapter.ViewHolder> {

    private ArrayList<EnquiryRequest> assignRequestContents;
    private ArrayList<EnquiryRequest> backupAssignRequestContents;
    Context context;
    private ItemClickListener clickListener;
    private ContactIn contactIn;

    public void updateList(ArrayList<EnquiryRequest> assignRequestContents) {
        this.backupAssignRequestContents = new ArrayList<>();
        this.backupAssignRequestContents.addAll(assignRequestContents);

        this.assignRequestContents = new ArrayList<>();
        this.assignRequestContents.addAll(assignRequestContents);
    }

    public void setAssignRequestContents(ArrayList<EnquiryRequest> assignRequestContents) {
        this.backupAssignRequestContents = new ArrayList<>();
        this.backupAssignRequestContents.addAll(assignRequestContents);

        this.assignRequestContents = new ArrayList<>();
        this.assignRequestContents.addAll(assignRequestContents);
    }

    public void filter(final String text) {
        try {
            assignRequestContents.clear();
            if (TextUtils.isEmpty(text)) {
                assignRequestContents.addAll(backupAssignRequestContents);
            } else {
                for (EnquiryRequest enquiryRequest : backupAssignRequestContents) {

                    String tempText = text.toLowerCase();
                    if (enquiryRequest.getNameOfEnquiry().toLowerCase().contains(tempText)) {

                        assignRequestContents.add(enquiryRequest);
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView tvName, tvVehicleName, textViewLeadPriority, textViewLocation,
                textViewSalesPersonName, textViewSalesPersonContactNo, textViewSource, textViewType;
        LinearLayout layoutItemAssignRequest;
        public TableRow tableRowVehicleDetails;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            tvName = v.findViewById(R.id.tvName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewSource = v.findViewById(R.id.textViewEnqSource);
            textViewType = v.findViewById(R.id.textViewEnqType);
            tvVehicleName = v.findViewById(R.id.tvBrokerId);
            textViewSalesPersonName = v.findViewById(R.id.textViewSalesPersonName);
            textViewSalesPersonContactNo = v.findViewById(R.id.textViewSalesPersonContactNo);
            ImageView imageViewCall = v.findViewById(R.id.imageViewCall);
            ImageView imageViewMessage = v.findViewById(R.id.imageViewMessage);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);
            layoutItemAssignRequest = v.findViewById(R.id.layout_item_assign_request);
            tableRowVehicleDetails = v.findViewById(R.id.tableRowVehicleDetails);

            layoutItemAssignRequest.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);
            imageViewMessage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.layout_item_assign_request:
                    if (clickListener != null) {
                        clickListener.onClick(view, getActualItemPosition(getAdapterPosition()));
                    }
                    break;
                case R.id.imageViewCall:

                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onCallPress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
                case R.id.imageViewMessage:
                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onMessagePress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
            }
        }

        private int getActualItemPosition(int adapterPosition) {
            EnquiryRequest enquiryRequest = assignRequestContents.get(adapterPosition);
            int index = -1;
            for (EnquiryRequest enquiryRequestCurrent: backupAssignRequestContents) {

                index++;
                if (enquiryRequest.equals(enquiryRequestCurrent)) {

                    return index;
                }
            }
            return -1;
        }
    }

    public void setClickListener(ItemClickListener monClick) {
        this.clickListener = monClick;
    }

    public void setContactListener(ContactIn contactIn) {
        this.contactIn = contactIn;
    }

    public AssignRequestSalesAdapter(Context _context, ContactIn contactIn, boolean showVehicleName) {
        context = _context;
        this.contactIn = contactIn;
    }

    @Override
    public AssignRequestSalesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_assign_request_sales, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tvName.setText(assignRequestContents.get(position).getNameOfEnquiry());
        holder.textViewLocation.setText(assignRequestContents.get(position).getLocation());

        String strMake = assignRequestContents.get(position).getMake();
        String strModel = assignRequestContents.get(position).getModel();
        holder.tvVehicleName.setText(String.format("%s %s", strMake, strModel));

        holder.textViewSalesPersonName.setText(
                assignRequestContents.get(position).getSalesconsultantname());
        holder.textViewSalesPersonContactNo.setText(
                assignRequestContents.get(position).getSalesconsultantContactnum());
        ViewHelper.populateLeadView(context, holder.textViewLeadPriority,
                assignRequestContents.get(position).getLeadPriority());
        String source = assignRequestContents.get(position).getEnquirySource();
        holder.textViewSource.setText(String.format("Source: %s", (null != source) ? source : ""));
        String type = assignRequestContents.get(position).getEnquiryType();
        holder.textViewType.setText(String.format("Type:  %s", (null != type) ? type : ""));
    }

    @Override
    public int getItemCount() {
        return assignRequestContents.size();
    }
}