package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 8/30/2018.
 */

public class AppointmentDetailsPresenter implements VolleySingleTon.VolleyInteractor {

    private Context context;
    private AppointmentDetailsIn appointmentDetailsIn;
    private String status;

    public AppointmentDetailsPresenter(Context context, AppointmentDetailsIn appointmentDetailsIn) {
        this.appointmentDetailsIn = appointmentDetailsIn;
        this.context = context;
    }

    public void updateAppointmentDetails(HashMap<String, String> params) {
        try {
            this.status = params.get(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.APPOINTMENT_STATUS);
            JSONObject jsonObject = new JSONObject(params);
            VolleySingleTon.getInstance().connectToPostUrl(context, this, com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.URL, jsonObject, 2);
        } catch (Exception e) {
            e.printStackTrace();
            appointmentDetailsIn.uploadFailed(WebConstants.EnquiryRequestList.REQUEST_CODE, context.getResources().getString(R.string.download_failed));
        }
    }

    public void assignRequestToEvaluator(ArrayList<EnquiryRequest> enquiryRequestArrayList, String sStartDate) {
        try {
            JSONObject params = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < enquiryRequestArrayList.size(); i++) {
                JSONObject jsonObjectChild = new JSONObject();
                EnquiryRequest enquiryRequest = enquiryRequestArrayList.get(i);
                jsonObjectChild.put("UserID", SessionUserDetails.getInstance().getUserID());
                jsonObjectChild.put("RequestID", enquiryRequest.getRequestID());
                jsonObjectChild.put("ScheduledStartDateTime", sStartDate);
                jsonObjectChild.put("EvaluatorID", SessionUserDetails.getInstance().getUserID());

                jsonArray.put(jsonObjectChild);
            }

            params.put("AssignRequestDetails", jsonArray);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.AssignRequestToEvaluator.URL, params,
                    WebConstants.AssignRequestToEvaluator.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        if (requestId == WebConstants.AssignRequestToEvaluator.REQUEST_CODE) {
            appointmentDetailsIn.uploadSuccess(WebConstants.AssignRequestToEvaluator.REQUEST_CODE, "SUCCESS");
        } else {
            if (status.equalsIgnoreCase(ChampionConstants.APPOINTMENT_STATUS_CONFIRM)) {
                appointmentDetailsIn.uploadSuccess(7, response);
            } else if (status.equalsIgnoreCase(ChampionConstants.APPOINTMENT_STATUS_CANCEL)) {
                appointmentDetailsIn.uploadSuccess(8, response);
            }
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            appointmentDetailsIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getNextEvaluationId() {
        EvaluatorHelper evaluatorHelper = new EvaluatorHelper(context);
        return evaluatorHelper.getNextEvaluationId();
    }
}
