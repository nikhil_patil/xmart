package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;
import com.mahindra.mitra20.interfaces.ContactIn;

import java.util.ArrayList;


/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class EvaluatorListAdapter extends RecyclerView.Adapter<EvaluatorListAdapter.ViewHolder> {

    private ArrayList<EvaluatorMaster> evaluatorMastersContents;
    private ItemClickListener clickListener;
    private ContactIn contactIn;
    Context context;

    public void updateList(ArrayList<EvaluatorMaster> evaluatorMastersContents){
        this.evaluatorMastersContents = evaluatorMastersContents;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textViewEvaluatorName, textViewCurrentlyAssigned;
        public ImageView imageViewCall;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewEvaluatorName = v.findViewById(R.id.textViewEvaluatorName);
            textViewCurrentlyAssigned = v.findViewById(R.id.textViewCurrentlyAssigned);
            imageViewCall = v.findViewById(R.id.imageViewCall);

            v.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int id = view.getId();

            if (id == R.id.imageViewCall) {
                contactIn.onCallPress(evaluatorMastersContents.get(getAdapterPosition()).getEvaluatorContactNo());
            } else {
                if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
            }
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public void setContactListener(ContactIn contactIn) {
        this.contactIn = contactIn;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public EvaluatorListAdapter(Context _context, ArrayList<EvaluatorMaster> _evaluatorMasterContents) {
        evaluatorMastersContents = _evaluatorMasterContents;
        context = _context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EvaluatorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_select_evaluator, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewEvaluatorName.setText(evaluatorMastersContents.get(position).getEvaluatorName());
        holder.textViewCurrentlyAssigned.setText("Currently Assigned : " + evaluatorMastersContents.get(position).getAllocatedReqCount());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return evaluatorMastersContents.size();
    }
}