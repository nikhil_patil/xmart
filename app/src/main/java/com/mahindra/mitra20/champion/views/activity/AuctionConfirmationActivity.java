package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TableRow;

import com.mahindra.mitra20.R;

public class AuctionConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    TableRow tableDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acution_confirmation);

        initUI();
    }

    private void initUI() {
        tableDone = findViewById(R.id.tableDone);

        tableDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.tableDone:
                finish();
                Intent intentDashboard = new Intent(this, ChampionDashboardActivity.class);
                intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentDashboard);
                break;
        }
    }
}
