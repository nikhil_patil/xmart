package com.mahindra.mitra20.champion.presenter;

import android.content.Context;
import android.util.Pair;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;
import com.mahindra.mitra20.champion.models.comparator.BidDateTimeComparator;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.presenters.BrokerEvaluationDetailsPresenter;
import com.mahindra.mitra20.sqlite.ChampionHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.XMART;

/**
 * Created by user on 8/11/2018.
 */

public class AssignRequestListPresenter implements VolleySingleTon.VolleyInteractor {

    private AssignRequestListIn assignRequestListIn;
    private Context context;
    private String status;
    private ChampionHelper championHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    public AssignRequestListPresenter(AssignRequestListIn assignRequestListIn, Context context) {
        this.assignRequestListIn = assignRequestListIn;
        this.context = context;
        championHelper = new ChampionHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    public void downloadEnquiryMaster(String status) {
        try {
            this.status = status;
            HashMap<String, String> params = new HashMap<>();
            String employeeCode = SessionUserDetails.getInstance().getEmployeeCode();
            if (null != employeeCode && !employeeCode.isEmpty())
                params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.ScheduleAppoinmentList.USER_ID, employeeCode);
            else
                params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getUserID());
            params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getUserID());
            params.put(WebConstants.EnquiryRequestList.REQUEST_STATUS, status);
            params.put(WebConstants.EnquiryRequestList.EVALUATION_REPORT_ID, "");
            params.put(WebConstants.EnquiryRequestList.COUNT_TO_DISPLAY_DASHBOARD, "9999");
            params.put(WebConstants.EnquiryRequestList.PINCODE, "");

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.EnquiryRequestList.URL, params,
                    WebConstants.EnquiryRequestList.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.EnquiryRequestList.REQUEST_CODE, context.getResources().getString(R.string.download_failed));
        }
    }

    public void downloadEvaluationReport(HashMap<String, String> param) {
        try {
            JSONObject params = new JSONObject(param);
            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.GetEvaluationReport.URL, params,
                    WebConstants.GetEvaluationReport.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.GetEvaluationReport.REQUEST_CODE,
                    context.getResources().getString(R.string.download_failed));
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            switch (requestId) {
                case WebConstants.EnquiryRequestList.REQUEST_CODE:
                    ArrayList<EnquiryRequest> listEnquiryRequests = new ArrayList<>();

                    JSONObject jsonObjectParent = new JSONObject(response);
                    JSONArray jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EnquiryRequestList");

                    if (arrayListMake.isEmpty()) {
                        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
                    }

                    for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {
                        EnquiryRequest enquiryRequest = new EnquiryRequest();

                        JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);
                        enquiryRequest.setContactNo(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "ContactNo"));
                        enquiryRequest.setDateOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "DateOfAppointment"));

                        if ((status.equalsIgnoreCase(ChampionConstants.EVALUATION_DONE)) ||
                                (status.equalsIgnoreCase(ChampionConstants.OFFER_PRICE_STATUS_KEY))) {
                            enquiryRequest.setMake(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Make"));
                            enquiryRequest.setModel(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Model"));
                        } else {
                            Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                                    arrayListMake,CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Make"),
                                    CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Model"));
                            enquiryRequest.setMake(makeModel.first);
                            enquiryRequest.setModel(makeModel.second);
                        }

                        enquiryRequest.setNameOfEnquiry(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "NameOfEnquiry"));
                        enquiryRequest.setPinCode(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Pincode"));
                        enquiryRequest.setRequestID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "RequestID"));
                        enquiryRequest.setLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "LocationOfAppointment"));
                        enquiryRequest.setCustomerExpectedPrice(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "CustomerExpectedPrice"));
                        enquiryRequest.setRequestStatus(status);

                        enquiryRequest.setEvaluationReportID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluationReportID"));
                        enquiryRequest.setEvaluatorID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluatorID"));
                        enquiryRequest.setTimeOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "TimeOfAppointment"));
                        enquiryRequest.setYearOfManifacturing(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "YearOfManufacturing"));
                        enquiryRequest.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "OptedNewVehicle"));
                        enquiryRequest.setSalesconsultantname(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Salesconsultantname"));
                        enquiryRequest.setSalesconsultantContactnum(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "SalesconsultantContactnum"));
                        enquiryRequest.setLeadPriority(CommonHelper.parseLeadPriorityKey(jsonObjectEnquiryRequest, "LeadPriority"));
                        enquiryRequest.setEnquiryLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryLocation"));
                        enquiryRequest.setEnquirySource(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquirySource"));
                        enquiryRequest.setEnquiryType(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryType"));
                        if (!CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "RequestStatus").equalsIgnoreCase("Auction completed")) {
                            listEnquiryRequests.add(enquiryRequest);
                        }
                    }

                    championHelper.deleteEnquiryRequestDetails(status);
                    championHelper.putEnquiryRequestDetails(listEnquiryRequests);
                    assignRequestListIn.onDownloadSuccess(WebConstants.EnquiryRequestList.REQUEST_CODE, listEnquiryRequests);
                    break;
                case WebConstants.EvaluatorMaster.REQUEST_CODE:
                    ArrayList<EvaluatorMaster> evaluatorMasterArrayList = new ArrayList<>();
                    jsonObjectParent = new JSONObject(response);
                    jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EvaluatorDetails");

                    for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {
                        EvaluatorMaster evaluatorMaster = new EvaluatorMaster();

                        JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);
                        evaluatorMaster.setAllocatedReqCount(jsonObjectEnquiryRequest.getString("ALLOCATED_REQ_COUNT"));
                        evaluatorMaster.setEvaluatorContactNo(jsonObjectEnquiryRequest.getString("Evaluator_Contact_Number"));
                        evaluatorMaster.setEvaluatorID(jsonObjectEnquiryRequest.getString("Evaluator_ID"));
                        evaluatorMaster.setEvaluatorName(jsonObjectEnquiryRequest.getString("Evaluator_Name"));

                        evaluatorMasterArrayList.add(evaluatorMaster);
                    }

                    championHelper.deleteEvaluatorMaster();
                    championHelper.putEvaluatorMaster(evaluatorMasterArrayList);
                    assignRequestListIn.onDownloadSuccess(WebConstants.EvaluatorMaster.REQUEST_CODE, null);
                    break;
                case WebConstants.GetEvaluationReport.REQUEST_CODE:
                    JSONObject jsonObject = new JSONObject(response);

                    NewEvaluation newEvaluation =
                            BrokerEvaluationDetailsPresenter.getNewEvaluationInstance(jsonObject);

                    EvaluatorHelper evaluatorHelper = new EvaluatorHelper(context);
                    long l = evaluatorHelper.addUpdateNewEvaluationDetails(newEvaluation,
                            EvaluatorConstants.EVALUATION_DB_STATUS_ONLINE);

                    ArrayList<EnquiryRequest> enquiryRequestArrayList = new ArrayList<>();
                    EnquiryRequest enquiryRequest = new EnquiryRequest();
                    enquiryRequest.setEvaluationReportID(String.valueOf(l));
                    enquiryRequestArrayList.add(enquiryRequest);

                    assignRequestListIn.onDownloadSuccess(WebConstants.GetEvaluationReport.REQUEST_CODE,
                            enquiryRequestArrayList);
                    break;
                case WebConstants.LiveAuctionMaster.REQUEST_CODE:
                    try {
                        jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("AuctionDetails");
                        ArrayList<AuctionDetail> auctionDetailArrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            AuctionDetail auctionDetail = new AuctionDetail();

                            JSONObject jsonObjectAuctionDetails = (JSONObject) jsonArray.get(i);
                            auctionDetail.setAuctionDate(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionDate"));
                            auctionDetail.setAuctionID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionID"));
                            auctionDetail.setEvaluationReportID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionID"));
                            auctionDetail.setAuctionStatus(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionStatus"));
                            auctionDetail.setCountOfBidders(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "CountOfBidders"));
                            auctionDetail.setMITRAPriceBasePrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                            auctionDetail.setRequestID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "RequestID"));
                            auctionDetail.setStartTimestamp(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "StartTimestamp"));
                            auctionDetail.setRemainingTimeInSeconds(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "remainingTimeInSeconds"));
                            auctionDetail.setVehOwnerName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "vehOwnerName"));
                            auctionDetail.setVehicleName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "vehicleName"));
                            auctionDetail.setHighestPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "HighestPrice"));
                            auctionDetail.setTransactionNo(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "TransactionNo."));
                            auctionDetail.setHighestBidder(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "HighestBidder"));
                            auctionDetail.setNegotiatorContactName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactName"));
                            auctionDetail.setNegotiatorContactNumber(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                            auctionDetail.setNoOfOwners(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NoOfOwners"));
                            auctionDetail.setOdometerReading(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "OdometerReading"));
                            auctionDetail.setVehicleUsage(SpinnerHelper.getValueByCodeFromMap(
                                    SpinnerConstants.mapVehicleUsage,
                                    CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "VehicleUsage")));

                            JSONArray jsonArrayLIST = jsonObjectAuctionDetails.getJSONArray("LIST");
                            List<AuctionLIST> listList = new ArrayList<>();

                            for (int j = 0; j < jsonArrayLIST.length(); j++) {
                                JSONObject jsonObjectLIST = jsonArrayLIST.getJSONObject(j);
                                AuctionLIST auctionLIST = new AuctionLIST();

                                auctionLIST.setMITRAId(CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId"));
                                auctionLIST.setMITRAName(CommonHelper.replaceCars24MFCWChampion(CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAName"), CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId"), CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactName")));
                                auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectLIST, "bidPrice"));
                                auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectLIST, "bidingDateTime"));

                                if (CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId").equalsIgnoreCase(ChampionConstants.MFCW_ID)) {
                                    auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                                } else {
                                    auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectLIST, "PhoneNum"));
                                }

                                auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectLIST,"AmountWithoutCommision"));
                                auctionLIST.setCommissionPrice(CommonHelper.hasJSONKey(jsonObjectLIST,"CommisionAmnt"));
                                listList.add(auctionLIST);
                            }

                            if (!CommonHelper.isChampionExistInList(listList)) {
                                AuctionLIST auctionLIST = new AuctionLIST();
                                auctionLIST.setMITRAId(SessionUserDetails.getInstance().getUserID().toUpperCase());
                                auctionLIST.setCommissionPrice("0");
                                auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                                auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                                auctionLIST.setMITRAName(XMART);
                                auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionDate") + " " + CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "StartTimestamp"));
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "ContactNo"));

                                listList.add(auctionLIST);
                            }

                            if (status.equalsIgnoreCase(ChampionConstants.COMPLETED_STATUS)) {
                                try {
                                    Collections.sort(listList, new BidDateTimeComparator());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            auctionDetail.setLIST(listList);
                            auctionDetailArrayList.add(auctionDetail);
                        }

                        championHelper.deleteAuctionDetails(status);
                        championHelper.putAuctionDetails(auctionDetailArrayList);

                        assignRequestListIn.onDownloadSuccess(WebConstants.LiveAuctionMaster.REQUEST_CODE, null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            CommonHelper.toast(jsonObject.getString("message"), context);
            assignRequestListIn.onDownloadFail(requestId, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}