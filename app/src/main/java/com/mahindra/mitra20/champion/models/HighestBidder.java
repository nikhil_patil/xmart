package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 8/2/2018.
 */

public class HighestBidder implements Parcelable {

    String MITRAID;
    String strBidderName;
    String strBiddingCost;
    String strCommissionCost;
    String contactNo;
    String strDeductedCost;
    boolean isSelected;
    boolean isCallEnable;

    public boolean isCallEnable() {
        return isCallEnable;
    }

    public void setCallEnable(boolean callEnable) {
        isCallEnable = callEnable;
    }

    public String getStrDeductedCost() {
        return strDeductedCost;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getStrCommissionCost() {
        return strCommissionCost;
    }

    public String getMITRAID() {
        return MITRAID;
    }

    public String getStrBidderName() {
        return strBidderName;
    }

    public String getStrBiddingCost() {
        return strBiddingCost;
    }

    public HighestBidder(String mitraID, String strBidderName, String strBiddingCost, String strDeductedCost, String strCommissionCost, String contactNo, boolean isSelected, boolean isCallEnable) {

        this.MITRAID = mitraID;
        this.strBidderName = strBidderName;
        this.strBiddingCost = strBiddingCost;
        this.strCommissionCost = strCommissionCost;
        this.contactNo = contactNo;
        this.isSelected = isSelected;
        this.strDeductedCost = strDeductedCost;
        this.isCallEnable = isCallEnable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.MITRAID);
        dest.writeString(this.strBidderName);
        dest.writeString(this.strBiddingCost);
        dest.writeString(this.strCommissionCost);
        dest.writeString(this.contactNo);
        dest.writeString(this.strDeductedCost);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isCallEnable ? (byte) 1 : (byte) 0);
    }

    protected HighestBidder(Parcel in) {
        this.MITRAID = in.readString();
        this.strBidderName = in.readString();
        this.strBiddingCost = in.readString();
        this.strCommissionCost = in.readString();
        this.contactNo = in.readString();
        this.strDeductedCost = in.readString();
        this.isSelected = in.readByte() != 0;
        this.isCallEnable = in.readByte() != 0;
    }

    public static final Creator<HighestBidder> CREATOR = new Creator<HighestBidder>() {
        @Override
        public HighestBidder createFromParcel(Parcel source) {
            return new HighestBidder(source);
        }

        @Override
        public HighestBidder[] newArray(int size) {
            return new HighestBidder[size];
        }
    };
}
