package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.ChampionDasboardContents;

import java.util.ArrayList;

/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class CustomGridAdapter extends RecyclerView.Adapter<CustomGridAdapter.ViewHolder> {
    private ArrayList<ChampionDasboardContents> championDashboardContents;
    private Context context;
    private ItemClickListener monClick;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView champion_dashboard_textView_count;
        TextView champion_dashboard_textView_title;
        LinearLayout champion_dashboard_list;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            champion_dashboard_textView_count = v.findViewById(R.id.champion_dashboard_textView_count);
            champion_dashboard_textView_title = v.findViewById(R.id.champion_dashboard_textView_title);
            champion_dashboard_list = v.findViewById(R.id.champion_dasbhboard_list);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (monClick != null) monClick.onClick(view, getAdapterPosition());
        }
    }

    public void setmonClick(ItemClickListener monClick) {
        this.monClick = monClick;
    }

    public CustomGridAdapter(Context _context , ArrayList<ChampionDasboardContents> _championDasboardContents) {
        championDashboardContents = _championDasboardContents;
        context = _context;
    }

    @Override
    public CustomGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.list_item_dashboard, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.champion_dashboard_textView_count.setText(championDashboardContents.get(position).getStrCount());
        holder.champion_dashboard_textView_title.setText(championDashboardContents.get(position).getStrTitle());
        if(position == 0) {
            holder.champion_dashboard_textView_count.setTextColor(
                    context.getResources().getColor(R.color.colorBlue));
        } else if(position == 1) {
            holder.champion_dashboard_textView_count.setTextColor(
                    context.getResources().getColor(R.color.colorPurple));
        } else if(position == 2) {
            holder.champion_dashboard_textView_count.setTextColor(
                    context.getResources().getColor(R.color.colorOrange));
        } else if(position == 3) {
            holder.champion_dashboard_textView_count.setTextColor(
                    context.getResources().getColor(R.color.colorPink));
        }
    }

    @Override
    public int getItemCount() {
        return championDashboardContents.size();
    }
}