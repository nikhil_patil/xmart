package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.AuctionDetail;

import java.util.ArrayList;

/**
 * Created by user on 8/18/2018.
 */

public interface MyLiveAuctionIn {

    void onDownloadSuccess(int requestId, ArrayList<AuctionDetail> enquiryRequestArrayList);

    void onDownloadFail(int requestId, String message);

}