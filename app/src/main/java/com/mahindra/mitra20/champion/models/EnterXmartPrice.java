package com.mahindra.mitra20.champion.models;

/**
 * Created by BADHAP-CONT on 7/26/2018.
 */

public class EnterXmartPrice {

    private String requestID;
    private String strName;
    private String strVehicleName;
    private String strHighestPrice;

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public void setStrVehicleName(String strVehicleName) {
        this.strVehicleName = strVehicleName;
    }

    public String getStrHighestPrice() {
        return strHighestPrice;
    }

    public void setStrHighestPrice(String strHighestPrice) {
        this.strHighestPrice = strHighestPrice;
    }
}