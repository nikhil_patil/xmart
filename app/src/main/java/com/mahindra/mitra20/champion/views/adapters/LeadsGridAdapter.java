package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.ChampionDasboardContents;

import java.util.ArrayList;

/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class LeadsGridAdapter extends RecyclerView.Adapter<LeadsGridAdapter.ViewHolder> {
    private ArrayList<ChampionDasboardContents> championDashboardContents;
    Context context;
    ItemClickListener monClick;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvCount, tvName;

        public ViewHolder(View v) {
            super(v);
            tvCount = v.findViewById(R.id.tvCount);
            tvName = v.findViewById(R.id.tvName);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (monClick != null) monClick.onClick(view, getAdapterPosition());
        }
    }

    public void setmonClick(ItemClickListener monClick) {
        this.monClick = monClick;
    }

    public LeadsGridAdapter(Context _context , ArrayList<ChampionDasboardContents> _championDasboardContents) {
        championDashboardContents = _championDasboardContents;
        context = _context;
    }

    @Override
    public LeadsGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.list_item_leads, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvCount.setText(championDashboardContents.get(position).getStrCount());
        holder.tvName.setText(championDashboardContents.get(position).getStrTitle());
        if(position == 0) {
            holder.tvCount.setTextColor(context.getResources().getColor(R.color.colorBlue));
        } else if(position == 1) {
            holder.tvCount.setTextColor(context.getResources().getColor(R.color.colorPurple));
        } else if(position == 2) {
            holder.tvCount.setTextColor(context.getResources().getColor(R.color.colorOrange));
        } else if(position == 3) {
            holder.tvCount.setTextColor(context.getResources().getColor(R.color.colorPink));
        }
    }

    @Override
    public int getItemCount() {
        return championDashboardContents.size();
    }
}