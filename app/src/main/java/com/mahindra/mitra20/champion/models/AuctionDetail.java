
package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class AuctionDetail implements Parcelable {

    private String auctionDate;
    private String auctionID;
    private String EvaluationReportID;
    private String auctionStatus;
    private String countOfBidders;
    private List<AuctionLIST> lIST = null;
    private String mITRAPriceBasePrice;
    private String requestID;
    private String startTimestamp;
    private String remainingTimeInSeconds;
    private String vehOwnerName;
    private String vehicleName;
    private String highestPrice;
    private String vehicleImage;
    private String bidResult;
    private String auctionWinnerId;
    private String transactionNo;
    private String vehiclePicURL;
    private String evaluationStatus;
    private String highestBidder;
    private String NegotiatorContactName;
    private String NegotiatorContactNumber;
    private String noOfOwners;
    private String odometerReading;
    private String feedbackRating;
    private String vehicleUsage;

    protected AuctionDetail(Parcel in) {
        auctionDate = in.readString();
        auctionID = in.readString();
        EvaluationReportID = in.readString();
        auctionStatus = in.readString();
        countOfBidders = in.readString();
        lIST = in.createTypedArrayList(AuctionLIST.CREATOR);
        mITRAPriceBasePrice = in.readString();
        requestID = in.readString();
        startTimestamp = in.readString();
        remainingTimeInSeconds = in.readString();
        vehOwnerName = in.readString();
        vehicleName = in.readString();
        highestPrice = in.readString();
        vehicleImage = in.readString();
        bidResult = in.readString();
        auctionWinnerId = in.readString();
        transactionNo = in.readString();
        vehiclePicURL = in.readString();
        evaluationStatus = in.readString();
        highestBidder = in.readString();
        NegotiatorContactName = in.readString();
        NegotiatorContactNumber = in.readString();
        noOfOwners = in.readString();
        odometerReading = in.readString();
        feedbackRating = in.readString();
        vehicleUsage = in.readString();
        contactNo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(auctionDate);
        dest.writeString(auctionID);
        dest.writeString(EvaluationReportID);
        dest.writeString(auctionStatus);
        dest.writeString(countOfBidders);
        dest.writeTypedList(lIST);
        dest.writeString(mITRAPriceBasePrice);
        dest.writeString(requestID);
        dest.writeString(startTimestamp);
        dest.writeString(remainingTimeInSeconds);
        dest.writeString(vehOwnerName);
        dest.writeString(vehicleName);
        dest.writeString(highestPrice);
        dest.writeString(vehicleImage);
        dest.writeString(bidResult);
        dest.writeString(auctionWinnerId);
        dest.writeString(transactionNo);
        dest.writeString(vehiclePicURL);
        dest.writeString(evaluationStatus);
        dest.writeString(highestBidder);
        dest.writeString(NegotiatorContactName);
        dest.writeString(NegotiatorContactNumber);
        dest.writeString(noOfOwners);
        dest.writeString(odometerReading);
        dest.writeString(feedbackRating);
        dest.writeString(vehicleUsage);
        dest.writeString(contactNo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AuctionDetail> CREATOR = new Creator<AuctionDetail>() {
        @Override
        public AuctionDetail createFromParcel(Parcel in) {
            return new AuctionDetail(in);
        }

        @Override
        public AuctionDetail[] newArray(int size) {
            return new AuctionDetail[size];
        }
    };

    public String getNegotiatorContactName() {
        return NegotiatorContactName;
    }

    public void setNegotiatorContactName(String negotiatorContactName) {
        NegotiatorContactName = negotiatorContactName;
    }

    public String getNegotiatorContactNumber() {
        return NegotiatorContactNumber;
    }

    public void setNegotiatorContactNumber(String negotiatorContactNumber) {
        NegotiatorContactNumber = negotiatorContactNumber;
    }

    public String getHighestBidder() {
        return highestBidder;
    }

    public void setHighestBidder(String highestBidder) {
        this.highestBidder = highestBidder;
    }

    public String getEvaluationStatus() {
        return evaluationStatus;
    }

    public void setEvaluationStatus(String evaluationStatus) {
        this.evaluationStatus = evaluationStatus;
    }

    public String getVehiclePicURL() {
        return vehiclePicURL;
    }

    public void setVehiclePicURL(String vehiclePicURL) {
        this.vehiclePicURL = vehiclePicURL;
    }

    public String getEvaluationReportID() {
        return EvaluationReportID;
    }

    public void setEvaluationReportID(String evaluationReportID) {
        EvaluationReportID = evaluationReportID;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getAuctionWinnerId() {
        return auctionWinnerId;
    }

    public void setAuctionWinnerId(String auctionWinnerId) {
        this.auctionWinnerId = auctionWinnerId;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    private String contactNo;

    public String getBidResult() {
        return bidResult;
    }

    public void setBidResult(String bidResult) {
        this.bidResult = bidResult;
    }

    public String getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(String vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public String getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(String highestPrice) {
        this.highestPrice = highestPrice;
    }


    public AuctionDetail() {
    }

    public String getAuctionDate() {
        return auctionDate;
    }

    public void setAuctionDate(String auctionDate) {
        this.auctionDate = auctionDate;
    }

    public String getAuctionID() {
        return auctionID;
    }

    public void setAuctionID(String auctionID) {
        this.auctionID = auctionID;
    }

    public String getAuctionStatus() {
        return auctionStatus;
    }

    public void setAuctionStatus(String auctionStatus) {
        this.auctionStatus = auctionStatus;
    }

    public String getCountOfBidders() {
        return countOfBidders;
    }

    public void setCountOfBidders(String countOfBidders) {
        this.countOfBidders = countOfBidders;
    }

    public List<AuctionLIST> getLIST() {
        return lIST;
    }

    public void setLIST(List<AuctionLIST> lIST) {
        this.lIST = lIST;
    }

    public String getMITRAPriceBasePrice() {
        return mITRAPriceBasePrice;
    }

    public void setMITRAPriceBasePrice(String mITRAPriceBasePrice) {
        this.mITRAPriceBasePrice = mITRAPriceBasePrice;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(String startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getRemainingTimeInSeconds() {
        return remainingTimeInSeconds;
    }

    public void setRemainingTimeInSeconds(String remainingTimeInSeconds) {
        this.remainingTimeInSeconds = remainingTimeInSeconds;
    }

    public String getVehOwnerName() {
        return vehOwnerName;
    }

    public void setVehOwnerName(String vehOwnerName) {
        this.vehOwnerName = vehOwnerName;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public List<AuctionLIST> getlIST() {
        return lIST;
    }

    public void setlIST(List<AuctionLIST> lIST) {
        this.lIST = lIST;
    }

    public String getmITRAPriceBasePrice() {
        return mITRAPriceBasePrice;
    }

    public void setmITRAPriceBasePrice(String mITRAPriceBasePrice) {
        this.mITRAPriceBasePrice = mITRAPriceBasePrice;
    }

    public String getNoOfOwners() {
        return noOfOwners;
    }

    public void setNoOfOwners(String noOfOwners) {
        this.noOfOwners = noOfOwners;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getFeedbackRating() {
        return feedbackRating;
    }

    public void setFeedbackRating(String feedbackRating) {
        this.feedbackRating = feedbackRating;
    }

    public String getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }
}