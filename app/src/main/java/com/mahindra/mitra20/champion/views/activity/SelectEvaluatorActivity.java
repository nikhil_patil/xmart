package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;
import com.mahindra.mitra20.champion.presenter.SelectEvaluatorIn;
import com.mahindra.mitra20.champion.presenter.SelectEvaluatorPresenter;
import com.mahindra.mitra20.champion.views.adapters.EvaluatorListAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;

public class SelectEvaluatorActivity extends AppCompatActivity implements ItemClickListener, SwipeRefreshLayout.OnRefreshListener, SelectEvaluatorIn, ContactIn {

    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private SwipeRefreshLayout swipeContainer;
    private SelectEvaluatorPresenter selectEvaluatorPresenter;
    private ArrayList<EvaluatorMaster> evaluatorMasterArrayList;
    private EvaluatorListAdapter evaluatorListAdapter;
    private ChampionHelper championHelper;
    private EvaluatorMaster selectedEvaluatorMaster;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_select_evaluator);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Select Evaluator");
        championHelper = new ChampionHelper(getApplicationContext());
        enquiryRequestArrayList = getIntent().getParcelableArrayListExtra(ChampionConstants.ASSIGN_REQUEST_LIST_MODEL_KEY);
        swipeContainer = findViewById(R.id.swipeContainer);
        ChampionHelper championHelper = new ChampionHelper(getBaseContext());
        selectEvaluatorPresenter = new SelectEvaluatorPresenter(this, this);

        dialog = new ProgressDialog(SelectEvaluatorActivity.this);
        dialog.setMessage(getResources().getString(R.string.title_loading));

        evaluatorMasterArrayList = championHelper.getEvaluatorMasters();

        EvaluatorMaster evaluatorMaster = new EvaluatorMaster();
        evaluatorMaster.setEvaluatorID(ChampionConstants.MFCW_ID);
        evaluatorMaster.setEvaluatorName("Mahindra First Choice");
        evaluatorMaster.setAllocatedReqCount("0");

        evaluatorMasterArrayList.add(evaluatorMaster);

        EmptyRecyclerView recyclerViewEvaluatorList = findViewById(R.id.recyclerViewEvaluatorList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            onRefresh();
        }

        recyclerViewEvaluatorList.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        evaluatorListAdapter = new EvaluatorListAdapter(getBaseContext(), evaluatorMasterArrayList);
        evaluatorListAdapter.setContactListener(this);
        recyclerViewEvaluatorList.setEmptyView(findViewById(R.id.textViewEmpty));
        //recyclerViewEvaluatorList.setNestedScrollingEnabled(false);
        recyclerViewEvaluatorList.setAdapter(evaluatorListAdapter);
        evaluatorListAdapter.setClickListener(this);
        swipeContainer.setOnRefreshListener(this);
    }

    @Override
    public void onClick(View view, int position) {
        selectedEvaluatorMaster = evaluatorMasterArrayList.get(position);
        dialog.show();
        EvaluatorMaster evaluatorMaster = evaluatorMasterArrayList.get(position);
        if (evaluatorMaster.getEvaluatorID().equalsIgnoreCase(ChampionConstants.MFCW_ID)) {
            selectEvaluatorPresenter.assignRequestToEvaluatorMFCW(enquiryRequestArrayList, evaluatorMaster);
        } else {
            selectEvaluatorPresenter.assignRequestToEvaluator(enquiryRequestArrayList, evaluatorMaster);
        }

    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onRefresh() {
        selectEvaluatorPresenter.downloadEvaluatorMaster();
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EvaluatorMaster> enquiryRequestArrayList) {
        EvaluatorMaster evaluatorMaster = new EvaluatorMaster();
        evaluatorMaster.setEvaluatorID(ChampionConstants.MFCW_ID);
        evaluatorMaster.setEvaluatorName("Mahindra First Choice");
        evaluatorMaster.setAllocatedReqCount("0");

        enquiryRequestArrayList.add(evaluatorMaster);

        if (!enquiryRequestArrayList.isEmpty()) {
            evaluatorMasterArrayList = enquiryRequestArrayList;
            evaluatorListAdapter.updateList(this.evaluatorMasterArrayList);
            evaluatorListAdapter.notifyDataSetChanged();
        }

        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onUploadSuccess(int requestId, String message) {
        dialog.dismiss();
        for (int i = 0; i < enquiryRequestArrayList.size(); i++) {
            EnquiryRequest enquiryRequest = enquiryRequestArrayList.get(i);
            championHelper.updateEnquiryStatus(ChampionConstants.FOLLOWUP_CUSTOMER,
                    enquiryRequest.getRequestID(), null);
        }

        Intent intentAssignConfirm = new Intent(this, AssignConfirmEvaluatorActivity.class);
        intentAssignConfirm.putExtra("data", selectedEvaluatorMaster);
        startActivity(intentAssignConfirm);
    }

    @Override
    public void onUploadFail(int requestId, String message) {
        dialog.dismiss();
        CommonHelper.toast(message, getBaseContext());
    }

    @Override
    public void onCallPress(String contactNo) {
        new DialogsHelper(SelectEvaluatorActivity.this).callDialog(contactNo);
    }

    @Override
    public void onMessagePress(String contactNo) {
        CommonHelper.sendMessage(contactNo, SelectEvaluatorActivity.this);
    }
}