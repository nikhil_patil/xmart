package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 7/24/2018.
 */

public class AssignRequest implements Parcelable {

    String strName;
    String strVehicleName;
    String strAppoinmentDay;
    String strAppoinmentTime;
    String strContactNo;
    int isSelected;

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public void setStrVehicleName(String strVehicleName) {
        this.strVehicleName = strVehicleName;
    }

    public String getStrAppoinmentDay() {
        return strAppoinmentDay;
    }

    public void setStrAppoinmentDay(String strAppoinmentDay) {
        this.strAppoinmentDay = strAppoinmentDay;
    }

    public String getStrAppoinmentTime() {
        return strAppoinmentTime;
    }

    public void setStrAppoinmentTime(String strAppoinmentTime) {
        this.strAppoinmentTime = strAppoinmentTime;
    }

    public String getStrContactNo() {
        return strContactNo;
    }

    public void setStrContactNo(String strContactNo) {
        this.strContactNo = strContactNo;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.strName);
        dest.writeString(this.strVehicleName);
        dest.writeString(this.strAppoinmentDay);
        dest.writeString(this.strAppoinmentTime);
        dest.writeString(this.strContactNo);
        dest.writeInt(this.isSelected);
    }

    public AssignRequest() {
    }

    protected AssignRequest(Parcel in) {
        this.strName = in.readString();
        this.strVehicleName = in.readString();
        this.strAppoinmentDay = in.readString();
        this.strAppoinmentTime = in.readString();
        this.strContactNo = in.readString();
        this.isSelected = in.readInt();
    }

    public static final Creator<AssignRequest> CREATOR = new Creator<AssignRequest>() {
        @Override
        public AssignRequest createFromParcel(Parcel source) {
            return new AssignRequest(source);
        }

        @Override
        public AssignRequest[] newArray(int size) {
            return new AssignRequest[size];
        }
    };
}
