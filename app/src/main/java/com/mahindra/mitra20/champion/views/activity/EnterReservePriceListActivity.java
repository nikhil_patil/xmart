package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AssignRequestListIn;
import com.mahindra.mitra20.champion.presenter.AssignRequestListPresenter;
import com.mahindra.mitra20.champion.views.adapters.AssignRequestAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class EnterReservePriceListActivity extends AppCompatActivity implements ItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, AssignRequestListIn, ContactIn {

    private ChampionHelper championHelper;
    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private SwipeRefreshLayout swipeContainer;
    private AssignRequestListPresenter assignRequestListPresenter;
    private AssignRequestAdapter assignRequestAdapter;
    private EnquiryRequest enquiryRequest;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_awaiting_reserve_price);

        initUI();
    }

    protected void initUI() {
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);

        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.title_loading));

        assignRequestListPresenter = new AssignRequestListPresenter(this, this);

        CommonHelper.settingCustomToolBar(this, "Dealer XMart Price");
        championHelper = new ChampionHelper(getBaseContext());
        enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(ChampionConstants.EVALUATION_DONE);

        EmptyRecyclerView recyclerViewAwaitingReservePrice = findViewById(R.id.recyclerViewAwaitingReservePrice);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());

        recyclerViewAwaitingReservePrice.setLayoutManager(linearLayoutManager);
        recyclerViewAwaitingReservePrice.setEmptyView(findViewById(R.id.textViewEmpty));
        assignRequestAdapter = new AssignRequestAdapter(getBaseContext(), enquiryRequestArrayList,
                this, false);
        assignRequestAdapter.setClickListener(this);
        recyclerViewAwaitingReservePrice.setAdapter(assignRequestAdapter);
    }

    @Override
    public void onClick(View view, int position) {
        enquiryRequest = enquiryRequestArrayList.get(position);
        HashMap<String, String> params = new HashMap<>();
        params.put(WebConstants.GetEvaluationReport.EvaluationReportID, enquiryRequestArrayList
                .get(position).getEvaluationReportID());
        params.put(WebConstants.GetEvaluationReport.RequestID, enquiryRequestArrayList.
                get(position).getRequestID());
        params.put(WebConstants.GetEvaluationReport.UserID, SessionUserDetails.getInstance().getUserID());
        dialog.show();
        assignRequestListPresenter.downloadEvaluationReport(params);
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.EVALUATION_DONE);
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> _enquiryRequestArrayList) {
        dialog.dismiss();
        switch (requestId) {
            case WebConstants.GetEvaluationReport.REQUEST_CODE:
                String evalId = _enquiryRequestArrayList.get(0).getEvaluationReportID();
                Intent intentEnterReservePrice = new Intent(this,
                        EnterReservePriceActivity.class);
                intentEnterReservePrice.putExtra(ChampionConstants.ASSIGN_REQUEST_MODEL_KEY,
                        enquiryRequest);
                intentEnterReservePrice.putExtra("OptedNewVehicle",
                        _enquiryRequestArrayList.get(0).getOptedNewVehicle());
                intentEnterReservePrice.putExtra("NewEvaluationId", evalId);
                intentEnterReservePrice.putExtra("status", "Completed");
                startActivity(intentEnterReservePrice);
                break;
            case WebConstants.EnquiryRequestList.REQUEST_CODE:
                if (_enquiryRequestArrayList != null && !_enquiryRequestArrayList.isEmpty()) {
                    enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(
                            ChampionConstants.EVALUATION_DONE);
                    this.assignRequestAdapter.updateList(this.enquiryRequestArrayList);
                    assignRequestAdapter.notifyDataSetChanged();
                }
                break;
        }
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        swipeContainer.setRefreshing(false);
        dialog.dismiss();
    }

    @Override
    public void onCallPress(String contactNo) {
        if (contactNo != null && !contactNo.isEmpty()) {
            new DialogsHelper(EnterReservePriceListActivity.this).callDialog(contactNo);
        }
    }

    @Override
    public void onMessagePress(String contactNo) {
        CommonHelper.sendMessage(contactNo, EnterReservePriceListActivity.this);
    }
}