package com.mahindra.mitra20.champion.presenter;

/**
 * Created by user on 8/20/2018.
 */

public interface SubmitProcurementPriceIn {

    void uploadProcurementDetailsSuccess(int requestID, String data);

    void uploadOffloadDetailsSuccess(int requestID, String data);

    void uploadFailed(int requestID, String data);

}