
package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AuctionLIST implements Parcelable {

    private String mITRAId;
    private String mITRAName;
    private String bidPrice;
    private String commissionPrice;
    private String bidingDateTime;
    private String deductedPrice;
    private String phoneNum;

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getDeductedPrice() {
        return deductedPrice;
    }

    public void setDeductedPrice(String deductedPrice) {
        this.deductedPrice = deductedPrice;
    }

    public AuctionLIST() {
    }

    public String getCommissionPrice() {
        return commissionPrice;
    }

    public void setCommissionPrice(String commissionPrice) {
        this.commissionPrice = commissionPrice;
    }

    public String getMITRAId() {
        return mITRAId;
    }

    public void setMITRAId(String mITRAId) {
        this.mITRAId = mITRAId;
    }

    public String getMITRAName() {
        return mITRAName;
    }

    public void setMITRAName(String mITRAName) {
        this.mITRAName = mITRAName;
    }

    public String getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public String getBidingDateTime() {
        return bidingDateTime;
    }

    public void setBidingDateTime(String bidingDateTime) {
        this.bidingDateTime = bidingDateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mITRAId);
        dest.writeString(this.mITRAName);
        dest.writeString(this.bidPrice);
        dest.writeString(this.commissionPrice);
        dest.writeString(this.bidingDateTime);
        dest.writeString(this.deductedPrice);
        dest.writeString(this.phoneNum);
    }

    protected AuctionLIST(Parcel in) {
        this.mITRAId = in.readString();
        this.mITRAName = in.readString();
        this.bidPrice = in.readString();
        this.commissionPrice = in.readString();
        this.bidingDateTime = in.readString();
        this.deductedPrice = in.readString();
        this.phoneNum = in.readString();
    }

    public static final Creator<AuctionLIST> CREATOR = new Creator<AuctionLIST>() {
        @Override
        public AuctionLIST createFromParcel(Parcel source) {
            return new AuctionLIST(source);
        }

        @Override
        public AuctionLIST[] newArray(int size) {
            return new AuctionLIST[size];
        }
    };
}
