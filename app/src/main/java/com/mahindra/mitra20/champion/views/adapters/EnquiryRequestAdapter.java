package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;


/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class EnquiryRequestAdapter extends RecyclerView.Adapter<EnquiryRequestAdapter.ViewHolder> {

    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private Context context;
    private ItemClickListener monClick;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView tvName,tvVehicleName,tvAppointmentDayTime;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            tvName = v.findViewById(R.id.textViewName);
            tvVehicleName = v.findViewById(R.id.textViewVehicleName);
            //tvAppointmentDayTime = (TextView) v.findViewById(R.id.textViewAppointmentDayTime);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (monClick != null) monClick.onClick(view, getAdapterPosition());
        }
    }

    public void setmonClick(ItemClickListener monClick) {
        this.monClick = monClick;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public EnquiryRequestAdapter(Context _context, ArrayList<EnquiryRequest> _enquiryRequestContents) {
        enquiryRequestArrayList = _enquiryRequestContents;
        context = _context;
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EnquiryRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_requests, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvName.setText(enquiryRequestArrayList.get(position).getNameOfEnquiry());

        String strMake = enquiryRequestArrayList.get(position).getMake();
        String strModel = enquiryRequestArrayList.get(position).getModel();
        holder.tvVehicleName.setText(strMake+" "+strModel);

        //holder.tvVehicleName.setText(enquiryRequestArrayList.get(position).getMake() + " " + enquiryRequestArrayList.get(position).getModel());
        //holder.tvAppointmentDayTime.setText(enquiryRequestArrayList.get(position).getDateOfAppointment());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return enquiryRequestArrayList.size();
    }
}