package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.BrokerDetails;
import com.mahindra.mitra20.champion.models.OffloadDetails;
import com.mahindra.mitra20.champion.presenter.SubmitProcurementPriceIn;
import com.mahindra.mitra20.champion.presenter.SubmitProcurementPricePresenter;
import com.mahindra.mitra20.constants.GeneralConstants;
import com.mahindra.mitra20.databinding.ActivityFinalProcurementBinding;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.helper.CommonHelper;

import org.json.JSONObject;

import java.util.List;

public class FinalProcurementAndOffloadActivity extends AppCompatActivity implements View.OnClickListener,
        SubmitProcurementPriceIn {

    private final String RETAIL = "RTS";
    private final String OFFLOAD = "OFS";
    ActivityFinalProcurementBinding binding;
    private SubmitProcurementPricePresenter submitProcurementPricePresenter;
    private AuctionDetail auctionDetailDB;
    private AuctionDetail auctionDetail;
    private ProgressDialog dialog;
    private String CustomerExpectedPrice, modeOfSale, saleOffloadTo, sourceOffIBBTrade;
    private AuctionLIST auctionLIST;
    OffloadDetails offloadDetails;
    boolean submitOffloadDetails;
    BrokerDetails selectedBrokerDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,
                R.layout.activity_final_procurement);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Final Offer And Procurement");

        auctionDetail = getIntent().getParcelableExtra(CommonHelper.EXTRA_AUCTION);
        auctionDetailDB = getIntent().getParcelableExtra(CommonHelper.EXTRA_AUCTION_DB);
        CustomerExpectedPrice = getIntent().getStringExtra(CommonHelper.EXTRA_CUSTOMER_EXPECTED_PRICE);
        String EvaluationStatus = getIntent().getStringExtra("EvaluationStatus");

        List<AuctionLIST> auctionDetailLIST = auctionDetail.getLIST();

        auctionLIST = auctionDetailLIST.get(getHighestPriceIndex(auctionDetailLIST));

        binding.radioGroupResponse.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radioButtonOther) {
                    binding.editTextReasonForLowPrice.setVisibility(View.VISIBLE);
                } else {
                    binding.editTextReasonForLowPrice.setVisibility(View.GONE);
                }
            }
        });
        binding.radioGroupModeOfSales.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (id == R.id.radioRetail) {
                    modeOfSale = RETAIL;
                    TranslateAnimation animate = new TranslateAnimation(0, 0 ,
                            0, 0 - binding.llSaleOffload.getHeight());
                    animate.setDuration(500);
                    animate.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            binding.llSaleOffload.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    binding.llSaleOffload.startAnimation(animate);
                } else {
                    modeOfSale = OFFLOAD;
                    TranslateAnimation animate = new TranslateAnimation(0, 0,
                            0 - binding.llSaleOffload.getHeight(), 0);
                    animate.setDuration(500);
                    binding.llSaleOffload.setVisibility(View.VISIBLE);
                    binding.llSaleOffload.startAnimation(animate);
                }
            }
        });
        binding.radioGroupSaleOffloadTo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (id == R.id.radioCustomer) {
                    saleOffloadTo = "CUS";
                    sourceOffIBBTrade = "NO";
                } else if (id == R.id.radioBroker) {
                    saleOffloadTo = "BRK";
                    sourceOffIBBTrade = "NO";
                    binding.editTextName.setEnabled(false);
                    binding.editTextMobileNo.setEnabled(false);
                    binding.editTextEmailId.setEnabled(false);
                    binding.editTextAddress.setEnabled(false);
                    binding.tvSelectBroker.setVisibility(View.VISIBLE);
                    binding.editTextName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            binding.tvSelectBroker.performLongClick();
                        }
                    });
                } else if (id == R.id.radioMFCW) {
                    selectedBrokerDetails = null;
                    saleOffloadTo = "MFCWIBB";
                    sourceOffIBBTrade = "YES";
                    binding.editTextName.setEnabled(true);
                    binding.editTextMobileNo.setEnabled(true);
                    binding.editTextEmailId.setEnabled(true);
                    binding.editTextAddress.setEnabled(true);
                    binding.tvSelectBroker.setVisibility(View.GONE);
                    binding.editTextName.setOnClickListener(null);
                }
                binding.editTextName.setText("");
                binding.editTextMobileNo.setText("");
                binding.editTextEmailId.setText("");
                binding.editTextAddress.setText("");
            }
        });
        binding.tvSelectBroker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FinalProcurementAndOffloadActivity.this,
                        BrokersListActivity.class);
                startActivityForResult(intent, 200);
            }
        });
        submitProcurementPricePresenter = new SubmitProcurementPricePresenter(
                this, this);

        binding.textViewReportID.setText(auctionDetail.getRequestID());
        binding.textViewVehicleName.setText(auctionDetail.getVehicleName());
        binding.textViewUserName.setText(auctionDetail.getVehOwnerName());
        binding.editTextProcurementPrice.setText(CustomerExpectedPrice);
        binding.editTextProcurementPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                reasonForLowerPriceEnable(editable.toString());
            }
        });
        if (EvaluationStatus != null) {
            if (EvaluationStatus.equalsIgnoreCase("COMP")) {
                binding.tableRowSubmit.setVisibility(View.GONE);
                binding.editTextProcurementPrice.setEnabled(false);
                binding.radioGroupResponse.setVisibility(View.GONE);
            } else {
                reasonForLowerPriceEnable(binding.editTextProcurementPrice.getText().toString());
            }
        } else {
            reasonForLowerPriceEnable(binding.editTextProcurementPrice.getText().toString());
        }

        binding.tableRowSubmit.setOnClickListener(this);
    }

    public void showProgressDialog(String msg) {
        if (null == dialog) {
            dialog = new ProgressDialog(FinalProcurementAndOffloadActivity.this);
        }
        if (null != msg && !msg.isEmpty())
            dialog.setMessage(msg);
        else
            dialog.setMessage("Wait while uploading");
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void reasonForLowerPriceEnable(String str) {
        try {
            if (!str.isEmpty()) {
                if (!CustomerExpectedPrice.isEmpty()) {
                    int iBefore = Integer.parseInt(auctionLIST.getDeductedPrice());
                    int iCurrent = Integer.parseInt(binding.editTextProcurementPrice.getText().toString());

                    if (iBefore > iCurrent) {
                        if (binding.lowerPrice.getVisibility() == View.GONE) {
                            binding.lowerPrice.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (binding.lowerPrice.getVisibility() == View.VISIBLE) {
                            binding.lowerPrice.setVisibility(View.GONE);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getHighestPriceIndex(List<AuctionLIST> list) {
        int highestPrice = 0;
        try {
            int[] listHighestPrice = new int[list.size()];
            try {
                for (int i = 0; i < list.size(); i++) {
                    AuctionLIST auctionLIST = list.get(i);
                    listHighestPrice[i] = Integer.parseInt(auctionLIST.getBidPrice());
                }
                highestPrice = getHighestValueIndex(listHighestPrice);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return highestPrice;
    }

    private int getHighestValueIndex(int[] listHighestPrice) {
        int max = listHighestPrice[0];
        int index = 0;
        for (int i = 1; i < listHighestPrice.length; i++) {
            if (listHighestPrice[i] > max) {
                max = listHighestPrice[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tableRowSubmit) {
            int selectedRadioButton = binding.radioGroupResponse.getCheckedRadioButtonId();
            if (binding.editTextProcurementPrice.getText().toString().isEmpty()) {
                CommonHelper.toast(getString(R.string.enter_procurement_price), this);
            } else if (!validateProcurementPrice(binding.editTextProcurementPrice.getText().toString())) {
                CommonHelper.toast(getString(R.string.enter_min_procurement_price), this);
            } else if (selectedRadioButton == -1 && binding.lowerPrice.getVisibility() == View.VISIBLE) {
                CommonHelper.toast(getString(R.string.select_reason_lower_price), this);
            } else if (binding.radioGroupModeOfSales.getCheckedRadioButtonId() == -1) {
                CommonHelper.toast(getString(R.string.select_mode_of_sales), this);
            } else {
                String reasonForLowerPrice = "";
                try {
                    if (selectedRadioButton != -1 && binding.lowerPrice.getVisibility() == View.VISIBLE) {
                        RadioButton radioButtonReason = binding.getRoot().findViewById(selectedRadioButton);
                        reasonForLowerPrice = radioButtonReason.getText().toString();

                        if (reasonForLowerPrice.equals("Other")) {
                            if (binding.editTextReasonForLowPrice.getText().toString().isEmpty()) {
                                CommonHelper.toast(getResources().getString(R.string.enter_reason_lower_price), this);
                                return;
                            } else {
                                reasonForLowerPrice = binding.editTextReasonForLowPrice.getText().toString();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (modeOfSale.equals(RETAIL)) {
                    showProgressDialog("Submitting procurement price...");
                    submitProcurementPricePresenter.SubmitProcurementPriceToCustomer(
                            auctionDetailDB.getAuctionID(), "",
                            binding.editTextProcurementPrice.getText().toString(),
                            reasonForLowerPrice, modeOfSale);
                } else {
                    if (binding.radioGroupSaleOffloadTo.getCheckedRadioButtonId() == -1) {
                        CommonHelper.toast("Select Sale Offload To", this);
                    } else if (binding.editTextSellingPrice.length() == 0) {
                        CommonHelper.toast("Enter Sales Price", this);
                        binding.editTextSellingPrice.requestFocus();
                    } else if (binding.editTextName.length() == 0) {
                        if (saleOffloadTo.equals("BRK")) {
                            CommonHelper.toast("Select Broker Details", this);
                        } else {
                            CommonHelper.toast("Enter Full Name", this);
                            binding.editTextName.requestFocus();
                        }
                    } else if (binding.editTextMobileNo.length() == 0) {
                        CommonHelper.toast("Enter Mobile Number", this);
                        binding.editTextMobileNo.requestFocus();
                    } else if (binding.editTextAddress.length() == 0) {
                        CommonHelper.toast("Enter Address", this);
                        binding.editTextAddress.requestFocus();
                    } else {
                        submitOffloadDetails = true;
                        showProgressDialog("Submitting procurement price...");
                        offloadDetails = new OffloadDetails();
                        if (saleOffloadTo.equals("BRK")) {
                            if (null == selectedBrokerDetails) {
                                CommonHelper.toast("Select Broker Details", this);
                            } else {
                                offloadDetails.setCustomerID(selectedBrokerDetails.getBrokerID());
                            }
                        } else {
                            offloadDetails.setCustomerID("");
                        }
                        offloadDetails.setAuctionID(auctionDetailDB.getAuctionID());
                        offloadDetails.setEvaluationID(auctionDetail.getAuctionID());
                        offloadDetails.setSaleOffLoadTo(saleOffloadTo);
                        offloadDetails.setSellingOffloadPrice(
                                binding.editTextSellingPrice.getText().toString());
                        offloadDetails.setCustomerName(
                                binding.editTextName.getText().toString());
                        offloadDetails.setCustomerMobile(
                                binding.editTextMobileNo.getText().toString());
                        offloadDetails.setCustomerEmail(
                                binding.editTextEmailId.getText().toString());
                        offloadDetails.setCustomerAddress1(
                                binding.editTextAddress.getText().toString());
                        offloadDetails.setSourceOfOffloadIBBTrade(sourceOffIBBTrade);
                        offloadDetails.setSellingDate(new DateHelper().getCurrentDate());
                        submitProcurementPricePresenter.SubmitProcurementPriceToCustomer(
                                auctionDetailDB.getAuctionID(), "",
                                binding.editTextProcurementPrice.getText().toString(),
                                reasonForLowerPrice, modeOfSale);
                    }
                }
            }
        }
    }

    private boolean validateProcurementPrice(String s) {
        try {
            return Integer.parseInt(s) > GeneralConstants.MINIMUM_PROCUREMENT_PRICE;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void uploadProcurementDetailsSuccess(int requestID, String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            CommonHelper.toast(jsonObject.getString("message"), this);

            if (!submitOffloadDetails) {
                navigateToDashboard();
            } else {
                showProgressDialog("Updating Offload Details");
                submitProcurementPricePresenter.SubmitOffloadDetails(offloadDetails);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadOffloadDetailsSuccess(int requestID, String data) {
        navigateToDashboard();
    }

    @Override
    public void uploadFailed(int requestID, String data) {
        hideProgressDialog();
        CommonHelper.toast(data, this);
    }

    private void navigateToDashboard() {
        hideProgressDialog();
        Intent intentDashboard = new Intent(this, ChampionDashboardActivity.class);
        intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentDashboard);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentDashboard = new Intent(FinalProcurementAndOffloadActivity.this,
                ChampionDashboardActivity.class);
        startActivity(intentDashboard);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 200) {
            if (null != data) {
                selectedBrokerDetails = data.getParcelableExtra("BrokerDetails");

                binding.editTextName.setText(selectedBrokerDetails.getBrokerName());
                binding.editTextMobileNo.setText(selectedBrokerDetails.getBrokerContactNumber());
                binding.editTextEmailId.setText(selectedBrokerDetails.getBrokerEmailID());
                binding.editTextAddress.setText(String.format("%s %s %s",
                        selectedBrokerDetails.getBrokerAddressline1(),
                        selectedBrokerDetails.getBrokerAddressline2(),
                        selectedBrokerDetails.getBrokerAddressline3()));
            }
        }
    }
}