package com.mahindra.mitra20.champion.presenter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.EvaluatorDetailsModel;
import com.mahindra.mitra20.models.SessionUserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EvaluatorDetailsPresenter implements VolleySingleTon.VolleyInteractor {
    private Context context;
    private EvaluatorDetailsIn mEvaluatorDetailsIn;

    public EvaluatorDetailsPresenter(Context context,EvaluatorDetailsIn evaluatorDetailsIn) {
        this.context = context;
        mEvaluatorDetailsIn = evaluatorDetailsIn;
    }

    public void downloadEvaluatorDetails() {
        HashMap<String, String> params = new HashMap<>();
        params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getEmployeeCode());
        VolleySingleTon.getInstance().connectToPostUrl(context, this, com.mahindra.mitra20
                .evaluator.constants.WebConstants.EvaluatorDetails.URL, new JSONObject(params), 14);
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(response);
            String isSuccess = jsonObject.getString("IsSuccessful");
            JSONArray list = jsonObject.getJSONArray("EvaluatorDetails");
            ArrayList<EvaluatorDetailsModel> evaluatorDetails = new ArrayList<>();
            Log.e("Response", "Response is " + isSuccess);
            if (isSuccess.equalsIgnoreCase("1")) {
                try {
                    for (int j = 0; j < list.length(); j++) {
                        EvaluatorDetailsModel evaluatorDetail = gson.fromJson(list.getJSONObject(j)
                                .toString(), EvaluatorDetailsModel.class);
                        evaluatorDetails.add(evaluatorDetail);
                    }
                    mEvaluatorDetailsIn.updateUI(evaluatorDetails);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        mEvaluatorDetailsIn.failed();
    }
}