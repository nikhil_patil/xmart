package com.mahindra.mitra20.champion.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EvaluatorDetailsModel {

    @SerializedName("AssignedRequestCount")
    private String assignedRequestCount;
    @SerializedName("AssignedRequestCountDetails")
    private List<EvaluatorWiseEnquiry> assignedRequestCountDetails = null;
    @SerializedName("CompletedAuctionCount")
    private String completedAuctionCount;
    @SerializedName("CompletedAuctionCountDetails")
    private List<EvaluatorWiseEnquiry> completedAuctionCountDetails = null;
    @SerializedName("CompletedEvaluationCount")
    private String completedEvaluationCount;
    @SerializedName("CompletedEvaluationCountDetails")
    private List<EvaluatorWiseEnquiry> completedEvaluationCountDetails = null;
    @SerializedName("CompletedProcurementCount")
    private String completedProcurementCount;
    @SerializedName("CompletedProcurementCountDetails")
    private List<EvaluatorWiseEnquiry> completedProcurementCountDetails = null;
    @SerializedName("EvaluationPendingCountDetails")
    private List<EvaluatorWiseEnquiry> evaluationPendingCountDetails = null;
    @SerializedName("Evaluator_Contact_Number")
    private String evaluatorContactNumber;
    @SerializedName("Evaluator_ID")
    private String evaluatorID;
    @SerializedName("Evaluator_Name")
    private String evaluatorName;
    @SerializedName("PendingEvaluationCount.")
    private String pendingEvaluationCount;

    public EvaluatorDetailsModel() {
    }

    public String getAssignedRequestCount() {
        return assignedRequestCount;
    }

    public void setAssignedRequestCount(String assignedRequestCount) {
        this.assignedRequestCount = assignedRequestCount;
    }

    public List<EvaluatorWiseEnquiry> getAssignedRequestCountDetails() {
        return assignedRequestCountDetails;
    }

    public void setAssignedRequestCountDetails(List<EvaluatorWiseEnquiry> assignedRequestCountDetails) {
        this.assignedRequestCountDetails = assignedRequestCountDetails;
    }

    public String getCompletedAuctionCount() {
        return completedAuctionCount;
    }

    public void setCompletedAuctionCount(String completedAuctionCount) {
        this.completedAuctionCount = completedAuctionCount;
    }

    public List<EvaluatorWiseEnquiry> getCompletedAuctionCountDetails() {
        return completedAuctionCountDetails;
    }

    public void setCompletedAuctionCountDetails(List<EvaluatorWiseEnquiry> completedAuctionCountDetails) {
        this.completedAuctionCountDetails = completedAuctionCountDetails;
    }

    public String getCompletedEvaluationCount() {
        return completedEvaluationCount;
    }

    public void setCompletedEvaluationCount(String completedEvaluationCount) {
        this.completedEvaluationCount = completedEvaluationCount;
    }

    public List<EvaluatorWiseEnquiry> getCompletedEvaluationCountDetails() {
        return completedEvaluationCountDetails;
    }

    public void setCompletedEvaluationCountDetails(List<EvaluatorWiseEnquiry> completedEvaluationCountDetails) {
        this.completedEvaluationCountDetails = completedEvaluationCountDetails;
    }

    public String getCompletedProcurementCount() {
        return completedProcurementCount;
    }

    public void setCompletedProcurementCount(String completedProcurementCount) {
        this.completedProcurementCount = completedProcurementCount;
    }

    public List<EvaluatorWiseEnquiry> getCompletedProcurementCountDetails() {
        return completedProcurementCountDetails;
    }

    public void setCompletedProcurementCountDetails(List<EvaluatorWiseEnquiry> completedProcurementCountDetails) {
        this.completedProcurementCountDetails = completedProcurementCountDetails;
    }

    public List<EvaluatorWiseEnquiry> getEvaluationPendingCountDetails() {
        return evaluationPendingCountDetails;
    }

    public void setEvaluationPendingCountDetails(List<EvaluatorWiseEnquiry> evaluationPendingCountDetails) {
        this.evaluationPendingCountDetails = evaluationPendingCountDetails;
    }

    public String getEvaluatorContactNumber() {
        return evaluatorContactNumber;
    }

    public void setEvaluatorContactNumber(String evaluatorContactNumber) {
        this.evaluatorContactNumber = evaluatorContactNumber;
    }

    public String getEvaluatorID() {
        return evaluatorID;
    }

    public void setEvaluatorID(String evaluatorID) {
        this.evaluatorID = evaluatorID;
    }

    public String getEvaluatorName() {
        return evaluatorName;
    }

    public void setEvaluatorName(String evaluatorName) {
        this.evaluatorName = evaluatorName;
    }

    public String getPendingEvaluationCount() {
        return pendingEvaluationCount;
    }

    public void setPendingEvaluationCount(String pendingEvaluationCount) {
        this.pendingEvaluationCount = pendingEvaluationCount;
    }
}