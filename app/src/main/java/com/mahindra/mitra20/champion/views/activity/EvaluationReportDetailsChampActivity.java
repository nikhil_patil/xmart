package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.fragment.ViewEvaluationFragment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

public class EvaluationReportDetailsChampActivity extends AppCompatActivity {

    CommonHelper commonHelper;
    NewEvaluation newEvaluation;
    private EvaluatorHelper evaluatorHelper;
    private String draftEvaluationId = "", evaluationReportID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_report_details);

        init();
    }

    private void init() {
        CommonHelper.settingCustomToolBar(this, "Evaluation Report Details");
        try {
            commonHelper = new CommonHelper();
            evaluatorHelper = new EvaluatorHelper(this);

            Intent intent = getIntent();
            draftEvaluationId = intent.getStringExtra("NewEvaluationId");
            evaluationReportID = intent.getStringExtra("EvaluationReportID");

            evaluatorHelper = new EvaluatorHelper(EvaluationReportDetailsChampActivity.this);

            getFromDraft();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getFromDraft() {
        newEvaluation = evaluatorHelper.getEvaluationDetailsById(evaluationReportID, draftEvaluationId);
        if (newEvaluation.getRequestId() != null || newEvaluation.getId() != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(CommonHelper.EXTRA_EVALUATION, newEvaluation);
            bundle.putBoolean(CommonHelper.EXTRA_ARE_IMAGES_ONLINE, true);
            ViewEvaluationFragment viewEvaluationFragment =
                    new ViewEvaluationFragment();
            viewEvaluationFragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.main_container, viewEvaluationFragment).commit();
        }
    }
}