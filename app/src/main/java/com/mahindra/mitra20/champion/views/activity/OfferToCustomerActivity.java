package com.mahindra.mitra20.champion.views.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.HighestBidder;
import com.mahindra.mitra20.champion.models.comparator.HighestBidderComparator;
import com.mahindra.mitra20.champion.presenter.SetOfferPriceIn;
import com.mahindra.mitra20.champion.presenter.SetOfferPricePresenter;
import com.mahindra.mitra20.champion.views.adapters.BidderAdapter;
import com.mahindra.mitra20.databinding.ActivityOfferToCustomerBinding;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class OfferToCustomerActivity extends AppCompatActivity implements View.OnClickListener,
        SetOfferPriceIn, ItemClickListener, RadioGroup.OnCheckedChangeListener {

    private ActivityOfferToCustomerBinding binding;
    private SetOfferPricePresenter setOfferpricePresenter;
    private AuctionDetail auctionDetail;
    private int index, indexCurrentHighestBidder;
    private List<AuctionLIST> listList;
    private String customerResponse = "";
    private ArrayList<HighestBidder> highestBidder = new ArrayList<>();
    private BidderAdapter bidderAdapter;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer_to_customer);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Offer To Customer");

        dialog = new ProgressDialog(OfferToCustomerActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Wait while loading");

        setOfferpricePresenter = new SetOfferPricePresenter(this, this);

        auctionDetail = getIntent().getParcelableExtra(ChampionConstants.SELECTED_AUCTION_DATA);

        listList = auctionDetail.getLIST();

        index = getHighestPriceIndex(listList);
        indexCurrentHighestBidder = getHighestPriceIndex(listList);

        binding.textViewMITRAName.setText(listList.get(index).getMITRAName());
        binding.textViewMITRAPrice.setText(listList.get(index).getDeductedPrice());
        binding.textViewRequestID.setText(auctionDetail.getRequestID());
        binding.textViewCustomerName.setText(auctionDetail.getVehOwnerName());
        binding.textViewVehicleName.setText(auctionDetail.getVehicleName());

        for (int i = 0; i < listList.size(); i++) {

            if (i != index) {
                AuctionLIST auctionLIST = listList.get(i);
                highestBidder.add(new HighestBidder(auctionLIST.getMITRAId(),
                        auctionLIST.getMITRAName(), auctionLIST.getBidPrice(),
                        auctionLIST.getDeductedPrice(), auctionLIST.getCommissionPrice(),
                        auctionLIST.getPhoneNum(), false, false));
            }
        }

        try {
            Collections.sort(highestBidder, new HighestBidderComparator());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addCars24MFCW();

        bidderAdapter = new BidderAdapter(getBaseContext(), highestBidder);
        bidderAdapter.setmonClick(this);
        binding.recyclerViewBidderList.setAdapter(bidderAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        binding.recyclerViewBidderList.setLayoutManager(linearLayoutManager);

        binding.submit.setOnClickListener(this);
        binding.tableRowAccepted.setOnClickListener(this);
        binding.tableRowRejected.setOnClickListener(this);
        binding.tableRowNotInterested.setOnClickListener(this);
        binding.radioButtonAggreed.setOnClickListener(this);
        binding.textViewCall.setOnClickListener(this);
        binding.imageViewHighestInfo.setOnClickListener(this);
        binding.radioGroupProcure.setOnCheckedChangeListener(this);
        binding.radioGroupBidderResponse.setOnCheckedChangeListener(this);
    }

    private void addCars24MFCW() {
        try {
            ArrayList<String> arrayListMITRANames = new ArrayList<>();
            for (int i = 0; i < listList.size(); i++) {
                AuctionLIST auctionLIST = listList.get(i);
                arrayListMITRANames.add(auctionLIST.getMITRAId().toUpperCase());
            }

            int impaneledCount = 0;
            for (int i = 0; i < listList.size(); i++) {
                AuctionLIST auctionLIST = listList.get(i);
                if (!auctionLIST.getMITRAId().equalsIgnoreCase("MFCW")
                        && !auctionLIST.getMITRAId().equalsIgnoreCase("BRKA002436")
                        && !auctionLIST.getMITRAId().equalsIgnoreCase(
                        SessionUserDetails.getInstance().getUserID())) {
                    impaneledCount++;
                }
            }

            if (impaneledCount == 0) {
                highestBidder.add(new HighestBidder("Empanelled Associate",
                        "Empanelled Associate", "--", "--",
                        "--", "", false, false));
            }

            if (!arrayListMITRANames.contains("BRKA002436")) {
                highestBidder.add(new HighestBidder("Cars24", "Cars24",
                        "", "--", "--", "",
                        false, false));
            }

            if (!arrayListMITRANames.contains("MFCW")) {
                if (auctionDetail.getNegotiatorContactName().isEmpty()) {
                    highestBidder.add(new HighestBidder(ChampionConstants.MFCW_ID,
                            "Mahindra First Choice", "", "--",
                            "--", auctionDetail.getNegotiatorContactNumber(),
                            false, false));
                } else {
                    highestBidder.add(new HighestBidder(ChampionConstants.MFCW_ID,
                            "Mahindra First Choice (" +
                                    auctionDetail.getNegotiatorContactName() + ")",
                            "", "--", "--",
                            auctionDetail.getNegotiatorContactNumber(), false, false));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getHighestPriceIndex(List<AuctionLIST> list) {
        int highestPrice = 0;
        try {
            int[] listhighestPrice = new int[list.size()];
            try {
                for (int i = 0; i < list.size(); i++) {
                    AuctionLIST auctionLIST = list.get(i);
                    listhighestPrice[i] = Integer.parseInt(auctionLIST.getBidPrice());
                }

                highestPrice = getHighestValueIndex(listhighestPrice);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return highestPrice;
    }

    private void resetCustomerResponse() {
        binding.imageViewNotInterested.setImageDrawable(getResources().getDrawable(R.mipmap.ic_not_interested));
        binding.imageViewRejected.setImageDrawable(getResources().getDrawable(R.mipmap.ic_rejected));
        binding.imageViewAccepted.setImageDrawable(getResources().getDrawable(R.mipmap.ic_accepted));
    }

    private int getHighestValueIndex(int[] listhighestPrice) {
        int max = listhighestPrice[0];
        int index = 0;

        for(int i = 1; i < listhighestPrice.length;i++) {
            if(listhighestPrice[i] > max) {
                max = listhighestPrice[i];
                index = i;
            }
        }

        return index;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                int radioButtonId = binding.radioGroupBidderResponse.getCheckedRadioButtonId();
                RadioButton radioButtonHighestBidderResponse = findViewById(radioButtonId);

                if (customerResponse.isEmpty() || customerResponse.equalsIgnoreCase("")) {
                    CommonHelper.toast("Please select Customer Response", getBaseContext());
                } else if (binding.editTextCustomerExpectedPrice.getText().toString().isEmpty()) {
                    CommonHelper.toast("Please enter Customer Expected Price", getBaseContext());
                } else {
                    String highestBidderResponse = "";
                    if (radioButtonHighestBidderResponse.getText().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.agree_to_procure))) {

                        highestBidderResponse = "6";
                    } else if (radioButtonHighestBidderResponse.getText().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.refuse_to_match))) {
                        highestBidderResponse = "7";
                    } else if (radioButtonHighestBidderResponse.getText().toString()
                            .equalsIgnoreCase(getResources().getString(R.string.backed_out))) {

                        highestBidderResponse = "8";
                    }

                    if (!highestBidderResponse.equalsIgnoreCase("6")
                            && !isClickPreferenceExist()) {
                        new AlertDialog.Builder(OfferToCustomerActivity.this)
                                .setIcon(R.mipmap.ic_launcher).setTitle("Bidder Selection")
                                .setMessage(getApplicationContext().getResources().getString(
                                        R.string.select_bidder_confirmation))
                                .setNegativeButton("Ok", null).show();
                    } else {
                        setOfferPriceToCustomer(highestBidderResponse);
                    }
                }
                break;
            case R.id.tableRowAccepted:
                resetCustomerResponse();
                binding.imageViewAccepted.setImageDrawable(getResources().getDrawable(
                        R.mipmap.ic_not_interested_selected));
                binding.radioGroupProcure.setVisibility(View.VISIBLE);
                customerResponse = getResources().getString(R.string.accepted);

                binding.editTextCustomerExpectedPrice.setText("");
                binding.editTextCustomerExpectedPrice.setVisibility(View.VISIBLE);
                binding.submitTitle.setText(getString(R.string.proceed_to_procurement));
                binding.arrow.setVisibility(View.VISIBLE);
                break;
            case R.id.tableRowRejected:
                resetCustomerResponse();
                binding.imageViewRejected.setImageDrawable(getResources().getDrawable(
                        R.mipmap.ic_not_interested_selected));
                binding.radioGroupProcure.setVisibility(View.INVISIBLE);
                customerResponse = getResources().getString(R.string.rejected);

                binding.editTextCustomerExpectedPrice.setText("0");
                binding.editTextCustomerExpectedPrice.setVisibility(View.GONE);
                binding.submitTitle.setText(getString(R.string.ok));
                binding.arrow.setVisibility(View.GONE);
                break;
            case R.id.tableRowNotInterested:
                resetCustomerResponse();
                binding.imageViewNotInterested.setImageDrawable(getResources().getDrawable(
                        R.mipmap.ic_not_interested_selected));
                binding.radioGroupProcure.setVisibility(View.INVISIBLE);
                customerResponse = getResources().getString(R.string.not_interested);

                binding.editTextCustomerExpectedPrice.setText("0");
                binding.editTextCustomerExpectedPrice.setVisibility(View.GONE);
                binding.submitTitle.setText(getString(R.string.ok));
                binding.arrow.setVisibility(View.GONE);
                break;
            case R.id.radioButtonAggreed:
                index = getHighestPriceIndex(listList);
                break;
            case R.id.textViewCall:
                if (listList.get(index).getPhoneNum() != null && !listList.get(index).getPhoneNum().isEmpty()) {
                    new DialogsHelper(OfferToCustomerActivity.this).callDialog(listList.get(index).getPhoneNum());
                } else {
                    CommonHelper.toast("Contact details unavailable", getBaseContext());
                }
                break;
            case R.id.imageViewInfo:
                DialogsHelper dialogsHelper = new DialogsHelper(OfferToCustomerActivity.this);
                List<AuctionLIST> auctionDetailLIST = auctionDetail.getLIST();

                if (auctionDetailLIST.get(index).getMITRAId().equalsIgnoreCase(
                        SessionUserDetails.getInstance().getUserID())) {
                    CommonHelper.toast(getResources().getString(R.string.no_commission_breakdown), getApplicationContext());
                } else {
                    dialogsHelper.showCommissionDialog(auctionDetailLIST.get(index).getBidPrice(),
                            auctionDetailLIST.get(index).getCommissionPrice());
                }
                break;
            case R.id.imageViewHighestInfo:
                dialogsHelper = new DialogsHelper(OfferToCustomerActivity.this);
                auctionDetailLIST = auctionDetail.getLIST();

                if (auctionDetailLIST.get(indexCurrentHighestBidder).getMITRAId().equalsIgnoreCase(
                        SessionUserDetails.getInstance().getUserID())) {
                    CommonHelper.toast(getResources().getString(R.string.no_commission_breakdown), getApplicationContext());
                } else {
                    dialogsHelper.showCommissionDialog(
                            auctionDetailLIST.get(indexCurrentHighestBidder).getBidPrice(),
                            auctionDetailLIST.get(indexCurrentHighestBidder).getCommissionPrice());
                }
                break;
        }
    }

    private void setOfferPriceToCustomer(String highestBidderResponse) {
        String procureBy = "";

        if (customerResponse.equalsIgnoreCase(getResources().getString(R.string.accepted))) {
            procureBy = listList.get(index).getMITRAId();
        }

        HashMap<String, String> hashMapParams = new HashMap<>();
        hashMapParams.put(WebConstants.SetOfferprice.RequestID, auctionDetail.getRequestID() + "||"
                + getActualMitraID(listList.get(indexCurrentHighestBidder).getMITRAId()));
        hashMapParams.put(WebConstants.SetOfferprice.CustomerResponse, customerResponse);
        hashMapParams.put(WebConstants.SetOfferprice.OfferPrice, listList.get(index).getDeductedPrice());
        hashMapParams.put(WebConstants.SetOfferprice.CustomerExpectedPrice,
                binding.editTextCustomerExpectedPrice.getText().toString());
        hashMapParams.put(WebConstants.SetOfferprice.HighestBidderResponse, highestBidderResponse);
        hashMapParams.put(WebConstants.SetOfferprice.ProcuredBy, procureBy);

        setOfferpricePresenter.setOfferPriceToCustomer(hashMapParams);
    }

    private String getActualMitraID(String mitraId) {
        if (mitraId.equalsIgnoreCase("MFCW")) {
            return SessionUserDetails.getInstance().getUserID();
        } else {
            return mitraId;
        }
    }

    @Override
    public void uploadSuccess(int requestID, String data) {
        if (requestID == WebConstants.SetOfferprice.REQUEST_CODE) {
            setOfferpricePresenter.getAuctionMasterByID(auctionDetail.getAuctionID());
        }
    }

    @Override
    public void uploadSuccess(int requestID, AuctionDetail auctionDetail) {
        if (requestID == WebConstants.LiveAuctionMasterByID.REQUEST_CODE) {
            dialog.hide();
            int id = binding.radioGroupProcure.getCheckedRadioButtonId();

            ChampionHelper championHelper = new ChampionHelper(this);

            championHelper.updateEnquiryStatus(ChampionConstants.OFFER_PRICE,
                    auctionDetail.getRequestID(), auctionDetail.getEvaluationReportID());
            championHelper.updateEnquiryCustomerResponse(customerResponse,
                    auctionDetail.getRequestID(), auctionDetail.getEvaluationReportID());
            championHelper.updateCustomerExpectedPrice(
                    binding.editTextCustomerExpectedPrice.getText().toString(),
                    auctionDetail.getRequestID(), auctionDetail.getEvaluationReportID());

            if (id == R.id.radioButtonNow && customerResponse.equalsIgnoreCase(getResources().getString(R.string.accepted))) {
                Intent intent = new Intent(this, FinalProcurementAndOffloadActivity.class);
                intent.putExtra(CommonHelper.EXTRA_AUCTION, auctionDetail);
                intent.putExtra(CommonHelper.EXTRA_CUSTOMER_EXPECTED_PRICE,
                        binding.editTextCustomerExpectedPrice.getText().toString());
                intent.putExtra(CommonHelper.EXTRA_AUCTION_DB, this.auctionDetail);
                startActivity(intent);
            } else if (id == R.id.radioButtonLater || customerResponse.equalsIgnoreCase(getResources().getString(R.string.rejected))
                    || customerResponse.equalsIgnoreCase(getResources().getString(R.string.not_interested))) {
                finish();
                Intent intent = new Intent(this, ChampionDashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }
    }

    @Override
    public void downloadSuccess(int requestID, NewEvaluation newEvaluation) {

    }

    @Override
    public void uploadFailed(int requestID, String data) {
        dialog.hide();
        CommonHelper.toast(data, getBaseContext());
    }

    @Override
    public void onClick(View view, int position) {
        int id = view.getId();
        if (id == R.id.tableRowData) {
            int radioButtonId = binding.radioGroupBidderResponse.getCheckedRadioButtonId();
            if (radioButtonId == R.id.radioButtonRefused || radioButtonId == R.id.radioButtonBackedOut) {
                List<AuctionLIST> auctionDetailLIST = auctionDetail.getLIST();
                CommonHelper.toast(auctionDetailLIST.get(position).getMITRAName() + "("
                        + auctionDetailLIST.get(position).getMITRAId() + ") "
                        + getResources().getString(R.string.winner_selected), getApplicationContext());
            }
        } else if (id == R.id.imageViewInfo) {
            DialogsHelper dialogsHelper = new DialogsHelper(OfferToCustomerActivity.this);
            if (highestBidder.get(position).getMITRAID().equalsIgnoreCase(SessionUserDetails.getInstance().getUserID())) {
                CommonHelper.toast(getResources().getString(R.string.no_commission_breakdown), getApplicationContext());
            } else {
                dialogsHelper.showCommissionDialog(highestBidder.get(position).getStrBiddingCost(),
                        highestBidder.get(position).getStrCommissionCost());
            }
        } else if (id == R.id.textViewCall) {
            if (highestBidder.get(position).getContactNo() != null
                    && !highestBidder.get(position).getContactNo().isEmpty()) {
                new DialogsHelper(OfferToCustomerActivity.this).callDialog(highestBidder.get(position).getContactNo());
            } else {
                CommonHelper.toast("Contact details unavailable", getApplicationContext());
            }
        }
    }

    @Override
    public void onLongClick(View view, int position) {
        int radioButtonId = binding.radioGroupBidderResponse.getCheckedRadioButtonId();
        RadioButton radioButtonHighestBidderResponse = findViewById(radioButtonId);
        if (radioButtonHighestBidderResponse.getText().toString().equalsIgnoreCase(getResources().getString(R.string.refuse_to_match))
                || radioButtonHighestBidderResponse.getText().toString().equalsIgnoreCase(getResources().getString(R.string.backed_out))) {
            if (view.getId() == R.id.tableRowData) {
                if (!this.highestBidder.get(position).getStrBiddingCost().equalsIgnoreCase("--")) {
                    resetClickPreference();
                    HighestBidder highestBidder = this.highestBidder.get(position);
                    LinearLayout tableRowData = view.findViewById(view.getId());

                    if ((int) (tableRowData.getTag()) == 1) {

                        resetClickPreference();
                    } else {
                        highestBidder.setSelected(true);
                        this.index = getIndexByID(this.highestBidder.get(position));
                    }

                    bidderAdapter.notifyDataSetChanged();
                } else {
                    CommonHelper.toast(getResources().getString(R.string.bid_unavailable), getApplicationContext());
                }
            }
        }
    }

    private int getIndexByID(HighestBidder highestBidder) {
        for (int i = 0; i < listList.size(); i++) {
            AuctionLIST auctionLIST = listList.get(i);

            if (auctionLIST.getMITRAId().equalsIgnoreCase(highestBidder.getMITRAID())) {
                return i;
            }
        }

        return index;
    }

    private void resetClickPreference() {
        for (int i = 0; i < this.highestBidder.size(); i++) {
            HighestBidder highestBidder = this.highestBidder.get(i);
            highestBidder.setSelected(false);
        }

        index = getHighestPriceIndex(listList);
        bidderAdapter.notifyDataSetChanged();
    }

    private boolean isClickPreferenceExist() {
        for (int i = 0; i < this.highestBidder.size(); i++) {
            HighestBidder highestBidder = this.highestBidder.get(i);
            if (highestBidder.isSelected()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        int radioSelection = radioGroup.getCheckedRadioButtonId();
        switch (radioSelection) {
            case R.id.radioButtonNow:
                binding.submitTitle.setText(getResources().getString(R.string.proceed_to_procurement));
                binding.arrow.setVisibility(View.VISIBLE);
                break;
            case R.id.radioButtonLater:
                binding.submitTitle.setText(getResources().getString(R.string.ok));
                binding.arrow.setVisibility(View.GONE);
                break;
            case R.id.radioButtonAggreed:
                resetClickPreference();
                callButtonEnable(false);
                break;
            case R.id.radioButtonRefused:
            case R.id.radioButtonBackedOut:
                callButtonEnable(true);
                break;
        }
    }

    private void callButtonEnable(boolean isEnable) {
        for (int i = 0; i < highestBidder.size(); i++) {
            HighestBidder highestBidder = this.highestBidder.get(i);
            highestBidder.setCallEnable(isEnable);
        }
        bidderAdapter.notifyDataSetChanged();
    }
}