package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.views.adapters.ChampionCancelledAppointmentAdapter;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.interfaces.ItemClickListener;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.models.UpcomingAppointmentModel;
import com.mahindra.mitra20.evaluator.presenter.AssignedRequestListIn;
import com.mahindra.mitra20.evaluator.presenter.AssignedRequestListPresenter;
import com.mahindra.mitra20.evaluator.presenter.UpcomingRequestIn;
import com.mahindra.mitra20.evaluator.views.activity.EvaluatorDashboardActivity;
import com.mahindra.mitra20.evaluator.views.adapters.EvaluationOverdueAdapter;
import com.mahindra.mitra20.evaluator.views.adapters.ScheduleAppointmentAdapter;
import com.mahindra.mitra20.evaluator.views.adapters.UpcomingAppointmentAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import java.util.ArrayList;

public class ChampionScheduleAppointmentActivity extends AppCompatActivity implements View.OnClickListener, ItemClickListener, SwipeRefreshLayout.OnRefreshListener,
        AssignedRequestListIn, UpcomingRequestIn {

    TextView textViewUpcoming, textViewScheduleForTheDay, textViewOverdue, textViewCancellation;
    private int[] textView_idAppointment = {R.id.textViewUpcomingLine, R.id.textViewScheduleForTheDayLine, R.id.textViewOverdueLine, R.id.textViewCancellationLine};
    private TextView[] textViewAppointment = new TextView[4];
    TextView textView_unfocusAppointment;
    EvaluatorHelper evaluatorHelper;
    EmptyRecyclerView recyclerViewScheduledVisit;
    LinearLayout linearLayoutUpcoming, linearLayoutScheduleForTheDay, linearLayoutOverdue, linearLayoutCancellation;

    TextView textViewAppoinmentType;
    String appoinmentType = "Today";
    SwipeRefreshLayout swipeContainer;
    ScheduleAppointmentAdapter scheduleAppointmentAdapter;
    private AssignedRequestListPresenter assignedRequestListPresenter;
    ArrayList<ScheduleAppoinment> todaysscheduleVisitArrayList;
    ArrayList<ScheduleAppoinment> upcomingAppointmentModelArrayList;
    ArrayList<EnquiryRequest> cancelledAppointmentModelArrayList;
    ArrayList<ScheduleAppoinment> overdueAppointmentArrayList;
    UpcomingAppointmentAdapter upcomingAppoinmentAdapter;
    ChampionCancelledAppointmentAdapter cancelledAppoinmentAdapter;
    private ChampionHelper championHelper;
    EvaluationOverdueAdapter overdueAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_champion_schedule_appointment);

        evaluatorHelper = new EvaluatorHelper(getBaseContext());

        assignedRequestListPresenter = new AssignedRequestListPresenter(this, this);

        init();
    }

    private void init() {

        if (getIntent().getStringExtra("appointmentType") != null)
            appoinmentType = getIntent().getStringExtra("appointmentType");

        CommonHelper.settingCustomToolBarEvaluator(this, "Appointment", View.INVISIBLE);

        textViewAppoinmentType = findViewById(R.id.textViewAppoinmentType);
        linearLayoutUpcoming = findViewById(R.id.linearLayoutUpcoming);
        linearLayoutScheduleForTheDay = findViewById(R.id.linearLayoutScheduleForTheDay);
        linearLayoutOverdue = findViewById(R.id.linearLayoutOverdue);

        linearLayoutCancellation = findViewById(R.id.linearLayoutCancellation);
        textViewCancellation = findViewById(R.id.textViewCancellation);

        linearLayoutUpcoming.setOnClickListener(this);
        linearLayoutScheduleForTheDay.setOnClickListener(this);
        linearLayoutOverdue.setOnClickListener(this);
        linearLayoutCancellation.setOnClickListener(this);

        championHelper = new ChampionHelper(getApplicationContext());
        todaysscheduleVisitArrayList = evaluatorHelper.getScheduleAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        upcomingAppointmentModelArrayList = evaluatorHelper.getUpcomingAppointmentDetails(EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        cancelledAppointmentModelArrayList = championHelper.getEnquiryRequestDetails(ChampionConstants.CANCELLATION_DONE);
        textViewUpcoming = findViewById(R.id.textViewUpcoming);
        textViewScheduleForTheDay = findViewById(R.id.textViewScheduleForTheDay);
        textViewOverdue = findViewById(R.id.textViewOverdue);

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);

        for (int i = 0; i < textViewAppointment.length; i++) {
            textViewAppointment[i] = findViewById(textView_idAppointment[i]);
            textViewAppointment[i].setBackgroundColor(getResources().getColor(R.color.colorGrey));
        }
        textView_unfocusAppointment = textViewAppointment[0];

        switch (appoinmentType) {
            case "Today":
                textViewAppoinmentType.setText("Todays Scheduled Visits");
                todaysScheduleVisit();
                setFocus(textView_unfocusAppointment, textViewAppointment[1]);
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorText));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;
            case "Pending":
                textViewAppoinmentType.setText("Overdue Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[2]);
                overdueScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorText));
                break;
            case "Upcoming":
                textViewAppoinmentType.setText("Upcoming Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[0]);
                upcomingScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorText));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;
            case "Cancellation":

                textViewAppoinmentType.setText("Cancelled Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[3]);
                // overdueAppointments();
                cancelledScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewCancellation.setTextColor(getResources().getColor(R.color.colorText));

                break;
        }
    }

    public void todaysScheduleVisit() {
        findViewById(R.id.list_empty_pending).setVisibility(View.GONE);
        findViewById(R.id.list_empty_upcoming).setVisibility(View.GONE);
        findViewById(R.id.list_empty_cancel).setVisibility(View.GONE);
        recyclerViewScheduledVisit = findViewById(R.id.recyclerViewScheduledVisit);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewScheduledVisit.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewScheduledVisit.setEmptyView(findViewById(R.id.list_empty));
        if (todaysscheduleVisitArrayList.size() > 0) {
            textViewAppoinmentType.setVisibility(View.VISIBLE);
        } else {
            textViewAppoinmentType.setVisibility(View.GONE);
        }
        scheduleAppointmentAdapter = new ScheduleAppointmentAdapter(this, todaysscheduleVisitArrayList);
        scheduleAppointmentAdapter.setClickListener(this);
        recyclerViewScheduledVisit.setAdapter(scheduleAppointmentAdapter);
    }

    public void overdueScheduleVisit() {
        findViewById(R.id.list_empty).setVisibility(View.GONE);
        findViewById(R.id.list_empty_upcoming).setVisibility(View.GONE);
        findViewById(R.id.list_empty_cancel).setVisibility(View.GONE);
        recyclerViewScheduledVisit = findViewById(R.id.recyclerViewScheduledVisit);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewScheduledVisit.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewScheduledVisit.setEmptyView(findViewById(R.id.list_empty_pending));
        overdueAppointmentArrayList = evaluatorHelper.getOverdueAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        if (overdueAppointmentArrayList.size() > 0) {
            textViewAppoinmentType.setVisibility(View.VISIBLE);

        } else {
            textViewAppoinmentType.setVisibility(View.GONE);

        }
        overdueAdapter = new EvaluationOverdueAdapter(this, overdueAppointmentArrayList, "Overdue", "");
        recyclerViewScheduledVisit.setAdapter(new EvaluationOverdueAdapter(this, evaluatorHelper.getOverdueAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS), "Overdue", ""));
    }

    public void upcomingScheduleVisit() {
        findViewById(R.id.list_empty).setVisibility(View.GONE);
        findViewById(R.id.list_empty_pending).setVisibility(View.GONE);
        findViewById(R.id.list_empty_cancel).setVisibility(View.GONE);
        recyclerViewScheduledVisit = findViewById(R.id.recyclerViewScheduledVisit);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewScheduledVisit.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewScheduledVisit.setEmptyView(findViewById(R.id.list_empty_upcoming));
        if (upcomingAppointmentModelArrayList.size() > 0) {
            textViewAppoinmentType.setVisibility(View.VISIBLE);
        } else {
            textViewAppoinmentType.setVisibility(View.GONE);
        }
        upcomingAppoinmentAdapter = new UpcomingAppointmentAdapter(this, upcomingAppointmentModelArrayList, "Upcoming");
        recyclerViewScheduledVisit.setAdapter(upcomingAppoinmentAdapter);
    }

    public void cancelledScheduleVisit() {
        findViewById(R.id.list_empty).setVisibility(View.GONE);
        findViewById(R.id.list_empty_pending).setVisibility(View.GONE);
        findViewById(R.id.list_empty_upcoming).setVisibility(View.GONE);
        recyclerViewScheduledVisit = findViewById(R.id.recyclerViewScheduledVisit);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewScheduledVisit.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewScheduledVisit.setEmptyView(findViewById(R.id.list_empty_cancel));
        if (cancelledAppointmentModelArrayList.size() > 0) {
            textViewAppoinmentType.setVisibility(View.VISIBLE);
        } else {
            textViewAppoinmentType.setVisibility(View.GONE);
        }

        cancelledAppoinmentAdapter = new ChampionCancelledAppointmentAdapter(this, cancelledAppointmentModelArrayList, "Cancel");
        recyclerViewScheduledVisit.setAdapter(cancelledAppoinmentAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearLayoutUpcoming:
                textViewAppoinmentType.setText("Upcoming Schedule Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[0]);
                // upcomingAppointents();
                upcomingScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorText));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewCancellation.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;

            case R.id.linearLayoutScheduleForTheDay:
                textViewAppoinmentType.setText("Todays Scheduled Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[1]);

                todaysScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorText));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewCancellation.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;

            case R.id.linearLayoutOverdue:
                textViewAppoinmentType.setText("Overdue Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[2]);
                // overdueAppointments();
                overdueScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorText));
                textViewCancellation.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;

            case R.id.linearLayoutCancellation:
                textViewAppoinmentType.setText("Cancelled Visits");
                setFocus(textView_unfocusAppointment, textViewAppointment[3]);
                // overdueAppointments();
                cancelledScheduleVisit();
                textViewUpcoming.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewScheduleForTheDay.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewOverdue.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewCancellation.setTextColor(getResources().getColor(R.color.colorText));
                break;
        }
    }

    private void setFocus(TextView textView_unfocus, TextView textView_focus) {
        textView_unfocus.setBackgroundColor(getResources().getColor(R.color.colorGrey));
        textView_focus.setBackgroundColor(getResources().getColor(R.color.colorRed));
        this.textView_unfocusAppointment = textView_focus;
    }

    @Override
    public void onClick(View view, int position) {


    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            //if(textViewAppoinmentType.getText().toString().equals("Todays Scheduled Visits")) {
            assignedRequestListPresenter.downloadEnquiryMaster();
            //  }
            /*else if(textViewAppoinmentType.getText().toString().equals("Upcoming Schedule Visits")) {
                upcomingRequestListPresenter.downloadUpcomingRequestMaster();
            }*/
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<ScheduleAppoinment> _scheduleAppointmentArrayList) {
        /*switch (RequestId) {
            case 1:
                todaysscheduleVisitArrayList = evaluatorHelper.getScheduleAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                this.scheduleAppointmentAdapter.updateList(this.todaysscheduleVisitArrayList);
                scheduleAppointmentAdapter.notifyDataSetChanged();
                //recyclerViewAssignRequest.invalidate();
                Log.d("--", "data");
                break;
        }
        swipeContainer.setRefreshing(false);*/
        switch (requestId) {
            case WebConstants.ScheduleAppoinmentList.REQUEST_CODE:
                if (appoinmentType.equalsIgnoreCase("Today")) {
                    todaysscheduleVisitArrayList = evaluatorHelper.getScheduleAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                    this.scheduleAppointmentAdapter.updateList(this.todaysscheduleVisitArrayList);
                    scheduleAppointmentAdapter.notifyDataSetChanged();
                    //recyclerViewAssignRequest.invalidate();
                    Log.d("--", "data");
                } else if (appoinmentType.equalsIgnoreCase("Pending")) {
                    overdueAppointmentArrayList = evaluatorHelper.getOverdueAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                    this.overdueAdapter.updateList(this.overdueAppointmentArrayList);
                    overdueAdapter.notifyDataSetChanged();
                    //recyclerViewAssignRequest.invalidate();
                    Log.d("--", "data");
                }
                break;
        }
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onUpcomingDownloadSuccess(int requestId, ArrayList<UpcomingAppointmentModel> scheduleAppoinments) {
        switch (requestId) {
            case 1:
                upcomingAppointmentModelArrayList = evaluatorHelper.getUpcomingAppointmentDetails(EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                this.upcomingAppoinmentAdapter.updateList(this.upcomingAppointmentModelArrayList);
                scheduleAppointmentAdapter.notifyDataSetChanged();
                //recyclerViewAssignRequest.invalidate();
                Log.d("--", "data");
                break;
        }

        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onUpcomingDownloadFail(int requestId, String message) {

    }

    @Override
    public void onBackPressed() {
        try {
            //COMMENTED BY PRAVIN DHARAM ON 22-11-2019
//            Intent intent;
//            if (SessionUserDetails.getInstance().getUserType() == 0) {
//                intent = new Intent(this, ChampionDashboardActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            } else {
//                intent = new Intent(this, EvaluatorDashboardActivity.class);
//            }
//
//ADDED BY PRAVIN DHARAM ON 22-11-2019
            if (SessionUserDetails.getInstance().getUserType() == 0) {
                super.onBackPressed();
            } else {
                startActivity(new Intent(this, EvaluatorDashboardActivity.class));
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
