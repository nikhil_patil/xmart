package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.EnquiryRequest;

import org.json.JSONObject;

/**
 * Created by user on 9/10/2018.
 */

public class CancelRequestPresenter implements VolleySingleTon.VolleyInteractor {

    CancelRequestIn cancelRequestIn;
    Context context;

    public CancelRequestPresenter(CancelRequestIn placeBidIn, Context context) {
        this.context = context;
        this.cancelRequestIn = placeBidIn;
    }

    public void cancelRequest(EnquiryRequest enquiryRequest, String status) {
        try {

            JSONObject params = new JSONObject();
            params.put(WebConstants.CancelAppointmentApproval.RequestID, enquiryRequest.getRequestID());
            params.put(WebConstants.CancelAppointmentApproval.EvaluatorID, enquiryRequest.getEvaluatorID());
            params.put(WebConstants.CancelAppointmentApproval.ApprovalStatus, status);

            VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.CancelAppointmentApproval.URL, params, WebConstants.CancelAppointmentApproval.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (WebConstants.CancelAppointmentApproval.REQUEST_CODE == requestId) {

                JSONObject jsonObject = new JSONObject(response);
                cancelRequestIn.uploadSuccess(WebConstants.PlaceBid.REQUEST_CODE, jsonObject.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            cancelRequestIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {

        }
    }
}