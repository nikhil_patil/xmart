package com.mahindra.mitra20.champion.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BrokerDetails implements Parcelable {

    private String BrokerEmailID;
    private String BrokerID;
    private String BrokerAddressline3;
    @SerializedName("BrokerContactNumber ")
    private String BrokerContactNumber;
    private String BrokerPinCode;
    private String BrokerWhatsAppNumber;
    private String ActiveFlag;
    private String BrokerAddressline2;
    private String BrokerName;
    private String BrokerAddressline1;

    public BrokerDetails(Parcel in) {
        BrokerEmailID = in.readString();
        BrokerID = in.readString();
        BrokerAddressline3 = in.readString();
        BrokerContactNumber = in.readString();
        BrokerPinCode = in.readString();
        BrokerWhatsAppNumber = in.readString();
        ActiveFlag = in.readString();
        BrokerAddressline2 = in.readString();
        BrokerName = in.readString();
        BrokerAddressline1 = in.readString();
    }

    public BrokerDetails() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(BrokerEmailID);
        dest.writeString(BrokerID);
        dest.writeString(BrokerAddressline3);
        dest.writeString(BrokerContactNumber);
        dest.writeString(BrokerPinCode);
        dest.writeString(BrokerWhatsAppNumber);
        dest.writeString(ActiveFlag);
        dest.writeString(BrokerAddressline2);
        dest.writeString(BrokerName);
        dest.writeString(BrokerAddressline1);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BrokerDetails> CREATOR = new Creator<BrokerDetails>() {
        @Override
        public BrokerDetails createFromParcel(Parcel in) {
            return new BrokerDetails(in);
        }

        @Override
        public BrokerDetails[] newArray(int size) {
            return new BrokerDetails[size];
        }
    };

    public String getBrokerEmailID() {
        return BrokerEmailID;
    }

    public void setBrokerEmailID(String BrokerEmailID) {
        this.BrokerEmailID = BrokerEmailID;
    }

    public String getBrokerID() {
        return BrokerID;
    }

    public void setBrokerID(String BrokerID) {
        this.BrokerID = BrokerID;
    }

    public String getBrokerAddressline3() {
        return (null != BrokerAddressline3) ? BrokerAddressline3 : "";
    }

    public void setBrokerAddressline3(String BrokerAddressline3) {
        this.BrokerAddressline3 = BrokerAddressline3;
    }

    public String getBrokerContactNumber() {
        return BrokerContactNumber;
    }

    public void setBrokerContactNumber(String BrokerContactNumber) {
        this.BrokerContactNumber = BrokerContactNumber;
    }

    public String getBrokerPinCode() {
        return BrokerPinCode;
    }

    public void setBrokerPinCode(String BrokerPinCode) {
        this.BrokerPinCode = BrokerPinCode;
    }

    public String getBrokerWhatsAppNumber() {
        return BrokerWhatsAppNumber;
    }

    public void setBrokerWhatsAppNumber(String BrokerWhatsAppNumber) {
        this.BrokerWhatsAppNumber = BrokerWhatsAppNumber;
    }

    public String getActiveFlag() {
        return ActiveFlag;
    }

    public void setActiveFlag(String ActiveFlag) {
        this.ActiveFlag = ActiveFlag;
    }

    public String getBrokerAddressline2() {
        return (null != BrokerAddressline2) ? BrokerAddressline2 : "";
    }

    public void setBrokerAddressline2(String BrokerAddressline2) {
        this.BrokerAddressline2 = BrokerAddressline2;
    }

    public String getBrokerName() {
        return BrokerName;
    }

    public void setBrokerName(String BrokerName) {
        this.BrokerName = BrokerName;
    }

    public String getBrokerAddressline1() {
        return (null != BrokerAddressline1) ? BrokerAddressline1 : "";
    }

    public void setBrokerAddressline1(String BrokerAddressline1) {
        this.BrokerAddressline1 = BrokerAddressline1;
    }
}