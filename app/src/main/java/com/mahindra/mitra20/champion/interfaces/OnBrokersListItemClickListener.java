package com.mahindra.mitra20.champion.interfaces;

import com.mahindra.mitra20.champion.models.BrokerDetails;

/**
 * Created by PATINIK-CONT on 01-Oct-19.
 */
public interface OnBrokersListItemClickListener {
    void onItemClick(BrokerDetails brokerDetails);

    void onCallPressed(BrokerDetails brokerDetails);
}
