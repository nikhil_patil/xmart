package com.mahindra.mitra20.champion.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.mahindra.mitra20.R;

public class UpdateAppHelper {
    private Context context;
    private String url;
    private boolean isMajorUpdate;

    public UpdateAppHelper(Context context, String url, boolean isMajorUpdate) {
        this.context = context;
        this.url = url;
        this.isMajorUpdate = isMajorUpdate;
    }

    public void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.update_dialog_title))
                .setMessage(context.getString(R.string.update_dialog_message))
                .setPositiveButton(context.getString(R.string.update_dialog_button),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((Activity) context).finish();
                                redirectStore(url);
                            }
                        });
        if (!isMajorUpdate) {
            builder.setNegativeButton("No, Thanks", null);
        }
        AlertDialog dialog = builder.create();

        if (isMajorUpdate)
            dialog.setCancelable(false);

        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
