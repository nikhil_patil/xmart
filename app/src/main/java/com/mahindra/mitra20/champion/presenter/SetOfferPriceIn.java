package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;

/**
 * Created by user on 8/20/2018.
 */

public interface SetOfferPriceIn {
    void uploadSuccess(int requestID, String data);

    void uploadSuccess(int requestID, AuctionDetail auctionDetail);

    void downloadSuccess(int requestID, NewEvaluation newEvaluation);

    void uploadFailed(int requestID, String data);
}