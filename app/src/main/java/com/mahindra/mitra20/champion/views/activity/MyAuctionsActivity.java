package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TabHost;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.presenter.MyLiveAuctionIn;
import com.mahindra.mitra20.champion.presenter.MyLiveAuctionPresenter;
import com.mahindra.mitra20.champion.views.adapters.myauctions.CompletedAuctionsAdapter;
import com.mahindra.mitra20.champion.views.adapters.myauctions.LiveAuctionsAdapter;
import com.mahindra.mitra20.champion.views.adapters.myauctions.UpcomingAuctionsAdapter;
import com.mahindra.mitra20.constants.AuctionType;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.utils.AuctionIntervalUtils;

import java.util.ArrayList;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.SELECTED_AUCTION_DATA;

public class MyAuctionsActivity extends AppCompatActivity implements ItemClickListener, SwipeRefreshLayout.OnRefreshListener, MyLiveAuctionIn, ContactIn {

    private ArrayList<AuctionDetail> auctionDetailArrayListLive = new ArrayList<>();
    private ArrayList<AuctionDetail> auctionDetailArrayListUpcoming = new ArrayList<>();
    private ArrayList<AuctionDetail> auctionDetailArrayListCompleted = new ArrayList<>();
    private TabHost tabHost;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MyLiveAuctionPresenter myLiveAuctionPresenter;
    private LiveAuctionsAdapter myAuctionsAdapterLive;
    private CompletedAuctionsAdapter completedAuctionsAdapter;
    private UpcomingAuctionsAdapter upcomingAuctionsAdapter;
    private AuctionIntervalUtils intervalUtils = new AuctionIntervalUtils();
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_auctions);

        initUI();
    }

    protected void initUI() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CommonHelper.settingCustomToolBar(this, "My Auction");
        myLiveAuctionPresenter = new MyLiveAuctionPresenter(this, this);
        String auctionStatus = getIntent().getStringExtra(ChampionConstants.AUCTION_STATUS_KEY);
        EmptyRecyclerView recyclerViewLiveAuctions = findViewById(R.id.recyclerViewLiveAuctions);
        EmptyRecyclerView recyclerViewLiveUpcoming = findViewById(R.id.recyclerViewLiveUpcoming);
        EmptyRecyclerView recyclerViewLiveCompleted = findViewById(R.id.recyclerViewLiveCompleted);

        tabHost = findViewById(R.id.tabhost); // initiate TabHost
        tabHost.setup();

        swipeRefreshLayout = findViewById(R.id.swipeContainer);

        TabHost.TabSpec tabSpecLive = tabHost.newTabSpec("tab1");
        TabHost.TabSpec tabSpecUpcoming = tabHost.newTabSpec("tab2");
        TabHost.TabSpec tabSpecCompleted = tabHost.newTabSpec("tab3");

        tabSpecLive.setIndicator(getResources().getString(R.string.live_auction));
        tabSpecUpcoming.setIndicator(getResources().getString(R.string.upcoming_auction));
        tabSpecCompleted.setIndicator(getResources().getString(R.string.completed_auction));

        tabSpecLive.setContent(R.id.tab1);
        tabSpecUpcoming.setContent(R.id.tab2);
        tabSpecCompleted.setContent(R.id.tab3);

        tabHost.addTab(tabSpecLive);
        tabHost.addTab(tabSpecUpcoming);
        tabHost.addTab(tabSpecCompleted);

        recyclerViewLiveAuctions.setLayoutManager(new LinearLayoutManager(getApplicationContext())); // set LayoutManager to RecyclerView
        recyclerViewLiveUpcoming.setLayoutManager(new LinearLayoutManager(getApplicationContext())); // set LayoutManager to RecyclerView
        recyclerViewLiveCompleted.setLayoutManager(new LinearLayoutManager(getApplicationContext())); // set LayoutManager to RecyclerView

        myAuctionsAdapterLive = new LiveAuctionsAdapter(getBaseContext(), auctionDetailArrayListLive);
        myAuctionsAdapterLive.setmonClick(this);
        myAuctionsAdapterLive.setContactIn(this);
        recyclerViewLiveAuctions.setAdapter(myAuctionsAdapterLive);
        recyclerViewLiveAuctions.setEmptyView(findViewById(R.id.list_empty_draft));

        upcomingAuctionsAdapter = new UpcomingAuctionsAdapter(getBaseContext(), auctionDetailArrayListUpcoming);
        upcomingAuctionsAdapter.setmonClick(this);
        upcomingAuctionsAdapter.setContactIn(this);
        recyclerViewLiveUpcoming.setAdapter(upcomingAuctionsAdapter);
        recyclerViewLiveUpcoming.setEmptyView(findViewById(R.id.list_empty_draft_2));

        completedAuctionsAdapter = new CompletedAuctionsAdapter(getBaseContext(), auctionDetailArrayListCompleted);
        completedAuctionsAdapter.setmonClick(this);
        completedAuctionsAdapter.setContactIn(this);
        recyclerViewLiveCompleted.setAdapter(completedAuctionsAdapter);
        recyclerViewLiveCompleted.setEmptyView(findViewById(R.id.list_empty_draft_3));
        setTabToOpen(auctionStatus);
        swipeRefreshLayout.setOnRefreshListener(this);
        onRefresh();
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                onRefresh();
            }
        });
    }

    private void setTabToOpen(String auctionStatus) {
        switch (auctionStatus) {
            case ChampionConstants.LIVE_STATUS:
                tabHost.setCurrentTab(0);
                break;
            case ChampionConstants.BIDDING_STATUS:
                tabHost.setCurrentTab(1);
                break;
            case ChampionConstants.COMPLETED_STATUS:
                tabHost.setCurrentTab(2);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onClick(View view, int position) {
        if (view.getId() == R.id.imageViewDetails) {
            ArrayList<AuctionDetail> auctionDetailArrayList = null;
            switch (tabHost.getCurrentTab()) {
                case 0:
                    auctionDetailArrayList = auctionDetailArrayListLive;
                    break;
                case 1:
                    auctionDetailArrayList = auctionDetailArrayListUpcoming;
                    break;
                case 2:
                    auctionDetailArrayList = auctionDetailArrayListCompleted;
                    break;
            }
            try {
                Intent intentBidDetails = new Intent(MyAuctionsActivity.this,
                        AuctionBidDetailsActivity.class);
                intentBidDetails.putExtra(SELECTED_AUCTION_DATA, auctionDetailArrayList.get(position));
                intentBidDetails.putExtra("key", tabHost.getCurrentTab());
                startActivity(intentBidDetails);
                //RESET AUCTION INTERVAL VALUES
                intervalUtils.resetValues();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            switch (tabHost.getCurrentTab()) {
                case 0:
                    if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.LIVE)) {
                        myLiveAuctionPresenter.downloadLiveAuctionMaster(ChampionConstants.LIVE_STATUS);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    break;
                case 1:
                    if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.PENDING)) {
                        myLiveAuctionPresenter.downloadLiveAuctionMaster(ChampionConstants.BIDDING_STATUS);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    break;
                case 2:
                    if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.COMPLETED)) {
                        myLiveAuctionPresenter.downloadLiveAuctionMaster(ChampionConstants.COMPLETED_STATUS);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    break;
            }
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<AuctionDetail> enquiryRequestArrayList) {
        if (requestId == WebConstants.LiveAuctionMaster.REQUEST_CODE_LIVE) {
            auctionDetailArrayListLive = enquiryRequestArrayList;
        } else if (requestId == WebConstants.LiveAuctionMaster.REQUEST_CODE_UPCOMING) {
            auctionDetailArrayListUpcoming = enquiryRequestArrayList;
        } else {
            auctionDetailArrayListCompleted = enquiryRequestArrayList;
        }
        switch (tabHost.getCurrentTab()) {
            case 0:
                if (enquiryRequestArrayList != null && !enquiryRequestArrayList.isEmpty()) {
                    for (int i = 0; i < enquiryRequestArrayList.size(); i++) {
                        AuctionDetail auctionDetail = enquiryRequestArrayList.get(i);
                        try {
                            if (auctionDetail.getHighestBidder().equalsIgnoreCase(ChampionConstants.Y)) {
                                auctionDetail.setBidResult("You are highest bidder");
                            } else {
                                auctionDetail.setBidResult("You are not highest bidder");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            auctionDetail.setBidResult("");
                        }
                    }

                    myAuctionsAdapterLive.updateList(this.auctionDetailArrayListLive);
                    myAuctionsAdapterLive.notifyDataSetChanged();
                }
                break;
            case 1:
                if (enquiryRequestArrayList != null && !enquiryRequestArrayList.isEmpty()) {
                    upcomingAuctionsAdapter.updateList(this.auctionDetailArrayListUpcoming);
                    upcomingAuctionsAdapter.notifyDataSetChanged();
                }
                break;
            case 2:
                if (enquiryRequestArrayList != null && !enquiryRequestArrayList.isEmpty()) {
                    completedAuctionsAdapter.updateList(this.auctionDetailArrayListCompleted);
                    completedAuctionsAdapter.notifyDataSetChanged();
                }
                break;
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        CommonHelper.toast(message, getApplicationContext());
        swipeRefreshLayout.setRefreshing(false);
        switch (tabHost.getCurrentTab()) {
            case 0:
                ArrayList<AuctionDetail> auctionDetailArrayListLive = new ArrayList<>();
                myAuctionsAdapterLive.updateList(auctionDetailArrayListLive);
                myAuctionsAdapterLive.notifyDataSetChanged();
                break;
            case 1:
                ArrayList<AuctionDetail> auctionDetailArrayListUpcoming = new ArrayList<>();
                upcomingAuctionsAdapter.updateList(auctionDetailArrayListUpcoming);
                upcomingAuctionsAdapter.notifyDataSetChanged();
                break;
            case 2:
                ArrayList<AuctionDetail> auctionDetailArrayListCompleted = new ArrayList<>();
                completedAuctionsAdapter.updateList(auctionDetailArrayListCompleted);
                completedAuctionsAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onCallPress(String contactNo) {
        if (contactNo != null && !contactNo.isEmpty()) {
            new DialogsHelper(MyAuctionsActivity.this).callDialog(contactNo);
        } else {
            CommonHelper.toast("Contact details unavailable", getApplicationContext());
        }
    }

    @Override
    public void onMessagePress(String contactNo) {
        CommonHelper.sendMessage(contactNo, getApplicationContext());
    }

}