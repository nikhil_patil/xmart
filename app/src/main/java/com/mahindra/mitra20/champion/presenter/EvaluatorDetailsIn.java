package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.EvaluatorDetailsModel;
import com.mahindra.mitra20.evaluator.constants.WebConstants;

import java.util.ArrayList;

public interface EvaluatorDetailsIn {

    void updateUI(ArrayList<EvaluatorDetailsModel> evaluatorDetailsModel);
    void failed();
}
