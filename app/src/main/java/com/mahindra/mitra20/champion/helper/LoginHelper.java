package com.mahindra.mitra20.champion.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.mahindra.mitra20.LoginActivity;
import com.mahindra.mitra20.MyApplication;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.sqlite.BrokerDbHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import io.paperdb.Paper;

/**
 * Created by user on 8/31/2018.
 */

public class LoginHelper {
    public static void logoutUser(final Context context) {
        new AlertDialog.Builder(context).setIcon(R.mipmap.ic_launcher).setTitle("Logout")
                .setMessage("Are you sure want to Logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                EvaluatorHelper evaluatorHelper = new EvaluatorHelper(context);
                                MasterDatabaseHelper masterDatabaseHelper = new MasterDatabaseHelper(context);
                                SharedPreferences pref = MyApplication.getMyApplicationContext().getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
                                pref.edit().clear();
                                Paper.book().destroy();
                                Intent i = new Intent(context, LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(i);
                                evaluatorHelper.truncateTable();
                                masterDatabaseHelper.truncateMasterTable();
                                new BrokerDbHelper(context).truncateNotificationsTable();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                ((Activity) context).finish();
                            }
                        }.execute();


                    }
                }).setNegativeButton("No", null).show();
    }
}
