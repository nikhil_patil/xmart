package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.OnClickDialog;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AppointmentDetailsIn;
import com.mahindra.mitra20.champion.presenter.CancelRequestIn;
import com.mahindra.mitra20.champion.presenter.CancelRequestPresenter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.HashMap;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.ASSIGN_REQUEST_MODEL_KEY;
import static com.mahindra.mitra20.champion.constants.ChampionConstants.OPEN_STATUS_KEY;

public class CancelRequestDetailsActivity extends AppCompatActivity implements View.OnClickListener,
        OnClickDialog, OnMapReadyCallback, AppointmentDetailsIn, CancelRequestIn {

    private EnquiryRequest enquiryRequest;
    private DialogsHelper dialogsHelper;
    private CancelRequestPresenter cancelRequestPresenter;
    private ProgressDialog dialog;
    private String APPROVAL_STATUS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_request_details);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Appointment Details");
        dialogsHelper = new DialogsHelper(this);
        dialogsHelper.setOnClickDialog(this);
        enquiryRequest = getIntent().getParcelableExtra(ASSIGN_REQUEST_MODEL_KEY);
        cancelRequestPresenter = new CancelRequestPresenter(this, this);

        dialog = new ProgressDialog(CancelRequestDetailsActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Wait while loading");

        Button buttonReassignRequest = findViewById(R.id.buttonReassignRequest);
        Button buttonCancelRequest = findViewById(R.id.buttonCancelRequest);

        TextView textViewNameOfEnquiry = findViewById(R.id.textViewNameOfEnquiry);
        TextView textViewVehicleName = findViewById(R.id.textViewVehicleName);
        TextView textViewDate = findViewById(R.id.textViewDate);
        TextView textViewTime = findViewById(R.id.textViewTime);
        TextView textViewContactNo = findViewById(R.id.textViewContactNo);
        TextView textViewAddress = findViewById(R.id.textViewAddress);
        ImageView imageViewCall = findViewById(R.id.imageViewCall);

        textViewNameOfEnquiry.setText(enquiryRequest.getNameOfEnquiry());
        textViewVehicleName.setText(String.format("%s %s", enquiryRequest.getMake(),
                enquiryRequest.getModel()));
        textViewContactNo.setText(enquiryRequest.getContactNo());
        textViewAddress.setText(enquiryRequest.getLocation());
        textViewDate.setText(enquiryRequest.getDateOfAppointment());
        textViewTime.setText(enquiryRequest.getTimeOfAppointment());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        buttonReassignRequest.setOnClickListener(this);
        buttonCancelRequest.setOnClickListener(this);

        imageViewCall.setOnClickListener(this);
        textViewTime.setOnClickListener(this);
        textViewDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonReassignRequest:
                dialog.show();
                APPROVAL_STATUS = ChampionConstants.APPROVAL_STATUS_CANCEL;
                cancelRequestPresenter.cancelRequest(enquiryRequest, ChampionConstants.APPROVAL_STATUS_CANCEL);
                break;
            case R.id.buttonCancelRequest:
                dialog.show();
                APPROVAL_STATUS = ChampionConstants.APPROVAL_STATUS_APPROVE;
                cancelRequestPresenter.cancelRequest(enquiryRequest, ChampionConstants.APPROVAL_STATUS_APPROVE);
                break;
            case R.id.imageViewCall:
                if (enquiryRequest.getContactNo() != null) {
                    dialogsHelper.callDialog(enquiryRequest.getContactNo());
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void uploadSuccess(int requestID, String data) {
        try {
            dialog.hide();
            CommonHelper.toast(data, getApplicationContext());
            ChampionHelper championHelper = new ChampionHelper(getApplicationContext());

            if (APPROVAL_STATUS.equalsIgnoreCase(ChampionConstants.APPROVAL_STATUS_CANCEL)) {
                championHelper.updateEnquiryStatus(OPEN_STATUS_KEY, enquiryRequest.getRequestID(), null);
            } else if (APPROVAL_STATUS.equalsIgnoreCase(ChampionConstants.APPROVAL_STATUS_APPROVE)) {
                championHelper.updateEnquiryStatus("", enquiryRequest.getRequestID(), null);
            }

            Intent intentDashboard = new Intent(CancelRequestDetailsActivity.this, ChampionDashboardActivity.class);
            intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentDashboard);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadFailed(int requestID, String data) {
        dialog.hide();
        CommonHelper.toast(data, getApplicationContext());
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onSuccess(HashMap<String, String> hashMapValues) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNeutral() {

    }
}