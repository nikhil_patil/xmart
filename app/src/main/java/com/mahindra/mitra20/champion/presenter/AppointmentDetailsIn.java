package com.mahindra.mitra20.champion.presenter;

/**
 * Created by user on 8/30/2018.
 */

public interface AppointmentDetailsIn {

    void uploadSuccess(int requestID, String data);

    void uploadFailed(int requestID, String data);

}
