package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;

public class AssignConfirmEvaluatorActivity extends AppCompatActivity implements View.OnClickListener {

    private TableRow tableRowAssignMore, tableDone;
    private TextView eval_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_assign_confirm_evaluator);

        initUI();
    }

    private void initUI() {

        tableRowAssignMore = findViewById(R.id.tableRowAssignMore);
        eval_name = findViewById(R.id.eval_name);
        tableDone = findViewById(R.id.tableDone);

        EvaluatorMaster selectedEvaluatorMaster = getIntent().getParcelableExtra("data");
        eval_name.setText(selectedEvaluatorMaster.getEvaluatorName());

        tableRowAssignMore.setOnClickListener(this);
        tableDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        finish();
        switch (id) {

            case R.id.tableRowAssignMore:
                //openFragmentIn.openFragmentByID(R.layout.layout_assign_request_list, null);
                Intent intentAssignReqList = new Intent(AssignConfirmEvaluatorActivity.this, AssignRequestListActivity.class);
                startActivity(intentAssignReqList);
                break;
            case R.id.tableDone:
                //openFragmentIn.openFragmentByID(R.layout.fragment_champion_dashboard, null);
                Intent intentChampionDashboard = new Intent(AssignConfirmEvaluatorActivity.this, ChampionDashboardActivity.class);
                intentChampionDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentChampionDashboard);
                break;
        }
    }
}
