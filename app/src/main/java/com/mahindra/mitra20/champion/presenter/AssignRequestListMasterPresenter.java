package com.mahindra.mitra20.champion.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;
import com.mahindra.mitra20.champion.models.comparator.BidDateTimeComparator;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.WebConstants.ScheduleAppoinmentList;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.XMART;

/**
 * Created by user on 8/11/2018.
 */

public class AssignRequestListMasterPresenter implements VolleySingleTon.VolleyInteractor {

    private AssignRequestListIn assignRequestListIn;
    private Context context;
    private String status, auction;
    private ChampionHelper championHelper;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    public AssignRequestListMasterPresenter(AssignRequestListIn assignRequestListIn, Context context) {
        this.assignRequestListIn = assignRequestListIn;
        this.context = context;
        championHelper = new ChampionHelper(context);
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    public void downloadLiveAuctionMaster(String status, int requestId) {
        try {
            this.auction = status;
            JSONObject params = new JSONObject();

            if (status.equalsIgnoreCase(ChampionConstants.COMPLETED_STATUS)) {
                params.put(WebConstants.LiveAuctionMaster.FromDate,
                        new DateHelper().getPreviousDateByDays(-30));
            } else {
                params.put(WebConstants.LiveAuctionMaster.FromDate, new DateHelper().getCurrentDate());
            }

            params.put(WebConstants.LiveAuctionMaster.toDate, new DateHelper().getCurrentDate());
            params.put(WebConstants.LiveAuctionMaster.AuctionStatus, status);
            params.put(WebConstants.LiveAuctionMaster.Make, "");
            params.put(WebConstants.LiveAuctionMaster.Model, "");
            params.put(WebConstants.LiveAuctionMaster.countToDisplay, "9999");
            params.put(WebConstants.LiveAuctionMaster.Sorting, "DATE");
            params.put(WebConstants.LiveAuctionMaster.UserID,
                    SessionUserDetails.getInstance().getUserID().toUpperCase());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.LiveAuctionMaster.URL, params, requestId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downloadEnquiryMaster(String status, int requestId) {
        try {
            this.status = status;
            HashMap<String, String> params = new HashMap<>();
            String employeeCode = SessionUserDetails.getInstance().getEmployeeCode();
            if (null != employeeCode && !employeeCode.isEmpty())
                params.put(ScheduleAppoinmentList.USER_ID, employeeCode);
            else
                params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getUserID());
            params.put(WebConstants.EnquiryRequestList.REQUEST_STATUS, status);
            params.put(WebConstants.EnquiryRequestList.EVALUATION_REPORT_ID, "");
            params.put(WebConstants.EnquiryRequestList.COUNT_TO_DISPLAY_DASHBOARD, "9999");
            params.put(WebConstants.EnquiryRequestList.PINCODE, "");

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.EnquiryRequestList.URL, params, requestId);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.EnquiryRequestList.REQUEST_CODE,
                    context.getResources().getString(R.string.download_failed));
        }
    }

    public void downloadEvaluatorMaster(int requestId) {
        try {

            HashMap<String, String> params = new HashMap<>();
            params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getUserID());

            VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.EvaluatorMaster.URL, params, requestId);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.EnquiryRequestList.REQUEST_CODE, context.getResources().getString(R.string.download_failed));
        }
    }

    public void downloadEnquiryMaster(int reqID) {
        try {
            HashMap<String, String> params = new HashMap<>();
            String employeeCode = SessionUserDetails.getInstance().getEmployeeCode();
            String[] arrUserId = SessionUserDetails.getInstance().getUserID().toLowerCase().split("\\.");
            String strUserId = arrUserId[0];
            if (null != employeeCode && !employeeCode.isEmpty())
                params.put(ScheduleAppoinmentList.USER_ID, employeeCode);
            else
                params.put(ScheduleAppoinmentList.USER_ID, strUserId);
            params.put(ScheduleAppoinmentList.REQUEST_STATUS, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS_KEY);
            params.put(ScheduleAppoinmentList.EVALUATION_REPORT_ID, "");
            params.put(ScheduleAppoinmentList.COUNT_TO_DISPLAY_DASHBOARD, "999");
            params.put(ScheduleAppoinmentList.PINCODE, "");
            JSONObject jsonObject = new JSONObject(params);

            VolleySingleTon.getInstance().connectToPostUrl(context, this, ScheduleAppoinmentList.URL, jsonObject, reqID);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(ScheduleAppoinmentList.REQUEST_CODE, context.getResources().getString(R.string.download_failed));
        }
    }

    @Override
    public void gotSuccessResponse(final int requestId, String response) {
        try {
            switch (requestId) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 12:
                case 13:
                    try {
                        if (arrayListMake.isEmpty()) {
                            arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
                        }

                        final ArrayList<EnquiryRequest> listEnquiryRequests = new ArrayList<>();

                        JSONObject jsonObjectParent = new JSONObject(response);
                        JSONArray jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EnquiryRequestList");

                        for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {

                            EnquiryRequest enquiryRequest = new EnquiryRequest();

                            JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);

                            enquiryRequest.setContactNo(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "ContactNo"));
                            enquiryRequest.setDateOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "DateOfAppointment"));
                            if ((status.equalsIgnoreCase(ChampionConstants.EVALUATION_DONE))
                                    || (status.equalsIgnoreCase(ChampionConstants.OFFER_PRICE_STATUS_KEY))) {
                                enquiryRequest.setMake(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Make"));
                                enquiryRequest.setModel(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Model"));
                            } else {
                                Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                                        arrayListMake, CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Make"),
                                        CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Model"));
                                enquiryRequest.setMake(makeModel.first);
                                enquiryRequest.setModel(makeModel.second);
                            }
                            enquiryRequest.setNameOfEnquiry(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "NameOfEnquiry"));
                            enquiryRequest.setPinCode(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Pincode"));
                            enquiryRequest.setRequestID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "RequestID"));
                            enquiryRequest.setLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "LocationOfAppointment"));
                            enquiryRequest.setCustomerExpectedPrice(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "CustomerExpectedPrice"));
                            enquiryRequest.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "OptedNewVehicle"));
                            enquiryRequest.setSalesconsultantname(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Salesconsultantname"));
                            enquiryRequest.setSalesconsultantContactnum(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "SalesconsultantContactnum"));
                            enquiryRequest.setLeadPriority(CommonHelper.parseLeadPriorityKey(jsonObjectEnquiryRequest, "LeadPriority"));
                            enquiryRequest.setEnquiryLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryLocation"));
                            enquiryRequest.setEnquirySource(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquirySource"));
                            enquiryRequest.setEnquiryType(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryType"));
                            enquiryRequest.setRequestStatus(status);
                            enquiryRequest.setEvaluationReportID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluationReportID"));
                            enquiryRequest.setEvaluatorID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluatorID"));
                            enquiryRequest.setTimeOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "TimeOfAppointment"));
                            enquiryRequest.setYearOfManifacturing(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "YearOfManufacturing"));

                            if (!CommonHelper.hasJSONKey(jsonObjectEnquiryRequest,
                                    "RequestStatus").equalsIgnoreCase("Auction completed")) {
                                listEnquiryRequests.add(enquiryRequest);
                            }
                        }
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                championHelper.deleteEnquiryRequestDetails(status);
                                championHelper.putEnquiryRequestDetails(listEnquiryRequests);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                assignRequestListIn.onDownloadSuccess(requestId, listEnquiryRequests);
                            }
                        }.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 6:
                case 7:
                case 8:
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("AuctionDetails");
                        final ArrayList<AuctionDetail> auctionDetailArrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length(); i++) {

                            AuctionDetail auctionDetail = new AuctionDetail();

                            JSONObject jsonObjectAuctionDetails = (JSONObject) jsonArray.get(i);
                            auctionDetail.setAuctionDate(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionDate"));
                            auctionDetail.setAuctionID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionID"));
                            auctionDetail.setEvaluationReportID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionID"));
                            auctionDetail.setAuctionStatus(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionStatus"));
                            auctionDetail.setCountOfBidders(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "CountOfBidders"));
                            auctionDetail.setMITRAPriceBasePrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                            auctionDetail.setRequestID(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "RequestID"));
                            auctionDetail.setStartTimestamp(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "StartTimestamp"));
                            auctionDetail.setRemainingTimeInSeconds(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "remainingTimeInSeconds"));
                            auctionDetail.setVehOwnerName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "vehOwnerName"));
                            auctionDetail.setVehicleName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "vehicleName"));
                            auctionDetail.setHighestPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "HighestPrice"));
                            auctionDetail.setTransactionNo(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "TransactionNo."));
                            auctionDetail.setHighestBidder(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "HighestBidder"));
                            auctionDetail.setNegotiatorContactName(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactName"));
                            auctionDetail.setNegotiatorContactNumber(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                            auctionDetail.setNoOfOwners(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NoOfOwners"));
                            auctionDetail.setOdometerReading(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "OdometerReading"));

                            JSONArray jsonArrayLIST = jsonObjectAuctionDetails.getJSONArray("LIST");
                            List<AuctionLIST> listList = new ArrayList<>();

                            for (int j = 0; j < jsonArrayLIST.length(); j++) {

                                JSONObject jsonObjectLIST = jsonArrayLIST.getJSONObject(j);
                                AuctionLIST auctionLIST = new AuctionLIST();

                                auctionLIST.setMITRAId(CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId"));
                                auctionLIST.setMITRAName(CommonHelper.replaceCars24MFCWChampion(CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAName"), CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId"), CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactName")));
                                auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectLIST, "bidPrice"));
                                auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectLIST, "biddingDateTime"));

                                if (CommonHelper.hasJSONKey(jsonObjectLIST, "MITRAId").equalsIgnoreCase(ChampionConstants.MFCW_ID)) {
                                    auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                                } else {
                                    auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectLIST, "PhoneNum"));
                                }

                                auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectLIST, "AmountWithoutCommision"));
                                auctionLIST.setCommissionPrice(CommonHelper.hasJSONKey(jsonObjectLIST, "CommisionAmnt"));
                                listList.add(auctionLIST);
                            }

                            if (!CommonHelper.isChampionExistInList(listList)) {

                                //TODO add champion in MITRA list
                                AuctionLIST auctionLIST = new AuctionLIST();
                                auctionLIST.setMITRAId(SessionUserDetails.getInstance().getUserID().toUpperCase());
                                auctionLIST.setCommissionPrice("0");
                                auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                                auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "MITRAprice(base price)"));
                                auctionLIST.setMITRAName(XMART);
                                auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "AuctionDate") + " " + CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "StartTimestamp"));
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "ContactNo"));

                                listList.add(auctionLIST);
                            }

                            if (auction.equalsIgnoreCase(ChampionConstants.COMPLETED_STATUS)) {
                                try {
                                    Collections.sort(listList, new BidDateTimeComparator());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            auctionDetail.setLIST(listList);
                            auctionDetailArrayList.add(auctionDetail);
                        }
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                championHelper.deleteAuctionDetails(auction);
                                championHelper.putAuctionDetails(auctionDetailArrayList);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                assignRequestListIn.onDownloadSuccess(requestId, null);
                            }
                        }.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 9:
                    final ArrayList<EvaluatorMaster> evaluatorMasterArrayList = new ArrayList<>();

                    JSONObject jsonObjectParent = new JSONObject(response);
                    JSONArray jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EvaluatorDetails");

                    for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {

                        EvaluatorMaster evaluatorMaster = new EvaluatorMaster();
                        JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);
                        evaluatorMaster.setAllocatedReqCount(jsonObjectEnquiryRequest.optString("ALLOCATED_REQ_COUNT"));
                        evaluatorMaster.setEvaluatorContactNo(jsonObjectEnquiryRequest.optString("Evaluator_Contact_Number"));
                        evaluatorMaster.setEvaluatorID(jsonObjectEnquiryRequest.optString("Evaluator_ID"));
                        evaluatorMaster.setEvaluatorName(jsonObjectEnquiryRequest.optString("Evaluator_Name"));

                        evaluatorMasterArrayList.add(evaluatorMaster);
                    }

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            championHelper.deleteEvaluatorMaster();
                            championHelper.putEvaluatorMaster(evaluatorMasterArrayList);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            assignRequestListIn.onDownloadSuccess(requestId, null);
                        }
                    }.execute();
                    break;
                case 10:
                    final ArrayList<ScheduleAppoinment> listScheduleAppoinments = new ArrayList<>();

                    jsonObjectParent = new JSONObject(response);
                    JSONArray jsonArrayScheduleAppoinmentList = jsonObjectParent.getJSONArray("EnquiryRequestList");

                    for (int i = 0; i < jsonArrayScheduleAppoinmentList.length(); i++) {
                        ScheduleAppoinment ScheduleAppoinment = new ScheduleAppoinment();
                        JSONObject jsonObjectScheduleAppoinment = (JSONObject) jsonArrayScheduleAppoinmentList.get(i);
                        String evaluatorId = SessionUserDetails.getInstance().getUserID();

                        String strUserId = "";
                        try {

                            if (evaluatorId.contains(".")) {
                                strUserId = evaluatorId.split("\\.")[0].toString();
                            } else {
                                strUserId = SessionUserDetails.getInstance().getUserID().toUpperCase();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (SessionUserDetails.getInstance().getUserType() == 0 && CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EvaluatorID").equalsIgnoreCase(strUserId)) {

                            ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "ContactNo"));
                            ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "DateOfAppointment"));
                            ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Make"));
                            ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Model"));
                            ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                            ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Pincode"));
                            ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "RequestID"));
                            ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "RequestStatus"));
                            ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                            ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                            ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Occupation"));
                            ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                            ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "PhoneNo"));
                            ScheduleAppoinment.setState(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "State"));
                            ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                            ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "City"));
                            ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EmailAddress"));
                            ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EnquirySource"));
                            ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EnquiryType"));
                            ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Salesconsultantname"));
                            ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));

                            listScheduleAppoinments.add(ScheduleAppoinment);

                        } else if (SessionUserDetails.getInstance().getUserType() == 1) {
                            ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "ContactNo"));
                            ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "DateOfAppointment"));
                            ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Make"));
                            ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Model"));
                            ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                            ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Pincode"));
                            ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "RequestID"));
                            ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "RequestStatus"));
                            ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                            ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                            ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Occupation"));
                            ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                            ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "PhoneNo"));
                            ScheduleAppoinment.setState(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "State"));
                            ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                            ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "City"));
                            ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EmailAddress"));
                            ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EnquirySource"));
                            ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "EnquiryType"));
                            ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "Salesconsultantname"));
                            ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));

                            listScheduleAppoinments.add(ScheduleAppoinment);
                        }
                    }

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            evaluatorHelper.deleteScheduleRequestDetails(EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                            evaluatorHelper.putScheduledAppointment(listScheduleAppoinments);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            assignRequestListIn.onDownloadSuccess(requestId, null);
                        }
                    }.execute();
                    break;
                case 11:
                    try {
                        if (arrayListMake.isEmpty()) {
                            arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
                        }

                        final ArrayList<EnquiryRequest> listEnquiryRequests = new ArrayList<>();

                        jsonObjectParent = new JSONObject(response);
                        jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EnquiryRequestList");

                        for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {

                            EnquiryRequest enquiryRequest = new EnquiryRequest();

                            JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);

                            enquiryRequest.setContactNo(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "ContactNo"));
                            enquiryRequest.setDateOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "DateOfAppointment"));

                            Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                                    arrayListMake, CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Make"),
                                    CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Model"));
                            enquiryRequest.setMake(makeModel.first);
                            enquiryRequest.setModel(makeModel.second);

                            enquiryRequest.setNameOfEnquiry(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "NameOfEnquiry"));
                            enquiryRequest.setPinCode(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Pincode"));
                            enquiryRequest.setRequestID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "RequestID"));
                            enquiryRequest.setLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "LocationOfAppointment"));
                            enquiryRequest.setCustomerExpectedPrice(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "CustomerExpectedPrice"));
                            enquiryRequest.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "OptedNewVehicle"));
                            enquiryRequest.setSalesconsultantname(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "Salesconsultantname"));
                            enquiryRequest.setSalesconsultantContactnum(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "SalesconsultantContactnum"));
                            enquiryRequest.setLeadPriority(CommonHelper.parseLeadPriorityKey(jsonObjectEnquiryRequest, "LeadPriority"));
                            enquiryRequest.setEnquiryLocation(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryLocation"));
                            enquiryRequest.setEnquirySource(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquirySource"));
                            enquiryRequest.setEnquiryType(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EnquiryType"));
                            enquiryRequest.setRequestStatus(status);

                            enquiryRequest.setEvaluationReportID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluationReportID"));
                            enquiryRequest.setEvaluatorID(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "EvaluatorID"));
                            enquiryRequest.setTimeOfAppointment(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "TimeOfAppointment"));
                            enquiryRequest.setYearOfManifacturing(CommonHelper.hasJSONKey(jsonObjectEnquiryRequest, "YearOfManufacturing"));

                            listEnquiryRequests.add(enquiryRequest);
                        }

                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                championHelper.deleteEnquiryRequestDetails(status);
                                championHelper.putEnquiryRequestDetails(listEnquiryRequests);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                assignRequestListIn.onDownloadSuccess(requestId, listEnquiryRequests);
                            }
                        }.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        if (requestId == 1 || requestId == 2 || requestId == 3 || requestId == 4 || requestId == 5) {
            championHelper.deleteEnquiryRequestDetails(status);
        } else if (requestId == 6 || requestId == 7 || requestId == 8) {

            championHelper.deleteAuctionDetails(auction);
        } else if (requestId == 9) {

            championHelper.deleteEvaluatorMaster();
        }
        assignRequestListIn.onDownloadSuccess(requestId, null);
    }

    public String getNextEvaluationId() {
        return evaluatorHelper.getNextEvaluationId();
    }
}