package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.OffloadDetails;
import com.mahindra.mitra20.models.SessionUserDetails;

import org.json.JSONObject;

/**
 * Created by user on 8/20/2018.
 */

public class SubmitProcurementPricePresenter implements VolleySingleTon.VolleyInteractor {

    private SubmitProcurementPriceIn submitProcurementPriceIn;
    private Context context;

    public SubmitProcurementPricePresenter(SubmitProcurementPriceIn submitProcurementPriceIn,
                                           Context context) {
        this.context = context;
        this.submitProcurementPriceIn = submitProcurementPriceIn;
    }

    public void SubmitProcurementPriceToCustomer(String auctionID, String customerResponse,
                                                 String procurementPrice, String reasonForLowerPrice,
                                                 String modeOfSale) {
        try {
            JSONObject params = new JSONObject();
            params.put(WebConstants.SubmitProcurementPrice.UserID,
                    SessionUserDetails.getInstance().getUserID());
            params.put(WebConstants.SubmitProcurementPrice.AuctionID, auctionID);
            params.put(WebConstants.SubmitProcurementPrice.CustomerResponse, customerResponse);
            params.put(WebConstants.SubmitProcurementPrice.ProcurementPrice, procurementPrice);
            params.put(WebConstants.SubmitProcurementPrice.ReasonForLowerPriceThanHighest,
                    reasonForLowerPrice);
            params.put(WebConstants.SubmitProcurementPrice.ModeOfSale, modeOfSale);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.SubmitProcurementPrice.URL, params,
                    WebConstants.SubmitProcurementPrice.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SubmitOffloadDetails(OffloadDetails offloadDetails) {
        try {
            offloadDetails.setUserID(SessionUserDetails.getInstance().getUserID());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.SubmitOffloadDetails.URL,
                    new JSONObject(new Gson().toJson(offloadDetails)),
                    WebConstants.SubmitOffloadDetails.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        if (WebConstants.SubmitProcurementPrice.REQUEST_CODE == requestId) {
            submitProcurementPriceIn.uploadProcurementDetailsSuccess(requestId, response);
        } else if (WebConstants.SubmitOffloadDetails.REQUEST_CODE == requestId) {
            submitProcurementPriceIn.uploadOffloadDetailsSuccess(requestId, response);
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            submitProcurementPriceIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            submitProcurementPriceIn.uploadFailed(requestId, response);
        }
    }
}