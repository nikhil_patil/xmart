package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AssignRequestListIn;
import com.mahindra.mitra20.champion.presenter.AssignRequestListPresenter;
import com.mahindra.mitra20.champion.presenter.SetOfferPriceIn;
import com.mahindra.mitra20.champion.presenter.SetOfferPricePresenter;
import com.mahindra.mitra20.champion.views.adapters.AssignRequestAdapter;
import com.mahindra.mitra20.databinding.ActivityFinalProcurementListBinding;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class FinalProcurementListActivity extends AppCompatActivity implements AssignRequestListIn,
        SwipeRefreshLayout.OnRefreshListener, ItemClickListener, SetOfferPriceIn, ContactIn,
        SearchView.OnQueryTextListener {

    ActivityFinalProcurementListBinding binding;
    private AssignRequestAdapter assignRequestAdapter;
    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private AssignRequestListPresenter assignRequestListPresenter;
    private SetOfferPricePresenter setOfferpricePresenter;
    private AuctionDetail auctionDetail;
    private ProgressDialog dialog;
    private AuctionDetail _auctionDetail;
    private EnquiryRequest enquiryRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_final_procurement_list);

        initUI();
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Procurement Details");

        dialog = new ProgressDialog(FinalProcurementListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Wait while loading");

        assignRequestListPresenter = new AssignRequestListPresenter(this, this);
        setOfferpricePresenter = new SetOfferPricePresenter(this, this);

        ChampionHelper championHelper = new ChampionHelper(getBaseContext());
        enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(ChampionConstants.OFFER_PRICE);

        if (enquiryRequestArrayList.size() == 0) {
            onRefresh();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recyclerViewCustomerResponse.setLayoutManager(linearLayoutManager);
        binding.recyclerViewCustomerResponse.setEmptyView(findViewById(R.id.textViewEmpty));
        assignRequestAdapter = new AssignRequestAdapter(getBaseContext(), this, true);
        assignRequestAdapter.setAssignRequestContents(enquiryRequestArrayList);

        assignRequestAdapter.setClickListener(this);
        binding.recyclerViewCustomerResponse.setAdapter(assignRequestAdapter);

        assignRequestAdapter.notifyDataSetChanged();
        binding.swipeContainer.setOnRefreshListener(this);

        setupSearchView();
    }

    private void setupSearchView() {
        binding.searchViewFilter.setIconifiedByDefault(false);
        binding.searchViewFilter.setOnQueryTextListener(this);
        binding.searchViewFilter.setSubmitButtonEnabled(true);
        binding.searchViewFilter.setFocusable(false);
        binding.searchViewFilter.setQueryHint("Search...");
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> _enquiryRequestArrayList) {
        switch (requestId) {
            case 1:
                if (_enquiryRequestArrayList != null && !_enquiryRequestArrayList.isEmpty()) {

                    enquiryRequestArrayList = _enquiryRequestArrayList;
                    this.assignRequestAdapter.updateList(_enquiryRequestArrayList);
                    assignRequestAdapter.notifyDataSetChanged();
                }
                break;
            case 8:
                String evaluationReportID = _enquiryRequestArrayList.get(0).getEvaluationReportID();

                Intent intentEnterReservePrice = new Intent(this,
                        FinalProcurementAndOffloadActivity.class);
                intentEnterReservePrice.putExtra("NewEvaluationId", evaluationReportID);
                startActivity(intentEnterReservePrice);
                break;
        }
        binding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {

    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.OFFER_PRICE);
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onClick(View view, int position) {
        try {
            dialog.show();
            enquiryRequest = enquiryRequestArrayList.get(position);
            ChampionHelper championHelper = new ChampionHelper(getApplicationContext());
            auctionDetail = championHelper.getAuctionMastersByID(enquiryRequest.getEvaluationReportID());
            setOfferpricePresenter.getAuctionMasterByID(auctionDetail.getAuctionID());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void uploadSuccess(int requestID, String data) {

    }

    @Override
    public void uploadSuccess(int requestID, AuctionDetail auctionDetail) {
        HashMap<String, String> params = new HashMap<>();
        _auctionDetail = auctionDetail;
        params.put(WebConstants.GetEvaluationReport.EvaluationReportID, auctionDetail.getAuctionID());
        params.put(WebConstants.GetEvaluationReport.RequestID, auctionDetail.getRequestID());
        params.put(WebConstants.GetEvaluationReport.UserID, SessionUserDetails.getInstance().getUserID());

        setOfferpricePresenter.downloadEvaluationReport(params);
    }

    @Override
    public void downloadSuccess(int requestID, NewEvaluation newEvaluation) {
        try {
            dialog.hide();
            finish();
            Intent intent = new Intent(this, FinalProcurementAndOffloadActivity.class);
            intent.putExtra(CommonHelper.EXTRA_AUCTION, _auctionDetail);
            intent.putExtra(CommonHelper.EXTRA_CUSTOMER_EXPECTED_PRICE, enquiryRequest.getCustomerExpectedPrice());
            intent.putExtra(CommonHelper.EXTRA_EVALUATION_STATUS, newEvaluation.getEvaluationStatus());
            intent.putExtra(CommonHelper.EXTRA_AUCTION_DB, this.auctionDetail);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadFailed(int requestID, String data) {
        dialog.hide();
        CommonHelper.toast(data, getApplicationContext());
    }

    @Override
    public void onCallPress(String contactNo) {
        if (contactNo != null && !contactNo.isEmpty()) {
            new DialogsHelper(FinalProcurementListActivity.this).callDialog(contactNo);
        }
    }

    @Override
    public void onMessagePress(String contactNo) {
        CommonHelper.sendMessage(contactNo, FinalProcurementListActivity.this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (assignRequestAdapter != null)
            assignRequestAdapter.filter(newText);
        return false;
    }
}