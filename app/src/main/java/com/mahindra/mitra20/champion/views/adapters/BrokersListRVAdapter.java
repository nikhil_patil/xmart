package com.mahindra.mitra20.champion.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.OnBrokersListItemClickListener;
import com.mahindra.mitra20.champion.models.BrokerDetails;
import com.mahindra.mitra20.databinding.ListItemBrokersBinding;

import java.util.ArrayList;
import java.util.List;

public class BrokersListRVAdapter extends RecyclerView.Adapter<BrokersListRVAdapter.ViewHolder>
        implements OnBrokersListItemClickListener {

    private List<BrokerDetails> allBrokerDetails;
    private List<BrokerDetails> filteredBrokerDetails;
    Context context;

    public void filter(final String text) {
        try {
            filteredBrokerDetails.clear();
            if (TextUtils.isEmpty(text)) {
                filteredBrokerDetails.addAll(allBrokerDetails);
            } else {
                for (BrokerDetails brokerDetails : allBrokerDetails) {
                    String tempText = text.toLowerCase();
                    if (brokerDetails.getBrokerName().toLowerCase().contains(tempText)) {
                        filteredBrokerDetails.add(brokerDetails);
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(BrokerDetails brokerDetails) {
        Intent intent = new Intent();
        intent.putExtra("BrokerDetails", brokerDetails);
        ((Activity) context).setResult(Activity.RESULT_OK, intent);
        ((Activity) context).finish();
    }

    @Override
    public void onCallPressed(BrokerDetails brokerDetails) {
        new DialogsHelper((Activity) context).callDialog(brokerDetails.getBrokerContactNumber());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ListItemBrokersBinding binding;

        public ViewHolder(ListItemBrokersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public BrokersListRVAdapter(Context _context, List<BrokerDetails> allBrokerDetails) {
        context = _context;
        this.allBrokerDetails = allBrokerDetails;
        this.filteredBrokerDetails = new ArrayList<>();
        this.filteredBrokerDetails.addAll(allBrokerDetails);
    }

    @Override
    public BrokersListRVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ListItemBrokersBinding binding = ListItemBrokersBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.binding.setBrokerDetails(filteredBrokerDetails.get(position));
        holder.binding.setClickListeners(this);
    }

    @Override
    public int getItemCount() {
        return filteredBrokerDetails.size();
    }
}