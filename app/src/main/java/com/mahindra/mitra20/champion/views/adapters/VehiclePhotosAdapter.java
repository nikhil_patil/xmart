package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.VehiclePhoto;

import java.io.File;
import java.util.List;

/**
 * Created by PATINIK-CONT on 7/10/2019.
 * Unused class
 */

public class VehiclePhotosAdapter extends RecyclerView.Adapter<VehiclePhotosAdapter.ViewHolder> {
    private List<VehiclePhoto> vehiclePhotoContents;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        ImageView ivVehiclePhoto;
        TextView tvVehiclePhotoName;
        View layout;

        public ViewHolder(View v) {
            super(v);
            ivVehiclePhoto = v.findViewById(R.id.iv_vehicle_photo);
            progressBar = v.findViewById(R.id.progress_bar);
            tvVehiclePhotoName = v.findViewById(R.id.tv_vehicle_photo_name);
            layout = v;
        }
    }

    public VehiclePhotosAdapter(Context _context, List<VehiclePhoto> _vehiclePhotoContents) {
        vehiclePhotoContents = _vehiclePhotoContents;
        context = _context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_vehicle_photos_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvVehiclePhotoName.setText(vehiclePhotoContents.get(position).getNameOfImage());
        holder.progressBar.setVisibility(View.GONE);
        if (!vehiclePhotoContents.get(position).getPath().equalsIgnoreCase("")) {
            holder.layout.setTag(vehiclePhotoContents.get(position).getPath());
            Glide.with(context)
                    .load(Uri.fromFile(new File(vehiclePhotoContents.get(position).getPath())))
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.ic_image_not_found))
                    .into(holder.ivVehiclePhoto);
        }
    }

    @Override
    public int getItemCount() {
        return vehiclePhotoContents.size();
    }
}