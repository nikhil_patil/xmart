package com.mahindra.mitra20.champion.models;

public class OffloadDetails {

    private String SaleOffLoadTo;
    private String EvaluationID;
    private String AuctionID;
    private String SellingOffloadPrice;
    private String SellingDate;
    private String UserID;
    private String CustomerID;
    private String CustomerName;
    private String CustomerMobile;
    private String CustomerEmail;
    private String CustomerAddress1;
    private String CustomerAddress2;
    private String CustomerAddress3;
    private String PinCode;
    private String State;
    private String City;
    private String District;
    private String Tehsil;
    private String SourceOfOffloadIBBTrade;

    public OffloadDetails() {
        SourceOfOffloadIBBTrade = "";
        State = "";
        City = "";
        Tehsil = "";
        CustomerAddress3 = "";
        CustomerAddress2 = "";
        District = "";
        PinCode = "";
    }

    public String getSourceOfOffloadIBBTrade() {
        return SourceOfOffloadIBBTrade;
    }

    public void setSourceOfOffloadIBBTrade(String sourceOfOffloadIBBTrade) {
        SourceOfOffloadIBBTrade = sourceOfOffloadIBBTrade;
    }

    public String getSaleOffLoadTo() {
        return SaleOffLoadTo;
    }

    public void setSaleOffLoadTo(String saleOffLoadTo) {
        SaleOffLoadTo = saleOffLoadTo;
    }

    public String getCustomerMobile() {
        return CustomerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        CustomerMobile = customerMobile;
    }

    public String getEvaluationID() {
        return EvaluationID;
    }

    public void setEvaluationID(String evaluationID) {
        EvaluationID = evaluationID;
    }

    public String getTehsil() {
        return Tehsil;
    }

    public void setTehsil(String tehsil) {
        Tehsil = tehsil;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPinCode() {
        return PinCode;
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getSellingOffloadPrice() {
        return SellingOffloadPrice;
    }

    public void setSellingOffloadPrice(String sellingOffloadPrice) {
        SellingOffloadPrice = sellingOffloadPrice;
    }

    public String getSellingDate() {
        return SellingDate;
    }

    public void setSellingDate(String sellingDate) {
        SellingDate = sellingDate;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getCustomerEmail() {
        return CustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        CustomerEmail = customerEmail;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCustomerAddress3() {
        return CustomerAddress3;
    }

    public void setCustomerAddress3(String customerAddress3) {
        CustomerAddress3 = customerAddress3;
    }

    public String getCustomerAddress2() {
        return CustomerAddress2;
    }

    public void setCustomerAddress2(String customerAddress2) {
        CustomerAddress2 = customerAddress2;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getAuctionID() {
        return AuctionID;
    }

    public void setAuctionID(String auctionID) {
        AuctionID = auctionID;
    }

    public String getCustomerAddress1() {
        return CustomerAddress1;
    }

    public void setCustomerAddress1(String customerAddress1) {
        CustomerAddress1 = customerAddress1;
    }
}