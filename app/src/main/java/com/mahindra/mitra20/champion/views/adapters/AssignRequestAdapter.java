package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.interfaces.ContactIn;

import java.util.ArrayList;

public class AssignRequestAdapter extends RecyclerView.Adapter<AssignRequestAdapter.ViewHolder> {

    private ArrayList<EnquiryRequest> assignRequestContents;
    private ArrayList<EnquiryRequest> backupAssignRequestContents;
    Context context;
    private ItemClickListener clickListener;
    private ContactIn contactIn;

    public void updateList(ArrayList<EnquiryRequest> assignRequestContents) {
        this.backupAssignRequestContents = new ArrayList<>();
        this.backupAssignRequestContents.addAll(assignRequestContents);

        this.assignRequestContents = new ArrayList<>();
        this.assignRequestContents.addAll(assignRequestContents);
    }

    public void setAssignRequestContents(ArrayList<EnquiryRequest> assignRequestContents) {
        this.backupAssignRequestContents = new ArrayList<>();
        this.backupAssignRequestContents.addAll(assignRequestContents);

        this.assignRequestContents = new ArrayList<>();
        this.assignRequestContents.addAll(assignRequestContents);
    }

    public void filter(final String text) {
        try {
            assignRequestContents.clear();
            if (TextUtils.isEmpty(text)) {
                assignRequestContents.addAll(backupAssignRequestContents);
            } else {
                for (EnquiryRequest enquiryRequest : backupAssignRequestContents) {
                    String tempText = text.toLowerCase();
                    if (enquiryRequest.getNameOfEnquiry().toLowerCase().contains(tempText)) {
                        assignRequestContents.add(enquiryRequest);
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName, tvVehicleName, textViewLocation, tvEnquirySource, tvEnquiryType;
        LinearLayout layoutItemAssignRequest;
        public TableRow tableRowVehicleDetails;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            tvName = v.findViewById(R.id.tvName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            tvVehicleName = v.findViewById(R.id.tvBrokerId);
            layoutItemAssignRequest = v.findViewById(R.id.layout_item_assign_request);
            tableRowVehicleDetails = v.findViewById(R.id.tableRowVehicleDetails);
            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);

            layoutItemAssignRequest.setOnClickListener(this);
            v.findViewById(R.id.imageViewCall).setOnClickListener(this);
            v.findViewById(R.id.imageViewMessage).setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.layout_item_assign_request:
                    if (clickListener != null) {
                        clickListener.onClick(view, getActualItemPosition(getAdapterPosition()));
                    }
                    break;
                case R.id.imageViewCall:
                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onCallPress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
                case R.id.imageViewMessage:
                    if (assignRequestContents.get(getAdapterPosition()).getContactNo() != null &&
                            !assignRequestContents.get(getAdapterPosition()).getContactNo().isEmpty()) {
                        contactIn.onMessagePress(assignRequestContents.get(getAdapterPosition()).getContactNo());
                    }
                    break;
            }
        }

        private int getActualItemPosition(int adapterPosition) {
            EnquiryRequest enquiryRequest = assignRequestContents.get(adapterPosition);
            int index = -1;
            for (EnquiryRequest enquiryRequestCurrent: backupAssignRequestContents) {

                index++;
                if (enquiryRequest.equals(enquiryRequestCurrent)) {

                    return index;
                }
            }
            return -1;
        }
    }

    public void setClickListener(ItemClickListener monClick) {
        this.clickListener = monClick;
    }

    public AssignRequestAdapter(Context _context, ArrayList<EnquiryRequest> _assignRequestContents, ContactIn contactIn, boolean showVehicleName) {
        this.backupAssignRequestContents = new ArrayList<>();
        this.backupAssignRequestContents.addAll(_assignRequestContents);

        this.assignRequestContents = new ArrayList<>();
        this.assignRequestContents.addAll(_assignRequestContents);

        context = _context;
        this.contactIn = contactIn;
    }

    public AssignRequestAdapter(Context _context, ContactIn contactIn, boolean showVehicleName) {
        context = _context;
        this.contactIn = contactIn;
    }

    @Override
    public AssignRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.list_item_assign_request, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvName.setText(assignRequestContents.get(position).getNameOfEnquiry());
        holder.textViewLocation.setText(assignRequestContents.get(position).getLocation());

        String strMake = assignRequestContents.get(position).getMake();
        String strModel = assignRequestContents.get(position).getModel();
        holder.tvVehicleName.setText(String.format("%s %s", strMake, strModel));
        String enqSource = assignRequestContents.get(position).getEnquirySource();
        if (null != enqSource && !enqSource.isEmpty())
            holder.tvEnquirySource.setText(String.format("Source - %s", enqSource));
        else
            holder.tvEnquirySource.setText("Source - NA");
        String enqType = assignRequestContents.get(position).getEnquiryType();
        if (null != enqType && !enqType.isEmpty())
            holder.tvEnquiryType.setText(String.format("Type - %s", enqType));
        else
            holder.tvEnquiryType.setText("Type - NA");
    }

    @Override
    public int getItemCount() {
        return assignRequestContents.size();
    }
}