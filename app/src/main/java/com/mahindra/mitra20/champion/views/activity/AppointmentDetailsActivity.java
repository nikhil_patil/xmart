package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.OnClickDialog;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AppointmentDetailsIn;
import com.mahindra.mitra20.champion.presenter.AppointmentDetailsPresenter;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DatePickerHelper;
import com.mahindra.mitra20.evaluator.helper.TimePickerHelper;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.ASSIGN_REQUEST_MODEL_KEY;

public class AppointmentDetailsActivity extends AppCompatActivity implements View.OnClickListener,
        OnClickDialog, OnMapReadyCallback, AppointmentDetailsIn {

    private EnquiryRequest enquiryRequest;
    private DialogsHelper dialogsHelper;
    private GoogleMap map;
    private String strLocation;
    private TextView textViewDate, textViewDateInvisible, textViewTime, textViewTimeHidden;
    private AppointmentDetailsPresenter appointmentDetailsPresenter;
    private ProgressDialog dialog;
    DateHelper dateHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_evaluator_appoinment_details);
        
        initUI();
    }

    private void initUI() {

        CommonHelper.settingCustomToolBar(this, "Appointment Details");
        dialogsHelper = new DialogsHelper(this);
        dialogsHelper.setOnClickDialog(this);
        enquiryRequest = getIntent().getParcelableExtra(ASSIGN_REQUEST_MODEL_KEY);

        appointmentDetailsPresenter = new AppointmentDetailsPresenter(this, this);
        dateHelper = new DateHelper();

        Button buttonSchedule = findViewById(R.id.buttonSchedule);
        Button buttonAssignRequest = findViewById(R.id.buttonAssignRequest);
        Button buttonStartEvaluation = findViewById(R.id.buttonStartEvaluation);
        Button buttonCancel = findViewById(R.id.buttonCancel);

        TextView textViewNameOfEnquiry = findViewById(R.id.textViewNameOfEnquiry);
        TextView textViewVehicleName = findViewById(R.id.textViewVehicleName);
        textViewDate = findViewById(R.id.textViewDate);
        textViewDateInvisible = findViewById(R.id.textViewDateInvisible);
        textViewTime = findViewById(R.id.textViewTime);
        textViewTimeHidden = findViewById(R.id.textViewTimeHidden);
        TextView textViewContactNo = findViewById(R.id.textViewContactNo);
        TextView textViewAddress = findViewById(R.id.textViewAddress);
        TextView textViewNameOfConsultant = findViewById(R.id.textViewNameOfConsultant);
        TextView textViewEnquiryLocation = findViewById(R.id.textViewEnquiryLocation);

        ImageView imageViewCall = findViewById(R.id.imageViewCall);
        ImageView imageViewCallConsultant = findViewById(R.id.imageViewCallConsultant);

        textViewNameOfEnquiry.setText(enquiryRequest.getNameOfEnquiry());
        textViewVehicleName.setText(String.format("%s %s", enquiryRequest.getMake(),
                enquiryRequest.getModel()));
        textViewContactNo.setText(enquiryRequest.getContactNo());
        textViewAddress.setText(enquiryRequest.getLocation());
        textViewNameOfConsultant.setText(enquiryRequest.getSalesconsultantname());
        textViewEnquiryLocation.setText(enquiryRequest.getEnquiryLocation());
        strLocation = enquiryRequest.getLocation();

        dialog = new ProgressDialog(AppointmentDetailsActivity.this);
        dialog.setMessage(getResources().getString(R.string.title_loading));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        buttonSchedule.setOnClickListener(this);
        buttonAssignRequest.setOnClickListener(this);
        buttonStartEvaluation.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        imageViewCall.setOnClickListener(this);
        imageViewCallConsultant.setOnClickListener(this);
        textViewTime.setOnClickListener(this);
        textViewDate.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        getLatLongFromAddress(strLocation);
    }

    private void getLatLongFromAddress(String fullAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(fullAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                LatLng sydney = new LatLng(latitude, longitude);
                map.addMarker(new MarkerOptions().position(sydney)
                        .title("Marker in Sydney"));
                map.moveCamera(CameraUpdateFactory.newLatLng(sydney));

                map.animateCamera(CameraUpdateFactory.zoomTo(15), 200, null);
    }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSchedule:
                if (textViewDate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_date))) {
                    CommonHelper.toast("Please select date", getBaseContext());
                } else if (textViewTime.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_time))) {
                    CommonHelper.toast("Please select time", getBaseContext());
                } else {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.REQUEST_ID, enquiryRequest.getRequestID()); //"ENQ19A000142"
                    params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.EVALUATIOR_ID, SessionUserDetails.getInstance().getUserID());// "MV010107");//evaluatorId
                    params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.APPOINTMENT_STATUS, ChampionConstants.APPOINTMENT_STATUS_CONFIRM);// "CNF");//appointmentStatus
                    params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.APPOINTMENT_DATE_TIME, textViewDateInvisible.getText() + " " + textViewTimeHidden.getText());//"06/08/2018 12:00:00");//appointmentDateTime
                    params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.CANCELLATION_REASON_ID, "");//"06/08/2018 12:00:00");//appointmentDateTime
                    appointmentDetailsPresenter.updateAppointmentDetails(params);
                }
                break;
            case R.id.buttonAssignRequest:
                if (textViewDate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_date))) {
                    CommonHelper.toast("Please select date", getBaseContext());
                } else if (textViewTime.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_time))) {
                    CommonHelper.toast("Please select time", getBaseContext());
                } else {
                    ArrayList<EnquiryRequest> enquiryRequestArrayList = new ArrayList<>();
                    enquiryRequest.setDateOfAppointment(textViewDateInvisible.getText() + " " + textViewTimeHidden.getText());
                    enquiryRequestArrayList.add(enquiryRequest);

                    finish();
                    Intent intentSelectEvaluator = new Intent(AppointmentDetailsActivity.this, SelectEvaluatorActivity.class);
                    intentSelectEvaluator.putParcelableArrayListExtra(ChampionConstants.ASSIGN_REQUEST_LIST_MODEL_KEY, enquiryRequestArrayList);
                    startActivity(intentSelectEvaluator);
                }
                break;
            case R.id.buttonStartEvaluation:
                if (textViewDate.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_date))) {
                    CommonHelper.toast("Please select date", getBaseContext());
                } else if (textViewTime.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_time))) {
                    CommonHelper.toast("Please select time", getBaseContext());
                } else {
                    ArrayList<EnquiryRequest> enquiryRequestArrayList = new ArrayList<>();
                    enquiryRequestArrayList.add(enquiryRequest);

                    appointmentDetailsPresenter.assignRequestToEvaluator(enquiryRequestArrayList,
                            textViewDateInvisible.getText().toString() + " " +
                                    textViewTimeHidden.getText());
                }
                break;
            case R.id.buttonCancel:
                dialogsHelper.showCancelAppointmentDialog(getBaseContext());
                break;
            case R.id.imageViewCall:
                if (enquiryRequest.getContactNo() != null) {
                    dialogsHelper.callDialog(enquiryRequest.getContactNo());
                }
                break;
            case R.id.imageViewCallConsultant:
                if (enquiryRequest.getSalesconsultantContactnum() != null) {
                    dialogsHelper.callDialog(enquiryRequest.getSalesconsultantContactnum());
                }
                break;
            case R.id.textViewDate:
                new DatePickerHelper(this,textViewDate, textViewDateInvisible, true);
                break;
            case R.id.textViewTime:
                new TimePickerHelper(this, R.id.textViewTime, R.id.tableRowTime, R.id.textViewTimeHidden);
                break;
        }
    }

    @Override
    public void onSuccess() {
        finish();
        Intent intentAssignRequestList = new Intent(AppointmentDetailsActivity.this,
                AssignRequestListActivity.class);
        startActivity(intentAssignRequestList);
    }

    @Override
    public void onSuccess(HashMap<String, String> hashMapValues) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.REQUEST_ID, enquiryRequest.getRequestID()); //"ENQ19A000142"
            params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.EVALUATIOR_ID, SessionUserDetails.getInstance().getUserID());// "MV010107");//evaluatorId
            params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.APPOINTMENT_STATUS, ChampionConstants.APPOINTMENT_STATUS_CANCEL);// "CNF");//appointmentStatus
            params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.APPOINTMENT_DATE_TIME, "");//"06/08/2018 12:00:00");//appointmentDateTime
            params.put(com.mahindra.mitra20.evaluator.constants.WebConstants.appointmentConfirm.CANCELLATION_REASON_ID, hashMapValues.get("value"));//"06/08/2018 12:00:00");//appointmentDateTime

            dialog.show();
            appointmentDetailsPresenter.updateAppointmentDetails(params);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNeutral() {

    }

    @Override
    public void uploadSuccess(int requestID, String data) {
        dialog.hide();
        switch (requestID) {
            case WebConstants.AssignRequestToEvaluator.REQUEST_CODE:
                ScheduleAppoinment scheduleAppoinmet = new ScheduleAppoinment();
                scheduleAppoinmet.setStrCustomerName(enquiryRequest.getNameOfEnquiry());
                scheduleAppoinmet.setStrDate(textViewDateInvisible.getText().toString());
                scheduleAppoinmet.setStrTime(textViewTime.getText().toString());
                scheduleAppoinmet.setStrVehicleName(enquiryRequest.getMake() + " " + enquiryRequest.getModel());
                scheduleAppoinmet.setStrLocation(enquiryRequest.getLocation());
                scheduleAppoinmet.setStrContactNo(enquiryRequest.getContactNo());
                scheduleAppoinmet.setStrMake(enquiryRequest.getMake());
                scheduleAppoinmet.setPincode(enquiryRequest.getPinCode());
                scheduleAppoinmet.setRequestID(enquiryRequest.getRequestID());
                scheduleAppoinmet.setRequestStatus(enquiryRequest.getRequestStatus());
                scheduleAppoinmet.setStrModel(enquiryRequest.getModel());
                scheduleAppoinmet.setStatus(enquiryRequest.getRequestStatus());

                finish();
                String id = appointmentDetailsPresenter.getNextEvaluationId();
                Intent intentStartEvaluation = new Intent(AppointmentDetailsActivity.this, NewEvaluationActivity.class);
                intentStartEvaluation.putExtra("scheduleAppoinment", scheduleAppoinmet);
                intentStartEvaluation.putExtra("NewEvaluationId", id);
                intentStartEvaluation.putExtra("purpose", "Direct Buy");
                startActivity(intentStartEvaluation);
                break;
            case 7:
                finish();
                Intent intentAppointmentConfirmation = new Intent(AppointmentDetailsActivity.this,
                        AppointmentConfirmationActivity.class);
                startActivity(intentAppointmentConfirmation);
                break;
            case 2:
                CommonHelper.toast(getResources().getString(R.string.cancel_appointment_successfully), getApplicationContext());
                finish();
                Intent intentDashboard = new Intent(this, ChampionDashboardActivity.class);
                intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentDashboard);
                break;
        }
    }

    @Override
    public void uploadFailed(int requestID, String data) {
        dialog.hide();
        CommonHelper.toast(data, getBaseContext());
    }
}
