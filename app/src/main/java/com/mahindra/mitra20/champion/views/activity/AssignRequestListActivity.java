package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.AssignRequestListIn;
import com.mahindra.mitra20.champion.presenter.AssignRequestListPresenter;
import com.mahindra.mitra20.champion.views.adapters.AssignRequestSalesAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import java.util.ArrayList;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.ASSIGN_REQUEST_MODEL_KEY;

public class AssignRequestListActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, AssignRequestListIn, ContactIn,
        ItemClickListener, SearchView.OnQueryTextListener {

    private ChampionHelper championHelper;
    private ArrayList<EnquiryRequest> enquiryRequestArrayList;
    private SwipeRefreshLayout swipeContainer;
    private AssignRequestListPresenter assignRequestListPresenter;
    private SearchView searchViewFilter;
    public AssignRequestSalesAdapter assignRequestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_assign_request_list);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchViewFilter.clearFocus();
        searchViewFilter.setQuery("", false);
        enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(ChampionConstants.OPEN_STATUS);
        this.assignRequestAdapter.updateList(enquiryRequestArrayList);
        assignRequestAdapter.notifyDataSetChanged();
    }

    public void initUI() {
        CommonHelper.settingCustomToolBar(this, "Assign Request");
        assignRequestListPresenter = new AssignRequestListPresenter(this, this);
        championHelper = new ChampionHelper(getBaseContext());

        enquiryRequestArrayList = championHelper.getEnquiryRequestDetails(ChampionConstants.OPEN_STATUS);

        swipeContainer = findViewById(R.id.swipeContainer);

        EmptyRecyclerView recyclerViewAssignRequest = findViewById(R.id.recyclerViewAssignRequest);
        searchViewFilter = findViewById(R.id.searchViewFilter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewAssignRequest.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewAssignRequest.setEmptyView(findViewById(R.id.textViewEmpty));
        assignRequestAdapter = new AssignRequestSalesAdapter(getApplicationContext(), this, true);
        assignRequestAdapter.setAssignRequestContents(enquiryRequestArrayList);
        recyclerViewAssignRequest.setAdapter(assignRequestAdapter);
        assignRequestAdapter.setContactListener(this);
        assignRequestAdapter.setClickListener(this);

        setupSearchView();
        assignRequestAdapter.notifyDataSetChanged();
        swipeContainer.setOnRefreshListener(this);
    }

    private void setupSearchView() {
        searchViewFilter.setIconifiedByDefault(false);
        searchViewFilter.setOnQueryTextListener(this);
        searchViewFilter.setSubmitButtonEnabled(true);
        searchViewFilter.setFocusable(false);

        searchViewFilter.setQueryHint("Search...");
    }

    @Override
    public void onRefresh() {
        if (CommonHelper.isConnectingToInternet(getBaseContext())) {
            assignRequestListPresenter.downloadEnquiryMaster(ChampionConstants.OPEN_STATUS);
        } else {
            CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> _enquiryRequestArrayList) {
        switch (requestId) {
            case 1:
                if (_enquiryRequestArrayList != null && !_enquiryRequestArrayList.isEmpty()) {
                    enquiryRequestArrayList = _enquiryRequestArrayList;
                    this.assignRequestAdapter.updateList(_enquiryRequestArrayList);
                    assignRequestAdapter.notifyDataSetChanged();
                }
                break;
        }

        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onCallPress(String contactNo) {
        new DialogsHelper(AssignRequestListActivity.this).callDialog(contactNo);
    }

    @Override
    public void onMessagePress(String contactNo) {
        CommonHelper.sendMessage(contactNo, AssignRequestListActivity.this);
    }

    @Override
    public void onClick(View view, int position) {
        int id = view.getId();
        if (id == R.id.layout_item_assign_request) {
            if (enquiryRequestArrayList != null && !enquiryRequestArrayList.isEmpty()) {
                Intent intentAssignRequest = new Intent(AssignRequestListActivity.this, AppointmentDetailsActivity.class);
                intentAssignRequest.putExtra(ASSIGN_REQUEST_MODEL_KEY, enquiryRequestArrayList.get(position));
                startActivity(intentAssignRequest);
                searchViewFilter.clearFocus();
            }
        }
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (assignRequestAdapter != null)
            assignRequestAdapter.filter(newText);

        return true;
    }
}