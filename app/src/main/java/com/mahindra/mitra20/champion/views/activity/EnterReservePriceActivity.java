package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.OnClickDialog;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.presenter.SendToAuctionIn;
import com.mahindra.mitra20.champion.presenter.SendToAuctionPresenter;
import com.mahindra.mitra20.databinding.LayoutEnterReservePriceBinding;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.evaluator.views.fragment.ViewEvaluationFragment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.ChampionHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import java.util.HashMap;
import java.util.Objects;

public class EnterReservePriceActivity extends AppCompatActivity implements View.OnClickListener,
        OnClickDialog, SendToAuctionIn {

    private LayoutEnterReservePriceBinding binding;
    private DialogsHelper dialogsHelper;
    private SendToAuctionPresenter sendToAuctionPresenter;
    private EnquiryRequest enquiryRequest;
    private NewEvaluation newEvaluation;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_enter_reserve_price);

        initUI();
    }

    public void initUI() {
        CommonHelper.settingCustomToolBar(this, "Dealer X Mart Price");
        try {
            View customView = Objects.requireNonNull(getSupportActionBar()).getCustomView();
            ImageView ivEdit = customView.findViewById(R.id.ic_edit);
            ivEdit.setVisibility(View.VISIBLE);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openEditEvaluationForm();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.title_loading));

        enquiryRequest = getIntent().getParcelableExtra(ChampionConstants.ASSIGN_REQUEST_MODEL_KEY);

        dialogsHelper = new DialogsHelper(this);
        dialogsHelper.setOnClickDialog(this);

        sendToAuctionPresenter = new SendToAuctionPresenter(this, this);

        binding.textViewNameOfEnquiry.setText(enquiryRequest.getNameOfEnquiry());
        binding.textViewReportID.setText(String.format("Report No %s",
                enquiryRequest.getEvaluationReportID()));
        binding.textViewRupees.setOnClickListener(this);
        getFromDraft();
    }

    private void openEditEvaluationForm() {
        if (newEvaluation != null) {
            String id = sendToAuctionPresenter.getNextEvaluationId();
            Intent intent = new Intent(EnterReservePriceActivity.this,
                    NewEvaluationActivity.class);
            intent.putExtra("NewEvaluation", newEvaluation);
            intent.putExtra("NewEvaluationId", id);
            intent.putExtra("status", NewEvaluationActivity.KEY_EDIT_EVAL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    void getFromDraft() {
        EvaluatorHelper evaluatorHelper = new EvaluatorHelper(this);
        String draftEvaluationId = getIntent().getStringExtra("NewEvaluationId");
        String requestId = "";
        newEvaluation = evaluatorHelper.getEvaluationDetailsById(requestId, draftEvaluationId);
        if (newEvaluation.getRequestId() != null || newEvaluation.getId() != null) {
            binding.textViewEvaluatorName.setText(newEvaluation.getEvaluatorName());
            binding.textViewVehicleName.setText(String.format("%s %s",
                    newEvaluation.getMake(), newEvaluation.getModel()));

            Bundle bundle = new Bundle();
            bundle.putParcelable(CommonHelper.EXTRA_EVALUATION, newEvaluation);
            bundle.putBoolean(CommonHelper.EXTRA_ARE_IMAGES_ONLINE, true);
            ViewEvaluationFragment viewEvaluationFragment =
                    new ViewEvaluationFragment();
            viewEvaluationFragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(binding.fragmentContainer.getId(),
                    viewEvaluationFragment).commit();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.imageViewCustomerCall:
                new DialogsHelper(EnterReservePriceActivity.this).callDialog(enquiryRequest.getContactNo());
                break;
            case R.id.imageViewEvaluatorCall:
                new DialogsHelper(EnterReservePriceActivity.this).callDialog(newEvaluation.getEvaluatorContactNo());
                break;
            case R.id.textViewRupees:
                dialogsHelper.showPickerSendToAuctionDialog(EnterReservePriceActivity.this,
                        enquiryRequest.getEvaluationReportID(), enquiryRequest.getDateOfAppointment());
                break;
        }
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onSuccess(HashMap<String, String> hashMapValues) {
        dialog.show();
        sendToAuctionPresenter.sendEnquiryToAuction(hashMapValues);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNeutral() {

    }

    @Override
    public void uploadSuccess(int requestID, String data) {
        CommonHelper.toast(data, getBaseContext());
        dialog.dismiss();

        ChampionHelper championHelper = new ChampionHelper(getApplicationContext());
        championHelper.updateEnquiryStatus(ChampionConstants.RESERVE_PRICE,
                enquiryRequest.getRequestID(), enquiryRequest.getEvaluationReportID());

        finish();
        Intent intentDashboard = new Intent(this, ChampionDashboardActivity.class);
        intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentDashboard);
    }

    @Override
    public void uploadFailed(int requestID, String data) {
        CommonHelper.toast(data, getBaseContext());
        dialog.dismiss();
    }
}