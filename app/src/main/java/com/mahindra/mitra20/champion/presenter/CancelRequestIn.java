package com.mahindra.mitra20.champion.presenter;

/**
 * Created by user on 9/10/2018.
 */

public interface CancelRequestIn {

    void uploadSuccess(int requestID, String data);

    void uploadFailed(int requestID, String data);

}
