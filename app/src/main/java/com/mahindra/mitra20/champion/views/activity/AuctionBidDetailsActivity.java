package com.mahindra.mitra20.champion.views.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.interfaces.OnClickDialog;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.HighestBidder;
import com.mahindra.mitra20.champion.models.comparator.HighestBidderComparator;
import com.mahindra.mitra20.champion.presenter.AssignRequestListIn;
import com.mahindra.mitra20.champion.presenter.AssignRequestListPresenter;
import com.mahindra.mitra20.champion.presenter.PlaceBidIn;
import com.mahindra.mitra20.champion.presenter.PlaceBidPresenter;
import com.mahindra.mitra20.champion.presenter.SetOfferPriceIn;
import com.mahindra.mitra20.champion.presenter.SetOfferPricePresenter;
import com.mahindra.mitra20.champion.views.adapters.myauctions.BidderDetailsAdapter;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.helper.AuctionHelper;
import com.mahindra.mitra20.module_broker.helper.StringHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.SELECTED_AUCTION_DATA;

public class AuctionBidDetailsActivity extends AppCompatActivity implements View.OnClickListener,
        OnClickDialog, PlaceBidIn, AssignRequestListIn, SetOfferPriceIn, ItemClickListener {

    private DialogsHelper dialogsHelper;
    private PlaceBidPresenter placeBidPresenter;
    private TextView textViewCustomerName;
    private TextView textViewVehicleName;
    private AuctionDetail auctionDetail;
    private CountDownTimer countDownTimer;
    private AssignRequestListPresenter assignRequestListPresenter;
    private SetOfferPricePresenter setOfferpricePresenter;
    private TableRow tableRowHeader;

    private int index;
    private TextView textViewTimer;
    private List<AuctionLIST> listList;
    private ArrayList<HighestBidder> highestBidder = new ArrayList<>();
    private ProgressDialog dialog;
    int tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_auction_bid_details);

        initUI();
    }

    public void cancelTimer(){
        countDownTimer.cancel();
        countDownTimer = null;
    }

    private void initUI() {
        CommonHelper.settingCustomToolBar(this, "Auction Bid Details");
        tab = getIntent().getIntExtra("key", -1);
        assignRequestListPresenter = new AssignRequestListPresenter(this, this);
        setOfferpricePresenter = new SetOfferPricePresenter(this, this);

        textViewCustomerName = findViewById(R.id.textViewCustomerName);
        textViewVehicleName = findViewById(R.id.textViewVehicleName);
        TextView textViewHighestPrice = findViewById(R.id.textViewHighestPrice);
        TextView titleResult = findViewById(R.id.titleResult);
        LinearLayout linearLayoutBidDetails = findViewById(R.id.linearLayoutBidDetails);
        ImageView imageViewDetails = findViewById(R.id.imageViewDetails);
        ImageView imageViewCall = findViewById(R.id.imageViewCall);
        ImageView imageViewVehicle = findViewById(R.id.imageViewVehicle);
        TextView textViewNoOfOwners = findViewById(R.id.textViewNoOfOwners);
        TextView textViewOdometer = findViewById(R.id.textViewOdometerReading);
        RatingBar ratingFeedback = findViewById(R.id.ratingFeedback);
        TextView vehicleUsage = findViewById(R.id.textViewVehicleUsage);

        auctionDetail = getIntent().getParcelableExtra(SELECTED_AUCTION_DATA);

        dialog = new ProgressDialog(AuctionBidDetailsActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Wait while loading");

        textViewCustomerName.setText(auctionDetail.getVehOwnerName());
        textViewVehicleName.setText(auctionDetail.getVehicleName());
        vehicleUsage.setText(String.format("Vehicle Usage - %s",
                auctionDetail.getVehicleUsage()));
        textViewTimer = findViewById(R.id.textViewTimer);
        textViewNoOfOwners.setText(String.format("No of owners : %s", auctionDetail.getNoOfOwners()));
        textViewOdometer.setText(String.format("Odometer(kms) : %s", auctionDetail.getOdometerReading()));
        try {
            if (auctionDetail.getFeedbackRating().trim().length() > 0)
                ratingFeedback.setRating(Float.parseFloat(auctionDetail.getFeedbackRating()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (null != auctionDetail.getVehiclePicURL()) {
            if (!auctionDetail.getVehiclePicURL().isEmpty()) {
                Glide.with(imageViewVehicle.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(auctionDetail.getVehiclePicURL()))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(imageViewVehicle);
            } else {
                Glide.with(imageViewVehicle.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(imageViewVehicle);
            }
        }

        Button buttonBid = findViewById(R.id.buttonBid);
        tableRowHeader = findViewById(R.id.tableRowHeader);
        dialogsHelper = new DialogsHelper(this);

        if (tab == 0) {
            titleResult.setText(auctionDetail.getBidResult());
            textViewHighestPrice.setVisibility(View.GONE);
            buttonBid.setVisibility(View.VISIBLE);
            linearLayoutBidDetails.setVisibility(View.GONE);

            long seconds = Integer.parseInt(auctionDetail.getRemainingTimeInSeconds());
            long milli = seconds * 1000;
            countDownTimer = null;
            countDownTimer = new CountDownTimer(milli, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long seconds = millisUntilFinished / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    String time = StringHelper.getDoubleDigit(String.valueOf(hours % 24)) + ":" +
                            StringHelper.getDoubleDigit(String.valueOf(((minutes % 60) > 9)
                                    ? minutes % 60 : "0" + (minutes % 60)) + ":" +
                                    StringHelper.getDoubleDigit(String.valueOf(seconds % 60)));
                    textViewTimer.setText(time);

                    int colorForTimer = AuctionHelper.getColorForTimer(millisUntilFinished);
                    Drawable mDrawable = getResources().getDrawable(R.drawable.button_half_circular_green);
                    mDrawable.setColorFilter(new
                            PorterDuffColorFilter(colorForTimer, PorterDuff.Mode.SRC_IN));
                    tableRowHeader.setBackground(mDrawable);
                }

                @Override
                public void onFinish() {
                    cancelTimer();
                }
            };

            countDownTimer.start();
        } else if (tab == 1) {
            textViewTimer.setText(auctionDetail.getStartTimestamp());
            textViewHighestPrice.setText("Auction yet to start");
            buttonBid.setVisibility(View.GONE);
            linearLayoutBidDetails.setVisibility(View.GONE);
        } else if (tab == 2) {
            if (auctionDetail.getHighestBidder().equalsIgnoreCase(ChampionConstants.Y)) {
                textViewTimer.setText("Win");
                tableRowHeader.setBackground(getResources().getDrawable(R.drawable.green_bg_right_corner_rounded));
            } else {
                textViewTimer.setText("Lost");
                tableRowHeader.setBackground(getResources().getDrawable(R.drawable.red_bg_right_corner_rounded));
            }

            textViewHighestPrice.setText(auctionDetail.getHighestPrice());
            buttonBid.setVisibility(View.VISIBLE);
            buttonBid.setText("Resend for Auction");
            linearLayoutBidDetails.setVisibility(View.VISIBLE);

            setLayoutData();
        }

        placeBidPresenter = new PlaceBidPresenter(this, this);

        dialogsHelper.setOnClickDialog(this);
        buttonBid.setOnClickListener(this);
        imageViewDetails.setOnClickListener(this);
        imageViewCall.setOnClickListener(this);
    }

    private void setLayoutData() {
        TextView textViewCall = findViewById(R.id.textViewCall);
        TextView textViewMITRAName = findViewById(R.id.textViewMITRAName);
        TextView textViewMITRAPrice = findViewById(R.id.textViewMITRAPrice);
        TextView textViewRequestID = findViewById(R.id.textViewRequestID);
        textViewCustomerName = findViewById(R.id.textViewCustomerName);
        textViewVehicleName = findViewById(R.id.textViewVehicleName);

        auctionDetail = getIntent().getParcelableExtra(ChampionConstants.SELECTED_AUCTION_DATA);
        listList = auctionDetail.getLIST();

        if (!listList.isEmpty()) {
            index = getHighestPriceIndex(listList);
            textViewMITRAName.setText(listList.get(index).getMITRAName());
            textViewMITRAPrice.setText(auctionDetail.getHighestPrice());
            textViewRequestID.setText(auctionDetail.getRequestID());
            textViewCustomerName.setText(auctionDetail.getVehOwnerName());
            textViewVehicleName.setText(auctionDetail.getVehicleName());

            for (int i = 0; i < listList.size(); i++) {
                if (i != index) {
                    AuctionLIST auctionLIST = listList.get(i);
                    highestBidder.add(new HighestBidder(auctionLIST.getMITRAId(),
                            auctionLIST.getMITRAName(), auctionLIST.getBidPrice(),
                            auctionLIST.getDeductedPrice(), auctionLIST.getCommissionPrice(),
                            auctionLIST.getPhoneNum(), false, false));
                }
            }

            Collections.sort(highestBidder, new HighestBidderComparator());

            addCars24MFCW();
            RecyclerView recyclerViewBidderList = findViewById(R.id.recyclerViewBidderList);
            BidderDetailsAdapter bidderAdapter = new BidderDetailsAdapter(getBaseContext(), highestBidder);
            recyclerViewBidderList.setAdapter(bidderAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
            recyclerViewBidderList.setLayoutManager(linearLayoutManager);
            textViewCall.setOnClickListener(this);
        }
    }

    private void addCars24MFCW() {
        try {
            ArrayList<String> arrayListMITRANames = new ArrayList<>();

            for (int i = 0; i < listList.size(); i++) {
                AuctionLIST auctionLIST = listList.get(i);
                arrayListMITRANames.add(auctionLIST.getMITRAId());
            }

            int impaneledCount = 0;
            for (int i = 0; i < listList.size(); i++) {
                AuctionLIST auctionLIST = listList.get(i);
                if ( !auctionLIST.getMITRAId().equalsIgnoreCase("MFCW") &&
                        !auctionLIST.getMITRAId().equalsIgnoreCase("BRKA002436") &&
                        !auctionLIST.getMITRAId().equalsIgnoreCase(SessionUserDetails.getInstance().getUserID())) {
                    impaneledCount++;
                }
            }

            if (impaneledCount == 0) {
                highestBidder.add(new HighestBidder("Empanelled Associate",
                        "Empanelled Associate", "--", "--",
                        "--", "", false, false));
            }

            if (!arrayListMITRANames.contains("BRKA002436")) {
                highestBidder.add(new HighestBidder("Cars24", "Cars24",
                        "--", "--", "--", "",
                        false, false));
            }

            if (!arrayListMITRANames.contains("MFCW")) {
                if (auctionDetail.getNegotiatorContactName().isEmpty()) {
                    highestBidder.add(new HighestBidder(ChampionConstants.MFCW_ID,
                            "Mahindra First Choice", "", "--",
                            "--", auctionDetail.getNegotiatorContactNumber(),
                            false, false));
                } else {
                    highestBidder.add(new HighestBidder(ChampionConstants.MFCW_ID,
                            "Mahindra First Choice (" +
                                    auctionDetail.getNegotiatorContactName() + ")", "",
                            "--", "--",
                            auctionDetail.getNegotiatorContactNumber(), false, false));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getHighestPriceIndex(List<AuctionLIST> list) {
        int highestPrice = 0;
        try {
            int[] listhighestPrice = new int[list.size()];
            try {
                for (int i = 0; i < list.size(); i++) {
                    AuctionLIST auctionLIST = list.get(i);
                    listhighestPrice[i] = Integer.parseInt(auctionLIST.getBidPrice());
                }
                highestPrice = getHighestValueIndex(listhighestPrice);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return highestPrice;
    }

    private int getHighestValueIndex(int[] listhighestPrice) {
        int max = listhighestPrice[0];
        int index = 0;
        for(int i = 1; i < listhighestPrice.length;i++)
        {
            if(listhighestPrice[i] > max)
            {
                max = listhighestPrice[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.buttonBid:
                if (tab == 0) {
                    dialogsHelper.showRebidDialog(
                            auctionDetail.getMITRAPriceBasePrice(), getApplicationContext());
                } else if (tab == 2) {
                    dialogsHelper.showPickerSendToAuctionDialog(AuctionBidDetailsActivity.this,
                            auctionDetail.getRequestID(), null);
                }
                break;
            case R.id.imageViewDetails:
                dialog.show();
                setOfferpricePresenter.getAuctionMasterByID(auctionDetail.getAuctionID());
                break;
            case R.id.textViewCall:
                if (listList.get(index).getPhoneNum() != null && !listList.get(index).getPhoneNum().isEmpty()) {
                    new DialogsHelper(AuctionBidDetailsActivity.this).callDialog(
                            listList.get(index).getPhoneNum());
                } else {
                    CommonHelper.toast("Contact details unavailable", getApplicationContext());
                }
                break;
            case R.id.imageViewCall:
                if (auctionDetail.getContactNo() != null && !auctionDetail.getContactNo().isEmpty()) {
                    new DialogsHelper(AuctionBidDetailsActivity.this).callDialog(
                            auctionDetail.getContactNo());
                } else {
                    CommonHelper.toast("Contact details unavailable", getApplicationContext());
                }
                break;
        }
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onSuccess(HashMap<String, String> hashMapValues) {
        if (!hashMapValues.containsKey(WebConstants.SendToAuction.reservedPrice)) {
            hashMapValues.put(WebConstants.PlaceBid.AuctionID, auctionDetail.getAuctionID());
            placeBidPresenter.placeBid(hashMapValues);
        } else {
            dialog.show();
            placeBidPresenter.sendEnquiryToAuction(hashMapValues);
        }
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNeutral() {

    }

    @Override
    public void uploadSuccess(int requestID, String data) {
        if (requestID == WebConstants.PlaceBid.REQUEST_CODE) {
            CommonHelper.toast(getResources().getString(R.string.bid_placed_success), getApplicationContext());
        } else if (requestID == WebConstants.SendToAuction.REQUEST_CODE) {
            finish();
            Intent intentDashboard = new Intent(this, ChampionDashboardActivity.class);
            intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentDashboard);
        }
    }

    @Override
    public void uploadSuccess(int requestID, AuctionDetail auctionDetail) {
        HashMap<String, String> params = new HashMap<>();
        params.put(WebConstants.GetEvaluationReport.EvaluationReportID, auctionDetail.getAuctionID());
        params.put(WebConstants.GetEvaluationReport.RequestID, auctionDetail.getRequestID());
        params.put(WebConstants.GetEvaluationReport.UserID, SessionUserDetails.getInstance().getUserID());
        assignRequestListPresenter.downloadEvaluationReport(params);
    }

    @Override
    public void downloadSuccess(int requestID, NewEvaluation newEvaluation) {

    }

    @Override
    public void uploadFailed(int requestID, String data) {
        dialog.hide();
        CommonHelper.toast(data, getApplicationContext());
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> _enquiryRequestArrayList) {
        dialog.hide();
        String evaluationReportID = _enquiryRequestArrayList.get(0).getEvaluationReportID();
        Intent intentEnterReservePrice = new Intent(this, EvaluationReportDetailsChampActivity.class);
        intentEnterReservePrice.putExtra("NewEvaluationId", evaluationReportID);
        intentEnterReservePrice.putExtra("EvaluationReportID", auctionDetail.getRequestID());
        intentEnterReservePrice.putExtra("status", "Completed");
        startActivity(intentEnterReservePrice);
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        dialog.hide();
    }

    @Override
    public void onClick(View view, int position) {
        int id = view.getId();
        if (id == R.id.textViewCall) {
            if (highestBidder.get(position).getContactNo() != null &&
                    !highestBidder.get(position).getContactNo().isEmpty()) {
                new DialogsHelper(AuctionBidDetailsActivity.this).callDialog(
                        highestBidder.get(position).getContactNo());
            } else {
                CommonHelper.toast("Contact details unavailable", getApplicationContext());
            }
        }
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}