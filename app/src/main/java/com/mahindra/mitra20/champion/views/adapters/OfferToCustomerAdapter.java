package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;

import java.util.ArrayList;


/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class OfferToCustomerAdapter extends RecyclerView.Adapter<OfferToCustomerAdapter.ViewHolder> {

    private ArrayList<AuctionDetail> myAuctionsContents;
    Context context;
    ItemClickListener clickListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textViewTime, textViewHighestBid,textViewXmartPrice,textViewCustomerName,textViewVehicleName;
        public ImageView imageViewCall, imageViewDetails;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewTime = v.findViewById(R.id.textViewTime);
            //textViewHighestBid = v.findViewById(R.id.textViewHighestBid);
            textViewXmartPrice = v.findViewById(R.id.textViewXmartPrice);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewDetails = v.findViewById(R.id.imageViewDetails);

            imageViewCall.setOnClickListener(this);
            imageViewDetails.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public void setmonClick(ItemClickListener monClick) {
        this.clickListener = monClick;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public OfferToCustomerAdapter(Context _context, ArrayList<AuctionDetail> _myAuctionsContents) {
        myAuctionsContents = _myAuctionsContents;
        context = _context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OfferToCustomerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_my_live_auctions, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
            holder.textViewTime.setText(myAuctionsContents.get(position).getStartTimestamp());
            //holder.textViewHighestBid.setText(myAuctionsContents.get(position).getHighestPrice());
            holder.textViewXmartPrice.setText(myAuctionsContents.get(position).getMITRAPriceBasePrice());
            holder.textViewCustomerName.setText(myAuctionsContents.get(position).getVehOwnerName());
            holder.textViewVehicleName.setText(myAuctionsContents.get(position).getVehicleName());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return myAuctionsContents.size();
    }
}