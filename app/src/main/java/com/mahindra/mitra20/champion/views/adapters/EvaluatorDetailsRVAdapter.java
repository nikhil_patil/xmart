package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mahindra.mitra20.champion.interfaces.OnEvaluatorwiseEnquiryListItemClickListener;
import com.mahindra.mitra20.champion.models.EvaluatorDetailsModel;
import com.mahindra.mitra20.champion.models.EvaluatorWiseEnquiry;
import com.mahindra.mitra20.champion.views.activity.EvaluatorwiseEnquiryListActivity;
import com.mahindra.mitra20.databinding.EvaluatorwiseStatusItemBinding;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;

import java.util.ArrayList;
import java.util.List;

public class EvaluatorDetailsRVAdapter extends RecyclerView.Adapter<EvaluatorDetailsRVAdapter.LeadsViewHolder>
        implements OnEvaluatorwiseEnquiryListItemClickListener {

    private List<EvaluatorDetailsModel> evaluatorDetailsModels;
    private Context context;

    public EvaluatorDetailsRVAdapter(Context context, List<EvaluatorDetailsModel> evaluatorDetailsModels) {
        this.evaluatorDetailsModels = evaluatorDetailsModels;
        this.context = context;
    }

    @NonNull
    @Override
    public LeadsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LeadsViewHolder(EvaluatorwiseStatusItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LeadsViewHolder holder, int position) {
        final EvaluatorDetailsModel lead = evaluatorDetailsModels.get(position);
        holder.binding.setEvaluator(lead);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        if (evaluatorDetailsModels != null) {
            return evaluatorDetailsModels.size();
        }
        return 0;
    }

    @Override
    public void onAnyCountClick(List<EvaluatorWiseEnquiry> list, int code) {
        String pageTitle = "";
        switch (code) {
            case 1:
                pageTitle = "Assigned Enquiries";
                break;
            case 2:
                pageTitle = "Evaluation Pending";
                break;
            case 3:
                pageTitle = "Evaluation Completed";
                break;
            case 4:
                pageTitle = "Auctions Done";
                break;
            case 5:
                pageTitle = "Procurement Done";
                break;
        }
        if (null != list && !list.isEmpty()) {
            Intent intent = new Intent(context, EvaluatorwiseEnquiryListActivity.class);
            intent.putExtra("toolbar_title", pageTitle);
            ArrayList<EvaluatorWiseEnquiry> myList = new ArrayList<>(list);
            intent.putParcelableArrayListExtra("list", myList);
            context.startActivity(intent);
        } else {
            try {
                if (SessionUserDetails.getInstance().getUserType() == 0 && code != 5)
                    CommonHelper.toast("List not available to view", context);
                else
                    CommonHelper.toast("List is empty", context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCallPress(String number) {
        CommonHelper.makeCall(number, context);
    }

    class LeadsViewHolder extends RecyclerView.ViewHolder {
        EvaluatorwiseStatusItemBinding binding;

        LeadsViewHolder(EvaluatorwiseStatusItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setClickListener(EvaluatorDetailsRVAdapter.this);
        }
    }
}