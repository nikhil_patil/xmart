package com.mahindra.mitra20.champion.interfaces;

import java.util.HashMap;

/**
 * Created by user on 8/6/2018.
 */

public interface OnClickDialog {

    void onSuccess();

    void onSuccess(HashMap<String, String> hashMapValues);

    void onCancel();

    void onNeutral();

}
