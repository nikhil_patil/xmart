package com.mahindra.mitra20.champion.constants;

/**
 * Created by user on 8/6/2018.
 */

public interface ChampionConstants {

    int DASHBOARD_DATA_LIMIT = 5;

    String ASSIGN_REQUEST_MODEL_KEY = "ASSIGN_REQUEST";
    String ASSIGN_REQUEST_LIST_MODEL_KEY = "ASSIGN_LIST_REQUEST";
    String SELECTED_AUCTION_DATA = "SELECTED_AUCTION_DATA";
    String SELECTED_EVALUATOR_LIST_MODEL_KEY = "SELECTED_EVALUATOR_LIST_MODEL_KEY";
    String XMART = "XMart";

    String OPEN_STATUS = "OPN";
    String OPEN_STATUS_KEY = "OPN";

    String RESERVE_PRICE = "RES";
    String RESERVE_PRICE_STATUS_KEY = "RES";

    String OFFER_PRICE = "OFFR";
    String OFFER_PRICE_STATUS_KEY = "OFFR";

    String PROCURE_PRICE = "COM";
    String PROCURE_STATUS_KEY = "COM";

    String FOLLOWUP_CUSTOMER = "FOL";
    String FOLLOWUP_CUSTOMER_STATUS_KEY = "FOL";

    String EVALUATION_DONE = "EWA";
    String EVALUATION_DONE_KEY = "EWA";

    String CANCELLATION_DONE = "CAN";
    String CANCELLATION_DONE_KEY = "CAN";

    String APPROVAL_STATUS_APPROVE = "APV";
    String APPROVAL_STATUS_CANCEL = "REJ";

    String AUCTION_STATUS_KEY = "AUCTION_STATUS_KEY";
    String LIVE_STATUS = "LIV";
    String COMPLETED_STATUS = "COM";
    String BIDDING_STATUS = "PEN";

    String APPOINTMENT_STATUS_CANCEL = "CAN";
    String APPOINTMENT_STATUS_RESCHEDULE = "RES";
    String APPOINTMENT_STATUS_CONFIRM = "CNF";
    String MFCW_ID = "MFCW";
    String Y = "Y";
    String N = "N";

    String FROM_NEW_EVALUTION_SCREEN = "from_new_evalution_screen";
    String IS_DRAFT_SAVED = "is_draft_saved";
    String FROM_APPOINTMENT_DETAILS_SCREEN = "from_appointment_details_screen";
    String FROM_SCHEDULE_APPOINTMENT_SCREEN = "from_schedule_appointment_screen";
    String FROM_LOGIN_SCREEN = "from_login_screen";
    String FROM_DRAFT_LIST_ACTIVITY = "from_draft_list_activity";
}
