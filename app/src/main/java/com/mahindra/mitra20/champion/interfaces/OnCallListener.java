package com.mahindra.mitra20.champion.interfaces;

/**
 * Created by PATINIK-CONT on 01-Oct-19.
 */
public interface OnCallListener {

    void onCallPressed(String mobileNumber);
}
