package com.mahindra.mitra20.champion.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.interfaces.OnClickDialog;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DatePickerHelper;
import com.mahindra.mitra20.helper.CommonHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by CHAVME-CONT on 10/13/2018.
 */
public class DialogsHelper {
    private OnClickDialog onClickDialog;
    private Activity activity;
    private DateHelper dateHelper;

    //TODO till 11:30 pm
    private String[] arr24Hours = {"10:00:00", "10:15:00", "10:30:00", "10:45:00", "11:00:00", "11:15:00",
            "11:30:00", "11:45:00", "12:00:00", "12:15:00", "12:30:00", "12:45:00", "13:00:00",
            "13:15:00", "13:30:00", "13:45:00", "14:00:00", "14:15:00", "14:30:00", "14:45:00",
            "15:00:00", "15:15:00", "15:30:00", "15:45:00", "16:00:00", "16:15:00", "16:30:00",
            "16:45:00", "17:00:00", "17:15:00", "17:30:00", "17:45:00", "18:00:00", "18:15:00",
            "18:30:00", "18:45:00", "19:00:00", "19:15:00", "19:30:00", "19:45:00", "20:00:00",
            "20:15:00", "20:30:00", "20:45:00", "21:00:00", "21:15:00", "21:30:00", "21:45:00",
            "22:00:00", "22:15:00", "22:30:00", "22:45:00", "23:00:00", "23:15:00", "23:30:00"};

    private String[] arr12Hours = {"10:00AM", "10:15AM", "10:30AM", "10:45AM", "11:00AM", "11:15AM",
            "11:30AM", "11:45AM", "12:00PM", "12:15PM", "12:30PM", "12:45PM", "01:00PM", "01:15PM",
            "01:30PM", "01:45PM", "02:00PM", "02:15PM", "02:30PM", "02:45PM", "03:00PM", "03:15PM",
            "03:30PM", "03:45PM", "04:00PM", "04:15PM", "04:30PM", "04:45PM", "05:00PM", "05:15PM",
            "05:30PM", "05:45PM", "06:00PM", "06:15PM", "06:30PM", "06:45PM", "07:00PM", "07:15PM",
            "07:30PM", "07:45PM", "08:00PM", "08:15PM", "08:30PM", "08:45PM", "09:00PM", "09:15PM",
            "09:30PM", "09:45PM", "10:00PM", "10:15PM", "10:30PM", "10:45PM", "11:00PM", "11:15PM",
            "11:30PM"};

    public void setOnClickDialog(OnClickDialog onClickDialog) {
        this.onClickDialog = onClickDialog;
    }

    public DialogsHelper(Activity activity) {
        this.activity = activity;
        dateHelper = new DateHelper();
    }

    public void showPickerSendToAuctionDialog(final Activity context, final String requestID,
                                              final String sDateOfAppointment) {
        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_send_to_auction, null);

            final android.app.AlertDialog.Builder alertDialogBuilder =
                    new android.app.AlertDialog.Builder(activity);

            TextView textViewSendToAuction = promptsView
                    .findViewById(R.id.textViewSendToAuction);

            ImageView imageViewClose = promptsView
                    .findViewById(R.id.imageViewClose);

            final EditText editTextReservePrice = promptsView.findViewById(R.id.editTextReservePrice);
            final TextView editTextStartTime = promptsView.findViewById(R.id.editTextStartTime);
            final TextView editTextStartTimeHidden = promptsView.findViewById(R.id.editTextStartTimeHidden);
            final TextView editTextEndTime = promptsView.findViewById(R.id.editTextEndTime);
            final TextView editTextEndTimeHidden = promptsView.findViewById(R.id.editTextEndTimeHidden);
            final TextView editTextFromDate = promptsView.findViewById(R.id.editTextAuctionFromDate);
            final TextView editTextDateInvisible = promptsView.findViewById(R.id.editTextAuctionDateInvisible);

            editTextFromDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        new DatePickerHelper(context, editTextFromDate, editTextDateInvisible, true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            editTextDateInvisible.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (editTextDateInvisible.getText().toString().length() > 0) {
                        String strFromDate = editTextDateInvisible.getText().toString();
                        try {
                            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(strFromDate);
                            Calendar c = Calendar.getInstance();
                            c.setTime(date1);
                            c.add(Calendar.DATE, 2);
                            date1 = c.getTime();

                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            String reportDate = df.format(date1);

                            Date appointmentDate = dateHelper.convertStringToDate(reportDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            editTextStartTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pickerTime(editTextStartTime, editTextStartTimeHidden, editTextEndTime, editTextEndTimeHidden);
                }
            });

            editTextStartTimeHidden.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (editTextStartTimeHidden.getText().toString().length() > 0) {
                        String strFromDate = editTextStartTimeHidden.getText().toString();
                        try {

                            editTextEndTime.setText(strFromDate);
                            editTextEndTimeHidden.setText(arr24Hours[Arrays.asList(arr12Hours).indexOf(editTextStartTime.getText().toString())]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            editTextEndTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //new TimePickerHelper(context, editTextEndTime, tableRowEndTime, editTextEndTimeHidden);
                }
            });

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            textViewSendToAuction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    if (onClickDialog != null) {
                        if (editTextReservePrice.getText().toString().isEmpty()) {
                            CommonHelper.toast(context.getResources().getString(R.string.enter_reserve_price), context);
                        } else if (editTextFromDate.getText().toString().isEmpty()) {
                            CommonHelper.toast(context.getResources().getString(R.string.enter_auction_date), context);
                        } else if (editTextStartTime.getText().toString().isEmpty()) {
                            CommonHelper.toast(context.getResources().getString(R.string.select_start_time), context);
                        } else if (editTextEndTime.getText().toString().isEmpty()) {
                            CommonHelper.toast(context.getResources().getString(R.string.select_end_time), context);
                        } else {
                            HashMap<String, String> hashMapValues = new HashMap<>();
                            hashMapValues.put(WebConstants.SendToAuction.RequestID, requestID);
                            hashMapValues.put(WebConstants.SendToAuction.reservedPrice,
                                    editTextReservePrice.getText().toString());
                            hashMapValues.put(WebConstants.SendToAuction.auctionStartDateTime,
                                    editTextDateInvisible.getText().toString() + " " +
                                            editTextStartTimeHidden.getText().toString());
                            hashMapValues.put(WebConstants.SendToAuction.auctionEndDateTime,
                                    editTextDateInvisible.getText().toString() + " " +
                                            editTextEndTimeHidden.getText().toString());
                            hashMapValues.put(WebConstants.SendToAuction.resendAuctionFlag, "Y");

                            onClickDialog.onSuccess(hashMapValues);
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void pickerTime(final TextView editTextStartTime, final TextView textStartTimeHidden,
                    final TextView editTextEndTime, final TextView textEndTimeHidden) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setCancelable(false);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_year_picker, null);
        builder.setView(dialogView);

        final NumberPicker numberPicker = dialogView.findViewById(R.id.yearPicker);
        numberPicker.setMinValue(0);
        //numberPicker.setMaxValue(24);
        numberPicker.setMaxValue(46);
        numberPicker.setDisplayedValues(arr12Hours);
        numberPicker.setWrapSelectorWheel(false);

        Button btnOK = dialogView.findViewById(R.id.btn_year_dialog_ok);
        Button btnCancel = dialogView.findViewById(R.id.btn_year_dialog_cancel);


        // create alert dialog
        final android.app.AlertDialog alertDialog = builder.create();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String displayTimeFrom = arr12Hours[numberPicker.getValue()];
                    String hiddenTimeFrom = arr24Hours[numberPicker.getValue()];

                    editTextStartTime.setText(displayTimeFrom);
                    textStartTimeHidden.setText(hiddenTimeFrom);

                    editTextEndTime.setText(arr12Hours[numberPicker.getValue() + 8]);
                    textEndTimeHidden.setText(arr24Hours[numberPicker.getValue() + 8]);

                    alertDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // show it
        alertDialog.show();
    }

    public void showCommissionDialog(String bidPrice, String commissionPrice) {
        try {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_xmart_commission, null);

            final android.app.AlertDialog.Builder alertDialogBuilder =
                    new android.app.AlertDialog.Builder(activity);

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            TextView textViewBiddedPrice = promptsView
                    .findViewById(R.id.textViewBiddedPrice);

            TextView textViewCommissionAmount = promptsView
                    .findViewById(R.id.textViewCommissionAmount);

            textViewBiddedPrice.setText(bidPrice);
            textViewCommissionAmount.setText(commissionPrice);

            TextView textViewClose = promptsView
                    .findViewById(R.id.textViewClose);

            ImageView imageViewClose = promptsView
                    .findViewById(R.id.imageViewClose);

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            textViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });
            // show it
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callDialog(final String strMobileNo) {
        try {
            new AlertDialog.Builder(activity).setIcon(R.mipmap.ic_launcher).setTitle("Call")
                    .setMessage(activity.getResources().getString(R.string.make_call_confirmation))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CommonHelper.makeCall(strMobileNo, activity);
                        }
                    }).setNegativeButton("No", null).show();

        } catch (Exception e) {
            CommonHelper.toast("Contact number is invalid", activity);
            e.printStackTrace();
        }
    }

    public void showRebidDialog(String mitraPriceBasePrice, final Context applicationContext) {
        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_place_bid, null);

            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(activity);
            TextView textViewSubmit = promptsView
                    .findViewById(R.id.textViewSubmit);
            ImageView imageViewClose = promptsView
                    .findViewById(R.id.imageViewClose);
            final EditText editTextOldPrice = promptsView
                    .findViewById(R.id.editTextOldPrice);
            final EditText editTextNewPrice = promptsView
                    .findViewById(R.id.editTextNewPrice);

            editTextOldPrice.setText(mitraPriceBasePrice);
            editTextOldPrice.setEnabled(false);

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            textViewSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int oldPrice = 0;
                        if (!editTextOldPrice.getText().toString().isEmpty()) {
                            oldPrice = Integer.parseInt(editTextOldPrice.getText().toString());
                        }
                        int newPrice = Integer.parseInt(editTextNewPrice.getText().toString());

                        if (oldPrice >= newPrice) {
                            CommonHelper.toast(applicationContext.getResources().getString(R.string.new_price_validation), applicationContext);
                        } else {
                            bidConfirmationDialog(editTextNewPrice.getText().toString(), alertDialog);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCancelAppointmentDialog(final Context baseContext) {
        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_cancel_appointment, null);

            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(activity);

            TextView textViewSubmit = promptsView
                    .findViewById(R.id.textViewSubmit);

            ImageView imageViewClose = promptsView
                    .findViewById(R.id.imageViewClose);

            final Spinner spinnerCancelReason = promptsView
                    .findViewById(R.id.cancellation_reason);

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            textViewSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (spinnerCancelReason.getSelectedItemPosition() == 0) {
                        CommonHelper.toast("Please select reason", baseContext);
                    } else {

                        if (onClickDialog != null) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("value", String.valueOf(spinnerCancelReason.getSelectedItemPosition()));

                            onClickDialog.onSuccess(hashMap);
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bidConfirmationDialog(final String sNewPrice, final AlertDialog alertDialogParent) {
        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_bid_confirmation, null);

            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(activity);

            TextView textViewYes = promptsView
                    .findViewById(R.id.textViewYes);

            TextView textViewNo = promptsView
                    .findViewById(R.id.textViewNo);

            ImageView imageViewClose = promptsView
                    .findViewById(R.id.imageViewClose);

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            textViewYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    alertDialogParent.dismiss();

                    HashMap<String, String> hashMapValues = new HashMap<>();
                    hashMapValues.put("value", sNewPrice);

                    if (onClickDialog != null) {
                        onClickDialog.onSuccess(hashMapValues);
                    }
                }
            });

            textViewNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onSuccess();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}