package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mahindra.mitra20.champion.interfaces.OnCallListener;
import com.mahindra.mitra20.champion.models.EvaluatorWiseEnquiry;
import com.mahindra.mitra20.databinding.ListItemEvaluatorwiseEnquiryBinding;
import com.mahindra.mitra20.helper.CommonHelper;

import java.util.List;

public class EvaluatorwiseEnquiriesRVAdapter extends RecyclerView.Adapter<EvaluatorwiseEnquiriesRVAdapter.ViewHolder>
        implements OnCallListener {

    Context context;
    private List<EvaluatorWiseEnquiry> evaluatorWiseEnquiries;

    public EvaluatorwiseEnquiriesRVAdapter(Context _context, List<EvaluatorWiseEnquiry> evaluatorWiseEnquiries) {
        context = _context;
        this.evaluatorWiseEnquiries = evaluatorWiseEnquiries;
    }

    @Override
    public void onCallPressed(String number) {
        CommonHelper.makeCall(number, context);
    }

    @Override
    public EvaluatorwiseEnquiriesRVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ListItemEvaluatorwiseEnquiryBinding binding = ListItemEvaluatorwiseEnquiryBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.binding.setEnquiry(evaluatorWiseEnquiries.get(position));
    }

    @Override
    public int getItemCount() {
        return evaluatorWiseEnquiries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ListItemEvaluatorwiseEnquiryBinding binding;

        public ViewHolder(ListItemEvaluatorwiseEnquiryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setListener(EvaluatorwiseEnquiriesRVAdapter.this);
        }
    }
}