package com.mahindra.mitra20.champion.views.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.EvaluatorWiseEnquiry;
import com.mahindra.mitra20.champion.views.adapters.BrokersListRVAdapter;
import com.mahindra.mitra20.champion.views.adapters.EvaluatorwiseEnquiriesRVAdapter;
import com.mahindra.mitra20.databinding.ActivityEvaluatorwiseEnquiryListBinding;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.helper.MyLeadsItemDecoration;

import java.util.ArrayList;

public class EvaluatorwiseEnquiryListActivity extends AppCompatActivity {

    ActivityEvaluatorwiseEnquiryListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_evaluatorwise_enquiry_list);
        ArrayList<EvaluatorWiseEnquiry> enquiries;
        enquiries = getIntent().getParcelableArrayListExtra("list");
        String toolbarTitle = getIntent().getStringExtra("toolbar_title");

        MyLeadsItemDecoration dividerItemDecoration = new MyLeadsItemDecoration(
                ScreenDensityHelper.dpToPx(this, 15),
                ScreenDensityHelper.dpToPx(this, 10),
                ScreenDensityHelper.dpToPx(this, 10));
        binding.recyclerViewEnquiries.addItemDecoration(dividerItemDecoration);
        EvaluatorwiseEnquiriesRVAdapter adapter = new EvaluatorwiseEnquiriesRVAdapter(this, enquiries);
        binding.recyclerViewEnquiries.setAdapter(adapter);
        initUI(toolbarTitle);
    }

    private void initUI(String toolbarTitle) {
        CommonHelper.settingCustomToolBar(this, toolbarTitle);
        binding.recyclerViewEnquiries.setEmptyView(findViewById(R.id.textViewEmpty));
    }
}