package com.mahindra.mitra20.champion.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TableRow;

import com.mahindra.mitra20.R;

public class AppointmentConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    private TableRow tableRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_confirmation);

        tableRow = findViewById(R.id.tableDone);
        tableRow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.tableDone:

                finish();
                Intent intent = new Intent(this, ChampionDashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }
}
