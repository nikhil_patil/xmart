package com.mahindra.mitra20.champion.views.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.EvaluatorDetailsModel;
import com.mahindra.mitra20.champion.presenter.EvaluatorDetailsIn;
import com.mahindra.mitra20.champion.presenter.EvaluatorDetailsPresenter;
import com.mahindra.mitra20.champion.views.adapters.EvaluatorDetailsRVAdapter;
import com.mahindra.mitra20.databinding.ActivityEvaluatorDetailsBinding;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.helper.MyLeadsItemDecoration;

import java.util.ArrayList;

public class EvaluatorwiseStatusActivity extends AppCompatActivity implements EvaluatorDetailsIn {

    EvaluatorDetailsPresenter evaluatorDetailsPresenter = new EvaluatorDetailsPresenter(
            this, this);
    ActivityEvaluatorDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_evaluator_details);
        CommonHelper.settingCustomToolBar(this,"All Statuses");
        MyLeadsItemDecoration dividerItemDecoration = new MyLeadsItemDecoration(
                ScreenDensityHelper.dpToPx(this, 15),
                ScreenDensityHelper.dpToPx(this, 10),
                ScreenDensityHelper.dpToPx(this, 10));
        binding.recyclerView.addItemDecoration(dividerItemDecoration);
        evaluatorDetailsPresenter.downloadEvaluatorDetails();
    }

    @Override
    public void updateUI(ArrayList<EvaluatorDetailsModel> evaluatorDetailsModel) {
        binding.setEvaluatorAdapter(new EvaluatorDetailsRVAdapter(this, evaluatorDetailsModel));
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void failed() {
        binding.progressBar.setVisibility(View.GONE);
        CommonHelper.toast("Something went wrong", this);
    }
}