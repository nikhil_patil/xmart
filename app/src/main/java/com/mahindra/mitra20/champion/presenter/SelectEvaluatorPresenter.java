package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 8/13/2018.
 */

public class SelectEvaluatorPresenter implements VolleySingleTon.VolleyInteractor {

    private Context context;
    private SelectEvaluatorIn selectEvaluatorIn;
    private ChampionHelper championHelper;

    public SelectEvaluatorPresenter(SelectEvaluatorIn selectEvaluatorIn, Context context) {
        this.context = context;
        this.selectEvaluatorIn = selectEvaluatorIn;
        championHelper = new ChampionHelper(context);
    }

    public void downloadEvaluatorMaster() {

        try {

            HashMap<String, String> params = new HashMap<>();
            params.put(WebConstants.EnquiryRequestList.USER_ID, SessionUserDetails.getInstance().getUserID());

            VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.EvaluatorMaster.URL, params, WebConstants.EvaluatorMaster.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            selectEvaluatorIn.onDownloadFail(WebConstants.EnquiryRequestList.REQUEST_CODE, context.getResources().getString(R.string.download_failed));
        }
    }

    public void assignRequestToEvaluator(ArrayList<EnquiryRequest> enquiryRequestArrayList, EvaluatorMaster evaluatorMaster) {
        try {
            JSONObject params = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < enquiryRequestArrayList.size(); i++) {
                JSONObject jsonObjectChild = new JSONObject();
                EnquiryRequest enquiryRequest = enquiryRequestArrayList.get(i);
                jsonObjectChild.put("UserID", SessionUserDetails.getInstance().getUserID());
                jsonObjectChild.put("RequestID", enquiryRequest.getRequestID());
                jsonObjectChild.put("ScheduledStartDateTime", enquiryRequest.getDateOfAppointment());
                jsonObjectChild.put("EvaluatorID", evaluatorMaster.getEvaluatorID());

                jsonArray.put(jsonObjectChild);
            }

            params.put("AssignRequestDetails", jsonArray);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.AssignRequestToEvaluator.URL, params,
                    WebConstants.AssignRequestToEvaluator.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void assignRequestToEvaluatorMFCW(ArrayList<EnquiryRequest> enquiryRequestArrayList,
                                             EvaluatorMaster evaluatorMaster) {
        try {
            JSONObject params = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < enquiryRequestArrayList.size(); i++) {

                JSONObject jsonObjectChild = new JSONObject();
                EnquiryRequest enquiryRequest = enquiryRequestArrayList.get(i);
                jsonObjectChild.put("UserID", SessionUserDetails.getInstance().getUserID().toUpperCase());
                jsonObjectChild.put("UserType", ChampionConstants.MFCW_ID);
                jsonObjectChild.put("RequestID", enquiryRequest.getRequestID());
                jsonObjectChild.put("ScheduledStartDateTime", enquiryRequest.getDateOfAppointment());
                jsonObjectChild.put("EvaluatorID", ChampionConstants.MFCW_ID);

                jsonArray.put(jsonObjectChild);
            }

            params.put("AssignRequestDetails", jsonArray);

            VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.AssignRequestToEvaluator.URL, params, WebConstants.AssignRequestToEvaluator.REQUEST_CODE);

            //selectEvaluatorIn.onUploadSuccess(WebConstants.AssignRequestToEvaluator.REQUEST_CODE, "SUCCESS");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            switch (requestId) {
                case WebConstants.AssignRequestToEvaluator.REQUEST_CODE:
                    selectEvaluatorIn.onUploadSuccess(WebConstants.AssignRequestToEvaluator.REQUEST_CODE, "SUCCESS");
                    break;
                case WebConstants.EvaluatorMaster.REQUEST_CODE:
                    ArrayList<EvaluatorMaster> evaluatorMasterArrayList = new ArrayList<>();
                    JSONObject jsonObjectParent = new JSONObject(response);
                    JSONArray jsonArrayEnquiryRequestList = jsonObjectParent.getJSONArray("EvaluatorDetails");
                    for (int i = 0; i < jsonArrayEnquiryRequestList.length(); i++) {
                        EvaluatorMaster evaluatorMaster = new EvaluatorMaster();
                        JSONObject jsonObjectEnquiryRequest = (JSONObject) jsonArrayEnquiryRequestList.get(i);
                        evaluatorMaster.setAllocatedReqCount(jsonObjectEnquiryRequest.optString("ALLOCATED_REQ_COUNT"));
                        evaluatorMaster.setEvaluatorContactNo(jsonObjectEnquiryRequest.optString("Evaluator_Contact_Number"));
                        evaluatorMaster.setEvaluatorID(jsonObjectEnquiryRequest.optString("Evaluator_ID"));
                        evaluatorMaster.setEvaluatorName(jsonObjectEnquiryRequest.optString("Evaluator_Name"));

                        evaluatorMasterArrayList.add(evaluatorMaster);
                    }

                    championHelper.deleteEvaluatorMaster();
                    championHelper.putEvaluatorMaster(evaluatorMasterArrayList);
                    selectEvaluatorIn.onDownloadSuccess(1, evaluatorMasterArrayList);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            selectEvaluatorIn.onUploadFail(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}