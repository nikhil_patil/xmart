package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.BrokerMaster;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;

import org.json.JSONObject;

/**
 * Created by PATINIK on 9/30/2019.
 */

public class BrokersListPresenter implements VolleySingleTon.VolleyInteractor {

    private BrokersListIn brokersListIn;
    private Context context;

    public BrokersListPresenter(Context context) {
        this.brokersListIn = (BrokersListIn) context;
        this.context = context;
    }

    public void downloadBrokersMaster() {
        try {
            JSONObject params = new JSONObject();
            params.put(WebConstants.BrokersMaster.UserID, SessionUserDetails.getInstance().getEmployeeCode());
            params.put(WebConstants.BrokersMaster.RequestCount, "");

            VolleySingleTon.getInstance().connectToPostUrl3(context, this,
                    WebConstants.BrokersMaster.URL, params, WebConstants.BrokersMaster.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (requestId == WebConstants.BrokersMaster.REQUEST_CODE) {
                try {
                    BrokerMaster brokerMaster = new Gson().fromJson(response, BrokerMaster.class);
                    brokersListIn.onDownloadSuccess(WebConstants.BrokersMaster.REQUEST_CODE, brokerMaster);
                } catch (Exception e) {
                    e.printStackTrace();
                    brokersListIn.onDownloadFail(requestId, response);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            brokersListIn.onDownloadFail(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}