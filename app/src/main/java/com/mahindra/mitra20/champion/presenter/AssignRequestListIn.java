package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.EnquiryRequest;

import java.util.ArrayList;

/**
 * Created by user on 8/11/2018.
 */

public interface AssignRequestListIn {

    void onDownloadSuccess(int requestId, ArrayList<EnquiryRequest> enquiryRequestArrayList);

    void onDownloadFail(int requestId, String message);

}
