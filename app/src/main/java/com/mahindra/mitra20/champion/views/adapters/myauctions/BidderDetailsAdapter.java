package com.mahindra.mitra20.champion.views.adapters.myauctions;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.HighestBidder;

import java.util.ArrayList;


/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class BidderDetailsAdapter extends RecyclerView.Adapter<BidderDetailsAdapter.ViewHolder> {

    private ArrayList<HighestBidder> HighestBidderContents = new ArrayList<>();
    Context context;
    ItemClickListener monClick;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        // each data item is just a string in this case
        public TextView textViewBidderName,textViewBiddingCost, textViewCall;
        private ImageView imageViewInfo;
        private LinearLayout tableRowData;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewBidderName = v.findViewById(R.id.textViewBidderName);
            textViewBiddingCost = v.findViewById(R.id.textViewBiddingCost);
            textViewCall = v.findViewById(R.id.textViewCall);
            imageViewInfo = v.findViewById(R.id.imageViewInfo);
            tableRowData = v.findViewById(R.id.tableRowData);

            imageViewInfo.setOnClickListener(this);
            tableRowData.setOnLongClickListener(this);
            textViewCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (monClick != null) monClick.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            if (monClick != null) monClick.onLongClick(view, getAdapterPosition());
            return false;
        }
    }

    public void setmonClick(ItemClickListener monClick) {
        this.monClick = monClick;
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public BidderDetailsAdapter(Context _context, ArrayList<HighestBidder> _HighestBidderContents) {

        if (_HighestBidderContents != null)
            HighestBidderContents.addAll(_HighestBidderContents);
        context = _context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BidderDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_highest_bidder, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewBidderName.setText(HighestBidderContents.get(position).getStrBidderName());
        holder.textViewBiddingCost.setText(HighestBidderContents.get(position).getStrBiddingCost());

        if (!HighestBidderContents.get(position).isCallEnable()) {
            holder.textViewCall.setVisibility(View.GONE);
            holder.imageViewInfo.setVisibility(View.GONE);
        } else {
            holder.textViewCall.setVisibility(View.VISIBLE);
            holder.imageViewInfo.setVisibility(View.VISIBLE);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return HighestBidderContents.size();
    }
}