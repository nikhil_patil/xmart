package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.comparator.AuctionDateSortComparator;
import com.mahindra.mitra20.champion.models.comparator.BidDateTimeComparator;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.ChampionHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.XMART;

/**
 * Created by user on 8/18/2018.
 */

public class MyLiveAuctionPresenter implements VolleySingleTon.VolleyInteractor{

    private Context context;
    private MyLiveAuctionIn myLiveAuctionIn;
    private ChampionHelper championHelper;

    public MyLiveAuctionPresenter(Context context, MyLiveAuctionIn myLiveAuctionIn) {
        this.myLiveAuctionIn = myLiveAuctionIn;
        this.context = context;
        championHelper = new ChampionHelper(context);
    }

    public void downloadLiveAuctionMaster(String status) {
        try {
            int requestId = WebConstants.LiveAuctionMaster.REQUEST_CODE_COMPLETED;
            if (status.equalsIgnoreCase(ChampionConstants.LIVE_STATUS)) {
                requestId = WebConstants.LiveAuctionMaster.REQUEST_CODE_LIVE;
            } else if (status.equalsIgnoreCase(ChampionConstants.BIDDING_STATUS)) {
                requestId = WebConstants.LiveAuctionMaster.REQUEST_CODE_UPCOMING;
            }
            JSONObject params = new JSONObject();

            if (status.equalsIgnoreCase(ChampionConstants.COMPLETED_STATUS)) {
                params.put(WebConstants.LiveAuctionMaster.FromDate, new DateHelper().getPreviousDateByDays(-30));
            } else {
                params.put(WebConstants.LiveAuctionMaster.FromDate, new DateHelper().getCurrentDate());
            }

            if (status.equalsIgnoreCase(ChampionConstants.BIDDING_STATUS)) {
                params.put(WebConstants.LiveAuctionMaster.toDate, new DateHelper().getNextDate(2));
            } else {
                params.put(WebConstants.LiveAuctionMaster.toDate, new DateHelper().getCurrentDate());
            }

            params.put(WebConstants.LiveAuctionMaster.AuctionStatus, status);
            params.put(WebConstants.LiveAuctionMaster.Make, "");
            params.put(WebConstants.LiveAuctionMaster.Model, "");
            params.put(WebConstants.LiveAuctionMaster.countToDisplay, "9999");
            params.put(WebConstants.LiveAuctionMaster.Sorting, "DATE");
            params.put(WebConstants.LiveAuctionMaster.UserID, SessionUserDetails.getInstance().getUserID().toUpperCase());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.LiveAuctionMaster.URL, params, requestId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case WebConstants.LiveAuctionMaster.REQUEST_CODE_LIVE:
            case WebConstants.LiveAuctionMaster.REQUEST_CODE_UPCOMING:
            case WebConstants.LiveAuctionMaster.REQUEST_CODE_COMPLETED:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("AuctionDetails");
                    ArrayList<AuctionDetail> auctionDetailArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        AuctionDetail auctionDetail = new AuctionDetail();
                        JSONObject jsonObjectAuctionDetails = (JSONObject) jsonArray.get(i);
                        auctionDetail.setAuctionDate(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"AuctionDate"));
                        auctionDetail.setAuctionID(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"AuctionID"));
                        auctionDetail.setEvaluationReportID(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"AuctionID"));
                        auctionDetail.setAuctionStatus(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"AuctionStatus"));
                        auctionDetail.setCountOfBidders(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"CountOfBidders"));
                        auctionDetail.setMITRAPriceBasePrice(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"MITRAprice(base price)"));
                        auctionDetail.setRequestID(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"RequestID"));
                        auctionDetail.setStartTimestamp(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"StartTimestamp"));
                        auctionDetail.setRemainingTimeInSeconds(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"remainingTimeInSeconds"));
                        auctionDetail.setVehOwnerName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"vehOwnerName"));
                        auctionDetail.setVehicleName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"vehicleName"));
                        auctionDetail.setHighestPrice(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"HighestPrice"));
                        auctionDetail.setTransactionNo(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"TransactionNo."));
                        auctionDetail.setVehiclePicURL(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"vehiclePicURL"));
                        auctionDetail.setContactNo(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"ContactNo"));
                        auctionDetail.setHighestBidder(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"HighestBidder"));
                        auctionDetail.setNegotiatorContactName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"NegotiatorContactName"));
                        auctionDetail.setNegotiatorContactNumber(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails,"NegotiatorContactNumber"));
                        auctionDetail.setNoOfOwners(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "NoOfOwners"));
                        auctionDetail.setOdometerReading(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "OdometerReading"));
                        auctionDetail.setFeedbackRating(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "OverallFeedbackRating"));
                        auctionDetail.setVehicleUsage(SpinnerHelper.getValueByCodeFromMap(
                                SpinnerConstants.mapVehicleUsage, CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "VehicleUsage")));

                        JSONArray jsonArrayLIST = jsonObjectAuctionDetails.getJSONArray("LIST");
                        List<AuctionLIST> listList = new ArrayList<>();

                        for (int j = 0; j < jsonArrayLIST.length(); j++) {
                            JSONObject jsonObjectLIST = jsonArrayLIST.getJSONObject(j);
                            AuctionLIST auctionLIST = new AuctionLIST();

                            auctionLIST.setMITRAId(CommonHelper.hasJSONKey(
                                    jsonObjectLIST,"MITRAId"));
                            auctionLIST.setMITRAName(CommonHelper.replaceCars24MFCWChampion(
                                    CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAName"),
                                    CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAId"),
                                    CommonHelper.hasJSONKey(
                                            jsonObjectAuctionDetails,"NegotiatorContactName")));
                            auctionLIST.setBidPrice(CommonHelper.hasJSONKey(
                                    jsonObjectLIST,"bidPrice"));
                            auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(
                                    jsonObjectLIST,"biddingDateTime"));//biddingDateTime

                            if (CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAId")
                                    .equalsIgnoreCase(ChampionConstants.MFCW_ID)) {
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(
                                        jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                            } else {
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(
                                        jsonObjectLIST,"PhoneNum"));
                            }

                            auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(
                                    jsonObjectLIST,"AmountWithoutCommision"));
                            auctionLIST.setCommissionPrice(CommonHelper.hasJSONKey(
                                    jsonObjectLIST,"CommisionAmnt"));
                            listList.add(auctionLIST);
                        }

                        if (!CommonHelper.isChampionExistInList(listList)) {
                            AuctionLIST auctionLIST = new AuctionLIST();
                            auctionLIST.setMITRAId(SessionUserDetails.getInstance()
                                    .getUserID().toUpperCase());
                            auctionLIST.setCommissionPrice("0");
                            auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(
                                    jsonObjectAuctionDetails,"MITRAprice(base price)"));
                            auctionLIST.setBidPrice(CommonHelper.hasJSONKey(
                                    jsonObjectAuctionDetails,"MITRAprice(base price)"));
                            auctionLIST.setMITRAName(XMART);
                            auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(
                                    jsonObjectAuctionDetails,"AuctionDate") + " "
                                    + CommonHelper.hasJSONKey(
                                            jsonObjectAuctionDetails,"StartTimestamp"));
                            auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(
                                    jsonObjectAuctionDetails,"ContactNo"));
                            listList.add(auctionLIST);
                        }

                        try {
                            Collections.sort(listList, new BidDateTimeComparator());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        auctionDetail.setLIST(listList);
                        auctionDetailArrayList.add(auctionDetail);
                    }
                    if (requestId == WebConstants.LiveAuctionMaster.REQUEST_CODE_LIVE) {
                        championHelper.deleteAuctionDetails(ChampionConstants.LIVE_STATUS);
                    } else if (requestId == WebConstants.LiveAuctionMaster.REQUEST_CODE_UPCOMING) {
                        championHelper.deleteAuctionDetails(ChampionConstants.BIDDING_STATUS);
                    } else {
                        championHelper.deleteAuctionDetails(ChampionConstants.COMPLETED_STATUS);
                    }
                    championHelper.putAuctionDetails(auctionDetailArrayList);
                    try {
                        Collections.sort(auctionDetailArrayList, new AuctionDateSortComparator());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myLiveAuctionIn.onDownloadSuccess(requestId,
                            auctionDetailArrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case WebConstants.LiveAuctionMasterByID.REQUEST_CODE:
                myLiveAuctionIn.onDownloadSuccess(WebConstants.LiveAuctionMaster.REQUEST_CODE_COMPLETED,
                        null);
                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            myLiveAuctionIn.onDownloadFail(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}