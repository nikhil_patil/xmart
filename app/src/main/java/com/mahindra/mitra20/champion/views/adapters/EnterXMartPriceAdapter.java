package com.mahindra.mitra20.champion.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.EnterXmartPrice;

import java.util.ArrayList;

/**
 * Created by BADHAP-CONT on 7/26/2018.
 */

public class EnterXMartPriceAdapter  extends RecyclerView.Adapter<EnterXMartPriceAdapter.ViewHolder> {

    private ArrayList<EnterXmartPrice> enterXmartPriceContents;
    private ItemClickListener clickListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewName, textViewVehicleName, textViewHighestPrice;// tvEnquirySource, tvEnquiryType;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewName = v.findViewById(R.id.textViewName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewHighestPrice = v.findViewById(R.id.textViewHighestPrice);
//            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
//            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public void updateList(ArrayList<EnterXmartPrice> assignRequestContents){
        this.enterXmartPriceContents = assignRequestContents;
    }

    public void setmonClick(ItemClickListener monClick) {
        this.clickListener = monClick;
    }

    public EnterXMartPriceAdapter(Context _context, ArrayList<EnterXmartPrice> _enterXmartPriceContents) {
        enterXmartPriceContents = _enterXmartPriceContents;
    }

    @Override
    public EnterXMartPriceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_xmart_pending_price, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textViewName.setText(enterXmartPriceContents.get(position).getStrName());
        holder.textViewVehicleName.setText(enterXmartPriceContents.get(position).getStrVehicleName());
        holder.textViewHighestPrice.setText(enterXmartPriceContents.get(position).getStrHighestPrice());
//        String enqSource = enterXmartPriceContents.get(position).getEnquirySource();
//        if (null != enqSource)
//            holder.tvEnquirySource.setText(String.format("Source - %s", enqSource));
//        String enqType = enterXmartPriceContents.get(position).getEnquiryType();
//        if (null != enqType)
//            holder.tvEnquiryType.setText(String.format("Type - %s", enqType));
    }

    @Override
    public int getItemCount() {
        return enterXmartPriceContents.size();
    }
}