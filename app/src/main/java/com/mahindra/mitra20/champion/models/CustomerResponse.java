package com.mahindra.mitra20.champion.models;

/**
 * Created by BADHAP-CONT on 7/24/2018.
 */

public class CustomerResponse {

    int imgIcon;
    String strLabel;

    public int getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(int imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getStrLabel() {
        return strLabel;
    }

    public void setStrLabel(String strLabel) {
        this.strLabel = strLabel;
    }

    public CustomerResponse(int imgIcon, String strLabel) {

        this.imgIcon = imgIcon;
        this.strLabel = strLabel;
    }
}
