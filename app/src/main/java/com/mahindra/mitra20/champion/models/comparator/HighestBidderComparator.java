package com.mahindra.mitra20.champion.models.comparator;

import com.mahindra.mitra20.champion.models.HighestBidder;

import java.util.Comparator;

/**
 * Created by user on 9/15/2018.
 */

public class HighestBidderComparator implements Comparator<HighestBidder> {

    @Override
    public int compare(HighestBidder auctionDetail, HighestBidder t1) {

        return Integer.compare(Integer.parseInt(t1.getStrBiddingCost()), Integer.parseInt(auctionDetail.getStrBiddingCost()));
    }
}