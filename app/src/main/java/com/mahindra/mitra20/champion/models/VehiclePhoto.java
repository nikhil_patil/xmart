package com.mahindra.mitra20.champion.models;

/**
 * Created by PATINIK-CONT on 7/10/2019.
 */

public class VehiclePhoto {
    private String nameOfImage;
    private String path;
    private String docCode;
    private boolean displayImage;

    public VehiclePhoto(String nameOfImage, String path, boolean displayImage) {
        this.nameOfImage = nameOfImage;
        this.path = path;
        this.displayImage = displayImage;
    }

    public VehiclePhoto(String nameOfImage, String path, String docCode, boolean displayImage) {
        this.nameOfImage = nameOfImage;
        this.path = path;
        this.docCode = docCode;
        this.displayImage = displayImage;
    }

    public String getNameOfImage() {
        return nameOfImage;
    }

    public String getPath() {
        return path;
    }

    public String getDocCode() {
        return docCode;
    }

    public boolean isDisplayImage() {
        return displayImage;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public interface ImageConstants {
        int FRONT_IMAGE = 0;
        int REAR_IMAGE = 1;
        int FOURTY_DEGREE_LEFT_VIEW = 2;
        int FOURTY_DEGREE_RIGHT_VIEW = 3;
        int DASHBOARD_VIEW = 4;
        int ODOMETER_IMAGE = 5;
        int CHASSIS_NO_IMAGE = 6;
        int ROOF_TOP_IMAGE = 7;
        int INTERIOR_VIEW = 8;
        int RCCOPY_IMAGE = 9;
        int RCCOPY_1 = 10;
        int RCCOPY_2 = 11;
        int RCCOPY_3 = 12;
        int INSURANCE_IMAGE = 13;
        int REAR_LEFT_TYRE = 14;
        int REAR_RIGHT_TYRE = 15;
        int FRONT_LEFT_TYRE = 16;
        int FRONT_RIGHT_TYRE = 17;
        int ENGINE_NO_IMAGE = 18;
        int DAMAGED_PART_IMAGE_1 = 19;
        int DAMAGED_PART_IMAGE_2 = 20;
        int DAMAGED_PART_IMAGE_3 = 21;
        int DAMAGED_PART_IMAGE_4 = 22;
    }
}