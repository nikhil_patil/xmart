package com.mahindra.mitra20.champion.presenter;

/**
 * Created by user on 8/19/2018.
 */

public interface SendToAuctionIn {

    void uploadSuccess(int requestID, String data);

    void uploadFailed(int requestID, String data);

}
