package com.mahindra.mitra20.champion.views.adapters.myauctions;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.interfaces.ItemClickListener;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.ContactIn;
import com.mahindra.mitra20.module_broker.helper.AuctionHelper;
import com.mahindra.mitra20.module_broker.helper.StringHelper;

import java.util.ArrayList;

/**
 * Created by WANIRO-CONT on 4/9/2018.
 */

public class LiveAuctionsAdapter extends RecyclerView.Adapter<LiveAuctionsAdapter.ViewHolder> {

    private ArrayList<AuctionDetail> myAuctionsContents;
    private Context context;
    private ItemClickListener clickListener;
    private ContactIn contactIn;

    public void setContactIn(ContactIn contactIn) {
        this.contactIn = contactIn;
    }

    public void updateList(ArrayList<AuctionDetail> myAuctionsContents){
        this.myAuctionsContents = myAuctionsContents;
    }

    @Override
    public LiveAuctionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_my_live_auctions, parent, false);
        return new ViewHolder(v);
    }

    public void setmonClick(ItemClickListener monClick) {
        this.clickListener = monClick;
    }

    public LiveAuctionsAdapter(Context _context, ArrayList<AuctionDetail> _myAuctionsContents) {
        myAuctionsContents = _myAuctionsContents;
        context = _context;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textViewXmartPrice.setText(String.format("Rs. %s",
                myAuctionsContents.get(position).getMITRAPriceBasePrice()));
        holder.textViewCustomerName.setText(myAuctionsContents.get(position).getVehOwnerName());
        holder.textViewVehicleName.setText(myAuctionsContents.get(position).getVehicleName());
        holder.textViewXmartBid.setText(myAuctionsContents.get(position).getBidResult());
        holder.textViewVehicleUsage.setText(String.format("Vehicle Usage - %s",
                myAuctionsContents.get(position).getVehicleUsage()));

        if (myAuctionsContents.get(position).getBidResult().equals("You are highest bidder")) {
            holder.arrow.setImageDrawable(context.getDrawable(R.mipmap.ic_arrow_up));
        } else if (myAuctionsContents.get(position).getBidResult().equals("You are not highest bidder")) {
            holder.arrow.setImageDrawable(context.getDrawable(R.mipmap.ic_arrow_down));
        }

        if (null != myAuctionsContents.get(position).getVehiclePicURL()) {
            if (!myAuctionsContents.get(position).getVehiclePicURL().isEmpty()) {
                Glide.with(holder.imageViewVehicle.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(
                                myAuctionsContents.get(position).getVehiclePicURL()))
                        .apply(CommonHelper.getGlideErrorImage())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .into(holder.imageViewVehicle);
            } else {
                Glide.with(holder.imageViewVehicle.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.imageViewVehicle);
            }
        }

        long seconds = Integer.parseInt(myAuctionsContents.get(position).getRemainingTimeInSeconds());
        long milli = seconds * 1000;

        if (holder.countDownTimer != null) {
            holder.countDownTimer.cancel();
        }

        holder.countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                String time = StringHelper.getDoubleDigit(String.valueOf(hours % 24)) + ":" +
                        StringHelper.getDoubleDigit((((minutes % 60) > 9)
                                ? minutes % 60 : "0" + (minutes % 60)) + ":" +
                                StringHelper.getDoubleDigit(String.valueOf(seconds % 60)));
                holder.textViewTime.setText(time);

                int colorForTimer = AuctionHelper.getColorForTimer(millisUntilFinished);
                Drawable mDrawable = context.getResources().getDrawable(R.drawable.button_half_circular_green);
                mDrawable.setColorFilter(new
                        PorterDuffColorFilter(colorForTimer, PorterDuff.Mode.SRC_IN));
                holder.tableRowHeader.setBackground(mDrawable);

            }

            @Override
            public void onFinish() {
                holder.countDownTimer.cancel();
    }
        };
        holder.countDownTimer.start();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTime, textViewXmartPrice,textViewCustomerName,textViewVehicleName,
                textViewXmartBid, textViewVehicleUsage;
        ImageView imageViewCall, imageViewDetails, arrow, imageViewVehicle;
        TableRow tableRowHeader;
        public View layout;
        CountDownTimer countDownTimer;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewXmartPrice = v.findViewById(R.id.textViewXmartPrice);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewXmartBid = v.findViewById(R.id.textViewXmartBid);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewDetails = v.findViewById(R.id.imageViewDetails);
            arrow = v.findViewById(R.id.arrow);
            imageViewVehicle = v.findViewById(R.id.imageViewVehicle);
            tableRowHeader = v.findViewById(R.id.tableRowHeader);
            textViewVehicleUsage = v.findViewById(R.id.textViewVehicleUsage);

            imageViewCall.setOnClickListener(this);
            imageViewDetails.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imageViewDetails:
                    if (clickListener != null) {
                        clickListener.onClick(view, getAdapterPosition());
                    }
                    break;
                case R.id.imageViewCall:
                    contactIn.onCallPress(myAuctionsContents.get(getAdapterPosition()).getContactNo());
                    break;
                case R.id.imageViewMessage:
                    contactIn.onMessagePress(myAuctionsContents.get(getAdapterPosition()).getContactNo());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return myAuctionsContents.size();
    }
}