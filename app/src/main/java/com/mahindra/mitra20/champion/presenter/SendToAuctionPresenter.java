package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user on 8/19/2018.
 */

public class SendToAuctionPresenter implements VolleySingleTon.VolleyInteractor {

    private SendToAuctionIn sendToAuctionIn;
    private Context context;

    public SendToAuctionPresenter(SendToAuctionIn sendToAuctionIn, Context context) {

        this.sendToAuctionIn = sendToAuctionIn;
        this.context = context;
    }

    public void sendEnquiryToAuction(HashMap<String, String> hashMapValues) {
        try {
            JSONObject params = new JSONObject(hashMapValues);
            params.put(WebConstants.SendToAuction.UserID, SessionUserDetails.getInstance().getUserID().toUpperCase());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.SendToAuction.URL, params, WebConstants.SendToAuction.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        sendToAuctionIn.uploadSuccess(WebConstants.SendToAuction.REQUEST_CODE, "SUCCESS");
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            sendToAuctionIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getNextEvaluationId() {
        return new EvaluatorHelper(context).getNextEvaluationId();
    }
}