package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.presenters.BrokerEvaluationDetailsPresenter;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mahindra.mitra20.champion.constants.ChampionConstants.XMART;

/**
 * Created by user on 8/20/2018.
 */

public class SetOfferPricePresenter implements VolleySingleTon.VolleyInteractor {

    private SetOfferPriceIn setOfferpriceIn;
    private Context context;

    public SetOfferPricePresenter(SetOfferPriceIn setOfferpriceIn, Context context) {
        this.setOfferpriceIn = setOfferpriceIn;
        this.context = context;
    }

    public void setOfferPriceToCustomer(HashMap<String, String> hashMapParams) {
        try {
            JSONObject params = new JSONObject(hashMapParams);
            params.put(WebConstants.SetOfferprice.UserID, SessionUserDetails.getInstance().getUserID());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.SetOfferprice.URL, params, WebConstants.SetOfferprice.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAuctionMasterByID(String auctionID) {
        try {
            JSONObject params = new JSONObject();
            params.put(WebConstants.LiveAuctionMasterByID.UserID,
                    SessionUserDetails.getInstance().getUserID().toUpperCase());
            params.put(WebConstants.LiveAuctionMasterByID.AuctionID, auctionID);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.LiveAuctionMasterByID.URL, params,
                    WebConstants.LiveAuctionMasterByID.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downloadEvaluationReport(HashMap<String, String> param) {
        try {
            JSONObject params = new JSONObject(param);
            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.GetEvaluationReport.URL, params,
                    WebConstants.GetEvaluationReport.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case WebConstants.SetOfferprice.REQUEST_CODE:
                setOfferpriceIn.uploadSuccess(requestId,response);
                break;
            case WebConstants.LiveAuctionMasterByID.REQUEST_CODE:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("AuctionDetails");
                    ArrayList<AuctionDetail> auctionDetailArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        AuctionDetail auctionDetail = new AuctionDetail();

                        JSONObject jsonObjectAuctionDetails = (JSONObject) jsonArray.get(i);
                        auctionDetail.setAuctionDate(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "AuctionDate"));
                        auctionDetail.setAuctionID(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "AuctionID"));
                        auctionDetail.setAuctionStatus(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "AuctionStatus"));
                        auctionDetail.setCountOfBidders(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "CountOfBidders"));
                        auctionDetail.setMITRAPriceBasePrice(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "MITRAprice(base price)"));
                        auctionDetail.setRequestID(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "RequestID"));
                        auctionDetail.setStartTimestamp(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "StartTimestamp"));
                        auctionDetail.setRemainingTimeInSeconds(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "remainingTimeInSeconds"));
                        auctionDetail.setVehOwnerName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "vehOwnerName"));
                        auctionDetail.setVehicleName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "vehicleName"));
                        auctionDetail.setHighestPrice(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "HighestPrice"));
                        auctionDetail.setAuctionWinnerId(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "AuctionWinnerId"));
                        auctionDetail.setEvaluationStatus(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "EvaluationStatus"));
                        auctionDetail.setHighestBidder(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "HighestBidder"));
                        auctionDetail.setNegotiatorContactName(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "NegotiatorContactName"));
                        auctionDetail.setNegotiatorContactNumber(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                        auctionDetail.setNoOfOwners(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "NoOfOwners"));
                        auctionDetail.setOdometerReading(CommonHelper.hasJSONKey(
                                jsonObjectAuctionDetails, "OdometerReading"));

                        JSONArray jsonArrayLIST = jsonObjectAuctionDetails.getJSONArray("LIST");
                        List<AuctionLIST> listList = new ArrayList<>();

                        for (int j = 0; j < jsonArrayLIST.length(); j++) {

                            JSONObject jsonObjectLIST = jsonArrayLIST.getJSONObject(j);
                            AuctionLIST auctionLIST = new AuctionLIST();

                            auctionLIST.setMITRAId(CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAId"));
                            auctionLIST.setMITRAName(CommonHelper.replaceCars24MFCWChampion(CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAName"), CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAId"), CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactName")));
                            auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectLIST,"bidPrice"));
                            auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectLIST,"bidingDateTime"));
                            if (CommonHelper.hasJSONKey(jsonObjectLIST,"MITRAId").equalsIgnoreCase(ChampionConstants.MFCW_ID)) {
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails, "NegotiatorContactNumber"));
                            } else {
                                auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectLIST,"PhoneNum"));
                            }

                            auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectLIST,"AmountWithoutCommision"));
                            auctionLIST.setCommissionPrice(CommonHelper.hasJSONKey(jsonObjectLIST,"CommisionAmnt"));

                            listList.add(auctionLIST);
                        }

                        if (!CommonHelper.isChampionExistInList(listList)) {
                            AuctionLIST auctionLIST = new AuctionLIST();
                            auctionLIST.setMITRAId(SessionUserDetails.getInstance().getUserID().toUpperCase());
                            auctionLIST.setCommissionPrice("0");
                            auctionLIST.setDeductedPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails,"MITRAprice(base price)"));
                            auctionLIST.setBidPrice(CommonHelper.hasJSONKey(jsonObjectAuctionDetails,"MITRAprice(base price)"));
                            auctionLIST.setMITRAName(XMART);
                            auctionLIST.setBidingDateTime(CommonHelper.hasJSONKey(jsonObjectAuctionDetails,"AuctionDate") + " " + CommonHelper.hasJSONKey(jsonObjectAuctionDetails,"StartTimestamp"));
                            auctionLIST.setPhoneNum(CommonHelper.hasJSONKey(jsonObjectAuctionDetails,"ContactNo"));

                            listList.add(auctionLIST);
                        }
                        auctionDetail.setLIST(listList);
                        auctionDetailArrayList.add(auctionDetail);
                    }
                    setOfferpriceIn.uploadSuccess(requestId, auctionDetailArrayList.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case WebConstants.GetEvaluationReport.REQUEST_CODE:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    NewEvaluation newEvaluation =
                            BrokerEvaluationDetailsPresenter.getNewEvaluationInstance(jsonObject);

                    EvaluatorHelper evaluatorHelper = new EvaluatorHelper(context);
                    long l = evaluatorHelper.addUpdateNewEvaluationDetails(newEvaluation,
                            EvaluatorConstants.EVALUATION_DB_STATUS_ONLINE);

                    ArrayList<EnquiryRequest> enquiryRequestArrayList = new ArrayList<>();
                    EnquiryRequest enquiryRequest = new EnquiryRequest();
                    enquiryRequest.setEvaluationReportID(String.valueOf(l));
                    enquiryRequestArrayList.add(enquiryRequest);

                    setOfferpriceIn.downloadSuccess(WebConstants.GetEvaluationReport.REQUEST_CODE, newEvaluation);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            setOfferpriceIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}