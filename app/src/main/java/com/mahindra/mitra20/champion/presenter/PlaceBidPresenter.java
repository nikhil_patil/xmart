package com.mahindra.mitra20.champion.presenter;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.models.SessionUserDetails;

import org.json.JSONObject;

import java.util.HashMap;

public class PlaceBidPresenter implements VolleySingleTon.VolleyInteractor {

    private PlaceBidIn placeBidIn;
    private Context context;

    public PlaceBidPresenter(PlaceBidIn placeBidIn, Context context) {
        this.context = context;
        this.placeBidIn = placeBidIn;
    }

    public void placeBid(HashMap<String, String> hashMapValues) {
        try {
            JSONObject params = new JSONObject();
            params.put(WebConstants.PlaceBid.AuctionID, hashMapValues.get(WebConstants.PlaceBid.AuctionID));
            params.put(WebConstants.PlaceBid.UserID, SessionUserDetails.getInstance().getUserID());
            params.put(WebConstants.PlaceBid.BIDAmount, hashMapValues.get("value"));

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.PlaceBid.URL, params, WebConstants.PlaceBid.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendEnquiryToAuction(HashMap<String, String> hashMapValues) {
        try {
            JSONObject params = new JSONObject(hashMapValues);
            params.put(WebConstants.SendToAuction.UserID, SessionUserDetails.getInstance().getUserID().toUpperCase());

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.SendToAuction.URL, params, WebConstants.SendToAuction.REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (WebConstants.PlaceBid.REQUEST_CODE == requestId) {
                JSONObject jsonObject = new JSONObject(response);
                placeBidIn.uploadSuccess(WebConstants.PlaceBid.REQUEST_CODE, jsonObject.getString("message"));
            } else if(WebConstants.SendToAuction.REQUEST_CODE == requestId) {
                placeBidIn.uploadSuccess(WebConstants.SendToAuction.REQUEST_CODE, "SUCCESS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            placeBidIn.uploadFailed(requestId, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}