package com.mahindra.mitra20.champion.presenter;

import com.mahindra.mitra20.champion.models.EvaluatorMaster;

import java.util.ArrayList;

/**
 * Created by user on 8/13/2018.
 */

public interface SelectEvaluatorIn {

    void onDownloadSuccess(int requestId, ArrayList<EvaluatorMaster> enquiryRequestArrayList);

    void onDownloadFail(int requestId, String message);

    void onUploadSuccess(int requestId, String message);

    void onUploadFail(int requestId, String message);

}
