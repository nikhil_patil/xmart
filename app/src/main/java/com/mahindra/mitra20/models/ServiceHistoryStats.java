package com.mahindra.mitra20.models;

/**
 * Created by PATINIK-CONT on 26-Jun-19.
 */
public class ServiceHistoryStats {

    private int serviceTypeRRCount = 0;
    private float serviceTypeRRTotalAmount = 0;
    private int serviceTypeScheduleCount = 0;
    private float serviceTypeScheduleTotalAmount = 0;
    private int serviceTypePRSLCount = 0;
    private float serviceTypePRSLTotalAmount = 0;
    private int serviceTypeSVCCount = 0;
    private float serviceTypeSVCTotalAmount = 0;
    private int serviceTypeSACCount = 0;
    private float serviceTypeSACTotalAmount = 0;
    private int serviceTypeACCCount = 0;
    private float serviceTypeACCTotalAmount = 0;
    private int serviceTypeOthersCount = 0;
    private float serviceTypeOthersTotalAmount = 0;
    private int serviceTypeAllCount = 0;
    private float serviceTypeAllTotalAmount = 0;

    private float serviceTypeRsPerKm = 0;
    private int serviceTypeMTV = 0;
    private float averageRun = 0;
    private float lastReportedKms = 0;

    public int getServiceTypeRRCount() {
        return serviceTypeRRCount;
    }

    public void setServiceTypeRRCount(int serviceTypeRRCount) {
        this.serviceTypeRRCount = serviceTypeRRCount;
    }

    public float getServiceTypeRRTotalAmount() {
        return serviceTypeRRTotalAmount;
    }

    public void setServiceTypeRRTotalAmount(float serviceTypeRRTotalAmount) {
        this.serviceTypeRRTotalAmount = serviceTypeRRTotalAmount;
    }

    public int getServiceTypeScheduleCount() {
        return serviceTypeScheduleCount;
    }

    public void setServiceTypeScheduleCount(int serviceTypeScheduleCount) {
        this.serviceTypeScheduleCount = serviceTypeScheduleCount;
    }

    public float getServiceTypeScheduleTotalAmount() {
        return serviceTypeScheduleTotalAmount;
    }

    public void setServiceTypeScheduleTotalAmount(float serviceTypeScheduleTotalAmount) {
        this.serviceTypeScheduleTotalAmount = serviceTypeScheduleTotalAmount;
    }

    public int getServiceTypePRSLCount() {
        return serviceTypePRSLCount;
    }

    public void setServiceTypePRSLCount(int serviceTypePRSLCount) {
        this.serviceTypePRSLCount = serviceTypePRSLCount;
    }

    public float getServiceTypePRSLTotalAmount() {
        return serviceTypePRSLTotalAmount;
    }

    public void setServiceTypePRSLTotalAmount(float serviceTypePRSLTotalAmount) {
        this.serviceTypePRSLTotalAmount = serviceTypePRSLTotalAmount;
    }

    public int getServiceTypeSVCCount() {
        return serviceTypeSVCCount;
    }

    public void setServiceTypeSVCCount(int serviceTypeSVCCount) {
        this.serviceTypeSVCCount = serviceTypeSVCCount;
    }

    public float getServiceTypeSVCTotalAmount() {
        return serviceTypeSVCTotalAmount;
    }

    public void setServiceTypeSVCTotalAmount(float serviceTypeSVCTotalAmount) {
        this.serviceTypeSVCTotalAmount = serviceTypeSVCTotalAmount;
    }

    public int getServiceTypeSACCount() {
        return serviceTypeSACCount;
    }

    public void setServiceTypeSACCount(int serviceTypeSACCount) {
        this.serviceTypeSACCount = serviceTypeSACCount;
    }

    public float getServiceTypeSACTotalAmount() {
        return serviceTypeSACTotalAmount;
    }

    public void setServiceTypeSACTotalAmount(float serviceTypeSACTotalAmount) {
        this.serviceTypeSACTotalAmount = serviceTypeSACTotalAmount;
    }

    public int getServiceTypeACCCount() {
        return serviceTypeACCCount;
    }

    public void setServiceTypeACCCount(int serviceTypeACCCount) {
        this.serviceTypeACCCount = serviceTypeACCCount;
    }

    public float getServiceTypeACCTotalAmount() {
        return serviceTypeACCTotalAmount;
    }

    public void setServiceTypeACCTotalAmount(float serviceTypeACCTotalAmount) {
        this.serviceTypeACCTotalAmount = serviceTypeACCTotalAmount;
    }

    public int getServiceTypeOthersCount() {
        return serviceTypeOthersCount;
    }

    public void setServiceTypeOthersCount(int serviceTypeOthersCount) {
        this.serviceTypeOthersCount = serviceTypeOthersCount;
    }

    public float getServiceTypeOthersTotalAmount() {
        return serviceTypeOthersTotalAmount;
    }

    public void setServiceTypeOthersTotalAmount(float serviceTypeOthersTotalAmount) {
        this.serviceTypeOthersTotalAmount = serviceTypeOthersTotalAmount;
    }

    public int getServiceTypeAllCount() {
        return serviceTypeAllCount;
    }

    public void setServiceTypeAllCount(int serviceTypeAllCount) {
        this.serviceTypeAllCount = serviceTypeAllCount;
    }

    public float getServiceTypeAllTotalAmount() {
        return serviceTypeAllTotalAmount;
    }

    public void setServiceTypeAllTotalAmount(float serviceTypeAllTotalAmount) {
        this.serviceTypeAllTotalAmount = serviceTypeAllTotalAmount;
    }

    public float getServiceTypeRsPerKm() {
        return serviceTypeRsPerKm;
    }

    public void setServiceTypeRsPerKm(float serviceTypeRsPerKm) {
        this.serviceTypeRsPerKm = serviceTypeRsPerKm;
    }

    public int getServiceTypeMTV() {
        return serviceTypeMTV;
    }

    public void setServiceTypeMTV(int serviceTypeMTV) {
        this.serviceTypeMTV = serviceTypeMTV;
    }

    public float getAverageRun() {
        return averageRun;
    }

    public void setAverageRun(float averageRun) {
        this.averageRun = averageRun;
    }

    public float getLastReportedKms() {
        return lastReportedKms;
    }

    public void setLastReportedKms(float lastReportedKms) {
        this.lastReportedKms = lastReportedKms;
    }
}