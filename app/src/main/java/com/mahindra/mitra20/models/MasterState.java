package com.mahindra.mitra20.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class MasterState {
    @SerializedName("STAT_CD")
    String stat_cd;
    @SerializedName("STAT_DESC")
    String stat_desc;
    @SerializedName("ACTV_IND")
    String actv_ind;

    public MasterState(JSONObject jObj) {
        try {
            stat_cd = jObj.getString("State Code");
            stat_desc = jObj.getString("State Name");
            actv_ind = jObj.getString("Active Status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getStat_cd() {
        return stat_cd;
    }

    public void setStat_cd(String stat_cd) {
        this.stat_cd = stat_cd;
    }

    public String getStat_desc() {
        return stat_desc;
    }

    public void setStat_desc(String stat_desc) {
        this.stat_desc = stat_desc;
    }

    public String getActv_ind() {
        return actv_ind;
    }

    public void setActv_ind(String actv_ind) {
        this.actv_ind = actv_ind;
    }
}
