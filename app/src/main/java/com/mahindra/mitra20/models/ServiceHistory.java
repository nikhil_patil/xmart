package com.mahindra.mitra20.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceHistory {
    private String SoldbyDealerName_2;
    private String modelFamily;
    private String soldDate;
    private String SoldbyDealerName_1;
    private String soldBy;
    private String messagecode;
    private String customerCode;
    private List<RoDetail> RoDetail;
    private String ShieldDescription_1;
    private String ShieldDescription_2;
    private String SellerDealerName;
    @SerializedName("VN.MFG_SCHEM2_KM")
    private String vnMFG_SCHEM2_KM;
    private String model;
    private String engineNo;
    private String address;
    private String ShieldEndDate_2;
    private String ShieldEndDate_1;
    private String RegistrationDate_1;
    private String RegistrationDate_2;
    private String customerName;
    private String registrationNo;
    private String chassisNo;
    @SerializedName("VN.MFG_SCHEM2")
    private String vnMFG_SCHEM2;

    @SerializedName("VN.MFG_SCHEM1")
    private String vnMFG_SCHEM1;

    @SerializedName("VN.MFG_SCHEM1_KM")
    private String vnMFG_SCHEM1_KM;

    private String IsSuccessful;

    public String getSoldbyDealerName_2() {
        return SoldbyDealerName_2;
    }

    public void setSoldbyDealerName_2(String soldbyDealerName_2) {
        SoldbyDealerName_2 = soldbyDealerName_2;
    }

    public String getModelFamily() {
        return modelFamily;
    }

    public void setModelFamily(String modelFamily) {
        this.modelFamily = modelFamily;
    }

    public String getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(String soldDate) {
        this.soldDate = soldDate;
    }

    public String getSoldbyDealerName_1() {
        return SoldbyDealerName_1;
    }

    public void setSoldbyDealerName_1(String soldbyDealerName_1) {
        SoldbyDealerName_1 = soldbyDealerName_1;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(String soldBy) {
        this.soldBy = soldBy;
    }

    public String getMessagecode() {
        return messagecode;
    }

    public void setMessagecode(String messagecode) {
        this.messagecode = messagecode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public List<RoDetail> getRoDetail() {
        return RoDetail;
    }

    public void setRoDetail(List<RoDetail> roDetail) {
        RoDetail = roDetail;
    }

    public String getShieldDescription_1() {
        return ShieldDescription_1;
    }

    public void setShieldDescription_1(String shieldDescription_1) {
        ShieldDescription_1 = shieldDescription_1;
    }

    public String getShieldDescription_2() {
        return ShieldDescription_2;
    }

    public void setShieldDescription_2(String shieldDescription_2) {
        ShieldDescription_2 = shieldDescription_2;
    }

    public String getSellerDealerName() {
        return SellerDealerName;
    }

    public void setSellerDealerName(String sellerDealerName) {
        SellerDealerName = sellerDealerName;
    }

    public String getVnMFG_SCHEM2_KM() {
        return vnMFG_SCHEM2_KM;
    }

    public void setVnMFG_SCHEM2_KM(String vnMFG_SCHEM2_KM) {
        this.vnMFG_SCHEM2_KM = vnMFG_SCHEM2_KM;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShieldEndDate_2() {
        return ShieldEndDate_2;
    }

    public void setShieldEndDate_2(String shieldEndDate_2) {
        ShieldEndDate_2 = shieldEndDate_2;
    }

    public String getShieldEndDate_1() {
        return ShieldEndDate_1;
    }

    public void setShieldEndDate_1(String shieldEndDate_1) {
        ShieldEndDate_1 = shieldEndDate_1;
    }

    public String getRegistrationDate_1() {
        return RegistrationDate_1;
    }

    public void setRegistrationDate_1(String registrationDate_1) {
        RegistrationDate_1 = registrationDate_1;
    }

    public String getRegistrationDate_2() {
        return RegistrationDate_2;
    }

    public void setRegistrationDate_2(String registrationDate_2) {
        RegistrationDate_2 = registrationDate_2;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getVnMFG_SCHEM2() {
        return vnMFG_SCHEM2;
    }

    public void setVnMFG_SCHEM2(String vnMFG_SCHEM2) {
        this.vnMFG_SCHEM2 = vnMFG_SCHEM2;
    }

    public String getVnMFG_SCHEM1() {
        return vnMFG_SCHEM1;
    }

    public void setVnMFG_SCHEM1(String vnMFG_SCHEM1) {
        this.vnMFG_SCHEM1 = vnMFG_SCHEM1;
    }

    public String getVnMFG_SCHEM1_KM() {
        return vnMFG_SCHEM1_KM;
    }

    public void setVnMFG_SCHEM1_KM(String vnMFG_SCHEM1_KM) {
        this.vnMFG_SCHEM1_KM = vnMFG_SCHEM1_KM;
    }

    public String getIsSuccessful() {
        return IsSuccessful;
    }

    public void setIsSuccessful(String isSuccessful) {
        IsSuccessful = isSuccessful;
    }
}