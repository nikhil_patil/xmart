package com.mahindra.mitra20.models;

public class LabourDetails
{
    private String amcFlag;

    private String laborDescription;

    private String labourGroupCode;

    private String labourAmount;

    private String laborCode;

    private String billableType;

    private String srl;

    public String getAmcFlag ()
    {
        return amcFlag;
    }

    public void setAmcFlag (String amcFlag)
    {
        this.amcFlag = amcFlag;
    }

    public String getLaborDescription ()
    {
        return laborDescription;
    }

    public void setLaborDescription (String laborDescription)
    {
        this.laborDescription = laborDescription;
    }

    public String getLabourGroupCode ()
    {
        return labourGroupCode;
    }

    public void setLabourGroupCode (String labourGroupCode)
    {
        this.labourGroupCode = labourGroupCode;
    }

    public String getLabourAmount ()
    {
        return labourAmount;
    }

    public void setLabourAmount (String labourAmount)
    {
        this.labourAmount = labourAmount;
    }

    public String getLaborCode ()
    {
        return laborCode;
    }

    public void setLaborCode (String laborCode)
    {
        this.laborCode = laborCode;
    }

    public String getBillableType ()
    {
        return billableType;
    }

    public void setBillableType (String billableType)
    {
        this.billableType = billableType;
    }

    public String getSrl ()
    {
        return srl;
    }

    public void setSrl (String srl)
    {
        this.srl = srl;
    }

}