package com.mahindra.mitra20.models;

public class RoDetail {
    private String serviceType;
    private String laborAmount;
    private String km;
    private String dealerName;
    private String dealerCode;
    private String branchName;
    private String billDate;
    private String partAmount;
    private String roDate;
    private PartDetails[] PartDetails;
    private String roNumber;
    private String totalAmount;
    private DRdetails[] DRdetails;
    private String mechanicName;
    private LabourDetails[] LabourDetails;
    private String recallStatus;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getLaborAmount() {
        return laborAmount;
    }

    public void setLaborAmount(String laborAmount) {
        this.laborAmount = laborAmount;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getPartAmount() {
        return partAmount;
    }

    public void setPartAmount(String partAmount) {
        this.partAmount = partAmount;
    }

    public String getRoDate() {
        return roDate;
    }

    public void setRoDate(String roDate) {
        this.roDate = roDate;
    }

    public PartDetails[] getPartDetails() {
        return PartDetails;
    }

    public void setPartDetails(PartDetails[] PartDetails) {
        this.PartDetails = PartDetails;
    }

    public String getRoNumber() {
        return roNumber;
    }

    public void setRoNumber(String roNumber) {
        this.roNumber = roNumber;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public DRdetails[] getDRdetails() {
        return DRdetails;
    }

    public void setDRdetails(DRdetails[] DRdetails) {
        this.DRdetails = DRdetails;
    }

    public String getMechanicName() {
        return mechanicName;
    }

    public void setMechanicName(String mechanicName) {
        this.mechanicName = mechanicName;
    }

    public LabourDetails[] getLabourDetails() {
        return LabourDetails;
    }

    public void setLabourDetails(LabourDetails[] LabourDetails) {
        this.LabourDetails = LabourDetails;
    }

    public String getRecallStatus() {
        return recallStatus;
    }

    public void setRecallStatus(String recallStatus) {
        this.recallStatus = recallStatus;
    }
}