package com.mahindra.mitra20.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class MasterPincode {

    @SerializedName("PIN_CD")
    String pin_cd;
    @SerializedName("PIN_DESC")
    String pin_desc;
    @SerializedName("CITY_CD")
    String city_cd;
    @SerializedName("STAT_CD")
    String stat_cd;
    @SerializedName("DIST_CD")
    String dist_cd;
    @SerializedName("ACTV_IND")
    String actv_ind;
    @SerializedName("TEHSIL_CODENEW")
    String tehsil_codenew;

    public MasterPincode(JSONObject jObj) {
        try {
            pin_cd = jObj.getString("Pincode");
            dist_cd = jObj.getString("District Code");
            actv_ind = jObj.getString("Active Status");
            tehsil_codenew = jObj.getString("Tehsil Code");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getPin_cd() {
        return pin_cd;
    }

    public void setPin_cd(String pin_cd) {
        this.pin_cd = pin_cd;
    }

    public String getPin_desc() {
        return pin_desc;
    }

    public void setPin_desc(String pin_desc) {
        this.pin_desc = pin_desc;
    }

    public String getCity_cd() {
        return city_cd;
    }

    public void setCity_cd(String city_cd) {
        this.city_cd = city_cd;
    }

    public String getStat_cd() {
        return stat_cd;
    }

    public void setStat_cd(String stat_cd) {
        this.stat_cd = stat_cd;
    }

    public String getDist_cd() {
        return dist_cd;
    }

    public void setDist_cd(String dist_cd) {
        this.dist_cd = dist_cd;
    }

    public String getActv_ind() {
        return actv_ind;
    }

    public void setActv_ind(String actv_ind) {
        this.actv_ind = actv_ind;
    }

    public String getTehsil_codenew() {
        return tehsil_codenew;
    }

    public void setTehsil_codenew(String tehsil_codenew) {
        this.tehsil_codenew = tehsil_codenew;
    }
}
