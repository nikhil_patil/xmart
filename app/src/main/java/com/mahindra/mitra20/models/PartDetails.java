package com.mahindra.mitra20.models;

public class PartDetails {
    private String amcFlag;
    private String quantity;
    private String partDescription;
    private String partNumber;
    private String billableType;
    private String partAmount;
    private String srl;

    public String getAmcFlag() {
        return amcFlag;
    }

    public void setAmcFlag(String amcFlag) {
        this.amcFlag = amcFlag;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getBillableType() {
        return billableType;
    }

    public void setBillableType(String billableType) {
        this.billableType = billableType;
    }

    public String getPartAmount() {
        return partAmount;
    }

    public void setPartAmount(String partAmount) {
        this.partAmount = partAmount;
    }

    public String getSrl() {
        return srl;
    }

    public void setSrl(String srl) {
        this.srl = srl;
    }
}