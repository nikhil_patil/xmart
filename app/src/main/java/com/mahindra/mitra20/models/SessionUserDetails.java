package com.mahindra.mitra20.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.models.Dealer;

import java.util.ArrayList;

import io.paperdb.Paper;

public class SessionUserDetails implements Parcelable {

    private String userID;
    private String userProfilePic;
    private String userName;
    private String dealerName;
    private Integer userType;
    private String employeeCode;
    private String actualUserID;

    private String MitraName;
    private String MitraCode;
    private String MitraType;
    private String ParentCode;
    private String parentName;
    private int currentUser;
    private ArrayList<Dealer> locationList;
    private String mitraMobileNum;
    public static SessionUserDetails instance;

    public SessionUserDetails() {
    }

    protected SessionUserDetails(Parcel in) {
        userID = in.readString();
        userProfilePic = in.readString();
        userName = in.readString();
        dealerName = in.readString();
        if (in.readByte() == 0) {
            userType = null;
        } else {
            userType = in.readInt();
        }
        employeeCode = in.readString();
        actualUserID = in.readString();
        MitraName = in.readString();
        MitraCode = in.readString();
        MitraType = in.readString();
        ParentCode = in.readString();
        parentName = in.readString();
        currentUser = in.readInt();
        mitraMobileNum = in.readString();
    }

    public static final Creator<SessionUserDetails> CREATOR = new Creator<SessionUserDetails>() {
        @Override
        public SessionUserDetails createFromParcel(Parcel in) {
            return new SessionUserDetails(in);
        }

        @Override
        public SessionUserDetails[] newArray(int size) {
            return new SessionUserDetails[size];
        }
    };

    public static SessionUserDetails getInstance() {
        if (instance == null) {
            try {
                //TODO add value in singleton object
                instance = Paper.book().read(CommonHelper.PAPER_DB_SESSION_USER_DETAILS);

                if (instance == null) {
                    instance = new SessionUserDetails();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public void assignSharedPrefValues(SessionUserDetails sessionUserDetails) {
        this.userID = sessionUserDetails.getUserID();
        this.userProfilePic = sessionUserDetails.getUserProfilePic();
        this.userName = sessionUserDetails.getUserName();
        this.dealerName = sessionUserDetails.getDealerName();
        this.userType = sessionUserDetails.getUserType();
        this.actualUserID = sessionUserDetails.getActualUserID();
        this.MitraName = sessionUserDetails.getMitraName();
        this.MitraCode = sessionUserDetails.getMitraCode();
        this.MitraType = sessionUserDetails.getMitraType();
        this.ParentCode = sessionUserDetails.getParentCode();
        this.parentName = sessionUserDetails.getParentName();
        this.setMitraMobileNum(sessionUserDetails.getMitraMobileNum());
        this.currentUser = sessionUserDetails.getCurrentUser();
        if (null != sessionUserDetails.getLocationList()) {
            this.locationList = new ArrayList<>();
            this.locationList.addAll(sessionUserDetails.getLocationList());
        }
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getActualUserID() {
        return actualUserID;
    }

    public void setActualUserID(String actualUserID) {
        this.actualUserID = actualUserID;
    }

    public String getUserProfilePic() {
        return userProfilePic;
    }

    public void setUserProfilePic(String userProfilePic) {
        this.userProfilePic = userProfilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getMitraName() {
        return MitraName;
    }

    public void setMitraName(String mitraName) {
        MitraName = mitraName;
    }

    public String getMitraCode() {
        return MitraCode;
    }

    public void setMitraCode(String mitraCode) {
        MitraCode = mitraCode;
    }

    public String getMitraType() {
        return MitraType;
    }

    public void setMitraType(String mitraType) {
        MitraType = mitraType;
    }

    public String getParentCode() {
        return ParentCode;
    }

    public void setParentCode(String parentCode) {
        ParentCode = parentCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public int getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(int currentUser) {
        this.currentUser = currentUser;
    }

    public ArrayList<Dealer> getLocationList() {
        return locationList;
    }

    public void setLocationList(ArrayList<Dealer> locationList) {
        this.locationList = locationList;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getMitraMobileNum() {
        return mitraMobileNum;
    }

    public void setMitraMobileNum(String mitraMobileNum) {
        this.mitraMobileNum = mitraMobileNum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userID);
        dest.writeString(userProfilePic);
        dest.writeString(userName);
        dest.writeString(dealerName);
        if (userType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userType);
        }
        dest.writeString(employeeCode);
        dest.writeString(actualUserID);
        dest.writeString(MitraName);
        dest.writeString(MitraCode);
        dest.writeString(MitraType);
        dest.writeString(ParentCode);
        dest.writeString(parentName);
        dest.writeInt(currentUser);
        dest.writeString(mitraMobileNum);
    }
}