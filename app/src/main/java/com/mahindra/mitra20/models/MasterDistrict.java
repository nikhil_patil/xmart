package com.mahindra.mitra20.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class MasterDistrict {
    @SerializedName("DIST_CD")
    String dist_cd;
    @SerializedName("STAT_CD")
    String stat_cd;
    @SerializedName("DIST_DESC")
    String dist_desc;
    @SerializedName("ACTV_IND")
    String actv_ind;

    public MasterDistrict(JSONObject jObj) {
        try {
            dist_cd = jObj.getString("District Code");
            stat_cd = jObj.getString("State Code");
            dist_desc = jObj.getString("District Name");
            actv_ind = jObj.getString("District Code");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDist_cd() {
        return dist_cd;
    }

    public void setDist_cd(String dist_cd) {
        this.dist_cd = dist_cd;
    }

    public String getStat_cd() {
        return stat_cd;
    }

    public void setStat_cd(String stat_cd) {
        this.stat_cd = stat_cd;
    }

    public String getDist_desc() {
        return dist_desc;
    }

    public void setDist_desc(String dist_desc) {
        this.dist_desc = dist_desc;
    }

    public String getActv_ind() {
        return actv_ind;
    }

    public void setActv_ind(String actv_ind) {
        this.actv_ind = actv_ind;
    }
}
