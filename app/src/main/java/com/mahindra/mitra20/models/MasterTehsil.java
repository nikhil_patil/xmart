package com.mahindra.mitra20.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class MasterTehsil {
    @SerializedName("DIST_CD")
    String dist_cd;
    @SerializedName("TEHSIL_CD")
    String tehsil_cd;
    @SerializedName("TEHSIL_DESC")
    String tehsil_desc;
    @SerializedName("ACTV_IND")
    String actv_ind;

    public MasterTehsil(JSONObject jObj) {
        try {
            tehsil_cd = jObj.getString("Tehsil Code");
            tehsil_desc = jObj.getString("Tehsil Name");
            actv_ind = jObj.getString("Active Status");
            dist_cd = jObj.getString("District Code");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDist_cd() {
        return dist_cd;
    }

    public void setDist_cd(String dist_cd) {
        this.dist_cd = dist_cd;
    }

    public String getTehsil_cd() {
        return tehsil_cd;
    }

    public void setTehsil_cd(String tehsil_cd) {
        this.tehsil_cd = tehsil_cd;
    }

    public String getTehsil_desc() {
        return tehsil_desc;
    }

    public void setTehsil_desc(String tehsil_desc) {
        this.tehsil_desc = tehsil_desc;
    }

    public String getActv_ind() {
        return actv_ind;
    }

    public void setActv_ind(String actv_ind) {
        this.actv_ind = actv_ind;
    }
}
