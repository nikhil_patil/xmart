package com.mahindra.mitra20.models;

public class DRdetails
{
    private String demandRepair;
    private String srl;

    public String getDemandRepair ()
    {
        return demandRepair;
    }

    public void setDemandRepair (String demandRepair)
    {
        this.demandRepair = demandRepair;
    }

    public String getSrl ()
    {
        return srl;
    }

    public void setSrl (String srl)
    {
        this.srl = srl;
    }
}