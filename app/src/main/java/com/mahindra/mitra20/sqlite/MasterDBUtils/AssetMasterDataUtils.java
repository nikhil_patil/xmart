package com.mahindra.mitra20.sqlite.MasterDBUtils;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mahindra.mitra20.MyApplication;
import com.mahindra.mitra20.models.MasterDistrict;
import com.mahindra.mitra20.models.MasterPincode;
import com.mahindra.mitra20.models.MasterState;
import com.mahindra.mitra20.models.MasterTehsil;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants;
import com.mahindra.mitra20.utils.AssetUtils;
import com.mahindra.mitra20.utils.LogUtil;

import java.util.List;

public class AssetMasterDataUtils extends MasterValueProvider {
    private MasterDatabaseHelper masterDatabaseHelper;

    public AssetMasterDataUtils() {
        masterDatabaseHelper = new MasterDatabaseHelper(MyApplication.getMyApplicationContext());
    }

    public void insertMastersDataFromAssets() {
        LogUtil.getInstance().logE("DB", "START INSERTION OF MASTER DATA FROM ASSET FILE");
        insertPincodeMst();
    }

    private void insertPincodeMst() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<MasterPincode> pincodeList =
                        new Gson().fromJson(
                                new AssetUtils().loadJSONFromAsset("PincodeMst.json"), //Read Pin code Json
                                new TypeToken<List<MasterPincode>>() {
                                }.getType()); //Convert json string to List<MasterPincodeConst> using Gson Library
                LogUtil.getInstance().logE("DB", "PIN CODE LIST SIZE: " + pincodeList.size());
                masterDatabaseHelper.insertPincodeMaster(pincodeList);
                LogUtil.getInstance().logE("DB", "INSERTED PIN CODE MASTER");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                insertStateMst();
            }
        }.execute();

    }

    private void insertStateMst() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<MasterState> stateList =
                        new Gson().fromJson(
                                new AssetUtils().loadJSONFromAsset("StateMst.json"), //Read Pin code Json
                                new TypeToken<List<MasterState>>() {
                                }.getType()); //Convert json string to List<MasterState> using Gson Library
                LogUtil.getInstance().logE("DB", "STATE LIST SIZE: " + stateList.size());
                masterDatabaseHelper.insertStateMaster(stateList);
                LogUtil.getInstance().logE("DB", "INSERTED STATE MASTER");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                insertDistrictMst();
            }
        }.execute();
    }

    private void insertDistrictMst() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<MasterDistrict> districtList =
                        new Gson().fromJson(
                                new AssetUtils().loadJSONFromAsset("DistrictMst.json"), //Read Pin code Json
                                new TypeToken<List<MasterDistrict>>() {
                                }.getType()); //Convert json string to List<MasterDistrict> using Gson Library

                LogUtil.getInstance().logE("DB", "DISTRICT LIST SIZE: " + districtList.size());
                masterDatabaseHelper.insertDistrictMaster(districtList);

                LogUtil.getInstance().logE("DB", "INSERTED DISTRICT MASTER");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                insertTehsilMst();
            }
        }.execute();
    }

    private void insertTehsilMst() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                LogUtil.getInstance().logE("DB", "START PARSING TEHSIL JSON");
                List<MasterTehsil> tehsilList =
                        new Gson().fromJson(
                                new AssetUtils().loadJSONFromAsset("TehsilMst.json"), //Read Pin code Json
                                new TypeToken<List<MasterTehsil>>() {
                                }.getType()); //Convert json string to List<MasterTehsil> using Gson Library

                masterDatabaseHelper.insertTehsilMaster(tehsilList);
                LogUtil.getInstance().logE("DB", "INSERTED TEHSIL MASTER");
                return null;
            }


        }.execute();
    }
}
