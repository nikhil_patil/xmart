package com.mahindra.mitra20.sqlite.MasterDBUtils;

import android.content.ContentValues;

import com.mahindra.mitra20.models.MasterDistrict;
import com.mahindra.mitra20.models.MasterPincode;
import com.mahindra.mitra20.models.MasterState;
import com.mahindra.mitra20.models.MasterTehsil;
import com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants;

public class MasterValueProvider {

    protected ContentValues getTehsilContentValue(MasterTehsil masterTehsil) {
        ContentValues values = new ContentValues(4);
        values.put(SQLiteDBHelperConstants.MasterTehsilConst.DIST_CD,  masterTehsil.getDist_cd());
        values.put(SQLiteDBHelperConstants.MasterTehsilConst.TEHSIL_CD,  masterTehsil.getTehsil_cd());
        values.put(SQLiteDBHelperConstants.MasterTehsilConst.TEHSIL_DESC,  masterTehsil.getTehsil_desc());
        values.put(SQLiteDBHelperConstants.MasterTehsilConst.ACTV_IND,  masterTehsil.getActv_ind());
        
        return values;
    }


    protected ContentValues getDistrictContentValue(MasterDistrict masterDistrict) {
        ContentValues values = new ContentValues(4);
        values.put(SQLiteDBHelperConstants.MasterDistrictConst.DIST_CD,  masterDistrict.getDist_cd());
        values.put(SQLiteDBHelperConstants.MasterDistrictConst.STAT_CD,  masterDistrict.getStat_cd());
        values.put(SQLiteDBHelperConstants.MasterDistrictConst.DIST_DESC,  masterDistrict.getDist_desc());
        values.put(SQLiteDBHelperConstants.MasterDistrictConst.ACTV_IND,  masterDistrict.getActv_ind());
        
        return values;
    }


    protected ContentValues getStateContentValue(MasterState masterState) {
        ContentValues values = new ContentValues(3);
        values.put(SQLiteDBHelperConstants.MasterStateConst.STAT_CD,  masterState.getStat_cd());
        values.put(SQLiteDBHelperConstants.MasterStateConst.STAT_DESC,  masterState.getStat_desc());
        values.put(SQLiteDBHelperConstants.MasterStateConst.ACTV_IND,  masterState.getActv_ind());
        
        return values;
    }

    protected ContentValues getPincodeContentValue(MasterPincode masterPincode) {
        ContentValues values = new ContentValues(7);
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.PIN_CD,  masterPincode.getPin_cd());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.PIN_DESC,  masterPincode.getPin_desc());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.CITY_CD,  masterPincode.getCity_cd());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.STAT_CD,  masterPincode.getStat_cd());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.DIST_CD,  masterPincode.getDist_cd());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.ACTV_IND,  masterPincode.getActv_ind());
        values.put(SQLiteDBHelperConstants.MasterPincodeConst.TEHSIL_CODENEW,  masterPincode.getTehsil_codenew());
        
        return values;
    }
}
