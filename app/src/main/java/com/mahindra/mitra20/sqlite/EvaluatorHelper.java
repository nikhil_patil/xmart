package com.mahindra.mitra20.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.CancelAppointmentModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Created by BADHAP-CONT on 8/7/2018.
 */

public class EvaluatorHelper {
    private final SQLiteDatabaseHelper sqLiteDatabaseHelper;
    private SQLiteDatabase db = null;
    private String currentDate1 = "";
    private String[] arrDate;
    private String strCDate;

    public EvaluatorHelper(Context context) {
        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
    }

    //----------------------------------Submit Evaluation---------------------------------
    public long addUpdateNewEvaluationDetails(NewEvaluation newEvaluation, String status) {
        long evalId = 0;
        boolean rowExists = false;
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            if (null != newEvaluation.getId() && !newEvaluation.getId().equals("")) {
                cursor = db.rawQuery("select * from " +
                        SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + " WHERE " +
                        SQLiteDBHelperConstants.newEvaluationMaster._ID + " = '"
                        + newEvaluation.getId() + "'", null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    rowExists = true;
                    evalId = cursor.getLong(cursor.getColumnIndex(SQLiteDBHelperConstants.newEvaluationMaster._ID));
                }
                cursor.close();
            } else if (null != newEvaluation.getRequestId() && !newEvaluation.getRequestId().equals("")) {
                cursor = db.rawQuery("select * from " +
                        SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + " WHERE " +
                        SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " = '"
                        + newEvaluation.getRequestId() + "'", null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    rowExists = true;
                    evalId = cursor.getLong(cursor.getColumnIndex(SQLiteDBHelperConstants.newEvaluationMaster._ID));
                }
                cursor.close();
            }
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RequestId,
                    newEvaluation.getRequestId());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EvaluatorId,
                    newEvaluation.getEvaluatorId());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EvaluationNo,
                    newEvaluation.getEvaluationNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AgainstTransaction,
                    newEvaluation.getAgainstTransaction());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Purpose,
                    newEvaluation.getPurpose());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.OptedNewVehicle,
                    newEvaluation.getOptedNewVehicle());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Date,
                    newEvaluation.getDate());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Time,
                    newEvaluation.getTime());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TransactionNo,
                    newEvaluation.getTransactionNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EvaluatorName,
                    newEvaluation.getEvaluatorName());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SourceOfTransaction,
                    newEvaluation.getSourceOfTransaction());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.OwnerName,
                    newEvaluation.getOwnerName());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Address,
                    newEvaluation.getAddress());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EmailId,
                    newEvaluation.getEmailId());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhoneNo,
                    newEvaluation.getPhoneNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.MobileNo,
                    newEvaluation.getMobileNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.State,
                    newEvaluation.getState());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.City,
                    newEvaluation.getCity());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PinCode,
                    newEvaluation.getPinCode());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Occupation,
                    newEvaluation.getOccupation());


            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Make,
                    newEvaluation.getMake());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Model,
                    newEvaluation.getModel());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Variant,
                    newEvaluation.getVariant());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.YearOfMfg,
                    newEvaluation.getYearOfMfg());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.YearOfReg,
                    newEvaluation.getYearOfReg());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RegNo,
                    newEvaluation.getRegNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Milege,
                    newEvaluation.getMileage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.MonthOfMfg,
                    newEvaluation.getMonthOfMfg());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.MonthOfReg,
                    newEvaluation.getMonthOfReg());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EngineNo,
                    newEvaluation.getEngineNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ChasisNo,
                    newEvaluation.getChassisNo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.NoOfOwners,
                    newEvaluation.getNoOfOwners());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Transmission,
                    newEvaluation.getTransmission());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ServiceBooklet,
                    newEvaluation.getServiceBooklet());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Color,
                    newEvaluation.getColor());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FuelType,
                    newEvaluation.getFuelType());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EuroVersion,
                    newEvaluation.getEuroVersion());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BodyType,
                    newEvaluation.getBodyType());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.NoOfSeats,
                    newEvaluation.getNoOfSeats());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RCBook,
                    newEvaluation.getRCBook());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Insurance,
                    newEvaluation.getInsurance());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.InsuranceDate,
                    newEvaluation.getInsuranceDate());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Ncb,
                    newEvaluation.getNcb());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Puc,
                    newEvaluation.getPuc());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Hypothecation,
                    newEvaluation.getHypothecation());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FinanceCompany,
                    newEvaluation.getFinanceCompany());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.NocStatus,
                    newEvaluation.getNocStatus());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.CustomerExpectedPrice,
                    newEvaluation.getCustomerExpectedPrice());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TestDrive,
                    newEvaluation.getTestDrive());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.IsAccident,
                    newEvaluation.getIsAccident());


            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Stereo,
                    newEvaluation.getStereo());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.NoOfKeys,
                    newEvaluation.getNoOfKeys());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ReverseParking,
                    newEvaluation.getReverseParking());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PowerSteering,
                    newEvaluation.getPowerSteering());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AlloyWheels,
                    newEvaluation.getAlloyWheels());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FogLamp,
                    newEvaluation.getFogLamp());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.CentralLocking,
                    newEvaluation.getCentralLocking());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ToolKitJack,
                    newEvaluation.getToolKitJack());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Comment,
                    newEvaluation.getComment());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.nnerTubelessTyres,
                    newEvaluation.getTubelessTyres());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RearWiper,
                    newEvaluation.getRearWiper());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.MusicSystem,
                    newEvaluation.getMusicSystem());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FMRadio,
                    newEvaluation.getFMRadio());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PowerWindow,
                    newEvaluation.getPowerWindow());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AC,
                    newEvaluation.getAC());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PowerAdjustableSeats,
                    newEvaluation.getPowerAdjustableSeats());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.KeyleassEntry,
                    newEvaluation.getKeyLessEntry());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PedalShifter,
                    newEvaluation.getPedalShifter());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ElectricallyAdjustableORVM,
                    newEvaluation.getElectricallyAdjustableORVM());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RearDefogger,
                    newEvaluation.getRearDefogger());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SunRoof,
                    newEvaluation.getSunRoof());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AutoClimateTechnologies,
                    newEvaluation.getAutoClimateTechnologies());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.LeatherSeats,
                    newEvaluation.getLeatherSeats());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BluetoothAudioSystems,
                    newEvaluation.getBluetoothAudioSystems());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.GPSNavigationSystems,
                    newEvaluation.getGPSNavigationSystems());


            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Odometer,
                    newEvaluation.getOdometer());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SeatCover,
                    newEvaluation.getSeatCover());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Dashboard,
                    newEvaluation.getDashboard());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AirBags,
                    newEvaluation.getAirBags());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontLHWindow,
                    newEvaluation.getFrontLHWindow());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontRHWindow,
                    newEvaluation.getFrontRHWindow());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RearLHWindow,
                    newEvaluation.getRearLHWindow());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RearRHWindow,
                    newEvaluation.getRearRHWindow());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PlatformPassengerCabin,
                    newEvaluation.getPlatformPassengerCabin());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PlatformBootSpace,
                    newEvaluation.getPlatformBootSpace());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontFenderRightSide,
                    newEvaluation.getFrontFenderRightSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontDoorRightSide,
                    newEvaluation.getFrontDoorRightSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackDoorRightSide,
                    newEvaluation.getBackDoorRightSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackFenderRightSide,
                    newEvaluation.getBackFenderRightSide());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Bonnet,
                    newEvaluation.getBonnet());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TailGate,
                    newEvaluation.getTailGate());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackBumper,
                    newEvaluation.getBackBumper());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontFenderLeftHandSide,
                    newEvaluation.getFrontFenderLeftHandSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontDoorLeftHandSide,
                    newEvaluation.getFrontDoorLeftHandSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackDoorLeftHandSide,
                    newEvaluation.getBackDoorLeftHandSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackFenderLeftHandSide,
                    newEvaluation.getBackFenderLeftHandSide());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontLight,
                    newEvaluation.getFrontLight());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BackLight,
                    newEvaluation.getBackLight());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.WindShield,
                    newEvaluation.getWindShield());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Pillars,
                    newEvaluation.getPillars());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Mirrors,
                    newEvaluation.getMirrors());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Wheels,
                    newEvaluation.getWheels());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TyreFrontLeft,
                    newEvaluation.getTyreFrontLeft());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TyreFrontRight,
                    newEvaluation.getTyreFrontRight());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TyreRearLeft,
                    newEvaluation.getTyreRearLeft());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TyreRearRight,
                    newEvaluation.getTyreRearRight());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SpareTyre,
                    newEvaluation.getSpareTyre());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontImage,
                    newEvaluation.getPhotoPathFrontImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearImage,
                    newEvaluation.getPhotoPathRearImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPath45DegreeLeftView,
                    newEvaluation.getPhotoPath45DegreeLeftView());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPath45DegreeRightView,
                    newEvaluation.getPhotoPath45DegreeRightView());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDashboardView,
                    newEvaluation.getPhotoPathDashboardView());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathOdometerImage,
                    newEvaluation.getPhotoPathOdometerImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathChasisNoImage,
                    newEvaluation.getPhotoPathChassisNoImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathEngineNoImage,
                    newEvaluation.getPhotoPathEngineNoImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathInteriorView,
                    newEvaluation.getPhotoPathInteriorView());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopyImage,
                    newEvaluation.getPhotoPathRCCopyImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathInsuranceImage,
                    newEvaluation.getPhotoPathInsuranceImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearLeftTyre,
                    newEvaluation.getPhotoPathRearLeftTyre());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearRightTyre,
                    newEvaluation.getPhotoPathRearRightTyre());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontLeftTyre,
                    newEvaluation.getPhotoPathFrontLeftTyre());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontRightTyre,
                    newEvaluation.getPhotoPathFrontRightTyre());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage1,
                    newEvaluation.getPhotoPathDamagedPartImage1());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage2,
                    newEvaluation.getPhotoPathDamagedPartImage2());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage3,
                    newEvaluation.getPhotoPathDamagedPartImage3());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage4,
                    newEvaluation.getPhotoPathDamagedPartImage4());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage5,
                    newEvaluation.getPhotoPathRoofTopImage());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy1,
                    newEvaluation.getPhotoPathRCCopy1());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy2,
                    newEvaluation.getPhotoPathRCCopy2());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy3,
                    newEvaluation.getPhotoPathRCCopy3());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BodyCost,
                    newEvaluation.getBodyCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BodyRemark,
                    newEvaluation.getStrBodyRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EngineCost,
                    newEvaluation.getEngineCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EngineRemark,
                    newEvaluation.getStrEngineRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIngnitionSystemCost,
                    newEvaluation.getFuelAndIgnitionSystemCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIgnitionSystemRemark,
                    newEvaluation.getStrFuelAndIgnitionSystemRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TransmissionCost,
                    newEvaluation.getTransmissionCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TransmissionRemark,
                    newEvaluation.getStrTransmissionRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemCost,
                    newEvaluation.getBreakSystemCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemRemark,
                    newEvaluation.getStrBreakSystemRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SuspensionCost,
                    newEvaluation.getSuspensionCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SuspensionSystemRemarks,
                    newEvaluation.getStrSuspensionSystemRemarks());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemCost,
                    newEvaluation.getSteeringSystemCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemRemark,
                    newEvaluation.getStrSteeringSystemRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AcCost,
                    newEvaluation.getAcCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ACSystemRemark,
                    newEvaluation.getStrACSystemRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemCost,
                    newEvaluation.getElectricalSystemCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemRemark,
                    newEvaluation.getStrElectricalSystemRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.totalOfAggregateCost,
                    newEvaluation.getTotalOfAggregateCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.generalServiceCost,
                    newEvaluation.getGeneralServiceCost());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.GeneralServiceCostRemark,
                    newEvaluation.getStrGeneralServiceCostRemark());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.totalRefurbishmentCost,
                    newEvaluation.getTotalRefurbishmentCost());

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.CNGKit,
                    newEvaluation.getCNGKit());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RcRemarks,
                    newEvaluation.getRcRemarks());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AccidentalType,
                    newEvaluation.getAccidentalType());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AccidentalRemarks,
                    newEvaluation.getAccidentalRemarks());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Latitude,
                    newEvaluation.getLatitude());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Longitude,
                    newEvaluation.getLongitude());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ScrapVehicle,
                    newEvaluation.getScrapVehicle());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Status, status);

            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BodyRemarkOther,
                    newEvaluation.getStrBodyRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EngineRemarkOther,
                    newEvaluation.getStrEngineRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIgnitionSystemRemarkOther,
                    newEvaluation.getStrFuelAndIgnitionSystemRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TransmissionRemarkOther,
                    newEvaluation.getStrTransmissionRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemRemarkOther,
                    newEvaluation.getStrBreakSystemRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SuspensionSystemRemarksOther,
                    newEvaluation.getStrSuspensionSystemRemarksOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemRemarkOther,
                    newEvaluation.getStrSteeringSystemRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ACSystemRemarkOther,
                    newEvaluation.getStrACSystemRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemRemarkOther,
                    newEvaluation.getStrElectricalSystemRemarkOther());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FitnessCertificate,
                    newEvaluation.getFitnessCertificate());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RoadTax,
                    newEvaluation.getRoadTax());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RoadTaxExpiryDate,
                    newEvaluation.getRoadTaxExpiryDate());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.FrontBumper,
                    newEvaluation.getFrontBumper());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorLH,
                    newEvaluation.getOutsideRearViewMirrorLH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorRH,
                    newEvaluation.getOutsideRearViewMirrorRH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PillarType,
                    newEvaluation.getPillarType());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.DickyDoor,
                    newEvaluation.getDickyDoor());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.BodyShell,
                    newEvaluation.getBodyShell());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Chassis,
                    newEvaluation.getChassis());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontLH,
                    newEvaluation.getApronFrontLH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontRH,
                    newEvaluation.getApronFrontRH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.StrutMountingArea,
                    newEvaluation.getStrutMountingArea());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Firewall,
                    newEvaluation.getFirewall());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.CowlTop,
                    newEvaluation.getCowlTop());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.UpperCrossMember,
                    newEvaluation.getUpperCrossMember());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.LowerCrossMember,
                    newEvaluation.getLowerCrossMember());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RadiatorSupport,
                    newEvaluation.getRadiatorSupport());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EngineRoomCarrierAssembly,
                    newEvaluation.getEngineRoomCarrierAssembly());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.HeadLightLH,
                    newEvaluation.getHeadLightLH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.HeadLightRH,
                    newEvaluation.getHeadLightRH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TailLightLH,
                    newEvaluation.getTailLightLH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.TailLightRH,
                    newEvaluation.getTailLightRH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardLH,
                    newEvaluation.getRunningBoardLH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardRH,
                    newEvaluation.getRunningBoardRH());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.PowerWindowType,
                    newEvaluation.getPowerWindowType());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AllWindowCondition,
                    newEvaluation.getAllWindowCondition());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.OverallFeedbackRating,
                    newEvaluation.getOverallFeedbackRating());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.AudioPathVehicleNoise,
                    newEvaluation.getAudioPathVehicleNoise());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.EvaluationCount,
                    newEvaluation.getEvaluationCount());
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.VehicleUsage,
                    newEvaluation.getVehicleUsage());

            if (rowExists) {
                if (null != newEvaluation.getId() && !newEvaluation.getId().isEmpty()) {
                    db.update(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, contentValues,
                            SQLiteDBHelperConstants.newEvaluationMaster._ID + " = ?",
                            new String[]{newEvaluation.getId()});
                } else if (null != newEvaluation.getRequestId() && !newEvaluation.getRequestId().isEmpty()) {
                    db.update(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, contentValues,
                            SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " = ?",
                            new String[]{newEvaluation.getRequestId()});
                }
            } else {
                evalId = db.insert(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME,
                        null, contentValues);
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return evalId;
    }

    //----------------------update status after submitting data to server------------------------------------
    public void updateDraftStatusToCompletedEvaluation(String requestId, String strId) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Status,
                    EvaluatorConstants.EVALUATION_DB_STATUS_COMPLETED);
            try {
                if (!requestId.equals("")) {
                    db.update(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, contentValues,
                            SQLiteDBHelperConstants.newEvaluationMaster.RequestId
                                    + "='" + requestId + "'", null);
                } else {
                    db.update(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, contentValues,
                            SQLiteDBHelperConstants.newEvaluationMaster._ID
                                    + "='" + strId + "'", null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //------------------------------------------------------------------------------------------------------
    public int getCountOfEvaluations(String type) {
        String strWhereCondition;
        int count = 0;
        if (type.equalsIgnoreCase("Draft")) {
            strWhereCondition = " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster.Status + " = '0'";
        } else {
            strWhereCondition = " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster.Status + " = '1'";
        }
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + strWhereCondition, null);
            count = cursor.getCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    //-------------------------------------------------------------------------
    public ArrayList<NewEvaluation> getEvaluationList(String status) {
        String whereCond = " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster.Status + " = '" + status + "'";
        ArrayList<NewEvaluation> scheduleAppointmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " +
                    SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + whereCond, null);
            if (cursor.moveToFirst()) {
                do {
                    NewEvaluation draftedEvaluation = new NewEvaluation();

                    draftedEvaluation.setId(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster._ID)));
                    draftedEvaluation.setRequestId(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.RequestId)));
                    draftedEvaluation.setDate(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Date)));
                    draftedEvaluation.setTime(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Time)));
                    draftedEvaluation.setPurpose(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Purpose)));
                    draftedEvaluation.setOwnerName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.OwnerName)));
                    draftedEvaluation.setAddress(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Address)));
                    draftedEvaluation.setMobileNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.MobileNo)));
                    draftedEvaluation.setMake(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Make)));
                    draftedEvaluation.setModel(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.newEvaluationMaster.Model)));
                    scheduleAppointmentArrayList.add(draftedEvaluation);
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return scheduleAppointmentArrayList;
    }

    //----------------------------------------Schedule Appoinment For Today------------------------------------------
    public void putScheduledAppointment(ArrayList<ScheduleAppoinment> listScheduleAppoinmentRequests) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < listScheduleAppoinmentRequests.size(); i++) {
                contentValues.clear();
                ScheduleAppoinment ScheduleAppoinment = listScheduleAppoinmentRequests.get(i);
                if (null != ScheduleAppoinment.getRequestID() && !ScheduleAppoinment.getRequestID().isEmpty()) {
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID, ScheduleAppoinment.getRequestID());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month, ScheduleAppoinment.getStrMonth());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId, ScheduleAppoinment.getStrCustId());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date, ScheduleAppoinment.getStrDate());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time, ScheduleAppoinment.getStrTime());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName, ScheduleAppoinment.getStrCustomerName());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName, ScheduleAppoinment.getStrMake() + "" + ScheduleAppoinment.getStrModel());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location, ScheduleAppoinment.getStrLocation());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo, ScheduleAppoinment.getStrContactNo());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.AppointmentConfirmationStatus, "0");
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus, ScheduleAppoinment.getRequestStatus());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make, ScheduleAppoinment.getStrMake());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model, ScheduleAppoinment.getStrModel());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource, ScheduleAppoinment.getEnquirySource());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType, ScheduleAppoinment.getEnquiryType());

                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Occupation, ScheduleAppoinment.getOccupation());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.OptedNewVehicle, ScheduleAppoinment.getOptedNewVehicle());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.PhoneNo, ScheduleAppoinment.getPhoneNo());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.State, ScheduleAppoinment.getState());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.YearOfManufacturing, ScheduleAppoinment.getYearOfManufacturing());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.City, ScheduleAppoinment.getCity());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Pincode, ScheduleAppoinment.getPincode());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Email, ScheduleAppoinment.getEmail());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes, "");
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname, ScheduleAppoinment.getSalesconsultantname());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum, ScheduleAppoinment.getSalesconsultantContactnum());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority, ScheduleAppoinment.getLeadPriority());
                    contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation, ScheduleAppoinment.getEnquiryLocation());
                    try {
                        long a = db.insert(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME, null, contentValues);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteScheduleRequestDetails(String requestStatus) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME,
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '" + requestStatus + "'", null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateScheduleAppointment(String requestId, String date, String time) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date, date);
            contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time, time);
            try {
                db.update(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID + "='" + requestId + "'", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateNoteForAppointment(String requestId, String strNotes) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes, strNotes);
            try {
                db.update(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID + "='" + requestId + "'", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getScheduleAppointmentNotes(String requestId) {
        String strNotes = "";
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursorNewEvaluation = db.rawQuery("select " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes + " from " +
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " where " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID + " = '" + requestId
                    + "'", null);

            if (cursorNewEvaluation.getCount() > 0) {
                if (cursorNewEvaluation.moveToFirst()) {
                    do {
                        strNotes = cursorNewEvaluation.getString(cursorNewEvaluation.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes));
                    } while (cursorNewEvaluation.moveToNext());
                }
            }
            cursorNewEvaluation.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strNotes;
    }

    //---------------------------------------------------------------------------------------------------------
    public ArrayList<ScheduleAppoinment> getScheduleAppointmentDetails(int limit, String requestStatus) {
        String strLimit = "";
        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursorDate = db.rawQuery(" SELECT datetime('now') as 'CurrentDate'", null);
            if (cursorDate.moveToFirst()) {

                do {
                    currentDate1 = cursorDate.getString(cursorDate.getColumnIndex("CurrentDate"));

                    arrDate = currentDate1.split(" ");
                    strCDate = arrDate[0];

                    String[] dt = strCDate.split("-");
                    strCDate = dt[2] + "/" + dt[1] + "/" + dt[0];
                }
                while (cursorDate.moveToNext());
            }

            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE " +
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '" + requestStatus + "'  ORDER BY " + SQLiteDBHelperConstants.ScheduleAppointmentMaster._ID +
                    strLimit, null);


            int i = 0;
            Cursor cursorNewEvaluation = db.rawQuery("select " + SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " from " +
                    SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, null);

            int cursorNewEvaluationCount = cursorNewEvaluation.getCount();
            String[] resultList = null;
            if (cursorNewEvaluation.getCount() > 0) {
                resultList = new String[cursorNewEvaluation.getCount()];
                if (cursorNewEvaluation.moveToFirst()) {
                    do {
                        String strRequestId = cursorNewEvaluation.getString(cursorNewEvaluation.getColumnIndex(SQLiteDBHelperConstants.newEvaluationMaster.RequestId));
                        resultList[i] = strRequestId;
                        i++;
                    } while (cursorNewEvaluation.moveToNext());
                }
            }

            cursorNewEvaluation.close();
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment scheduleAppoinment = new ScheduleAppoinment();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    scheduleAppoinment.setStrMake(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    scheduleAppoinment.setStrModel(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));
                    scheduleAppoinment.setOccupation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Occupation)));
                    scheduleAppoinment.setOptedNewVehicle(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.OptedNewVehicle)));
                    scheduleAppoinment.setPhoneNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.PhoneNo)));
                    scheduleAppoinment.setState(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.State)));
                    scheduleAppoinment.setYearOfManufacturing(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.YearOfManufacturing)));
                    scheduleAppoinment.setCity(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.City)));
                    scheduleAppoinment.setPincode(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Pincode)));
                    scheduleAppoinment.setEmail(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Email)));
                    scheduleAppoinment.setSalesconsultantContactnum(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum)));
                    scheduleAppoinment.setSalesconsultantname(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname)));
                    scheduleAppoinment.setLeadPriority(cursor.getInt(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority)));
                    scheduleAppoinment.setEnquiryLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation)));
                    scheduleAppoinment.setEnquirySource(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource)));
                    scheduleAppoinment.setEnquiryType(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);

                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = formatter.format(date);

                    if (sDate1.equals(strDate)) {
                        if (resultList != null) {
                            if (!Arrays.asList(resultList).contains(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)))) {
                                scheduleAppoinmentArrayList.add(scheduleAppoinment);
                            }
                        } else {
                            scheduleAppoinmentArrayList.add(scheduleAppoinment);
                        }
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (limit == 0) {
            return scheduleAppoinmentArrayList;
        } else {
            ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList1 = new ArrayList<>();
            if (scheduleAppoinmentArrayList.size() > 0) {
                if ((scheduleAppoinmentArrayList.size() == 2)) {
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 1)); // The last
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 2)); // The one before the last
                } else {
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 1)); // The last
                }
            }

            return scheduleAppoinmentArrayList1;
        }
    }

    public int getScheduledAppointmentCount(String requestStatus) {
        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursorDate = db.rawQuery(" SELECT datetime('now') as 'CurrentDate'", null);
            if (cursorDate.moveToFirst()) {

                do {
                    currentDate1 = cursorDate.getString(cursorDate.getColumnIndex("CurrentDate"));

                    arrDate = currentDate1.split(" ");
                    strCDate = arrDate[0];

                    String[] dt = strCDate.split("-");
                    strCDate = dt[2] + "/" + dt[1] + "/" + dt[0];
                }
                while (cursorDate.moveToNext());
            }

            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE " +
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '"
                            + requestStatus + "'  ORDER BY " + SQLiteDBHelperConstants.ScheduleAppointmentMaster._ID
                    , null);


            int i = 0;
            Cursor cursorNewEvaluation = db.rawQuery("select " + SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " from " +
                    SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, null);

            String[] resultList = null;
            if (cursorNewEvaluation.getCount() > 0) {
                resultList = new String[cursorNewEvaluation.getCount()];
                if (cursorNewEvaluation.moveToFirst()) {
                    do {
                        String strRequestId = cursorNewEvaluation.getString(cursorNewEvaluation.getColumnIndex(SQLiteDBHelperConstants.newEvaluationMaster.RequestId));
                        resultList[i] = strRequestId;
                        i++;
                    } while (cursorNewEvaluation.moveToNext());
                }
            }

            cursorNewEvaluation.close();
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment scheduleAppoinment = new ScheduleAppoinment();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    scheduleAppoinment.setStrMake(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    scheduleAppoinment.setStrModel(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));
                    scheduleAppoinment.setOccupation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Occupation)));
                    scheduleAppoinment.setOptedNewVehicle(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.OptedNewVehicle)));
                    scheduleAppoinment.setPhoneNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.PhoneNo)));
                    scheduleAppoinment.setState(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.State)));
                    scheduleAppoinment.setYearOfManufacturing(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.YearOfManufacturing)));
                    scheduleAppoinment.setCity(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.City)));
                    scheduleAppoinment.setPincode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Pincode)));
                    scheduleAppoinment.setEmail(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Email)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);

                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = formatter.format(date);

                    if (sDate1.equals(strDate)) {
                        if (resultList != null) {
                            if (!Arrays.asList(resultList).contains(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)))) {
                                scheduleAppoinmentArrayList.add(scheduleAppoinment);
                            }
                        } else {
                            scheduleAppoinmentArrayList.add(scheduleAppoinment);
                        }
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return scheduleAppoinmentArrayList.size();
    }

    //-----------------------------------------------------------------------------------------------------------
    public ArrayList<ScheduleAppoinment> getOverdueAppointmentDetails(int limit, String requestStatus) {
        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursorDate = db.rawQuery(" SELECT datetime('now') as 'CurrentDate'", null);
            if (cursorDate.moveToFirst()) {
                do {
                    currentDate1 = cursorDate.getString(cursorDate.getColumnIndex("CurrentDate"));

                    arrDate = currentDate1.split(" ");
                    strCDate = arrDate[0];

                    String[] dt = strCDate.split("-");
                    strCDate = dt[2] + "/" + dt[1] + "/" + dt[0];
                }
                while (cursorDate.moveToNext());
            }

            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE "
                    + SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '" + EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS + "'" +
                    " AND " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date + " != '' ORDER BY " + SQLiteDBHelperConstants.ScheduleAppointmentMaster._ID, null);

            int i = 0;
            Cursor cursorNewEvaluation = db.rawQuery("select " + SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " from " +
                    SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, null);

            String[] resultList = null;
            if (cursorNewEvaluation.getCount() > 0) {
                resultList = new String[cursorNewEvaluation.getCount()];
                if (cursorNewEvaluation.moveToFirst()) {
                    do {
                        String strRequestId = cursorNewEvaluation.getString(cursorNewEvaluation.getColumnIndex(SQLiteDBHelperConstants.newEvaluationMaster.RequestId));
                        resultList[i] = strRequestId;
                        i++;
                    } while (cursorNewEvaluation.moveToNext());
                }
            }

            cursorNewEvaluation.close();

            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment scheduleAppoinment = new ScheduleAppoinment();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    scheduleAppoinment.setStrMake(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    scheduleAppoinment.setStrModel(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));
                    scheduleAppoinment.setSalesconsultantname(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname)));
                    scheduleAppoinment.setSalesconsultantContactnum(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum)));
                    scheduleAppoinment.setLeadPriority(cursor.getInt(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority)));
                    scheduleAppoinment.setEnquiryLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation)));
                    scheduleAppoinment.setEnquirySource(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource)));
                    scheduleAppoinment.setEnquiryType(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    Date todayDate = new Date();
                    //Date date = new Date();
                    // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = formatter.format(todayDate);
                    Date databaseDate1 = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);

                    if (databaseDate.compareTo(databaseDate1) < 0) {
                        if (resultList != null) {
                            if (!Arrays.asList(resultList).contains(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)))) {
                                scheduleAppoinmentArrayList.add(scheduleAppoinment);
                            }
                        } else {
                            scheduleAppoinmentArrayList.add(scheduleAppoinment);
                        }
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (limit == 0) {
            return scheduleAppoinmentArrayList;
        } else {
            ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList1 = new ArrayList<>();
            if (scheduleAppoinmentArrayList.size() > 0) {
                if ((scheduleAppoinmentArrayList.size() >= 2)) {
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 1)); // The last
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 2)); // The one before the last
                } else {
                    scheduleAppoinmentArrayList1.add(scheduleAppoinmentArrayList.get(scheduleAppoinmentArrayList.size() - 1)); // The last
                }
            }
            return scheduleAppoinmentArrayList1;
        }
    }

    public int getOverdueAppointmentCount() {

        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();

            Cursor cursorDate = db.rawQuery(" SELECT datetime('now') as 'CurrentDate'", null);
            if (cursorDate.moveToFirst()) {
                do {
                    currentDate1 = cursorDate.getString(cursorDate.getColumnIndex("CurrentDate"));

                    arrDate = currentDate1.split(" ");
                    strCDate = arrDate[0];

                    String[] dt = strCDate.split("-");
                    strCDate = dt[2] + "/" + dt[1] + "/" + dt[0];
                }
                while (cursorDate.moveToNext());
            }

            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE "
                    + SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '" + EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS + "'" +
                    " AND " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date + " != '' ", null);

            int count = cursor.getCount();
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment scheduleAppoinment = new ScheduleAppoinment();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    scheduleAppoinment.setStrMake(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    scheduleAppoinment.setStrModel(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");


                    Date todayDate = new Date();
                    //Date date = new Date();
                    // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String strDate = formatter.format(todayDate);
                    Date databaseDate1 = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);

                    if (databaseDate.compareTo(databaseDate1) < 0) {
                        scheduleAppoinmentArrayList.add(scheduleAppoinment);
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return scheduleAppoinmentArrayList.size();
    }

    //------------------------------------------------------------------------------------------

    public ArrayList<ScheduleAppoinment> getUpcomingAppointmentDetails(String requestStatus) {

        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " where " +
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + "='" + requestStatus +
                            "' ORDER BY " + SQLiteDBHelperConstants.ScheduleAppointmentMaster._ID
                    , null);


            int count = cursor.getCount();
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment upcomingAppointment = new ScheduleAppoinment();
                    upcomingAppointment.setStrCustId(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    upcomingAppointment.setStrContactNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    upcomingAppointment.setStrMonth(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    upcomingAppointment.setStrDate(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    upcomingAppointment.setStrTime(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    upcomingAppointment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    upcomingAppointment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    upcomingAppointment.setStrLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    upcomingAppointment.setRequestID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    upcomingAppointment.setStrMake(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    upcomingAppointment.setStrModel(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));
                    upcomingAppointment.setSalesconsultantname(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname)));
                    upcomingAppointment.setSalesconsultantContactnum(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum)));
                    upcomingAppointment.setLeadPriority(cursor.getInt(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority)));
                    upcomingAppointment.setEnquiryLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation)));
                    upcomingAppointment.setEnquirySource(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource)));
                    upcomingAppointment.setEnquiryType(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy",
                            Locale.ENGLISH).parse(sDate1);

                    Date currentDate = new Date();
                    if (databaseDate.compareTo(currentDate) > 0) {
                        scheduleAppoinmentArrayList.add(upcomingAppointment);
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return scheduleAppoinmentArrayList;
    }

    public int getUpcomingAppointmentCount() {
        ArrayList<ScheduleAppoinment> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " +
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment upcomingAppointment = new ScheduleAppoinment();
                    upcomingAppointment.setStrCustId(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    upcomingAppointment.setStrContactNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    upcomingAppointment.setStrMonth(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    upcomingAppointment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    upcomingAppointment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    upcomingAppointment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    upcomingAppointment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    upcomingAppointment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    upcomingAppointment.setRequestID(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));

                    String sDate1 = cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date));
                    Date databaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date currentDate = new Date();
                    if (databaseDate.compareTo(currentDate) > 0) {
                        scheduleAppoinmentArrayList.add(upcomingAppointment);
                    }
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return scheduleAppoinmentArrayList.size();
    }

    //-------------------------------------------------------------------------------------------
    public NewEvaluation getEvaluationDetailsById(String requestId, String primaryId) {
        NewEvaluation newEvaluation = new NewEvaluation();
        String query = "select * from " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME;
        if (!requestId.equals("")) {
            if(requestId.contains("EVL")) {
                query = query + " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster.EvaluationNo + " = '" + requestId + "'";
            } else {
                query = query + " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster.RequestId + " = '" + requestId + "'";
            }
        } else {
            query = query + " WHERE " + SQLiteDBHelperConstants.newEvaluationMaster._ID + " = '" + primaryId + "'";
        }
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        newEvaluation.setId(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster._ID)));
                        newEvaluation.setRequestId(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RequestId)));
                        newEvaluation.setEvaluatorId(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EvaluatorId)));
                        newEvaluation.setEvaluationNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EvaluationNo)));
                        newEvaluation.setAgainstTransaction(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AgainstTransaction)));
                        newEvaluation.setPurpose(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Purpose)));
                        newEvaluation.setOptedNewVehicle(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.OptedNewVehicle)));
                        newEvaluation.setDate(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Date)));
                        newEvaluation.setTime(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Time)));
                        newEvaluation.setTransactionNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TransactionNo)));
                        newEvaluation.setEvaluatorName(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EvaluatorName)));
                        newEvaluation.setSourceOfTransaction(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SourceOfTransaction)));

                        newEvaluation.setOwnerName(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.OwnerName)));
                        newEvaluation.setAddress(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Address)));
                        newEvaluation.setEmailId(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EmailId)));
                        newEvaluation.setPhoneNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhoneNo)));
                        newEvaluation.setMobileNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.MobileNo)));
                        newEvaluation.setState(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.State)));
                        newEvaluation.setCity(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.City)));
                        newEvaluation.setPinCode(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PinCode)));
                        newEvaluation.setOccupation(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Occupation)));

                        newEvaluation.setMake(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Make)));
                        newEvaluation.setModel(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Model)));
                        newEvaluation.setVariant(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Variant)));
                        newEvaluation.setYearOfMfg(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.YearOfMfg)));
                        newEvaluation.setYearOfReg(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.YearOfReg)));
                        newEvaluation.setRegNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RegNo)));
                        newEvaluation.setMileage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Milege)));
                        newEvaluation.setMonthOfMfg(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.MonthOfMfg)));
                        newEvaluation.setMonthOfReg(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.MonthOfReg)));
                        newEvaluation.setEngineNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EngineNo)));
                        newEvaluation.setChassisNo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ChasisNo)));
                        newEvaluation.setNoOfOwners(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.NoOfOwners)));
                        newEvaluation.setTransmission(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Transmission)));
                        newEvaluation.setServiceBooklet(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ServiceBooklet)));
                        newEvaluation.setColor(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Color)));
                        newEvaluation.setFuelType(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FuelType)));
                        newEvaluation.setEuroVersion(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EuroVersion)));
                        newEvaluation.setBodyType(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BodyType)));
                        newEvaluation.setNoOfSeats(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.NoOfSeats)));
                        newEvaluation.setRCBook(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RCBook)));
                        newEvaluation.setInsurance(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Insurance)));
                        newEvaluation.setInsuranceDate(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.InsuranceDate)));
                        newEvaluation.setNcb(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Ncb)));
                        newEvaluation.setPuc(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Puc)));
                        newEvaluation.setHypothecation(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Hypothecation)));
                        newEvaluation.setFinanceCompany(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FinanceCompany)));
                        newEvaluation.setNocStatus(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.NocStatus)));
                        newEvaluation.setCustomerExpectedPrice(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.CustomerExpectedPrice)));
                        newEvaluation.setTestDrive(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TestDrive)));
                        newEvaluation.setIsAccident(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.IsAccident)));

                        newEvaluation.setBodyCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BodyCost)));
                        newEvaluation.setEngineCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EngineCost)));
                        newEvaluation.setFuelAndIgnitionSystemCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIngnitionSystemCost)));
                        newEvaluation.setTransmissionCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TransmissionCost)));
                        newEvaluation.setBreakSystemCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemCost)));
                        newEvaluation.setSuspensionCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SuspensionCost)));
                        newEvaluation.setSteeringSystemCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemCost)));
                        newEvaluation.setAcCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AcCost)));
                        newEvaluation.setElectricalSystemCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemCost)));
                        newEvaluation.setTotalOfAggregateCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.totalOfAggregateCost)));
                        newEvaluation.setGeneralServiceCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.generalServiceCost)));
                        newEvaluation.setTotalRefurbishmentCost(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.totalRefurbishmentCost)));

                        newEvaluation.setStereo(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Stereo)));
                        newEvaluation.setNoOfKeys(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.NoOfKeys)));
                        newEvaluation.setReverseParking(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ReverseParking)));
                        newEvaluation.setPowerSteering(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PowerSteering)));
                        newEvaluation.setAlloyWheels(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AlloyWheels)));
                        newEvaluation.setFogLamp(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FogLamp)));
                        newEvaluation.setCentralLocking(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.CentralLocking)));
                        newEvaluation.setToolKitJack(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ToolKitJack)));
                        newEvaluation.setComment(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Comment)));
                        newEvaluation.setTubelessTyres(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.nnerTubelessTyres)));
                        newEvaluation.setRearWiper(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RearWiper)));
                        newEvaluation.setMusicSystem(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.MusicSystem)));
                        newEvaluation.setFMRadio(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FMRadio)));
                        newEvaluation.setPowerWindow(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PowerWindow)));
                        newEvaluation.setPowerWindowType(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PowerWindowType)));
                        newEvaluation.setAC(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AC)));
                        newEvaluation.setPowerAdjustableSeats(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PowerAdjustableSeats)));
                        newEvaluation.setKeyLessEntry(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.KeyleassEntry)));
                        newEvaluation.setPedalShifter(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PedalShifter)));
                        newEvaluation.setElectricallyAdjustableORVM(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ElectricallyAdjustableORVM)));
                        newEvaluation.setRearDefogger(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RearDefogger)));
                        newEvaluation.setSunRoof(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SunRoof)));
                        newEvaluation.setAutoClimateTechnologies(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AutoClimateTechnologies)));
                        newEvaluation.setLeatherSeats(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.LeatherSeats)));
                        newEvaluation.setBluetoothAudioSystems(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BluetoothAudioSystems)));
                        newEvaluation.setGPSNavigationSystems(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.GPSNavigationSystems)));

                        newEvaluation.setOdometer(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Odometer)));
                        newEvaluation.setSeatCover(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SeatCover)));
                        newEvaluation.setDashboard(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Dashboard)));
                        newEvaluation.setAirBags(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AirBags)));
                        newEvaluation.setAllWindowCondition(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AllWindowCondition)));
                        newEvaluation.setFrontLHWindow(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontLHWindow)));
                        newEvaluation.setFrontRHWindow(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontRHWindow)));
                        newEvaluation.setRearLHWindow(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RearLHWindow)));
                        newEvaluation.setRearRHWindow(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RearRHWindow)));
                        newEvaluation.setPlatformPassengerCabin(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PlatformPassengerCabin)));
                        newEvaluation.setPlatformBootSpace(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PlatformBootSpace)));

                        newEvaluation.setFrontFenderRightSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontFenderRightSide)));
                        newEvaluation.setFrontDoorRightSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontDoorRightSide)));
                        newEvaluation.setBackDoorRightSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackDoorRightSide)));
                        newEvaluation.setBackFenderRightSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackFenderRightSide)));
                        newEvaluation.setBonnet(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Bonnet)));
                        newEvaluation.setTailGate(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TailGate)));
                        newEvaluation.setFrontFenderLeftHandSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontFenderLeftHandSide)));
                        newEvaluation.setFrontDoorLeftHandSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontDoorLeftHandSide)));
                        newEvaluation.setBackDoorLeftHandSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackDoorLeftHandSide)));
                        newEvaluation.setBackFenderLeftHandSide(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackFenderLeftHandSide)));
                        newEvaluation.setHeadLightLH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.HeadLightLH)));
                        newEvaluation.setHeadLightRH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.HeadLightRH)));
                        newEvaluation.setTailLightLH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TailLightLH)));
                        newEvaluation.setTailLightRH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TailLightRH)));
                        newEvaluation.setWindShield(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.WindShield)));
                        newEvaluation.setDickyDoor(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.DickyDoor)));
                        newEvaluation.setPillars(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Pillars)));
                        newEvaluation.setPillarType(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PillarType)));
                        newEvaluation.setMirrors(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Mirrors)));
                        newEvaluation.setFrontBumper(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontBumper)));
                        newEvaluation.setBackBumper(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackBumper)));
                        newEvaluation.setOutsideRearViewMirrorLH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorLH)));
                        newEvaluation.setOutsideRearViewMirrorRH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorRH)));
                        newEvaluation.setBodyShell(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BodyShell)));
                        newEvaluation.setChassis(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Chassis)));
                        newEvaluation.setApronFrontLH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontLH)));
                        newEvaluation.setApronFrontRH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontRH)));
                        newEvaluation.setStrutMountingArea(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.StrutMountingArea)));
                        newEvaluation.setFirewall(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Firewall)));
                        newEvaluation.setUpperCrossMember(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.UpperCrossMember)));
                        newEvaluation.setLowerCrossMember(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.LowerCrossMember)));
                        newEvaluation.setCowlTop(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.CowlTop)));
                        newEvaluation.setRadiatorSupport(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RadiatorSupport)));
                        newEvaluation.setEngineRoomCarrierAssembly(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EngineRoomCarrierAssembly)));
                        newEvaluation.setRunningBoardLH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardLH)));
                        newEvaluation.setRunningBoardRH(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardRH)));

                        newEvaluation.setFrontLight(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FrontLight)));
                        newEvaluation.setBackLight(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BackLight)));

                        newEvaluation.setWheels(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Wheels)));
                        newEvaluation.setTyreFrontLeft(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TyreFrontLeft)));
                        newEvaluation.setTyreFrontRight(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TyreFrontRight)));
                        newEvaluation.setTyreRearLeft(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TyreRearLeft)));
                        newEvaluation.setTyreRearRight(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TyreRearRight)));
                        newEvaluation.setSpareTyre(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SpareTyre)));

                        newEvaluation.setPhotoPathFrontImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontImage)));
                        newEvaluation.setPhotoPathRearImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearImage)));
                        newEvaluation.setPhotoPath45DegreeLeftView(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPath45DegreeLeftView)));
                        newEvaluation.setPhotoPath45DegreeRightView(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPath45DegreeRightView)));
                        newEvaluation.setPhotoPathDashboardView(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDashboardView)));
                        newEvaluation.setPhotoPathOdometerImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathOdometerImage)));
                        newEvaluation.setPhotoPathChassisNoImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathChasisNoImage)));
                        newEvaluation.setPhotoPathEngineNoImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathEngineNoImage)));
                        newEvaluation.setPhotoPathInteriorView(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathInteriorView)));
                        newEvaluation.setPhotoPathRCCopyImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopyImage)));
                        newEvaluation.setPhotoPathInsuranceImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathInsuranceImage)));
                        newEvaluation.setPhotoPathRearLeftTyre(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearLeftTyre)));
                        newEvaluation.setPhotoPathRearRightTyre(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRearRightTyre)));
                        newEvaluation.setPhotoPathFrontLeftTyre(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontLeftTyre)));
                        newEvaluation.setPhotoPathFrontRightTyre(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathFrontRightTyre)));
                        newEvaluation.setPhotoPathDamagedPartImage1(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage1)));
                        newEvaluation.setPhotoPathDamagedPartImage2(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage2)));
                        newEvaluation.setPhotoPathDamagedPartImage3(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage3)));
                        newEvaluation.setPhotoPathDamagedPartImage4(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage4)));
                        newEvaluation.setPhotoPathRoofTopImage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathDamagedPartImage5)));
                        newEvaluation.setPhotoPathRCCopy1(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy1)));
                        newEvaluation.setPhotoPathRCCopy2(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy2)));
                        newEvaluation.setPhotoPathRCCopy3(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy3)));

                        newEvaluation.setStrBodyRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BodyRemark)));
                        newEvaluation.setStrEngineRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EngineRemark)));
                        newEvaluation.setStrFuelAndIgnitionSystemRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIgnitionSystemRemark)));
                        newEvaluation.setStrTransmissionRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TransmissionRemark)));
                        newEvaluation.setStrBreakSystemRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemRemark)));
                        newEvaluation.setStrSuspensionSystemRemarks(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SuspensionSystemRemarks)));
                        newEvaluation.setStrSteeringSystemRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemRemark)));
                        newEvaluation.setStrACSystemRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ACSystemRemark)));
                        newEvaluation.setStrElectricalSystemRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemRemark)));
                        newEvaluation.setStrGeneralServiceCostRemark(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.GeneralServiceCostRemark)));

                        newEvaluation.setCNGKit(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.CNGKit)));
                        newEvaluation.setRcRemarks(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RcRemarks)));
                        newEvaluation.setAccidentalType(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AccidentalType)));
                        newEvaluation.setAccidentalRemarks(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AccidentalRemarks)));
                        newEvaluation.setLatitude(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Latitude)));
                        newEvaluation.setLongitude(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.Longitude)));
                        newEvaluation.setScrapVehicle(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ScrapVehicle)));

                        newEvaluation.setStrBodyRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BodyRemarkOther)));
                        newEvaluation.setStrEngineRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EngineRemarkOther)));
                        newEvaluation.setStrFuelAndIgnitionSystemRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIgnitionSystemRemarkOther)));
                        newEvaluation.setStrTransmissionRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.TransmissionRemarkOther)));
                        newEvaluation.setStrBreakSystemRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemRemarkOther)));
                        newEvaluation.setStrSuspensionSystemRemarksOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SuspensionSystemRemarksOther)));
                        newEvaluation.setStrSteeringSystemRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemRemarkOther)));
                        newEvaluation.setStrACSystemRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ACSystemRemarkOther)));
                        newEvaluation.setStrElectricalSystemRemarkOther(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemRemarkOther)));
                        newEvaluation.setFitnessCertificate(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.FitnessCertificate)));
                        newEvaluation.setRoadTax(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RoadTax)));
                        newEvaluation.setRoadTaxExpiryDate(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.RoadTaxExpiryDate)));


                        newEvaluation.setOverallFeedbackRating(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.OverallFeedbackRating)));
                        newEvaluation.setAudioPathVehicleNoise(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.AudioPathVehicleNoise)));
                        newEvaluation.setEvaluationCount(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.EvaluationCount)));
                        newEvaluation.setVehicleUsage(cursor.getString(cursor.getColumnIndex(
                                SQLiteDBHelperConstants.newEvaluationMaster.VehicleUsage)));
                    }
                    while (cursor.moveToNext());
                }
                cursor.close();
                return newEvaluation;
            } else {
                return newEvaluation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    //---------------------------Cancel Appointment--------------------------------------
    public void cancelAppointment(String reason, String requestId, String strNotes) {
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE " +
                    SQLiteDBHelperConstants.CancelAppointmentMaster.RequestID + " = '" + requestId + "'", null);
            ContentValues contentValues = new ContentValues();
            if (cursor.moveToFirst()) {

                do {
                    contentValues.clear();
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.RequestID, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Month, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.CustId, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Date, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Time, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.CustomerName, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.VehicleName, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Location, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.ContactNo, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.CancellationStatus, "Pending");
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.CancellationReason, reason);
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Notes, strNotes);
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Make, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make)));
                    contentValues.put(SQLiteDBHelperConstants.CancelAppointmentMaster.Model, cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model)));

                    db.insert(SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME, null, contentValues);


                    db.delete(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME,
                            SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID + "='" + requestId + "'", null);
                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<CancelAppointmentModel> getCancelledRequest() {
        ArrayList<CancelAppointmentModel> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();

            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME
                    , null);
            if (cursor.moveToFirst()) {
                do {
                    CancelAppointmentModel scheduleAppoinment = new CancelAppointmentModel();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Location)));
                    scheduleAppoinment.setStrMake(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Make)));
                    scheduleAppoinment.setStrModel(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Model)));

                    scheduleAppoinmentArrayList.add(scheduleAppoinment);

                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return scheduleAppoinmentArrayList;
    }

    public ArrayList<CancelAppointmentModel> getCancellationDetails(String requestId) {
        ArrayList<CancelAppointmentModel> scheduleAppoinmentArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME + " WHERE " +
                            SQLiteDBHelperConstants.CancelAppointmentMaster.RequestID + " = '" + requestId + "'"
                    , null); //AND "+SQLiteDBHelperConstants.ScheduleAppointmentMaster.Status +" = 0
            int count = cursor.getCount();
            if (cursor.moveToFirst()) {
                do {
                    CancelAppointmentModel scheduleAppoinment = new CancelAppointmentModel();
                    scheduleAppoinment.setStrCustId(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.CustId)));
                    scheduleAppoinment.setRequestID(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.RequestID)));
                    scheduleAppoinment.setStrContactNo(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.ContactNo)));
                    scheduleAppoinment.setStrMonth(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Month)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Time)));
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.Location)));
                    scheduleAppoinment.setStrCancel(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.CancelAppointmentMaster.CancellationReason)));

                    scheduleAppoinmentArrayList.add(scheduleAppoinment);

                }
                while (cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return scheduleAppoinmentArrayList;

    }

    //-------------Truncate table--------------------------------------------

    public void truncateTable() {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        db.execSQL("delete from " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.EvaluatorMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.EvaluatorMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.MitraMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.MitraMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.OverdueMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.OverdueMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.UpcomingAppointmentMaster.TABLE_NAME);
        LogUtil.getInstance().logE("logout", "Delete table : " + SQLiteDBHelperConstants.UpcomingAppointmentMaster.TABLE_NAME);
        db.close();
    }

    //------------Confirm Appointment---------------------------------------
    public void confirmedAppointment(String requestId) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(SQLiteDBHelperConstants.confirmedApppointment.requestId, requestId);
            db.insert(SQLiteDBHelperConstants.confirmedApppointment.TABLE_NAME, null, contentValues);

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int isAppointmentConfirmed(String strRequestId) {
        int count = 0;
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.confirmedApppointment.TABLE_NAME + " WHERE " +
                            SQLiteDBHelperConstants.confirmedApppointment.requestId + " = '" + strRequestId + "'"
                    , null); //AND "+SQLiteDBHelperConstants.ScheduleAppointmentMaster.Status +" = 0
            count = cursor.getCount();

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;

    }


    //------------Pcs List------------------------------------------------------------------------------------------------------------------------
    public void putPcsScheduledAppointment(ArrayList<ScheduleAppoinment> listPCSAppointments) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < listPCSAppointments.size(); i++) {
                contentValues.clear();
                ScheduleAppoinment ScheduleAppoinment = listPCSAppointments.get(i);
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestID, ScheduleAppoinment.getRequestID());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Month, ScheduleAppoinment.getStrMonth());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustId, ScheduleAppoinment.getStrCustId());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date, ScheduleAppoinment.getStrDate());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time, ScheduleAppoinment.getStrTime());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName, ScheduleAppoinment.getStrCustomerName());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName, ScheduleAppoinment.getStrMake() + "" + ScheduleAppoinment.getStrModel());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Location, ScheduleAppoinment.getStrLocation());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.ContactNo, ScheduleAppoinment.getStrContactNo());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.AppointmentConfirmationStatus, "0");
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus, ScheduleAppoinment.getRequestStatus());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Make, ScheduleAppoinment.getStrMake());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Model, ScheduleAppoinment.getStrModel());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Occupation, ScheduleAppoinment.getOccupation());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.OptedNewVehicle, ScheduleAppoinment.getOptedNewVehicle());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.PhoneNo, ScheduleAppoinment.getPhoneNo());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.State, ScheduleAppoinment.getState());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.YearOfManufacturing, ScheduleAppoinment.getYearOfManufacturing());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.City, ScheduleAppoinment.getCity());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Pincode, ScheduleAppoinment.getPincode());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Email, ScheduleAppoinment.getEmail());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes, "");
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname, ScheduleAppoinment.getSalesconsultantname());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum, ScheduleAppoinment.getSalesconsultantContactnum());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority, ScheduleAppoinment.getLeadPriority());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation, ScheduleAppoinment.getEnquiryLocation());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource, ScheduleAppoinment.getEnquirySource());
                contentValues.put(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType, ScheduleAppoinment.getEnquiryType());
                try {
                    long a = db.insert(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME, null, contentValues);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deletePcsScheduleRequestDetails(String procurementStatusKey) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME,
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.RequestStatus + " = '" + procurementStatusKey + "'", null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<ScheduleAppoinment> getPcsScheduledAppointment() {
        ArrayList<ScheduleAppoinment> scheduleAppoinments = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + "=" + '"' + EvaluatorConstants.PROCUREMENT_STATUS + '"', null);
            if (cursor.moveToFirst()) {
                do {
                    ScheduleAppoinment scheduleAppoinment = new ScheduleAppoinment();
                    scheduleAppoinment.setStrCustomerName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CustomerName)));
                    scheduleAppoinment.setStrDate(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Date)));
                    scheduleAppoinment.setStrTime(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Time)));
                    scheduleAppoinment.setSalesconsultantname(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname)));
                    scheduleAppoinment.setStrVehicleName(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.VehicleName)));
                    scheduleAppoinment.setStrLocation(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation)));
                    scheduleAppoinments.add(scheduleAppoinment);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scheduleAppoinments;
    }

    public int getPcsScheduledAppointmentCount() {
        Cursor cursor = null;
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME + " WHERE " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + "=" + '"' + EvaluatorConstants.PROCUREMENT_STATUS + '"', null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor.getCount();
    }

    public String getNextEvaluationId() {
        long rowId = 0;
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            cursor = db.rawQuery("select * from SQLITE_SEQUENCE", null);
            if (cursor.moveToFirst()) {
                do {
                    if(cursor.getString(cursor.getColumnIndex("name")).equalsIgnoreCase("NewEvaluationMaster")) {
                        rowId = cursor.getInt(cursor.getColumnIndex("seq"));
                        break;
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return String.valueOf(rowId + 1);
    }

    //ADDED BY PRAVIN DHARAM ON 04-01-2020
    public boolean deleteDraftEntry(String evaluationID) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
        int result = 0;
        try {
            result = db.delete(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, SQLiteDBHelperConstants.newEvaluationMaster._ID+"='"+evaluationID+"'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            db.close();
        }
        return result>0;
    }
}