package com.mahindra.mitra20.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.models.MasterDistrict;
import com.mahindra.mitra20.models.MasterPincode;
import com.mahindra.mitra20.models.MasterState;
import com.mahindra.mitra20.models.MasterTehsil;
import com.mahindra.mitra20.sqlite.MasterDBUtils.MasterValueProvider;
import com.mahindra.mitra20.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.ColorMaster;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.MakeMaster;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.MasterDistrictConst;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.MasterPincodeConst;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.MasterStateConst;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.MasterTehsilConst;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.ModelMaster;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.VariantMaster;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.VehicleSegmentMaster;
import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.makeMasterModel;

public class MasterDatabaseHelper extends MasterValueProvider {
    private SQLiteDatabaseHelper sqLiteDatabaseHelper;

    public MasterDatabaseHelper() {
        //TODO
    }

    public MasterDatabaseHelper(Context context) {
        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
    }

    public boolean isMasterSync() {
        int count = 0;
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ModelMaster.TABLE_NAME, null);
            count = cursor.getCount();
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count > 0;
    }


    public void insertColorMaster(ArrayList<MasterModel> arrColorMaster) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < arrColorMaster.size(); i++) {
                contentValues.clear();
                contentValues.put(ColorMaster.code, arrColorMaster.get(i).getCode());
                contentValues.put(ColorMaster.description, arrColorMaster.get(i).getDescription());
                db.insert(ColorMaster.TABLE_NAME, null, contentValues);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

//    public void insertStateMaster(ArrayList<MasterModel> arrStateMaster) {
//        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
//        try {
//            db.beginTransaction();
//            ContentValues contentValues = new ContentValues();
//            for (int i = 0; i < arrStateMaster.size(); i++) {
//
//                contentValues.clear();
//                contentValues.put(ColorMaster.code, arrStateMaster.get(i).getCode());
//                contentValues.put(ColorMaster.description, arrStateMaster.get(i).getDescription());
//                db.insert(StateMaster.TABLE_NAME, null, contentValues);
//            }
//            db.setTransactionSuccessful();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            db.endTransaction();
//            db.close();
//        }
//    }

//    public void insertCityMaster(ArrayList<MasterModel> arrCityMaster) {
//        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
//        try {
//            db.beginTransaction();
//            ContentValues contentValues = new ContentValues();
//            for (int i = 0; i < arrCityMaster.size(); i++) {
//
//                contentValues.clear();
//                contentValues.put(ColorMaster.code, arrCityMaster.get(i).getCode());
//                contentValues.put(ColorMaster.description, arrCityMaster.get(i).getDescription());
//                db.insert(CityMaster.TABLE_NAME, null, contentValues);
//            }
//            db.setTransactionSuccessful();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            db.endTransaction();
//            db.close();
//        }
//    }

    public void insertVariantMaster(ArrayList<MasterModel> arrVariantMaster) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < arrVariantMaster.size(); i++) {
                contentValues.clear();
                contentValues.put(VariantMaster.code, arrVariantMaster.get(i).getCode());
                contentValues.put(VariantMaster.description, arrVariantMaster.get(i).getDescription().split("\\|")[1]);
                contentValues.put(VariantMaster.model, arrVariantMaster.get(i).getDescription().split("\\|")[0]);
                db.insert(VariantMaster.TABLE_NAME, null, contentValues);
            }

            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void insertMakeMaster(ArrayList<MasterModel> arrMakeMaster) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < arrMakeMaster.size(); i++) {

                contentValues.clear();
                contentValues.put(ColorMaster.code, arrMakeMaster.get(i).getCode());
                contentValues.put(ColorMaster.description, arrMakeMaster.get(i).getDescription());
                db.insert(MakeMaster.TABLE_NAME, null, contentValues);
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void insertVehicleSegmentMaster(ArrayList<MasterModel> arrVehicleSegmentMaster) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < arrVehicleSegmentMaster.size(); i++) {

                contentValues.clear();
                contentValues.put(ColorMaster.code, arrVehicleSegmentMaster.get(i).getCode());
                contentValues.put(ColorMaster.description, arrVehicleSegmentMaster.get(i).getDescription());
                db.insert(VehicleSegmentMaster.TABLE_NAME, null, contentValues);
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void insertModelMaster(ArrayList<MasterModel> arrModelMaster) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < arrModelMaster.size(); i++) {

                contentValues.clear();
                contentValues.put(ModelMaster.code, arrModelMaster.get(i).getCode());
                contentValues.put(ModelMaster.description, arrModelMaster.get(i).getDescription().split("\\|")[1]);
                contentValues.put(ModelMaster.make, arrModelMaster.get(i).getDescription().split("\\|")[0]);
                db.insert(ModelMaster.TABLE_NAME, null, contentValues);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void putDmsMasterModel(ArrayList<DmsMakeMasterModel> listScheduleAppoinmentRequests) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            db.beginTransaction();
            for (int i = 0; i < listScheduleAppoinmentRequests.size(); i++) {

                contentValues.clear();
                DmsMakeMasterModel dmsMasterModel = listScheduleAppoinmentRequests.get(i);
                contentValues.put(makeMasterModel.Make, dmsMasterModel.getMake());
                contentValues.put(makeMasterModel.MakeCode, dmsMasterModel.getMakeCode());
                contentValues.put(makeMasterModel.ModelCode, dmsMasterModel.getModelCode());
                contentValues.put(makeMasterModel.ModelDesc, dmsMasterModel.getModelDesc());
                long a = db.insert(makeMasterModel.TABLE_NAME, null, contentValues);

            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void insertPincodeMaster(List<MasterPincode> pincodeList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();

            for (int i = 0; i < pincodeList.size(); i++) {
                long a = db.insert(MasterPincodeConst.TABLE_NAME, null, getPincodeContentValue(pincodeList.get(i)));
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updatePincodeMaster(List<MasterPincode> pincodeList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < pincodeList.size(); i++) {

                String whereClause = MasterPincodeConst.PIN_CD + "=? AND " +
                        MasterPincodeConst.DIST_CD + "=? AND " +
                        MasterPincodeConst.TEHSIL_CODENEW + "=?";
                String[] whereArgs = new String[]{pincodeList.get(i).getPin_cd(), pincodeList.get(i).getDist_cd(), pincodeList.get(i).getTehsil_codenew()};


                long a = db.update(MasterPincodeConst.TABLE_NAME, getPincodeContentValue(pincodeList.get(i)), whereClause, whereArgs);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    public void insertStateMaster(List<MasterState> stateList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < stateList.size(); i++) {
                long a = db.insert(MasterStateConst.TABLE_NAME, null, getStateContentValue(stateList.get(i)));
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateStateMaster(List<MasterState> stateList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < stateList.size(); i++) {

                String whereClause = MasterStateConst.STAT_CD + "=?";
                String[] whereArgs = new String[]{stateList.get(i).getStat_cd()};


                long a = db.update(MasterStateConst.TABLE_NAME, getStateContentValue(stateList.get(i)), whereClause, whereArgs);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    public void insertDistrictMaster(List<MasterDistrict> districtList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < districtList.size(); i++) {
                long a = db.insert(MasterDistrictConst.TABLE_NAME, null, getDistrictContentValue(districtList.get(i)));
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateDistrictMaster(List<MasterDistrict> districtList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < districtList.size(); i++) {

                String whereClause = MasterDistrictConst.DIST_CD + "=? AND " +
                        MasterDistrictConst.STAT_CD + "=?";
                String[] whereArgs = new String[]{districtList.get(i).getDist_cd(), districtList.get(i).getStat_cd()};

                long a = db.update(MasterDistrictConst.TABLE_NAME, getDistrictContentValue(districtList.get(i)), whereClause, whereArgs);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    public void insertTehsilMaster(List<MasterTehsil> tehsilList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < tehsilList.size(); i++) {
                long a = db.insert(MasterTehsilConst.TABLE_NAME, null, getTehsilContentValue(tehsilList.get(i)));
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateTehsilMaster(List<MasterTehsil> tehsilList) {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < tehsilList.size(); i++) {

                String whereClause = MasterTehsilConst.TEHSIL_CD + "=? AND " +
                        MasterTehsilConst.DIST_CD + "=?";
                String[] whereArgs = new String[]{tehsilList.get(i).getTehsil_cd(), tehsilList.get(i).getDist_cd()};

                long a = db.update(MasterTehsilConst.TABLE_NAME, getTehsilContentValue(tehsilList.get(i)), whereClause, whereArgs);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public ArrayList<MasterModel> getStateList() {
        ArrayList<MasterModel> stateArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + MasterStateConst.TABLE_NAME + " WHERE " + MasterStateConst.ACTV_IND + "='Y' ORDER BY " + MasterStateConst.STAT_DESC + " ASC", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(MasterStateConst.STAT_CD)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(MasterStateConst.STAT_DESC)));
                    stateArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateArrayList;
    }

    public ArrayList<MasterModel> getDistrictList(String stateID) {
        ArrayList<MasterModel> districtList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select " + MasterDistrictConst.DIST_CD + "," + MasterDistrictConst.DIST_DESC + " from " + MasterDistrictConst.TABLE_NAME + " WHERE " + MasterDistrictConst.STAT_CD + " = '" + stateID + "' AND " + MasterDistrictConst.ACTV_IND + "='Y' ORDER BY " + MasterDistrictConst.DIST_DESC + " ASC ", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(MasterDistrictConst.DIST_CD)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(MasterDistrictConst.DIST_DESC)));
                    districtList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return districtList;
    }

    public ArrayList<MasterModel> getTehsilList(String districtID) {
        ArrayList<MasterModel> tehsilList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select " + MasterTehsilConst.TEHSIL_CD + "," + MasterTehsilConst.TEHSIL_DESC + " from " + MasterTehsilConst.TABLE_NAME + " WHERE " + MasterTehsilConst.DIST_CD + " = '" + districtID + "' AND " + MasterTehsilConst.ACTV_IND + "='Y'  ORDER BY " + MasterTehsilConst.TEHSIL_DESC + " ASC ", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(MasterTehsilConst.TEHSIL_CD)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(MasterTehsilConst.TEHSIL_DESC)));
                    tehsilList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tehsilList;
    }

    public ArrayList<MasterModel> getPincodeList(String tehsilID) {
        ArrayList<MasterModel> pincodeList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select  " + MasterPincodeConst.PIN_CD + " from " + MasterPincodeConst.TABLE_NAME + " WHERE " + MasterPincodeConst.TEHSIL_CODENEW + " = '" + tehsilID + "' AND " + MasterPincodeConst.ACTV_IND + "='Y'  ORDER BY " + MasterPincodeConst.PIN_DESC + " ASC ", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(MasterPincodeConst.PIN_CD)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(MasterPincodeConst.PIN_CD)));
                    pincodeList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pincodeList;
    }

    public String getDistrictID(String pinCode) {
        String districtID = null;
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select  " + MasterTehsilConst.DIST_CD + " from "
                    + MasterPincodeConst.TABLE_NAME + " WHERE " + MasterPincodeConst.PIN_CD
                    + " = '" + pinCode + "' AND " + MasterTehsilConst.ACTV_IND + "='Y' ", null);
            if (cursor.moveToFirst()) {
                do {
                    districtID = cursor.getString(cursor.getColumnIndex(MasterTehsilConst.DIST_CD));
                    break;
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return districtID;
    }

    public ArrayList<MasterModel> getColors() {
        ArrayList<MasterModel> colorsArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ColorMaster.TABLE_NAME + " ORDER BY " + SQLiteDBHelperConstants.ColorMaster.description + " ASC", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ColorMaster.code)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ColorMaster.description)));
                    colorsArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return colorsArrayList;
    }

    public ArrayList<MasterModel> getVariantMaster(String strCode) {
        ArrayList<MasterModel> VariantArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.VariantMaster.TABLE_NAME + " where " + SQLiteDBHelperConstants.VariantMaster.model + " = '" + strCode + "'" + " ORDER BY " + SQLiteDBHelperConstants.MakeMaster.description + " ASC", null);

            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();
                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.VariantMaster.code)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.VariantMaster.description)));
                    masterModel.setParentType(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.VariantMaster.model)));
                    VariantArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return VariantArrayList;
    }

    public ArrayList<MasterModel> getMakeMaster() {
        ArrayList<MasterModel> MakeArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.MakeMaster.TABLE_NAME + " ORDER BY " +
                    SQLiteDBHelperConstants.MakeMaster.description + " ASC", null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.MakeMaster.code)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.MakeMaster.description)));
                    MakeArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MakeArrayList;
    }

    public ArrayList<MasterModel> getVehicleSegmentMaster() {
        ArrayList<MasterModel> ModelArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.VehicleSegmentMaster.TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ModelMaster.code)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ModelMaster.description)));
                    ModelArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ModelArrayList;
    }

    public ArrayList<MasterModel> getModelMaster(String make) {
        ArrayList<MasterModel> ModelArrayList = new ArrayList<>();
        try {
            Cursor cursor;
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            if (make.length() > 0) {
                cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ModelMaster.TABLE_NAME + " where " + SQLiteDBHelperConstants.ModelMaster.make + "='" + make + "' ORDER BY " + SQLiteDBHelperConstants.ModelMaster.description + " ASC", null);
            } else {
                cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.ModelMaster.TABLE_NAME + " ORDER BY " + SQLiteDBHelperConstants.ModelMaster.description + " ASC", null);
            }
            if (cursor.moveToFirst()) {
                do {
                    MasterModel masterModel = new MasterModel();

                    masterModel.setCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ModelMaster.code)));
                    masterModel.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ModelMaster.description)));
                    masterModel.setParentType(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.ModelMaster.make)));
                    ModelArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ModelArrayList;
    }

    public ArrayList<DmsMakeMasterModel> getDmsMasterModel() {
        ArrayList<DmsMakeMasterModel> colorsArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.makeMasterModel.TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    DmsMakeMasterModel masterModel = new DmsMakeMasterModel();

                    masterModel.setMakeCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.makeMasterModel.MakeCode)));
                    masterModel.setMake(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.makeMasterModel.Make)));
                    masterModel.setModelCode(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.makeMasterModel.ModelCode)));
                    masterModel.setModelDesc(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.makeMasterModel.ModelDesc)));
                    masterModel.setSortOrder(cursor.getString(cursor.getColumnIndex(SQLiteDBHelperConstants.makeMasterModel.sortOrder)));
                    colorsArrayList.add(masterModel);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return colorsArrayList;
    }

    public void truncateMasterTable() {
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();

        db.execSQL("delete from " + SQLiteDBHelperConstants.ColorMaster.TABLE_NAME);
        //COMMENTED BY PRAVIN DHARAM ON 13-12-2019 //CHANGES IN CITY MASTER
//        db.execSQL("delete from " + SQLiteDBHelperConstants.StateMaster.TABLE_NAME);
//        db.execSQL("delete from " + SQLiteDBHelperConstants.CityMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.VariantMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.MakeMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.ModelMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.VehicleSegmentMaster.TABLE_NAME);
        db.execSQL("delete from " + SQLiteDBHelperConstants.makeMasterModel.TABLE_NAME);
        db.close();

    }

    public void insertUpdatePincodeList(List<MasterPincode> pincodeList) {
        List<MasterPincode> insertList = new ArrayList<>();
        List<MasterPincode> updateList = new ArrayList<>();

        for (int i = 0; i < pincodeList.size(); i++) {

            String[] columnName = new String[]{MasterPincodeConst.PIN_CD, MasterPincodeConst.DIST_CD, MasterPincodeConst.TEHSIL_CODENEW};
            String[] columnValue = new String[]{pincodeList.get(i).getPin_cd(), pincodeList.get(i).getDist_cd(), pincodeList.get(i).getTehsil_codenew()};

            if (sqLiteDatabaseHelper.isExist(MasterPincodeConst.TABLE_NAME, columnName, columnValue))
                updateList.add(pincodeList.get(i));
            else
                insertList.add(pincodeList.get(i));

        }

        LogUtil.getInstance().logE("DB", "PINCODE UPDATE LIST SIZE: " + updateList.size());
        LogUtil.getInstance().logE("DB", "PINCODE INSERT LIST SIZE: " + insertList.size());

        if (updateList.size() > 0) {

            updatePincodeMaster(updateList);
        }

        if (insertList.size() > 0) {

            insertPincodeMaster(insertList);
        }

    }

    public void insertUpdateStateList(List<MasterState> states) {
        List<MasterState> insertList = new ArrayList<>();
        List<MasterState> updateList = new ArrayList<>();

        for (int i = 0; i < states.size(); i++) {

            String[] columnName = new String[]{MasterStateConst.STAT_CD};
            String[] columnValue = new String[]{states.get(i).getStat_cd()};

            if (sqLiteDatabaseHelper.isExist(MasterStateConst.TABLE_NAME, columnName, columnValue))
                updateList.add(states.get(i));
            else
                insertList.add(states.get(i));

        }
        LogUtil.getInstance().logE("DB", "STATE UPDATE LIST SIZE: " + updateList.size());
        LogUtil.getInstance().logE("DB", "STATE INSERT LIST SIZE: " + insertList.size());
        if (updateList.size() > 0) {

            updateStateMaster(updateList);
        }

        if (insertList.size() > 0) {

            insertStateMaster(insertList);
        }

    }

    public void insertUpdateDistrictList(List<MasterDistrict> districts) {
        List<MasterDistrict> insertList = new ArrayList<>();
        List<MasterDistrict> updateList = new ArrayList<>();

        for (int i = 0; i < districts.size(); i++) {

            String[] columnName = new String[]{MasterDistrictConst.DIST_CD, MasterDistrictConst.STAT_CD};
            String[] columnValue = new String[]{districts.get(i).getDist_cd(), districts.get(i).getStat_cd()};

            if (sqLiteDatabaseHelper.isExist(MasterDistrictConst.TABLE_NAME, columnName, columnValue))
                updateList.add(districts.get(i));
            else
                insertList.add(districts.get(i));

        }
        LogUtil.getInstance().logE("DB", "DISTRICT UPDATE LIST SIZE: " + updateList.size());
        LogUtil.getInstance().logE("DB", "DISTRICT INSERT LIST SIZE: " + insertList.size());
        if (updateList.size() > 0) {

            updateDistrictMaster(updateList);
        }

        if (insertList.size() > 0) {

            insertDistrictMaster(insertList);
        }

    }

    public void insertUpdateTehsilList(List<MasterTehsil> tehsils) {
        List<MasterTehsil> insertList = new ArrayList<>();
        List<MasterTehsil> updateList = new ArrayList<>();

        for (int i = 0; i < tehsils.size(); i++) {

            String[] columnName = new String[]{MasterTehsilConst.TEHSIL_CD, MasterTehsilConst.DIST_CD};
            String[] columnValue = new String[]{tehsils.get(i).getTehsil_cd(), tehsils.get(i).getDist_cd()};

            if (sqLiteDatabaseHelper.isExist(MasterTehsilConst.TABLE_NAME, columnName, columnValue))
                updateList.add(tehsils.get(i));
            else
                insertList.add(tehsils.get(i));

        }

        LogUtil.getInstance().logE("DB", "TEHSIL UPDATE LIST SIZE: " + updateList.size());
        LogUtil.getInstance().logE("DB", "TEHSIL INSERT LIST SIZE: " + insertList.size());
        if (updateList.size() > 0) {

            updateTehsilMaster(updateList);
        }

        if (insertList.size() > 0) {

            insertTehsilMaster(insertList);
        }

    }

}
