package com.mahindra.mitra20.sqlite;

/**
 * Created by user on 8/1/2018.
 */

public interface SQLiteDBHelperConstants {

    String DATABASE_NAME = "DB_MITRA";
    int DATABASE_VERSION = 17;

    interface ColorMaster {
        String TABLE_NAME = "ColorMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface StateMaster {
        String TABLE_NAME = "StateMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    interface CityMaster {
        String TABLE_NAME = "CityMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    interface VariantMaster {
        String TABLE_NAME = "VariantMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";
        String model = "Model";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT, " +
                model + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface MakeMaster {
        String TABLE_NAME = "MakeMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface ModelMaster {
        String TABLE_NAME = "ModelMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";
        String make = "Make";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT, " +
                make + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface VehicleSegmentMaster {
        String TABLE_NAME = "VehicleSegmentMaster";
        String _ID = "ID";
        String code = "Code";
        String description = "Description";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                code + " TEXT, " +
                description + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface EnquiryRequestMaster {
        String TABLE_NAME = "EnquiryRequestMaster";
        String _ID = "_ID";
        String RequestID = "RequestID";
        String RequestStatus = "RequestStatus";
        String PinCode = "PinCode";
        String NameOfEnquiry = "NameOfEnquiry";
        String Make = "Make";
        String Model = "Model";
        String DateOfAppointment = "DateOfAppointment";
        String ContactNo = "ContactNo";
        String Location = "Location";
        String AuctionID = "AuctionID";
        String EvaluationReportID = "evaluationReportID";
        String EvaluatorID = "evaluatorID";
        String TimeOfAppointment = "timeOfAppointment";
        String YearOfManifacturing = "yearOfManifacturing";
        String CustomerResponse = "CustomerResponse";
        String AppointmentStatus = "AppointmentStatus";
        String CustomerExpectedPrice = "ExpectedPrice";
        String OptedNewVehicle = "OptedNewVehicle";
        String Salesconsultantname = "Salesconsultantname";
        String SalesconsultantContactnum = "SalesconsultantContactnum";
        String LeadPriority = "leadPriority";
        String EnquiryLocation = "enquirylocation";
        String EnquirySource = "enquirysource";
        String EnquiryType = "enquirytype";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RequestID + " TEXT, " +
                NameOfEnquiry + " TEXT, " +
                RequestStatus + " TEXT, " +
                Make + " TEXT, " +
                Model + " TEXT, " +
                DateOfAppointment + " TEXT, " +
                ContactNo + " TEXT, " +
                Location + " TEXT, " +
                AuctionID + " TEXT, " +
                EvaluationReportID + " TEXT, " +
                EvaluatorID + " TEXT, " +
                PinCode + " TEXT, " +
                TimeOfAppointment + " TEXT, " +
                YearOfManifacturing + " TEXT, " +
                CustomerResponse + " TEXT, " +
                AppointmentStatus + " TEXT, " +
                CustomerExpectedPrice + " TEXT, " +
                OptedNewVehicle + " TEXT, " +
                Salesconsultantname + " TEXT, " +
                SalesconsultantContactnum + " TEXT, " +
                LeadPriority + " INTEGER, " +
                EnquiryLocation + " TEXT, " +
                EnquirySource + " TEXT, " +
                EnquiryType + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface EvaluatorMaster {
        String TABLE_NAME = "EvaluatorMaster";
        String _ID = "_ID";
        String ALLOCATED_REQ_COUNT = "ALLOCATED_REQ_COUNT";
        String Evaluator_Contact_Number = "Evaluator_Contact_Number";
        String Evaluator_ID = "Evaluator_ID";
        String Evaluator_Name = "Evaluator_Name";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ALLOCATED_REQ_COUNT + " TEXT, " +
                Evaluator_Contact_Number + " TEXT, " +
                Evaluator_ID + " TEXT, " +
                Evaluator_Name + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    interface AuctionMaster {
        String TABLE_NAME = "AuctionMaster";
        String _ID = "_ID";
        String AUCTION_DATE = "Auction_Date";
        String AUCTION_ID = "Auction_ID";
        String EVALUATION_REPORT_ID = "EVALUATION_ID";
        String AUCTION_STATUS = "Auction_Status";
        String BIDDER_COUNT = "Count_of_bidders";
        String MITRA_PRICE = "MITRA_price";
        String REQUEST_ID = "Request_ID";
        String START_TIMESTAMP = "Start_timestamp";
        String REMAINING_TIME = "remaining_time_in_seconds";
        String VEH_OWNER = "veh_Owner_name";
        String VEH_NAME = "vehicle_name";
        String CONTACT_NO = "CONTACT_NO";
        String HighestPrice = "HighestPrice";
        String HighestBidder = "HighestBidder";
        String NegotiatorContactName = "NegotiatorContactName";
        String NegotiatorContactNumber = "NegotiatorContactNumber";
        String NoOfOwners = "NoOfOwners";
        String OdometerReading = "OdometerReading";
        String VehicleUsage = "VehicleUsage";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                AUCTION_DATE + " TEXT, " +
                AUCTION_ID + " TEXT PRIMARY KEY, " +
                EVALUATION_REPORT_ID + " TEXT, " +
                AUCTION_STATUS + " TEXT, " +
                BIDDER_COUNT + " TEXT, " +
                MITRA_PRICE + " TEXT, " +
                REQUEST_ID + " TEXT, " +
                START_TIMESTAMP + " TEXT, " +
                REMAINING_TIME + " TEXT, " +
                VEH_OWNER + " TEXT, " +
                VEH_NAME + " TEXT, " +
                CONTACT_NO + " TEXT, " +
                HighestPrice + " TEXT, " +
                HighestBidder + " TEXT, " +
                NegotiatorContactName + " TEXT, " +
                NegotiatorContactNumber + " TEXT, " +
                NoOfOwners + " TEXT, " +
                OdometerReading + " TEXT, " +
                VehicleUsage + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    interface MitraMaster {
        String TABLE_NAME = "MitraMster";
        String _ID = "_ID";
        String AUCTION_ID = "Auction_ID";
        String MITRA_ID = "MITRA_id";
        String MITRA_NAME = "MITRA_name";
        String BID_PRICE = "bid_price";
        String BID_DATE = "biding_date_time";
        String COMMISSION_PRICE = "commissionPrice";
        String DEDUCTED_PRICE = "deductedPrice";
        String CONTACT_NO = "CONTACT_NO";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AUCTION_ID + " TEXT, " +
                MITRA_ID + " TEXT, " +
                MITRA_NAME + " TEXT, " +
                BID_PRICE + " TEXT, " +
                BID_DATE + " TEXT, " +
                COMMISSION_PRICE + " TEXT, " +
                DEDUCTED_PRICE + " TEXT, " +
                CONTACT_NO + " TEXT, " +
                "foreign key (" + AUCTION_ID + ") references " + AuctionMaster.TABLE_NAME + "(" + AuctionMaster.AUCTION_ID + ")" +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface newEvaluationMaster {
        String TABLE_NAME = "NewEvaluationMaster";
        String _ID = "_ID";
        String RequestId = "requestId";
        String EvaluatorId = "EvaluatorId";
        String EvaluationNo = "EvaluationNo";
        String AgainstTransaction = "AgainstTransaction";
        String Purpose = "Purpose";
        String OptedNewVehicle = "OptedNewVehicle";
        String Date = "Date";
        String Time = "Time";
        String TransactionNo = "TransactionNo";
        String EvaluatorName = "EvaluatorName";
        String SourceOfTransaction = "SourceOfTransaction";
        String OwnerName = "OwnerName";
        String Address = "Address";
        String EmailId = "EmailId";
        String PhoneNo = "PhoneNo";
        String MobileNo = "MobileNo";
        String State = "State";
        String City = "City";
        String PinCode = "PinCode";
        String Occupation = "Occupation";
        String Make = "Make";
        String Model = "Model";
        String Variant = "Variant";
        String YearOfMfg = "YearOfMfg";
        String YearOfReg = "YearOfReg";
        String RegNo = "RegNo";
        String Milege = "Milege";
        String MonthOfMfg = "MonthOfMfg";
        String MonthOfReg = "MonthOfReg";
        String EngineNo = "EngineNo";
        String ChasisNo = "ChasisNo";
        String NoOfOwners = "NoOfOwners";
        String Transmission = "Transmission";
        String ServiceBooklet = "ServiceBooklet";
        String Color = "Color";
        String FuelType = "FuelType";
        String EuroVersion = "EuroVersion";
        String BodyType = "BodyType";
        String NoOfSeats = "NoOfSeats";
        String RCBook = "RCBook";
        String Insurance = "Insurance";
        String InsuranceDate = "InsuranceDate";
        String Ncb = "Ncb";
        String Puc = "Puc";
        String Hypothecation = "Hypothecation";
        String FinanceCompany = "FinanceCompany";
        String NocStatus = "NocStatus";
        String CustomerExpectedPrice = "CustomerExpectedPrice";
        String TestDrive = "TestDrive";
        String IsAccident = "IsAccident";
        String BodyCost = "BodyCost";
        String EngineCost = "EngineCost";
        String FuelAndIngnitionSystemCost = "FuelAndIngnitionSystemCost";
        String TransmissionCost = "TransmissionCost";
        String BreakSystemCost = "BreakSystemCost";
        String SuspensionCost = "SuspensionCost";
        String SteeringSystemCost = "SteeringSystemCost";
        String AcCost = "AcCost";
        String ElectricalSystemCost = "ElectricalSystemCost";
        String totalOfAggregateCost = "totalOfAggregateCost";
        String generalServiceCost = "generalServiceCost";
        String totalRefurbishmentCost = "totalRefurbishmentCost";
        String Stereo = "Stereo";
        String NoOfKeys = "NoOfKeys";
        String ReverseParking = "ReverseParking";
        String PowerSteering = "PowerSteering";
        String AlloyWheels = "AlloyWheels";
        String FogLamp = "FogLamp";
        String CentralLocking = "CentralLocking";
        String ToolKitJack = "ToolKitJack";
        String Comment = "Comment";
        String nnerTubelessTyres = "nnerTubelessTyres";
        String RearWiper = "RearWiper";
        String MusicSystem = "MusicSystem";
        String FMRadio = "FMRadio";
        String PowerWindow = "PowerWindow";
        String AC = "AC";
        String PowerAdjustableSeats = "PowerAdjustableSeats";
        String KeyleassEntry = "KeyleassEntry";
        String PedalShifter = "PedalShifter";
        String ElectricallyAdjustableORVM = "ElectricallyAdjustableORVM";
        String RearDefogger = "RearDefogger";
        String SunRoof = "SunRoof";
        String AutoClimateTechnologies = "AutoClimateTechnologies";
        String LeatherSeats = "LeatherSeats";
        String BluetoothAudioSystems = "BluetoothAudioSystems";
        String GPSNavigationSystems = "GPSNavigationSystems";
        String Odometer = "Odometer";
        String SeatCover = "SeatCover";
        String Dashboard = "Dashboard";
        String AirBags = "AirBags";
        String FrontFenderRightSide = "FrontFenderRightSide";
        String FrontDoorRightSide = "FrontDoorRightSide";
        String BackDoorRightSide = "BackDoorRightSide";
        String BackFenderRightSide = "BackFenderRightSide";
        String Bonnet = "Bonnet";
        String TailGate = "TailGate";
        String BackBumper = "BackBumper";
        String FrontFenderLeftHandSide = "FrontFenderLeftHandSide";
        String FrontDoorLeftHandSide = "FrontDoorLeftHandSide";
        String BackDoorLeftHandSide = "BackDoorLeftHandSide";
        String BackFenderLeftHandSide = "BackFenderLeftHandSide";
        String FrontLight = "FrontLight";
        String BackLight = "BackLight";
        String WindShield = "WindShield";
        String Pillars = "Pillars";
        String Mirrors = "Mirrors";
        String Wheels = "Wheels";
        String TyreFrontLeft = "TyreFrontLeft";
        String TyreFrontRight = "TyreFrontRight";
        String TyreRearLeft = "TyreRearLeft";
        String TyreRearRight = "TyreRearRight";
        String SpareTyre = "SpareTyre";
        String PhotoPathFrontImage = "PhotoPathFrontImage";
        String PhotoPathRearImage = "PhotoPathRearImage";
        String PhotoPath45DegreeLeftView = "PhotoPath45DegreeLeftView";
        String PhotoPath45DegreeRightView = "PhotoPath45DegreeRightView";
        String PhotoPathDashboardView = "PhotoPathDashboardView";
        String PhotoPathOdometerImage = "PhotoPathOdometerImage";
        String PhotoPathChasisNoImage = "PhotoPathChasisNoImage";
        String PhotoPathEngineNoImage = "PhotoPathEngineNoImage";
        String PhotoPathInteriorView = "PhotoPathInteriorView";
        String PhotoPathRCCopyImage = "PhotoPathRCCopyImage";
        String PhotoPathInsuranceImage = "PhotoPathInsuranceImage";
        String PhotoPathRearLeftTyre = "PhotoPathRearLeftTyre";
        String PhotoPathRearRightTyre = "PhotoPathRearRightTyre";
        String PhotoPathFrontLeftTyre = "PhotoPathFrontLeftTyre";
        String PhotoPathFrontRightTyre = "PhotoPathFrontRightTyre";
        String PhotoPathDamagedPartImage1 = "PhotoPathDamagedPartImage1";
        String PhotoPathDamagedPartImage2 = "PhotoPathDamagedPartImage2";
        String PhotoPathDamagedPartImage3 = "PhotoPathDamagedPartImage3";
        String PhotoPathDamagedPartImage4 = "PhotoPathDamagedPartImage4";
        String PhotoPathDamagedPartImage5 = "PhotoPathDamagedPartImage5";
        String Status = "Status";
        String BodyRemark = "BodyRemark";
        String EngineRemark = "EngineRemark";
        String FuelAndIgnitionSystemRemark = "FuelAndIgnitionSystemRemark";
        String TransmissionRemark = "TransmissionRemark";
        String BreakSystemRemark = "BreakSystemRemark";
        String SuspensionSystemRemarks = "SuspensionSystemRemarks";
        String SteeringSystemRemark = "SteeringSystemRemark";
        String ACSystemRemark = "ACSystemRemark";
        String ElectricalSystemRemark = "ElectricalSystemRemark";
        String GeneralServiceCostRemark = "GeneralServiceCostRemark";
        String SyncFlagPhotoFrontImage = "SyncPhotoFrontImage";
        String SyncFlagPhotoRearImage = "SyncPhotoRearImage";
        String SyncFlagPhoto45DegreeLeftView = "SyncPhoto45DegreeLeftView";
        String SyncFlagPhoto45DegreeRightView = "SyncPhoto45DegreeRightView";
        String SyncFlagPhotoDashboardView = "SyncPhotoDashboardView";
        String SyncFlagPhotoOdometerImage = "SyncPhotoOdometerImage";
        String SyncFlagPhotoChasisNoImage = "SyncPhotoChasisNoImage";
        String SyncFlagPhotoEngineNoImage = "SyncPhotoEngineNoImage";
        String SyncFlagPhotoInteriorView = "SyncPhotoInteriorView";
        String SyncFlagPhotoRCCopyImage = "SyncPhotoRCCopyImage";
        String SyncFlagPhotoInsuranceImage = "SyncPhotoInsuranceImage";
        String SyncFlagPhotoRearLeftTyre = "SyncPhotoRearLeftTyre";
        String SyncFlagPhotoRearRightTyre = "SyncPhotoRearRightTyre";
        String SyncFlagPhotoFrontLeftTyre = "SyncPhotoFrontLeftTyre";
        String SyncFlagPhotoFrontRightTyre = "SyncPhotoFrontRightTyre";
        String SyncFlagPhotoDamagedPartImage1 = "SyncPhotoDamagedPartImage1";
        String SyncFlagPhotoDamagedPartImage2 = "SyncPhotoDamagedPartImage2";
        String SyncFlagPhotoDamagedPartImage3 = "SyncPhotoDamagedPartImage3";
        String SyncFlagPhotoDamagedPartImage4 = "SyncPhotoDamagedPartImage4";
        String SyncFlagPhotoDamagedPartImage5 = "SyncPhotoDamagedPartImage5";

        String CNGKit = "CNGKit";
        String RcRemarks = "RcRemarks";
        String AccidentalType = "AccidentalType";
        String AccidentalRemarks = "AccidentalRemarks";
        String Latitude = "Latitude";
        String Longitude = "Longitude";
        String ScrapVehicle = "ScrapVehicle";

        String PhotoPathRCCopy1 = "PhotoPathRCCopy1";
        String PhotoPathRCCopy2 = "PhotoPathRCCopy2";
        String PhotoPathRCCopy3 = "PhotoPathRCCopy3";

        String BodyRemarkOther = "BodyRemarkOther";
        String EngineRemarkOther = "EngineRemarkOther";
        String FuelAndIgnitionSystemRemarkOther = "FuelAndIgnitionSystemRemarkOther";
        String TransmissionRemarkOther = "TransmissionRemarkOther";
        String BreakSystemRemarkOther = "BreakSystemRemarkOther";
        String SuspensionSystemRemarksOther = "SuspensionSystemRemarksOther";
        String SteeringSystemRemarkOther = "SteeringSystemRemarkOther";
        String ACSystemRemarkOther = "ACSystemRemarkOther";
        String ElectricalSystemRemarkOther = "ElectricalSystemRemarkOther";
        String FitnessCertificate = "FitnessCertificate";
        String RoadTax = "RoadTax";
        String RoadTaxExpiryDate = "RoadTaxExpiryDate";
        String FrontBumper = "FrontBumper";
        String OutsideRearViewMirrorLH = "OutsideRearViewMirrorLH";
        String OutsideRearViewMirrorRH = "OutsideRearViewMirrorRH";
        String PillarType = "PillarType";
        String DickyDoor = "DickyDoor";
        String BodyShell = "BodyShell";
        String Chassis = "Chassis";
        String ApronFrontLH = "ApronFrontLH";
        String ApronFrontRH = "ApronFrontRH";
        String StrutMountingArea = "StrutMountingArea";
        String Firewall = "Firewall";
        String CowlTop = "CowlTop";
        String UpperCrossMember = "UpperCrossMember";
        String LowerCrossMember = "LowerCrossMember";
        String RadiatorSupport = "RadiatorSupport";
        String EngineRoomCarrierAssembly = "EngineRoomCarrierAssembly";
        String HeadLightLH = "HeadLightLH";
        String HeadLightRH = "HeadLightRH";
        String TailLightLH = "TailLightLH";
        String TailLightRH = "TailLightRH";
        String RunningBoardLH = "RunningBoardLH";
        String RunningBoardRH = "RunningBoardRH";
        String PowerWindowType = "PowerWindowType";
        String AllWindowCondition = "AllWindowCondition";
        String FrontLHWindow = "FrontLHWindow";
        String FrontRHWindow = "FrontRHWindow";
        String RearLHWindow = "RearLHWindow";
        String RearRHWindow = "RearRHWindow";
        String PlatformPassengerCabin = "PlatformPassengerCabin";
        String PlatformBootSpace = "PlatformBootSpace";
        String AudioPathVehicleNoise = "AudioPathVehicleNoise";
        String OverallFeedbackRating = "OverallFeedbackRating";
        String EvaluationCount = "EvaluationCount";
        String VehicleUsage = "VehicleUsage";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RequestId + " TEXT, " +
                EvaluatorId + " TEXT, " +
                EvaluationNo + " TEXT, " +
                AgainstTransaction + " TEXT, " +
                Purpose + " TEXT, " +
                OptedNewVehicle + " TEXT, " +
                Date + " TEXT, " +
                Time + " TEXT, " +
                TransactionNo + " TEXT, " +
                EvaluatorName + " TEXT, " +
                SourceOfTransaction + " TEXT, " +
                OwnerName + " TEXT, " +
                Address + " TEXT, " +
                EmailId + " TEXT, " +
                PhoneNo + " TEXT, " +
                MobileNo + " TEXT, " +
                State + " TEXT, " +
                City + " TEXT, " +
                PinCode + " TEXT, " +
                Occupation + " TEXT, " +
                Make + " TEXT, " +
                Model + " TEXT, " +
                Variant + " TEXT, " +
                YearOfMfg + " TEXT, " +
                YearOfReg + " TEXT, " +
                RegNo + " TEXT, " +
                Milege + " TEXT, " +
                MonthOfMfg + " TEXT, " +
                MonthOfReg + " TEXT, " +
                EngineNo + " TEXT, " +
                ChasisNo + " TEXT, " +
                NoOfOwners + " TEXT, " +
                Transmission + " TEXT, " +
                ServiceBooklet + " TEXT, " +
                Color + " TEXT, " +
                FuelType + " TEXT, " +
                EuroVersion + " TEXT, " +
                BodyType + " TEXT, " +
                NoOfSeats + " TEXT, " +
                RCBook + " TEXT, " +
                Insurance + " TEXT, " +
                InsuranceDate + " TEXT, " +
                Ncb + " TEXT, " +
                Puc + " TEXT, " +
                Hypothecation + " TEXT, " +
                FinanceCompany + " TEXT, " +
                NocStatus + " TEXT, " +
                CustomerExpectedPrice + " TEXT, " +
                TestDrive + " TEXT, " +
                IsAccident + " TEXT, " +
                BodyCost + " TEXT, " +
                EngineCost + " TEXT, " +
                FuelAndIngnitionSystemCost + " TEXT, " +
                TransmissionCost + " TEXT, " +
                BreakSystemCost + " TEXT, " +
                SuspensionCost + " TEXT, " +
                SteeringSystemCost + " TEXT, " +
                AcCost + " TEXT, " +
                ElectricalSystemCost + " TEXT, " +
                totalOfAggregateCost + " TEXT, " +
                generalServiceCost + " TEXT, " +
                totalRefurbishmentCost + " TEXT, " +
                Stereo + " TEXT, " +
                NoOfKeys + " TEXT, " +
                ReverseParking + " TEXT, " +
                PowerSteering + " TEXT, " +
                AlloyWheels + " TEXT, " +
                FogLamp + " TEXT, " +
                CentralLocking + " TEXT, " +
                ToolKitJack + " TEXT, " +
                Comment + " TEXT, " +
                nnerTubelessTyres + " TEXT, " +
                RearWiper + " TEXT, " +
                MusicSystem + " TEXT, " +
                FMRadio + " TEXT, " +
                PowerWindow + " TEXT, " +
                AC + " TEXT, " +
                PowerAdjustableSeats + " TEXT, " +
                KeyleassEntry + " TEXT, " +
                PedalShifter + " TEXT, " +
                ElectricallyAdjustableORVM + " TEXT, " +
                RearDefogger + " TEXT, " +
                SunRoof + " TEXT, " +
                AutoClimateTechnologies + " TEXT, " +
                LeatherSeats + " TEXT, " +
                BluetoothAudioSystems + " TEXT, " +
                GPSNavigationSystems + " TEXT, " +
                Odometer + " TEXT, " +
                SeatCover + " TEXT, " +
                Dashboard + " TEXT, " +
                AirBags + " TEXT, " +
                FrontFenderRightSide + " TEXT, " +
                FrontDoorRightSide + " TEXT, " +
                BackDoorRightSide + " TEXT, " +
                BackFenderRightSide + " TEXT, " +
                Bonnet + " TEXT, " +
                TailGate + " TEXT, " +
                BackBumper + " TEXT, " +
                FrontFenderLeftHandSide + " TEXT, " +
                FrontDoorLeftHandSide + " TEXT, " +
                BackDoorLeftHandSide + " TEXT, " +
                BackFenderLeftHandSide + " TEXT, " +
                FrontLight + " TEXT, " +
                BackLight + " TEXT, " +
                WindShield + " TEXT, " +
                Pillars + " TEXT, " +
                Mirrors + " TEXT, " +
                Wheels + " TEXT, " +
                TyreFrontLeft + " TEXT, " +
                TyreFrontRight + " TEXT, " +
                TyreRearLeft + " TEXT, " +
                TyreRearRight + " TEXT, " +
                SpareTyre + " TEXT, " +
                PhotoPathFrontImage + " TEXT, " +
                PhotoPathRearImage + " TEXT, " +
                PhotoPath45DegreeLeftView + " TEXT, " +
                PhotoPath45DegreeRightView + " TEXT, " +
                PhotoPathDashboardView + " TEXT, " +
                PhotoPathOdometerImage + " TEXT, " +
                PhotoPathChasisNoImage + " TEXT, " +
                PhotoPathEngineNoImage + " TEXT, " +
                PhotoPathInteriorView + " TEXT, " +
                PhotoPathRCCopyImage + " TEXT, " +
                PhotoPathInsuranceImage + " TEXT, " +
                PhotoPathRearLeftTyre + " TEXT, " +
                PhotoPathRearRightTyre + " TEXT, " +
                PhotoPathFrontLeftTyre + " TEXT, " +
                PhotoPathFrontRightTyre + " TEXT, " +
                PhotoPathDamagedPartImage1 + " TEXT, " +
                PhotoPathDamagedPartImage2 + " TEXT, " +
                PhotoPathDamagedPartImage3 + " TEXT, " +
                PhotoPathDamagedPartImage4 + " TEXT, " +
                PhotoPathDamagedPartImage5 + " TEXT, " +
                BodyRemark + " TEXT, " +
                EngineRemark + " TEXT, " +
                FuelAndIgnitionSystemRemark + " TEXT, " +
                TransmissionRemark + " TEXT, " +
                BreakSystemRemark + " TEXT, " +
                SuspensionSystemRemarks + " TEXT, " +
                SteeringSystemRemark + " TEXT, " +
                ACSystemRemark + " TEXT, " +
                ElectricalSystemRemark + " TEXT, " +
                GeneralServiceCostRemark + " TEXT, " +
                Status + " TEXT, " +
                SyncFlagPhotoFrontImage + " INTEGER, " +
                SyncFlagPhotoRearImage + " INTEGER, " +
                SyncFlagPhoto45DegreeLeftView + " INTEGER, " +
                SyncFlagPhoto45DegreeRightView + " INTEGER, " +
                SyncFlagPhotoDashboardView + " INTEGER, " +
                SyncFlagPhotoOdometerImage + " INTEGER, " +
                SyncFlagPhotoChasisNoImage + " INTEGER, " +
                SyncFlagPhotoEngineNoImage + " INTEGER, " +
                SyncFlagPhotoInteriorView + " INTEGER, " +
                SyncFlagPhotoRCCopyImage + " INTEGER, " +
                SyncFlagPhotoInsuranceImage + " INTEGER, " +
                SyncFlagPhotoRearLeftTyre + " INTEGER, " +
                SyncFlagPhotoRearRightTyre + " INTEGER, " +
                SyncFlagPhotoFrontLeftTyre + " INTEGER, " +
                SyncFlagPhotoFrontRightTyre + " INTEGER, " +
                SyncFlagPhotoDamagedPartImage1 + " INTEGER, " +
                SyncFlagPhotoDamagedPartImage2 + " INTEGER, " +
                SyncFlagPhotoDamagedPartImage3 + " INTEGER, " +
                SyncFlagPhotoDamagedPartImage4 + " INTEGER, " +
                SyncFlagPhotoDamagedPartImage5 + " INTEGER,  " +
                CNGKit + " TEXT, " + RcRemarks + " TEXT, " + AccidentalType + " TEXT, " +
                AccidentalRemarks + " TEXT, " + Latitude + " TEXT, " + Longitude + " TEXT, " +
                ScrapVehicle + " TEXT, " + PhotoPathRCCopy1 + " TEXT, " +
                PhotoPathRCCopy2 + " TEXT, " + PhotoPathRCCopy3 + " TEXT, " +
                BodyRemarkOther + " TEXT, " +
                EngineRemarkOther + " TEXT, " +
                FuelAndIgnitionSystemRemarkOther + " TEXT, " +
                TransmissionRemarkOther + " TEXT, " +
                BreakSystemRemarkOther + " TEXT, " +
                SuspensionSystemRemarksOther + " TEXT, " +
                SteeringSystemRemarkOther + " TEXT, " +
                ACSystemRemarkOther + " TEXT, " +
                ElectricalSystemRemarkOther + " TEXT, " +
                FitnessCertificate + " TEXT, " +
                RoadTax + " TEXT, " +
                RoadTaxExpiryDate + " TEXT, " +
                FrontBumper + " TEXT, " +
                OutsideRearViewMirrorLH + " TEXT, " +
                OutsideRearViewMirrorRH + " TEXT, " +
                PillarType + " TEXT, " +
                DickyDoor + " TEXT, " +
                BodyShell + " TEXT, " +
                Chassis + " TEXT, " +
                ApronFrontLH + " TEXT, " +
                ApronFrontRH + " TEXT, " +
                StrutMountingArea + " TEXT, " +
                Firewall + " TEXT, " +
                CowlTop + " TEXT, " +
                UpperCrossMember + " TEXT, " +
                LowerCrossMember + " TEXT, " +
                RadiatorSupport + " TEXT, " +
                EngineRoomCarrierAssembly + " TEXT, " +
                HeadLightLH + " TEXT, " +
                HeadLightRH + " TEXT, " +
                TailLightLH + " TEXT, " +
                TailLightRH + " TEXT, " +
                RunningBoardLH + " TEXT, " +
                RunningBoardRH + " TEXT, " +
                PowerWindowType + " TEXT, " +
                AllWindowCondition + " TEXT, " +
                FrontLHWindow + " TEXT, " +
                FrontRHWindow + " TEXT, " +
                RearLHWindow + " TEXT, " +
                RearRHWindow + " TEXT, " +
                PlatformPassengerCabin + " TEXT, " +
                PlatformBootSpace + " TEXT, " +
                OverallFeedbackRating + " TEXT, " +
                EvaluationCount + " TEXT, " +
                VehicleUsage + " TEXT, " +
                AudioPathVehicleNoise + " TEXT " + ");";
        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface newEvaluationMasterChamp {
        String TABLE_NAME = "NewEvaluationMasterChamp";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface ScheduleAppointmentMaster {

        String TABLE_NAME = "ScheduledAppointmentMaster";
        String _ID = "_ID";
        String RequestID = "RequestID";
        String Month = "Month";
        String CustId = "CustId";
        String Date = "Date";
        String Time = "Time";
        String CustomerName = "CustomerName";
        String VehicleName = "VehicleName";
        String Location = "Location";
        String ContactNo = "ContactNo";
        String AppointmentConfirmationStatus = "AppointmentConfirmationStatus";//Appointment Confirm or not 1-Confirm,0-Not Confirm
        String RequestStatus = "RequestStatus";//request status - Follow up or open Follow up => For Evaluation, Open => Upcoming
        String Make = "Make";
        String Model = "Model";
        String Occupation = "Occupation";
        String OptedNewVehicle = "OptedNewVehicle";
        String PhoneNo = "PhoneNo";
        String State = "State";
        String YearOfManufacturing = "YearOfManufacturing";
        String City = "City";
        String Pincode = "Pincode";
        String Email = "Email";
        String Notes = "Notes";
        String Salesconsultantname = "Salesconsultantname";
        String SalesconsultantContactnum = "SalesconsultantContactnum";
        String LeadPriority = "leadPriority";
        String EnquiryLocation = "enquirylocation";
        String EnquirySource = "enquirysource";
        String EnquiryType = "enquirytype";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RequestID + " TEXT, " +
                CustId + " TEXT, " +
                Month + " TEXT, " +
                CustomerName + " TEXT, " +
                VehicleName + " TEXT, " +
                Date + " DATE, " +
                Time + " TEXT, " +
                ContactNo + " TEXT, " +
                AppointmentConfirmationStatus + " TEXT, " +
                RequestStatus + " TEXT, " +
                Make + " TEXT, " +
                Model + " TEXT, " +
                Location + " TEXT, " +
                Occupation + " TEXT, " +
                OptedNewVehicle + " TEXT, " +
                PhoneNo + " TEXT, " +
                State + " TEXT, " +
                YearOfManufacturing + " TEXT, " +
                Pincode + " TEXT, " +
                Email + " TEXT, " +
                Notes + " TEXT, " +
                City + " TEXT, " +
                Salesconsultantname + " TEXT, " +
                SalesconsultantContactnum + " TEXT, " +
                LeadPriority + " INTEGER, " +
                EnquiryLocation + " TEXT, " +
                EnquirySource + " TEXT, " +
                EnquiryType + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface CancelAppointmentMaster {

        String TABLE_NAME = "CancelAppointmentMaster";
        String _ID = "_ID";
        String RequestID = "RequestID";
        String Month = "Month";
        String CustId = "CustId";
        String Date = "Date";
        String Time = "Time";
        String CustomerName = "CustomerName";
        String VehicleName = "VehicleName";
        String Location = "Location";
        String ContactNo = "ContactNo";
        String CancellationStatus = "CancellationStatus";//request status - Follow up or open Follow up => For Evaluation, Open => Upcoming
        String CancellationReason = "CancellationReason";
        String Notes = "Notes";
        String Make = "Make";
        String Model = "Model";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RequestID + " TEXT, " +
                CustId + " TEXT, " +
                Month + " TEXT, " +
                CustomerName + " TEXT, " +
                VehicleName + " TEXT, " +
                Date + " DATE, " +
                Time + " TEXT, " +
                ContactNo + " TEXT, " +
                CancellationStatus + " TEXT, " +
                CancellationReason + " TEXT, " +
                Notes + " TEXT, " +
                Make + " TEXT, " +
                Model + " TEXT, " +
                Location + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface OverdueMaster {
        String TABLE_NAME = "OverdueAppoinmentMaster";
        String _ID = "_ID";
        String CustId = "CustId";
        String Month = "Month";
        String Date = "Date";
        String Time = "Time";
        String CustomerName = "CustomerName";
        String VehicleName = "VehicleName";
        String Location = "Location";
        String ContactNo = "ContactNo";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CustId + " TEXT, " +
                Month + " TEXT, " +
                CustomerName + " TEXT, " +
                VehicleName + " TEXT, " +
                Date + " TEXT, " +
                Time + " TEXT, " +
                ContactNo + " TEXT, " +
                Location + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface UpcomingAppointmentMaster {
        String TABLE_NAME = "upcomingAppointmentMaster";
        String _ID = "_ID";
        String CustId = "CustId";
        String Month = "Month";
        String Date = "Date";
        String Time = "Time";
        String CustomerName = "CustomerName";
        String VehicleName = "VehicleName";
        String Location = "Location";
        String ContactNo = "ContactNo";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CustId + " TEXT, " +
                Month + " TEXT, " +
                CustomerName + " TEXT, " +
                VehicleName + " TEXT, " +
                Date + " TEXT, " +
                Time + " TEXT, " +
                ContactNo + " TEXT, " +
                Location + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface confirmedApppointment {
        String TABLE_NAME = "confirmendAppointmentMaster";
        String _ID = "_ID";
        String requestId = "RequestId";
        String notes = "notes";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                notes + " TEXT, " +
                requestId + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface makeMasterModel {
        String TABLE_NAME = "dmsMakeMaster";
        String _ID = "_ID";
        String Make = "Make";
        String MakeCode = "MakeCode";
        String ModelCode = "ModelCode";
        String ModelDesc = "ModelDesc";
        String sortOrder = "sortOrder";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Make + " TEXT, " +
                MakeCode + " TEXT, " +
                ModelCode + " TEXT, " +
                ModelDesc + " TEXT, " +
                sortOrder + " TEXT " +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface Notifications {
        String TABLE_NAME = "Notifications";
        String _ID = "id";
        String title = "title";
        String message = "message";
        String eventId = "eventId";
        String payload = "payload";
        String dateTime = "dateTime";
        String isViewed = "isViewed";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                title + " TEXT, " +
                message + " TEXT, " +
                eventId + " TEXT, " +
                payload + " TEXT, " +
                dateTime + " TEXT, " +
                isViewed + " INTEGER DEFAULT 0" +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface QuickLeads {
        String TABLE_NAME = "Leads";
        String _ID = "id";
        String leadId = "leadId";
        String leadDate = "leadDate";
        String mobileNumber = "mobileNumber";
        String model = "model";
        String status = "status";

        String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                leadId + " TEXT, " +
                leadDate + " TEXT, " +
                mobileNumber + " TEXT, " +
                model + " TEXT, " +
                status + " TEXT" +
                ");";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface MasterPincodeConst {
        String TABLE_NAME = "MASTER_PINCODE";
        String PIN_CD = "PIN_CD";
        String PIN_DESC = "PIN_DESC";
        String CITY_CD = "CITY_CD";
        String STAT_CD = "STAT_CD";
        String DIST_CD = "DIST_CD";
        String ACTV_IND = "ACTV_IND";
        String TEHSIL_CODENEW = "TEHSIL_CODENEW";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                PIN_CD + " TEXT, " +
                PIN_DESC + " TEXT, " +
                CITY_CD + " REAL, " +
                STAT_CD + " TEXT, " +
                DIST_CD + " TEXT, " +
                ACTV_IND + " TEXT, " +
                TEHSIL_CODENEW + " TEXT" +
                ")";
        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface MasterStateConst {
        String TABLE_NAME = "MASTER_STATE";
        String STAT_CD = "STAT_CD";
        String STAT_DESC = "STAT_DESC";
        String ACTV_IND = "ACTV_IND";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                STAT_CD + " TEXT, " +
                STAT_DESC + " TEXT, " +
                ACTV_IND + " TEXT)";
        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface MasterDistrictConst {
        String TABLE_NAME = "MASTER_DIST";
        String DIST_CD = "DIST_CD";
        String STAT_CD = "STAT_CD";
        String DIST_DESC = "DIST_DESC";
        String ACTV_IND = "ACTV_IND";
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                DIST_CD + " TEXT, " +
                STAT_CD + " TEXT, " +
                DIST_DESC + " TEXT, " +
                ACTV_IND + " TEXT)";
        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    interface MasterTehsilConst {
        String TABLE_NAME = "MASTER_TEHSIL";
        String DIST_CD = "DIST_CD";
        String TEHSIL_CD = "TEHSIL_CD";
        String TEHSIL_DESC = "TEHSIL_DESC";
        String ACTV_IND = "ACTV_IND";

        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "( " +
                DIST_CD + " TEXT, " +
                TEHSIL_CD + " TEXT, " +
                TEHSIL_DESC + " TEXT, " +
                ACTV_IND + " TEXT)";

        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}