package com.mahindra.mitra20.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mahindra.mitra20.sqlite.MasterDBUtils.AssetMasterDataUtils;

import static com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.DATABASE_VERSION;

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    // Database Information
    private static final String DB_NAME = SQLiteDBHelperConstants.DATABASE_NAME;

    // database version
    private static final int DB_VERSION = DATABASE_VERSION;

    SQLiteDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLiteDBHelperConstants.EnquiryRequestMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.EvaluatorMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.newEvaluationMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.ScheduleAppointmentMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.OverdueMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.UpcomingAppointmentMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.ColorMaster.CREATE_TABLE);
        //COMMENTED BY PRAVIN DHARAM ON 13-12-2019
//        db.execSQL(SQLiteDBHelperConstants.StateMaster.CREATE_TABLE);
//        db.execSQL(SQLiteDBHelperConstants.CityMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.VariantMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MakeMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.ModelMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.VehicleSegmentMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.CancelAppointmentMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.AuctionMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MitraMaster.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.confirmedApppointment.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.makeMasterModel.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.Notifications.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.QuickLeads.CREATE_TABLE);
        /**
         * Master added by PRAVIN DHARAM on 10-12-2019
         */
        db.execSQL(SQLiteDBHelperConstants.MasterPincodeConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterStateConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterDistrictConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterTehsilConst.CREATE_TABLE);
        insertAssetsMasterData();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                upgradeV1(db);
            case 2:
                upgradeV2(db);
            case 3:
                upgradeV3(db);
            case 4:
                upgradeV4(db);
            case 5:
                upgradeV5(db);
            case 6:
                upgradeV6(db);
            case 7:
            case 8:
                upgradeV8(db);
            case 9:
                upgradeV9(db);
            case 10:
                upgradeV10(db);
            case 11:
                upgradeV11(db);
            case 12:
                upgradeV12(db);
            case 13:
                upgradeV13(db);
            case 14:
                upgradeV14(db);
            case 15:
                upgradeV15(db);
            case 16:
                upgradeV16(db);
        }
    }

    private void upgradeV16(SQLiteDatabase db) {

        db.execSQL(SQLiteDBHelperConstants.StateMaster.DROP_TABLE);
        db.execSQL(SQLiteDBHelperConstants.CityMaster.DROP_TABLE);

        db.execSQL(SQLiteDBHelperConstants.MasterPincodeConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterStateConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterDistrictConst.CREATE_TABLE);
        db.execSQL(SQLiteDBHelperConstants.MasterTehsilConst.CREATE_TABLE);

        insertAssetsMasterData();
    }

    private void upgradeV15(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.VehicleUsage + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.VehicleUsage + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertAssetsMasterData() {
        new AssetMasterDataUtils().insertMastersDataFromAssets();
    }

    private void upgradeV14(SQLiteDatabase db) {
        try {
            db.execSQL(SQLiteDBHelperConstants.QuickLeads.DROP_TABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryType + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.EnquirySource + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryType + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquirySource + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV13(SQLiteDatabase db) {
        try {
            db.execSQL(SQLiteDBHelperConstants.QuickLeads.CREATE_TABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV12(SQLiteDatabase db) {
        try {
            //eval new evaluations
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.BodyRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.EngineRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.FuelAndIgnitionSystemRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.TransmissionRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.BreakSystemRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.SuspensionSystemRemarksOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.SteeringSystemRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.ACSystemRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.ElectricalSystemRemarkOther + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.FitnessCertificate + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RoadTax + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RoadTaxExpiryDate + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.FrontBumper + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorLH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.OutsideRearViewMirrorRH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PillarType + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.DickyDoor + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.BodyShell + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.Chassis + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontLH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.ApronFrontRH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.StrutMountingArea + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.Firewall + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.CowlTop + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.UpperCrossMember + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.LowerCrossMember + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RadiatorSupport + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.EngineRoomCarrierAssembly + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.HeadLightLH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.HeadLightRH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.TailLightLH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.TailLightRH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardLH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RunningBoardRH + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PowerWindowType + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.AllWindowCondition + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.FrontLHWindow + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.FrontRHWindow + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RearLHWindow + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RearRHWindow + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PlatformPassengerCabin + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PlatformBootSpace + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.OverallFeedbackRating + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.EvaluationCount + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.AudioPathVehicleNoise + " TEXT");

            db.execSQL(SQLiteDBHelperConstants.newEvaluationMasterChamp.DROP_TABLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV11(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.LeadPriority + " INTEGER default 3");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.LeadPriority + " INTEGER default 3");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.EnquiryLocation + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryLocation + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.NoOfOwners + " TEXT default 'NA'");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.OdometerReading + " TEXT default 'NA'");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy1 + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy2 + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.PhotoPathRCCopy3 + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV10(SQLiteDatabase db) {
        try {
            //eval new evaluations
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.CNGKit + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.RcRemarks + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.AccidentalType + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.AccidentalRemarks + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.Latitude + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.Longitude + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.newEvaluationMaster.ScrapVehicle + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV9(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.Salesconsultantname + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.SalesconsultantContactnum + " TEXT");

            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.Salesconsultantname + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.SalesconsultantContactnum + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV8(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactName + " TEXT");
        db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactNumber + " TEXT");
    }

    private void upgradeV6(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME
                + " ADD COLUMN " + SQLiteDBHelperConstants.AuctionMaster.HighestBidder + " TEXT");
    }

    private void upgradeV5(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                + " ADD COLUMN " + SQLiteDBHelperConstants.EnquiryRequestMaster.OptedNewVehicle + " TEXT");
    }

    private void upgradeV4(SQLiteDatabase db) {
        try {

            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.Notifications.TABLE_NAME + " ADD COLUMN " +
                    SQLiteDBHelperConstants.Notifications.isViewed + " INTEGER DEFAULT 0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV3(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME + " ADD COLUMN " +
                    SQLiteDBHelperConstants.CancelAppointmentMaster.Make + " TEXT");
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.CancelAppointmentMaster.TABLE_NAME + " ADD COLUMN " +
                    SQLiteDBHelperConstants.CancelAppointmentMaster.Model + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.execSQL(SQLiteDBHelperConstants.Notifications.CREATE_TABLE);
    }

    private void upgradeV2(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.ScheduleAppointmentMaster.TABLE_NAME
                    + " ADD COLUMN " +
                    SQLiteDBHelperConstants.ScheduleAppointmentMaster.Notes + " TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upgradeV1(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME
                + " ADD COLUMN " +
                SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerExpectedPrice + " TEXT");
        // we want both updates, so no break statement here...
    }


    //CREATED BY PRAVIN DHARAM ON 14-12-2019
    public boolean isExist(String tableName, String[] columnName, String[] columnValue) {
        if (columnName.length != columnValue.length) {
            Log.e("DB", "MasterDatabaseHelper,java -> isExist(" + tableName + ", " + columnName + ", " + columnValue + ") length not same Column name:" + columnName.length + "  and Column value:" + columnValue.length);
            return false;
        }

        SQLiteDatabase db = getReadableDatabase();

        StringBuilder whereClause = new StringBuilder();
        for (int i = 0; i < columnName.length; i++) {

            if (i != 0)
                whereClause.append(" AND ");

            whereClause.append(columnName[i] + "='" + columnValue[i] + "'");

        }
        String sql_query = "SELECT * FROM " + tableName + " WHERE " + whereClause;

        int foundRecordCount = db.rawQuery(sql_query, null).getCount();

        return foundRecordCount > 0;
    }
}