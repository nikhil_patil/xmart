package com.mahindra.mitra20.sqlite.MasterDBUtils;

import android.content.ContentValues;
import android.database.Cursor;

import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.SQLiteDBHelperConstants.newEvaluationMaster;

public class EvaluationValueProvider {
    protected ContentValues getOwnerDetailsContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.OwnerName,
                newEval.getOwnerName());
        values.put(newEvaluationMaster.Address,
                newEval.getAddress());
        values.put(newEvaluationMaster.EmailId,
                newEval.getEmailId());
        values.put(newEvaluationMaster.PhoneNo,
                newEval.getPhoneNo());
        values.put(newEvaluationMaster.MobileNo,
                newEval.getMobileNo());
        values.put(newEvaluationMaster.State,
                newEval.getState());
        values.put(newEvaluationMaster.City,
                newEval.getCity());
        values.put(newEvaluationMaster.PinCode,
                newEval.getPinCode());
        values.put(newEvaluationMaster.Occupation,
                newEval.getOccupation());
        return values;
    }

    protected ContentValues getVehicleEvaluationContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.AgainstTransaction,
                newEval.getAgainstTransaction());
        values.put(newEvaluationMaster.Purpose,
                newEval.getPurpose());
        values.put(newEvaluationMaster.OptedNewVehicle,
                newEval.getOptedNewVehicle());
        values.put(newEvaluationMaster.Date,
                newEval.getDate());
        values.put(newEvaluationMaster.Time,
                newEval.getTime());
        values.put(newEvaluationMaster.TransactionNo,
                newEval.getTransactionNo());
        values.put(newEvaluationMaster.EvaluatorName,
                newEval.getEvaluatorName());
        return values;
    }

    protected ContentValues getVehicleDetailsContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.Make,
                newEval.getMake());
        values.put(newEvaluationMaster.Model,
                newEval.getModel());
        values.put(newEvaluationMaster.Variant,
                newEval.getVariant());
        values.put(newEvaluationMaster.YearOfMfg,
                newEval.getYearOfMfg());
        values.put(newEvaluationMaster.YearOfReg,
                newEval.getYearOfReg());
        values.put(newEvaluationMaster.RegNo,
                newEval.getRegNo());
        values.put(newEvaluationMaster.Milege,
                newEval.getMileage());
        values.put(newEvaluationMaster.MonthOfMfg,
                newEval.getMonthOfMfg());
        values.put(newEvaluationMaster.MonthOfReg,
                newEval.getMonthOfReg());
        values.put(newEvaluationMaster.EngineNo,
                newEval.getEngineNo());
        values.put(newEvaluationMaster.ChasisNo,
                newEval.getChassisNo());
        values.put(newEvaluationMaster.NoOfOwners,
                newEval.getNoOfOwners());
        values.put(newEvaluationMaster.Transmission,
                newEval.getTransmission());
        values.put(newEvaluationMaster.ServiceBooklet,
                newEval.getServiceBooklet());
        values.put(newEvaluationMaster.Color,
                newEval.getColor());
        values.put(newEvaluationMaster.FuelType,
                newEval.getFuelType());
        values.put(newEvaluationMaster.EuroVersion,
                newEval.getEuroVersion());
        values.put(newEvaluationMaster.BodyType,
                newEval.getBodyType());
        values.put(newEvaluationMaster.NoOfSeats,
                newEval.getNoOfSeats());
        values.put(newEvaluationMaster.VehicleUsage,
                newEval.getVehicleUsage());
        values.put(newEvaluationMaster.RCBook,
                newEval.getRCBook());
        values.put(newEvaluationMaster.RcRemarks,
                newEval.getRcRemarks());
        values.put(newEvaluationMaster.Insurance,
                newEval.getInsurance());
        values.put(newEvaluationMaster.InsuranceDate,
                newEval.getInsuranceDate());
        values.put(newEvaluationMaster.Ncb,
                newEval.getNcb());
        values.put(newEvaluationMaster.Puc,
                newEval.getPuc());
        values.put(newEvaluationMaster.Hypothecation,
                newEval.getHypothecation());
        values.put(newEvaluationMaster.FinanceCompany,
                newEval.getFinanceCompany());
        values.put(newEvaluationMaster.NocStatus,
                newEval.getNocStatus());
        values.put(newEvaluationMaster.CustomerExpectedPrice,
                newEval.getCustomerExpectedPrice());
        values.put(newEvaluationMaster.TestDrive,
                newEval.getTestDrive());
        values.put(newEvaluationMaster.IsAccident,
                newEval.getIsAccident());
        values.put(newEvaluationMaster.AccidentalType,
                newEval.getAccidentalType());
        values.put(newEvaluationMaster.AccidentalRemarks,
                newEval.getAccidentalRemarks());
        values.put(newEvaluationMaster.ScrapVehicle,
                newEval.getScrapVehicle());
        values.put(newEvaluationMaster.FitnessCertificate,
                newEval.getFitnessCertificate());
        values.put(newEvaluationMaster.RoadTax,
                newEval.getRoadTax());
        values.put(newEvaluationMaster.RoadTaxExpiryDate,
                newEval.getRoadTaxExpiryDate());
        return values;
    }

    protected ContentValues getAccessoriesContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.Stereo,
                newEval.getStereo());
        values.put(newEvaluationMaster.NoOfKeys,
                newEval.getNoOfKeys());
        values.put(newEvaluationMaster.ReverseParking,
                newEval.getReverseParking());
        values.put(newEvaluationMaster.PowerSteering,
                newEval.getPowerSteering());
        values.put(newEvaluationMaster.AlloyWheels,
                newEval.getAlloyWheels());
        values.put(newEvaluationMaster.FogLamp,
                newEval.getFogLamp());
        values.put(newEvaluationMaster.CentralLocking,
                newEval.getCentralLocking());
        values.put(newEvaluationMaster.ToolKitJack,
                newEval.getToolKitJack());
        values.put(newEvaluationMaster.Comment,
                newEval.getComment());
        values.put(newEvaluationMaster.nnerTubelessTyres,
                newEval.getTubelessTyres());
        values.put(newEvaluationMaster.RearWiper,
                newEval.getRearWiper());
        values.put(newEvaluationMaster.MusicSystem,
                newEval.getMusicSystem());
        values.put(newEvaluationMaster.FMRadio,
                newEval.getFMRadio());
        values.put(newEvaluationMaster.PowerWindow,
                newEval.getPowerWindow());
        values.put(newEvaluationMaster.PowerWindowType,
                newEval.getPowerWindowType());
        values.put(newEvaluationMaster.AC,
                newEval.getAC());
        values.put(newEvaluationMaster.PowerAdjustableSeats,
                newEval.getPowerAdjustableSeats());
        values.put(newEvaluationMaster.KeyleassEntry,
                newEval.getKeyLessEntry());
        values.put(newEvaluationMaster.PedalShifter,
                newEval.getPedalShifter());
        values.put(newEvaluationMaster.ElectricallyAdjustableORVM,
                newEval.getElectricallyAdjustableORVM());
        values.put(newEvaluationMaster.RearDefogger,
                newEval.getRearDefogger());
        values.put(newEvaluationMaster.SunRoof,
                newEval.getSunRoof());
        values.put(newEvaluationMaster.AutoClimateTechnologies,
                newEval.getAutoClimateTechnologies());
        values.put(newEvaluationMaster.LeatherSeats,
                newEval.getLeatherSeats());
        values.put(newEvaluationMaster.BluetoothAudioSystems,
                newEval.getBluetoothAudioSystems());
        values.put(newEvaluationMaster.GPSNavigationSystems,
                newEval.getGPSNavigationSystems());
        return values;
    }

    protected ContentValues getInteriorContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.Odometer,
                newEval.getOdometer());
        values.put(newEvaluationMaster.SeatCover,
                newEval.getSeatCover());
        values.put(newEvaluationMaster.Dashboard,
                newEval.getDashboard());
        values.put(newEvaluationMaster.AirBags,
                newEval.getAirBags());
        values.put(newEvaluationMaster.AllWindowCondition,
                newEval.getAllWindowCondition());
        values.put(newEvaluationMaster.FrontLHWindow,
                newEval.getFrontLHWindow());
        values.put(newEvaluationMaster.FrontRHWindow,
                newEval.getFrontRHWindow());
        values.put(newEvaluationMaster.RearLHWindow,
                newEval.getRearLHWindow());
        values.put(newEvaluationMaster.RearRHWindow,
                newEval.getRearRHWindow());
        values.put(newEvaluationMaster.PlatformPassengerCabin,
                newEval.getPlatformPassengerCabin());
        values.put(newEvaluationMaster.PlatformBootSpace,
                newEval.getPlatformBootSpace());
        return values;
    }

    protected ContentValues getExteriorContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.FrontFenderRightSide,
                newEval.getFrontFenderRightSide());
        values.put(newEvaluationMaster.FrontDoorRightSide,
                newEval.getFrontDoorRightSide());
        values.put(newEvaluationMaster.BackDoorRightSide,
                newEval.getBackDoorRightSide());
        values.put(newEvaluationMaster.BackFenderRightSide,
                newEval.getBackFenderRightSide());
        values.put(newEvaluationMaster.Bonnet,
                newEval.getBonnet());
        values.put(newEvaluationMaster.TailGate,
                newEval.getTailGate());
        values.put(newEvaluationMaster.FrontFenderLeftHandSide,
                newEval.getFrontFenderLeftHandSide());
        values.put(newEvaluationMaster.FrontDoorLeftHandSide,
                newEval.getFrontDoorLeftHandSide());
        values.put(newEvaluationMaster.BackDoorLeftHandSide,
                newEval.getBackDoorLeftHandSide());
        values.put(newEvaluationMaster.BackFenderLeftHandSide,
                newEval.getBackFenderLeftHandSide());
        values.put(newEvaluationMaster.HeadLightLH,
                newEval.getHeadLightLH());
        values.put(newEvaluationMaster.HeadLightRH,
                newEval.getHeadLightRH());
        values.put(newEvaluationMaster.TailLightLH,
                newEval.getTailLightLH());
        values.put(newEvaluationMaster.TailLightRH,
                newEval.getTailLightRH());
        values.put(newEvaluationMaster.WindShield,
                newEval.getWindShield());
        values.put(newEvaluationMaster.DickyDoor,
                newEval.getDickyDoor());
        values.put(newEvaluationMaster.Pillars,
                newEval.getPillars());
        values.put(newEvaluationMaster.PillarType,
                newEval.getPillarType());
        values.put(newEvaluationMaster.Mirrors,
                newEval.getMirrors());
        values.put(newEvaluationMaster.FrontBumper,
                newEval.getFrontBumper());
        values.put(newEvaluationMaster.BackBumper,
                newEval.getBackBumper());
        values.put(newEvaluationMaster.OutsideRearViewMirrorLH,
                newEval.getOutsideRearViewMirrorLH());
        values.put(newEvaluationMaster.OutsideRearViewMirrorRH,
                newEval.getOutsideRearViewMirrorRH());
        values.put(newEvaluationMaster.BodyShell,
                newEval.getBodyShell());
        values.put(newEvaluationMaster.Chassis,
                newEval.getChassis());
        values.put(newEvaluationMaster.ApronFrontLH,
                newEval.getApronFrontLH());
        values.put(newEvaluationMaster.ApronFrontRH,
                newEval.getApronFrontRH());
        values.put(newEvaluationMaster.StrutMountingArea,
                newEval.getStrutMountingArea());
        values.put(newEvaluationMaster.Firewall,
                newEval.getFirewall());
        values.put(newEvaluationMaster.UpperCrossMember,
                newEval.getUpperCrossMember());
        values.put(newEvaluationMaster.LowerCrossMember,
                newEval.getLowerCrossMember());
        values.put(newEvaluationMaster.CowlTop,
                newEval.getCowlTop());
        values.put(newEvaluationMaster.RadiatorSupport,
                newEval.getRadiatorSupport());
        values.put(newEvaluationMaster.EngineRoomCarrierAssembly,
                newEval.getEngineRoomCarrierAssembly());
        values.put(newEvaluationMaster.RunningBoardLH,
                newEval.getRunningBoardLH());
        values.put(newEvaluationMaster.RunningBoardRH,
                newEval.getRunningBoardRH());
        return values;
    }

    protected ContentValues getWheelsAndTyresContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.Wheels,
                newEval.getWheels());
        values.put(newEvaluationMaster.TyreFrontLeft,
                newEval.getTyreFrontLeft());
        values.put(newEvaluationMaster.TyreFrontRight,
                newEval.getTyreFrontRight());
        values.put(newEvaluationMaster.TyreRearLeft,
                newEval.getTyreRearLeft());
        values.put(newEvaluationMaster.TyreRearRight,
                newEval.getTyreRearRight());
        values.put(newEvaluationMaster.SpareTyre,
                newEval.getSpareTyre());
        return values;
    }

    protected ContentValues getAggrigateDetailsContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.BodyCost,
                newEval.getBodyCost());
        values.put(newEvaluationMaster.BodyRemark,
                newEval.getStrBodyRemark());

        values.put(newEvaluationMaster.EngineCost,
                newEval.getEngineCost());
        values.put(newEvaluationMaster.EngineRemark,
                newEval.getStrEngineRemark());

        values.put(newEvaluationMaster.FuelAndIngnitionSystemCost,
                newEval.getFuelAndIgnitionSystemCost());
        values.put(newEvaluationMaster.FuelAndIgnitionSystemRemark,
                newEval.getStrFuelAndIgnitionSystemRemark());

        values.put(newEvaluationMaster.TransmissionCost,
                newEval.getTransmissionCost());
        values.put(newEvaluationMaster.TransmissionRemark,
                newEval.getStrTransmissionRemark());

        values.put(newEvaluationMaster.BreakSystemCost,
                newEval.getBreakSystemCost());
        values.put(newEvaluationMaster.BreakSystemRemark,
                newEval.getStrBreakSystemRemark());

        values.put(newEvaluationMaster.SuspensionCost,
                newEval.getSuspensionCost());
        values.put(newEvaluationMaster.SuspensionSystemRemarks,
                newEval.getStrSuspensionSystemRemarks());

        values.put(newEvaluationMaster.SteeringSystemCost,
                newEval.getSteeringSystemCost());
        values.put(newEvaluationMaster.SteeringSystemRemark,
                newEval.getStrSteeringSystemRemark());

        values.put(newEvaluationMaster.AcCost,
                newEval.getAcCost());
        values.put(newEvaluationMaster.ACSystemRemark,
                newEval.getStrACSystemRemark());

        values.put(newEvaluationMaster.ElectricalSystemCost,
                newEval.getElectricalSystemCost());
        values.put(newEvaluationMaster.ElectricalSystemRemark,
                newEval.getStrElectricalSystemRemark());

        values.put(newEvaluationMaster.totalOfAggregateCost,
                newEval.getTotalOfAggregateCost());

        values.put(newEvaluationMaster.generalServiceCost,
                newEval.getGeneralServiceCost());
        values.put(newEvaluationMaster.GeneralServiceCostRemark,
                newEval.getStrGeneralServiceCostRemark());

        values.put(newEvaluationMaster.totalRefurbishmentCost,
                newEval.getTotalRefurbishmentCost());
        return values;
    }

    protected ContentValues getImagesContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        if (!newEval.getPhotoPathFrontImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathFrontImage,
                    newEval.getPhotoPathFrontImage());
        if (!newEval.getPhotoPathRearImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRearImage,
                    newEval.getPhotoPathRearImage());
        if (!newEval.getPhotoPath45DegreeLeftView().isEmpty())
            values.put(newEvaluationMaster.PhotoPath45DegreeLeftView,
                    newEval.getPhotoPath45DegreeLeftView());
        if (!newEval.getPhotoPath45DegreeRightView().isEmpty())
            values.put(newEvaluationMaster.PhotoPath45DegreeRightView,
                    newEval.getPhotoPath45DegreeRightView());
        if (!newEval.getPhotoPathDashboardView().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDashboardView,
                    newEval.getPhotoPathDashboardView());
        if (!newEval.getPhotoPathOdometerImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathOdometerImage,
                    newEval.getPhotoPathOdometerImage());
        if (!newEval.getPhotoPathChassisNoImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathChasisNoImage,
                    newEval.getPhotoPathChassisNoImage());
        if (!newEval.getPhotoPathRoofTopImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDamagedPartImage5,
                    newEval.getPhotoPathRoofTopImage());
        if (!newEval.getPhotoPathInteriorView().isEmpty())
            values.put(newEvaluationMaster.PhotoPathInteriorView,
                    newEval.getPhotoPathInteriorView());
        if (!newEval.getPhotoPathRCCopyImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRCCopyImage,
                    newEval.getPhotoPathRCCopyImage());
        if (!newEval.getPhotoPathRCCopy1().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRCCopy1,
                    newEval.getPhotoPathRCCopy1());
        if (!newEval.getPhotoPathRCCopy2().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRCCopy2,
                    newEval.getPhotoPathRCCopy2());
        if (!newEval.getPhotoPathRCCopy3().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRCCopy3,
                    newEval.getPhotoPathRCCopy3());
        if (!newEval.getPhotoPathInsuranceImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathInsuranceImage,
                    newEval.getPhotoPathInsuranceImage());
        if (!newEval.getPhotoPathRearLeftTyre().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRearLeftTyre,
                    newEval.getPhotoPathRearLeftTyre());
        if (!newEval.getPhotoPathRearRightTyre().isEmpty())
            values.put(newEvaluationMaster.PhotoPathRearRightTyre,
                    newEval.getPhotoPathRearRightTyre());
        if (!newEval.getPhotoPathFrontLeftTyre().isEmpty())
            values.put(newEvaluationMaster.PhotoPathFrontLeftTyre,
                    newEval.getPhotoPathFrontLeftTyre());
        if (!newEval.getPhotoPathFrontRightTyre().isEmpty())
            values.put(newEvaluationMaster.PhotoPathFrontRightTyre,
                    newEval.getPhotoPathFrontRightTyre());
        if (!newEval.getPhotoPathDamagedPartImage1().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDamagedPartImage1,
                    newEval.getPhotoPathDamagedPartImage1());
        if (!newEval.getPhotoPathDamagedPartImage2().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDamagedPartImage2,
                    newEval.getPhotoPathDamagedPartImage2());
        if (!newEval.getPhotoPathDamagedPartImage3().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDamagedPartImage3,
                    newEval.getPhotoPathDamagedPartImage3());
        if (!newEval.getPhotoPathDamagedPartImage4().isEmpty())
            values.put(newEvaluationMaster.PhotoPathDamagedPartImage4,
                    newEval.getPhotoPathDamagedPartImage4());
        if (!newEval.getPhotoPathEngineNoImage().isEmpty())
            values.put(newEvaluationMaster.PhotoPathEngineNoImage,
                    newEval.getPhotoPathEngineNoImage());
        return values;
    }

    protected ContentValues getAudioAndOverallFeedbackContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.AudioPathVehicleNoise,
                newEval.getAudioPathVehicleNoise());
        values.put(newEvaluationMaster.OverallFeedbackRating,
                newEval.getOverallFeedbackRating());
        return values;
    }

    protected ContentValues getAppointmentContentValue(NewEvaluation newEval) {
        ContentValues values = new ContentValues();
        values.put(newEvaluationMaster.RequestId, newEval.getRequestId());
        values.put(newEvaluationMaster.OwnerName, newEval.getOwnerName());
        values.put(newEvaluationMaster.MobileNo, newEval.getMobileNo());
        values.put(newEvaluationMaster.Address, newEval.getAddress());
        values.put(newEvaluationMaster.Occupation, newEval.getOccupation());
        values.put(newEvaluationMaster.OptedNewVehicle, newEval.getOptedNewVehicle());
        values.put(newEvaluationMaster.PhoneNo, newEval.getPhoneNo());
        values.put(newEvaluationMaster.State, newEval.getState());
        values.put(newEvaluationMaster.City, newEval.getCity());
        values.put(newEvaluationMaster.PinCode, newEval.getPinCode());
        values.put(newEvaluationMaster.EmailId, newEval.getEmailId());
        values.put(newEvaluationMaster.Date, newEval.getDate());
        values.put(newEvaluationMaster.Time, newEval.getTime());
        values.put(newEvaluationMaster.Make, newEval.getMake());
        values.put(newEvaluationMaster.Model, newEval.getModel());
        return values;
    }


    protected NewEvaluation setVehicleEvaluationDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setEvaluatorId(c.getString(c.getColumnIndex(
                newEvaluationMaster.EvaluatorId)));
        newEvaluation.setEvaluationNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.EvaluationNo)));
        newEvaluation.setAgainstTransaction(c.getString(c.getColumnIndex(
                newEvaluationMaster.AgainstTransaction)));
        newEvaluation.setPurpose(c.getString(c.getColumnIndex(
                newEvaluationMaster.Purpose)));
        newEvaluation.setOptedNewVehicle(c.getString(c.getColumnIndex(
                newEvaluationMaster.OptedNewVehicle)));
        newEvaluation.setDate(c.getString(c.getColumnIndex(
                newEvaluationMaster.Date)));
        newEvaluation.setTime(c.getString(c.getColumnIndex(
                newEvaluationMaster.Time)));
        newEvaluation.setTransactionNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.TransactionNo)));
        newEvaluation.setEvaluatorName(c.getString(c.getColumnIndex(
                newEvaluationMaster.EvaluatorName)));
        newEvaluation.setSourceOfTransaction(c.getString(c.getColumnIndex(
                newEvaluationMaster.SourceOfTransaction)));
        return newEvaluation;
    }

    protected NewEvaluation setOwnerDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setOwnerName(c.getString(c.getColumnIndex(
                newEvaluationMaster.OwnerName)));
        newEvaluation.setAddress(c.getString(c.getColumnIndex(
                newEvaluationMaster.Address)));
        newEvaluation.setEmailId(c.getString(c.getColumnIndex(
                newEvaluationMaster.EmailId)));
        newEvaluation.setPhoneNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhoneNo)));
        newEvaluation.setMobileNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.MobileNo)));
        newEvaluation.setState(c.getString(c.getColumnIndex(
                newEvaluationMaster.State)));
        newEvaluation.setCity(c.getString(c.getColumnIndex(
                newEvaluationMaster.City)));
        newEvaluation.setPinCode(c.getString(c.getColumnIndex(
                newEvaluationMaster.PinCode)));
        newEvaluation.setOccupation(c.getString(c.getColumnIndex(
                newEvaluationMaster.Occupation)));
        return newEvaluation;
    }

    protected NewEvaluation setVehicleDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setMake(c.getString(c.getColumnIndex(
                newEvaluationMaster.Make)));
        newEvaluation.setModel(c.getString(c.getColumnIndex(
                newEvaluationMaster.Model)));
        newEvaluation.setVariant(c.getString(c.getColumnIndex(
                newEvaluationMaster.Variant)));
        newEvaluation.setYearOfMfg(c.getString(c.getColumnIndex(
                newEvaluationMaster.YearOfMfg)));
        newEvaluation.setYearOfReg(c.getString(c.getColumnIndex(
                newEvaluationMaster.YearOfReg)));
        newEvaluation.setRegNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.RegNo)));
        newEvaluation.setMileage(c.getString(c.getColumnIndex(
                newEvaluationMaster.Milege)));
        newEvaluation.setMonthOfMfg(c.getString(c.getColumnIndex(
                newEvaluationMaster.MonthOfMfg)));
        newEvaluation.setMonthOfReg(c.getString(c.getColumnIndex(
                newEvaluationMaster.MonthOfReg)));
        newEvaluation.setEngineNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.EngineNo)));
        newEvaluation.setChassisNo(c.getString(c.getColumnIndex(
                newEvaluationMaster.ChasisNo)));
        newEvaluation.setNoOfOwners(c.getString(c.getColumnIndex(
                newEvaluationMaster.NoOfOwners)));
        newEvaluation.setTransmission(c.getString(c.getColumnIndex(
                newEvaluationMaster.Transmission)));
        newEvaluation.setServiceBooklet(c.getString(c.getColumnIndex(
                newEvaluationMaster.ServiceBooklet)));
        newEvaluation.setColor(c.getString(c.getColumnIndex(
                newEvaluationMaster.Color)));
        newEvaluation.setFuelType(c.getString(c.getColumnIndex(
                newEvaluationMaster.FuelType)));
        newEvaluation.setEuroVersion(c.getString(c.getColumnIndex(
                newEvaluationMaster.EuroVersion)));
        newEvaluation.setBodyType(c.getString(c.getColumnIndex(
                newEvaluationMaster.BodyType)));
        newEvaluation.setNoOfSeats(c.getString(c.getColumnIndex(
                newEvaluationMaster.NoOfSeats)));
        newEvaluation.setVehicleUsage(c.getString(c.getColumnIndex(
                newEvaluationMaster.VehicleUsage)));
        newEvaluation.setRCBook(c.getString(c.getColumnIndex(
                newEvaluationMaster.RCBook)));
        newEvaluation.setRcRemarks(c.getString(c.getColumnIndex(
                newEvaluationMaster.RcRemarks)));
        newEvaluation.setInsurance(c.getString(c.getColumnIndex(
                newEvaluationMaster.Insurance)));
        newEvaluation.setInsuranceDate(c.getString(c.getColumnIndex(
                newEvaluationMaster.InsuranceDate)));
        newEvaluation.setNcb(c.getString(c.getColumnIndex(
                newEvaluationMaster.Ncb)));
        newEvaluation.setPuc(c.getString(c.getColumnIndex(
                newEvaluationMaster.Puc)));
        newEvaluation.setHypothecation(c.getString(c.getColumnIndex(
                newEvaluationMaster.Hypothecation)));
        newEvaluation.setFinanceCompany(c.getString(c.getColumnIndex(
                newEvaluationMaster.FinanceCompany)));
        newEvaluation.setNocStatus(c.getString(c.getColumnIndex(
                newEvaluationMaster.NocStatus)));
        newEvaluation.setCustomerExpectedPrice(c.getString(c.getColumnIndex(
                newEvaluationMaster.CustomerExpectedPrice)));
        newEvaluation.setTestDrive(c.getString(c.getColumnIndex(
                newEvaluationMaster.TestDrive)));
        newEvaluation.setIsAccident(c.getString(c.getColumnIndex(
                newEvaluationMaster.IsAccident)));
        newEvaluation.setAccidentalType(c.getString(c.getColumnIndex(
                newEvaluationMaster.AccidentalType)));
        newEvaluation.setAccidentalRemarks(c.getString(c.getColumnIndex(
                newEvaluationMaster.AccidentalRemarks)));
        newEvaluation.setScrapVehicle(c.getString(c.getColumnIndex(
                newEvaluationMaster.ScrapVehicle)));
        newEvaluation.setFitnessCertificate(c.getString(c.getColumnIndex(
                newEvaluationMaster.FitnessCertificate)));
        newEvaluation.setRoadTax(c.getString(c.getColumnIndex(
                newEvaluationMaster.RoadTax)));
        newEvaluation.setRoadTaxExpiryDate(c.getString(c.getColumnIndex(
                newEvaluationMaster.RoadTaxExpiryDate)));
        return newEvaluation;
    }

    protected NewEvaluation setAccessoriesDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setStereo(c.getString(c.getColumnIndex(
                newEvaluationMaster.Stereo)));
        newEvaluation.setNoOfKeys(c.getString(c.getColumnIndex(
                newEvaluationMaster.NoOfKeys)));
        newEvaluation.setReverseParking(c.getString(c.getColumnIndex(
                newEvaluationMaster.ReverseParking)));
        newEvaluation.setPowerSteering(c.getString(c.getColumnIndex(
                newEvaluationMaster.PowerSteering)));
        newEvaluation.setAlloyWheels(c.getString(c.getColumnIndex(
                newEvaluationMaster.AlloyWheels)));
        newEvaluation.setFogLamp(c.getString(c.getColumnIndex(
                newEvaluationMaster.FogLamp)));
        newEvaluation.setCentralLocking(c.getString(c.getColumnIndex(
                newEvaluationMaster.CentralLocking)));
        newEvaluation.setToolKitJack(c.getString(c.getColumnIndex(
                newEvaluationMaster.ToolKitJack)));
        newEvaluation.setComment(c.getString(c.getColumnIndex(
                newEvaluationMaster.Comment)));
        newEvaluation.setTubelessTyres(c.getString(c.getColumnIndex(
                newEvaluationMaster.nnerTubelessTyres)));
        newEvaluation.setRearWiper(c.getString(c.getColumnIndex(
                newEvaluationMaster.RearWiper)));
        newEvaluation.setMusicSystem(c.getString(c.getColumnIndex(
                newEvaluationMaster.MusicSystem)));
        newEvaluation.setFMRadio(c.getString(c.getColumnIndex(
                newEvaluationMaster.FMRadio)));
        newEvaluation.setPowerWindow(c.getString(c.getColumnIndex(
                newEvaluationMaster.PowerWindow)));
        newEvaluation.setPowerWindowType(c.getString(c.getColumnIndex(
                newEvaluationMaster.PowerWindowType)));
        newEvaluation.setAC(c.getString(c.getColumnIndex(
                newEvaluationMaster.AC)));
        newEvaluation.setPowerAdjustableSeats(c.getString(c.getColumnIndex(
                newEvaluationMaster.PowerAdjustableSeats)));
        newEvaluation.setKeyLessEntry(c.getString(c.getColumnIndex(
                newEvaluationMaster.KeyleassEntry)));
        newEvaluation.setPedalShifter(c.getString(c.getColumnIndex(
                newEvaluationMaster.PedalShifter)));
        newEvaluation.setElectricallyAdjustableORVM(c.getString(c.getColumnIndex(
                newEvaluationMaster.ElectricallyAdjustableORVM)));
        newEvaluation.setRearDefogger(c.getString(c.getColumnIndex(
                newEvaluationMaster.RearDefogger)));
        newEvaluation.setSunRoof(c.getString(c.getColumnIndex(
                newEvaluationMaster.SunRoof)));
        newEvaluation.setAutoClimateTechnologies(c.getString(c.getColumnIndex(
                newEvaluationMaster.AutoClimateTechnologies)));
        newEvaluation.setLeatherSeats(c.getString(c.getColumnIndex(
                newEvaluationMaster.LeatherSeats)));
        newEvaluation.setBluetoothAudioSystems(c.getString(c.getColumnIndex(
                newEvaluationMaster.BluetoothAudioSystems)));
        newEvaluation.setGPSNavigationSystems(c.getString(c.getColumnIndex(
                newEvaluationMaster.GPSNavigationSystems)));
        return newEvaluation;
    }

    protected NewEvaluation setInteriorDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setOdometer(c.getString(c.getColumnIndex(
                newEvaluationMaster.Odometer)));
        newEvaluation.setSeatCover(c.getString(c.getColumnIndex(
                newEvaluationMaster.SeatCover)));
        newEvaluation.setDashboard(c.getString(c.getColumnIndex(
                newEvaluationMaster.Dashboard)));
        newEvaluation.setAirBags(c.getString(c.getColumnIndex(
                newEvaluationMaster.AirBags)));
        newEvaluation.setAllWindowCondition(c.getString(c.getColumnIndex(
                newEvaluationMaster.AllWindowCondition)));
        newEvaluation.setFrontLHWindow(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontLHWindow)));
        newEvaluation.setFrontRHWindow(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontRHWindow)));
        newEvaluation.setRearLHWindow(c.getString(c.getColumnIndex(
                newEvaluationMaster.RearLHWindow)));
        newEvaluation.setRearRHWindow(c.getString(c.getColumnIndex(
                newEvaluationMaster.RearRHWindow)));
        newEvaluation.setPlatformPassengerCabin(c.getString(c.getColumnIndex(
                newEvaluationMaster.PlatformPassengerCabin)));
        newEvaluation.setPlatformBootSpace(c.getString(c.getColumnIndex(
                newEvaluationMaster.PlatformBootSpace)));
        return newEvaluation;
    }

    protected NewEvaluation setExteriorDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setFrontFenderRightSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontFenderRightSide)));
        newEvaluation.setFrontDoorRightSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontDoorRightSide)));
        newEvaluation.setBackDoorRightSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.BackDoorRightSide)));
        newEvaluation.setBackFenderRightSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.BackFenderRightSide)));
        newEvaluation.setBonnet(c.getString(c.getColumnIndex(
                newEvaluationMaster.Bonnet)));
        newEvaluation.setTailGate(c.getString(c.getColumnIndex(
                newEvaluationMaster.TailGate)));
        newEvaluation.setFrontFenderLeftHandSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontFenderLeftHandSide)));
        newEvaluation.setFrontDoorLeftHandSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontDoorLeftHandSide)));
        newEvaluation.setBackDoorLeftHandSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.BackDoorLeftHandSide)));
        newEvaluation.setBackFenderLeftHandSide(c.getString(c.getColumnIndex(
                newEvaluationMaster.BackFenderLeftHandSide)));
        newEvaluation.setHeadLightLH(c.getString(c.getColumnIndex(
                newEvaluationMaster.HeadLightLH)));
        newEvaluation.setHeadLightRH(c.getString(c.getColumnIndex(
                newEvaluationMaster.HeadLightRH)));
        newEvaluation.setTailLightLH(c.getString(c.getColumnIndex(
                newEvaluationMaster.TailLightLH)));
        newEvaluation.setTailLightRH(c.getString(c.getColumnIndex(
                newEvaluationMaster.TailLightRH)));
        newEvaluation.setWindShield(c.getString(c.getColumnIndex(
                newEvaluationMaster.WindShield)));
        newEvaluation.setDickyDoor(c.getString(c.getColumnIndex(
                newEvaluationMaster.DickyDoor)));
        newEvaluation.setPillars(c.getString(c.getColumnIndex(
                newEvaluationMaster.Pillars)));
        newEvaluation.setPillarType(c.getString(c.getColumnIndex(
                newEvaluationMaster.PillarType)));
        newEvaluation.setMirrors(c.getString(c.getColumnIndex(
                newEvaluationMaster.Mirrors)));
        newEvaluation.setFrontBumper(c.getString(c.getColumnIndex(
                newEvaluationMaster.FrontBumper)));
        newEvaluation.setBackBumper(c.getString(c.getColumnIndex(
                newEvaluationMaster.BackBumper)));
        newEvaluation.setOutsideRearViewMirrorLH(c.getString(c.getColumnIndex(
                newEvaluationMaster.OutsideRearViewMirrorLH)));
        newEvaluation.setOutsideRearViewMirrorRH(c.getString(c.getColumnIndex(
                newEvaluationMaster.OutsideRearViewMirrorRH)));
        newEvaluation.setBodyShell(c.getString(c.getColumnIndex(
                newEvaluationMaster.BodyShell)));
        newEvaluation.setChassis(c.getString(c.getColumnIndex(
                newEvaluationMaster.Chassis)));
        newEvaluation.setApronFrontLH(c.getString(c.getColumnIndex(
                newEvaluationMaster.ApronFrontLH)));
        newEvaluation.setApronFrontRH(c.getString(c.getColumnIndex(
                newEvaluationMaster.ApronFrontRH)));
        newEvaluation.setStrutMountingArea(c.getString(c.getColumnIndex(
                newEvaluationMaster.StrutMountingArea)));
        newEvaluation.setFirewall(c.getString(c.getColumnIndex(
                newEvaluationMaster.Firewall)));
        newEvaluation.setUpperCrossMember(c.getString(c.getColumnIndex(
                newEvaluationMaster.UpperCrossMember)));
        newEvaluation.setLowerCrossMember(c.getString(c.getColumnIndex(
                newEvaluationMaster.LowerCrossMember)));
        newEvaluation.setCowlTop(c.getString(c.getColumnIndex(
                newEvaluationMaster.CowlTop)));
        newEvaluation.setRadiatorSupport(c.getString(c.getColumnIndex(
                newEvaluationMaster.RadiatorSupport)));
        newEvaluation.setEngineRoomCarrierAssembly(c.getString(c.getColumnIndex(
                newEvaluationMaster.EngineRoomCarrierAssembly)));
        newEvaluation.setRunningBoardLH(c.getString(c.getColumnIndex(
                newEvaluationMaster.RunningBoardLH)));
        newEvaluation.setRunningBoardRH(c.getString(c.getColumnIndex(
                newEvaluationMaster.RunningBoardRH)));
        return newEvaluation;
    }

    protected NewEvaluation setWheelsAndTyresDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setWheels(c.getString(c.getColumnIndex(
                newEvaluationMaster.Wheels)));
        newEvaluation.setTyreFrontLeft(c.getString(c.getColumnIndex(
                newEvaluationMaster.TyreFrontLeft)));
        newEvaluation.setTyreFrontRight(c.getString(c.getColumnIndex(
                newEvaluationMaster.TyreFrontRight)));
        newEvaluation.setTyreRearLeft(c.getString(c.getColumnIndex(
                newEvaluationMaster.TyreRearLeft)));
        newEvaluation.setTyreRearRight(c.getString(c.getColumnIndex(
                newEvaluationMaster.TyreRearRight)));
        newEvaluation.setSpareTyre(c.getString(c.getColumnIndex(
                newEvaluationMaster.SpareTyre)));
        return newEvaluation;
    }

    protected NewEvaluation setAggregatesDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setBodyCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.BodyCost)));
        newEvaluation.setEngineCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.EngineCost)));
        newEvaluation.setFuelAndIgnitionSystemCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.FuelAndIngnitionSystemCost)));
        newEvaluation.setTransmissionCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.TransmissionCost)));
        newEvaluation.setBreakSystemCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.BreakSystemCost)));
        newEvaluation.setSuspensionCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.SuspensionCost)));
        newEvaluation.setSteeringSystemCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.SteeringSystemCost)));
        newEvaluation.setAcCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.AcCost)));
        newEvaluation.setElectricalSystemCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.ElectricalSystemCost)));
        newEvaluation.setTotalOfAggregateCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.totalOfAggregateCost)));
        newEvaluation.setGeneralServiceCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.generalServiceCost)));
        newEvaluation.setTotalRefurbishmentCost(c.getString(c.getColumnIndex(
                newEvaluationMaster.totalRefurbishmentCost)));
        newEvaluation.setStrBodyRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.BodyRemark)));
        newEvaluation.setStrEngineRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.EngineRemark)));
        newEvaluation.setStrFuelAndIgnitionSystemRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.FuelAndIgnitionSystemRemark)));
        newEvaluation.setStrTransmissionRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.TransmissionRemark)));
        newEvaluation.setStrBreakSystemRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.BreakSystemRemark)));
        newEvaluation.setStrSuspensionSystemRemarks(c.getString(c.getColumnIndex(
                newEvaluationMaster.SuspensionSystemRemarks)));
        newEvaluation.setStrSteeringSystemRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.SteeringSystemRemark)));
        newEvaluation.setStrACSystemRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.ACSystemRemark)));
        newEvaluation.setStrElectricalSystemRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.ElectricalSystemRemark)));
        newEvaluation.setStrGeneralServiceCostRemark(c.getString(c.getColumnIndex(
                newEvaluationMaster.GeneralServiceCostRemark)));
        return newEvaluation;
    }

    protected NewEvaluation setImagesDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setPhotoPathFrontImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathFrontImage)));
        newEvaluation.setPhotoPathRearImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRearImage)));
        newEvaluation.setPhotoPath45DegreeLeftView(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPath45DegreeLeftView)));
        newEvaluation.setPhotoPath45DegreeRightView(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPath45DegreeRightView)));
        newEvaluation.setPhotoPathDashboardView(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDashboardView)));
        newEvaluation.setPhotoPathOdometerImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathOdometerImage)));
        newEvaluation.setPhotoPathChassisNoImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathChasisNoImage)));
        newEvaluation.setPhotoPathRoofTopImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDamagedPartImage5)));
        newEvaluation.setPhotoPathInteriorView(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathInteriorView)));
        newEvaluation.setPhotoPathRCCopyImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRCCopyImage)));
        newEvaluation.setPhotoPathRCCopy1(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRCCopy1)));
        newEvaluation.setPhotoPathRCCopy2(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRCCopy2)));
        newEvaluation.setPhotoPathRCCopy3(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRCCopy3)));
        newEvaluation.setPhotoPathInsuranceImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathInsuranceImage)));
        newEvaluation.setPhotoPathRearLeftTyre(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRearLeftTyre)));
        newEvaluation.setPhotoPathRearRightTyre(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathRearRightTyre)));
        newEvaluation.setPhotoPathFrontLeftTyre(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathFrontLeftTyre)));
        newEvaluation.setPhotoPathFrontRightTyre(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathFrontRightTyre)));
        newEvaluation.setPhotoPathEngineNoImage(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathEngineNoImage)));
        newEvaluation.setPhotoPathDamagedPartImage1(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDamagedPartImage1)));
        newEvaluation.setPhotoPathDamagedPartImage2(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDamagedPartImage2)));
        newEvaluation.setPhotoPathDamagedPartImage3(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDamagedPartImage3)));
        newEvaluation.setPhotoPathDamagedPartImage4(c.getString(c.getColumnIndex(
                newEvaluationMaster.PhotoPathDamagedPartImage4)));
        return newEvaluation;
    }

    protected NewEvaluation setAudioAndFeedbackDetails(Cursor c) {
        NewEvaluation newEvaluation = new NewEvaluation();
        newEvaluation.setId(c.getString(c.getColumnIndex(
                newEvaluationMaster._ID)));
        newEvaluation.setRequestId(c.getString(c.getColumnIndex(
                newEvaluationMaster.RequestId)));
        newEvaluation.setOverallFeedbackRating(c.getString(c.getColumnIndex(
                newEvaluationMaster.OverallFeedbackRating)));
        newEvaluation.setAudioPathVehicleNoise(c.getString(c.getColumnIndex(
                newEvaluationMaster.AudioPathVehicleNoise)));
        return newEvaluation;
    }
}