package com.mahindra.mitra20.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mahindra.mitra20.module_broker.models.Notification;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;
import com.mahindra.mitra20.utils.LogUtil;

import java.util.ArrayList;

/**
 * Created by PATINIK-CONT on 25-Sep-18.
 */

public class BrokerDbHelper {
    private final SQLiteDatabaseHelper sqLiteDatabaseHelper;

    public BrokerDbHelper(Context context) {
        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
    }

    //Insert Notification Data
    public void insertNotification(Notification notification) {
        try (SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.Notifications.title, notification.getTitle());
            contentValues.put(SQLiteDBHelperConstants.Notifications.message, notification.getMessage());
            contentValues.put(SQLiteDBHelperConstants.Notifications.eventId, notification.getEventId());
            contentValues.put(SQLiteDBHelperConstants.Notifications.payload, notification.getPayload());
            contentValues.put(SQLiteDBHelperConstants.Notifications.dateTime, notification.getDateTime());
            contentValues.put(SQLiteDBHelperConstants.Notifications.isViewed, notification.isViewed() ? 1 : 0);
            db.insert(SQLiteDBHelperConstants.Notifications.TABLE_NAME, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Notification db", e.getMessage());
        }
    }

    public ArrayList<Notification> getNotifications() {
        ArrayList<Notification> notifications = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.Notifications.TABLE_NAME +
                    " ORDER BY " + SQLiteDBHelperConstants.Notifications._ID + " DESC", null);
            if (cursor.moveToFirst()) {
                do {
                    Notification notification = new Notification();
                    notification.set_ID(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications._ID)));
                    notification.setTitle(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.title)));
                    notification.setMessage(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.message)));
                    notification.setEventId(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.eventId)));
                    notification.setPayload(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.payload)));
                    notification.setDateTime(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.dateTime)));
                    notification.setViewed(cursor.getInt(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.Notifications.isViewed)) == 1);
                    notifications.add(notification);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notifications;
    }

    public boolean checkUnreadNotifications() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.Notifications.TABLE_NAME +
                    " WHERE " + SQLiteDBHelperConstants.Notifications.isViewed + " = 0", null);
            if (cursor.moveToFirst()) {
                cursor.close();
                return true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setAllNotificationsRead() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            db.execSQL("Update " + SQLiteDBHelperConstants.Notifications.TABLE_NAME
                    + " set " + SQLiteDBHelperConstants.Notifications.isViewed + " = 1 " +
                    " WHERE " + SQLiteDBHelperConstants.Notifications.isViewed + " = 0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void truncateNotificationsTable() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.execSQL("delete from " + SQLiteDBHelperConstants.Notifications.TABLE_NAME);
            LogUtil.getInstance().logE("logout", "Delete table: " + SQLiteDBHelperConstants.Notifications.TABLE_NAME);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Insert Notification Data
    public void insertLead(LeadDetail leadDetail) {
        try (SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.QuickLeads.leadId, leadDetail.getLeadId());
            contentValues.put(SQLiteDBHelperConstants.QuickLeads.leadDate, leadDetail.getLeadDate());
            contentValues.put(SQLiteDBHelperConstants.QuickLeads.mobileNumber, leadDetail.getMobileNo());
            contentValues.put(SQLiteDBHelperConstants.QuickLeads.model, leadDetail.getModel());
            contentValues.put(SQLiteDBHelperConstants.QuickLeads.status, leadDetail.getLeadStatus());
            db.insert(SQLiteDBHelperConstants.QuickLeads.TABLE_NAME, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Notification db", e.getMessage());
        }
    }

    public ArrayList<LeadDetail> getLeadsByStatus(String status) {
        ArrayList<LeadDetail> leadDetails = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.QuickLeads.TABLE_NAME +
                    " WHERE " + SQLiteDBHelperConstants.QuickLeads.status + " = '" + status + "'", null);
            if (cursor.moveToFirst()) {
                do {
                    LeadDetail leadDetail = new LeadDetail();
                    leadDetail.setLeadId(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.leadId)));
                    leadDetail.setLeadDate(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.leadDate)));
                    leadDetail.setMobileNo(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.mobileNumber)));
                    leadDetail.setModel(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.model)));
                    leadDetail.setLeadStatus(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.status)));
                    leadDetails.add(leadDetail);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leadDetails;
    }

    public ArrayList<LeadDetail> getAllLeads() {
        ArrayList<LeadDetail> leadDetails = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from " + SQLiteDBHelperConstants.QuickLeads.TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    LeadDetail leadDetail = new LeadDetail();
                    leadDetail.setLeadId(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.leadId)));
                    leadDetail.setLeadDate(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.leadDate)));
                    leadDetail.setMobileNo(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.mobileNumber)));
                    leadDetail.setModel(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.model)));
                    leadDetail.setLeadStatus(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.QuickLeads.status)));
                    leadDetails.add(leadDetail);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leadDetails;
    }

    public void deleteAllLeads() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            db.execSQL("delete from " + SQLiteDBHelperConstants.QuickLeads.TABLE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}