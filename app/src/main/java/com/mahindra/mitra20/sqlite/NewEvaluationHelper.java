package com.mahindra.mitra20.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.MasterDBUtils.EvaluationValueProvider;

import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.ACCESSORIES;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.AGGREGATE_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.APPT_DATA;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.AUDIO_AND_OVERALL_FEEDBACK;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.EXTERIOR;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.IMAGES;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.INTERIOR;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.OWNER_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.VEHICLE_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.VEHICLE_EVALUATION_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.WHEELS_AND_TYRES;

public class NewEvaluationHelper extends EvaluationValueProvider {
    private final SQLiteDatabaseHelper sqLiteDatabaseHelper;

    public NewEvaluationHelper(Context context) {
        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
    }

    public long saveAndUpdateNewEvaluationDraft(NewEvaluation newEvaluation, int evalPart) {
        int id = -1;
        String existingId = checkEvaluationExistAndReturnIdOrNull(newEvaluation.getId());
        switch (evalPart) {
            case VEHICLE_EVALUATION_DETAILS:
                saveAndUpdateEvaluationDraft(getVehicleEvaluationContentValue(newEvaluation), existingId);
                break;
            case OWNER_DETAILS:
                saveAndUpdateEvaluationDraft(getOwnerDetailsContentValue(newEvaluation), existingId);
                break;
            case VEHICLE_DETAILS:
                saveAndUpdateEvaluationDraft(getVehicleDetailsContentValue(newEvaluation), existingId);
                break;
            case ACCESSORIES:
                saveAndUpdateEvaluationDraft(getAccessoriesContentValue(newEvaluation), existingId);
                break;
            case INTERIOR:
                saveAndUpdateEvaluationDraft(getInteriorContentValue(newEvaluation), existingId);
                break;
            case EXTERIOR:
                saveAndUpdateEvaluationDraft(getExteriorContentValue(newEvaluation), existingId);
                break;
            case WHEELS_AND_TYRES:
                saveAndUpdateEvaluationDraft(getWheelsAndTyresContentValue(newEvaluation), existingId);
                break;
            case AGGREGATE_DETAILS:
                saveAndUpdateEvaluationDraft(getAggrigateDetailsContentValue(newEvaluation), existingId);
                break;
            case IMAGES:
                saveAndUpdateEvaluationDraft(getImagesContentValue(newEvaluation), existingId);
                break;
            case AUDIO_AND_OVERALL_FEEDBACK:
                saveAndUpdateEvaluationDraft(getAudioAndOverallFeedbackContentValue(newEvaluation), existingId);
                break;
            case APPT_DATA:
                saveAndUpdateEvaluationDraft(getAppointmentContentValue(newEvaluation), existingId);
                break;
        }
        return id;
    }

    private long saveAndUpdateEvaluationDraft(ContentValues contentValues, String id) {
        long rowId = -1;
        contentValues.put(SQLiteDBHelperConstants.newEvaluationMaster.Status,
                EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT);
        SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
        try {
            if (id != null && !id.trim().equals("")) {
                db.update(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.newEvaluationMaster._ID + " = ?",
                        new String[]{id});
                rowId = Long.parseLong(id);
            } else {
                rowId = db.insert(SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME,
                        null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return rowId;
    }

    public NewEvaluation getEvaluationDetails(String evaluationID, int evalPart) {
        NewEvaluation evaluation = new NewEvaluation();
        SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
        try {
            Cursor c = db.rawQuery("select * from " +
                    SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + " WHERE " +
                    SQLiteDBHelperConstants.newEvaluationMaster._ID + " = '" + evaluationID + "'", null);
            if (c.getCount() > 0) {
                if (c.moveToFirst()) {
                    do {
                        switch (evalPart) {
                            case VEHICLE_EVALUATION_DETAILS:
                                evaluation = setVehicleEvaluationDetails(c);
                                break;
                            case OWNER_DETAILS:
                                evaluation = setOwnerDetails(c);
                                break;
                            case VEHICLE_DETAILS:
                                evaluation = setVehicleDetails(c);
                                break;
                            case ACCESSORIES:
                                evaluation = setAccessoriesDetails(c);
                                break;
                            case INTERIOR:
                                evaluation = setInteriorDetails(c);
                                break;
                            case EXTERIOR:
                                evaluation = setExteriorDetails(c);
                                break;
                            case WHEELS_AND_TYRES:
                                evaluation = setWheelsAndTyresDetails(c);
                                break;
                            case AGGREGATE_DETAILS:
                                evaluation = setAggregatesDetails(c);
                                break;
                            case IMAGES:
                                evaluation = setImagesDetails(c);
                                break;
                            case AUDIO_AND_OVERALL_FEEDBACK:
                                evaluation = setAudioAndFeedbackDetails(c);
                                break;
                        }

                    }
                    while (c.moveToNext());
                }
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return evaluation;
    }

    private boolean isEvaluationExist(String evaluationID) {
        boolean rowExists = false;
        SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
        try {
            Cursor cursor;
            if (null != evaluationID && !evaluationID.equals("")) {
                cursor = db.rawQuery("select * from " +
                        SQLiteDBHelperConstants.newEvaluationMaster.TABLE_NAME + " WHERE " +
                        SQLiteDBHelperConstants.newEvaluationMaster._ID + " = '" + evaluationID + "'", null);
                rowExists = (cursor.getCount() > 0);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return rowExists;
    }

    private String checkEvaluationExistAndReturnIdOrNull(String id) {
        return isEvaluationExist(id) ? id : null;
    }
}