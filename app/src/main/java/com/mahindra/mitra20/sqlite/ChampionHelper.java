package com.mahindra.mitra20.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.models.AuctionDetail;
import com.mahindra.mitra20.champion.models.AuctionLIST;
import com.mahindra.mitra20.champion.models.EnquiryRequest;
import com.mahindra.mitra20.champion.models.EvaluatorMaster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 8/3/2018.
 */

public class ChampionHelper {

    private final SQLiteDatabaseHelper sqLiteDatabaseHelper;
    private SQLiteDatabase db = null;

    public ChampionHelper(Context context) {
        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
    }

    public ArrayList<EnquiryRequest> getEnquiryRequestDetails(String status) {
        return getEnquiryRequestDetailsList("select * from " +
                SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + " = '" + status + "'");
    }

    public ArrayList<EnquiryRequest> getEnquiryRequestDetails(int limit, String status) {
        return getEnquiryRequestDetailsList("select * from " +
                SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME+ " where " +
                SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + " = '" +
                status + "'" + " LIMIT " + limit);
    }

    public void deleteEvaluatorMaster() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.EvaluatorMaster.TABLE_NAME, null, null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteEnquiryRequestDetails(String requestStatus) {
        try {

            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME,
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + " = '"
                            + requestStatus + "'", null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAuctionDetails(String requestStatus) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME,
                    SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS + " = '"
                            + requestStatus + "'", null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<EnquiryRequest> getEnquiryRequestDetailsList(String query) {
        ArrayList<EnquiryRequest> enquiryRequestArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery( query, null );

            if (cursor.moveToFirst()) {
                do {
                    EnquiryRequest enquiryRequest = new EnquiryRequest();
                    enquiryRequest.setRequestID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID)));
                    enquiryRequest.setNameOfEnquiry(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.NameOfEnquiry)));
                    enquiryRequest.setRequestStatus(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus)));
                    enquiryRequest.setMake(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.Make)));
                    enquiryRequest.setModel(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.Model)));
                    enquiryRequest.setDateOfAppointment(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.DateOfAppointment)));
                    enquiryRequest.setPinCode(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.PinCode)));
                    enquiryRequest.setContactNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.ContactNo)));
                    enquiryRequest.setLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.Location)));
                    enquiryRequest.setAuctionID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.AuctionID)));
                    enquiryRequest.setCustomerExpectedPrice(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerExpectedPrice)));
                    enquiryRequest.setOptedNewVehicle(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.OptedNewVehicle)));
                    enquiryRequest.setSalesconsultantname(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.Salesconsultantname)));
                    enquiryRequest.setSalesconsultantContactnum(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.SalesconsultantContactnum)));
                    enquiryRequest.setLeadPriority(cursor.getInt(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.LeadPriority)));
                    enquiryRequest.setEnquiryLocation(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryLocation)));
                    enquiryRequest.setEnquirySource(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.EnquirySource)));
                    enquiryRequest.setEnquiryType(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryType)));

                    enquiryRequest.setEvaluationReportID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID)));
                    enquiryRequest.setEvaluatorID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluatorID)));
                    enquiryRequest.setTimeOfAppointment(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.TimeOfAppointment)));
                    enquiryRequest.setYearOfManifacturing(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.YearOfManifacturing)));
                    enquiryRequest.setYearOfManifacturing(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerResponse)));
                    enquiryRequest.setAppointmentStatus(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EnquiryRequestMaster.AppointmentStatus)));

                    enquiryRequest.setSelected(false);
                    enquiryRequestArrayList.add(enquiryRequest);
                }
                while(cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return enquiryRequestArrayList;
    }

    private boolean isEnquiryExist(String requestID, String status) {
        try {
            int count = 0;
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery( "select count(*) from " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID + " = '" + requestID
                    + "' AND " + SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus
                    + " = '" + status + "'", null );

            if (cursor.moveToFirst()) {
                do {
                    count = cursor.getInt(0);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
            return count != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<EvaluatorMaster> getEvaluatorMasters() {
        ArrayList<EvaluatorMaster> evaluatorMasterArrayList = new ArrayList<>();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery( "SELECT * FROM " +
                    SQLiteDBHelperConstants.EvaluatorMaster.TABLE_NAME, null );

            if (cursor.moveToFirst()) {
                do {
                    EvaluatorMaster evaluatorMaster = new EvaluatorMaster();
                    evaluatorMaster.setEvaluatorName(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_Name)));
                    evaluatorMaster.setEvaluatorID(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_ID)));
                    evaluatorMaster.setEvaluatorContactNo(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_Contact_Number)));
                    evaluatorMaster.setAllocatedReqCount(cursor.getString(cursor.getColumnIndex(
                            SQLiteDBHelperConstants.EvaluatorMaster.ALLOCATED_REQ_COUNT)));

                    evaluatorMasterArrayList.add(evaluatorMaster);
                }
                while(cursor.moveToNext());
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return evaluatorMasterArrayList;
    }

    public void putEnquiryRequestDetails(ArrayList<EnquiryRequest> listEnquiryRequests) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < listEnquiryRequests.size(); i++) {
                contentValues.clear();
                EnquiryRequest enquiryRequest = listEnquiryRequests.get(i);

                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID,
                        enquiryRequest.getRequestID());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.NameOfEnquiry,
                        enquiryRequest.getNameOfEnquiry());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus,
                        enquiryRequest.getRequestStatus());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.Make,
                        enquiryRequest.getMake());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.Model,
                        enquiryRequest.getModel());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.DateOfAppointment,
                        enquiryRequest.getDateOfAppointment());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.PinCode,
                        enquiryRequest.getPinCode());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.ContactNo,
                        enquiryRequest.getContactNo());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.Location,
                        enquiryRequest.getLocation());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.AuctionID,
                        enquiryRequest.getAuctionID());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerExpectedPrice,
                        enquiryRequest.getCustomerExpectedPrice());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.OptedNewVehicle,
                        enquiryRequest.getOptedNewVehicle());

                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID,
                        enquiryRequest.getEvaluationReportID());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluatorID,
                        enquiryRequest.getEvaluatorID());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.TimeOfAppointment,
                        enquiryRequest.getTimeOfAppointment());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.YearOfManifacturing,
                        enquiryRequest.getYearOfManifacturing());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerResponse,
                        enquiryRequest.getYearOfManifacturing());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.AppointmentStatus,
                        enquiryRequest.getAppointmentStatus());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.Salesconsultantname,
                        enquiryRequest.getSalesconsultantname());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.SalesconsultantContactnum,
                        enquiryRequest.getSalesconsultantContactnum());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.LeadPriority,
                        enquiryRequest.getLeadPriority());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryLocation,
                        enquiryRequest.getEnquiryLocation());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.EnquirySource,
                        enquiryRequest.getEnquirySource());
                contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.EnquiryType,
                        enquiryRequest.getEnquiryType());

                db.insert(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, null, contentValues);
            }

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEnquiryStatus(String status, String requestID, String evaluationReportID) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus, status);

            if (!requestID.isEmpty())
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID + " = '"
                            + requestID + "'", null);
            else
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID + " = '"
                                + evaluationReportID + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEnquiryCustomerResponse(String customerResponse, String requestID,
                                              String evaluationReportID) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerResponse, customerResponse);

            if (!requestID.isEmpty())
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID + " = '"
                            + requestID + "'", null);
            else
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID + " = '"
                                + evaluationReportID + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCustomerExpectedPrice(String customerResponse, String requestID,
                                            String evaluationReportID) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteDBHelperConstants.EnquiryRequestMaster.CustomerExpectedPrice,
                    customerResponse);

            if (!requestID.isEmpty())
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID + " = '"
                                + evaluationReportID + "'", null);
            else
                db.update(SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME, contentValues,
                        SQLiteDBHelperConstants.EnquiryRequestMaster.RequestID + " = '"
                                + requestID + "'", null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AuctionDetail getAuctionMastersByID(String auctionID) {
        AuctionDetail auctionDetail = new AuctionDetail();
        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery( "SELECT * FROM " + SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID + " = '" + auctionID + "'", null );

            if (cursor.moveToFirst()) {
                do {
                    auctionDetail.setAuctionDate(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.AUCTION_DATE)));
                    auctionDetail.setAuctionID(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.AUCTION_ID)));
                    auctionDetail.setEvaluationReportID(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.EVALUATION_REPORT_ID)));
                    auctionDetail.setAuctionStatus(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS)));
                    auctionDetail.setCountOfBidders(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.BIDDER_COUNT)));
                    auctionDetail.setMITRAPriceBasePrice(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.MITRA_PRICE)));
                    auctionDetail.setRequestID(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID)));
                    auctionDetail.setStartTimestamp(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.START_TIMESTAMP)));
                    auctionDetail.setRemainingTimeInSeconds(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.REMAINING_TIME)));
                    auctionDetail.setVehOwnerName(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.VEH_OWNER)));
                    auctionDetail.setVehicleName(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.VEH_NAME)));
                    auctionDetail.setContactNo(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.CONTACT_NO)));
                    auctionDetail.setHighestBidder(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.HighestBidder)));
                    auctionDetail.setNegotiatorContactNumber(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactNumber)));
                    auctionDetail.setNegotiatorContactName(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactName)));
                    auctionDetail.setVehicleUsage(cursor.getString(
                            cursor.getColumnIndex(SQLiteDBHelperConstants.AuctionMaster.VehicleUsage)));

                    Cursor cursorIn = db.rawQuery( "SELECT * FROM " +
                            SQLiteDBHelperConstants.MitraMaster.TABLE_NAME + " where "
                            + SQLiteDBHelperConstants.MitraMaster.AUCTION_ID + " = '"
                            + auctionDetail.getRequestID() + "'", null );
                    List<AuctionLIST> listList = new ArrayList<>();
                    if (cursorIn.moveToFirst()) {
                        do {
                            AuctionLIST auctionLIST = new AuctionLIST();
                            auctionLIST.setMITRAId(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.MITRA_ID)));
                            auctionLIST.setMITRAName(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.MITRA_NAME)));
                            auctionLIST.setBidPrice(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.BID_PRICE)));
                            auctionLIST.setBidingDateTime(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.BID_DATE)));
                            auctionLIST.setCommissionPrice(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.COMMISSION_PRICE)));
                            auctionLIST.setDeductedPrice(cursor.getString(cursor.getColumnIndex(
                                    SQLiteDBHelperConstants.MitraMaster.DEDUCTED_PRICE)));

                            listList.add(auctionLIST);
                        }
                        while (cursorIn.moveToNext());
                    }

                    auctionDetail.setLIST(listList);
                    cursorIn.close();
                }
                while(cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return auctionDetail;
    }

    public void putAuctionDetails(ArrayList<AuctionDetail> listEnquiryRequests) {
        try {
            deleteBidDetailsByAuctionID();
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();

            for (int i = 0; i < listEnquiryRequests.size(); i++) {
                contentValues.clear();
                AuctionDetail auctionDetail = listEnquiryRequests.get(i);

                List<AuctionLIST> listList = auctionDetail.getLIST();

                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.AUCTION_DATE,
                        auctionDetail.getAuctionDate());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.AUCTION_ID,
                        auctionDetail.getAuctionID());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.EVALUATION_REPORT_ID,
                        auctionDetail.getEvaluationReportID());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS,
                        auctionDetail.getAuctionStatus());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.BIDDER_COUNT,
                        auctionDetail.getCountOfBidders());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.MITRA_PRICE,
                        auctionDetail.getMITRAPriceBasePrice());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID,
                        auctionDetail.getRequestID());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.START_TIMESTAMP,
                        auctionDetail.getStartTimestamp());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.REMAINING_TIME,
                        auctionDetail.getRemainingTimeInSeconds());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.VEH_OWNER,
                        auctionDetail.getVehOwnerName());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.VEH_NAME,
                        auctionDetail.getVehicleName());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.CONTACT_NO,
                        auctionDetail.getContactNo());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.HighestPrice,
                        auctionDetail.getHighestPrice());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.HighestBidder,
                        auctionDetail.getHighestBidder());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactName,
                        auctionDetail.getNegotiatorContactName());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.NegotiatorContactNumber,
                        auctionDetail.getNegotiatorContactNumber());
                contentValues.put(SQLiteDBHelperConstants.AuctionMaster.VehicleUsage,
                        auctionDetail.getVehicleUsage());
                db.insert(SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME,
                        null, contentValues);

                for (int j = 0; j < listList.size(); j++) {
                    ContentValues contentValuesMITRA = new ContentValues();
                    AuctionLIST auctionLIST = listList.get(j);

                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.AUCTION_ID,
                            auctionDetail.getAuctionID());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.MITRA_ID,
                            auctionLIST.getMITRAId().toUpperCase());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.MITRA_NAME,
                            auctionLIST.getMITRAName());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.BID_PRICE,
                            auctionLIST.getBidPrice());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.BID_DATE,
                            auctionLIST.getBidingDateTime());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.COMMISSION_PRICE,
                            auctionLIST.getCommissionPrice());
                    contentValuesMITRA.put(SQLiteDBHelperConstants.MitraMaster.DEDUCTED_PRICE,
                            auctionLIST.getDeductedPrice());

                    db.insert(SQLiteDBHelperConstants.MitraMaster.TABLE_NAME, null, contentValuesMITRA);
                }
            }

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteBidDetailsByAuctionID() {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            db.delete(SQLiteDBHelperConstants.MitraMaster.TABLE_NAME, null, null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void putEvaluatorMaster(ArrayList<EvaluatorMaster> evaluatorMasterArrayList) {
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < evaluatorMasterArrayList.size(); i++) {
                contentValues.clear();
                EvaluatorMaster evaluatorMaster = evaluatorMasterArrayList.get(i);
                contentValues.put(SQLiteDBHelperConstants.EvaluatorMaster.ALLOCATED_REQ_COUNT,
                        evaluatorMaster.getAllocatedReqCount());
                contentValues.put(SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_Contact_Number,
                        evaluatorMaster.getEvaluatorContactNo());
                contentValues.put(SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_ID,
                        evaluatorMaster.getEvaluatorID());
                contentValues.put(SQLiteDBHelperConstants.EvaluatorMaster.Evaluator_Name,
                        evaluatorMaster.getEvaluatorName());

                db.insert(SQLiteDBHelperConstants.EvaluatorMaster.TABLE_NAME, null, contentValues);
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, Integer> getDashboardCount(ArrayList<String> evaluationReportIDsOFFR) {
        HashMap<String, Integer> hashMapValues = new HashMap<>();
        hashMapValues.put("AssignOpen", 0);
        hashMapValues.put("XMartPrice", 0);
        hashMapValues.put("OfferCustomer", 0);
        hashMapValues.put("ProcurementDetails", 0);
        hashMapValues.put("CancellationDetails", 0);
        hashMapValues.put("LIVE", 0);
        hashMapValues.put("COMPLETED", 0);
        hashMapValues.put("BIDDING", 0);

        try {
            db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery( "select count("+ SQLiteDBHelperConstants.EnquiryRequestMaster._ID +") from " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus
                    + " = '" + ChampionConstants.OPEN_STATUS + "'" , null );

            if (cursor.moveToFirst()) {
                do {
                    hashMapValues.put("AssignOpen", cursor.getInt(0));
                } while(cursor.moveToNext());
            }
            cursor.close();

            cursor = db.rawQuery( "select count("+ SQLiteDBHelperConstants.EnquiryRequestMaster._ID +") from " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus
                    + " = '" + ChampionConstants.EVALUATION_DONE + "'" , null );

            if (cursor.moveToFirst()) {

                do {
                    hashMapValues.put("XMartPrice", cursor.getInt(0));
                } while(cursor.moveToNext());
            }
            cursor.close();

            cursor = db.rawQuery( "select count("+ SQLiteDBHelperConstants.EnquiryRequestMaster._ID +") from " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus
                    + " = '" + ChampionConstants.OFFER_PRICE + "'" , null );

            if (cursor.moveToFirst()) {
                do {
                    hashMapValues.put("OfferCustomer", cursor.getInt(0));
                } while(cursor.moveToNext());
            }
            cursor.close();
            cursor = db.rawQuery( "select count("+ SQLiteDBHelperConstants.EnquiryRequestMaster._ID +") from " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus
                    + " = '" + ChampionConstants.CANCELLATION_DONE + "'" , null );

            if (cursor.moveToFirst()) {
                do {
                    hashMapValues.put("CancellationDetails", cursor.getInt(0));
                } while(cursor.moveToNext());
            }
            cursor.close();

            cursor = db.rawQuery( "select distinct("+ SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID +") from " +
                    SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS
                    + " = '" + ChampionConstants.LIVE_STATUS + "'" , null );

            hashMapValues.put("LIVE", cursor.getCount());
            cursor.close();

            cursor = db.rawQuery( "select distinct("+ SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID +") from " +
                    SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS
                    + " = '" + ChampionConstants.COMPLETED_STATUS + "'" , null );

            hashMapValues.put("COMPLETED", cursor.getCount());

            cursor.close();

            String EvaluationReportIDs = "";
            for (int i = 0; i < evaluationReportIDsOFFR.size(); i++) {
                if (i == 0) {
                    EvaluationReportIDs += "'" + getEvaluationReportIDsOFFR().get(i) + "'";
                } else {

                    EvaluationReportIDs += ",'" + getEvaluationReportIDsOFFR().get(i) + "'";
                }
            }

            db = sqLiteDatabaseHelper.getReadableDatabase();
            cursor = db.rawQuery( "select distinct("+ SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID +") from " +
                    SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS
                    + " = '" + ChampionConstants.COMPLETED_STATUS + "' AND " +
                    SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID + " NOT IN ( " + EvaluationReportIDs + ")" , null );

            hashMapValues.put("ProcurementDetails", cursor.getCount());

            cursor.close();

            cursor = db.rawQuery( "select distinct("+ SQLiteDBHelperConstants.AuctionMaster.REQUEST_ID +") from " +
                    SQLiteDBHelperConstants.AuctionMaster.TABLE_NAME + " where " +
                    SQLiteDBHelperConstants.AuctionMaster.AUCTION_STATUS
                    + " = '" + ChampionConstants.BIDDING_STATUS + "'" , null );

            hashMapValues.put("BIDDING", cursor.getCount());
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hashMapValues;
    }

    public ArrayList<String> getEvaluationReportIDsOFFR() {
        ArrayList<String> arrayListEvaluationReportID = new ArrayList<>();
        try {
            SQLiteDatabase db = sqLiteDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select " +
                    SQLiteDBHelperConstants.EnquiryRequestMaster.EvaluationReportID + " from "
                    + SQLiteDBHelperConstants.EnquiryRequestMaster.TABLE_NAME + " WHERE "
                    + SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + " = '"
                    + ChampionConstants.OFFER_PRICE + "' OR "
                    + SQLiteDBHelperConstants.EnquiryRequestMaster.RequestStatus + " = '"
                    + ChampionConstants.PROCURE_PRICE + "'", null);
            if (cursor.moveToFirst()) {
                do {
                     arrayListEvaluationReportID.add(cursor.getString(0));
                } while(cursor.moveToNext());
            }
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayListEvaluationReportID;
    }
}