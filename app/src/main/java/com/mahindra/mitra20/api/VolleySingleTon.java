package com.mahindra.mitra20.api;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mahindra.mitra20.champion.constants.WebConstants;
import com.mahindra.mitra20.utils.LogUtil;
import com.mahindra.mitra20.utils.NetworkUtilities;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleySingleTon {
    private static VolleySingleTon instance;
    private RequestQueue volleyRequestQue;
    private static final String TAG = "VolleySingleTon";

    public interface VolleyInteractor {
        void gotSuccessResponse(int requestId, String response);

        void gotErrorResponse(int requestId, String response);
    }

    public interface VolleyInteractorAuctions {
        void gotSuccessResponseForAuctions(int requestId, String response, int auctionId);

        void gotErrorResponse(int requestId, String response);
    }

    public static VolleySingleTon getInstance() {
        if (instance == null) {
            instance = new VolleySingleTon();
        }
        return instance;
    }

    private VolleySingleTon() {

    }

    public synchronized void connectToPostUrl(Context context, final VolleyInteractor interactor,
                                              String url, final HashMap<String, String> params,
                                              final int requestCode) {
        try {
            boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
            if (networkConnected) {
                volleyRequestQue = Volley.newRequestQueue(context);

                LogUtil.getInstance().logE(TAG, "URL: " + url + "\nDATA: " + new JSONObject(params));

                JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                                try {
                                    if (!response.toString().contains("MasterUpdateDate")) {
                                        if (response.has(WebConstants.IsSuccessful) && response.getString(WebConstants.IsSuccessful)
                                                .equalsIgnoreCase("1")
                                                || response.has(WebConstants.IsSuccessfull) && response.getString(WebConstants.IsSuccessfull) //ADD "IsSuccessfull" FOR SALES GENIE API--
                                                .equalsIgnoreCase("1")) {
                                            interactor.gotSuccessResponse(requestCode, response.toString());
                                        } else {
                                            interactor.gotErrorResponse(requestCode, response.toString());
                                        }
                                    } else {
                                        interactor.gotSuccessResponse(requestCode, response.toString());
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                        interactor.gotErrorResponse(requestCode, error.toString());
                    }
                });

                setDefaultVolleyTimeOut(request);
                volleyRequestQue.add(request);
            }
        } catch (Exception e) {
            e.printStackTrace();
            String response = "{\"IsSuccessful\":\"0\",\"message\":\"Request failed. Please try again!\",\"messagecode\":\"20\"}";
            interactor.gotSuccessResponse(requestCode, response);
        }

    }

    private void setDefaultVolleyTimeOut(JsonObjectRequest request) {
        request.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public synchronized void connectToPostUrl(Context context, final VolleyInteractor view, String url, final JSONObject params, final int requestCode) {
        try {
            boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
            if (networkConnected) {
                volleyRequestQue = Volley.newRequestQueue(context);
                LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
                JsonObjectRequest request = new JsonObjectRequest(url, params,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                                try {
                                    if (!response.toString().contains("MasterUpdateDate")) {
                                        if (response.getString(WebConstants.IsSuccessful).equalsIgnoreCase("1")) {
                                            view.gotSuccessResponse(requestCode, response.toString());
                                        } else {
                                            view.gotErrorResponse(requestCode, response.toString());
                                        }
                                    } else {
                                        view.gotSuccessResponse(requestCode, response.toString());
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                        view.gotErrorResponse(requestCode, error.toString());
                    }
                });

                setDefaultVolleyTimeOut(request);
                volleyRequestQue.add(request);
            }
        } catch (Exception e) {
            e.printStackTrace();
            String response = "{\"IsSuccessful\":\"0\",\"message\":\"Request failed. Please try again!\",\"messagecode\":\"20\"}";
            view.gotSuccessResponse(requestCode, response);
        }
    }

    public synchronized void connectToPostUrl(Context context, final VolleyInteractor view,
                                              String url, final JSONObject params,
                                              final int requestCode, final HashMap<String, String> headers) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest request = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            try {
                                if (!response.toString().contains("MasterUpdateDate")) {
                                    if (response.getString(WebConstants.IsSuccessful)
                                            .equalsIgnoreCase("1")) {
                                        view.gotSuccessResponse(requestCode, response.toString());
                                    } else {
                                        view.gotErrorResponse(requestCode, response.toString());
                                    }
                                } else {
                                    view.gotSuccessResponse(requestCode, response.toString());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.toString());
                }
            }) {
                /** Passing some request headers* */
                @Override
                public Map getHeaders() {
                    return headers;
                }
            };
            setDefaultVolleyTimeOut(request);
            volleyRequestQue.add(request);
        }
    }

    public synchronized void connectToPostUrl2(Context context, final VolleyInteractor view,
                                               String url, final JSONObject params, final int requestCode) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest request = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            try {
                                String mitraCode = response.optString("MitraCode");
                                if (!mitraCode.equalsIgnoreCase("")) {
                                    view.gotSuccessResponse(requestCode, response.toString());
                                } else {
                                    view.gotErrorResponse(requestCode, response.toString());
                                }

                            } catch (Exception e) {
                                view.gotErrorResponse(requestCode, response.toString());
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.toString());
                }
            });
            //setDefaultVolleyTimeOut(request);
            volleyRequestQue.add(request);
        }
    }

    public synchronized void connectToPostUrl3(Context context, final VolleyInteractor view,
                                               String url, final JSONObject params,
                                               final int requestCode) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest request = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            try {
                                if (response.getString(WebConstants.IsSuccessful)
                                        .equalsIgnoreCase("1")) {
                                    view.gotSuccessResponse(requestCode, response.toString());
                                } else {
                                    view.gotErrorResponse(requestCode, response.toString());
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.toString());
                }
            });

            setDefaultVolleyTimeOut(request);

            volleyRequestQue.add(request);
        }
    }

    public synchronized void connectToPostUrl3ForAuctions(Context context,
                                                          final VolleyInteractorAuctions view,
                                                          String url, final JSONObject params,
                                                          final int requestCode,
                                                          final int currentAuctionPage) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest request = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            try {
                                if (response.getString(WebConstants.IsSuccessful)
                                        .equalsIgnoreCase("1")) {
                                    view.gotSuccessResponseForAuctions(requestCode,
                                            response.toString(), currentAuctionPage);
                                } else {
                                    view.gotSuccessResponseForAuctions(requestCode,
                                            response.toString(), currentAuctionPage);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.toString());
                }
            });
            setDefaultVolleyTimeOut(request);
            volleyRequestQue.add(request);
        }
    }

    public synchronized void connectToPostUrlForSmallData(Context context,
                                                          final VolleyInteractor view,
                                                          String url, final JSONObject params,
                                                          final int requestCode) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest request = new JsonObjectRequest(url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            view.gotSuccessResponse(requestCode, response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.toString());
                }
            });

            setDefaultVolleyTimeOut(request);
            volleyRequestQue.add(request);
        }
    }

    public synchronized void connectToPostUrl3WithHeaders(Context context,
                                                          final VolleyInteractor view, String url,
                                                          final JSONObject params, final int requestCode,
                                                          final HashMap<String, String> headers) {
        boolean networkConnected = NetworkUtilities.isNetworkConnected(context);
        if (networkConnected) {
            volleyRequestQue = Volley.newRequestQueue(context);
            LogUtil.getInstance().logE(TAG, "connectToPostUrl: URL: " + url + "\nDATA: " + params);
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtil.getInstance().logE(TAG, "onResponse: " + response);
                            view.gotSuccessResponse(requestCode, response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LogUtil.getInstance().logE(TAG, "onErrorResponse: " + error.getMessage());
                    view.gotErrorResponse(requestCode, error.getMessage());

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            volleyRequestQue.add(jsonRequest);
        }
    }
}