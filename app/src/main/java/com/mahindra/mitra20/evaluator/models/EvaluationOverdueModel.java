package com.mahindra.mitra20.evaluator.models;

/**
 * Created by BADHAP-CONT on 7/30/2018.
 */

public class EvaluationOverdueModel {

    String strCustId;
    String strMonth;
    String strDate;
    String strTime;
    String strCustomerName;
    String strVehicleName;
    String strLocation;
    String strContactNo;

    public EvaluationOverdueModel() {
    }

    public EvaluationOverdueModel(String strMonth, String strDate, String strTime, String strCustomerName, String strVehicleName, String strLocation) {
        this.strMonth = strMonth;
        this.strDate = strDate;
        this.strTime = strTime;
        this.strCustomerName = strCustomerName;
        this.strVehicleName = strVehicleName;
        this.strLocation = strLocation;
    }

    public String getStrCustId() {
        return strCustId;
    }

    public void setStrCustId(String strCustId) {
        this.strCustId = strCustId;
    }

    public String getStrContactNo() {
        return strContactNo;
    }

    public void setStrContactNo(String strContactNo) {
        this.strContactNo = strContactNo;
    }

    public String getStrMonth() {

        return strMonth;
    }

    public void setStrMonth(String strMonth) {
        this.strMonth = strMonth;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    public String getStrCustomerName() {
        return strCustomerName;
    }

    public void setStrCustomerName(String strCustomerName) {
        this.strCustomerName = strCustomerName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public void setStrVehicleName(String strVehicleName) {
        this.strVehicleName = strVehicleName;
    }

    public String getStrLocation() {
        return strLocation;
    }

    public void setStrLocation(String strLocation) {
        this.strLocation = strLocation;
    }
}
