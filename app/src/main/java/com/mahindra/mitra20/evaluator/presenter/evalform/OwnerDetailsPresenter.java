package com.mahindra.mitra20.evaluator.presenter.evalform;

import android.content.Context;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

import java.util.List;

/**
 * Created by PATINIK-CONT on 30/12/2019.
 */

public class OwnerDetailsPresenter {

    private Context context;
    private MasterDatabaseHelper masterDatabaseHelper;

    public OwnerDetailsPresenter(Context context) {
        this.context = context;
        this.masterDatabaseHelper = new MasterDatabaseHelper(context);
    }

    public List<MasterModel> getStateList() {
        return masterDatabaseHelper.getStateList();
    }

    public String getDistrictID(String pinCode) {
        return masterDatabaseHelper.getDistrictID(pinCode);
    }

    public List<MasterModel> getPinCodeList(String tehsilID) {
        return masterDatabaseHelper.getPincodeList(tehsilID);
    }

    public List<MasterModel> getTehsilList(String districtID) {
        return masterDatabaseHelper.getTehsilList(districtID);
    }

    public List<MasterModel> getDistrictList(String stateID) {
        return masterDatabaseHelper.getDistrictList(stateID);
    }

    public void saveData(NewEvaluation newEvaluation) {
        try {
            new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                    EvaluatorConstants.EvaluationParts.OWNER_DETAILS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NewEvaluation getEvaluationData(String evaluationId) {
        return new NewEvaluationHelper(context).getEvaluationDetails(evaluationId,
                EvaluatorConstants.EvaluationParts.OWNER_DETAILS);
    }
}