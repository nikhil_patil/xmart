package com.mahindra.mitra20.evaluator.interfaces;

/**
 * Created by user on 8/6/2018.
 */

public interface OnClickDialog {



    public void onSuccess(String reason,String requestId,String evaluatorId);

    public void onCancel();

    public void onNeutral();

}
