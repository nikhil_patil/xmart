package com.mahindra.mitra20.evaluator.helper;

import android.content.Context;
import android.util.DisplayMetrics;

public class ScreenDensityHelper {

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
