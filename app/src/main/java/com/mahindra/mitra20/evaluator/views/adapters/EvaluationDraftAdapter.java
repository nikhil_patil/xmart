package com.mahindra.mitra20.evaluator.views.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.DialogsHelper;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.activity.CompletedEvaluationActivity;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class EvaluationDraftAdapter extends RecyclerView.Adapter<EvaluationDraftAdapter.EvaluationDraftViewHolder> {
    private ArrayList<NewEvaluation> evaluationArrayList;
    private Context context;
    private String type, strTime;
    private DateHelper dateHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();
    private DraftListener draftListener;

    public EvaluationDraftAdapter(Context _context, ArrayList<NewEvaluation> _NewEvaluationContents, String _type) {
        evaluationArrayList = _NewEvaluationContents;
        context = _context;
        type = _type;
        dateHelper = new DateHelper();
        arrayListMake.addAll(new MasterDatabaseHelper(context).getDmsMasterModel());
    }

    @Override
    public void onBindViewHolder(EvaluationDraftViewHolder holder, int position) {
        holder.bind(evaluationArrayList.get(position));
    }

    @Override
    public EvaluationDraftViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EvaluationDraftViewHolder(LayoutInflater.from(context).inflate(R.layout.row_draft, parent, false));
    }

    public void removeAndNotifyAdapter(NewEvaluation newEvaluation) {
        int index = evaluationArrayList.indexOf(newEvaluation);
        evaluationArrayList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return evaluationArrayList.size();
    }

    public void setDraftListener(DraftListener draftListener) {
        this.draftListener = draftListener;
    }

    public interface DraftListener {
        void deleteDraft(NewEvaluation newEvaluation);
    }

    public class EvaluationDraftViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewBackground, textViewMonth, textViewDate, textViewTime,
                textViewCustomerName, textViewVehicleName, textViewLocation, tv_owner_mobile, tv_make_model;
        LinearLayout linearLayoutOverdueVisits;
        ImageView imageViewCall, imageViewMessage, iv_delete_draft;

        public EvaluationDraftViewHolder(View v) {
            super(v);
            init(v);
            setListener();
        }

        private void init(View v) {
            textViewMonth = v.findViewById(R.id.textViewMonth);
            textViewDate = v.findViewById(R.id.textViewDate);
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewBackground = v.findViewById(R.id.textViewBackground);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewMessage = v.findViewById(R.id.imageViewMessage);
            iv_delete_draft = v.findViewById(R.id.iv_delete_draft);
            iv_delete_draft.setVisibility(View.VISIBLE);
            linearLayoutOverdueVisits = v.findViewById(R.id.linearLayoutOverdueVisits);
            tv_owner_mobile = v.findViewById(R.id.tv_owner_mobile);
        }

        private void setListener() {
            linearLayoutOverdueVisits.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);
            imageViewMessage.setOnClickListener(this);
            iv_delete_draft.setOnClickListener(this);
        }


        public void bind(NewEvaluation evaluation) {
            String strDate = evaluation.getDate();
            try {
                Date appointmentDate = dateHelper.convertStringToDate(strDate);
                String strMonth = dateHelper.getMonthName(appointmentDate);
                String strAppointmentDate = dateHelper.getDate(appointmentDate);

                textViewMonth.setText(strMonth);
                textViewDate.setText(strAppointmentDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (evaluation.getTime() != null && !evaluation.getTime().isEmpty()) {
                strTime = dateHelper.convertTwentyFourHourToTweleveHour(
                        evaluation.getTime());
            }

            textViewTime.setText(strTime);
            textViewCustomerName.setText(TextUtils.isEmpty(evaluation.getOwnerName()) ? "Name Undefined" : evaluation.getOwnerName());
            tv_owner_mobile.setText(TextUtils.isEmpty(evaluation.getMobileNo()) ? "Mobile Undefined" : evaluation.getMobileNo());
            Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                    arrayListMake, evaluation.getMake(),
                    evaluation.getModel());
            String strMake = makeModel.first;
            String strModel = makeModel.second;
            textViewVehicleName.setText((TextUtils.isEmpty(evaluation.getMake()) ? "Make Undefined" : evaluation.getMake()) + " - " + (TextUtils.isEmpty(evaluation.getModel()) ? "Model Undefined" : evaluation.getModel()));
            textViewLocation.setText(TextUtils.isEmpty(evaluation.getAddress()) ? "Location Undefined" : evaluation.getAddress());
            if (type.equals("Overdue") || type.equals("Draft")) {
                textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorOrange));
            } else if (type.equals("Upcoming")) {
                textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutOverdueVisits:
                    callOverdueVisits();
                    break;
                case R.id.imageViewCall:
                    new DialogsHelper((Activity) context)
                            .callDialog(evaluationArrayList.get(getLayoutPosition()).getMobileNo());
                    break;
                case R.id.imageViewMessage:
                    CommonHelper.sendMessage(evaluationArrayList.get(getLayoutPosition()).getMobileNo(), context);
                    break;
                case R.id.iv_delete_draft:
                    deleteDraft();
                    break;
            }
        }

        private void callOverdueVisits() {
            if (!type.equalsIgnoreCase("Completed")) {
                NewEvaluation scheduleAppointment = evaluationArrayList.get(getLayoutPosition());
                Intent intent = new Intent(context, NewEvaluationActivity.class);
                intent.putExtra("NewEvaluationId", scheduleAppointment.getId());
                intent.putExtra("purpose", scheduleAppointment.getPurpose());
                intent.putExtra("status", type);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                ((Activity) context).finish();
            } else {
                NewEvaluation scheduleAppointment = evaluationArrayList.get(getLayoutPosition());
                Intent intent = new Intent(context, CompletedEvaluationActivity.class);
                intent.putExtra("NewEvaluationId", scheduleAppointment.getId());
                intent.putExtra("status", type);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        }

        private void deleteDraft() {
            if (draftListener != null) {
                new AlertDialog.Builder(context).setIcon(R.mipmap.ic_launcher).setTitle("Delete draft?")
                        .setMessage("Are you sure you want to delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                draftListener.deleteDraft(evaluationArrayList.get(getLayoutPosition()));
                            }
                        }).setNegativeButton("No", null).show();
            }
        }
    }
}