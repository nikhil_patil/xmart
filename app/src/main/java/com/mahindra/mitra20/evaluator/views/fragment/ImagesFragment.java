package com.mahindra.mitra20.evaluator.views.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.BuildConfig;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.VehiclePhoto;
import com.mahindra.mitra20.databinding.LayoutEvalImagesBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.BitmapHelper;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.ImagesPresenter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.CHASSIS_NO_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.DAMAGED_PART_IMAGE_1;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.DAMAGED_PART_IMAGE_2;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.DAMAGED_PART_IMAGE_3;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.DAMAGED_PART_IMAGE_4;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.DASHBOARD_VIEW;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.ENGINE_NO_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.FOURTY_DEGREE_LEFT_VIEW;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.FOURTY_DEGREE_RIGHT_VIEW;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.FRONT_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.FRONT_LEFT_TYRE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.FRONT_RIGHT_TYRE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.INSURANCE_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.INTERIOR_VIEW;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.ODOMETER_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.RCCOPY_1;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.RCCOPY_2;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.RCCOPY_3;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.RCCOPY_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.REAR_IMAGE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.REAR_LEFT_TYRE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.REAR_RIGHT_TYRE;
import static com.mahindra.mitra20.champion.models.VehiclePhoto.ImageConstants.ROOF_TOP_IMAGE;

public class ImagesFragment extends Fragment implements CommonMethods {

    public static final String KEY_EDIT_EVAL = "EditEval";
    private final int CAMERA_REQUEST_CODE = 100;
    private static final int PERMISSION_REQUEST_CODE = 200;
    LayoutEvalImagesBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private ImagesPresenter imagesPresenter;
    private List<VehiclePhoto> vehiclePhotos;
    String type = "";
    String evaluationId;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutEvalImagesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        imagesPresenter = new ImagesPresenter(context);
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = imagesPresenter.getEvaluationData(evaluationId);
            type = getArguments().getString("type");
        }
        init();
        displayData();
    }

    @Override
    public void init() {

    }

    @Override
    public void displayData() {
        vehiclePhotos = new ArrayList<>();
        vehiclePhotos.addAll(imagesPresenter.prepareVehiclePhotosArray(newEvaluation));
        for (int i = 0; i < vehiclePhotos.size(); i += 3) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayout.setPadding(0, 10, 0, 0);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = i; j < i + 3; j++) {
                LayoutInflater inflater = LayoutInflater.from(context);
                View itemView = inflater.inflate(R.layout.list_item_vehicle_photos_view,
                        linearLayout, false);

                TextView tvVehiclePhotoName = itemView.findViewById(R.id.tv_vehicle_photo_name);
                final ProgressBar progressBar = itemView.findViewById(R.id.progress_bar);
                final ImageView ivVehiclePhoto = itemView.findViewById(R.id.iv_vehicle_photo);

                ivVehiclePhoto.setId(4567 + j);
                tvVehiclePhotoName.setText(vehiclePhotos.get(j).getNameOfImage());
                if (!vehiclePhotos.get(j).getPath().equals("")) {
                    itemView.setTag(vehiclePhotos.get(j).getPath());
                    if (vehiclePhotos.get(j).getPath().contains("http")) {
                        Glide.with(this)
                                .load(CommonHelper.getAuthenticatedUrlForGlide(
                                        vehiclePhotos.get(j).getPath()))
                                .apply(RequestOptions.placeholderOf(R.mipmap.ic_image_not_found)
                                        .error(R.mipmap.ic_image_not_found))
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e,
                                                                Object model,
                                                                Target<Drawable> target,
                                                                boolean isFirstResource) {
                                        progressBar.setVisibility(View.GONE);
                                        ivVehiclePhoto.setImageDrawable(getResources()
                                                .getDrawable(R.mipmap.ic_image_not_found));
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource,
                                                                   Object model,
                                                                   Target<Drawable> target,
                                                                   DataSource dataSource,
                                                                   boolean isFirstResource) {
                                        progressBar.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(ivVehiclePhoto);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Glide.with(this)
                                .load(Uri.fromFile(new File(vehiclePhotos.get(j).getPath())))
                                .apply(new RequestOptions()
                                        .placeholder(R.mipmap.ic_image_not_found))
                                .into(ivVehiclePhoto);
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                if (!vehiclePhotos.get(j).isDisplayImage()) {
                    itemView.setVisibility(View.INVISIBLE);
                }
                itemView.setTag(j);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkPermission()) {
                                takePicture((int) view.getTag());
                            } else {
                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE,
                                                CAMERA}, PERMISSION_REQUEST_CODE);
                            }
                        } else {
                            takePicture((int) view.getTag());
                        }
                    }
                });
                linearLayout.addView(itemView);
            }
            binding.linearLayoutVehiclePhotos.addView(linearLayout);
        }
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setPhotoPathFrontImage(vehiclePhotos.get(FRONT_IMAGE).getPath());
            newEvaluation.setPhotoPathRearImage(vehiclePhotos.get(REAR_IMAGE).getPath());
            newEvaluation.setPhotoPath45DegreeLeftView(vehiclePhotos.get(FOURTY_DEGREE_LEFT_VIEW).getPath());
            newEvaluation.setPhotoPath45DegreeRightView(vehiclePhotos.get(FOURTY_DEGREE_RIGHT_VIEW).getPath());
            newEvaluation.setPhotoPathDashboardView(vehiclePhotos.get(DASHBOARD_VIEW).getPath());
            newEvaluation.setPhotoPathOdometerImage(vehiclePhotos.get(ODOMETER_IMAGE).getPath());
            newEvaluation.setPhotoPathChassisNoImage(vehiclePhotos.get(CHASSIS_NO_IMAGE).getPath());
            newEvaluation.setPhotoPathRoofTopImage(vehiclePhotos.get(ROOF_TOP_IMAGE).getPath());//roofTop
            newEvaluation.setPhotoPathInteriorView(vehiclePhotos.get(INTERIOR_VIEW).getPath());
            newEvaluation.setPhotoPathRCCopyImage(vehiclePhotos.get(RCCOPY_IMAGE).getPath());
            newEvaluation.setPhotoPathRCCopy1(vehiclePhotos.get(RCCOPY_1).getPath());
            newEvaluation.setPhotoPathRCCopy2(vehiclePhotos.get(RCCOPY_2).getPath());
            newEvaluation.setPhotoPathRCCopy3(vehiclePhotos.get(RCCOPY_3).getPath());
            newEvaluation.setPhotoPathInsuranceImage(vehiclePhotos.get(INSURANCE_IMAGE).getPath());
            newEvaluation.setPhotoPathRearLeftTyre(vehiclePhotos.get(REAR_LEFT_TYRE).getPath());
            newEvaluation.setPhotoPathRearRightTyre(vehiclePhotos.get(REAR_RIGHT_TYRE).getPath());
            newEvaluation.setPhotoPathFrontLeftTyre(vehiclePhotos.get(FRONT_LEFT_TYRE).getPath());
            newEvaluation.setPhotoPathFrontRightTyre(vehiclePhotos.get(FRONT_RIGHT_TYRE).getPath());
            newEvaluation.setPhotoPathEngineNoImage(vehiclePhotos.get(ENGINE_NO_IMAGE).getPath());
            newEvaluation.setPhotoPathDamagedPartImage1(vehiclePhotos.get(DAMAGED_PART_IMAGE_1).getPath());
            newEvaluation.setPhotoPathDamagedPartImage2(vehiclePhotos.get(DAMAGED_PART_IMAGE_2).getPath());
            newEvaluation.setPhotoPathDamagedPartImage3(vehiclePhotos.get(DAMAGED_PART_IMAGE_3).getPath());
            newEvaluation.setPhotoPathDamagedPartImage4(vehiclePhotos.get(DAMAGED_PART_IMAGE_4).getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    private NewEvaluation setSingleImageInEvaluation(int position) {
        NewEvaluation tempEval = new NewEvaluation();
        tempEval.setId(evaluationId);
        switch (position) {
            case FRONT_IMAGE:
                tempEval.setPhotoPathFrontImage(vehiclePhotos.get(FRONT_IMAGE).getPath());
                break;
            case REAR_IMAGE:
                tempEval.setPhotoPathRearImage(vehiclePhotos.get(REAR_IMAGE).getPath());
                break;
            case FOURTY_DEGREE_LEFT_VIEW:
                tempEval.setPhotoPath45DegreeLeftView(vehiclePhotos.get(FOURTY_DEGREE_LEFT_VIEW).getPath());
                break;
            case FOURTY_DEGREE_RIGHT_VIEW:
                tempEval.setPhotoPath45DegreeRightView(vehiclePhotos.get(FOURTY_DEGREE_RIGHT_VIEW).getPath());
                break;
            case DASHBOARD_VIEW:
                tempEval.setPhotoPathDashboardView(vehiclePhotos.get(DASHBOARD_VIEW).getPath());
                break;
            case ODOMETER_IMAGE:
                tempEval.setPhotoPathOdometerImage(vehiclePhotos.get(ODOMETER_IMAGE).getPath());
                break;
            case CHASSIS_NO_IMAGE:
                tempEval.setPhotoPathChassisNoImage(vehiclePhotos.get(CHASSIS_NO_IMAGE).getPath());
                break;
            case ROOF_TOP_IMAGE:
                tempEval.setPhotoPathRoofTopImage(vehiclePhotos.get(ROOF_TOP_IMAGE).getPath());//roofTop
                break;
            case INTERIOR_VIEW:
                tempEval.setPhotoPathInteriorView(vehiclePhotos.get(INTERIOR_VIEW).getPath());
                break;
            case RCCOPY_IMAGE:
                tempEval.setPhotoPathRCCopyImage(vehiclePhotos.get(RCCOPY_IMAGE).getPath());
                break;
            case RCCOPY_1:
                tempEval.setPhotoPathRCCopy1(vehiclePhotos.get(RCCOPY_1).getPath());
                break;
            case RCCOPY_2:
                tempEval.setPhotoPathRCCopy2(vehiclePhotos.get(RCCOPY_2).getPath());
                break;
            case RCCOPY_3:
                tempEval.setPhotoPathRCCopy3(vehiclePhotos.get(RCCOPY_3).getPath());
                break;
            case INSURANCE_IMAGE:
                tempEval.setPhotoPathInsuranceImage(vehiclePhotos.get(INSURANCE_IMAGE).getPath());
                break;
            case REAR_LEFT_TYRE:
                tempEval.setPhotoPathRearLeftTyre(vehiclePhotos.get(REAR_LEFT_TYRE).getPath());
                break;
            case REAR_RIGHT_TYRE:
                tempEval.setPhotoPathRearRightTyre(vehiclePhotos.get(REAR_RIGHT_TYRE).getPath());
                break;
            case FRONT_LEFT_TYRE:
                tempEval.setPhotoPathFrontLeftTyre(vehiclePhotos.get(FRONT_LEFT_TYRE).getPath());
                break;
            case FRONT_RIGHT_TYRE:
                tempEval.setPhotoPathFrontRightTyre(vehiclePhotos.get(FRONT_RIGHT_TYRE).getPath());
                break;
            case ENGINE_NO_IMAGE:
                tempEval.setPhotoPathEngineNoImage(vehiclePhotos.get(ENGINE_NO_IMAGE).getPath());
                break;
            case DAMAGED_PART_IMAGE_1:
                tempEval.setPhotoPathDamagedPartImage1(vehiclePhotos.get(DAMAGED_PART_IMAGE_1).getPath());
                break;
            case DAMAGED_PART_IMAGE_2:
                tempEval.setPhotoPathDamagedPartImage2(vehiclePhotos.get(DAMAGED_PART_IMAGE_2).getPath());
                break;
            case DAMAGED_PART_IMAGE_3:
                tempEval.setPhotoPathDamagedPartImage3(vehiclePhotos.get(DAMAGED_PART_IMAGE_3).getPath());
                break;
            case DAMAGED_PART_IMAGE_4:
                tempEval.setPhotoPathDamagedPartImage4(vehiclePhotos.get(DAMAGED_PART_IMAGE_4).getPath());
                break;
        }
        return tempEval;
    }

    @Override
    public void saveData() {
        getData();
        imagesPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (!type.equals(KEY_EDIT_EVAL)) {
            for (int iter = 0; iter < vehiclePhotos.size(); iter++) {
                if (vehiclePhotos.get(iter).getNameOfImage().contains("*") &&
                        vehiclePhotos.get(iter).getPath().length() == 0) {
                    CommonHelper.toast("Select " + vehiclePhotos.get(iter).getNameOfImage()
                            .replace("*", "").replace("\n", ""), context);
                    return false;
                }
            }
        }
        return true;
    }

    private void takePicture(int requestCode) {
        Uri photoURI;
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "image.jpg");
        photoURI = FileProvider.getUriForFile(context,
                BuildConfig.APPLICATION_ID + ".provider",
                file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE + requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode >= CAMERA_REQUEST_CODE
                    && requestCode < CAMERA_REQUEST_CODE + 23) {
                try {
                    ImageView imageView = binding.getRoot().findViewById(4567 + (requestCode - CAMERA_REQUEST_CODE));
                    imageView.requestFocus();
                    File fileToUpload;
                    String filePath = new BitmapHelper().compressImage((Activity) context,
                            Environment.getExternalStorageDirectory()
                                    + File.separator + "image.jpg");
                    fileToUpload = new File(filePath);

                    float fileSize = fileToUpload.length() / 1024F / 1024F;
                    if (fileSize < 2.0) {
                        vehiclePhotos.get(requestCode - CAMERA_REQUEST_CODE).setPath(
                                fileToUpload.getAbsolutePath());
                        imagesPresenter.saveData(setSingleImageInEvaluation(requestCode - CAMERA_REQUEST_CODE));
                        Bitmap bitmap = new BitmapHelper().grabImage(fileToUpload.getAbsolutePath());
                        imageView.setImageBitmap(bitmap);
                        imageView.requestLayout();
                    }
                } catch (Exception e) {
                    CommonHelper.toast("Couldn't get image, please try again", context);
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        int result = ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean externalStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraAccepted && externalStorageAccepted)
                    Log.i("Camera_permission", "Permission Granted, Now you can access camera.");
                    //  CommonHelper.toast("Permission Granted, Now you can access camera.", context);
                else {
                    //Log.i(LOG_TAG, "Permission Granted, Now you can access camera.");
                    CommonHelper.toast("Permission Denied, You cannot access camera.", context);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(CAMERA)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{CAMERA, WRITE_EXTERNAL_STORAGE},
                                                        PERMISSION_REQUEST_CODE);
                                            }
                                        }
                                    });
                        }
                    }
                }
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void fillTestDataInForm() {

    }
}