package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.ViewServiceHistoryActivity;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.LayoutVehicleDetailsBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.DatePickerHelper;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.VehicleDetailsPresenter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity.randomNumGen;

public class VehicleDetailsFragment extends Fragment implements CommonMethods, View.OnClickListener {

    LayoutVehicleDetailsBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private VehicleDetailsPresenter vehicleDetailsPresenter;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);

    private ArrayList<MasterModel> arrayListColor = new ArrayList<>();
    private ArrayList<MasterModel> arrayListVariant = new ArrayList<>();
    private ArrayList<MasterModel> arrayListMake = new ArrayList<>();
    private ArrayList<MasterModel> arrayListModel = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutVehicleDetailsBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        vehicleDetailsPresenter = new VehicleDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = vehicleDetailsPresenter.getEvaluationData(evaluationId);
        }
        newEvaluation.setTransmission("M");
        init();
        displayData();
    }

    @Override
    public void init() {
        arrayListColor.add(new MasterModel("Select Colour", "Select Colour"));
        arrayListColor.addAll(vehicleDetailsPresenter.getColors());
        ArrayAdapter<MasterModel> colorAdapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListColor);
        colorAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        binding.spinnerColor.setAdapter(colorAdapter);

        arrayListMake.add(new MasterModel("Select Make", "Select Make"));
        arrayListMake.addAll(vehicleDetailsPresenter.getMakeMaster());
        ArrayAdapter<MasterModel> MakeAdapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListMake);
        MakeAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        binding.spinnerMake.setAdapter(MakeAdapter);
        binding.spinnerMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String strCode = arrayListMake.get(position).getCode();
                arrayListModel.clear();
                if (position != 0) {
                    arrayListModel.add(new MasterModel("Select Model", "Select Model"));
                    arrayListModel.addAll(vehicleDetailsPresenter.getModelMaster(strCode));
                    ArrayAdapter<MasterModel> ModelAdapter = new ArrayAdapter<MasterModel>(context,
                            R.layout.simple_spinner_item, arrayListModel);
                    ModelAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                    binding.spinnerModel.setAdapter(ModelAdapter);
                    SpinnerHelper.setSpinnerValue(arrayListModel, newEvaluation.getModel(), binding.spinnerModel);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayListModel.add(new MasterModel("Select Model", "Select Model"));
        ArrayAdapter<MasterModel> modelArrayAdapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListModel);
        modelArrayAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        binding.spinnerModel.setAdapter(modelArrayAdapter);
        binding.spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (arrayListModel.size() > 0) {
                    String strCode = arrayListModel.get(position).getCode();
                    arrayListVariant.clear();
                    arrayListVariant.add(new MasterModel("Select Variant", "Select Variant"));
                    arrayListVariant.addAll(vehicleDetailsPresenter.getVariantMaster(strCode));

                    ArrayAdapter<MasterModel> VariantAdapter = new ArrayAdapter<>(context,
                            R.layout.simple_spinner_item, arrayListVariant);
                    VariantAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                    binding.spinnerVariant.setAdapter(VariantAdapter);
                    SpinnerHelper.setSpinnerValue(arrayListVariant, newEvaluation.getVariant(), binding.spinnerVariant);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayListVariant = vehicleDetailsPresenter.getVariantMaster(newEvaluation.getModel());
        ArrayAdapter<MasterModel> VariantAdapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListVariant);
        VariantAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        binding.spinnerVariant.setAdapter(VariantAdapter);

        binding.spinnerFuelType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (adapterView.getSelectedItem().toString().equalsIgnoreCase("Liquefied Petroleum Gas")
                            || binding.spinnerFuelType.getSelectedItem().toString()
                            .equalsIgnoreCase("Compressed Natural Gas")) {
                        binding.trCNGKit.setVisibility(View.VISIBLE);
                    } else {
                        binding.trCNGKit.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spinnerRoadTax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.tableRowSelectRoadTaxExpiryDate.
                        setVisibility((position == 2) ? View.INVISIBLE : View.VISIBLE);
                binding.tableRowRoadTaxExpiryDate.
                        setVisibility((position == 2) ? View.INVISIBLE : View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.buttonServiceBookletYes.setOnClickListener(this);
        binding.buttonServiceBookletNo.setOnClickListener(this);
        binding.buttonPucYes.setOnClickListener(this);
        binding.buttonPucNo.setOnClickListener(this);
        binding.buttonHypothecationYes.setOnClickListener(this);
        binding.buttonHypothecationNo.setOnClickListener(this);
        binding.buttonNocStatusYes.setOnClickListener(this);
        binding.buttonNocStatusNo.setOnClickListener(this);
        binding.buttonNocStatusYes.setOnClickListener(this);
        binding.buttonNocStatusNo.setOnClickListener(this);
        binding.buttonTestDriveYes.setOnClickListener(this);
        binding.buttonTestDriveNo.setOnClickListener(this);
        binding.buttonAccidentalStatusYes.setOnClickListener(this);
        binding.buttonAccidentalStatusNo.setOnClickListener(this);
        binding.buttonFitnessCertYes.setOnClickListener(this);
        binding.buttonFitnessCertNo.setOnClickListener(this);
        binding.tvViewServiceHistory.setOnClickListener(this);
        binding.tvViewServiceHistoryReg.setOnClickListener(this);

        binding.linearLayoutAutomatic.setOnClickListener(this);
        binding.linearLayoutManual.setOnClickListener(this);
        binding.tableRowSelectInsuranceDate.setOnClickListener(this);
        binding.tableRowSelectRoadTaxExpiryDate.setOnClickListener(this);
    }

    @Override
    public void displayData() {
        SpinnerHelper.setSpinnerValue(arrayListColor, newEvaluation.getColor(), binding.spinnerColor);
        SpinnerHelper.setSpinnerValue(arrayListMake, newEvaluation.getMake(), binding.spinnerMake);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.ncb),
                newEvaluation.getNcb(),
                binding.spinnerNcb);

        binding.editTextYearOfMfg.setText(newEvaluation.getYearOfMfg());
        binding.editTextYearOfReg.setText(newEvaluation.getYearOfReg());
        binding.editTextRegNo.setText(newEvaluation.getRegNo());
        binding.editTextMilege.setText(newEvaluation.getMileage());

        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.month),
                newEvaluation.getMonthOfMfg(), binding.spinnerMonthOfMfg);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.month),
                newEvaluation.getMonthOfReg(), binding.spinnerMonthOfReg);

        binding.editTextEngineNo.setText(newEvaluation.getEngineNo());
        binding.editTextChasisNo.setText(newEvaluation.getChassisNo());
        binding.editTextNumberOfOwners.setText(newEvaluation.getNoOfOwners());

        setUpTransmissionToggleButtons(newEvaluation.getTransmission(),
                new LinearLayout[]{binding.linearLayoutAutomatic,
                        binding.linearLayoutManual},
                new ImageView[]{binding.imageViewAutomatic,
                        binding.imageViewManual},
                new TextView[]{binding.textViewAutomatic,
                        binding.textViewManual});
        setUpToggleButtons(newEvaluation.getServiceBooklet(),
                new Button[]{binding.buttonServiceBookletYes,
                        binding.buttonServiceBookletNo});

        if (newEvaluation.getFuelType().equalsIgnoreCase("PET")) {
            binding.spinnerFuelType.setSelection(1);
            binding.trCNGKit.setVisibility(View.GONE);
        } else if (newEvaluation.getFuelType().equalsIgnoreCase("DIE")) {
            binding.spinnerFuelType.setSelection(2);
            binding.trCNGKit.setVisibility(View.GONE);
        } else if (newEvaluation.getFuelType().equalsIgnoreCase("LPG")) {
            binding.spinnerFuelType.setSelection(3);
            binding.trCNGKit.setVisibility(View.VISIBLE);
        } else if (newEvaluation.getFuelType().equalsIgnoreCase("CNG")) {
            binding.spinnerFuelType.setSelection(4);
            binding.trCNGKit.setVisibility(View.VISIBLE);
        } else if (newEvaluation.getFuelType().equalsIgnoreCase("ELC")) {
            binding.spinnerFuelType.setSelection(5);
            binding.trCNGKit.setVisibility(View.GONE);
        }

        if (newEvaluation.getEuroVersion().equalsIgnoreCase("OTH")) {
            binding.spinnerEuroVersion.setSelection(4);
        } else {
            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.euro_version),
                    newEvaluation.getEuroVersion(), binding.spinnerEuroVersion);
        }
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.vehicle_usage),
                SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapVehicleUsage,
                        newEvaluation.getVehicleUsage()), binding.spinnerVehicleUsage);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.body_type), newEvaluation.getBodyType(),
                binding.spinnerBodyType);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.no_of_seats), newEvaluation.getNoOfSeats(),
                binding.spinnerNoOfSeats);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.rc_book), newEvaluation.getRCBook(),
                binding.spinnerRCBook);
        SpinnerHelper.setSpinnerValueFromMap(getResources().getStringArray(R.array.rc_remarks),
                newEvaluation.getRcRemarks(),
                binding.spinnerRCRemarks, SpinnerConstants.mapRcRemarks);
        SpinnerHelper.setSpinnerValueFromMap(getResources().getStringArray(R.array.accidental_type),
                newEvaluation.getAccidentalType(),
                binding.spinnerAccidentalType, SpinnerConstants.mapAccidentalType);
        SpinnerHelper.setSpinnerValueFromMap(getResources().getStringArray(R.array.cng_kit),
                newEvaluation.getCNGKit(),
                binding.spinnerCNGKit, SpinnerConstants.mapCNGKit);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.scrap_vehicle), newEvaluation.getScrapVehicle(),
                binding.spinnerScrapVehicle);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.insurance), newEvaluation.getInsurance(),
                binding.spinnerInsurance);
        binding.etAccidentalRemarks.setText(newEvaluation.getAccidentalRemarks());
        binding.textViewInsuranceDate.setText(newEvaluation.getInsuranceDate());

        SpinnerHelper.setSpinnerValueFromMap(getResources().getStringArray(R.array.road_tax),
                newEvaluation.getRoadTax(),
                binding.spinnerRoadTax, SpinnerConstants.mapRoadTax);
        binding.textViewRoadTaxExpiryDate.setText(newEvaluation.getRoadTaxExpiryDate());

        setUpToggleButtons(newEvaluation.getPuc(),
                new Button[]{binding.buttonPucYes,
                        binding.buttonPucNo});
        setUpToggleButtons(newEvaluation.getHypothecation(),
                new Button[]{binding.buttonHypothecationYes,
                        binding.buttonHypothecationNo});
        setUpToggleButtons(newEvaluation.getNocStatus(),
                new Button[]{binding.buttonNocStatusYes,
                        binding.buttonNocStatusNo});

        binding.editTextFinanceCompany.setText(newEvaluation.getFinanceCompany());
        setUpToggleButtons(newEvaluation.getNocStatus(),
                new Button[]{binding.buttonNocStatusYes,
                        binding.buttonNocStatusNo});

        binding.editTextCustomerExpectedPrice.setText(newEvaluation.getCustomerExpectedPrice());
        setUpToggleButtons(newEvaluation.getTestDrive(),
                new Button[]{binding.buttonTestDriveYes,
                        binding.buttonTestDriveNo});
        setUpToggleButtons(newEvaluation.getIsAccident(),
                new Button[]{binding.buttonAccidentalStatusYes,
                        binding.buttonAccidentalStatusNo});
        setUpToggleButtons(newEvaluation.getFitnessCertificate(),
                new Button[]{binding.buttonFitnessCertYes,
                        binding.buttonFitnessCertNo});
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setMake(SpinnerHelper.getSpinnerCodeFromArrayList(
                    binding.spinnerMake, arrayListMake));
            newEvaluation.setModel(SpinnerHelper.getSpinnerCodeFromArrayList(
                    binding.spinnerModel, arrayListModel));
            newEvaluation.setVariant(SpinnerHelper.getSpinnerCodeFromArrayList(
                    binding.spinnerVariant, arrayListVariant));
            newEvaluation.setYearOfMfg(binding.editTextYearOfMfg.getText().toString().trim());
            newEvaluation.setYearOfReg(binding.editTextYearOfReg.getText().toString().trim());
            newEvaluation.setRegNo(binding.editTextRegNo.getText().toString().trim());
            newEvaluation.setMileage(binding.editTextMilege.getText().toString().trim());
            newEvaluation.setMonthOfMfg(binding.spinnerMonthOfMfg.getSelectedItem().toString().trim());
            newEvaluation.setMonthOfReg(binding.spinnerMonthOfReg.getSelectedItem().toString().trim());
            newEvaluation.setEngineNo(binding.editTextEngineNo.getText().toString().trim());
            newEvaluation.setChassisNo(binding.editTextChasisNo.getText().toString().trim());
            newEvaluation.setNoOfOwners(binding.editTextNumberOfOwners.getText().toString().trim());
            newEvaluation.setColor(SpinnerHelper.getSpinnerCodeFromArrayList(
                    binding.spinnerColor, arrayListColor));
            newEvaluation.setFuelType(MultiSelectDialogHelper.getKeysFromValues(
                    binding.spinnerFuelType.getSelectedItem().toString(),
                    SpinnerConstants.mapFueltype, MultiSelectDialogHelper.COMMA));
            if (newEvaluation.getFuelType().equalsIgnoreCase("CNG")
                    || newEvaluation.getFuelType().equalsIgnoreCase("LPG")) {
                newEvaluation.setCNGKit(MultiSelectDialogHelper.getKeysFromValues(
                        binding.spinnerCNGKit.getSelectedItem().toString(),
                        SpinnerConstants.mapCNGKit, MultiSelectDialogHelper.COMMA));
            }

            String strEuroVersion = binding.spinnerEuroVersion.getSelectedItem().toString();
            if (strEuroVersion.equalsIgnoreCase("Other")) {
                strEuroVersion = "OTH";
            }
            newEvaluation.setEuroVersion(strEuroVersion);
            newEvaluation.setVehicleUsage(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerVehicleUsage.getSelectedItem().toString(),
                    SpinnerConstants.mapVehicleUsage));
            if (binding.spinnerBodyType.getSelectedItemPosition() > 0)
                newEvaluation.setBodyType(String.valueOf(binding.spinnerBodyType.getSelectedItemPosition()));

            if (binding.spinnerNoOfSeats.getSelectedItemPosition() > 0)
                newEvaluation.setNoOfSeats(String.valueOf(binding.spinnerNoOfSeats.getSelectedItemPosition()));


            String strRCBook = binding.spinnerRCBook.getSelectedItem().toString();
            if (strRCBook.equalsIgnoreCase("Duplicate")) {
                strRCBook = "D";
            } else if (strRCBook.equalsIgnoreCase("Original")) {
                strRCBook = "O";
            }
            newEvaluation.setRCBook(strRCBook);
            newEvaluation.setRcRemarks(MultiSelectDialogHelper.getKeysFromValues(
                    binding.spinnerRCRemarks.getSelectedItem().toString(),
                    SpinnerConstants.mapRcRemarks, MultiSelectDialogHelper.COMMA));
            if (newEvaluation.getIsAccident().equalsIgnoreCase("Y")) {
                newEvaluation.setAccidentalType(MultiSelectDialogHelper.getKeysFromValues(
                        binding.spinnerAccidentalType.getSelectedItem().toString(),
                        SpinnerConstants.mapAccidentalType, MultiSelectDialogHelper.COMMA));
                newEvaluation.setAccidentalRemarks(binding.etAccidentalRemarks.getText().toString().trim());
            } else {
                newEvaluation.setAccidentalType("");
                newEvaluation.setAccidentalRemarks("");
            }
            newEvaluation.setScrapVehicle(binding.spinnerScrapVehicle.getSelectedItem().toString());
            String strInsurance = binding.spinnerInsurance.getSelectedItem().toString();
            if (strInsurance.equalsIgnoreCase("Third Party")) {
                strInsurance = "T";
            } else if (strInsurance.equalsIgnoreCase("Comprehensive")) {
                strInsurance = "C";
            } else if (strInsurance.equalsIgnoreCase("Expired")) {
                strInsurance = "E";
            }
            newEvaluation.setInsurance(strInsurance);
            newEvaluation.setInsuranceDate(binding.textViewInsuranceDate.getText().toString().trim());
            newEvaluation.setNcb(binding.spinnerNcb.getSelectedItem().toString().trim());
            newEvaluation.setFinanceCompany(binding.editTextFinanceCompany.getText().toString().trim());
            newEvaluation.setCustomerExpectedPrice(
                    binding.editTextCustomerExpectedPrice.getText().toString().trim());
            newEvaluation.setRoadTax(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerRoadTax.getSelectedItem().toString(), SpinnerConstants.mapRoadTax));
            newEvaluation.setRoadTaxExpiryDate(binding.textViewRoadTaxExpiryDate.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        vehicleDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.spinnerMake.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerMake,
                    "Select vehicle make");
        }
        if (binding.spinnerModel.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerModel,
                    "Select vehicle model");
        }
        if (binding.spinnerVariant.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerVariant,
                    "Select vehicle variant");
        }
        if (binding.editTextYearOfMfg.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextYearOfMfg,
                    "Enter manufacture year");
        }

        if (binding.editTextYearOfMfg.length() <= 4) {
            try {
                if (Integer.parseInt(simpleDateFormat.format(new Date()))
                        < Integer.parseInt(binding.editTextYearOfMfg.getText().toString())) {
                    return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextYearOfMfg,
                            "Manufacture year can't be greater than current year");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (binding.editTextYearOfReg.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextYearOfReg,
                    "Enter registration year");
        }

        if (binding.editTextYearOfReg.length() <= 4) {
            try {
                if (Integer.parseInt(simpleDateFormat.format(new Date())) < Integer.parseInt(
                        binding.editTextYearOfReg.getText().toString())) {
                    return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextYearOfReg,
                            "Registration year can't be greater than current year");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            if (Integer.parseInt(binding.editTextYearOfMfg.getText().toString())
                    > Integer.parseInt(binding.editTextYearOfReg.getText().toString())) {
                return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextYearOfReg,
                        "Manufacturer year can't be greater than mfg. year");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (binding.editTextRegNo.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextRegNo,
                    "Enter registration number");
        }
        if (binding.editTextMilege.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextMilege,
                    "Enter Odometer reading");
        }
        if (binding.spinnerMonthOfReg.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerMonthOfReg,
                    "Select month of registration");
        }
        if (binding.editTextEngineNo.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextEngineNo,
                    "Enter engine number");
        }
        if (binding.editTextChasisNo.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextChasisNo,
                    "Enter chassis number");
        }
        if (binding.editTextNumberOfOwners.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextNumberOfOwners,
                    "Enter Number of owners");
        }
        if (Integer.parseInt(binding.editTextNumberOfOwners.getText().toString()) == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextNumberOfOwners,
                    "Number of Owners must be greater than 0");
        }
        if (null != newEvaluation.getTransmission() && newEvaluation.getTransmission().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.linearLayoutAutomatic,
                    "Select transmission");
        }
        if (newEvaluation.getServiceBooklet().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select service booklet");
        }
        if (binding.spinnerColor.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerColor,
                    "Select vehicle colour");
        }
        if (binding.spinnerFuelType.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerFuelType,
                    "Select vehicle fuel type");
        }
        if (binding.spinnerFuelType.getSelectedItem().toString().equalsIgnoreCase("Liquefied Petroleum Gas")
                || binding.spinnerFuelType.getSelectedItem().toString().equalsIgnoreCase("Compressed Natural Gas")) {
            if (binding.spinnerCNGKit.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerCNGKit,
                        "Select vehicle CNG Kit");
            }
        }
        if (binding.spinnerEuroVersion.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerEuroVersion,
                    "Select vehicle euro version");
        }
        if (binding.spinnerVehicleUsage.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerVehicleUsage,
                    "Select vehicle usage");
        }
        if (binding.spinnerRCBook.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRCBook,
                    "Select vehicle RC type");
        }
        if (binding.spinnerRCRemarks.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRCRemarks,
                    "Select vehicle RC Remarks");
        }
        if (binding.spinnerInsurance.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerInsurance,
                    "Select vehicle insurance type");
        }
        if (binding.textViewInsuranceDate.length() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.textViewInsuranceDate,
                    "Select insurance expiry date");
        }
        if (binding.spinnerNcb.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerNcb,
                    "Select NCB");
        }
        if (null != newEvaluation.getPuc() && newEvaluation.getPuc().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select PUC");
        }
        if (null != newEvaluation.getHypothecation() && newEvaluation.getHypothecation().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select hypothecation");
        }
        if (null != newEvaluation.getHypothecation() && newEvaluation.getHypothecation().equalsIgnoreCase("Y")) {
            newEvaluation.setFinanceCompany(binding.editTextFinanceCompany.getText().toString().trim());
            if (null != newEvaluation.getFinanceCompany() && newEvaluation.getFinanceCompany().length() == 0)
                return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextFinanceCompany,
                        "Enter Finance Company name");
        }
        if (null != newEvaluation.getNocStatus() && newEvaluation.getNocStatus().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select NOC status");
        }
        if (binding.editTextCustomerExpectedPrice.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextCustomerExpectedPrice,
                    "Enter customer expected price");
        }
        if (null != newEvaluation.getTestDrive() && newEvaluation.getTestDrive().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select test drive");
        }
        if (null != newEvaluation.getIsAccident() && newEvaluation.getIsAccident().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select accidental status");
        }
        if (null != newEvaluation.getFitnessCertificate() && newEvaluation.getFitnessCertificate().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, null,
                    "Select fitness certificate");
        }

        if (newEvaluation.getIsAccident().equalsIgnoreCase("Y") ||
                newEvaluation.getIsAccident().equalsIgnoreCase("Yes")) {
            if (binding.spinnerAccidentalType.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerAccidentalType,
                        "Select Accidental Type");
            }

            if (binding.etAccidentalRemarks.length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context, binding.etAccidentalRemarks,
                        "Enter Accidental Remarks");
            }
        }

        if (binding.spinnerScrapVehicle.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerScrapVehicle,
                    "Select Scrap Vehicle");
        }

        if (binding.spinnerRoadTax.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRoadTax,
                    "Select Road Tax");
        }

        if (binding.spinnerRoadTax.getSelectedItemPosition() == 1) {
            if (binding.textViewRoadTaxExpiryDate.length() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.textViewRoadTaxExpiryDate,
                        "Select road tax expiry date");
            } else {
//                Date date = new Date();
//                date.after()
            }
        }
        saveData();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tableRowSelectInsuranceDate:
                if (binding.spinnerInsurance.getSelectedItem().toString()
                        .equalsIgnoreCase("Expired")) {
                    new DatePickerHelper(context, R.id.textViewInsuranceDate,
                            R.id.textViewInsuranceDate, false, true);
                } else if (binding.spinnerInsurance.getSelectedItem().toString()
                        .equalsIgnoreCase("Comprehensive") ||
                        binding.spinnerInsurance.getSelectedItem().toString()
                                .equalsIgnoreCase("Third Party")) {
                    new DatePickerHelper(context, R.id.textViewInsuranceDate,
                            R.id.textViewInsuranceDate, true, false);
                } else {
                    CommonHelper.toast("Please select insurance type", context);
                }
                break;
            case R.id.linearLayoutAutomatic:
            case R.id.linearLayoutManual:
                if (v.getId() == R.id.linearLayoutAutomatic) {
                    newEvaluation.setTransmission("A");
                } else {
                    newEvaluation.setTransmission("M");
                }
                setUpTransmissionToggleButtons(newEvaluation.getTransmission(),
                        new LinearLayout[]{binding.linearLayoutAutomatic,
                                binding.linearLayoutManual},
                        new ImageView[]{binding.imageViewAutomatic,
                                binding.imageViewManual},
                        new TextView[]{binding.textViewAutomatic,
                                binding.textViewManual});
                break;
            case R.id.buttonTestDriveYes:
            case R.id.buttonTestDriveNo:
                if (v.getId() == R.id.buttonTestDriveYes) {
                    newEvaluation.setTestDrive("Y");
                } else {
                    newEvaluation.setTestDrive("N");
                }
                setUpToggleButtons(newEvaluation.getTestDrive(),
                        new Button[]{binding.buttonTestDriveYes,
                                binding.buttonTestDriveNo});
                break;
            case R.id.buttonFitnessCertYes:
            case R.id.buttonFitnessCertNo:
                if (v.getId() == R.id.buttonFitnessCertYes) {
                    newEvaluation.setFitnessCertificate("YES");
                } else {
                    newEvaluation.setFitnessCertificate("NO");
                }
                setUpToggleButtons(newEvaluation.getFitnessCertificate(),
                        new Button[]{binding.buttonFitnessCertYes,
                                binding.buttonFitnessCertNo});
                break;
            case R.id.buttonAccidentalStatusYes:
            case R.id.buttonAccidentalStatusNo:
                if (v.getId() == R.id.buttonAccidentalStatusYes) {
                    newEvaluation.setIsAccident("Y");
                    binding.llAccidentalRemarks.setVisibility(View.VISIBLE);
                    binding.trAccidentalType.setVisibility(View.VISIBLE);
                } else {
                    newEvaluation.setIsAccident("N");
                    binding.llAccidentalRemarks.setVisibility(View.GONE);
                    binding.trAccidentalType.setVisibility(View.GONE);
                }
                setUpToggleButtons(newEvaluation.getIsAccident(),
                        new Button[]{binding.buttonAccidentalStatusYes,
                                binding.buttonAccidentalStatusNo});
                break;
            case R.id.buttonServiceBookletYes:
            case R.id.buttonServiceBookletNo:
                if (v.getId() == R.id.buttonServiceBookletYes) {
                    newEvaluation.setServiceBooklet("Y");
                } else {
                    newEvaluation.setServiceBooklet("N");
                }
                setUpToggleButtons(newEvaluation.getServiceBooklet(),
                        new Button[]{binding.buttonServiceBookletYes,
                                binding.buttonServiceBookletNo});
                break;
            case R.id.buttonNocStatusYes:
            case R.id.buttonNocStatusNo:
                if (v.getId() == R.id.buttonNocStatusYes) {
                    newEvaluation.setNocStatus("Y");
                } else {
                    newEvaluation.setNocStatus("N");
                }
                setUpToggleButtons(newEvaluation.getNocStatus(),
                        new Button[]{binding.buttonNocStatusYes,
                                binding.buttonNocStatusNo});
                break;
            case R.id.buttonPucYes:
            case R.id.buttonPucNo:
                if (v.getId() == R.id.buttonPucYes) {
                    newEvaluation.setPuc("Y");
                } else {
                    newEvaluation.setPuc("N");
                }
                setUpToggleButtons(newEvaluation.getPuc(),
                        new Button[]{binding.buttonPucYes,
                                binding.buttonPucNo});
                break;
            case R.id.buttonHypothecationYes:
            case R.id.buttonHypothecationNo:
                if (v.getId() == R.id.buttonHypothecationYes) {
                    newEvaluation.setHypothecation("Y");
                    newEvaluation.setNocStatus("Y");
                    binding.editTextFinanceCompany.setEnabled(true);
                    binding.buttonNocStatusYes.setEnabled(true);
                    binding.buttonNocStatusNo.setEnabled(true);
                } else {
                    newEvaluation.setHypothecation("N");
                    newEvaluation.setNocStatus("N");
                    binding.editTextFinanceCompany.setEnabled(false);
                    binding.buttonNocStatusYes.setEnabled(false);
                    binding.buttonNocStatusNo.setEnabled(false);
                }
                setUpToggleButtons(newEvaluation.getHypothecation(),
                        new Button[]{binding.buttonHypothecationYes,
                                binding.buttonHypothecationNo});
                setUpToggleButtons(newEvaluation.getNocStatus(),
                        new Button[]{binding.buttonNocStatusYes,
                                binding.buttonNocStatusNo});
                break;
            case R.id.tableRowSelectRoadTaxExpiryDate:
                new DatePickerHelper(context, R.id.textViewRoadTaxExpiryDate,
                        R.id.textViewRoadTaxExpiryDate, true, false);
                break;
            case R.id.tv_view_service_history:
                if (null != binding.editTextChasisNo &&
                        !binding.editTextChasisNo.getText().toString().isEmpty()) {
                    Intent intent = new Intent(context, ViewServiceHistoryActivity.class);
                    intent.putExtra("chassis_no", binding.editTextChasisNo.getText().toString());
                    startActivity(intent);
                } else {
                    CommonHelper.toast("Please enter chassis number", context);
                }
                break;
            case R.id.tv_view_service_history_reg:
                if (null != binding.editTextRegNo &&
                        !binding.editTextRegNo.getText().toString().isEmpty()) {
                    Intent intent = new Intent(context, ViewServiceHistoryActivity.class);
                    intent.putExtra("reg_no", binding.editTextRegNo.getText().toString());
                    startActivity(intent);
                } else {
                    CommonHelper.toast("Please enter registration number", context);
                }
                break;
        }
    }

    private void setUpToggleButtons(String value, Button[] btns) {
        int highlightPos = -1, dimPos = -1;
        if ((value.equalsIgnoreCase("No")) ||
                (value.equalsIgnoreCase("N"))) {
            highlightPos = 0;
            dimPos = 1;
        } else if ((value.equalsIgnoreCase("Yes")) ||
                (value.equalsIgnoreCase("Y"))) {
            highlightPos = 1;
            dimPos = 0;
        }

        if (highlightPos != -1 && dimPos != -1) {
            btns[highlightPos].setTextColor(getResources().getColor(R.color.colorblack));
            btns[highlightPos].setBackgroundResource(R.drawable.disable_edit_text_border);

            btns[dimPos].setTextColor(getResources().getColor(R.color.colorWhite));
            btns[dimPos].setBackgroundResource(R.drawable.gray_rounded_bg);
        }
    }

    private void setUpTransmissionToggleButtons(String value, LinearLayout[] linearLayouts,
                                                ImageView[] imageViews, TextView[] textViews) {
        int highlightPos = 1, dimPos = 0;
        if (value.equalsIgnoreCase("A")) {
            highlightPos = 0;
            dimPos = 1;
        }

        linearLayouts[highlightPos].setBackgroundResource(R.drawable.gray_rounded_bg);
        imageViews[highlightPos].setColorFilter(ContextCompat.getColor(context, R.color.colorWhite));
        textViews[highlightPos].setTextColor(getResources().getColor(R.color.colorWhite));
        linearLayouts[dimPos].setBackgroundResource(R.drawable.dotted_line_box);
        imageViews[dimPos].setColorFilter(ContextCompat.getColor(context, R.color.colorGrey));
        textViews[dimPos].setTextColor(getResources().getColor(R.color.colorGrey));
    }

    @Override
    public void fillTestDataInForm() {
        final int randomNum = new Random().nextInt(100);
        binding.spinnerMake.setSelection(randomNumGen(10, 1));
//        binding.spinnerModel.setSelection(1);
        binding.editTextYearOfMfg.setText(randomNumGen(2011, 2001) + "");
        binding.editTextYearOfReg.setText(randomNumGen(2019, 2011) + "");
        binding.editTextRegNo.setText("9044328");
        binding.editTextMilege.setText(randomNumGen(100000, 100) + "");
        binding.spinnerMonthOfMfg.setSelection(randomNumGen(11, 1));
        binding.spinnerMonthOfReg.setSelection(randomNumGen(11, 1));
        binding.editTextEngineNo.setText("8328237");
        binding.editTextChasisNo.setText("3993284");
        binding.editTextNumberOfOwners.setText(randomNumGen(9, 1) + "");
        binding.buttonServiceBookletYes.performClick();
        binding.spinnerColor.setSelection(randomNumGen(150, 1));
        binding.spinnerFuelType.setSelection(randomNumGen(2, 1));
        binding.spinnerEuroVersion.setSelection(randomNumGen(4, 1));
        binding.spinnerVehicleUsage.setSelection(randomNumGen(3, 1));
        binding.spinnerBodyType.setSelection(randomNumGen(3, 1));
        binding.spinnerNoOfSeats.setSelection(randomNumGen(7, 1));
        binding.spinnerRCBook.setSelection(randomNumGen(2, 1));
        binding.spinnerRCRemarks.setSelection(randomNumGen(3, 1));
        binding.spinnerInsurance.setSelection(randomNumGen(3, 1));
        binding.textViewInsuranceDate.setText("12/07/2020");
        binding.spinnerNcb.setSelection(randomNumGen(6, 1));
        binding.buttonPucYes.performClick();
        binding.buttonHypothecationYes.performClick();
        binding.buttonNocStatusYes.performClick();
        binding.editTextFinanceCompany.setText("FinanceComapany" + randomNum);
        binding.editTextCustomerExpectedPrice.setText(randomNumGen(10000000, 10000) + "");
        binding.buttonTestDriveYes.performClick();
        binding.buttonAccidentalStatusYes.performClick();
        binding.spinnerAccidentalType.setSelection(randomNumGen(3, 1));
        binding.spinnerRoadTax.setSelection(1);
        binding.textViewRoadTaxExpiryDate.setText("12/07/2020");
        binding.etAccidentalRemarks.setText("Accidental Remark" + randomNum);
        binding.spinnerScrapVehicle.setSelection(randomNumGen(2, 1));
        binding.buttonFitnessCertYes.performClick();
    }
}