package com.mahindra.mitra20.evaluator.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.module_broker.models.Notification;

import java.util.ArrayList;

/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private ArrayList<Notification> NotificationModelContents;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNotification;
        TableRow trListItem;

        public ViewHolder(View v) {
            super(v);
            textViewNotification = v.findViewById(R.id.textViewNotification);
            trListItem = v.findViewById(R.id.tr_list_item);
        }
    }

    public void updateList(ArrayList<Notification> NotificationModelContents) {
        this.NotificationModelContents = NotificationModelContents;
    }

    public NotificationAdapter(Context _context, ArrayList<Notification> _NotificationModelContents) {
        NotificationModelContents = _NotificationModelContents;
        context = _context;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_notifications, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textViewNotification.setText(NotificationModelContents.get(position).getMessage());
        holder.textViewNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return NotificationModelContents.size();
    }
}