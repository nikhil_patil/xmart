package com.mahindra.mitra20.evaluator.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.bumptech.glide.util.Preconditions.checkArgument;

/**
 * Created by BADHAP-CONT on 8/16/2018.
 */

public class DateHelper {
    private String monthName = "";
    private String year = "";

    public String getMonthName(Date date) {
        if (date != null) {
            monthName = new SimpleDateFormat("MMM").format(date);
        }
        return monthName;
    }

    public String getYear(Date date) {
        if (date != null) {
            year = new SimpleDateFormat("yyyy").format(date);
        }
        return year;
    }

    public String getDate(Date date) {
        try {
            String suffix = "";
            Calendar lCal = Calendar.getInstance();
            lCal.setTime(date);
            int lYear = lCal.get(Calendar.YEAR);
            int lMonth = lCal.get(Calendar.MONTH) + 1;

            int lDay = lCal.get(Calendar.DATE);

            checkArgument(lDay >= 1 && lDay <= 31, "illegal day of month: " + lDay);
            if (lDay >= 11 && lDay <= 13) {
                return String.valueOf(lDay) +"th";
            }
            switch (lDay % 10) {
                case 1:
                    suffix = "st";
                    break;
                case 2:
                    suffix = "nd";
                    break;
                case 3:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }
            return String.valueOf(lDay) + suffix;
        } catch (Exception e) {
            return "";
        }
    }

    public String convertTwentyFourHourToTweleveHour(String time) {
        String strTime = "";
        try {
            if (null != time) {
                if (time.length() != 0) {
                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                    Date _24HourDt = _24HourSDF.parse(time);
                    strTime = _12HourSDF.format(_24HourDt);
                    strTime = strTime.replace("a.m.", "AM").replace("p.m.", "PM");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strTime;
    }

    public Date convertStringToDate(String strDate) {
        Date date1 = null;
        try {
            if (strDate != null) {
                if (strDate.contains("-")) {
                    date1 = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);
                } else if (strDate.contains("/")) {
                    date1 = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                }
            }
        } catch
                (Exception e) {
            e.printStackTrace();
        }
        return date1;
    }

    public String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);


        return formattedDate;
    }

    public String getNextDate(int days) {

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);

        Date d = c.getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(d);

        return formattedDate;
    }

    public String getTenPreviousDate() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -10);

        Date d = c.getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(d);

        return formattedDate;
    }

    public String getPreviousDateByDays(int noDay) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, noDay);

        Date d = c.getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(d);

        return formattedDate;
    }

    public int compareDates(Date date1, Date date2)
    {
       if(date1.compareTo(date2) == 0)
       {
           return 0;
       }
       else if(date1.compareTo(date2) < 0)
       {
           return -1;
       }
       else if(date1.compareTo(date2) > 0)
       {
           return  1;
       }
       return 0;
    }

}
