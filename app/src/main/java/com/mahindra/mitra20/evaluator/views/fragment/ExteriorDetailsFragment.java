package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.LayoutExteriorBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.ExteriorDetailsPresenter;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.util.ArrayList;

import static com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity.randomNumGen;

public class ExteriorDetailsFragment extends Fragment implements CommonMethods, View.OnClickListener {

    LayoutExteriorBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private ExteriorDetailsPresenter exteriorDetailsPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutExteriorBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        exteriorDetailsPresenter = new ExteriorDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = exteriorDetailsPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        binding.editTextFrontFenderRightSide.setOnClickListener(this);
        binding.editTextFrontDoorRightSide.setOnClickListener(this);
        binding.editTextBackDoorRightSide.setOnClickListener(this);
        binding.editTextBackFenderRightSide.setOnClickListener(this);
        binding.editTextBonnet.setOnClickListener(this);
        binding.editTextTailGate.setOnClickListener(this);
        binding.editTextFrontFenderLeftHandSide.setOnClickListener(this);
        binding.editTextFrontDoorLeftHandSide.setOnClickListener(this);
        binding.editTextBackDoorLeftHandSide.setOnClickListener(this);
        binding.editTextBackFenderLeftHandSide.setOnClickListener(this);
        binding.editTextPillarType.setOnClickListener(this);
        binding.editTextBackBumper.setOnClickListener(this);
    }

    @Override
    public void displayData() {
        binding.editTextFrontFenderRightSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getFrontFenderRightSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextFrontDoorRightSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getFrontDoorRightSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextBackDoorRightSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBackDoorRightSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextBackFenderRightSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBackFenderRightSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextBonnet.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBonnet(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextTailGate.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getTailGate(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextFrontFenderLeftHandSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getFrontFenderLeftHandSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextFrontDoorLeftHandSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getFrontDoorLeftHandSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextBackDoorLeftHandSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBackDoorLeftHandSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        binding.editTextBackFenderLeftHandSide.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBackFenderLeftHandSide(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.good_broken),
                newEvaluation.getWindShield(), binding.spinnerWindShield);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.original_repair_damaged),
                newEvaluation.getPillars(), binding.spinnerPillars);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.good_feded_broken),
                newEvaluation.getMirrors(), binding.spinnerMirrors);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapHeadTailLight.keySet()),
                newEvaluation.getHeadLightLH(), binding.spinnerHeadLightLH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapHeadTailLight.keySet()),
                newEvaluation.getHeadLightRH(), binding.spinnerHeadLightRH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapHeadTailLight.keySet()),
                newEvaluation.getTailLightLH(), binding.spinnerTailLightLH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapHeadTailLight.keySet()),
                newEvaluation.getTailLightRH(), binding.spinnerTailLightRH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapDickyDoor.keySet()),
                newEvaluation.getDickyDoor(), binding.spinnerDickyDoor);
        binding.editTextPillarType.setText(
                MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getPillarType(),
                        SpinnerConstants.mapPillarType, MultiSelectDialogHelper.COMMA));
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFrontBumper.keySet()),
                newEvaluation.getFrontBumper(), binding.spinnerFrontBumper);
        binding.editTextBackBumper.setText(MultiSelectDialogHelper.getValuesFromKeys(
                newEvaluation.getBackBumper(), SpinnerConstants.mapCondition,
                MultiSelectDialogHelper.COMMA));
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapOutsideRearViewMirrors.keySet()),
                newEvaluation.getOutsideRearViewMirrorLH(), binding.spinnerOutSideRearViewMirrorLH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapOutsideRearViewMirrors.keySet()),
                newEvaluation.getOutsideRearViewMirrorRH(), binding.spinnerOutSideRearViewMirrorRH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapBodyShell.keySet()),
                newEvaluation.getBodyShell(), binding.spinnerBodyShell);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapChassis.keySet()),
                newEvaluation.getChassis(), binding.spinnerChassis);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapApronFront.keySet()),
                newEvaluation.getApronFrontLH(), binding.spinnerApronFrontLH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapApronFront.keySet()),
                newEvaluation.getApronFrontRH(), binding.spinnerApronFrontRH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapStrutMountingArea.keySet()),
                newEvaluation.getStrutMountingArea(), binding.spinnerStrutMountingArea);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFirewall.keySet()),
                newEvaluation.getFirewall(), binding.spinnerFirewall);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapUpperLowerCrossMember.keySet()),
                newEvaluation.getUpperCrossMember(), binding.spinnerUpperCrossMember);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapUpperLowerCrossMember.keySet()),
                newEvaluation.getLowerCrossMember(), binding.spinnerLowerCrossMember);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapCowlTop.keySet()),
                newEvaluation.getCowlTop(), binding.spinnerCowlTop);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapRadiatorSupport.keySet()),
                newEvaluation.getRadiatorSupport(), binding.spinnerRadiator);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapEngineRoomCarrierAssembly.keySet()),
                newEvaluation.getEngineRoomCarrierAssembly(), binding.spinnerEngineRoomCarrierAssembly);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapRunningBoard.keySet()),
                newEvaluation.getRunningBoardLH(), binding.spinnerRunningBoardLH);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapRunningBoard.keySet()),
                newEvaluation.getRunningBoardRH(), binding.spinnerRunningBoardRH);
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setBackBumper(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackBumper.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setFrontFenderRightSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextFrontFenderRightSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setFrontDoorRightSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextFrontDoorRightSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setBackDoorRightSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackDoorRightSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setBackFenderRightSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackFenderRightSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setBonnet(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBonnet.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setTailGate(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextTailGate.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setFrontFenderLeftHandSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextFrontFenderLeftHandSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setFrontDoorLeftHandSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextFrontDoorLeftHandSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setBackDoorLeftHandSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackDoorLeftHandSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setBackFenderLeftHandSide(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackFenderLeftHandSide.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setWindShield(String.valueOf(
                    binding.spinnerWindShield.getSelectedItemPosition()));
            newEvaluation.setPillars(String.valueOf(
                    binding.spinnerPillars.getSelectedItemPosition()));
            newEvaluation.setMirrors(String.valueOf(
                    binding.spinnerMirrors.getSelectedItemPosition()));
            newEvaluation.setHeadLightLH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerHeadLightLH.getSelectedItem().toString(),
                    SpinnerConstants.mapHeadTailLight));
            newEvaluation.setHeadLightRH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerHeadLightRH.getSelectedItem().toString(),
                    SpinnerConstants.mapHeadTailLight));
            newEvaluation.setTailLightLH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerTailLightLH.getSelectedItem().toString(),
                    SpinnerConstants.mapHeadTailLight));
            newEvaluation.setTailLightRH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerTailLightRH.getSelectedItem().toString(),
                    SpinnerConstants.mapHeadTailLight));
            newEvaluation.setDickyDoor(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerDickyDoor.getSelectedItem().toString(),
                    SpinnerConstants.mapDickyDoor));
            newEvaluation.setFrontBumper(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerFrontBumper.getSelectedItem().toString(),
                    SpinnerConstants.mapFrontBumper));
            newEvaluation.setBackBumper(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextBackBumper.getText().toString(),
                    SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
            newEvaluation.setOutsideRearViewMirrorLH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerOutSideRearViewMirrorLH.getSelectedItem().toString(),
                    SpinnerConstants.mapOutsideRearViewMirrors));
            newEvaluation.setOutsideRearViewMirrorRH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerOutSideRearViewMirrorRH.getSelectedItem().toString(),
                    SpinnerConstants.mapOutsideRearViewMirrors));
            newEvaluation.setBodyShell(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerBodyShell.getSelectedItem().toString(),
                    SpinnerConstants.mapBodyShell));
            newEvaluation.setChassis(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerChassis.getSelectedItem().toString(),
                    SpinnerConstants.mapChassis));
            newEvaluation.setApronFrontLH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerApronFrontLH.getSelectedItem().toString(),
                    SpinnerConstants.mapApronFront));
            newEvaluation.setApronFrontRH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerApronFrontRH.getSelectedItem().toString(),
                    SpinnerConstants.mapApronFront));
            newEvaluation.setStrutMountingArea(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerStrutMountingArea.getSelectedItem().toString(),
                    SpinnerConstants.mapStrutMountingArea));
            newEvaluation.setFirewall(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerFirewall.getSelectedItem().toString(),
                    SpinnerConstants.mapFirewall));
            newEvaluation.setUpperCrossMember(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerUpperCrossMember.getSelectedItem().toString(),
                    SpinnerConstants.mapUpperLowerCrossMember));
            newEvaluation.setLowerCrossMember(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerLowerCrossMember.getSelectedItem().toString(),
                    SpinnerConstants.mapUpperLowerCrossMember));
            newEvaluation.setCowlTop(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerCowlTop.getSelectedItem().toString(),
                    SpinnerConstants.mapCowlTop));
            newEvaluation.setRadiatorSupport(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerRadiator.getSelectedItem().toString(),
                    SpinnerConstants.mapRadiatorSupport));
            newEvaluation.setEngineRoomCarrierAssembly(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerEngineRoomCarrierAssembly.getSelectedItem().toString(),
                    SpinnerConstants.mapEngineRoomCarrierAssembly));
            newEvaluation.setRunningBoardLH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerRunningBoardLH.getSelectedItem().toString(),
                    SpinnerConstants.mapRunningBoard));
            newEvaluation.setRunningBoardRH(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerRunningBoardRH.getSelectedItem().toString(),
                    SpinnerConstants.mapRunningBoard));
            newEvaluation.setPillarType(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextPillarType.getText().toString().trim(),
                    SpinnerConstants.mapPillarType, MultiSelectDialogHelper.COMMA));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        exteriorDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.editTextFrontFenderRightSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextFrontFenderRightSide,
                    "Select front fender right side status");
        }
        if (binding.editTextFrontDoorRightSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextFrontDoorRightSide,
                    "Select front door right side status");
        }
        if (binding.editTextBackDoorRightSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBackDoorRightSide,
                    "Select back door right side status");
        }
        if (binding.editTextBackFenderRightSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBackFenderRightSide,
                    "Select back fender right side status");
        }
        if (binding.editTextBonnet.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBonnet,
                    "Select bonnet status");
        }
        if (binding.editTextTailGate.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextTailGate,
                    "Select tail gate status");
        }
        if (binding.editTextFrontFenderLeftHandSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextFrontFenderLeftHandSide,
                    "Select front fender left side status");
        }
        if (binding.editTextFrontDoorLeftHandSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextFrontDoorLeftHandSide,
                    "Select front door left side status");
        }
        if (binding.editTextBackDoorLeftHandSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBackDoorLeftHandSide,
                    "Select back door left side status");
        }
        if (binding.editTextBackFenderLeftHandSide.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBackFenderLeftHandSide,
                    "Select back fender left side status");
        }
        if (binding.spinnerWindShield.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerWindShield,
                    "Select wind shield status");
        }
        if (binding.spinnerPillars.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerPillars,
                    "Select pillars status");
        }
        if (binding.spinnerPillars.getSelectedItemPosition() > 1) {
            if (binding.editTextPillarType.getText().toString().trim().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextPillarType,
                        "Select pillar type");
            }
        }
        if (binding.spinnerMirrors.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerMirrors,
                    "Select mirrors status");
        }
        if (binding.spinnerHeadLightLH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerHeadLightLH,
                    "Select Head Light LH status");
        }
        if (binding.spinnerHeadLightRH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerHeadLightRH,
                    "Select Head Light RH status");
        }
        if (binding.spinnerTailLightLH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTailLightLH,
                    "Select Tail Light LH status");
        }
        if (binding.spinnerTailLightRH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTailLightRH,
                    "Select Tail Light RH status");
        }
        if (binding.spinnerDickyDoor.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerDickyDoor,
                    "Select Dicky Door status");
        }
        if (binding.spinnerFrontBumper.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerFrontBumper,
                    "Select Front Bumper status");
        }
        if (binding.editTextBackBumper.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextBackBumper,
                    "Select Back Bumper status");
        }
        if (binding.spinnerOutSideRearViewMirrorLH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerOutSideRearViewMirrorLH,
                    "Select OutSide RearView Mirror LH status");
        }
        if (binding.spinnerOutSideRearViewMirrorRH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerOutSideRearViewMirrorRH,
                    "Select OutSide RearView Mirror RH status");
        }
        if (binding.spinnerApronFrontLH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerApronFrontLH,
                    "Select Apron Front LH status");
        }
        if (binding.spinnerApronFrontRH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerApronFrontRH,
                    "Select Apron Front RH status");
        }
        if (binding.spinnerStrutMountingArea.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerStrutMountingArea,
                    "Select Strut Mounting Area status");
        }
        if (binding.spinnerFirewall.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerFirewall,
                    "Select Firewall status");
        }
        if (binding.spinnerRunningBoardLH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRunningBoardLH,
                    "Select Running Board LH status");
        }
        if (binding.spinnerRunningBoardRH.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRunningBoardRH,
                    "Select Running Board RH status");
        }
        saveData();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editTextFrontFenderRightSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextFrontFenderRightSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextFrontDoorRightSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextFrontDoorRightSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBackDoorRightSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBackDoorRightSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBackFenderRightSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBackFenderRightSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBonnet:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBonnet,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextTailGate:
                MultiSelectDialogHelper.showPicker(context, binding.editTextTailGate,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextFrontFenderLeftHandSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextFrontFenderLeftHandSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextFrontDoorLeftHandSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextFrontDoorLeftHandSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBackDoorLeftHandSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBackDoorLeftHandSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBackFenderLeftHandSide:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBackFenderLeftHandSide,
                        SpinnerConstants.mapCondition, "Select Condition/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextPillarType:
                MultiSelectDialogHelper.showPicker(context, binding.editTextPillarType,
                        SpinnerConstants.mapPillarType, "Choose Pillar Type/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
            case R.id.editTextBackBumper:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBackBumper,
                        SpinnerConstants.mapCondition, "Choose back Bumper value/s",
                        null, MultiSelectDialogHelper.COMMA);
                break;
        }
    }

    @Override
    public void fillTestDataInForm() {
        binding.editTextFrontFenderRightSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextFrontDoorRightSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextBackDoorRightSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextBackFenderRightSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextBonnet.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextTailGate.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextFrontFenderLeftHandSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextFrontDoorLeftHandSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextBackDoorLeftHandSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.editTextBackFenderLeftHandSide.setText(SpinnerConstants.mapCondition.get("2"));
        binding.spinnerWindShield.setSelection(randomNumGen(2, 1));
        binding.spinnerPillars.setSelection(randomNumGen(2, 1));
        binding.spinnerMirrors.setSelection(randomNumGen(2, 1));
        binding.spinnerHeadLightLH.setSelection(randomNumGen(2, 1));
        binding.spinnerHeadLightRH.setSelection(randomNumGen(2, 1));
        binding.spinnerTailLightLH.setSelection(randomNumGen(2, 1));
        binding.spinnerTailLightRH.setSelection(randomNumGen(2, 1));
        binding.spinnerDickyDoor.setSelection(randomNumGen(2, 1));
        binding.spinnerFrontBumper.setSelection(randomNumGen(2, 1));
        binding.editTextBackBumper.setText(SpinnerConstants.mapCondition.get("2"));
        binding.spinnerOutSideRearViewMirrorLH.setSelection(randomNumGen(2, 1));
        binding.spinnerOutSideRearViewMirrorRH.setSelection(randomNumGen(2, 1));
        binding.spinnerBodyShell.setSelection(randomNumGen(2, 1));
        binding.spinnerChassis.setSelection(randomNumGen(2, 1));
        binding.spinnerApronFrontLH.setSelection(randomNumGen(2, 1));
        binding.spinnerApronFrontRH.setSelection(randomNumGen(2, 1));
        binding.spinnerStrutMountingArea.setSelection(randomNumGen(2, 1));
        binding.spinnerFirewall.setSelection(randomNumGen(2, 1));
        binding.spinnerUpperCrossMember.setSelection(randomNumGen(2, 1));
        binding.spinnerLowerCrossMember.setSelection(randomNumGen(2, 1));
        binding.spinnerCowlTop.setSelection(randomNumGen(2, 1));
        binding.spinnerRadiator.setSelection(randomNumGen(2, 1));
        binding.spinnerEngineRoomCarrierAssembly.setSelection(randomNumGen(2, 1));
        binding.spinnerRunningBoardLH.setSelection(randomNumGen(2, 1));
        binding.spinnerRunningBoardRH.setSelection(randomNumGen(2, 1));
        binding.editTextPillarType.setText(SpinnerConstants.mapPillarType.get("A"));
    }
}