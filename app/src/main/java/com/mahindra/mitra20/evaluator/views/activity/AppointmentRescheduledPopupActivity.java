package com.mahindra.mitra20.evaluator.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.models.SessionUserDetails;

public class AppointmentRescheduledPopupActivity extends AppCompatActivity implements View.OnClickListener{

    TextView textViewdateTime,textViewContinue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_appoinment_reschedule_popup);

        init();
    }

    public  void init()
    {
        Intent i = getIntent();
        String strdate = i.getStringExtra("date");
        String strtime = i.getStringExtra("time");
        textViewdateTime = (TextView)findViewById(R.id.textViewdateTime);
        textViewdateTime.setText("to "+strdate +" at "+strtime);

        textViewContinue = (TextView)findViewById(R.id.textViewContinue);
        textViewContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewContinue:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(SessionUserDetails.getInstance().getUserType()== 0)
        {
            Intent i = new Intent(AppointmentRescheduledPopupActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
        else {
            //super.onBackPressed();
            Intent i = new Intent(AppointmentRescheduledPopupActivity.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}
