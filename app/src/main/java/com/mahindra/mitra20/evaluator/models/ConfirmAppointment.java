package com.mahindra.mitra20.evaluator.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 9/8/2018.
 */

public class ConfirmAppointment  implements Parcelable {
    String strRequestId;
    String Id;

    public ConfirmAppointment() {
    }

    protected ConfirmAppointment(Parcel in) {
        strRequestId = in.readString();
        Id = in.readString();
    }

    public static final Creator<ConfirmAppointment> CREATOR = new Creator<ConfirmAppointment>() {
        @Override
        public ConfirmAppointment createFromParcel(Parcel in) {
            return new ConfirmAppointment(in);
        }

        @Override
        public ConfirmAppointment[] newArray(int size) {
            return new ConfirmAppointment[size];
        }
    };

    public String getStrRequestId() {
        return strRequestId;
    }

    public void setStrRequestId(String strRequestId) {
        this.strRequestId = strRequestId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strRequestId);
        dest.writeString(Id);
    }
}
