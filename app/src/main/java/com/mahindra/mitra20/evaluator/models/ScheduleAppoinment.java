package com.mahindra.mitra20.evaluator.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 7/24/2018.
 */

public class ScheduleAppoinment implements Parcelable {

    private String strCustId;
    private String strMonth;
    private String strDate;
    private String strTime;
    private String strCustomerName;
    private String strVehicleName;
    private String strLocation;
    private String strContactNo;
    private String strMake;
    private String Pincode;
    private String RequestID;
    private String RequestStatus;
    private String strModel;
    private String Status;
    private String Occupation;
    private String OptedNewVehicle;
    private String PhoneNo;
    private String State;
    private String YearOfManufacturing;
    private String City;
    private String Email;
    private String Notes;
    private String Salesconsultantname;
    private String SalesconsultantContactnum;
    private int LeadPriority;
    private String enquiryLocation;
    private String enquirySource;
    private String enquiryType;

    protected ScheduleAppoinment(Parcel in) {
        strCustId = in.readString();
        strMonth = in.readString();
        strDate = in.readString();
        strTime = in.readString();
        strCustomerName = in.readString();
        strVehicleName = in.readString();
        strLocation = in.readString();
        strContactNo = in.readString();
        strMake = in.readString();
        Pincode = in.readString();
        RequestID = in.readString();
        RequestStatus = in.readString();
        strModel = in.readString();
        Status = in.readString();
        Occupation = in.readString();
        OptedNewVehicle = in.readString();
        PhoneNo = in.readString();
        State = in.readString();
        YearOfManufacturing = in.readString();
        City = in.readString();
        Email = in.readString();
        Notes = in.readString();
        Salesconsultantname = in.readString();
        SalesconsultantContactnum = in.readString();
        LeadPriority = in.readInt();
        enquiryLocation = in.readString();
        enquirySource = in.readString();
        enquiryType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strCustId);
        dest.writeString(strMonth);
        dest.writeString(strDate);
        dest.writeString(strTime);
        dest.writeString(strCustomerName);
        dest.writeString(strVehicleName);
        dest.writeString(strLocation);
        dest.writeString(strContactNo);
        dest.writeString(strMake);
        dest.writeString(Pincode);
        dest.writeString(RequestID);
        dest.writeString(RequestStatus);
        dest.writeString(strModel);
        dest.writeString(Status);
        dest.writeString(Occupation);
        dest.writeString(OptedNewVehicle);
        dest.writeString(PhoneNo);
        dest.writeString(State);
        dest.writeString(YearOfManufacturing);
        dest.writeString(City);
        dest.writeString(Email);
        dest.writeString(Notes);
        dest.writeString(Salesconsultantname);
        dest.writeString(SalesconsultantContactnum);
        dest.writeInt(LeadPriority);
        dest.writeString(enquiryLocation);
        dest.writeString(enquirySource);
        dest.writeString(enquiryType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScheduleAppoinment> CREATOR = new Creator<ScheduleAppoinment>() {
        @Override
        public ScheduleAppoinment createFromParcel(Parcel in) {
            return new ScheduleAppoinment(in);
        }

        @Override
        public ScheduleAppoinment[] newArray(int size) {
            return new ScheduleAppoinment[size];
        }
    };

    public String getSalesconsultantname() {
        return Salesconsultantname;
    }

    public void setSalesconsultantname(String salesconsultantname) {
        Salesconsultantname = salesconsultantname;
    }

    public String getSalesconsultantContactnum() {
        return SalesconsultantContactnum;
    }

    public void setSalesconsultantContactnum(String salesconsultantContactnum) {
        SalesconsultantContactnum = salesconsultantContactnum;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public ScheduleAppoinment() {
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getOptedNewVehicle() {
        return OptedNewVehicle;
    }

    public void setOptedNewVehicle(String optedNewVehicle) {
        OptedNewVehicle = optedNewVehicle;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getYearOfManufacturing() {
        return YearOfManufacturing;
    }

    public void setYearOfManufacturing(String yearOfManufacturing) {
        YearOfManufacturing = yearOfManufacturing;
    }

    public String getStrCustId() {
        return strCustId;
    }

    public void setStrCustId(String strCustId) {
        this.strCustId = strCustId;
    }

    public String getStrMonth() {
        return strMonth;
    }

    public void setStrMonth(String strMonth) {
        this.strMonth = strMonth;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    public String getStrCustomerName() {
        return strCustomerName;
    }

    public void setStrCustomerName(String strCustomerName) {
        this.strCustomerName = strCustomerName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public void setStrVehicleName(String strVehicleName) {
        this.strVehicleName = strVehicleName;
    }

    public String getStrLocation() {
        return strLocation;
    }

    public void setStrLocation(String strLocation) {
        this.strLocation = strLocation;
    }

    public String getStrContactNo() {
        return strContactNo;
    }

    public void setStrContactNo(String strContactNo) {
        this.strContactNo = strContactNo;
    }

    public String getStrMake() {
        return strMake;
    }

    public void setStrMake(String strMake) {
        this.strMake = strMake;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getRequestStatus() {
        return RequestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        RequestStatus = requestStatus;
    }

    public String getStrModel() {
        return strModel;
    }

    public void setStrModel(String strModel) {
        this.strModel = strModel;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getLeadPriority() {
        return LeadPriority;
    }

    public void setLeadPriority(int leadPriority) {
        LeadPriority = leadPriority;
    }

    public String getEnquiryLocation() {
        return enquiryLocation;
    }

    public void setEnquiryLocation(String enquiryLocation) {
        this.enquiryLocation = enquiryLocation;
    }

    public String getEnquirySource() {
        return enquirySource;
    }

    public void setEnquirySource(String enquirySource) {
        this.enquirySource = enquirySource;
    }

    public String getEnquiryType() {
        return enquiryType;
    }

    public void setEnquiryType(String enquiryType) {
        this.enquiryType = enquiryType;
    }
}