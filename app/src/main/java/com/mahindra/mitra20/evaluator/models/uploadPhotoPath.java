package com.mahindra.mitra20.evaluator.models;

/**
 * Created by BADHAP-CONT on 8/28/2018.
 */

public class uploadPhotoPath {
    String docType;
    String photoPath;

    public uploadPhotoPath(String docType, String photoPath) {
        this.docType = docType;
        this.photoPath = photoPath;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }
}
