package com.mahindra.mitra20.evaluator.views.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.databinding.LayoutCompletedEvaluationBinding;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.fragment.ViewEvaluationFragment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

public class CompletedEvaluationActivity extends AppCompatActivity {

    LayoutCompletedEvaluationBinding binding;
    EvaluatorHelper evaluatorHelper;
    String requestId = "";
    NewEvaluation newEvaluation;

    String evaluatorId, draftEvaluationId;
    CommonHelper commonHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_completed_evaluation);
        init();
    }

    private void init() {
        evaluatorId = SessionUserDetails.getInstance().getUserID();
        CommonHelper.settingCustomToolBarEvaluator(this, "Completed Evaluation",
                View.INVISIBLE);
        commonHelper = new CommonHelper();
        evaluatorHelper = new EvaluatorHelper(getApplicationContext());

        Intent intent = getIntent();
        draftEvaluationId = intent.getStringExtra("NewEvaluationId");
        getFromDraft();
    }

    void getFromDraft() {
        newEvaluation = evaluatorHelper.getEvaluationDetailsById(requestId, draftEvaluationId);

        if (newEvaluation.getRequestId() != null || newEvaluation.getId() != null) {
            binding.textViewCustomeName.setText(newEvaluation.getOwnerName());
            binding.textViewVehicleNameTop.setText(String.format("%s %s",
                    newEvaluation.getMake(), newEvaluation.getModel()));

            Bundle bundle = new Bundle();
            bundle.putParcelable(CommonHelper.EXTRA_EVALUATION, newEvaluation);
            bundle.putBoolean(CommonHelper.EXTRA_ARE_IMAGES_ONLINE, false);
            ViewEvaluationFragment viewEvaluationFragment =
                    new ViewEvaluationFragment();
            viewEvaluationFragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(binding.fragmentContainer.getId(),
                    viewEvaluationFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SessionUserDetails.getInstance().getUserType() == 0) {
            Intent i = new Intent(CompletedEvaluationActivity.this,
                    ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(CompletedEvaluationActivity.this,
                    EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}