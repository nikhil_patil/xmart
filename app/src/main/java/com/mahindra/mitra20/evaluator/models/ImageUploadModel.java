package com.mahindra.mitra20.evaluator.models;

/**
 * Created by BADHAP-CONT on 9/11/2018.
 */

public class ImageUploadModel {

    private String docType;
    private String imagePath;
    private String evaluationReportId;
    private String documentName;

    public ImageUploadModel(String docType, String imagePath, String evaluationReportId,
                            String documentName) {
        this.docType = docType;
        this.imagePath = imagePath;
        this.evaluationReportId = evaluationReportId;
        this.documentName = documentName;
    }

    public String getDocType() {
        return docType;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getEvaluationReportId() {
        return evaluationReportId;
    }

    public String getDocumentName() {
        return documentName;
    }
}
