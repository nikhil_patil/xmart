package com.mahindra.mitra20.evaluator.presenter;

import android.os.Bundle;

/**
 * Created by user on 8/3/2018.
 */

public interface OpenFragmentIn {

    public void openFragmentByID(int id, Bundle bundle);
    public void openFragmentByID(int id);
}
