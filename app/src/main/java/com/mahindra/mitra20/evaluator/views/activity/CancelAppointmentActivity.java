package com.mahindra.mitra20.evaluator.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.mahindra.mitra20.R;

public class CancelAppointmentActivity extends  AppCompatActivity implements View.OnClickListener{

    ImageView imageViewClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cancel_appointment);

        imageViewClose = (ImageView)findViewById(R.id.imageViewClose);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imageViewClose:

                break;
        }
    }
}
