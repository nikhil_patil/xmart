package com.mahindra.mitra20.evaluator.presenter.evalform;

import android.content.Context;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

import java.util.ArrayList;

/**
 * Created by PATINIK-CONT on 30/12/2019.
 */

public class VehicleDetailsPresenter {

    private Context context;
    private MasterDatabaseHelper masterDatabaseHelper;

    public VehicleDetailsPresenter(Context context) {
        this.context = context;
        this.masterDatabaseHelper = new MasterDatabaseHelper(context);
    }

    public ArrayList<MasterModel> getColors() {
        return masterDatabaseHelper.getColors();
    }

    public ArrayList<MasterModel> getMakeMaster() {
        return masterDatabaseHelper.getMakeMaster();
    }

    public ArrayList<MasterModel> getModelMaster(String strCode) {
        return masterDatabaseHelper.getModelMaster(strCode);
    }

    public ArrayList<MasterModel> getVariantMaster(String strCode) {
        return masterDatabaseHelper.getVariantMaster(strCode);
    }

    public void saveData(NewEvaluation newEvaluation) {
        new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                EvaluatorConstants.EvaluationParts.VEHICLE_DETAILS);
    }

    public NewEvaluation getEvaluationData(String evaluationId) {
        return new NewEvaluationHelper(context).getEvaluationDetails(evaluationId,
                EvaluatorConstants.EvaluationParts.VEHICLE_DETAILS);
    }
}