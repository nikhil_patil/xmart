package com.mahindra.mitra20.evaluator.models;

/**
 * Created by BADHAP-CONT on 8/13/2018.
 */

public class MasterModel {
    private String Code;
    private String Description;
    private String parentType;

    public String getParentType() {
        return parentType;
    }

    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public MasterModel() {
    }

    public MasterModel(String code, String description) {

        Code = code;
        Description = description;
    }

    @Override
    public String toString() {
        return  Description;
    }
}
