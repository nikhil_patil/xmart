package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.LayoutAccessoriesBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.AccessoriesPresenter;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

import static com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity.randomNumGen;

public class AccessoriesFragment extends Fragment implements CommonMethods, View.OnClickListener {

    LayoutAccessoriesBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private AccessoriesPresenter accessoriesPresenterPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutAccessoriesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        accessoriesPresenterPresenter = new AccessoriesPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = accessoriesPresenterPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        binding.buttonStereoYes.setOnClickListener(this);
        binding.buttonStereoNo.setOnClickListener(this);
        binding.buttonReverseParkingYes.setOnClickListener(this);
        binding.buttonReverseParkingNo.setOnClickListener(this);
        binding.buttonPowerSteeringYes.setOnClickListener(this);
        binding.buttonPowerSteeringNo.setOnClickListener(this);
        binding.buttonAlloyWheelsYes.setOnClickListener(this);
        binding.buttonAlloyWheelsNo.setOnClickListener(this);
        binding.buttonFogLampYes.setOnClickListener(this);
        binding.buttonFogLampNo.setOnClickListener(this);
        binding.buttonCentralLockingYes.setOnClickListener(this);
        binding.buttonCentralLockingNo.setOnClickListener(this);
        binding.buttonToolKitJackYes.setOnClickListener(this);
        binding.buttonToolKitJackNo.setOnClickListener(this);
        binding.buttonRearWiperYes.setOnClickListener(this);
        binding.buttonRearWiperNo.setOnClickListener(this);
        binding.buttonMusicSystemYes.setOnClickListener(this);
        binding.buttonMusicSystemNo.setOnClickListener(this);
        binding.buttonFMRadioYes.setOnClickListener(this);
        binding.buttonFMRadioNo.setOnClickListener(this);
        binding.buttonPowerWindowYes.setOnClickListener(this);
        binding.buttonPowerWindowNo.setOnClickListener(this);
        binding.buttonACYes.setOnClickListener(this);
        binding.buttonACNo.setOnClickListener(this);
        binding.buttonPowerAdjustableSeatsYes.setOnClickListener(this);
        binding.buttonPowerAdjustableSeatsNo.setOnClickListener(this);
        binding.buttonKeyleassEntryYes.setOnClickListener(this);
        binding.buttonKeyleassEntryNo.setOnClickListener(this);
        binding.buttonPedalShifterYes.setOnClickListener(this);
        binding.buttonPedalShifterNo.setOnClickListener(this);
        binding.buttonElectricallyAdjustableORVMYes.setOnClickListener(this);
        binding.buttonElectricallyAdjustableORVMNo.setOnClickListener(this);
        binding.buttonRearDefoggerYes.setOnClickListener(this);
        binding.buttonRearDefoggerNo.setOnClickListener(this);
        binding.buttonSunRoofYes.setOnClickListener(this);
        binding.buttonSunRoofNo.setOnClickListener(this);
        binding.buttonAutoClimateTechnologiesYes.setOnClickListener(this);
        binding.buttonAutoClimateTechnologiesNo.setOnClickListener(this);
        binding.buttonLeatherSeatsYes.setOnClickListener(this);
        binding.buttonLeatherSeatsNo.setOnClickListener(this);
        binding.buttonBluetoothAudioSystemsYes.setOnClickListener(this);
        binding.buttonBluetoothAudioSystemsNo.setOnClickListener(this);
        binding.buttonGPSNavigationSystemsYes.setOnClickListener(this);
        binding.buttonGPSNavigationSystemsNo.setOnClickListener(this);
        binding.editTextPowerWindowType.setOnClickListener(this);
    }

    @Override
    public void displayData() {
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                newEvaluation.getNoOfKeys(), binding.spinnerNoOfKeys);
        setUpToggleButtons(newEvaluation.getStereo(), new Button[]{binding.buttonStereoYes,
                        binding.buttonStereoNo});
        setUpToggleButtons(newEvaluation.getReverseParking(), new Button[]{binding.buttonReverseParkingYes,
                        binding.buttonReverseParkingNo});
        setUpToggleButtons(newEvaluation.getPowerSteering(), new Button[]{binding.buttonPowerSteeringYes,
                        binding.buttonPowerSteeringNo});
        setUpToggleButtons(newEvaluation.getAlloyWheels(), new Button[]{binding.buttonAlloyWheelsYes,
                        binding.buttonAlloyWheelsNo});
        setUpToggleButtons(newEvaluation.getFogLamp(), new Button[]{binding.buttonFogLampYes,
                        binding.buttonFogLampNo});
        setUpToggleButtons(newEvaluation.getCentralLocking(), new Button[]{binding.buttonCentralLockingYes,
                        binding.buttonCentralLockingNo});
        setUpToggleButtons(newEvaluation.getToolKitJack(), new Button[]{binding.buttonToolKitJackYes,
                        binding.buttonToolKitJackNo});
        binding.editTextComment.setText(newEvaluation.getComment());
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                newEvaluation.getTubelessTyres(), binding.spinnerTubelessTyres);
        setUpToggleButtons(newEvaluation.getRearWiper(), new Button[]{binding.buttonRearWiperYes,
                        binding.buttonRearWiperNo});
        setUpToggleButtons(newEvaluation.getMusicSystem(), new Button[]{binding.buttonMusicSystemYes,
                        binding.buttonMusicSystemNo});
        setUpToggleButtons(newEvaluation.getFMRadio(), new Button[]{binding.buttonFMRadioYes,
                        binding.buttonFMRadioNo});
        setUpToggleButtons(newEvaluation.getPowerWindow(), new Button[]{binding.buttonPowerWindowYes,
                        binding.buttonPowerWindowNo});
        binding.editTextPowerWindowType.setText(
                MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getPowerWindowType(),
                        SpinnerConstants.mapPowerWindowType, MultiSelectDialogHelper.COMMA));
        setUpToggleButtons(newEvaluation.getAC(), new Button[]{binding.buttonACYes,
                        binding.buttonACNo});
        setUpToggleButtons(newEvaluation.getPowerAdjustableSeats(),
                new Button[]{binding.buttonPowerAdjustableSeatsYes,
                        binding.buttonPowerAdjustableSeatsNo});
        setUpToggleButtons(newEvaluation.getKeyLessEntry(),
                new Button[]{binding.buttonKeyleassEntryYes,
                        binding.buttonKeyleassEntryNo});
        setUpToggleButtons(newEvaluation.getPedalShifter(),
                new Button[]{binding.buttonPedalShifterYes,
                        binding.buttonPedalShifterNo});
        setUpToggleButtons(newEvaluation.getElectricallyAdjustableORVM(),
                new Button[]{binding.buttonElectricallyAdjustableORVMYes,
                        binding.buttonElectricallyAdjustableORVMNo});
        setUpToggleButtons(newEvaluation.getRearDefogger(), new Button[]{binding.buttonRearDefoggerYes,
                        binding.buttonRearDefoggerNo});
        setUpToggleButtons(newEvaluation.getSunRoof(), new Button[]{binding.buttonSunRoofYes,
                        binding.buttonSunRoofNo});
        setUpToggleButtons(newEvaluation.getAutoClimateTechnologies(),
                new Button[]{binding.buttonAutoClimateTechnologiesYes,
                        binding.buttonAutoClimateTechnologiesNo});
        setUpToggleButtons(newEvaluation.getLeatherSeats(),
                new Button[]{binding.buttonLeatherSeatsYes,
                        binding.buttonLeatherSeatsNo});
        setUpToggleButtons(newEvaluation.getBluetoothAudioSystems(),
                new Button[]{binding.buttonBluetoothAudioSystemsYes,
                        binding.buttonBluetoothAudioSystemsNo});
        setUpToggleButtons(newEvaluation.getGPSNavigationSystems(),
                new Button[]{binding.buttonGPSNavigationSystemsYes,
                        binding.buttonGPSNavigationSystemsNo});
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setNoOfKeys(
                    binding.spinnerNoOfKeys.getSelectedItem().toString().equals("Yes") ? "Y" : "N");
            newEvaluation.setComment(binding.editTextComment.getText().toString().trim());
            newEvaluation.setPowerWindowType(MultiSelectDialogHelper.getKeysFromValues(
                    binding.editTextPowerWindowType.getText().toString().trim(),
                    SpinnerConstants.mapPowerWindowType, MultiSelectDialogHelper.COMMA));
            newEvaluation.setTubelessTyres(
                    binding.spinnerTubelessTyres.getSelectedItem().toString().equals("Yes") ? "Y" : "N");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        accessoriesPresenterPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (null != newEvaluation.getStereo() && newEvaluation.getStereo().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select stereo");
        }
        if (binding.spinnerNoOfKeys.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerNoOfKeys,
                    "Select number of keys");
        }
        if (null != newEvaluation.getReverseParking() && newEvaluation.getReverseParking().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select reverse parking");
        }
        if (null != newEvaluation.getPowerSteering() && newEvaluation.getPowerSteering().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select power steering");
        }
        if (null != newEvaluation.getAlloyWheels() && newEvaluation.getAlloyWheels().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select alloy wheels");
        }
        if (null != newEvaluation.getFogLamp() && newEvaluation.getFogLamp().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select fog lamp");
        }
        if (null != newEvaluation.getCentralLocking() && newEvaluation.getCentralLocking().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select central locking");
        }
        if (null != newEvaluation.getToolKitJack() && newEvaluation.getToolKitJack().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select Tool kit and Jack");
        }
        if (binding.spinnerTubelessTyres.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTubelessTyres,
                    "Select tubeless tyre");
        }
        if (null != newEvaluation.getRearWiper() && newEvaluation.getRearWiper().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select rear wiper");
        }
        if (null != newEvaluation.getMusicSystem() && newEvaluation.getMusicSystem().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select music system");
        }
        if (null != newEvaluation.getFMRadio() && newEvaluation.getFMRadio().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select fm radio");
        }
        if (null != newEvaluation.getPowerWindow() && newEvaluation.getPowerWindow().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select power window");
        }
        if (null != newEvaluation.getPowerWindow() && newEvaluation.getPowerWindow().equalsIgnoreCase("YES")) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextPowerWindowType,
                    "Select power window types");
        }
        if (null != newEvaluation.getAC() && newEvaluation.getAC().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select AC");
        }
        if (null != newEvaluation.getPowerAdjustableSeats() && newEvaluation.getPowerAdjustableSeats().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select adjustable seats");
        }
        if (null != newEvaluation.getKeyLessEntry() && newEvaluation.getKeyLessEntry().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select keyless entry");
        }
        if (null != newEvaluation.getPedalShifter() && newEvaluation.getPedalShifter().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select paddle shifter");
        }
        if (null != newEvaluation.getElectricallyAdjustableORVM() && newEvaluation.getElectricallyAdjustableORVM().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select electrically adjustable ORVM");
        }
        if (null != newEvaluation.getRearDefogger() && newEvaluation.getRearDefogger().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select defogger");
        }
        if (null != newEvaluation.getSunRoof() && newEvaluation.getSunRoof().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select sun roof");
        }
        if (null != newEvaluation.getAutoClimateTechnologies() && newEvaluation.getAutoClimateTechnologies().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select auto climate technologies");
        }
        if (null != newEvaluation.getLeatherSeats() && newEvaluation.getLeatherSeats().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select leather seats");
        }
        if (null != newEvaluation.getBluetoothAudioSystems() && newEvaluation.getBluetoothAudioSystems().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select bluetooth audio systems");
        }
        if (null != newEvaluation.getGPSNavigationSystems() && newEvaluation.getGPSNavigationSystems().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context,null,
                    "Select navigation systems");
        }
        saveData();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonStereoYes:
            case R.id.buttonStereoNo:
                if (v.getId() == R.id.buttonStereoYes) {
                    newEvaluation.setStereo("Y");
                } else {
                    newEvaluation.setStereo("N");
                }
                setUpToggleButtons(newEvaluation.getStereo(),
                        new Button[]{binding.buttonStereoYes,
                                binding.buttonStereoNo});
                break;
            case R.id.buttonReverseParkingYes:
            case R.id.buttonReverseParkingNo:
                if (v.getId() == R.id.buttonReverseParkingYes) {
                    newEvaluation.setReverseParking("Y");
                } else {
                    newEvaluation.setReverseParking("N");
                }
                setUpToggleButtons(newEvaluation.getReverseParking(),
                        new Button[]{binding.buttonReverseParkingYes,
                                binding.buttonReverseParkingNo});
                break;
            case R.id.buttonPowerSteeringYes:
            case R.id.buttonPowerSteeringNo:
                if (v.getId() == R.id.buttonPowerSteeringYes) {
                    newEvaluation.setPowerSteering("Y");
                } else {
                    newEvaluation.setPowerSteering("N");
                }
                setUpToggleButtons(newEvaluation.getPowerSteering(),
                        new Button[]{binding.buttonPowerSteeringYes,
                                binding.buttonPowerSteeringNo});
                break;
            case R.id.buttonAlloyWheelsYes:
            case R.id.buttonAlloyWheelsNo:
                if (v.getId() == R.id.buttonAlloyWheelsYes) {
                    newEvaluation.setAlloyWheels("Y");
                } else {
                    newEvaluation.setAlloyWheels("N");
                }
                setUpToggleButtons(newEvaluation.getAlloyWheels(),
                        new Button[]{binding.buttonAlloyWheelsYes,
                                binding.buttonAlloyWheelsNo});
                break;
            case R.id.buttonFogLampYes:
            case R.id.buttonFogLampNo:
                if (v.getId() == R.id.buttonFogLampYes) {
                    newEvaluation.setFogLamp("Y");
                } else {
                    newEvaluation.setFogLamp("N");
                }
                setUpToggleButtons(newEvaluation.getFogLamp(),
                        new Button[]{binding.buttonFogLampYes,
                                binding.buttonFogLampNo});
                break;
            case R.id.buttonCentralLockingYes:
            case R.id.buttonCentralLockingNo:
                if (v.getId() == R.id.buttonCentralLockingYes) {
                    newEvaluation.setCentralLocking("Y");
                } else {
                    newEvaluation.setCentralLocking("N");
                }
                setUpToggleButtons(newEvaluation.getCentralLocking(),
                        new Button[]{binding.buttonCentralLockingYes,
                                binding.buttonCentralLockingNo});
                break;
            case R.id.buttonToolKitJackYes:
            case R.id.buttonToolKitJackNo:
                if (v.getId() == R.id.buttonToolKitJackYes) {
                    newEvaluation.setToolKitJack("Y");
                } else {
                    newEvaluation.setToolKitJack("N");
                }
                setUpToggleButtons(newEvaluation.getToolKitJack(),
                        new Button[]{binding.buttonToolKitJackYes,
                                binding.buttonToolKitJackNo});
                break;
            case R.id.buttonRearWiperYes:
            case R.id.buttonRearWiperNo:
                if (v.getId() == R.id.buttonRearWiperYes) {
                    newEvaluation.setRearWiper("Y");
                } else {
                    newEvaluation.setRearWiper("N");
                }
                setUpToggleButtons(newEvaluation.getRearWiper(),
                        new Button[]{binding.buttonRearWiperYes,
                                binding.buttonRearWiperNo});
                break;
            case R.id.buttonMusicSystemYes:
            case R.id.buttonMusicSystemNo:
                if (v.getId() == R.id.buttonMusicSystemYes) {
                    newEvaluation.setMusicSystem("Y");
                } else {
                    newEvaluation.setMusicSystem("N");
                }
                setUpToggleButtons(newEvaluation.getMusicSystem(),
                        new Button[]{binding.buttonMusicSystemYes,
                                binding.buttonMusicSystemNo});
                break;
            case R.id.buttonFMRadioYes:
            case R.id.buttonFMRadioNo:
                if (v.getId() == R.id.buttonFMRadioYes) {
                    newEvaluation.setFMRadio("Y");
                } else {
                    newEvaluation.setFMRadio("N");
                }
                setUpToggleButtons(newEvaluation.getFMRadio(),
                        new Button[]{binding.buttonFMRadioYes,
                                binding.buttonFMRadioNo});
                break;
            case R.id.buttonPowerWindowYes:
            case R.id.buttonPowerWindowNo:
                if (v.getId() == R.id.buttonPowerWindowYes) {
                    newEvaluation.setPowerWindow("Y");
                    binding.llPowerWindowType.setVisibility(View.VISIBLE);
                } else {
                    newEvaluation.setPowerWindow("N");
                    binding.llPowerWindowType.setVisibility(View.GONE);
                }
                setUpToggleButtons(newEvaluation.getPowerWindow(),
                        new Button[]{binding.buttonPowerWindowYes,
                                binding.buttonPowerWindowNo});
                break;
            case R.id.buttonACYes:
            case R.id.buttonACNo:
                if (v.getId() == R.id.buttonACYes) {
                    newEvaluation.setAC("Y");
                } else {
                    newEvaluation.setAC("N");
                }
                setUpToggleButtons(newEvaluation.getAC(),
                        new Button[]{binding.buttonACYes,
                                binding.buttonACNo});
                break;
            case R.id.buttonPowerAdjustableSeatsYes:
            case R.id.buttonPowerAdjustableSeatsNo:
                if (v.getId() == R.id.buttonPowerAdjustableSeatsYes) {
                    newEvaluation.setPowerAdjustableSeats("Y");
                } else {
                    newEvaluation.setPowerAdjustableSeats("N");
                }
                setUpToggleButtons(newEvaluation.getPowerAdjustableSeats(),
                        new Button[]{binding.buttonPowerAdjustableSeatsYes,
                                binding.buttonPowerAdjustableSeatsNo});
                break;
            case R.id.buttonKeyleassEntryYes:
            case R.id.buttonKeyleassEntryNo:
                if (v.getId() == R.id.buttonKeyleassEntryYes) {
                    newEvaluation.setKeyLessEntry("Y");
                } else {
                    newEvaluation.setKeyLessEntry("N");
                }
                setUpToggleButtons(newEvaluation.getKeyLessEntry(),
                        new Button[]{binding.buttonKeyleassEntryYes,
                                binding.buttonKeyleassEntryNo});
                break;
            case R.id.buttonPedalShifterYes:
            case R.id.buttonPedalShifterNo:
                if (v.getId() == R.id.buttonPedalShifterYes) {
                    newEvaluation.setPedalShifter("Y");
                } else {
                    newEvaluation.setPedalShifter("N");
                }
                setUpToggleButtons(newEvaluation.getPedalShifter(),
                        new Button[]{binding.buttonPedalShifterYes,
                                binding.buttonPedalShifterNo});
                break;
            case R.id.buttonElectricallyAdjustableORVMYes:
            case R.id.buttonElectricallyAdjustableORVMNo:
                if (v.getId() == R.id.buttonElectricallyAdjustableORVMYes) {
                    newEvaluation.setElectricallyAdjustableORVM("Y");
                } else {
                    newEvaluation.setElectricallyAdjustableORVM("N");
                }
                setUpToggleButtons(newEvaluation.getElectricallyAdjustableORVM(),
                        new Button[]{binding.buttonElectricallyAdjustableORVMYes,
                                binding.buttonElectricallyAdjustableORVMNo});
                break;
            case R.id.buttonRearDefoggerYes:
            case R.id.buttonRearDefoggerNo:
                if (v.getId() == R.id.buttonRearDefoggerYes) {
                    newEvaluation.setRearDefogger("Y");
                } else {
                    newEvaluation.setRearDefogger("N");
                }
                setUpToggleButtons(newEvaluation.getRearDefogger(),
                        new Button[]{binding.buttonRearDefoggerYes,
                                binding.buttonRearDefoggerNo});
                break;
            case R.id.buttonSunRoofYes:
            case R.id.buttonSunRoofNo:
                if (v.getId() == R.id.buttonSunRoofYes) {
                    newEvaluation.setSunRoof("Y");
                } else {
                    newEvaluation.setSunRoof("N");
                }
                setUpToggleButtons(newEvaluation.getSunRoof(),
                        new Button[]{binding.buttonSunRoofYes,
                                binding.buttonSunRoofNo});
                break;
            case R.id.buttonAutoClimateTechnologiesYes:
            case R.id.buttonAutoClimateTechnologiesNo:
                if (v.getId() == R.id.buttonAutoClimateTechnologiesYes) {
                    newEvaluation.setAutoClimateTechnologies("Y");
                } else {
                    newEvaluation.setAutoClimateTechnologies("N");
                }
                setUpToggleButtons(newEvaluation.getAutoClimateTechnologies(),
                        new Button[]{binding.buttonAutoClimateTechnologiesYes,
                                binding.buttonAutoClimateTechnologiesNo});
                break;
            case R.id.buttonLeatherSeatsYes:
            case R.id.buttonLeatherSeatsNo:
                if (v.getId() == R.id.buttonLeatherSeatsYes) {
                    newEvaluation.setLeatherSeats("Y");
                } else {
                    newEvaluation.setLeatherSeats("N");
                }
                setUpToggleButtons(newEvaluation.getLeatherSeats(),
                        new Button[]{binding.buttonLeatherSeatsYes,
                                binding.buttonLeatherSeatsNo});
                break;
            case R.id.buttonBluetoothAudioSystemsYes:
            case R.id.buttonBluetoothAudioSystemsNo:
                if (v.getId() == R.id.buttonBluetoothAudioSystemsYes) {
                    newEvaluation.setBluetoothAudioSystems("Y");
                } else {
                    newEvaluation.setBluetoothAudioSystems("N");
                }
                setUpToggleButtons(newEvaluation.getBluetoothAudioSystems(),
                        new Button[]{binding.buttonBluetoothAudioSystemsYes,
                                binding.buttonBluetoothAudioSystemsNo});
                break;
            case R.id.buttonGPSNavigationSystemsYes:
            case R.id.buttonGPSNavigationSystemsNo:
                if (v.getId() == R.id.buttonGPSNavigationSystemsYes) {
                    newEvaluation.setGPSNavigationSystems("Y");
                } else {
                    newEvaluation.setGPSNavigationSystems("N");
                }
                setUpToggleButtons(newEvaluation.getGPSNavigationSystems(),
                        new Button[]{binding.buttonGPSNavigationSystemsYes,
                                binding.buttonGPSNavigationSystemsNo});
                break;
            case R.id.editTextPowerWindowType:
                MultiSelectDialogHelper.showPicker(context,
                        binding.editTextPowerWindowType, SpinnerConstants.mapPowerWindowType,
                        "Choose Power Window Type/s", null, MultiSelectDialogHelper.COMMA);
                break;
        }
    }
    
    private void setUpToggleButtons(String value, Button[] btns) {
        int highlightPos = -1, dimPos = -1;
        if ((value.equalsIgnoreCase("No")) ||
                (value.equalsIgnoreCase("N"))) {
            highlightPos = 0;
            dimPos = 1;
        } else if ((value.equalsIgnoreCase("Yes")) ||
                (value.equalsIgnoreCase("Y"))) {
            highlightPos = 1;
            dimPos = 0;
        }

        if (highlightPos != -1 && dimPos != -1) {
            btns[highlightPos].setTextColor(getResources().getColor(R.color.colorblack));
            btns[highlightPos].setBackgroundResource(R.drawable.disable_edit_text_border);

            btns[dimPos].setTextColor(getResources().getColor(R.color.colorWhite));
            btns[dimPos].setBackgroundResource(R.drawable.gray_rounded_bg);
        }
    }

    @Override
    public void fillTestDataInForm() {
        binding.buttonStereoYes.performClick();
        binding.spinnerNoOfKeys.setSelection(randomNumGen(2, 1));
        binding.buttonReverseParkingYes.performClick();
        binding.buttonPowerSteeringYes.performClick();
        binding.buttonAlloyWheelsYes.performClick();
        binding.buttonFogLampYes.performClick();
        binding.buttonCentralLockingYes.performClick();
        binding.buttonToolKitJackNo.performClick();
        binding.spinnerTubelessTyres.setSelection(randomNumGen(2, 1));
        binding.buttonRearWiperNo.performClick();
        binding.buttonMusicSystemYes.performClick();
        binding.buttonFMRadioNo.performClick();
        binding.buttonPowerWindowYes.performClick();
        binding.llPowerWindowType.setVisibility(View.VISIBLE);
        binding.editTextPowerWindowType.setText(SpinnerConstants.mapPowerWindowType.get("FRNTRH"));
        binding.buttonACYes.performClick();
        binding.buttonPowerAdjustableSeatsNo.performClick();
        binding.buttonKeyleassEntryYes.performClick();
        binding.buttonPedalShifterNo.performClick();
        binding.buttonElectricallyAdjustableORVMYes.performClick();
        binding.buttonRearDefoggerNo.performClick();
        binding.buttonSunRoofYes.performClick();
        binding.buttonAutoClimateTechnologiesYes.performClick();
        binding.buttonLeatherSeatsNo.performClick();
        binding.buttonBluetoothAudioSystemsYes.performClick();
        binding.buttonGPSNavigationSystemsYes.performClick();
    }
}