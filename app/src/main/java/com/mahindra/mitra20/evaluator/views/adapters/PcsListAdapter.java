package com.mahindra.mitra20.evaluator.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;

import java.util.ArrayList;
import java.util.Date;

public class PcsListAdapter extends RecyclerView.Adapter<PcsListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ScheduleAppoinment> scheduleAppoinment;
    private DateHelper dateHelper;

    public PcsListAdapter(Context context, ArrayList<ScheduleAppoinment> scheduleAppoinment, DateHelper dateHelper) {
        this.context = context;
        this.scheduleAppoinment = scheduleAppoinment;
        this.dateHelper = dateHelper;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_item_pcs_status, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String strDate = scheduleAppoinment.get(i).getStrDate();
        Date date= dateHelper.convertStringToDate(strDate);
        String userName=scheduleAppoinment.get(i).getStrCustomerName();
        String  month=dateHelper.getMonthName(date);
        String mdate =dateHelper.getDate(date);
        String time = dateHelper.convertTwentyFourHourToTweleveHour(scheduleAppoinment.get(i).getStrTime());
        String salesPersonName = scheduleAppoinment.get(i).getSalesconsultantname();
        String vehicalName = scheduleAppoinment.get(i).getStrVehicleName();
        String location = scheduleAppoinment.get(i).getStrLocation();

        viewHolder.userNametextView.setText(userName);
        viewHolder.monthtextView.setText(month);
        viewHolder.datetextView.setText(mdate);
        viewHolder.timetextView.setText(time);
        viewHolder.salesPersonNametextView.setText(salesPersonName);
        viewHolder.vehicalNametextView.setText(vehicalName);
        viewHolder.locationtextView.setText(location);
    }

    @Override
    public int getItemCount() {
        return scheduleAppoinment.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView monthtextView;
        private TextView datetextView;
        private TextView timetextView;
        private TextView vehicalNametextView;
        private TextView locationtextView;
        private TextView salesPersonNametextView;
        private TextView userNametextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userNametextView=itemView.findViewById(R.id.textViewCustomerName);
            monthtextView = itemView.findViewById(R.id.textViewMonth);
            datetextView = itemView.findViewById(R.id.textViewDate);
            timetextView = itemView.findViewById(R.id.textViewTime);
            vehicalNametextView = itemView.findViewById(R.id.textViewVehicleName);
            locationtextView = itemView.findViewById(R.id.textViewLocation);
            salesPersonNametextView = itemView.findViewById(R.id.textViewSalesPersonName);
        }
    }
}
