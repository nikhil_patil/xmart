package com.mahindra.mitra20.evaluator.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.ViewHelper;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.views.activity.AppoinmentDetailsActivity;
import com.mahindra.mitra20.evaluator.views.activity.ScheduleAppoinmentActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class EvaluationOverdueAdapter extends RecyclerView.Adapter<EvaluationOverdueAdapter.ViewHolder> {
    private ArrayList<ScheduleAppoinment> scheduleAppoinment;
    private Context context;
    private String type;
    private DateHelper dateHelper;
    private String strTime = "";
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private String where;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    public void updateList(ArrayList<ScheduleAppoinment> scheduleAppoinments) {
        this.scheduleAppoinment = scheduleAppoinments;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewBackground, textViewMonth, textViewDate, textViewTime, tvEnquirySource,
                textViewCustomerName, textViewVehicleName, textViewLocation, tvEnquiryType,
                textViewSalesPersonName, textViewSalesPersonContactNo, textViewLeadPriority;
        LinearLayout linearLayoutOverdueVisits;
        public View layout;
        public ImageView imageViewCall, imageViewMessage;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewMonth = v.findViewById(R.id.textViewMonth);
            textViewDate = v.findViewById(R.id.textViewDate);
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewBackground = v.findViewById(R.id.textViewBackground);
            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewMessage = v.findViewById(R.id.imageViewMessage);
            linearLayoutOverdueVisits = v.findViewById(R.id.linearLayoutOverdueVisits);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);

            textViewSalesPersonName = v.findViewById(R.id.textViewSalesPersonName);
            textViewSalesPersonContactNo = v.findViewById(R.id.textViewSalesPersonContactNo);
        }
    }

    public EvaluationOverdueAdapter(Context _context, ArrayList<ScheduleAppoinment> _scheduleAppoinment, String _type, String _where) {
        scheduleAppoinment = _scheduleAppoinment;
        context = _context;
        type = _type;
        dateHelper = new DateHelper();
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);
        where = _where;

        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    @Override
    public EvaluationOverdueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_overdue_visits, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String strDate = scheduleAppoinment.get(position).getStrDate();
        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        String strMonth = dateHelper.getMonthName(appointmentDate);
        String strAppointmentDate = dateHelper.getDate(appointmentDate);
        if (!scheduleAppoinment.get(position).getStrTime().equals("")) {
            strTime = dateHelper.convertTwentyFourHourToTweleveHour(scheduleAppoinment.get(position).getStrTime());
        }

        holder.textViewMonth.setText(strMonth);
        holder.textViewDate.setText(strAppointmentDate);
        holder.textViewTime.setText(strTime);
        holder.textViewCustomerName.setText(scheduleAppoinment.get(position).getStrCustomerName());
        holder.textViewVehicleName.setText(scheduleAppoinment.get(position).getStrVehicleName());

        holder.textViewSalesPersonName.setText(scheduleAppoinment.get(position).getSalesconsultantname());
        holder.textViewSalesPersonContactNo.setText(scheduleAppoinment.get(position).getSalesconsultantContactnum());
        ViewHelper.populateLeadView(context, holder.textViewLeadPriority,
                scheduleAppoinment.get(position).getLeadPriority());

        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, scheduleAppoinment.get(position).getStrMake(),
                scheduleAppoinment.get(position).getStrModel());
        String strMake = makeModel.first;
        String strModel = makeModel.second;

        holder.textViewVehicleName.setText(String.format("%s %s", strMake, strModel));
        holder.textViewLocation.setText(scheduleAppoinment.get(position).getStrLocation());
        String enqSource = scheduleAppoinment.get(position).getEnquirySource();
        if (null != enqSource)
            holder.tvEnquirySource.setText(String.format("Source - %s", enqSource));
        String enqType = scheduleAppoinment.get(position).getEnquiryType();
        if (null != enqType)
            holder.tvEnquiryType.setText(String.format("Type - %s", enqType));

        holder.linearLayoutOverdueVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (where.equals("Dashboard")) {
                    if (masterDatabaseHelper.isMasterSync()) {
                        Intent intentScheduleAppoinment = new Intent(context, ScheduleAppoinmentActivity.class);
                        intentScheduleAppoinment.putExtra("appoinmentType", "Pending");
                        intentScheduleAppoinment.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intentScheduleAppoinment);
//                        ((Activity) context).finish();
                    } else {
                        CommonHelper.toast(R.string.master_sync_request, context);
                    }
                } else {
                    if (masterDatabaseHelper.isMasterSync()) {
                        ScheduleAppoinment scheduleAppoinmet = scheduleAppoinment.get(position);
                        Intent intent = new Intent(context, AppoinmentDetailsActivity.class);
                        intent.putExtra("scheduleAppoinment", scheduleAppoinmet);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
//                        ((Activity) context).finish();
                    } else {
                        CommonHelper.toast(R.string.master_sync_request, context);
                    }
                }
            }
        });

        holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new com.mahindra.mitra20.champion.helper.DialogsHelper((Activity) context).callDialog(scheduleAppoinment.get(position).getStrContactNo());
            }
        });

        holder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.
                        sendMessage(scheduleAppoinment.get(position).getStrContactNo(), context);
            }
        });

        switch (type) {
            case "Overdue":
            case "Draft":
                holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorOrange));
                break;
            case "Completed":
                holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
                break;
            case "Upcoming":
                holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return scheduleAppoinment.size();
    }
}