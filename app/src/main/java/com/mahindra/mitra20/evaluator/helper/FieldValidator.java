package com.mahindra.mitra20.evaluator.helper;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mahindra.mitra20.helper.CommonHelper;

/**
 * Created by PATINIK-CONT on 7/12/2018.
 */

public class FieldValidator {
    /**
     * Checks if edit text is empty. if empty opens particular section containing that edit
     * text also display error message
     *
     * @param context       activity context for toast
     * @param header        header of the section to be opened
     * @param isBodyVisible is editText containing section visible
     * @param editText      edit text to be validated
     * @param toastMessage  error message
     * @return
     */
    public static boolean validateEmptyText(Context context, View header, boolean isBodyVisible,
                                            EditText editText, String toastMessage) {
        if (!isBodyVisible) {
            header.performClick();
        }
        editText.requestFocus();
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Same as above only TextView is used in place of EditText
     *
     * @param context
     * @param header
     * @param isBodyVisible
     * @param textView
     * @param toastMessage
     * @return
     */
    public static boolean validateEmptyText(Context context, View header, boolean isBodyVisible,
                                            TextView textView, String toastMessage) {
        if (!isBodyVisible) {
            header.performClick();
        }
        textView.requestFocus();
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Checks if spinner has selected. if empty opens particular section containing that spinner
     * also display error message
     *
     * @param context       activity context for toast
     * @param header        header of the section to be opened
     * @param isBodyVisible is spinner containing section visible
     * @param view          view to be validated
     * @param toastMessage  error message
     * @return
     */
    public static boolean validateSelection(Context context, View header, boolean isBodyVisible,
                                            View view, String toastMessage) {
        if (!isBodyVisible) {
            header.performClick();
        }
        view.performClick();
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Checks if spinner has selected. if empty opens particular section containing that spinner
     * also display error message
     *
     * @param context       activity context for toast
     * @param header        header of the section to be opened
     * @param isBodyVisible is spinner containing section visible
     * @param view          view to be selected
     * @param toastMessage  error message
     * @return
     */
    public static boolean validateToggleSelection(Context context, View header, boolean isBodyVisible,
                                                  View view, String toastMessage) {
        if (!isBodyVisible) {
            header.performClick();
        }
        if (null != view)
            view.requestFocus();
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Checks if spinner has selected. if empty opens particular section containing that spinner
     * also display error message
     *
     * @param context       activity context for toast
     * @param header        header of the section to be opened
     * @param isBodyVisible is spinner containing section visible
     * @param header        view to be validated
     * @param toastMessage  error message
     * @return
     */
    public static boolean validateImageSelection(Context context, View header, boolean isBodyVisible,
                                                 String toastMessage) {
        if (!isBodyVisible) {
            header.performClick();
        }
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Checks if edit text is empty. if empty display error message
     *
     * @param context      activity context for toast
     * @param view         edit text to be validated
     * @param toastMessage error message
     * @return false always
     */
    public static boolean focusViewAndDisplayMessage(Context context, View view, String toastMessage) {
        if (null != view)
            view.requestFocus();
        CommonHelper.toast(toastMessage, context);
        return false;
    }

    /**
     * Checks if spinner has selected. if empty opens spinner and display error message
     *
     * @param context      activity context for toast
     * @param view         view to be validated
     * @param toastMessage error message
     * @return false always
     */
    public static boolean openViewAndDisplayMessage(Context context, View view, String toastMessage) {
        view.performClick();
        CommonHelper.toast(toastMessage, context);
        return false;
    }


    public static boolean isEmpty(Context context, String value, String errorMsg) {
        if (TextUtils.isEmpty(value)) {
            CommonHelper.toast_long(errorMsg, context);
            return true;
        }
        return false;
    }
}