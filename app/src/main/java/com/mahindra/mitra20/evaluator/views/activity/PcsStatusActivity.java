package com.mahindra.mitra20.evaluator.views.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.views.adapters.PcsListAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import java.util.ArrayList;

public class PcsStatusActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PcsListAdapter pcsListAdapter;
    EvaluatorHelper evaluatorHelper = new EvaluatorHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcs_status);
        CommonHelper.settingCustomToolBar(this, "Procurement Status");
        recyclerView=findViewById(R.id.recyclerViewPcsList);
        pcsListAdapter= new PcsListAdapter(this, evaluatorHelper.getPcsScheduledAppointment(), new DateHelper());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(pcsListAdapter);
    }
}
