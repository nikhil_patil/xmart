package com.mahindra.mitra20.evaluator.views.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DialogsHelper;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AppoinmentDetailsActivity extends AppCompatActivity implements View.OnClickListener, VolleySingleTon.VolleyInteractor, OnMapReadyCallback {

    private TextView textViewCustomerName, textViewVehicleName, textViewDate, textViewTime, textViewAddress,
            textViewMobileNo, textViewNameOfConsultant, textViewEnquiryLocation;
    private Button button_confirm_appoinment, button_reschedule_appoinment, buttonCancelAppointment;
    private String strOwnerName, strMobileNo, strVehicleName, strDate, strMonth, strAppointmentDate = "",
            strYear = "", strTime, strLocation, strRequestId, strMake = "", strModel = "";
    private ImageView imageViewCall, imageViewCallConsultant;
    private String appointmentStatus = "CNF";
    private String evaluatorId = "";
    private Dialog dialog;
    private ScheduleAppoinment scheduleAppoinment;
    private DateHelper dateHelper;
    private DialogsHelper dialogsHelper;
    private Context context;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private GoogleMap map;
    private String strTimeText;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();
    private String strNotes = "";
    private EditText editTextNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_appoinment_details_activity);

        context = getApplicationContext();
        dateHelper = new DateHelper();
        dialogsHelper = new DialogsHelper(this);
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);

        evaluatorId = SessionUserDetails.getInstance().getUserID();

        Intent intent = getIntent();
        scheduleAppoinment = intent.getParcelableExtra("scheduleAppoinment");
        strRequestId = scheduleAppoinment.getRequestID();
        strOwnerName = scheduleAppoinment.getStrCustomerName();
        strMobileNo = scheduleAppoinment.getStrContactNo();
        strVehicleName = scheduleAppoinment.getStrVehicleName();
        strDate = scheduleAppoinment.getStrDate();
        strMonth = scheduleAppoinment.getStrMonth();
        strTime = scheduleAppoinment.getStrTime();
        strLocation = scheduleAppoinment.getStrLocation();
        strMake = scheduleAppoinment.getStrMake();
        strModel = scheduleAppoinment.getStrModel();
        scheduleAppoinment.getStrModel();

        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        strMonth = dateHelper.getMonthName(appointmentDate);
        strAppointmentDate = dateHelper.getDate(appointmentDate);
        strYear = dateHelper.getYear(appointmentDate);
        strTimeText = dateHelper.convertTwentyFourHourToTweleveHour(scheduleAppoinment.getStrTime());
        init();
    }

    public void init() {

        CommonHelper.settingCustomToolBarEvaluator(this, "Appointment Details", View.INVISIBLE);

        textViewCustomerName = findViewById(R.id.textViewCustomerName);
        textViewVehicleName = findViewById(R.id.textViewVehicleName);
        textViewDate = findViewById(R.id.textViewDate);
        textViewTime = findViewById(R.id.textViewTime);
        textViewAddress = findViewById(R.id.textViewAddress);
        textViewMobileNo = findViewById(R.id.textViewMobileNo);
        button_confirm_appoinment = findViewById(R.id.button_confirm_appoinment);
        button_reschedule_appoinment = findViewById(R.id.button_reschedule_appoinment);
        buttonCancelAppointment = findViewById(R.id.buttonCancelAppointment);
        imageViewCall = findViewById(R.id.imageViewCall);

        textViewNameOfConsultant = findViewById(R.id.textViewNameOfConsultant);
        textViewEnquiryLocation = findViewById(R.id.textViewEnquiryLocation);
        imageViewCallConsultant = findViewById(R.id.imageViewCallConsultant);
        editTextNotes = findViewById(R.id.editTextNotes);

        button_confirm_appoinment.setOnClickListener(this);
        button_reschedule_appoinment.setOnClickListener(this);
        buttonCancelAppointment.setOnClickListener(this);

        textViewCustomerName = findViewById(R.id.textViewCustomerName);
        textViewVehicleName = findViewById(R.id.textViewVehicleName);
        textViewMobileNo = findViewById(R.id.textViewMobileNo);
        textViewDate = findViewById(R.id.textViewDate);
        textViewTime = findViewById(R.id.textViewTime);
        textViewAddress = findViewById(R.id.textViewAddress);

        textViewCustomerName.setText(strOwnerName);
        textViewMobileNo.setText(strMobileNo);
        textViewDate.setText(String.format("%s %s %s", strAppointmentDate, strMonth, strYear));
        textViewTime.setText(strTimeText);
        textViewAddress.setText(strLocation);
        textViewNameOfConsultant.setText(scheduleAppoinment.getSalesconsultantname());
        textViewEnquiryLocation.setText(scheduleAppoinment.getEnquiryLocation());

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogsHelper.callDialog(strMobileNo);
            }
        });

        imageViewCallConsultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogsHelper.callDialog(scheduleAppoinment.getSalesconsultantContactnum());
            }
        });

        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());

        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, strMake, strModel);
        String strMake1 = makeModel.first;
        String strModel1 = makeModel.second;

        textViewVehicleName.setText(strMake1 + " " + strModel1);
        // Getting Reference to SupportMapFragment of activity_map.xml
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (evaluatorHelper.isAppointmentConfirmed(strRequestId) > 0) {
            button_confirm_appoinment.setVisibility(View.GONE);
            button_reschedule_appoinment.setVisibility(View.GONE);
        } else {
            button_confirm_appoinment.setVisibility(View.VISIBLE);
            button_reschedule_appoinment.setVisibility(View.VISIBLE);
        }

        strNotes = evaluatorHelper.getScheduleAppointmentNotes(strRequestId);
        editTextNotes.setText(strNotes);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        getLatLongFromAddress(strLocation);
    }

    private void getLatLongFromAddress(String fullAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(fullAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                LatLng latLng = new LatLng(latitude, longitude);
                map.addMarker(new MarkerOptions().position(latLng)
                        .title(strLocation));
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                map.animateCamera(CameraUpdateFactory.zoomTo(15), 200, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        updateNotes();
        switch (v.getId()) {
            case R.id.button_confirm_appoinment:
                //appointmentConfirmPresenter.confirmAppointment(strRequestId, evaluatorId, appointmentStatus, strDate+" "+strTime);
                //startActivity(new Intent(this, AppointmentConfirmedActivity.class));
                String userId1 = SessionUserDetails.getInstance().getUserID();
                if (userId1.contains(".")) {
                    String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                    evaluatorId = arrUserId[0].toUpperCase();
                } else {
                    evaluatorId = userId1;
                }
                try {
                    HashMap<String, String> params = new HashMap<>();
                    params.put(WebConstants.appointmentConfirm.REQUEST_ID, strRequestId); //"ENQ19A000142"
                    params.put(WebConstants.appointmentConfirm.EVALUATIOR_ID, evaluatorId);// "MV010107");//evaluatorId
                    params.put(WebConstants.appointmentConfirm.APPOINTMENT_STATUS, appointmentStatus);// "CNF");//appointmentStatus
                    params.put(WebConstants.appointmentConfirm.APPOINTMENT_DATE_TIME, strDate + " " + strTime);//"06/08/2018 12:00:00");//appointmentDateTime
                    params.put(WebConstants.appointmentConfirm.CANCELLATION_REASON_ID, "");
                    JSONObject jsonObject = new JSONObject(params);
                    VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.appointmentConfirm.URL, jsonObject, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.buttonCancelAppointment:
                dialogsHelper.showCancelAppointmentDialog(2, strRequestId, strNotes);
                break;
            case R.id.button_reschedule_appoinment:

                Intent intentReschedule = new Intent(AppoinmentDetailsActivity.this, RescheduleAppointmentActivity.class);
                intentReschedule.putExtra("scheduleAppoinment", scheduleAppoinment);
                startActivity(intentReschedule);
                finish();
                break;
            case R.id.imageViewClose:
                dialog.dismiss();
                break;
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case 1:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    if (IsSuccessful.equals("1")) {
                        CommonHelper.toast(message, getApplicationContext());
                        evaluatorHelper.confirmedAppointment(strRequestId);
                        startActivity(new Intent(AppoinmentDetailsActivity.this, AppointmentConfirmedActivity.class));
                        finish();
                    } else {
                        CommonHelper.toast(message, getApplicationContext());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                CommonHelper.toast(R.string.reschedule_appointment_successfully, context);
                context.startActivity(new Intent(context, EvaluatorDashboardActivity.class));
                finish();
            case 3:
                CommonHelper.toast(R.string.cancel_appointment_successfully, context);

                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        switch (requestId) {
            case 1:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    if (IsSuccessful.equals("1")) {
                        evaluatorHelper.updateScheduleAppointment(strRequestId, strDate, strTime);
                        CommonHelper.toast(message, getApplicationContext());
                        startActivity(new Intent(AppoinmentDetailsActivity.this, AppointmentRescheduledPopupActivity.class).putExtra("date", textViewDate.getText().toString()).putExtra("time", textViewTime.getText().toString()));
                        finish();
                    } else {
                        CommonHelper.toast(message, getApplicationContext());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void updateNotes() {
        strNotes = editTextNotes.getText().toString().trim();
        evaluatorHelper.updateNoteForAppointment(strRequestId, strNotes);
    }

    @Override
    public void onBackPressed() {
        updateNotes();
        if (SessionUserDetails.getInstance().getUserType() == 0) {
            Intent i = new Intent(AppoinmentDetailsActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(ChampionConstants.FROM_APPOINTMENT_DETAILS_SCREEN, true);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(AppoinmentDetailsActivity.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(EvaluatorConstants.FROM_APPOINTMENT_DETAILS_SCREEN, true);
            startActivity(i);
            finish();
        }
    }
}
