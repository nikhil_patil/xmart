package com.mahindra.mitra20.evaluator.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 8/24/2018.
 */

public class CancelAppointmentModel implements Parcelable {

    String strCustId;
    String strMonth;
    String strDate;
    String strTime;
    String strCustomerName;
    String strVehicleName;
    String strLocation;
    String strContactNo;
    String strMake;
    String Pincode;
    String RequestID;
    String RequestStatus;
    String strModel;
    String Status;
    String strCancel;


    public CancelAppointmentModel() {
    }

    protected CancelAppointmentModel(Parcel in) {
        strCustId = in.readString();
        strMonth = in.readString();
        strDate = in.readString();
        strTime = in.readString();
        strCustomerName = in.readString();
        strVehicleName = in.readString();
        strLocation = in.readString();
        strContactNo = in.readString();
        strMake = in.readString();
        Pincode = in.readString();
        RequestID = in.readString();
        RequestStatus = in.readString();
        strModel = in.readString();

        Status = in.readString();
        strCancel = in.readString();
    }

    public static final Creator<CancelAppointmentModel> CREATOR = new Creator<CancelAppointmentModel>() {
        @Override
        public CancelAppointmentModel createFromParcel(Parcel in) {
            return new CancelAppointmentModel(in);
        }

        @Override
        public CancelAppointmentModel[] newArray(int size) {
            return new CancelAppointmentModel[size];
        }
    };

    public String getStrCustId() {
        return strCustId;
    }

    public void setStrCustId(String strCustId) {
        this.strCustId = strCustId;
    }

    public String getStrMonth() {
        return strMonth;
    }

    public void setStrMonth(String strMonth) {
        this.strMonth = strMonth;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    public String getStrCustomerName() {
        return strCustomerName;
    }

    public void setStrCustomerName(String strCustomerName) {
        this.strCustomerName = strCustomerName;
    }

    public String getStrVehicleName() {
        return strVehicleName;
    }

    public void setStrVehicleName(String strVehicleName) {
        this.strVehicleName = strVehicleName;
    }

    public String getStrLocation() {
        return strLocation;
    }

    public void setStrLocation(String strLocation) {
        this.strLocation = strLocation;
    }

    public String getStrContactNo() {
        return strContactNo;
    }

    public void setStrContactNo(String strContactNo) {
        this.strContactNo = strContactNo;
    }

    public String getStrMake() {
        return strMake;
    }

    public void setStrMake(String strMake) {
        this.strMake = strMake;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getRequestStatus() {
        return RequestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        RequestStatus = requestStatus;
    }

    public String getStrModel() {
        return strModel;
    }

    public void setStrModel(String strModel) {
        this.strModel = strModel;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStrCancel() {
        return strCancel;
    }

    public void setStrCancel(String strCancel) {
        this.strCancel = strCancel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strCustId);
        dest.writeString(strMonth);
        dest.writeString(strDate);
        dest.writeString(strTime);
        dest.writeString(strCustomerName);
        dest.writeString(strVehicleName);
        dest.writeString(strLocation);
        dest.writeString(strContactNo);
        dest.writeString(strMake);
        dest.writeString(Pincode);
        dest.writeString(RequestID);
        dest.writeString(RequestStatus);
        dest.writeString(strModel);
        dest.writeString(Status);
        dest.writeString(strCancel);
    }
}
