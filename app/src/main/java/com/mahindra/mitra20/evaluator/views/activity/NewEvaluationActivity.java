package com.mahindra.mitra20.evaluator.views.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.security.ProviderInstaller;
import com.mahindra.mitra20.BuildConfig;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.models.VehiclePhoto;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.databinding.ActivityNewEvaluationBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.evaluatorVolley.VolleyMultipartRequest;
import com.mahindra.mitra20.evaluator.helper.BitmapHelper;
import com.mahindra.mitra20.evaluator.interfaces.CustomStepperListener;
import com.mahindra.mitra20.evaluator.models.ImageUploadModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.models.Stepper;
import com.mahindra.mitra20.evaluator.presenter.NewEvaluationActivityIn;
import com.mahindra.mitra20.evaluator.presenter.NewEvaluationActivityPresenter;
import com.mahindra.mitra20.evaluator.presenter.evalform.ImagesPresenter;
import com.mahindra.mitra20.evaluator.views.adapters.CenterLayoutManager;
import com.mahindra.mitra20.evaluator.views.adapters.CustomStepperAdapter;
import com.mahindra.mitra20.evaluator.views.fragment.AccessoriesFragment;
import com.mahindra.mitra20.evaluator.views.fragment.AggregateDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.AudioAndFeedbackRatingFragment;
import com.mahindra.mitra20.evaluator.views.fragment.ExteriorDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.ImagesFragment;
import com.mahindra.mitra20.evaluator.views.fragment.InteriorDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.OwnerDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.VehicleDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.VehicleEvaluationDetailsFragment;
import com.mahindra.mitra20.evaluator.views.fragment.WheelsAndTyresFragment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.utils.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.SSLContext;

import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.ACCESSORIES;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.AGGREGATE_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.AUDIO_AND_OVERALL_FEEDBACK;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.EXTERIOR;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.IMAGES;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.INTERIOR;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.OWNER_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.VEHICLE_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.VEHICLE_EVALUATION_DETAILS;
import static com.mahindra.mitra20.evaluator.constants.EvaluatorConstants.EvaluationParts.WHEELS_AND_TYRES;

public class NewEvaluationActivity extends BaseActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener,
        GoogleApiClient.OnConnectionFailedListener, NewEvaluationActivityIn,
        View.OnLongClickListener, CustomStepperListener {

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 201;
    private static final int REQUEST_LOCATION_PERMISSION = 203;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private final String TAG = "LOCATION";
    public static final String KEY_EDIT_EVAL = "EditEval";
    private ArrayList<ImageUploadModel> arrImageUpload = new ArrayList<>();
    private int successfulImageUploadDoc = 0, j = -1;

    private ActivityNewEvaluationBinding binding;
    private NewEvaluationActivityPresenter presenter;
    private EvaluatorHelper evaluatorHelper;
    private ProgressDialog dialog;
    private Context context;

    String EvaluationReportID = "", evaluatorId = "";
    NewEvaluation newEvaluation;
    private ScheduleAppoinment scheduleAppoinment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location currentLocation;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    String type = "";

    private CustomStepperAdapter adapter;
    private List<Stepper> stepperList;
    private RecyclerView.LayoutManager layoutManager;
    private Fragment currentFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_evaluation);
        presenter = new NewEvaluationActivityPresenter(this);
        init();

        trackLocation();
    }

    void trackLocation() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000)        // 5 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
        mGoogleApiClient.connect();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                try {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            Log.i(TAG, "All location settings are satisfied.");
                            if (ActivityCompat.checkSelfPermission(NewEvaluationActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) !=
                                    PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(NewEvaluationActivity.this,
                                            Manifest.permission.ACCESS_COARSE_LOCATION)
                                            != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(
                                        NewEvaluationActivity.this, PERMISSIONS, REQUEST_LOCATION_PERMISSION);
                                return;
                            }
                            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                                    mLocationRequest, NewEvaluationActivity.this);
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                            try {
                                status.startResolutionForResult(NewEvaluationActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                Log.i(TAG, "PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void init() {
        initAudioSettings();
        CommonHelper.settingCustomToolBarEvaluator(this, "New Evaluation", View.VISIBLE);
        context = getApplicationContext();
        evaluatorHelper = new EvaluatorHelper(this);
        try {
            String draftEvaluationId = "";
            Intent intent = getIntent();
            type = intent.getStringExtra("status");
            if (null != type && type.equals(KEY_EDIT_EVAL)) {
                newEvaluation = intent.getParcelableExtra("NewEvaluation");
            } else {
                draftEvaluationId = intent.getStringExtra("NewEvaluationId");
                scheduleAppoinment = intent.getParcelableExtra("scheduleAppoinment");
                String requestId = (null != scheduleAppoinment && null != scheduleAppoinment.getRequestID()) ?
                        ((scheduleAppoinment.getRequestID().equals("")) ? "" : scheduleAppoinment.getRequestID()) : "";
                if (!requestId.isEmpty() || (null != draftEvaluationId && !draftEvaluationId.isEmpty()))
                    newEvaluation = evaluatorHelper.getEvaluationDetailsById(requestId, draftEvaluationId);
                if (null == type) {
                    type = "";
                }
            }
            if (null == newEvaluation) {
                newEvaluation = new NewEvaluation();
                newEvaluation.setId(draftEvaluationId);
                newEvaluation.setNewObject(true);
                newEvaluation.initializeFields();
            } else if (newEvaluation.getId().isEmpty()) {
                newEvaluation.setId(draftEvaluationId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        evaluatorId = SessionUserDetails.getInstance().getUserID();
        try {
            String userId1 = SessionUserDetails.getInstance().getUserID();
            if (userId1.contains(".")) {
                String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                evaluatorId = arrUserId[0];
            } else {
                evaluatorId = userId1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != scheduleAppoinment) {
//            newEvaluation.setNewObject(true);
//            newEvaluation.initializeImages();
            binding.textViewCustomerName.setVisibility(View.VISIBLE);
            newEvaluation.setRequestId(scheduleAppoinment.getRequestID());
            newEvaluation.setOwnerName(scheduleAppoinment.getStrCustomerName());
            newEvaluation.setMobileNo(scheduleAppoinment.getStrContactNo());
            newEvaluation.setAddress(scheduleAppoinment.getStrLocation());
            newEvaluation.setOccupation(scheduleAppoinment.getOccupation());
            newEvaluation.setOptedNewVehicle(scheduleAppoinment.getOptedNewVehicle());
            newEvaluation.setPhoneNo(scheduleAppoinment.getPhoneNo());
            newEvaluation.setState(scheduleAppoinment.getState());
            newEvaluation.setCity(scheduleAppoinment.getCity());
            newEvaluation.setPinCode(scheduleAppoinment.getPincode());
            newEvaluation.setEmailId(scheduleAppoinment.getEmail());
            newEvaluation.setDate(scheduleAppoinment.getStrDate());
            newEvaluation.setTime(scheduleAppoinment.getStrTime());
            newEvaluation.setMake(scheduleAppoinment.getStrMake());
            newEvaluation.setModel(scheduleAppoinment.getStrModel());
            presenter.saveApptEvaluationDraft(newEvaluation);
        }

        if (newEvaluation.getOwnerName().isEmpty()) {
            binding.textViewCustomerName.setVisibility(View.GONE);
        } else {
            binding.textViewCustomerName.setText(newEvaluation.getOwnerName());
        }

        stepperList = presenter.getStepperList();
        binding.btnNext.setOnClickListener(this);
        binding.btnPrevious.setOnClickListener(this);
        if (BuildConfig.DEBUG)
            binding.btnNext.setOnLongClickListener(this);

        replaceFragment(VEHICLE_EVALUATION_DETAILS, true);

        setupRecyclerView();
    }

    private void replaceFragment(int pos, boolean isAdd) {
        CommonHelper.hideKeyboard(this);
        if (!isAdd) {
            binding.btnNext.setEnabled(false);
            saveCurrentFragmentData();
            binding.btnNext.setEnabled(true);
        }
        Bundle bundle = new Bundle();
        bundle.putString(EvaluatorConstants.EXTRA_EVAL_ID, newEvaluation.getId());
        bundle.putString("type", type);
        Fragment fragment;
        String fragmentTag;
        switch (pos) {
            case OWNER_DETAILS:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.OWNER_DETAILS;
                fragment = new OwnerDetailsFragment();
                break;
            case VEHICLE_DETAILS:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.VEHICLE_DETAILS;
                fragment = new VehicleDetailsFragment();
                break;
            case ACCESSORIES:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.ACCESSORIES;
                fragment = new AccessoriesFragment();
                break;
            case INTERIOR:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.INTERIOR;
                fragment = new InteriorDetailsFragment();
                break;
            case EXTERIOR:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.EXTERIOR;
                fragment = new ExteriorDetailsFragment();
                break;
            case WHEELS_AND_TYRES:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.WHEELS_AND_TYRES;
                fragment = new WheelsAndTyresFragment();
                break;
            case AGGREGATE_DETAILS:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.AGGREGATE_DETAILS;
                fragment = new AggregateDetailsFragment();
                break;
            case IMAGES:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.IMAGES;
                fragment = new ImagesFragment();
                break;
            case AUDIO_AND_OVERALL_FEEDBACK:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.AUDIO_AND_OVERALL_FEEDBACK;
                fragment = new AudioAndFeedbackRatingFragment();
                break;
            case VEHICLE_EVALUATION_DETAILS:
            default:
                fragmentTag = EvaluatorConstants.EvaluationFragmentTags.VEHICLE_EVALUATION_DETAILS;
                fragment = new VehicleEvaluationDetailsFragment();
                break;
        }
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (isAdd)
            fragmentManager.beginTransaction().add(binding.fragmentContainer.getId(), fragment,
                    fragmentTag).commit();
        else
            fragmentManager.beginTransaction().replace(binding.fragmentContainer.getId(), fragment,
                    fragmentTag).commit();
        setCurrentFragment(fragment);
    }

    private void initAudioSettings() {
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
    }

    private void setupRecyclerView() {
        layoutManager = new CenterLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        binding.rvStepper.setLayoutManager(layoutManager);

        adapter = new CustomStepperAdapter(stepperList, this);
        adapter.setCustomStepperListener(this);
        binding.rvStepper.setAdapter(adapter);

        binding.tvTitle.setText(stepperList.get(0).getTitle());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (adapter.getCurrentPosition() == (adapter.getItemCount() - 1)) {
                    validateEvaluationDataSectionwise();
                } else {
                    adapter.nextStep();
                }
                break;
            case R.id.btn_previous:
                adapter.previousStep();
                break;
        }
    }

    //    private boolean validateEvaluationDataSectionwise() {
//        Fragment fragment = getVisibleFragment();
//        if (fragment != null && fragment.getTag() != null) {
//            switch (fragment.getTag()) {
//                case EvaluatorConstants.EvaluationFragmentTags.VEHICLE_EVALUATION_DETAILS:
//                    return ((VehicleEvaluationDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.OWNER_DETAILS:
//                    return ((OwnerDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.VEHICLE_DETAILS:
//                    return ((VehicleDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.ACCESSORIES:
//                    return ((AccessoriesFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.INTERIOR:
//                    return ((InteriorDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.EXTERIOR:
//                    return ((ExteriorDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.WHEELS_AND_TYRES:
//                    return ((WheelsAndTyresFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.AGGREGATE_DETAILS:
//                    return ((AggregateDetailsFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.IMAGES:
//                    return ((ImagesFragment) fragment).isDataValid();
//                case EvaluatorConstants.EvaluationFragmentTags.AUDIO_AND_OVERALL_FEEDBACK:
//                    return ((AudioAndFeedbackRatingFragment) fragment).isDataValid();
//            }
//        }
//        return false;
//    }
    private void validateEvaluationDataSectionwise() {
        newEvaluation = presenter.getEvaluationObject(context, newEvaluation.getId());
        if (!presenter.validateVehicleEvaluation(newEvaluation)) {
            adapter.setActiveState(VEHICLE_EVALUATION_DETAILS);
            binding.rvStepper.smoothScrollToPosition(VEHICLE_EVALUATION_DETAILS);
        } else if (!presenter.validateOwnerDetails(newEvaluation)) {
            adapter.setActiveState(OWNER_DETAILS);
            binding.rvStepper.smoothScrollToPosition(OWNER_DETAILS);
        } else if (!presenter.validateVehicleDetails(newEvaluation)) {
            adapter.setActiveState(VEHICLE_DETAILS);
            binding.rvStepper.smoothScrollToPosition(VEHICLE_DETAILS);
        } else if (!presenter.validateAccessoriesDetails(newEvaluation)) {
            adapter.setActiveState(ACCESSORIES);
            binding.rvStepper.smoothScrollToPosition(ACCESSORIES);
        } else if (!presenter.validateInteriorDetails(newEvaluation)) {
            adapter.setActiveState(INTERIOR);
            binding.rvStepper.smoothScrollToPosition(INTERIOR);
        } else if (!presenter.validateExteriorDetails(newEvaluation)) {
            adapter.setActiveState(EXTERIOR);
            binding.rvStepper.smoothScrollToPosition(EXTERIOR);
        } else if (!presenter.validateWheelsAndTyresDetails(newEvaluation)) {
            adapter.setActiveState(WHEELS_AND_TYRES);
            binding.rvStepper.smoothScrollToPosition(WHEELS_AND_TYRES);
        } else if (!presenter.validateAggregateDetails(newEvaluation)) {
            adapter.setActiveState(AGGREGATE_DETAILS);
            binding.rvStepper.smoothScrollToPosition(AGGREGATE_DETAILS);
        } else if (!presenter.validateImagesDetails(newEvaluation)) {
            adapter.setActiveState(IMAGES);
            binding.rvStepper.smoothScrollToPosition(IMAGES);
        } else {
            if (currentFragment instanceof AudioAndFeedbackRatingFragment) {

                ((AudioAndFeedbackRatingFragment) currentFragment).saveData();
                newEvaluation = presenter.getEvaluationObject(context, newEvaluation.getId());

                if (!presenter.validateAudioAndFeedbackDetails(newEvaluation)) {
                    adapter.setActiveState(AUDIO_AND_OVERALL_FEEDBACK);
                    binding.rvStepper.smoothScrollToPosition(AUDIO_AND_OVERALL_FEEDBACK);
                } else {
                    submitData();
                }
            }

        }
    }

    public static int randomNumGen(int max, int min) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public void saveDraft() {
        try {
            saveCurrentFragmentData();
            CommonHelper.toast("Saved in draft", context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        navigateToAppropriateActivity();
    }

    private void saveCurrentFragmentData() {
        switch (adapter.getCurrentPosition()) {
            case VEHICLE_EVALUATION_DETAILS:
                ((VehicleEvaluationDetailsFragment) currentFragment).saveData();
                break;
            case OWNER_DETAILS:
                ((OwnerDetailsFragment) currentFragment).saveData();
                break;
            case VEHICLE_DETAILS:
                ((VehicleDetailsFragment) currentFragment).saveData();
                break;
            case ACCESSORIES:
                ((AccessoriesFragment) currentFragment).saveData();
                break;
            case INTERIOR:
                ((InteriorDetailsFragment) currentFragment).saveData();
                break;
            case EXTERIOR:
                ((ExteriorDetailsFragment) currentFragment).saveData();
                break;
            case WHEELS_AND_TYRES:
                ((WheelsAndTyresFragment) currentFragment).saveData();
                break;
            case AGGREGATE_DETAILS:
                ((AggregateDetailsFragment) currentFragment).saveData();
                break;
            case IMAGES:
                ((ImagesFragment) currentFragment).saveData();
                break;
            case EvaluationParts.AUDIO_AND_OVERALL_FEEDBACK:
                ((AudioAndFeedbackRatingFragment) currentFragment).saveData();
                break;
        }
    }

    private void navigateToAppropriateActivity() {
        Intent i;
        if (SessionUserDetails.getInstance().getUserType() == 0) {
            i = new Intent(NewEvaluationActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(ChampionConstants.FROM_NEW_EVALUTION_SCREEN, true);
            i.putExtra(ChampionConstants.IS_DRAFT_SAVED, true);
        } else {
            if (type.equals("newEvaluationWithoutEnquiry")) {
                i = new Intent(NewEvaluationActivity.this, EvaluatorDashboardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(ChampionConstants.FROM_NEW_EVALUTION_SCREEN, true);
                i.putExtra(ChampionConstants.IS_DRAFT_SAVED, true);
            } else if (type.equals("Draft")) {
                i = new Intent(NewEvaluationActivity.this, EvaluationCompletedDraftActivity.class);
                i.putExtra("appointmentType", "Draft");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                i = new Intent(NewEvaluationActivity.this, ScheduleAppoinmentActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            }
        }
        startActivity(i);
        finish();
    }

    public void submitData() {
        try {
            if (CommonHelper.isConnectingToInternet(context)) {
                binding.btnNext.setEnabled(false);
                newEvaluation = presenter.getEvaluationObject(context, newEvaluation.getId());
                if (null != currentLocation) {
                    newEvaluation.setLatitude(String.valueOf(currentLocation.getLatitude()));
                    newEvaluation.setLongitude(String.valueOf(currentLocation.getLongitude()));
                } else {
                    binding.btnNext.setEnabled(true);
                    trackLocation();
                    return;
                }
                String jsonStr = presenter.prepareEvaluationDataJson(newEvaluation,
                        SessionUserDetails.getInstance().getUserID().toUpperCase());
                Log.e("Input JSON Evaluation", jsonStr);
                JSONObject jsonObj = new JSONObject(jsonStr);
                showProgressDialog("Submitting your form... Please wait...");
                presenter.submitEvaluationData(context, jsonObj);
            } else {
                binding.btnNext.setEnabled(true);
                CommonHelper.toast("Not connected to internet", context);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataSubmissionSuccess(int requestId, String response) {
        try {
            Log.e("Xmart Eval Res Success", response);
            switch (requestId) {
                case 1:
                    JSONObject jsonObject = new JSONObject(response);
                    long rowId = evaluatorHelper.addUpdateNewEvaluationDetails(newEvaluation,
                            EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT);
                    if (jsonObject.getString("IsSuccessful").equalsIgnoreCase("1")) {
                        if (!type.equals(KEY_EDIT_EVAL))
                            EvaluationReportID = jsonObject.get("EvaluationReportID").toString();
                        else
                            EvaluationReportID = newEvaluation.getEvaluationNo();
                        if ((!newEvaluation.getRequestId().isEmpty()) || (!newEvaluation.getId().isEmpty())) {
                            evaluatorHelper.updateDraftStatusToCompletedEvaluation(
                                    newEvaluation.getRequestId(), String.valueOf(rowId));
                        }
                        uploadImages();
                    } else {
                        CommonHelper.toast(jsonObject.getString("message"), context);
                    }
                    break;
                case 2:
                    JSONObject jsonObject1 = new JSONObject(response);
                    evaluatorHelper.addUpdateNewEvaluationDetails(newEvaluation,
                            EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT);
                    if (jsonObject1.getString("IsSuccessful").equalsIgnoreCase("1")) {
                        CommonHelper.toast(jsonObject1.getString("message"), context);
                    } else {
                        CommonHelper.toast(jsonObject1.getString("message"), context);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataSubmissionFailed(int requestId, String message) {
        try {
            Log.e("Xmart Eval Res Error", message);
            if (requestId == WebConstants.submitEvaluationReport.REQUEST_CODE) {
                hideProgressDialog();
                binding.btnNext.setEnabled(true);
                CommonHelper.toast(message, context);
            }
        } catch (Exception e) {
            CommonHelper.toast(message, context);
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String msg) {
        try {
            if (null == dialog) {
                dialog = new ProgressDialog(NewEvaluationActivity.this);
                dialog.setCancelable(false);
            }
            if (null != msg && !msg.isEmpty())
                dialog.setMessage(msg);
            else
                dialog.setMessage("Wait while uploading");
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        saveDraft();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length <= 0) {
                    Log.i(TAG, "User interaction was cancelled.");
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    trackLocation();
                }
                break;
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Log.i(LOG_TAG, "Permission Granted, Now you can access camera");
                else CommonHelper.toast("Permission Denied, You cannot access camera", context);
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(NewEvaluationActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void uploadAudio() {
        try {
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(
                    Request.Method.POST, WebConstants.uploadEvaluationDocument.URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            try {
                                JSONObject obj = new JSONObject(new String(response.data));
                                if (obj.getString("IsSuccessful").contains("1")) {
                                    hideProgressDialog();
                                    startActivity(new Intent(NewEvaluationActivity.this,
                                            EvaluationSubmittedSuccessfully.class)
                                            .putExtra("EvaluationReportID", EvaluationReportID));
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                successfulImageUploadDoc = successfulImageUploadDoc + 1;
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            binding.btnNext.setEnabled(true);
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "Error : "
                                    + error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    String userId;
                    if (SessionUserDetails.getInstance().getUserType() == 0) {//champ
                        userId = SessionUserDetails.getInstance().getActualUserID().toLowerCase();
                    } else {
                        userId = SessionUserDetails.getInstance().getUserID().toLowerCase();
                    }

                    Map<String, String> params = new HashMap<>();
                    params.put("docType", "DOC0030");
                    params.put("userId", userId);
                    params.put("userType", "CHAMP");
                    params.put("evalId", EvaluationReportID);
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long audioname = System.currentTimeMillis();
                    // TODO : fetch file and upload.. compress will be done at the time of image capture
                    try {
                        params.put("file", new DataPart(audioname + ".mp3", convert(
                                newEvaluation.getAudioPathVehicleNoise())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return params;
                }
            };

            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Volley.newRequestQueue(this).add(volleyMultipartRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void uploadDocument(final String docType, final String imagePath,
                        final String evaluationReportId, final String documentType) {
        LogUtil.getInstance().logE("VolleySingleTon", "POST URL: " + WebConstants.uploadEvaluationDocument.URL + "\nDocType: " + docType + "\nImagePath: " + imagePath + "\nEvaluationID: " + evaluationReportId + "\nDocumentType: " + documentType);
        try {
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(
                    Request.Method.POST, WebConstants.uploadEvaluationDocument.URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            LogUtil.getInstance().logE("VolleySingleTon", "RESPONSE: " + response.data);
                            try {
                                JSONObject obj = new JSONObject(new String(response.data));
                                if (obj.getString("IsSuccessful").contains("1")) {
                                    if (null != dialog) {
                                        showProgressDialog("Uploading " + documentType);
                                    }
                                    successfulImageUploadDoc = successfulImageUploadDoc + 1;
                                    j = j + 1;
                                    if (j < arrImageUpload.size()) {
                                        uploadDocument(arrImageUpload.get(j).getDocType(),
                                                arrImageUpload.get(j).getImagePath(),
                                                arrImageUpload.get(j).getEvaluationReportId(),
                                                arrImageUpload.get(j).getDocumentName());
                                    }
                                    if (arrImageUpload.size() == successfulImageUploadDoc) {
                                        if (!newEvaluation.getAudioPathVehicleNoise().isEmpty()) {
                                            uploadAudio();
                                        } else {
                                            hideProgressDialog();
                                            startActivity(new Intent(NewEvaluationActivity.this,
                                                    EvaluationSubmittedSuccessfully.class)
                                                    .putExtra("EvaluationReportID", EvaluationReportID));
                                            finish();
                                        }
                                    }
                                } else {
                                    successfulImageUploadDoc = successfulImageUploadDoc + 1;
                                    CommonHelper.toast("Failure of Image upload " + documentType + "\n" +
                                            obj.getString("Message"), context);
                                    j = j + 1;
                                    if (j < arrImageUpload.size()) {
                                        uploadDocument(arrImageUpload.get(j).getDocType(),
                                                arrImageUpload.get(j).getImagePath(),
                                                arrImageUpload.get(j).getEvaluationReportId(),
                                                arrImageUpload.get(j).getDocumentName());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                LogUtil.getInstance().logE("VolleySingleTon", "JSON EXCEPTION: " + e.getMessage());
                                successfulImageUploadDoc = successfulImageUploadDoc + 1;
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            LogUtil.getInstance().logE("VolleySingleTon", "ERROR: " + error.getMessage() + "\n N/W Time MS: " + error.getNetworkTimeMs());
                            binding.btnNext.setEnabled(true);
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "Error : " +
                                    error.getMessage(), Toast.LENGTH_SHORT).show();
                            successfulImageUploadDoc = successfulImageUploadDoc + 1;
                            CommonHelper.toast("Failure of Image upload " + documentType + "\n" +
                                    error.toString(), context);
                            j = j + 1;
                            if (j < arrImageUpload.size()) {
                                uploadDocument(arrImageUpload.get(j).getDocType(),
                                        arrImageUpload.get(j).getImagePath(),
                                        arrImageUpload.get(j).getEvaluationReportId(),
                                        arrImageUpload.get(j).getDocumentName());
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    String userId;
                    if (SessionUserDetails.getInstance().getUserType() == 0) {//champ
                        userId = SessionUserDetails.getInstance().getActualUserID().toLowerCase();
                    } else {
                        userId = SessionUserDetails.getInstance().getUserID().toLowerCase();
                    }

                    Map<String, String> params = new HashMap<>();
                    params.put("docType", docType);
                    params.put("userId", userId);
                    params.put("userType", "EVAL");
                    params.put("evalId", evaluationReportId);
                    LogUtil.getInstance().logE(TAG, "REQUEST: " + params.toString());
                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long imageName = System.currentTimeMillis();
                    try {
                        params.put("file", new DataPart(imageName + ".jpg",
                                new BitmapHelper().fullyReadFileToBytes(new File(imagePath))));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return params;
                }
            };

            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void uploadImages() {
        initializeSSLContext(context);
        arrImageUpload.clear();
        List<VehiclePhoto> vehiclePhotos = new ImagesPresenter(this)
                .prepareVehiclePhotosArray(newEvaluation);
        for (int iter = 0; iter < vehiclePhotos.size(); iter++) {
            if (vehiclePhotos.get(iter).getPath().length() > 0
                    && vehiclePhotos.get(iter).getPath().contains(".jpg")) {
                ImageUploadModel imageUploadModel = new ImageUploadModel(
                        vehiclePhotos.get(iter).getDocCode(),
                        vehiclePhotos.get(iter).getPath(), EvaluationReportID,
                        vehiclePhotos.get(iter).getNameOfImage().replace("*", "")
                                .replace("\n", ""));
                arrImageUpload.add(imageUploadModel);
            }
        }

        if (arrImageUpload.size() > 0) {
            uploadDocument(arrImageUpload.get(0).getDocType(),
                    arrImageUpload.get(0).getImagePath(),
                    arrImageUpload.get(0).getEvaluationReportId(),
                    arrImageUpload.get(0).getDocumentName());
        } else {
            hideProgressDialog();
            startActivity(new Intent(NewEvaluationActivity.this,
                    EvaluationSubmittedSuccessfully.class)
                    .putExtra("EvaluationReportID", EvaluationReportID));
            finish();
        }
    }

    public static void initializeSSLContext(Context mContext) {
        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location location;
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);
            } else {
                this.currentLocation = location;
            }
        } else {
            ActivityCompat.requestPermissions(NewEvaluationActivity.this, PERMISSIONS,
                    REQUEST_LOCATION_PERMISSION);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation = location;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("Error", "Location services connection failed with code " +
                    connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mGoogleApiClient) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }
    }

    private byte[] convert(String path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];

        for (int readNum; (readNum = fis.read(b)) != -1; ) {
            bos.write(b, 0, readNum);
        }

        return bos.toByteArray();
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.btn_next) {
            fillTestDataInCurrentFragment();
        }
        return true;
    }

    private void fillTestDataInCurrentFragment() {
        switch (adapter.getCurrentPosition()) {
            case VEHICLE_EVALUATION_DETAILS:
                ((VehicleEvaluationDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case OWNER_DETAILS:
                ((OwnerDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case VEHICLE_DETAILS:
                ((VehicleDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case ACCESSORIES:
                ((AccessoriesFragment) currentFragment).fillTestDataInForm();
                break;
            case INTERIOR:
                ((InteriorDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case EXTERIOR:
                ((ExteriorDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case WHEELS_AND_TYRES:
                ((WheelsAndTyresFragment) currentFragment).fillTestDataInForm();
                break;
            case AGGREGATE_DETAILS:
                ((AggregateDetailsFragment) currentFragment).fillTestDataInForm();
                break;
            case IMAGES:
                ((ImagesFragment) currentFragment).fillTestDataInForm();
                break;
            case EvaluationParts.AUDIO_AND_OVERALL_FEEDBACK:
                ((AudioAndFeedbackRatingFragment) currentFragment).fillTestDataInForm();
                break;
        }
    }

    @Override
    public void activeStateChange(int position, String title) {
        binding.tvTitle.setText(title);
        layoutManager.smoothScrollToPosition(binding.rvStepper, null, position);

        Log.e("Stepper", "activeStateChange position: " + position + " title: " + title);
        binding.btnPrevious.setVisibility((position > 0) ? View.VISIBLE : View.INVISIBLE);

        if (position == (stepperList.size() - 1)) {
            binding.btnNext.setText(getString(R.string.submit).toUpperCase());
            binding.btnNext.setBackground(getResources().getDrawable(R.drawable.primary_button_bg));
            binding.btnNext.setTextColor(getResources().getColor(R.color.white));
        } else {
            binding.btnNext.setText(getString(R.string.next).toUpperCase());
            binding.btnNext.setBackground(getResources().getDrawable(R.drawable.border_button_bg));
            binding.btnNext.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        replaceFragment(position, false);
    }

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }
}