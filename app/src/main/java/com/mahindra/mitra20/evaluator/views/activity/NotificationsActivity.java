package com.mahindra.mitra20.evaluator.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.NotificationsItemDecoration;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.evaluator.views.adapters.NotificationAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.module_broker.models.Notification;
import com.mahindra.mitra20.sqlite.BrokerDbHelper;

import java.util.ArrayList;

public class NotificationsActivity extends AppCompatActivity {

    EmptyRecyclerView recyclerViewNotifications;
    ArrayList<Notification> notifications;
    NotificationAdapter notificationAdapter;
    BrokerDbHelper brokerDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notification_list);
        brokerDbHelper = new BrokerDbHelper(this);

        init();
        brokerDbHelper.setAllNotificationsRead();
    }

    public void init() {
        CommonHelper.settingCustomToolBarEvaluator(this, "Notifications", View.INVISIBLE);

        notifications = brokerDbHelper.getNotifications();
        recyclerViewNotifications = findViewById(R.id.recyclerViewNotifications);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotifications.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewNotifications.setEmptyView(findViewById(R.id.list_empty_notification));
        recyclerViewNotifications.addItemDecoration(new NotificationsItemDecoration(
                ScreenDensityHelper.dpToPx(this, 10),
                ScreenDensityHelper.dpToPx(this, 20),
                ScreenDensityHelper.dpToPx(this, 20)));
        notificationAdapter = new NotificationAdapter(this, notifications);
        recyclerViewNotifications.setAdapter(notificationAdapter);
    }
}