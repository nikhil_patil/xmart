package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.databinding.LayoutOwnerDetailsNewBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.OwnerDetailsPresenter;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class OwnerDetailsFragment extends Fragment implements CommonMethods {

    private LayoutOwnerDetailsNewBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private ArrayList<MasterModel> arrayListState = new ArrayList<>();
    private ArrayList<MasterModel> arrayListDistrict = new ArrayList<>();
    private ArrayList<MasterModel> arrayListCity = new ArrayList<>();
    private ArrayList<MasterModel> arrayListPinCode = new ArrayList<>();
    private OwnerDetailsPresenter ownerDetailsPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {

        binding = LayoutOwnerDetailsNewBinding.inflate(inflater,
                container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        ownerDetailsPresenter = new OwnerDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = ownerDetailsPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        arrayListState.add(new MasterModel("Select State", "Select State"));
        arrayListState.addAll(ownerDetailsPresenter.getStateList());
        ArrayAdapter<MasterModel> stateAdapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListState);
        stateAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        binding.spinnerState.setAdapter(stateAdapter);
        binding.spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    updateDistrictSpinner(arrayListState.get(position).getCode());
                else {
                    clearDistrictSpinner();
                    clearCitySpinner();
                    clearPinCodeSpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    updateCitySpinner(arrayListDistrict.get(position).getCode());
                else {
                    clearCitySpinner();
                    clearPinCodeSpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerTehsil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    updatePinCodeSpinner(arrayListCity.get(position).getCode());
                else {
                    clearPinCodeSpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void displayData() {
        try {
            binding.editTextOwnerName.setText(newEvaluation.getOwnerName());
            binding.editTextAddres.setText(newEvaluation.getAddress());
            binding.editTextEmailId.setText(newEvaluation.getEmailId());
            binding.editTextPhoneNo.setText(newEvaluation.getPhoneNo());
            binding.editTextMobileNo.setText(newEvaluation.getMobileNo());

            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.occupation),
                    getResources().getStringArray(R.array.occupation_code),
                    newEvaluation.getOccupation(), binding.spinnerOccupation);

            if (newEvaluation.getState() != null && !newEvaluation.getState().trim().equals(""))
                SpinnerHelper.setSpinnerValue(arrayListState, newEvaluation.getState(), binding.spinnerState);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setOwnerName(binding.editTextOwnerName.getText().toString().trim());
            newEvaluation.setAddress(binding.editTextAddres.getText().toString().trim());
            newEvaluation.setEmailId(binding.editTextEmailId.getText().toString().trim());
            newEvaluation.setPhoneNo(binding.editTextPhoneNo.getText().toString().trim());
            newEvaluation.setMobileNo(binding.editTextMobileNo.getText().toString().trim());
            newEvaluation.setState(binding.spinnerState.getSelectedItem().toString());
            newEvaluation.setCity(binding.spinnerTehsil.getSelectedItem().toString());
            newEvaluation.setPinCode(binding.spinnerPincode.getSelectedItem().toString());
            newEvaluation.setOccupation(SpinnerHelper.getSpinnerValue(binding.spinnerOccupation,
                    getResources().getStringArray(R.array.occupation_code)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        ownerDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.editTextOwnerName.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextOwnerName,
                    "Enter owner name");
        }
        if (binding.editTextAddres.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextAddres,
                    "Enter owner address");
        }
        if (binding.editTextEmailId.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextEmailId,
                    "Enter owner's email");
        }
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (!pattern.matcher(binding.editTextEmailId.getText()).matches()) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextEmailId,
                    "Enter valid email id");
        }
        if (binding.editTextPhoneNo.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextPhoneNo,
                    "Enter owner's phone");
        }
        if (binding.editTextMobileNo.length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextMobileNo,
                    "Enter owner's mobile number");
        }
        if (binding.editTextMobileNo.length() != 10) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextMobileNo,
                    "Enter valid 10 digit mobile number");
        }
        if (binding.spinnerState.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerState,
                    "Select State");
        }
        if (binding.spinnerDistrict.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerDistrict,
                    "Select District");
        }
        if (binding.spinnerTehsil.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTehsil,
                    "Select City");
        }
        if (binding.spinnerPincode.getSelectedItemPosition() <= 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerPincode,
                    "Select Pin Code");
        }
        if (binding.spinnerOccupation.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerOccupation,
                    "Select Occupation");
        }
        saveData();
        return true;
    }

    private void clearDistrictSpinner() {
        arrayListDistrict.clear();
        arrayListDistrict.add(new MasterModel("", "Select District"));
        binding.spinnerDistrict.setAdapter(new ArrayAdapter<>(context, R.layout.simple_spinner_item, arrayListDistrict));
    }

    private void clearCitySpinner() {
        arrayListCity.clear();
        arrayListCity.add(new MasterModel("", "Select City"));
        binding.spinnerTehsil.setAdapter(new ArrayAdapter<>(context, R.layout.simple_spinner_item, arrayListCity));
    }

    private void clearPinCodeSpinner() {
        arrayListPinCode.clear();
        arrayListPinCode.add(new MasterModel("", "Select Pincode"));
        binding.spinnerPincode.setAdapter(new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListPinCode));
    }

    private void updateDistrictSpinner(String stateID) {
        arrayListDistrict.clear();
        arrayListDistrict.add(new MasterModel("", "Select District"));
        arrayListDistrict.addAll(ownerDetailsPresenter.getDistrictList(stateID));
        binding.spinnerDistrict.setAdapter(new ArrayAdapter<>(context, R.layout.simple_spinner_item, arrayListDistrict));
        String districtID = ownerDetailsPresenter.getDistrictID(newEvaluation.getPinCode());
        SpinnerHelper.setSpinnerValue(arrayListDistrict, districtID, binding.spinnerDistrict);
    }

    private void updateCitySpinner(String districtID) {
        arrayListCity.clear();
        arrayListCity.add(new MasterModel("", "Select City"));
        arrayListCity.addAll(ownerDetailsPresenter.getTehsilList(districtID));
        binding.spinnerTehsil.setAdapter(new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListCity));
        SpinnerHelper.setSpinnerValue(arrayListCity, newEvaluation.getCity(), binding.spinnerTehsil);

    }

    private void updatePinCodeSpinner(String tehsilID) {
        arrayListPinCode.clear();
        arrayListPinCode.add(new MasterModel("", "Select Pincode"));
        arrayListPinCode.addAll(ownerDetailsPresenter.getPinCodeList(tehsilID));
        binding.spinnerPincode.setAdapter(new ArrayAdapter<>(context,
                R.layout.simple_spinner_item, arrayListPinCode));
        SpinnerHelper.setSpinnerValue(arrayListPinCode, newEvaluation.getPinCode(), binding.spinnerPincode);
    }

    @Override
    public void fillTestDataInForm() {
        binding.editTextOwnerName.setText("Nikhil Patil");
        binding.editTextAddres.setText("Pune");
        binding.editTextEmailId.setText("test@gmail.com");
        binding.editTextPhoneNo.setText("8888888888");
        binding.editTextMobileNo.setText("8888888899");
        binding.spinnerOccupation.setSelection(2);
        binding.spinnerState.setSelection(21);
    }
}