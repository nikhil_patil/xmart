package com.mahindra.mitra20.evaluator.views.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.databinding.LayoutAudioBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.AudioAndFeedbackRatingPresenter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class AudioAndFeedbackRatingFragment extends Fragment implements CommonMethods,
        View.OnClickListener, MediaRecorder.OnInfoListener, MediaPlayer.OnCompletionListener {

    LayoutAudioBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private AudioAndFeedbackRatingPresenter presenter;
    private MediaPlayer player = null;
    private MediaRecorder recorder = null;
    private boolean isRecording = false, isPlaying = false;
    private String filePath = null;
    private boolean isAudioAvailable = false;
    private String fileName;
    private String path;
    private final String LOG_TAG = "AudioRecordTest";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutAudioBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        presenter = new AudioAndFeedbackRatingPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = presenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        path = "/data/data/com.mahindra.xmart/files/";
        fileName = "recordedAudio.mp3";
        filePath = path + fileName;
        binding.recordBT.setOnClickListener(this);
        binding.playBT.setOnClickListener(this);
        binding.recordBT.setEnabled(true);
        binding.playBT.setEnabled(false);
        binding.deleteBT.setOnClickListener(this);

        isPlaying = false;
        isRecording = false;
    }

    @Override
    public void displayData() {
        if (!newEvaluation.getAudioPathVehicleNoise().equalsIgnoreCase("")) {
            binding.playBT.setVisibility(View.VISIBLE);
            binding.playTV.setVisibility(View.VISIBLE);
            binding.deleteBT.setVisibility(View.VISIBLE);
            binding.deleteTV.setVisibility(View.VISIBLE);
            binding.playBT.setEnabled(true);
            binding.recordTV.setText(R.string.start_rerecording);
            filePath = newEvaluation.getAudioPathVehicleNoise();
            isAudioAvailable = true;
        } else {
            isAudioAvailable = false;
        }
        binding.ratingFeedback.setRating(getParsedRating());
    }

    @Override
    public NewEvaluation getData() {
        try {
            if (isAudioAvailable) {
                newEvaluation.setAudioPathVehicleNoise(filePath);
            } else {
                newEvaluation.setAudioPathVehicleNoise(null);
            }
            newEvaluation.setOverallFeedbackRating(String.valueOf((int) binding.ratingFeedback.getRating()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        presenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.ratingFeedback.getRating() == 0.0f) {
            CommonHelper.toast("Please Select Rating", context);
            return false;
        }
        saveData();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recordBT:
                if (!isRecording) {
                    startRecording();
                    isRecording = true;
                    binding.playBT.setEnabled(false);
                    binding.playBT.setVisibility(View.INVISIBLE);
                    binding.playTV.setVisibility(View.INVISIBLE);
                    binding.deleteBT.setVisibility(View.INVISIBLE);
                    binding.deleteTV.setVisibility(View.INVISIBLE);
                    binding.recordBT.setImageResource(R.drawable.ic_stop);
                    binding.recordTV.setText(R.string.stop_recording);
                } else if (isRecording) {
                    stopRecording();
                    isRecording = false;
                    binding.playBT.setEnabled(true);
                    binding.playBT.setVisibility(View.VISIBLE);
                    binding.playTV.setVisibility(View.VISIBLE);
                    binding.deleteBT.setVisibility(View.VISIBLE);
                    binding.deleteTV.setVisibility(View.VISIBLE);
                    binding.recordTV.setText(R.string.start_rerecording);
                    binding.recordBT.setImageResource(R.drawable.ic_record);
                }
                break;
            case R.id.playBT:
                if (!isPlaying) {
                    startPlaying();
                    isPlaying = true;
                    binding.playBT.setImageResource(R.drawable.ic_stop);
                    binding.recordBT.setEnabled(false);
                    binding.playTV.setText(R.string.stop_playing);
                    binding.deleteBT.setEnabled(false);
                } else if (isPlaying) {
                    stopPlaying();
                    isPlaying = false;
                    binding.playBT.setImageResource(R.drawable.ic_play);
                    binding.recordBT.setEnabled(true);
                    binding.playTV.setText(R.string.start_playing);
                    binding.deleteBT.setEnabled(true);
                }
                break;
            case R.id.deleteBT:
                new AlertDialog.Builder(context).setIcon(R.drawable.ic_delete_black_24dp).
                        setTitle("Delete Audio")
                        .setMessage("Are you sure you want to delete recorded audio?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (new File(filePath).delete()) {
                                    isAudioAvailable = false;
                                    binding.playBT.setEnabled(false);
                                    binding.playBT.setVisibility(View.INVISIBLE);
                                    binding.playTV.setVisibility(View.INVISIBLE);
                                    binding.deleteBT.setVisibility(View.INVISIBLE);
                                    binding.deleteTV.setVisibility(View.INVISIBLE);
                                    binding.recordTV.setText(R.string.start_recording);
                                } else if (filePath.contains("https")) {
                                    isAudioAvailable = false;
                                    binding.playBT.setEnabled(false);
                                    binding.playBT.setVisibility(View.INVISIBLE);
                                    binding.playTV.setVisibility(View.INVISIBLE);
                                    binding.deleteBT.setVisibility(View.INVISIBLE);
                                    binding.deleteTV.setVisibility(View.INVISIBLE);
                                    binding.recordTV.setText(R.string.start_recording);
                                    filePath = "";
                                }
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                break;
        }
    }


    private void startRecording() {
        if (new File(filePath).exists()) {
            if (new File(filePath).delete())
                recordAudio();
        } else recordAudio();

    }

    private void recordAudio() {
        recorder = new MediaRecorder();
        recorder.setOnInfoListener(this);
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        long timestamp = System.currentTimeMillis() / 1000;
        String name = Long.toString(timestamp).replaceAll(" ", "_");
        name = name.replaceAll(":", "-") + fileName;
        filePath = path + name;
        recorder.setOutputFile(filePath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setMaxFileSize(200000);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
        recorder.start();
        isAudioAvailable = true;
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;
    }

    private void startPlaying() {
        player = new MediaPlayer();
        if (filePath.contains("https")) {
            if (!isPlaying && !player.isPlaying()) {
                binding.playBT.setVisibility(View.GONE);
                binding.audioPB.setVisibility(View.VISIBLE);
                binding.playTV.setVisibility(View.GONE);
                Uri uri = Uri.parse(newEvaluation.getAudioPathVehicleNoise());
                Map<String, String> headers = new HashMap<>();
                String audioAuthCode = "YWRtaW46UHJvZDEyMyE=";
                headers.put("authCd", audioAuthCode);
                String audioContentType = "application/octet-stream";
                headers.put("Content-Type", audioContentType);
                // Use java reflection call the hide API:
                Method method = null;
                try {
                    method = player.getClass().
                            getMethod("setDataSource", new Class[]{Context.class, Uri.class, Map.class});
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                try {
                    if (method != null) {
                        method.invoke(player, new Object[]{this, uri, headers});
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.prepareAsync();
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        binding.playBT.setImageResource(R.drawable.ic_stop);
                        binding.playBT.setVisibility(View.VISIBLE);
                        binding.audioPB.setVisibility(View.GONE);
                        binding.playTV.setVisibility(View.VISIBLE);
                        binding.playTV.setText(R.string.stop_playing);
                        mp.start();
                        isPlaying = true;
                    }
                });
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        binding.playBT.setImageResource(R.drawable.ic_play);
                        binding.playTV.setText(R.string.start_playing);
                        mp.reset();
                        isPlaying = false;
                    }
                });
            } else if (player.isPlaying() && isPlaying) {
                player.stop();
                player.reset();
                binding.playBT.setImageResource(R.drawable.ic_play);
                binding.playTV.setText(R.string.start_playing);
                isPlaying = false;
            }
        } else {
            player.setOnCompletionListener(this);
            try {
                player.setDataSource(filePath);
                player.prepare();
                player.start();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() failed");
            }
        }
    }

    private void stopPlaying() {
        player.release();
        player = null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlaying();
        isPlaying = false;
        binding.playBT.setImageResource(R.drawable.ic_play);
        binding.recordBT.setEnabled(true);
        binding.playTV.setText(R.string.start_playing);
        binding.deleteBT.setEnabled(true);
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {
            Log.d("MainActivity", "Media exceeded the size");
            isRecording = false;
            stopRecording();
            binding.playBT.setVisibility(View.VISIBLE);
            binding.playTV.setVisibility(View.VISIBLE);
            binding.deleteBT.setVisibility(View.VISIBLE);
            binding.deleteTV.setVisibility(View.VISIBLE);
            binding.recordTV.setText(R.string.start_rerecording);
            binding.playBT.setEnabled(true);
            binding.recordBT.setImageResource(R.drawable.ic_record);
            binding.deleteBT.setEnabled(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }

        if (player != null) {
            player.release();
            player = null;
        }
    }

    private float getParsedRating() {
        try {
            return Float.parseFloat(newEvaluation.getOverallFeedbackRating());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    @Override
    public void fillTestDataInForm() {
        binding.ratingFeedback.setRating(3.0f);
    }
}