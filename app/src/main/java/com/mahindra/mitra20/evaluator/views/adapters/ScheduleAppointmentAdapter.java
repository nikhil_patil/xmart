package com.mahindra.mitra20.evaluator.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DialogsHelper;
import com.mahindra.mitra20.evaluator.helper.ViewHelper;
import com.mahindra.mitra20.evaluator.interfaces.ItemClickListener;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.views.activity.AppoinmentDetailsActivity;
import com.mahindra.mitra20.evaluator.views.activity.EvaluatorDashboardActivity;
import com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity;
import com.mahindra.mitra20.evaluator.views.activity.RescheduleAppointmentActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class ScheduleAppointmentAdapter extends
        RecyclerView.Adapter<ScheduleAppointmentAdapter.ViewHolder>
        implements VolleySingleTon.VolleyInteractor {

    private ArrayList<ScheduleAppoinment> scheduleAppointments;
    private Context context;
    private ItemClickListener clickListener;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private DateHelper dateHelper;
    private DialogsHelper dialogsHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    public void setClickListener(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case 1:
                break;
            case 2:
                CommonHelper.toast(R.string.cancel_appointment_successfully, context);
                context.startActivity(new Intent(context, EvaluatorDashboardActivity.class));
                ((Activity) context).finish();
                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ItemClickListener {
        // each data item is just a string in this case
        TextView textViewMonth, textViewDate, textViewTime, textViewCustomerName, tvEnquiryType,
                textViewVehicleName, textViewLocation, textViewStartEvaluation, tvEnquirySource,
                textViewReschedule, textViewCancel, textViewSalesPersonName,
                textViewSalesPersonContactNo, textViewLeadPriority;
        LinearLayout linearLayoutOverdueVisits, linearLayoutShowAppoointmentDetails;
        public View layout;
        public ImageView imageViewCall, imageViewMessage;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewMonth = v.findViewById(R.id.textViewMonth);
            textViewDate = v.findViewById(R.id.textViewDate);
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewStartEvaluation = v.findViewById(R.id.textViewStartEvaluation);
            textViewReschedule = v.findViewById(R.id.textViewReschedule);
            textViewCancel = v.findViewById(R.id.textViewCancel);
            linearLayoutOverdueVisits = v.findViewById(R.id.linearLayoutOverdueVisits);
            linearLayoutShowAppoointmentDetails = v.findViewById(R.id.linearLayoutShowAppoointmentDetails);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewMessage = v.findViewById(R.id.imageViewMessage);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);
            textViewSalesPersonName = v.findViewById(R.id.textViewSalesPersonName);
            textViewSalesPersonContactNo = v.findViewById(R.id.textViewSalesPersonContactNo);
            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);
        }

        @Override
        public void onClick(View view, int position) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public void updateList(ArrayList<ScheduleAppoinment> scheduleAppoinments) {
        this.scheduleAppointments = scheduleAppoinments;
    }

    public ScheduleAppointmentAdapter(Context _context, ArrayList<ScheduleAppoinment> _scheduleAppoinments) {
        scheduleAppointments = _scheduleAppoinments;
        context = _context;
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper=new MasterDatabaseHelper(context);
        dateHelper = new DateHelper();
        dialogsHelper = new DialogsHelper(context);
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    @Override
    public ScheduleAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_scheduled_visit, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String strDate = scheduleAppointments.get(position).getStrDate();
        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        String strMonth = dateHelper.getMonthName(appointmentDate);
        String strAppointmentDate = dateHelper.getDate(appointmentDate);
        String strTime = dateHelper.convertTwentyFourHourToTweleveHour(scheduleAppointments.get(position).getStrTime());

        holder.textViewMonth.setText(strMonth);
        holder.textViewDate.setText(strAppointmentDate);
        holder.textViewTime.setText(strTime);
        holder.textViewCustomerName.setText(scheduleAppointments.get(position).getStrCustomerName());
        holder.textViewVehicleName.setText(scheduleAppointments.get(position).getStrVehicleName());
        holder.textViewLocation.setText(scheduleAppointments.get(position).getStrLocation());

        holder.textViewSalesPersonName.setText(scheduleAppointments.get(position).getSalesconsultantname());
        holder.textViewSalesPersonContactNo.setText(scheduleAppointments.get(position).getSalesconsultantContactnum());

        ViewHelper.populateLeadView(context, holder.textViewLeadPriority,
                scheduleAppointments.get(position).getLeadPriority());
        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, scheduleAppointments.get(position).getStrMake(),
                scheduleAppointments.get(position).getStrModel());
        String strMake = makeModel.first;
        String strModel = makeModel.second;
        holder.textViewVehicleName.setText(String.format("%s %s", strMake, strModel));
        String enqSource = scheduleAppointments.get(position).getEnquirySource();
        if (null != enqSource)
            holder.tvEnquirySource.setText(String.format("Source - %s", enqSource));
        String enqType = scheduleAppointments.get(position).getEnquiryType();
        if (null != enqType)
            holder.tvEnquiryType.setText(String.format("Type - %s", enqType));

        holder.textViewStartEvaluation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masterDatabaseHelper.isMasterSync()) {
                    String id = evaluatorHelper.getNextEvaluationId();
                    ScheduleAppoinment scheduleAppoinmet = scheduleAppointments.get(position);
                    Intent intent = new Intent(context, NewEvaluationActivity.class);
                    intent.putExtra("scheduleAppoinment", scheduleAppoinmet);
                    intent.putExtra("status", "newEvaluation");
                    intent.putExtra("NewEvaluationId", id);
                    intent.putExtra("purpose", "Exchange");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
            }
        });

        holder.textViewReschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masterDatabaseHelper.isMasterSync()) {
                    if (evaluatorHelper.isAppointmentConfirmed(scheduleAppointments.get(position).getRequestID()) > 0) {
                        CommonHelper.toast("Appointment already confirmed.", context);
                    } else {
                        ScheduleAppoinment scheduleAppoinmet = scheduleAppointments.get(position);
                        Intent intent = new Intent(context, RescheduleAppointmentActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("scheduleAppoinment", scheduleAppoinmet);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    }

                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
            }
        });

        holder.linearLayoutShowAppoointmentDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masterDatabaseHelper.isMasterSync()) {
                    ScheduleAppoinment scheduleAppoinmet = scheduleAppointments.get(position);
                    Intent intent = new Intent(context, AppoinmentDetailsActivity.class);
                    intent.putExtra("scheduleAppoinment", scheduleAppoinmet);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
            }
        });

        holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new com.mahindra.mitra20.champion.helper.DialogsHelper((Activity) context)
                        .callDialog(scheduleAppointments.get(position).getStrContactNo());
            }
        });

        holder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.sendMessage(scheduleAppointments.get(position).getStrContactNo(), context);
            }
        });
        holder.textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String requestId = scheduleAppointments.get(position).getRequestID();
                String strNotes = scheduleAppointments.get(position).getNotes();
                dialogsHelper.showCancelAppointmentDialog(2, requestId, strNotes);
            }
        });
    }

    @Override
    public int getItemCount() {
        return scheduleAppointments.size();
    }
}