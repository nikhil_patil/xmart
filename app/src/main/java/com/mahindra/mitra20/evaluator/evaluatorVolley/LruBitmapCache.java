package com.mahindra.mitra20.evaluator.evaluatorVolley;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import java.lang.reflect.Method;

/**
 * Created by BADHAP-CONT on 8/31/2018.
 */

public class LruBitmapCache {

    public static final int DEFAULT_MEMORY_CACHE_PERCENTAGE = 25;
    private static final int DEFAULT_MEMORY_CAPACITY_FOR_DEVICES_OLDER_THAN_API_LEVEL_4 = 12;
    private LruCache<String, Bitmap> cache;
    private int capacity;

    /**
     * It is possible to set a specific percentage of memory to be used only for images.
     * @param context
     * @param percentageOfMemoryForCache 1-80
     */
    public LruBitmapCache(Context context, int percentageOfMemoryForCache) {
        int memClass = 0;
        ActivityManager am = ((ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE));
        try {
            Method m = ActivityManager.class.getMethod("getMemoryClass");
            memClass = (Integer)m.invoke(am);
        } catch (Exception e) {
        }
        if(memClass == 0) {
            memClass = DEFAULT_MEMORY_CAPACITY_FOR_DEVICES_OLDER_THAN_API_LEVEL_4;
        }
        if(percentageOfMemoryForCache < 0) {
            percentageOfMemoryForCache = 0;
        }
        if(percentageOfMemoryForCache > 81) {
            percentageOfMemoryForCache = 80;
        }
        this.capacity = (1024 *1024*(memClass * percentageOfMemoryForCache))/100;
        if(this.capacity <= 0) {
            this.capacity = 1024*1024*4;
        }
        reset();
    }

    /**
     * Setting the default memory size to 25% percent of the total memory
     * available of the application.
     * @param context
     */
    public LruBitmapCache(Context context) {
        this(context, DEFAULT_MEMORY_CACHE_PERCENTAGE);
    }

    private void reset() {
        cache = new LruCache<String, Bitmap>(capacity) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getRowBytes()*bitmap.getHeight();
            }
        };
    }

   /* @Override
    public Bitmap get(String url, int width, int height) {
        int i = width;
        if(width < height) {
            i = height;
        }
        return cache.get(url + i);
    }

    @Override
    public void put(String url, Bitmap bmp) {
        int i = bmp.getWidth();
        if(bmp.getWidth() < bmp.getHeight()) {
            i = bmp.getHeight();
        }
        cache.put(url + i, bmp);
    }

    @Override
    public void clean() {
        reset();
    }*/

}
