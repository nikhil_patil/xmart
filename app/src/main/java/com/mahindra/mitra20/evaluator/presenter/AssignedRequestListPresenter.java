package com.mahindra.mitra20.evaluator.presenter;

import android.content.Context;
import android.util.Log;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.constants.GeneralConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 8/11/2018.
 */

public class AssignedRequestListPresenter implements VolleySingleTon.VolleyInteractor {

    private AssignedRequestListIn assignRequestListIn;
    private Context context;
    private EvaluatorHelper evaluatorHelper;
    String strUserId = "";

    public AssignedRequestListPresenter(AssignedRequestListIn assignRequestListIn, Context context) {
        this.assignRequestListIn = assignRequestListIn;
        this.context = context;
        evaluatorHelper = new EvaluatorHelper(context);
    }

    public void downloadEnquiryMaster() {
        try {
            HashMap<String, String> params = new HashMap<>();
            String employeeCode = SessionUserDetails.getInstance().getEmployeeCode();
            String[] arrUserId = SessionUserDetails.getInstance().getUserID().toLowerCase().split("\\.");
            String strUserId = arrUserId[0];
            if (null != employeeCode && !employeeCode.isEmpty())
                params.put(WebConstants.ScheduleAppoinmentList.USER_ID, employeeCode);
            else
                params.put(WebConstants.ScheduleAppoinmentList.USER_ID, strUserId);
            params.put(WebConstants.ScheduleAppoinmentList.REQUEST_STATUS,
                    EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS_KEY);
            params.put(WebConstants.ScheduleAppoinmentList.EVALUATION_REPORT_ID, "");
            params.put(WebConstants.ScheduleAppoinmentList.COUNT_TO_DISPLAY_DASHBOARD, "9999");
            params.put(WebConstants.ScheduleAppoinmentList.PINCODE, "");
            JSONObject jsonObject = new JSONObject(params);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.ScheduleAppoinmentList.URL, jsonObject, WebConstants.ScheduleAppoinmentList.REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.ScheduleAppoinmentList.REQUEST_CODE,
                    context.getResources().getString(R.string.download_failed));
        }
    }

    public void downloadPCSEnquiryMaster() {
        try {
            HashMap<String, String> params = new HashMap<>();
            String employeeCode = SessionUserDetails.getInstance().getEmployeeCode();
            String[] arrUserId = SessionUserDetails.getInstance().getUserID().toLowerCase().split("\\.");
            String strUserId = arrUserId[0];
            if (null != employeeCode && !employeeCode.isEmpty())
                params.put(WebConstants.ScheduleAppoinmentList.USER_ID, employeeCode);
            else
                params.put(WebConstants.ScheduleAppoinmentList.USER_ID, strUserId);
            params.put(WebConstants.ScheduleAppoinmentList.REQUEST_STATUS,
                    EvaluatorConstants.PROCUREMENT_STATUS_KEY);
            params.put(WebConstants.ScheduleAppoinmentList.EVALUATION_REPORT_ID, "");
            params.put(WebConstants.ScheduleAppoinmentList.COUNT_TO_DISPLAY_DASHBOARD, "9999");
            params.put(WebConstants.ScheduleAppoinmentList.PINCODE, "");
            JSONObject jsonObject = new JSONObject(params);

            VolleySingleTon.getInstance().connectToPostUrl(context, this,
                    WebConstants.ScheduleAppoinmentList.URL, jsonObject, WebConstants.ScheduleAppoinmentList.REQUEST_CODE_PCS);

        } catch (Exception e) {
            e.printStackTrace();
            assignRequestListIn.onDownloadFail(WebConstants.ScheduleAppoinmentList.REQUEST_CODE,
                    context.getResources().getString(R.string.download_failed));
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (requestId == WebConstants.ScheduleAppoinmentList.REQUEST_CODE) {
                ArrayList<ScheduleAppoinment> listScheduleAppoinments = new ArrayList<>();

                JSONObject jsonObjectParent = new JSONObject(response);
                JSONArray jsonArrayScheduleAppoinmentList = jsonObjectParent.getJSONArray("EnquiryRequestList");

                for (int i = 0; i < jsonArrayScheduleAppoinmentList.length(); i++) {
                    ScheduleAppoinment ScheduleAppoinment = new ScheduleAppoinment();
                    JSONObject jsonObjectScheduleAppoinment = (JSONObject) jsonArrayScheduleAppoinmentList.get(i);

                    String evaluatorId = SessionUserDetails.getInstance().getUserID();
                    try {
                        if (evaluatorId.contains(".")) {
                            strUserId = evaluatorId.split("\\.")[0];
                        } else {
                            strUserId = SessionUserDetails.getInstance().getUserID().toUpperCase();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (SessionUserDetails.getInstance().getUserType() == 0 &&
                            CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment,
                                    "EvaluatorID").equalsIgnoreCase(strUserId)) {

                        ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "ContactNo"));
                        ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "DateOfAppointment"));
                        ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Make"));
                        ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Model"));
                        ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                        ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Pincode"));
                        ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "RequestID"));
                        ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "RequestStatus"));
                        ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                        ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                        ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Occupation"));
                        ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                        ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "PhoneNo"));
                        ScheduleAppoinment.setState(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "State"));
                        ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                        ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "City"));
                        ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EmailAddress"));
                        ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Salesconsultantname"));
                        ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));
                        ScheduleAppoinment.setLeadPriority(CommonHelper.parseLeadPriorityKey(
                                jsonObjectScheduleAppoinment, "LeadPriority"));
                        ScheduleAppoinment.setEnquiryLocation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquiryLocation"));
                        ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquirySource"));
                        ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquiryType"));
                    } else if (SessionUserDetails.getInstance().getUserType() == 1) {
                        ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "ContactNo"));
                        ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "DateOfAppointment"));
                        ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Make"));
                        ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Model"));
                        ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                        ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Pincode"));
                        ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "RequestID"));
                        ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "RequestStatus"));
                        ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                        ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                        ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Occupation"));
                        ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                        ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "PhoneNo"));
                        ScheduleAppoinment.setState(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "State"));
                        ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                        ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "City"));
                        ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EmailAddress"));
                        ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "Salesconsultantname"));
                        ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));
                        ScheduleAppoinment.setLeadPriority(CommonHelper.parseLeadPriorityKey(
                                jsonObjectScheduleAppoinment, "LeadPriority"));
                        ScheduleAppoinment.setEnquiryLocation(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquiryLocation"));
                        ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquirySource"));
                        ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(
                                jsonObjectScheduleAppoinment, "EnquiryType"));
                    }
                    listScheduleAppoinments.add(ScheduleAppoinment);
                }

                try {
                    GeneralConstants.DATA_FETCHED = "true";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                evaluatorHelper.deleteScheduleRequestDetails(EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
                evaluatorHelper.putScheduledAppointment(listScheduleAppoinments);
                assignRequestListIn.onDownloadSuccess(WebConstants.ScheduleAppoinmentList.REQUEST_CODE, listScheduleAppoinments);
                downloadPCSEnquiryMaster();
            }
            else if (requestId == WebConstants.ScheduleAppoinmentList.REQUEST_CODE_PCS) {
                JSONObject jsonObjectParent = new JSONObject(response);
                if (Integer.parseInt(jsonObjectParent.getString("IsSuccessful")) == 1) {
                    ArrayList<ScheduleAppoinment> listPCSAppointments = new ArrayList<>();
                    JSONArray jsonArrayScheduleAppointmentList = jsonObjectParent.getJSONArray("EnquiryRequestList");
                    for (int i = 0; i < jsonArrayScheduleAppointmentList.length(); i++) {

                        ScheduleAppoinment ScheduleAppoinment = new ScheduleAppoinment();

                        JSONObject jsonObjectScheduleAppoinment = (JSONObject) jsonArrayScheduleAppointmentList.get(i);

                        String evaluatorId = SessionUserDetails.getInstance().getUserID();

                        try {
                            if (evaluatorId.contains(".")) {
                                strUserId = evaluatorId.split("\\.")[0];
                            } else {
                                strUserId = SessionUserDetails.getInstance().getUserID().toUpperCase();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (SessionUserDetails.getInstance().getUserType() == 0 &&
                                CommonHelper.hasJSONKey(jsonObjectScheduleAppoinment,
                                        "EvaluatorID").equalsIgnoreCase(strUserId)) {
                            ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "ContactNo"));
                            ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "DateOfAppointment"));
                            ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Make"));
                            ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Model"));
                            ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                            ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Pincode"));
                            ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "RequestID"));
                            ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "RequestStatus"));
                            ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                            ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                            ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Occupation"));
                            ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                            ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "PhoneNo"));
                            ScheduleAppoinment.setState(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "State"));
                            ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                            ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "City"));
                            ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EmailAddress"));
                            ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Salesconsultantname"));
                            ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));
                            ScheduleAppoinment.setLeadPriority(CommonHelper.parseLeadPriorityKey(
                                    jsonObjectScheduleAppoinment, "LeadPriority"));
                            ScheduleAppoinment.setEnquiryLocation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquiryLocation"));
                            ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquirySource"));
                            ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquiryType"));
                        } else if (SessionUserDetails.getInstance().getUserType() == 1) {
                            ScheduleAppoinment.setStrContactNo(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "ContactNo"));
                            ScheduleAppoinment.setStrDate(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "DateOfAppointment"));
                            ScheduleAppoinment.setStrMake(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Make"));
                            ScheduleAppoinment.setStrModel(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Model"));
                            ScheduleAppoinment.setStrCustomerName(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "NameOfEnquiry"));
                            ScheduleAppoinment.setPincode(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Pincode"));
                            ScheduleAppoinment.setRequestID(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "RequestID"));
                            ScheduleAppoinment.setRequestStatus(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "RequestStatus"));
                            ScheduleAppoinment.setStrLocation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "LocationOfAppointment"));
                            ScheduleAppoinment.setStrTime(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "TimeOfAppointment"));
                            ScheduleAppoinment.setOccupation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Occupation"));
                            ScheduleAppoinment.setOptedNewVehicle(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "OptedNewVehicle"));
                            ScheduleAppoinment.setPhoneNo(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "PhoneNo"));
                            ScheduleAppoinment.setState(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "State"));
                            ScheduleAppoinment.setYearOfManufacturing(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "YearOfManufacturing"));
                            ScheduleAppoinment.setCity(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "City"));
                            ScheduleAppoinment.setEmail(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EmailAddress"));
                            ScheduleAppoinment.setSalesconsultantname(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "Salesconsultantname"));
                            ScheduleAppoinment.setSalesconsultantContactnum(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "SalesconsultantContactnum"));
                            ScheduleAppoinment.setLeadPriority(CommonHelper.parseLeadPriorityKey(
                                    jsonObjectScheduleAppoinment, "LeadPriority"));
                            ScheduleAppoinment.setEnquiryLocation(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquiryLocation"));
                            ScheduleAppoinment.setEnquirySource(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquirySource"));
                            ScheduleAppoinment.setEnquiryType(CommonHelper.hasJSONKey(
                                    jsonObjectScheduleAppoinment, "EnquiryType"));
                        }
                        listPCSAppointments.add(ScheduleAppoinment);
                    }
                    evaluatorHelper.deletePcsScheduleRequestDetails(EvaluatorConstants.PROCUREMENT_STATUS);
                    evaluatorHelper.putPcsScheduledAppointment(listPCSAppointments);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        assignRequestListIn.onDownloadFail(requestId, response);
    }

    public String getNextEvaluationId() {
        return evaluatorHelper.getNextEvaluationId();
    }
}