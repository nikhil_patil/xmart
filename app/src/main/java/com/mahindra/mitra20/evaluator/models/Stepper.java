package com.mahindra.mitra20.evaluator.models;

import java.io.Serializable;

public class Stepper implements Serializable {
    private String title;
    private String abbreviation_title;
    private Status status;

    public Stepper(String title, Status status) {
        this.title = title;
        this.status = status;
    }

    public Stepper(String title, String abbreviation_title) {
        this.title = title;
        this.abbreviation_title = abbreviation_title;
        this.status = Status.IN_ACTIVE;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getAbbreviation_title() {
        return abbreviation_title;
    }

    public void setAbbreviation_title(String abbreviation_title) {
        this.abbreviation_title = abbreviation_title;
    }

    public enum Status {
        ACTIVE, IN_ACTIVE
    }
}