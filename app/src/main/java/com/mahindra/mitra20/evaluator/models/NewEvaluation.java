package com.mahindra.mitra20.evaluator.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 8/7/2018.
 */

public class NewEvaluation implements Parcelable {
    private boolean newObject;
    private String Id;
    private String requestId;
    private String EvaluatorId;
    private String EvaluatorContactNo;
    private String EvaluationNo;
    private String AgainstTransaction;
    private String Purpose;
    private String OptedNewVehicle;
    private String Date;
    private String Time;
    private String TransactionNo;
    private String EvaluatorName;
    private String SourceOfTransaction;
    private String OwnerName;
    private String Address;
    private String EmailId;
    private String PhoneNo;
    private String MobileNo;
    private String State;
    private String City;
    private String PinCode;
    private String Occupation;
    private String Make;
    private String Model;
    private String Variant;
    private String YearOfMfg;
    private String YearOfReg;
    private String RegNo;
    private String Mileage;
    private String MonthOfMfg;
    private String MonthOfReg;
    private String EngineNo;
    private String ChassisNo;
    private String NoOfOwners;
    private String Transmission;
    private String ServiceBooklet;
    private String Color;
    private String FuelType;
    private String EuroVersion;
    private String BodyType;
    private String NoOfSeats;
    private String RCBook;
    private String Insurance;
    private String InsuranceDate;
    private String Ncb;
    private String Puc;
    private String Hypothecation;
    private String FinanceCompany;
    private String NocStatus;
    private String CustomerExpectedPrice;
    private String TestDrive;
    private String IsAccident;
    private String BodyCost;
    private String EngineCost;
    private String FuelAndIgnitionSystemCost;
    private String TransmissionCost;
    private String BreakSystemCost;
    private String SuspensionCost;
    private String SteeringSystemCost;
    private String AcCost;
    private String ElectricalSystemCost;
    private String totalOfAggregateCost;
    private String generalServiceCost;
    private String totalRefurbishmentCost;
    private String Stereo;
    private String NoOfKeys;
    private String ReverseParking;
    private String PowerSteering;
    private String AlloyWheels;
    private String FogLamp;
    private String CentralLocking;
    private String ToolKitJack;
    private String Comment;
    private String TubelessTyres;
    private String RearWiper;
    private String MusicSystem;
    private String FMRadio;
    private String PowerWindow;
    private String AC;
    private String PowerAdjustableSeats;
    private String KeyLessEntry;
    private String PedalShifter;
    private String ElectricallyAdjustableORVM;
    private String RearDefogger;
    private String SunRoof;
    private String AutoClimateTechnologies;
    private String LeatherSeats;
    private String BluetoothAudioSystems;
    private String GPSNavigationSystems;
    private String Odometer;
    private String SeatCover;
    private String Dashboard;
    private String AirBags;
    private String FrontFenderRightSide;
    private String FrontDoorRightSide;
    private String BackDoorRightSide;
    private String BackFenderRightSide;
    private String Bonnet;
    private String TailGate;
    private String BackBumper;
    private String FrontFenderLeftHandSide;
    private String FrontDoorLeftHandSide;
    private String BackDoorLeftHandSide;
    private String BackFenderLeftHandSide;
    private String FrontLight;
    private String BackLight;
    private String WindShield;
    private String Pillars;
    private String Mirrors;
    private String Wheels;
    private String TyreFrontLeft;
    private String TyreFrontRight;
    private String TyreRearLeft;
    private String TyreRearRight;
    private String SpareTyre;
    private String PhotoPathFrontImage;
    private String PhotoPathRearImage;
    private String PhotoPath45DegreeLeftView;
    private String PhotoPath45DegreeRightView;
    private String PhotoPathDashboardView;
    private String PhotoPathOdometerImage;
    private String PhotoPathChassisNoImage;
    private String PhotoPathEngineNoImage;
    private String PhotoPathInteriorView;
    private String PhotoPathRCCopyImage;
    private String PhotoPathInsuranceImage;
    private String PhotoPathRearLeftTyre;
    private String PhotoPathRearRightTyre;
    private String PhotoPathFrontLeftTyre;
    private String PhotoPathFrontRightTyre;
    private String PhotoPathDamagedPartImage1;
    private String PhotoPathDamagedPartImage2;
    private String PhotoPathDamagedPartImage3;
    private String PhotoPathDamagedPartImage4;
    private String PhotoPathRoofTopImage;
    private String PhotoPathRCCopy1;
    private String PhotoPathRCCopy2;
    private String PhotoPathRCCopy3;
    private String strBodyRemark;
    private String strEngineRemark;
    private String strFuelAndIgnitionSystemRemark;
    private String strTransmissionRemark;
    private String strBreakSystemRemark;
    private String strSuspensionSystemRemarks;
    private String strSteeringSystemRemark;
    private String strACSystemRemark;
    private String strElectricalSystemRemark;
    private String strGeneralServiceCostRemark;
    private String EvaluationStatus;
    private String CNGKit;
    private String RcRemarks;
    private String AccidentalType;
    private String AccidentalRemarks;
    private String Latitude;
    private String Longitude;
    private String ScrapVehicle;

    private String strBodyRemarkOther;
    private String strEngineRemarkOther;
    private String strFuelAndIgnitionSystemRemarkOther;
    private String strTransmissionRemarkOther;
    private String strBreakSystemRemarkOther;
    private String strSuspensionSystemRemarksOther;
    private String strSteeringSystemRemarkOther;
    private String strACSystemRemarkOther;
    private String strElectricalSystemRemarkOther;
    private String FitnessCertificate;
    private String RoadTax;
    private String RoadTaxExpiryDate;
    private String FrontBumper;
    private String OutsideRearViewMirrorLH;
    private String OutsideRearViewMirrorRH;
    private String PillarType;
    private String DickyDoor;
    private String BodyShell;
    private String Chassis;
    private String ApronFrontLH;
    private String ApronFrontRH;
    private String StrutMountingArea;
    private String Firewall;
    private String CowlTop;
    private String UpperCrossMember;
    private String LowerCrossMember;
    private String RadiatorSupport;
    private String EngineRoomCarrierAssembly;
    private String HeadLightLH;
    private String HeadLightRH;
    private String TailLightLH;
    private String TailLightRH;
    private String RunningBoardLH;
    private String RunningBoardRH;
    private String PowerWindowType;
    private String AllWindowCondition;
    private String FrontLHWindow;
    private String FrontRHWindow;
    private String RearLHWindow;
    private String RearRHWindow;
    private String PlatformPassengerCabin;
    private String PlatformBootSpace;
    private String OverallFeedbackRating;
    private String AudioPathVehicleNoise;
    private String EvaluationCount;
    private String VehicleUsage;

    public NewEvaluation() {

    }

    protected NewEvaluation(Parcel in) {
        newObject = in.readByte() != 0;
        Id = in.readString();
        requestId = in.readString();
        EvaluatorId = in.readString();
        EvaluatorContactNo = in.readString();
        EvaluationNo = in.readString();
        AgainstTransaction = in.readString();
        Purpose = in.readString();
        OptedNewVehicle = in.readString();
        Date = in.readString();
        Time = in.readString();
        TransactionNo = in.readString();
        EvaluatorName = in.readString();
        SourceOfTransaction = in.readString();
        OwnerName = in.readString();
        Address = in.readString();
        EmailId = in.readString();
        PhoneNo = in.readString();
        MobileNo = in.readString();
        State = in.readString();
        City = in.readString();
        PinCode = in.readString();
        Occupation = in.readString();
        Make = in.readString();
        Model = in.readString();
        Variant = in.readString();
        YearOfMfg = in.readString();
        YearOfReg = in.readString();
        RegNo = in.readString();
        Mileage = in.readString();
        MonthOfMfg = in.readString();
        MonthOfReg = in.readString();
        EngineNo = in.readString();
        ChassisNo = in.readString();
        NoOfOwners = in.readString();
        Transmission = in.readString();
        ServiceBooklet = in.readString();
        Color = in.readString();
        FuelType = in.readString();
        EuroVersion = in.readString();
        BodyType = in.readString();
        NoOfSeats = in.readString();
        RCBook = in.readString();
        Insurance = in.readString();
        InsuranceDate = in.readString();
        Ncb = in.readString();
        Puc = in.readString();
        Hypothecation = in.readString();
        FinanceCompany = in.readString();
        NocStatus = in.readString();
        CustomerExpectedPrice = in.readString();
        TestDrive = in.readString();
        IsAccident = in.readString();
        BodyCost = in.readString();
        EngineCost = in.readString();
        FuelAndIgnitionSystemCost = in.readString();
        TransmissionCost = in.readString();
        BreakSystemCost = in.readString();
        SuspensionCost = in.readString();
        SteeringSystemCost = in.readString();
        AcCost = in.readString();
        ElectricalSystemCost = in.readString();
        totalOfAggregateCost = in.readString();
        generalServiceCost = in.readString();
        totalRefurbishmentCost = in.readString();
        Stereo = in.readString();
        NoOfKeys = in.readString();
        ReverseParking = in.readString();
        PowerSteering = in.readString();
        AlloyWheels = in.readString();
        FogLamp = in.readString();
        CentralLocking = in.readString();
        ToolKitJack = in.readString();
        Comment = in.readString();
        TubelessTyres = in.readString();
        RearWiper = in.readString();
        MusicSystem = in.readString();
        FMRadio = in.readString();
        PowerWindow = in.readString();
        AC = in.readString();
        PowerAdjustableSeats = in.readString();
        KeyLessEntry = in.readString();
        PedalShifter = in.readString();
        ElectricallyAdjustableORVM = in.readString();
        RearDefogger = in.readString();
        SunRoof = in.readString();
        AutoClimateTechnologies = in.readString();
        LeatherSeats = in.readString();
        BluetoothAudioSystems = in.readString();
        GPSNavigationSystems = in.readString();
        Odometer = in.readString();
        SeatCover = in.readString();
        Dashboard = in.readString();
        AirBags = in.readString();
        FrontFenderRightSide = in.readString();
        FrontDoorRightSide = in.readString();
        BackDoorRightSide = in.readString();
        BackFenderRightSide = in.readString();
        Bonnet = in.readString();
        TailGate = in.readString();
        BackBumper = in.readString();
        FrontFenderLeftHandSide = in.readString();
        FrontDoorLeftHandSide = in.readString();
        BackDoorLeftHandSide = in.readString();
        BackFenderLeftHandSide = in.readString();
        FrontLight = in.readString();
        BackLight = in.readString();
        WindShield = in.readString();
        Pillars = in.readString();
        Mirrors = in.readString();
        Wheels = in.readString();
        TyreFrontLeft = in.readString();
        TyreFrontRight = in.readString();
        TyreRearLeft = in.readString();
        TyreRearRight = in.readString();
        SpareTyre = in.readString();
        PhotoPathFrontImage = in.readString();
        PhotoPathRearImage = in.readString();
        PhotoPath45DegreeLeftView = in.readString();
        PhotoPath45DegreeRightView = in.readString();
        PhotoPathDashboardView = in.readString();
        PhotoPathOdometerImage = in.readString();
        PhotoPathChassisNoImage = in.readString();
        PhotoPathEngineNoImage = in.readString();
        PhotoPathInteriorView = in.readString();
        PhotoPathRCCopyImage = in.readString();
        PhotoPathInsuranceImage = in.readString();
        PhotoPathRearLeftTyre = in.readString();
        PhotoPathRearRightTyre = in.readString();
        PhotoPathFrontLeftTyre = in.readString();
        PhotoPathFrontRightTyre = in.readString();
        PhotoPathDamagedPartImage1 = in.readString();
        PhotoPathDamagedPartImage2 = in.readString();
        PhotoPathDamagedPartImage3 = in.readString();
        PhotoPathDamagedPartImage4 = in.readString();
        PhotoPathRoofTopImage = in.readString();
        PhotoPathRCCopy1 = in.readString();
        PhotoPathRCCopy2 = in.readString();
        PhotoPathRCCopy3 = in.readString();
        strBodyRemark = in.readString();
        strEngineRemark = in.readString();
        strFuelAndIgnitionSystemRemark = in.readString();
        strTransmissionRemark = in.readString();
        strBreakSystemRemark = in.readString();
        strSuspensionSystemRemarks = in.readString();
        strSteeringSystemRemark = in.readString();
        strACSystemRemark = in.readString();
        strElectricalSystemRemark = in.readString();
        strGeneralServiceCostRemark = in.readString();
        EvaluationStatus = in.readString();
        CNGKit = in.readString();
        RcRemarks = in.readString();
        AccidentalType = in.readString();
        AccidentalRemarks = in.readString();
        Latitude = in.readString();
        Longitude = in.readString();
        ScrapVehicle = in.readString();
        strBodyRemarkOther = in.readString();
        strEngineRemarkOther = in.readString();
        strFuelAndIgnitionSystemRemarkOther = in.readString();
        strTransmissionRemarkOther = in.readString();
        strBreakSystemRemarkOther = in.readString();
        strSuspensionSystemRemarksOther = in.readString();
        strSteeringSystemRemarkOther = in.readString();
        strACSystemRemarkOther = in.readString();
        strElectricalSystemRemarkOther = in.readString();
        FitnessCertificate = in.readString();
        RoadTax = in.readString();
        RoadTaxExpiryDate = in.readString();
        FrontBumper = in.readString();
        OutsideRearViewMirrorLH = in.readString();
        OutsideRearViewMirrorRH = in.readString();
        PillarType = in.readString();
        DickyDoor = in.readString();
        BodyShell = in.readString();
        Chassis = in.readString();
        ApronFrontLH = in.readString();
        ApronFrontRH = in.readString();
        StrutMountingArea = in.readString();
        Firewall = in.readString();
        CowlTop = in.readString();
        UpperCrossMember = in.readString();
        LowerCrossMember = in.readString();
        RadiatorSupport = in.readString();
        EngineRoomCarrierAssembly = in.readString();
        HeadLightLH = in.readString();
        HeadLightRH = in.readString();
        TailLightLH = in.readString();
        TailLightRH = in.readString();
        RunningBoardLH = in.readString();
        RunningBoardRH = in.readString();
        PowerWindowType = in.readString();
        AllWindowCondition = in.readString();
        FrontLHWindow = in.readString();
        FrontRHWindow = in.readString();
        RearLHWindow = in.readString();
        RearRHWindow = in.readString();
        PlatformPassengerCabin = in.readString();
        PlatformBootSpace = in.readString();
        OverallFeedbackRating = in.readString();
        AudioPathVehicleNoise = in.readString();
        EvaluationCount = in.readString();
        VehicleUsage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (newObject ? 1 : 0));
        dest.writeString(Id);
        dest.writeString(requestId);
        dest.writeString(EvaluatorId);
        dest.writeString(EvaluatorContactNo);
        dest.writeString(EvaluationNo);
        dest.writeString(AgainstTransaction);
        dest.writeString(Purpose);
        dest.writeString(OptedNewVehicle);
        dest.writeString(Date);
        dest.writeString(Time);
        dest.writeString(TransactionNo);
        dest.writeString(EvaluatorName);
        dest.writeString(SourceOfTransaction);
        dest.writeString(OwnerName);
        dest.writeString(Address);
        dest.writeString(EmailId);
        dest.writeString(PhoneNo);
        dest.writeString(MobileNo);
        dest.writeString(State);
        dest.writeString(City);
        dest.writeString(PinCode);
        dest.writeString(Occupation);
        dest.writeString(Make);
        dest.writeString(Model);
        dest.writeString(Variant);
        dest.writeString(YearOfMfg);
        dest.writeString(YearOfReg);
        dest.writeString(RegNo);
        dest.writeString(Mileage);
        dest.writeString(MonthOfMfg);
        dest.writeString(MonthOfReg);
        dest.writeString(EngineNo);
        dest.writeString(ChassisNo);
        dest.writeString(NoOfOwners);
        dest.writeString(Transmission);
        dest.writeString(ServiceBooklet);
        dest.writeString(Color);
        dest.writeString(FuelType);
        dest.writeString(EuroVersion);
        dest.writeString(BodyType);
        dest.writeString(NoOfSeats);
        dest.writeString(RCBook);
        dest.writeString(Insurance);
        dest.writeString(InsuranceDate);
        dest.writeString(Ncb);
        dest.writeString(Puc);
        dest.writeString(Hypothecation);
        dest.writeString(FinanceCompany);
        dest.writeString(NocStatus);
        dest.writeString(CustomerExpectedPrice);
        dest.writeString(TestDrive);
        dest.writeString(IsAccident);
        dest.writeString(BodyCost);
        dest.writeString(EngineCost);
        dest.writeString(FuelAndIgnitionSystemCost);
        dest.writeString(TransmissionCost);
        dest.writeString(BreakSystemCost);
        dest.writeString(SuspensionCost);
        dest.writeString(SteeringSystemCost);
        dest.writeString(AcCost);
        dest.writeString(ElectricalSystemCost);
        dest.writeString(totalOfAggregateCost);
        dest.writeString(generalServiceCost);
        dest.writeString(totalRefurbishmentCost);
        dest.writeString(Stereo);
        dest.writeString(NoOfKeys);
        dest.writeString(ReverseParking);
        dest.writeString(PowerSteering);
        dest.writeString(AlloyWheels);
        dest.writeString(FogLamp);
        dest.writeString(CentralLocking);
        dest.writeString(ToolKitJack);
        dest.writeString(Comment);
        dest.writeString(TubelessTyres);
        dest.writeString(RearWiper);
        dest.writeString(MusicSystem);
        dest.writeString(FMRadio);
        dest.writeString(PowerWindow);
        dest.writeString(AC);
        dest.writeString(PowerAdjustableSeats);
        dest.writeString(KeyLessEntry);
        dest.writeString(PedalShifter);
        dest.writeString(ElectricallyAdjustableORVM);
        dest.writeString(RearDefogger);
        dest.writeString(SunRoof);
        dest.writeString(AutoClimateTechnologies);
        dest.writeString(LeatherSeats);
        dest.writeString(BluetoothAudioSystems);
        dest.writeString(GPSNavigationSystems);
        dest.writeString(Odometer);
        dest.writeString(SeatCover);
        dest.writeString(Dashboard);
        dest.writeString(AirBags);
        dest.writeString(FrontFenderRightSide);
        dest.writeString(FrontDoorRightSide);
        dest.writeString(BackDoorRightSide);
        dest.writeString(BackFenderRightSide);
        dest.writeString(Bonnet);
        dest.writeString(TailGate);
        dest.writeString(BackBumper);
        dest.writeString(FrontFenderLeftHandSide);
        dest.writeString(FrontDoorLeftHandSide);
        dest.writeString(BackDoorLeftHandSide);
        dest.writeString(BackFenderLeftHandSide);
        dest.writeString(FrontLight);
        dest.writeString(BackLight);
        dest.writeString(WindShield);
        dest.writeString(Pillars);
        dest.writeString(Mirrors);
        dest.writeString(Wheels);
        dest.writeString(TyreFrontLeft);
        dest.writeString(TyreFrontRight);
        dest.writeString(TyreRearLeft);
        dest.writeString(TyreRearRight);
        dest.writeString(SpareTyre);
        dest.writeString(PhotoPathFrontImage);
        dest.writeString(PhotoPathRearImage);
        dest.writeString(PhotoPath45DegreeLeftView);
        dest.writeString(PhotoPath45DegreeRightView);
        dest.writeString(PhotoPathDashboardView);
        dest.writeString(PhotoPathOdometerImage);
        dest.writeString(PhotoPathChassisNoImage);
        dest.writeString(PhotoPathEngineNoImage);
        dest.writeString(PhotoPathInteriorView);
        dest.writeString(PhotoPathRCCopyImage);
        dest.writeString(PhotoPathInsuranceImage);
        dest.writeString(PhotoPathRearLeftTyre);
        dest.writeString(PhotoPathRearRightTyre);
        dest.writeString(PhotoPathFrontLeftTyre);
        dest.writeString(PhotoPathFrontRightTyre);
        dest.writeString(PhotoPathDamagedPartImage1);
        dest.writeString(PhotoPathDamagedPartImage2);
        dest.writeString(PhotoPathDamagedPartImage3);
        dest.writeString(PhotoPathDamagedPartImage4);
        dest.writeString(PhotoPathRoofTopImage);
        dest.writeString(PhotoPathRCCopy1);
        dest.writeString(PhotoPathRCCopy2);
        dest.writeString(PhotoPathRCCopy3);
        dest.writeString(strBodyRemark);
        dest.writeString(strEngineRemark);
        dest.writeString(strFuelAndIgnitionSystemRemark);
        dest.writeString(strTransmissionRemark);
        dest.writeString(strBreakSystemRemark);
        dest.writeString(strSuspensionSystemRemarks);
        dest.writeString(strSteeringSystemRemark);
        dest.writeString(strACSystemRemark);
        dest.writeString(strElectricalSystemRemark);
        dest.writeString(strGeneralServiceCostRemark);
        dest.writeString(EvaluationStatus);
        dest.writeString(CNGKit);
        dest.writeString(RcRemarks);
        dest.writeString(AccidentalType);
        dest.writeString(AccidentalRemarks);
        dest.writeString(Latitude);
        dest.writeString(Longitude);
        dest.writeString(ScrapVehicle);
        dest.writeString(strBodyRemarkOther);
        dest.writeString(strEngineRemarkOther);
        dest.writeString(strFuelAndIgnitionSystemRemarkOther);
        dest.writeString(strTransmissionRemarkOther);
        dest.writeString(strBreakSystemRemarkOther);
        dest.writeString(strSuspensionSystemRemarksOther);
        dest.writeString(strSteeringSystemRemarkOther);
        dest.writeString(strACSystemRemarkOther);
        dest.writeString(strElectricalSystemRemarkOther);
        dest.writeString(FitnessCertificate);
        dest.writeString(RoadTax);
        dest.writeString(RoadTaxExpiryDate);
        dest.writeString(FrontBumper);
        dest.writeString(OutsideRearViewMirrorLH);
        dest.writeString(OutsideRearViewMirrorRH);
        dest.writeString(PillarType);
        dest.writeString(DickyDoor);
        dest.writeString(BodyShell);
        dest.writeString(Chassis);
        dest.writeString(ApronFrontLH);
        dest.writeString(ApronFrontRH);
        dest.writeString(StrutMountingArea);
        dest.writeString(Firewall);
        dest.writeString(CowlTop);
        dest.writeString(UpperCrossMember);
        dest.writeString(LowerCrossMember);
        dest.writeString(RadiatorSupport);
        dest.writeString(EngineRoomCarrierAssembly);
        dest.writeString(HeadLightLH);
        dest.writeString(HeadLightRH);
        dest.writeString(TailLightLH);
        dest.writeString(TailLightRH);
        dest.writeString(RunningBoardLH);
        dest.writeString(RunningBoardRH);
        dest.writeString(PowerWindowType);
        dest.writeString(AllWindowCondition);
        dest.writeString(FrontLHWindow);
        dest.writeString(FrontRHWindow);
        dest.writeString(RearLHWindow);
        dest.writeString(RearRHWindow);
        dest.writeString(PlatformPassengerCabin);
        dest.writeString(PlatformBootSpace);
        dest.writeString(OverallFeedbackRating);
        dest.writeString(AudioPathVehicleNoise);
        dest.writeString(EvaluationCount);
        dest.writeString(VehicleUsage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewEvaluation> CREATOR = new Creator<NewEvaluation>() {
        @Override
        public NewEvaluation createFromParcel(Parcel in) {
            return new NewEvaluation(in);
        }

        @Override
        public NewEvaluation[] newArray(int size) {
            return new NewEvaluation[size];
        }
    };

    public String getEvaluationStatus() {
        return (null != EvaluationStatus) ? EvaluationStatus : "";
    }

    public void setEvaluationStatus(String evaluationStatus) {
        EvaluationStatus = evaluationStatus;
    }

    public String getEvaluatorContactNo() {
        return (null != EvaluatorContactNo) ? EvaluatorContactNo : "";
    }

    public void setEvaluatorContactNo(String evaluatorContactNo) {
        EvaluatorContactNo = evaluatorContactNo;
    }

    public String getId() {
        return (null != Id) ? Id : "";
    }

    public void setId(String id) {
        Id = id;
    }

    public String getRequestId() {
        return (null != requestId) ? requestId : "";
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getEvaluatorId() {
        return (null != EvaluatorId) ? EvaluatorId : "";
    }

    public void setEvaluatorId(String evaluatorId) {
        EvaluatorId = evaluatorId;
    }

    public String getEvaluationNo() {
        return (null != EvaluationNo) ? EvaluationNo : "";
    }

    public void setEvaluationNo(String evaluationNo) {
        EvaluationNo = evaluationNo;
    }

    public String getAgainstTransaction() {
        return (null != AgainstTransaction) ? AgainstTransaction : "";
    }

    public void setAgainstTransaction(String againstTransaction) {
        AgainstTransaction = againstTransaction;
    }

    public String getPurpose() {
        return (null != Purpose) ? Purpose : "";
    }

    public void setPurpose(String purpose) {
        Purpose = purpose;
    }

    public String getOptedNewVehicle() {
        return (null != OptedNewVehicle) ? OptedNewVehicle : "";
    }

    public void setOptedNewVehicle(String optedNewVehicle) {
        OptedNewVehicle = optedNewVehicle;
    }

    public String getDate() {
        return (null != Date) ? Date : "";
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return (null != Time) ? Time : "";
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getTransactionNo() {
        return (null != TransactionNo) ? TransactionNo : "";
    }

    public void setTransactionNo(String transactionNo) {
        TransactionNo = transactionNo;
    }

    public String getEvaluatorName() {
        return (null != EvaluatorName) ? EvaluatorName : "";
    }

    public void setEvaluatorName(String evaluatorName) {
        EvaluatorName = evaluatorName;
    }

    public String getSourceOfTransaction() {
        return (null != SourceOfTransaction) ? SourceOfTransaction : "";
    }

    public void setSourceOfTransaction(String sourceOfTransaction) {
        SourceOfTransaction = sourceOfTransaction;
    }

    public String getOwnerName() {
        return (null != OwnerName) ? OwnerName : "";
    }

    public void setOwnerName(String ownerName) {
        OwnerName = ownerName;
    }

    public String getAddress() {
        return (null != Address) ? Address : "";
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEmailId() {
        return (null != EmailId) ? EmailId : "";
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getPhoneNo() {
        return (null != PhoneNo) ? PhoneNo : "";
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getMobileNo() {
        return (null != MobileNo) ? MobileNo : "";
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getState() {
        return (null != State) ? State : "";
    }

    public void setState(String state) {
        State = state;
    }

    public String getCity() {
        return (null != City) ? City : "";
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPinCode() {
        return (null != PinCode) ? PinCode : "";
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getOccupation() {
        return (null != Occupation) ? Occupation : "";
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getMake() {
        return (null != Make) ? Make : "";
    }

    public void setMake(String make) {
        Make = make;
    }

    public String getModel() {
        return (null != Model) ? Model : "";
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getVariant() {
        return (null != Variant) ? Variant : "";
    }

    public void setVariant(String variant) {
        Variant = variant;
    }

    public String getYearOfMfg() {
        return (null != YearOfMfg) ? YearOfMfg : "";
    }

    public void setYearOfMfg(String yearOfMfg) {
        YearOfMfg = yearOfMfg;
    }

    public String getYearOfReg() {
        return (null != YearOfReg) ? YearOfReg : "";
    }

    public void setYearOfReg(String yearOfReg) {
        YearOfReg = yearOfReg;
    }

    public String getRegNo() {
        return (null != RegNo) ? RegNo : "";
    }

    public void setRegNo(String regNo) {
        RegNo = regNo;
    }

    public String getMileage() {
        return (null != Mileage) ? Mileage : "";
    }

    public void setMileage(String mileage) {
        Mileage = mileage;
    }

    public String getMonthOfMfg() {
        return (null != MonthOfMfg) ? MonthOfMfg : "";
    }

    public void setMonthOfMfg(String monthOfMfg) {
        MonthOfMfg = monthOfMfg;
    }

    public String getMonthOfReg() {
        return (null != MonthOfReg) ? MonthOfReg : "";
    }

    public void setMonthOfReg(String monthOfReg) {
        MonthOfReg = monthOfReg;
    }

    public String getEngineNo() {
        return (null != EngineNo) ? EngineNo : "";
    }

    public void setEngineNo(String engineNo) {
        EngineNo = engineNo;
    }

    public String getChassisNo() {
        return (null != ChassisNo) ? ChassisNo : "";
    }

    public void setChassisNo(String chassisNo) {
        ChassisNo = chassisNo;
    }

    public String getNoOfOwners() {
        return (null != NoOfOwners) ? NoOfOwners : "";
    }

    public void setNoOfOwners(String noOfOwners) {
        NoOfOwners = noOfOwners;
    }

    public String getTransmission() {
        return (null != Transmission) ? Transmission : "";
    }

    public void setTransmission(String transmission) {
        Transmission = transmission;
    }

    public String getServiceBooklet() {
        return (null != ServiceBooklet) ? ServiceBooklet : "";
    }

    public void setServiceBooklet(String serviceBooklet) {
        ServiceBooklet = serviceBooklet;
    }

    public String getColor() {
        return (null != Color) ? Color : "";
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getFuelType() {
        return (null != FuelType) ? FuelType : "";
    }

    public void setFuelType(String fuelType) {
        FuelType = fuelType;
    }

    public String getEuroVersion() {
        return (null != EuroVersion) ? EuroVersion : "";
    }

    public void setEuroVersion(String euroVersion) {
        EuroVersion = euroVersion;
    }

    public String getBodyType() {
        return (null != BodyType) ? BodyType : "";
    }

    public void setBodyType(String bodyType) {
        BodyType = bodyType;
    }

    public String getNoOfSeats() {
        return (null != NoOfSeats) ? NoOfSeats : "";
    }

    public void setNoOfSeats(String noOfSeats) {
        NoOfSeats = noOfSeats;
    }

    public String getRCBook() {
        return (null != RCBook) ? RCBook : "";
    }

    public void setRCBook(String RCBook) {
        this.RCBook = RCBook;
    }

    public String getInsurance() {
        return (null != Insurance) ? Insurance : "";
    }

    public void setInsurance(String insurance) {
        Insurance = insurance;
    }

    public String getInsuranceDate() {
        return (null != InsuranceDate) ? InsuranceDate : "";
    }

    public void setInsuranceDate(String insuranceDate) {
        InsuranceDate = insuranceDate;
    }

    public String getNcb() {
        return (null != Ncb) ? Ncb : "";
    }

    public void setNcb(String ncb) {
        Ncb = ncb;
    }

    public String getPuc() {
        return (null != Puc) ? Puc : "";
    }

    public void setPuc(String puc) {
        Puc = puc;
    }

    public String getHypothecation() {
        return (null != Hypothecation) ? Hypothecation : "";
    }

    public void setHypothecation(String hypothecation) {
        Hypothecation = hypothecation;
    }

    public String getFinanceCompany() {
        return (null != FinanceCompany) ? FinanceCompany : "";
    }

    public void setFinanceCompany(String financeCompany) {
        FinanceCompany = financeCompany;
    }

    public String getNocStatus() {
        return (null != NocStatus) ? NocStatus : "";
    }

    public void setNocStatus(String nocStatus) {
        NocStatus = nocStatus;
    }

    public String getCustomerExpectedPrice() {
        return (null != CustomerExpectedPrice) ? CustomerExpectedPrice : "";
    }

    public void setCustomerExpectedPrice(String customerExpectedPrice) {
        CustomerExpectedPrice = customerExpectedPrice;
    }

    public String getTestDrive() {
        return (null != TestDrive) ? TestDrive : "";
    }

    public void setTestDrive(String testDrive) {
        TestDrive = testDrive;
    }

    public String getIsAccident() {
        return (null != IsAccident) ? IsAccident : "";
    }

    public void setIsAccident(String isAccident) {
        IsAccident = isAccident;
    }

    public String getBodyCost() {
        return (null != BodyCost) ? BodyCost : "";
    }

    public void setBodyCost(String bodyCost) {
        BodyCost = bodyCost;
    }

    public String getEngineCost() {
        return (null != EngineCost) ? EngineCost : "";
    }

    public void setEngineCost(String engineCost) {
        EngineCost = engineCost;
    }

    public String getFuelAndIgnitionSystemCost() {
        return (null != FuelAndIgnitionSystemCost) ? FuelAndIgnitionSystemCost : "";
    }

    public void setFuelAndIgnitionSystemCost(String fuelAndIgnitionSystemCost) {
        FuelAndIgnitionSystemCost = fuelAndIgnitionSystemCost;
    }

    public String getTransmissionCost() {
        return (null != TransmissionCost) ? TransmissionCost : "";
    }

    public void setTransmissionCost(String transmissionCost) {
        TransmissionCost = transmissionCost;
    }

    public String getBreakSystemCost() {
        return (null != BreakSystemCost) ? BreakSystemCost : "";
    }

    public void setBreakSystemCost(String breakSystemCost) {
        BreakSystemCost = breakSystemCost;
    }

    public String getSuspensionCost() {
        return (null != SuspensionCost) ? SuspensionCost : "";
    }

    public void setSuspensionCost(String suspensionCost) {
        SuspensionCost = suspensionCost;
    }

    public String getSteeringSystemCost() {
        return (null != SteeringSystemCost) ? SteeringSystemCost : "";
    }

    public void setSteeringSystemCost(String steeringSystemCost) {
        SteeringSystemCost = steeringSystemCost;
    }

    public String getAcCost() {
        return (null != AcCost) ? AcCost : "";
    }

    public void setAcCost(String acCost) {
        AcCost = acCost;
    }

    public String getElectricalSystemCost() {
        return (null != ElectricalSystemCost) ? ElectricalSystemCost : "";
    }

    public void setElectricalSystemCost(String electricalSystemCost) {
        ElectricalSystemCost = electricalSystemCost;
    }

    public String getTotalOfAggregateCost() {
        return (null != totalOfAggregateCost) ? totalOfAggregateCost : "";
    }

    public void setTotalOfAggregateCost(String totalOfAggregateCost) {
        this.totalOfAggregateCost = totalOfAggregateCost;
    }

    public String getGeneralServiceCost() {
        return (null != generalServiceCost) ? generalServiceCost : "";
    }

    public void setGeneralServiceCost(String generalServiceCost) {
        this.generalServiceCost = generalServiceCost;
    }

    public String getTotalRefurbishmentCost() {
        return (null != totalRefurbishmentCost) ? totalRefurbishmentCost : "";
    }

    public void setTotalRefurbishmentCost(String totalRefurbishmentCost) {
        this.totalRefurbishmentCost = totalRefurbishmentCost;
    }

    public String getStereo() {
        return (null != Stereo) ? Stereo : "";
    }

    public void setStereo(String stereo) {
        Stereo = stereo;
    }

    public String getNoOfKeys() {
        return (null != NoOfKeys) ? NoOfKeys : "";
    }

    public void setNoOfKeys(String noOfKeys) {
        NoOfKeys = noOfKeys;
    }

    public String getReverseParking() {
        return (null != ReverseParking) ? ReverseParking : "";
    }

    public void setReverseParking(String reverseParking) {
        ReverseParking = reverseParking;
    }

    public String getPowerSteering() {
        return (null != PowerSteering) ? PowerSteering : "";
    }

    public void setPowerSteering(String powerSteering) {
        PowerSteering = powerSteering;
    }

    public String getAlloyWheels() {
        return (null != AlloyWheels) ? AlloyWheels : "";
    }

    public void setAlloyWheels(String alloyWheels) {
        AlloyWheels = alloyWheels;
    }

    public String getFogLamp() {
        return (null != FogLamp) ? FogLamp : "";
    }

    public void setFogLamp(String fogLamp) {
        FogLamp = fogLamp;
    }

    public String getCentralLocking() {
        return (null != CentralLocking) ? CentralLocking : "";
    }

    public void setCentralLocking(String centralLocking) {
        CentralLocking = centralLocking;
    }

    public String getToolKitJack() {
        return (null != ToolKitJack) ? ToolKitJack : "";
    }

    public void setToolKitJack(String toolKitJack) {
        ToolKitJack = toolKitJack;
    }

    public String getComment() {
        return (null != Comment) ? Comment : "";
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getTubelessTyres() {
        return (null != TubelessTyres) ? TubelessTyres : "";
    }

    public void setTubelessTyres(String tubelessTyres) {
        this.TubelessTyres = tubelessTyres;
    }

    public String getRearWiper() {
        return (null != RearWiper) ? RearWiper : "";
    }

    public void setRearWiper(String rearWiper) {
        RearWiper = rearWiper;
    }

    public String getMusicSystem() {
        return (null != MusicSystem) ? MusicSystem : "";
    }

    public void setMusicSystem(String musicSystem) {
        MusicSystem = musicSystem;
    }

    public String getFMRadio() {
        return (null != FMRadio) ? FMRadio : "";
    }

    public void setFMRadio(String FMRadio) {
        this.FMRadio = FMRadio;
    }

    public String getPowerWindow() {
        return (null != PowerWindow) ? PowerWindow : "";
    }

    public void setPowerWindow(String powerWindow) {
        PowerWindow = powerWindow;
    }

    public String getAC() {
        return (null != AC) ? AC : "";
    }

    public void setAC(String AC) {
        this.AC = AC;
    }

    public String getPowerAdjustableSeats() {
        return (null != PowerAdjustableSeats) ? PowerAdjustableSeats : "";
    }

    public void setPowerAdjustableSeats(String powerAdjustableSeats) {
        PowerAdjustableSeats = powerAdjustableSeats;
    }

    public String getKeyLessEntry() {
        return (null != KeyLessEntry) ? KeyLessEntry : "";
    }

    public void setKeyLessEntry(String keyLessEntry) {
        KeyLessEntry = keyLessEntry;
    }

    public String getPedalShifter() {
        return (null != PedalShifter) ? PedalShifter : "";
    }

    public void setPedalShifter(String pedalShifter) {
        PedalShifter = pedalShifter;
    }

    public String getElectricallyAdjustableORVM() {
        return (null != ElectricallyAdjustableORVM) ? ElectricallyAdjustableORVM : "";
    }

    public void setElectricallyAdjustableORVM(String electricallyAdjustableORVM) {
        ElectricallyAdjustableORVM = electricallyAdjustableORVM;
    }

    public String getRearDefogger() {
        return (null != RearDefogger) ? RearDefogger : "";
    }

    public void setRearDefogger(String rearDefogger) {
        RearDefogger = rearDefogger;
    }

    public String getSunRoof() {
        return (null != SunRoof) ? SunRoof : "";
    }

    public void setSunRoof(String sunRoof) {
        SunRoof = sunRoof;
    }

    public String getAutoClimateTechnologies() {
        return (null != AutoClimateTechnologies) ? AutoClimateTechnologies : "";
    }

    public void setAutoClimateTechnologies(String autoClimateTechnologies) {
        AutoClimateTechnologies = autoClimateTechnologies;
    }

    public String getLeatherSeats() {
        return (null != LeatherSeats) ? LeatherSeats : "";
    }

    public void setLeatherSeats(String leatherSeats) {
        LeatherSeats = leatherSeats;
    }

    public String getBluetoothAudioSystems() {
        return (null != BluetoothAudioSystems) ? BluetoothAudioSystems : "";
    }

    public void setBluetoothAudioSystems(String bluetoothAudioSystems) {
        BluetoothAudioSystems = bluetoothAudioSystems;
    }

    public String getGPSNavigationSystems() {
        return (null != GPSNavigationSystems) ? GPSNavigationSystems : "";
    }

    public void setGPSNavigationSystems(String GPSNavigationSystems) {
        this.GPSNavigationSystems = GPSNavigationSystems;
    }

    public String getOdometer() {
        return (null != Odometer) ? Odometer : "";
    }

    public void setOdometer(String odometer) {
        Odometer = odometer;
    }

    public String getSeatCover() {
        return (null != SeatCover) ? SeatCover : "";
    }

    public void setSeatCover(String seatCover) {
        SeatCover = seatCover;
    }

    public String getDashboard() {
        return (null != Dashboard) ? Dashboard : "";
    }

    public void setDashboard(String dashboard) {
        Dashboard = dashboard;
    }

    public String getAirBags() {
        return (null != AirBags) ? AirBags : "";
    }

    public void setAirBags(String airBags) {
        AirBags = airBags;
    }

    public String getFrontFenderRightSide() {
        return (null != FrontFenderRightSide) ? FrontFenderRightSide : "";
    }

    public void setFrontFenderRightSide(String frontFenderRightSide) {
        FrontFenderRightSide = frontFenderRightSide;
    }

    public String getFrontDoorRightSide() {
        return (null != FrontDoorRightSide) ? FrontDoorRightSide : "";
    }

    public void setFrontDoorRightSide(String frontDoorRightSide) {
        FrontDoorRightSide = frontDoorRightSide;
    }

    public String getBackDoorRightSide() {
        return (null != BackDoorRightSide) ? BackDoorRightSide : "";
    }

    public void setBackDoorRightSide(String backDoorRightSide) {
        BackDoorRightSide = backDoorRightSide;
    }

    public String getBackFenderRightSide() {
        return (null != BackFenderRightSide) ? BackFenderRightSide : "";
    }

    public void setBackFenderRightSide(String backFenderRightSide) {
        BackFenderRightSide = backFenderRightSide;
    }

    public String getBonnet() {
        return (null != Bonnet) ? Bonnet : "";
    }

    public void setBonnet(String bonnet) {
        Bonnet = bonnet;
    }

    public String getTailGate() {
        return (null != TailGate) ? TailGate : "";
    }

    public void setTailGate(String tailGate) {
        TailGate = tailGate;
    }

    public String getBackBumper() {
        return (null != BackBumper) ? BackBumper : "";
    }

    public void setBackBumper(String backBumper) {
        BackBumper = backBumper;
    }

    public String getFrontFenderLeftHandSide() {
        return (null != FrontFenderLeftHandSide) ? FrontFenderLeftHandSide : "";
    }

    public void setFrontFenderLeftHandSide(String frontFenderLeftHandSide) {
        FrontFenderLeftHandSide = frontFenderLeftHandSide;
    }

    public String getFrontDoorLeftHandSide() {
        return (null != FrontDoorLeftHandSide) ? FrontDoorLeftHandSide : "";
    }

    public void setFrontDoorLeftHandSide(String frontDoorLeftHandSide) {
        FrontDoorLeftHandSide = frontDoorLeftHandSide;
    }

    public String getBackDoorLeftHandSide() {
        return (null != BackDoorLeftHandSide) ? BackDoorLeftHandSide : "";
    }

    public void setBackDoorLeftHandSide(String backDoorLeftHandSide) {
        BackDoorLeftHandSide = backDoorLeftHandSide;
    }

    public String getBackFenderLeftHandSide() {
        return (null != BackFenderLeftHandSide) ? BackFenderLeftHandSide : "";
    }

    public void setBackFenderLeftHandSide(String backFenderLeftHandSide) {
        BackFenderLeftHandSide = backFenderLeftHandSide;
    }

    public String getFrontLight() {
        return (null != FrontLight) ? FrontLight : "";
    }

    public void setFrontLight(String frontLight) {
        FrontLight = frontLight;
    }

    public String getBackLight() {
        return (null != BackLight) ? BackLight : "";
    }

    public void setBackLight(String backLight) {
        BackLight = backLight;
    }

    public String getWindShield() {
        return (null != WindShield) ? WindShield : "";
    }

    public void setWindShield(String windShield) {
        WindShield = windShield;
    }

    public String getPillars() {
        return (null != Pillars) ? Pillars : "";
    }

    public void setPillars(String pillars) {
        Pillars = pillars;
    }

    public String getMirrors() {
        return (null != Mirrors) ? Mirrors : "";
    }

    public void setMirrors(String mirrors) {
        Mirrors = mirrors;
    }

    public String getWheels() {
        return (null != Wheels) ? Wheels : "";
    }

    public void setWheels(String wheels) {
        Wheels = wheels;
    }

    public String getTyreFrontLeft() {
        return (null != TyreFrontLeft) ? TyreFrontLeft : "";
    }

    public void setTyreFrontLeft(String tyreFrontLeft) {
        TyreFrontLeft = tyreFrontLeft;
    }

    public String getTyreFrontRight() {
        return (null != TyreFrontRight) ? TyreFrontRight : "";
    }

    public void setTyreFrontRight(String tyreFrontRight) {
        TyreFrontRight = tyreFrontRight;
    }

    public String getTyreRearLeft() {
        return (null != TyreRearLeft) ? TyreRearLeft : "";
    }

    public void setTyreRearLeft(String tyreRearLeft) {
        TyreRearLeft = tyreRearLeft;
    }

    public String getTyreRearRight() {
        return (null != TyreRearRight) ? TyreRearRight : "";
    }

    public void setTyreRearRight(String tyreRearRight) {
        TyreRearRight = tyreRearRight;
    }

    public String getSpareTyre() {
        return (null != SpareTyre) ? SpareTyre : "";
    }

    public void setSpareTyre(String spareTyre) {
        SpareTyre = spareTyre;
    }

    public String getPhotoPathFrontImage() {
        return (null != PhotoPathFrontImage) ? PhotoPathFrontImage : "";
    }

    public void setPhotoPathFrontImage(String photoPathFrontImage) {
        PhotoPathFrontImage = photoPathFrontImage;
    }

    public String getPhotoPathRearImage() {
        return (null != PhotoPathRearImage) ? PhotoPathRearImage : "";
    }

    public void setPhotoPathRearImage(String photoPathRearImage) {
        PhotoPathRearImage = photoPathRearImage;
    }

    public String getPhotoPath45DegreeLeftView() {
        return (null != PhotoPath45DegreeLeftView) ? PhotoPath45DegreeLeftView : "";
    }

    public void setPhotoPath45DegreeLeftView(String photoPath45DegreeLeftView) {
        PhotoPath45DegreeLeftView = photoPath45DegreeLeftView;
    }

    public String getPhotoPath45DegreeRightView() {
        return (null != PhotoPath45DegreeRightView) ? PhotoPath45DegreeRightView : "";
    }

    public void setPhotoPath45DegreeRightView(String photoPath45DegreeRightView) {
        PhotoPath45DegreeRightView = photoPath45DegreeRightView;
    }

    public String getPhotoPathDashboardView() {
        return (null != PhotoPathDashboardView) ? PhotoPathDashboardView : "";
    }

    public void setPhotoPathDashboardView(String photoPathDashboardView) {
        PhotoPathDashboardView = photoPathDashboardView;
    }

    public String getPhotoPathOdometerImage() {
        return (null != PhotoPathOdometerImage) ? PhotoPathOdometerImage : "";
    }

    public void setPhotoPathOdometerImage(String photoPathOdometerImage) {
        PhotoPathOdometerImage = photoPathOdometerImage;
    }

    public String getPhotoPathChassisNoImage() {
        return (null != PhotoPathChassisNoImage) ? PhotoPathChassisNoImage : "";
    }

    public void setPhotoPathChassisNoImage(String photoPathChassisNoImage) {
        PhotoPathChassisNoImage = photoPathChassisNoImage;
    }

    public String getPhotoPathEngineNoImage() {
        return (null != PhotoPathEngineNoImage) ? PhotoPathEngineNoImage : "";
    }

    public void setPhotoPathEngineNoImage(String photoPathEngineNoImage) {
        PhotoPathEngineNoImage = photoPathEngineNoImage;
    }

    public String getPhotoPathInteriorView() {
        return (null != PhotoPathInteriorView) ? PhotoPathInteriorView : "";
    }

    public void setPhotoPathInteriorView(String photoPathInteriorView) {
        PhotoPathInteriorView = photoPathInteriorView;
    }

    public String getPhotoPathRCCopyImage() {
        return (null != PhotoPathRCCopyImage) ? PhotoPathRCCopyImage : "";
    }

    public void setPhotoPathRCCopyImage(String photoPathRCCopyImage) {
        PhotoPathRCCopyImage = photoPathRCCopyImage;
    }

    public String getPhotoPathInsuranceImage() {
        return (null != PhotoPathInsuranceImage) ? PhotoPathInsuranceImage : "";
    }

    public void setPhotoPathInsuranceImage(String photoPathInsuranceImage) {
        PhotoPathInsuranceImage = photoPathInsuranceImage;
    }

    public String getPhotoPathRearLeftTyre() {
        return (null != PhotoPathRearLeftTyre) ? PhotoPathRearLeftTyre : "";
    }

    public void setPhotoPathRearLeftTyre(String photoPathRearLeftTyre) {
        PhotoPathRearLeftTyre = photoPathRearLeftTyre;
    }

    public String getPhotoPathRearRightTyre() {
        return (null != PhotoPathRearRightTyre) ? PhotoPathRearRightTyre : "";
    }

    public void setPhotoPathRearRightTyre(String photoPathRearRightTyre) {
        PhotoPathRearRightTyre = photoPathRearRightTyre;
    }

    public String getPhotoPathFrontLeftTyre() {
        return (null != PhotoPathFrontLeftTyre) ? PhotoPathFrontLeftTyre : "";
    }

    public void setPhotoPathFrontLeftTyre(String photoPathFrontLeftTyre) {
        PhotoPathFrontLeftTyre = photoPathFrontLeftTyre;
    }

    public String getPhotoPathFrontRightTyre() {
        return (null != PhotoPathFrontRightTyre) ? PhotoPathFrontRightTyre : "";
    }

    public void setPhotoPathFrontRightTyre(String photoPathFrontRightTyre) {
        PhotoPathFrontRightTyre = photoPathFrontRightTyre;
    }

    public String getPhotoPathDamagedPartImage1() {
        return (null != PhotoPathDamagedPartImage1) ? PhotoPathDamagedPartImage1 : "";
    }

    public void setPhotoPathDamagedPartImage1(String photoPathDamagedPartImage1) {
        PhotoPathDamagedPartImage1 = photoPathDamagedPartImage1;
    }

    public String getPhotoPathDamagedPartImage2() {
        return (null != PhotoPathDamagedPartImage2) ? PhotoPathDamagedPartImage2 : "";
    }

    public void setPhotoPathDamagedPartImage2(String photoPathDamagedPartImage2) {
        PhotoPathDamagedPartImage2 = photoPathDamagedPartImage2;
    }

    public String getPhotoPathDamagedPartImage3() {
        return (null != PhotoPathDamagedPartImage3) ? PhotoPathDamagedPartImage3 : "";
    }

    public void setPhotoPathDamagedPartImage3(String photoPathDamagedPartImage3) {
        PhotoPathDamagedPartImage3 = photoPathDamagedPartImage3;
    }

    public String getPhotoPathDamagedPartImage4() {
        return (null != PhotoPathDamagedPartImage4) ? PhotoPathDamagedPartImage4 : "";
    }

    public void setPhotoPathDamagedPartImage4(String photoPathDamagedPartImage4) {
        PhotoPathDamagedPartImage4 = photoPathDamagedPartImage4;
    }

    public String getPhotoPathRoofTopImage() {
        return (null != PhotoPathRoofTopImage) ? PhotoPathRoofTopImage : "";
    }

    public void setPhotoPathRoofTopImage(String photoPathRoofTopImage) {
        PhotoPathRoofTopImage = photoPathRoofTopImage;
    }

    public String getStrBodyRemark() {
        return (null != strBodyRemark) ? strBodyRemark : "";
    }

    public void setStrBodyRemark(String strBodyRemark) {
        this.strBodyRemark = strBodyRemark;
    }

    public String getStrEngineRemark() {
        return (null != strEngineRemark) ? strEngineRemark : "";
    }

    public void setStrEngineRemark(String strEngineRemark) {
        this.strEngineRemark = strEngineRemark;
    }

    public String getStrFuelAndIgnitionSystemRemark() {
        return (null != strFuelAndIgnitionSystemRemark) ? strFuelAndIgnitionSystemRemark : "";
    }

    public void setStrFuelAndIgnitionSystemRemark(String strFuelAndIgnitionSystemRemark) {
        this.strFuelAndIgnitionSystemRemark = strFuelAndIgnitionSystemRemark;
    }

    public String getStrTransmissionRemark() {
        return (null != strTransmissionRemark) ? strTransmissionRemark : "";
    }

    public void setStrTransmissionRemark(String strTransmissionRemark) {
        this.strTransmissionRemark = strTransmissionRemark;
    }

    public String getStrBreakSystemRemark() {
        return (null != strBreakSystemRemark) ? strBreakSystemRemark : "";
    }

    public void setStrBreakSystemRemark(String strBreakSystemRemark) {
        this.strBreakSystemRemark = strBreakSystemRemark;
    }

    public String getStrSuspensionSystemRemarks() {
        return (null != strSuspensionSystemRemarks) ? strSuspensionSystemRemarks : "";
    }

    public void setStrSuspensionSystemRemarks(String strSuspensionSystemRemarks) {
        this.strSuspensionSystemRemarks = strSuspensionSystemRemarks;
    }

    public String getStrSteeringSystemRemark() {
        return (null != strSteeringSystemRemark) ? strSteeringSystemRemark : "";
    }

    public void setStrSteeringSystemRemark(String strSteeringSystemRemark) {
        this.strSteeringSystemRemark = strSteeringSystemRemark;
    }

    public String getStrACSystemRemark() {
        return (null != strACSystemRemark) ? strACSystemRemark : "";
    }

    public void setStrACSystemRemark(String strACSystemRemark) {
        this.strACSystemRemark = strACSystemRemark;
    }

    public String getStrElectricalSystemRemark() {
        return (null != strElectricalSystemRemark) ? strElectricalSystemRemark : "";
    }

    public void setStrElectricalSystemRemark(String strElectricalSystemRemark) {
        this.strElectricalSystemRemark = strElectricalSystemRemark;
    }

    public String getStrGeneralServiceCostRemark() {
        return (null != strGeneralServiceCostRemark) ? strGeneralServiceCostRemark : "";
    }

    public void setStrGeneralServiceCostRemark(String strGeneralServiceCostRemark) {
        this.strGeneralServiceCostRemark = strGeneralServiceCostRemark;
    }

    public String getCNGKit() {
        return (null != CNGKit) ? CNGKit : "";
    }

    public void setCNGKit(String CNGKit) {
        this.CNGKit = CNGKit;
    }

    public String getRcRemarks() {
        return (null != RcRemarks) ? RcRemarks : "";
    }

    public void setRcRemarks(String rcRemarks) {
        RcRemarks = rcRemarks;
    }

    public String getAccidentalType() {
        return (null != AccidentalType) ? AccidentalType : "";
    }

    public void setAccidentalType(String accidentalType) {
        AccidentalType = accidentalType;
    }

    public String getAccidentalRemarks() {
        return (null != AccidentalRemarks) ? AccidentalRemarks : "";
    }

    public void setAccidentalRemarks(String accidentalRemarks) {
        AccidentalRemarks = accidentalRemarks;
    }

    public String getLatitude() {
        return (null != Latitude) ? Latitude : "";
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return (null != Longitude) ? Longitude : "";
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getScrapVehicle() {
        return (null != ScrapVehicle) ? ScrapVehicle : "";
    }

    public void setScrapVehicle(String scrapVehicle) {
        ScrapVehicle = scrapVehicle;
    }

    public String getPhotoPathRCCopy1() {
        return (null != PhotoPathRCCopy1) ? PhotoPathRCCopy1 : "";
    }

    public void setPhotoPathRCCopy1(String photoPathRCCopy1) {
        PhotoPathRCCopy1 = photoPathRCCopy1;
    }

    public String getPhotoPathRCCopy2() {
        return (null != PhotoPathRCCopy2) ? PhotoPathRCCopy2 : "";
    }

    public void setPhotoPathRCCopy2(String photoPathRCCopy2) {
        PhotoPathRCCopy2 = photoPathRCCopy2;
    }

    public String getPhotoPathRCCopy3() {
        return (null != PhotoPathRCCopy3) ? PhotoPathRCCopy3 : "";
    }

    public void setPhotoPathRCCopy3(String photoPathRCCopy3) {
        PhotoPathRCCopy3 = photoPathRCCopy3;
    }

    public String getStrBodyRemarkOther() {
        return (null != strBodyRemarkOther) ? strBodyRemarkOther : "";
    }

    public void setStrBodyRemarkOther(String strBodyRemarkOther) {
        this.strBodyRemarkOther = strBodyRemarkOther;
    }

    public String getStrEngineRemarkOther() {
        return (null != strEngineRemarkOther) ? strEngineRemarkOther : "";
    }

    public void setStrEngineRemarkOther(String strEngineRemarkOther) {
        this.strEngineRemarkOther = strEngineRemarkOther;
    }

    public String getStrFuelAndIgnitionSystemRemarkOther() {
        return (null != strFuelAndIgnitionSystemRemarkOther) ? strFuelAndIgnitionSystemRemarkOther : "";
    }

    public void setStrFuelAndIgnitionSystemRemarkOther(String strFuelAndIgnitionSystemRemarkOther) {
        this.strFuelAndIgnitionSystemRemarkOther = strFuelAndIgnitionSystemRemarkOther;
    }

    public String getStrTransmissionRemarkOther() {
        return (null != strTransmissionRemarkOther) ? strTransmissionRemarkOther : "";
    }

    public void setStrTransmissionRemarkOther(String strTransmissionRemarkOther) {
        this.strTransmissionRemarkOther = strTransmissionRemarkOther;
    }

    public String getStrBreakSystemRemarkOther() {
        return (null != strBreakSystemRemarkOther) ? strBreakSystemRemarkOther : "";
    }

    public void setStrBreakSystemRemarkOther(String strBreakSystemRemarkOther) {
        this.strBreakSystemRemarkOther = strBreakSystemRemarkOther;
    }

    public String getStrSuspensionSystemRemarksOther() {
        return (null != strSuspensionSystemRemarksOther) ? strSuspensionSystemRemarksOther : "";
    }

    public void setStrSuspensionSystemRemarksOther(String strSuspensionSystemRemarksOther) {
        this.strSuspensionSystemRemarksOther = strSuspensionSystemRemarksOther;
    }

    public String getStrSteeringSystemRemarkOther() {
        return (null != strSteeringSystemRemarkOther) ? strSteeringSystemRemarkOther : "";
    }

    public void setStrSteeringSystemRemarkOther(String strSteeringSystemRemarkOther) {
        this.strSteeringSystemRemarkOther = strSteeringSystemRemarkOther;
    }

    public String getStrACSystemRemarkOther() {
        return (null != strACSystemRemarkOther) ? strACSystemRemarkOther : "";
    }

    public void setStrACSystemRemarkOther(String strACSystemRemarkOther) {
        this.strACSystemRemarkOther = strACSystemRemarkOther;
    }

    public String getStrElectricalSystemRemarkOther() {
        return (null != strElectricalSystemRemarkOther) ? strElectricalSystemRemarkOther : "";
    }

    public void setStrElectricalSystemRemarkOther(String strElectricalSystemRemarkOther) {
        this.strElectricalSystemRemarkOther = strElectricalSystemRemarkOther;
    }

    public boolean isNewObject() {
        return newObject;
    }

    public void setNewObject(boolean newObject) {
        this.newObject = newObject;
    }

    public String getFitnessCertificate() {
        return (null == FitnessCertificate) ? "" : FitnessCertificate;
    }

    public void setFitnessCertificate(String fitnessCertificate) {
        FitnessCertificate = fitnessCertificate;
    }

    public String getRoadTax() {
        return (null == RoadTax) ? "" : RoadTax;
    }

    public void setRoadTax(String roadTax) {
        RoadTax = roadTax;
    }

    public String getRoadTaxExpiryDate() {
        return (null == RoadTaxExpiryDate) ? "" : RoadTaxExpiryDate;
    }

    public void setRoadTaxExpiryDate(String roadTaxExpiryDate) {
        RoadTaxExpiryDate = roadTaxExpiryDate;
    }

    public String getFrontBumper() {
        return (null == FrontBumper) ? "" : FrontBumper;
    }

    public void setFrontBumper(String frontBumper) {
        FrontBumper = frontBumper;
    }

    public String getOutsideRearViewMirrorLH() {
        return (null == OutsideRearViewMirrorLH) ? "" : OutsideRearViewMirrorLH;
    }

    public void setOutsideRearViewMirrorLH(String outsideRearViewMirrorLH) {
        OutsideRearViewMirrorLH = outsideRearViewMirrorLH;
    }

    public String getOutsideRearViewMirrorRH() {
        return (null == OutsideRearViewMirrorRH) ? "" : OutsideRearViewMirrorRH;
    }

    public void setOutsideRearViewMirrorRH(String outsideRearViewMirrorRH) {
        OutsideRearViewMirrorRH = outsideRearViewMirrorRH;
    }

    public String getPillarType() {
        return (null == PillarType) ? "" : PillarType;
    }

    public void setPillarType(String pillarType) {
        PillarType = pillarType;
    }

    public String getDickyDoor() {
        return (null == DickyDoor) ? "" : DickyDoor;
    }

    public void setDickyDoor(String dickyDoor) {
        DickyDoor = dickyDoor;
    }

    public String getBodyShell() {
        return (null == BodyShell) ? "" : BodyShell;
    }

    public void setBodyShell(String bodyShell) {
        BodyShell = bodyShell;
    }

    public String getChassis() {
        return (null == Chassis) ? "" : Chassis;
    }

    public void setChassis(String chassis) {
        Chassis = chassis;
    }

    public String getApronFrontLH() {
        return (null == ApronFrontLH) ? "" : ApronFrontLH;
    }

    public void setApronFrontLH(String apronFrontLH) {
        ApronFrontLH = apronFrontLH;
    }

    public String getApronFrontRH() {
        return (null == ApronFrontRH) ? "" : ApronFrontRH;
    }

    public void setApronFrontRH(String apronFrontRH) {
        ApronFrontRH = apronFrontRH;
    }

    public String getStrutMountingArea() {
        return (null == StrutMountingArea) ? "" : StrutMountingArea;
    }

    public void setStrutMountingArea(String strutMountingArea) {
        StrutMountingArea = strutMountingArea;
    }

    public String getFirewall() {
        return (null == Firewall) ? "" : Firewall;
    }

    public void setFirewall(String firewall) {
        Firewall = firewall;
    }

    public String getCowlTop() {
        return (null == CowlTop) ? "" : CowlTop;
    }

    public void setCowlTop(String cowlTop) {
        CowlTop = cowlTop;
    }

    public String getUpperCrossMember() {
        return (null == UpperCrossMember) ? "" : UpperCrossMember;
    }

    public void setUpperCrossMember(String upperCrossMember) {
        UpperCrossMember = upperCrossMember;
    }

    public String getLowerCrossMember() {
        return (null == LowerCrossMember) ? "" : LowerCrossMember;
    }

    public void setLowerCrossMember(String lowerCrossMember) {
        LowerCrossMember = lowerCrossMember;
    }

    public String getRadiatorSupport() {
        return (null == RadiatorSupport) ? "" : RadiatorSupport;
    }

    public void setRadiatorSupport(String radiatorSupport) {
        RadiatorSupport = radiatorSupport;
    }

    public String getEngineRoomCarrierAssembly() {
        return (null == EngineRoomCarrierAssembly) ? "" : EngineRoomCarrierAssembly;
    }

    public void setEngineRoomCarrierAssembly(String engineRoomCarrierAssembly) {
        EngineRoomCarrierAssembly = engineRoomCarrierAssembly;
    }

    public String getHeadLightLH() {
        return (null == HeadLightLH) ? "" : HeadLightLH;
    }

    public void setHeadLightLH(String headLightLH) {
        HeadLightLH = headLightLH;
    }

    public String getHeadLightRH() {
        return (null == HeadLightRH) ? "" : HeadLightRH;
    }

    public void setHeadLightRH(String headLightRH) {
        HeadLightRH = headLightRH;
    }

    public String getTailLightLH() {
        return (null == TailLightLH) ? "" : TailLightLH;
    }

    public void setTailLightLH(String tailLightLH) {
        TailLightLH = tailLightLH;
    }

    public String getTailLightRH() {
        return (null == TailLightRH) ? "" : TailLightRH;
    }

    public void setTailLightRH(String tailLightRH) {
        TailLightRH = tailLightRH;
    }

    public String getRunningBoardLH() {
        return (null == RunningBoardLH) ? "" : RunningBoardLH;
    }

    public void setRunningBoardLH(String runningBoardLH) {
        RunningBoardLH = runningBoardLH;
    }

    public String getRunningBoardRH() {
        return (null == RunningBoardRH) ? "" : RunningBoardRH;
    }

    public void setRunningBoardRH(String runningBoardRH) {
        RunningBoardRH = runningBoardRH;
    }

    public String getPowerWindowType() {
        return (null == PowerWindowType) ? "" : PowerWindowType;
    }

    public void setPowerWindowType(String powerWindowType) {
        PowerWindowType = powerWindowType;
    }

    public String getAllWindowCondition() {
        return (null == AllWindowCondition) ? "" : AllWindowCondition;
    }

    public void setAllWindowCondition(String allWindowCondition) {
        AllWindowCondition = allWindowCondition;
    }

    public String getFrontLHWindow() {
        return (null == FrontLHWindow) ? "" : FrontLHWindow;
    }

    public void setFrontLHWindow(String frontLHWindow) {
        FrontLHWindow = frontLHWindow;
    }

    public String getFrontRHWindow() {
        return (null == FrontRHWindow) ? "" : FrontRHWindow;
    }

    public void setFrontRHWindow(String frontRHWindow) {
        FrontRHWindow = frontRHWindow;
    }

    public String getRearLHWindow() {
        return (null == RearLHWindow) ? "" : RearLHWindow;
    }

    public void setRearLHWindow(String rearLHWindow) {
        RearLHWindow = rearLHWindow;
    }

    public String getRearRHWindow() {
        return (null == RearRHWindow) ? "" : RearRHWindow;
    }

    public void setRearRHWindow(String rearRHWindow) {
        RearRHWindow = rearRHWindow;
    }

    public String getPlatformPassengerCabin() {
        return (null == PlatformPassengerCabin) ? "" : PlatformPassengerCabin;
    }

    public void setPlatformPassengerCabin(String platformPassengerCabin) {
        PlatformPassengerCabin = platformPassengerCabin;
    }

    public String getPlatformBootSpace() {
        return (null == PlatformBootSpace) ? "" : PlatformBootSpace;
    }

    public void setPlatformBootSpace(String platformBootSpace) {
        PlatformBootSpace = platformBootSpace;
    }

    public String getAudioPathVehicleNoise() {
        return (null == AudioPathVehicleNoise) ? "" : AudioPathVehicleNoise;
    }

    public void setAudioPathVehicleNoise(String audioPathVehicleNoise) {
        AudioPathVehicleNoise = audioPathVehicleNoise;
    }

    public String getOverallFeedbackRating() {
        return (OverallFeedbackRating == null) ? "" : OverallFeedbackRating;
    }

    public void setOverallFeedbackRating(String overallFeedbackRating) {
        OverallFeedbackRating = overallFeedbackRating;
    }

    public String getEvaluationCount() {
        return (null == EvaluationCount) ? "" : EvaluationCount;
    }

    public void setEvaluationCount(String evaluationCount) {
        EvaluationCount = evaluationCount;
    }

    public String getVehicleUsage() {
        return (null == VehicleUsage) ? "" : VehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        VehicleUsage = vehicleUsage;
    }

    public void initializeFields() {
        Id = "";
        requestId = "";
        EvaluatorId = "";
        EvaluatorContactNo = "";
        EvaluationNo = "";
        AgainstTransaction = "";
        Purpose = "";
        OptedNewVehicle = "";
        Date = "";
        Time = "";
        TransactionNo = "";
        EvaluatorName = "";
        SourceOfTransaction = "";
        OwnerName = "";
        Address = "";
        EmailId = "";
        PhoneNo = "";
        MobileNo = "";
        State = "";
        City = "";
        PinCode = "";
        Occupation = "";
        Make = "";
        Model = "";
        Variant = "";
        YearOfMfg = "";
        YearOfReg = "";
        RegNo = "";
        Mileage = "";
        MonthOfMfg = "";
        MonthOfReg = "";
        EngineNo = "";
        ChassisNo = "";
        NoOfOwners = "";
        Transmission = "";
        ServiceBooklet = "";
        Color = "";
        FuelType = "";
        EuroVersion = "";
        BodyType = "";
        NoOfSeats = "";
        RCBook = "";
        Insurance = "";
        InsuranceDate = "";
        Ncb = "";
        Puc = "";
        Hypothecation = "";
        FinanceCompany = "";
        NocStatus = "";
        CustomerExpectedPrice = "";
        TestDrive = "";
        IsAccident = "";
        BodyCost = "";
        EngineCost = "";
        FuelAndIgnitionSystemCost = "";
        TransmissionCost = "";
        BreakSystemCost = "";
        SuspensionCost = "";
        SteeringSystemCost = "";
        AcCost = "";
        ElectricalSystemCost = "";
        totalOfAggregateCost = "";
        generalServiceCost = "";
        totalRefurbishmentCost = "";
        Stereo = "";
        NoOfKeys = "";
        ReverseParking = "";
        PowerSteering = "";
        AlloyWheels = "";
        FogLamp = "";
        CentralLocking = "";
        ToolKitJack = "";
        Comment = "";
        TubelessTyres = "";
        RearWiper = "";
        MusicSystem = "";
        FMRadio = "";
        PowerWindow = "";
        AC = "";
        PowerAdjustableSeats = "";
        KeyLessEntry = "";
        PedalShifter = "";
        ElectricallyAdjustableORVM = "";
        RearDefogger = "";
        SunRoof = "";
        AutoClimateTechnologies = "";
        LeatherSeats = "";
        BluetoothAudioSystems = "";
        GPSNavigationSystems = "";
        Odometer = "";
        SeatCover = "";
        Dashboard = "";
        AirBags = "";
        FrontFenderRightSide = "";
        FrontDoorRightSide = "";
        BackDoorRightSide = "";
        BackFenderRightSide = "";
        Bonnet = "";
        TailGate = "";
        BackBumper = "";
        FrontFenderLeftHandSide = "";
        FrontDoorLeftHandSide = "";
        BackDoorLeftHandSide = "";
        BackFenderLeftHandSide = "";
        FrontLight = "";
        BackLight = "";
        WindShield = "";
        Pillars = "";
        Mirrors = "";
        Wheels = "";
        TyreFrontLeft = "";
        TyreFrontRight = "";
        TyreRearLeft = "";
        TyreRearRight = "";
        SpareTyre = "";
        initializeImages();
        strBodyRemark = "";
        strEngineRemark = "";
        strFuelAndIgnitionSystemRemark = "";
        strTransmissionRemark = "";
        strBreakSystemRemark = "";
        strSuspensionSystemRemarks = "";
        strSteeringSystemRemark = "";
        strACSystemRemark = "";
        strElectricalSystemRemark = "";
        strGeneralServiceCostRemark = "";
        EvaluationStatus = "";
        CNGKit = "";
        RcRemarks = "";
        AccidentalType = "";
        AccidentalRemarks = "";
        Latitude = "";
        Longitude = "";
        ScrapVehicle = "";
        strBodyRemarkOther = "";
        strEngineRemarkOther = "";
        strFuelAndIgnitionSystemRemarkOther = "";
        strTransmissionRemarkOther = "";
        strBreakSystemRemarkOther = "";
        strSuspensionSystemRemarksOther = "";
        strSteeringSystemRemarkOther = "";
        strACSystemRemarkOther = "";
        strElectricalSystemRemarkOther = "";
        FitnessCertificate = "";
        RoadTax = "";
        RoadTaxExpiryDate = "";
        FrontBumper = "";
        OutsideRearViewMirrorLH = "";
        OutsideRearViewMirrorRH = "";
        PillarType = "";
        DickyDoor = "";
        BodyShell = "";
        Chassis = "";
        ApronFrontLH = "";
        ApronFrontRH = "";
        StrutMountingArea = "";
        Firewall = "";
        CowlTop = "";
        UpperCrossMember = "";
        LowerCrossMember = "";
        RadiatorSupport = "";
        EngineRoomCarrierAssembly = "";
        HeadLightLH = "";
        HeadLightRH = "";
        TailLightLH = "";
        TailLightRH = "";
        RunningBoardLH = "";
        RunningBoardRH = "";
        PowerWindowType = "";
        AllWindowCondition = "";
        FrontLHWindow = "";
        FrontRHWindow = "";
        RearLHWindow = "";
        RearRHWindow = "";
        PlatformPassengerCabin = "";
        PlatformBootSpace = "";
        AudioPathVehicleNoise = "";
        OverallFeedbackRating = "";
        EvaluationCount = "";
        VehicleUsage = "";
    }

    public void initializeImages() {
        PhotoPathFrontImage = "";
        PhotoPathRearImage = "";
        PhotoPath45DegreeLeftView = "";
        PhotoPath45DegreeRightView = "";
        PhotoPathDashboardView = "";
        PhotoPathOdometerImage = "";
        PhotoPathChassisNoImage = "";
        PhotoPathEngineNoImage = "";
        PhotoPathInteriorView = "";
        PhotoPathRCCopyImage = "";
        PhotoPathInsuranceImage = "";
        PhotoPathRearLeftTyre = "";
        PhotoPathRearRightTyre = "";
        PhotoPathFrontLeftTyre = "";
        PhotoPathFrontRightTyre = "";
        PhotoPathDamagedPartImage1 = "";
        PhotoPathDamagedPartImage2 = "";
        PhotoPathDamagedPartImage3 = "";
        PhotoPathDamagedPartImage4 = "";
        PhotoPathRoofTopImage = "";
        PhotoPathRCCopy1 = "";
        PhotoPathRCCopy2 = "";
        PhotoPathRCCopy3 = "";
    }
}