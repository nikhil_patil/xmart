package com.mahindra.mitra20.evaluator.presenter;

/**
 * Created by BADHAP-CONT on 8/12/2018.
 */

public interface AppointmentConfirmIn {
    public void onAppointmentConfirmSuccess(int requestId,String response);

    public void onAppointmentConfirmFailure(int requestId,String response);
}
