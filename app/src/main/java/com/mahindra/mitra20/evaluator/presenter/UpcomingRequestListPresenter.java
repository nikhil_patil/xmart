package com.mahindra.mitra20.evaluator.presenter;

import android.content.Context;
import android.util.Log;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

/**
 * Created by BADHAP-CONT on 8/17/2018.
 */

public class UpcomingRequestListPresenter implements VolleySingleTon.VolleyInteractor {

    UpcomingRequestIn upcomingRequestIn;
    Context context;
    EvaluatorHelper evaluatorHelper;

    public UpcomingRequestListPresenter(UpcomingRequestIn upcomingRequestIn, Context context) {
        this.upcomingRequestIn = upcomingRequestIn;
        this.context = context;
        evaluatorHelper = new EvaluatorHelper(context);
    }
    @Override
    public void gotSuccessResponse(int requestId, String response) {
        Log.d("tag", response);
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        upcomingRequestIn.onUpcomingDownloadFail(requestId, response);
    }
}