package com.mahindra.mitra20.evaluator.views.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.Activity_Profile_Pic;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.helper.LoginHelper;
import com.mahindra.mitra20.champion.helper.UpdateAppHelper;
import com.mahindra.mitra20.champion.views.activity.EvaluatorwiseStatusActivity;
import com.mahindra.mitra20.constants.GeneralConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.evaluator.presenter.AssignedRequestListIn;
import com.mahindra.mitra20.evaluator.presenter.AssignedRequestListPresenter;
import com.mahindra.mitra20.evaluator.views.adapters.EvaluationOverdueAdapter;
import com.mahindra.mitra20.evaluator.views.adapters.ScheduleAppointmentDashboardAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.helper.MasterSyncHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.sqlite.BrokerDbHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.utils.ForceUpdateChecker;
import com.mahindra.mitra20.utils.LogUtil;
import com.mahindra.mitra20.utils.NetworkUtilities;

import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EvaluatorDashboardActivity extends AppCompatActivity implements
        View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, AssignedRequestListIn, ForceUpdateChecker.OnUpdateNeededListener {

    private ProgressDialog dialog;
    private EmptyRecyclerView recyclerViewTodaysScheduleVisit, recyclerViewPendingVisits;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private TextView textViewTodayScheduleVisitViewAll, textViewPendingViewAll;
    private TextView textViewDraftCount, textViewCompletedScheduledCount, textViewOverdueScheduledCount,
            textViewUpcomingScheduledCount, textViewTodaysScheduledCount, textViewPCSCount;
    private LinearLayout linearLayoutScheduleForTheDay, linearLayoutUpcoming, linearLayoutOverdue, linearLayoutMenu;
    private TableRow tableRowCompletedEvaluation, tableRowDraftEvaluation, tableRowProcurement;
    private ImageView imageViewProfilePic, imageViewMenu, imageViewNewEvaluation, imageViewNotification,
            imageViewNavigationProfilePic;
    private Context context;
    private TableRow tableRowDashboard, tableRowVisits, tableRowEvaluation, tableRowMasterSync;
    private TextView textViewEvaluatorNameDrawer, textViewTeam, textViewLogout;
    private DrawerLayout drawer;
    private LinearLayoutManager linearLayoutManager;
    private AssignedRequestListPresenter assignedRequestListPresenter;
    private ArrayList<ScheduleAppoinment> todaysScheduleVisitArrayList;
    private ArrayList<ScheduleAppoinment> overdueScheduleVisitArrayList;
    private EvaluationOverdueAdapter evaluationOverdueAdapter;
    private ScheduleAppointmentDashboardAdapter scheduledAppointmentDashboardAdapter;
    private SwipeRefreshLayout swipeContainer;
    private ProgressBar progressProfileImage, progressProfileNavigationImage;
    private SharedPreferences sharedPref;

    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_evaluator_dashboard_new);
        init();
        displayUnreadNotificationIcon();

        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                    new IntentFilter(GeneralConstants.BROADCAST_RECEIVER_KEY));

        } catch (Exception e) {
            e.printStackTrace();
        }

        sharedPref = getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
        boolean isLoginHelpDisplayed = sharedPref.getBoolean(
                SharedPreferenceKeys.EVALUATOR_DASHBOARD_HELP_DISPLAYED, false);
        if (!isLoginHelpDisplayed) {
            showHelp();
        }

        try {
            if (GeneralConstants.DATA_FETCHED.equalsIgnoreCase("false")) {
                LogUtil.getInstance().logE("downloadEnquiryMaster", "onCreate()->autoRefreshDownloadMasters()");
                autoRefreshDownloadMasters();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (NetworkUtilities.isNetworkConnected(this))
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
    }

    private void showHelp() {
        try {
            openDrawer();
            findViewById(R.id.cl_overlay_sync).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_sync_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeDrawer();
                    findViewById(R.id.cl_overlay_sync).setVisibility(View.GONE);
                    findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.VISIBLE);

                    findViewById(R.id.tv_pull_to_refresh_next).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.GONE);
                            findViewById(R.id.cl_overlay_notification).setVisibility(View.VISIBLE);

                            findViewById(R.id.tv_notification_done).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    findViewById(R.id.cl_overlay_notification).setVisibility(View.GONE);

                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putBoolean(
                                            SharedPreferenceKeys.EVALUATOR_DASHBOARD_HELP_DISPLAYED, true);
                                    editor.apply();
                                }
                            });
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayUnreadNotificationIcon() {
        BrokerDbHelper brokerDbHelper = new BrokerDbHelper(this);
        if (brokerDbHelper.checkUnreadNotifications()) {
            findViewById(R.id.iv_notification_available).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.iv_notification_available).setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                GeneralConstants.DATA_FETCHED = "false";
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            finish();
                        }
                    }).setNegativeButton("No", null).show();
        }
    }

    private void openDrawer() {
        if (null != drawer && !drawer.isDrawerOpen(Gravity.START)) {
            drawer.openDrawer(Gravity.START);
        }
    }

    private void closeDrawer() {
        if (null != drawer && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void init() {
        assignedRequestListPresenter = new AssignedRequestListPresenter(this, this);

        if (!checkPermission()) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE,
                    READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);
        }

        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);

        textViewEvaluatorNameDrawer = findViewById(R.id.textViewEvaluatorName1);
        textViewTeam = findViewById(R.id.textViewTeam);
        tableRowDashboard = findViewById(R.id.tableRowDshboard);
        tableRowVisits = findViewById(R.id.tableRowVisits);
        tableRowEvaluation = findViewById(R.id.tableRowEvaluation);
        tableRowMasterSync = findViewById(R.id.tableRowMasterSync);
        textViewLogout = findViewById(R.id.textViewLogout);
        imageViewNavigationProfilePic = findViewById(R.id.imageViewNavigationProfilePic);

        TextView textDealerName = findViewById(R.id.textDealerName);
        TextView textUserName = findViewById(R.id.textUserName);

        try {
            textViewEvaluatorNameDrawer.setText(SessionUserDetails.getInstance().getUserName());
            textViewTeam.setText(SessionUserDetails.getInstance().getDealerName());
            textUserName.setText(SessionUserDetails.getInstance().getUserName());
            textDealerName.setText(SessionUserDetails.getInstance().getDealerName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        linearLayoutMenu = findViewById(R.id.linearLayoutMenu);
        imageViewMenu = findViewById(R.id.imageViewMenu);
        imageViewProfilePic = findViewById(R.id.imageViewProfilePic);

        tableRowDashboard.setOnClickListener(this);
        tableRowVisits.setOnClickListener(this);
        tableRowEvaluation.setOnClickListener(this);
        tableRowMasterSync.setOnClickListener(this);
        textViewLogout.setOnClickListener(this);
        imageViewProfilePic.setOnClickListener(this);


        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        evaluatorHelper = new EvaluatorHelper(this.getBaseContext());
        masterDatabaseHelper = new MasterDatabaseHelper(this.getBaseContext());
        imageViewMenu.setOnClickListener(this);
        linearLayoutMenu.setOnClickListener(this);

        imageViewNewEvaluation = findViewById(R.id.imageViewNewEvaluation);
        imageViewNewEvaluation.setOnClickListener(this);

        imageViewNotification = findViewById(R.id.imageViewNotification);
        imageViewNotification.setOnClickListener(this);

        linearLayoutScheduleForTheDay = findViewById(R.id.linearLayoutScheduleForTheDay);
        linearLayoutUpcoming = findViewById(R.id.linearLayoutUpcoming);
        linearLayoutOverdue = findViewById(R.id.linearLayoutOverdue);
        linearLayoutScheduleForTheDay.setOnClickListener(this);
        linearLayoutUpcoming.setOnClickListener(this);
        linearLayoutOverdue.setOnClickListener(this);
        textViewDraftCount = findViewById(R.id.textViewDraftCount);
        textViewCompletedScheduledCount = findViewById(R.id.tvCount);
        textViewOverdueScheduledCount = findViewById(R.id.textViewOverdueScheduledCount);
        textViewUpcomingScheduledCount = findViewById(R.id.textViewUpcomingScheduledCount);
        textViewTodaysScheduledCount = findViewById(R.id.textViewTodaysScheduledCount);
        textViewPCSCount = findViewById(R.id.textViewPCSCount);

        recyclerViewTodaysScheduleVisit = findViewById(R.id.recyclerViewTodaysScheduleVisit);

        tableRowCompletedEvaluation = findViewById(R.id.tableRowCompltetedEvaluation);
        tableRowDraftEvaluation = findViewById(R.id.tableRowDraftEvaluation);
        tableRowProcurement = findViewById(R.id.tableRowAllStatus);

        tableRowCompletedEvaluation.setOnClickListener(this);
        tableRowDraftEvaluation.setOnClickListener(this);
        tableRowProcurement.setOnClickListener(this);

        textViewTodayScheduleVisitViewAll = findViewById(R.id.textViewTodayScheduleVisitViewAll);
        textViewTodayScheduleVisitViewAll.setOnClickListener(this);
        textViewPendingViewAll = findViewById(R.id.textViewPendingViewAll);
        textViewPendingViewAll.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        progressProfileImage = findViewById(R.id.progressProfileImage);
        progressProfileNavigationImage = findViewById(R.id.progressProfileNavigationImage);

        String strProfilePic = SessionUserDetails.getInstance().getUserProfilePic();
        if (null != strProfilePic) {
            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(SessionUserDetails.getInstance().getUserProfilePic()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            imageViewProfilePic.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(imageViewProfilePic);

            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(SessionUserDetails.getInstance().getUserProfilePic()))

                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressProfileNavigationImage.setVisibility(View.GONE);
                            imageViewNavigationProfilePic.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressProfileNavigationImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(imageViewNavigationProfilePic);
        }
        LogUtil.getInstance().logE("getDataFromLocalDb", "init()->getDataFromLocalDb()");
        getDataFromLocalDb();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewMenu:
            case R.id.linearLayoutMenu:
                drawer.openDrawer(GravityCompat.START);
                break;

            case R.id.imageViewNotification:
                Intent intentNotif = new Intent(
                        EvaluatorDashboardActivity.this, NotificationsActivity.class);
                startActivityForResult(intentNotif, 300);
                break;

            case R.id.textViewTodayScheduleVisitViewAll:
            case R.id.linearLayoutScheduleForTheDay:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentScheduleAppoinment = new Intent(EvaluatorDashboardActivity.this, ScheduleAppoinmentActivity.class);
                    intentScheduleAppoinment.putExtra("appoinmentType", "Today");
                    startActivity(intentScheduleAppoinment);
//                    finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.textViewPendingViewAll:
            case R.id.linearLayoutOverdue:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentPendingAppoinment = new Intent(EvaluatorDashboardActivity.this, ScheduleAppoinmentActivity.class);
                    intentPendingAppoinment.putExtra("appoinmentType", "Pending");
                    startActivity(intentPendingAppoinment);
//                    finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.linearLayoutUpcoming:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentUpcominigAppoinment = new Intent(EvaluatorDashboardActivity.this, ScheduleAppoinmentActivity.class);
                    intentUpcominigAppoinment.putExtra("appoinmentType", "Upcoming");
                    startActivity(intentUpcominigAppoinment);
//                    finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowCompltetedEvaluation:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentCompletedEvaluation = new Intent(EvaluatorDashboardActivity.this, EvaluationCompletedDraftActivity.class);
                    intentCompletedEvaluation.putExtra("appointmentType", "Completed");
                    startActivity(intentCompletedEvaluation);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowDraftEvaluation:
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentDraftEvaluation = new Intent(EvaluatorDashboardActivity.this, EvaluationCompletedDraftActivity.class);
                    intentDraftEvaluation.putExtra("appointmentType", "Draft");
                    startActivity(intentDraftEvaluation);
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowMasterSync:
                if (CommonHelper.isConnectingToInternet(context)) {
                    masterDatabaseHelper.truncateMasterTable();
                    new MasterSyncHelper(this).syncMasters();
                } else
                    CommonHelper.toast(R.string.internet_error, this);
                drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                break;
            case R.id.tableRowDshboard:
                drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent intentDashboard = new Intent(EvaluatorDashboardActivity.this, EvaluatorDashboardActivity.class);
                intentDashboard.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentDashboard);
                finish();
                break;
            case R.id.tableRowVisits:
                drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentScheduleAppoinment = new Intent(EvaluatorDashboardActivity.this, ScheduleAppoinmentActivity.class);
                    intentScheduleAppoinment.putExtra("appoinmentType", "Today");
                    intentScheduleAppoinment.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentScheduleAppoinment);
//                    finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.tableRowEvaluation:
                drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                if (masterDatabaseHelper.isMasterSync()) {
                    Intent intentEvaluationActivity = new Intent(
                            EvaluatorDashboardActivity.this, NewEvaluationActivity.class);
                    intentEvaluationActivity.putExtra("status", "newEvaluationWithoutEnquiry");
                    intentEvaluationActivity.putExtra("purpose", "Direct Buy");
                    intentEvaluationActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentEvaluationActivity);
//                    finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.imageViewNewEvaluation:
                if (masterDatabaseHelper.isMasterSync()) {
                    String id = assignedRequestListPresenter.getNextEvaluationId();
                    Intent intentEvaluationActivity2 = new Intent(EvaluatorDashboardActivity.this, NewEvaluationActivity.class);
                    intentEvaluationActivity2.putExtra("status", "newEvaluationWithoutEnquiry");
                    intentEvaluationActivity2.putExtra("purpose", "Direct Buy");
                    intentEvaluationActivity2.putExtra("NewEvaluationId", id);
                    intentEvaluationActivity2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentEvaluationActivity2);
                    finish();
                    break;
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
                break;
            case R.id.imageViewProfilePic:
                startActivity(new Intent(EvaluatorDashboardActivity.this, Activity_Profile_Pic.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;
            case R.id.textViewLogout:
                LoginHelper.logoutUser(this);
            case R.id.tableRowAllStatus:
                startActivity(new Intent(EvaluatorDashboardActivity.this, EvaluatorwiseStatusActivity.class));
                break;
        }
    }

    public void showProgressDialog(String msg) {
        if (null == dialog) {
            dialog = new ProgressDialog(EvaluatorDashboardActivity.this);
        }
        dialog.setCancelable(false);
        if (null != msg && !msg.isEmpty()) {
            dialog.setMessage(msg);
        } else {
            dialog.setMessage("Wait while loading");
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRefresh() {
        swipeContainer.setRefreshing(false);
        if (masterDatabaseHelper.isMasterSync()) {
            if (CommonHelper.isConnectingToInternet(getBaseContext())) {
                LogUtil.getInstance().logE("downloadEnquiryMaster", "onRefresh()->downloadEnquiryMaster()");
                showProgressDialog("Downloading dashboard data... Please wait...");
                assignedRequestListPresenter.downloadEnquiryMaster();
            } else {
                CommonHelper.toast(getResources().getString(R.string.internet_error), getBaseContext());
                swipeContainer.setRefreshing(false);
            }
        } else {
            hideProgressDialog();
            CommonHelper.toast(R.string.master_sync_request, context);
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onDownloadSuccess(int requestId, ArrayList<ScheduleAppoinment> scheduleAppoinments) {
        if (requestId == 1) {
            todaysScheduleVisitArrayList = evaluatorHelper.getScheduleAppointmentDetails(0, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
            this.scheduledAppointmentDashboardAdapter.updateList(this.todaysScheduleVisitArrayList);
            scheduledAppointmentDashboardAdapter.notifyDataSetChanged();

            overdueScheduleVisitArrayList = evaluatorHelper.getOverdueAppointmentDetails(2, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
            this.evaluationOverdueAdapter.updateList(this.overdueScheduleVisitArrayList);
            evaluationOverdueAdapter.notifyDataSetChanged();
            LogUtil.getInstance().logE("getDataFromLocalDb", "onDownloadSuccess()->getDataFromLocalDb()");
            getDataFromLocalDb();
            swipeContainer.setRefreshing(false);
            hideProgressDialog();
        }
    }

    @Override
    public void onDownloadFail(int requestId, String message) {
        if (requestId == 1) {
            hideProgressDialog();
            //COMMENTED BY PRAVIN DHARAM ON 25/11/2019
            //CommonHelper.toast(message, context);
            CommonHelper.toast("Data not found...", context);
            swipeContainer.setRefreshing(false);
        }
    }

    public void getDataFromLocalDb() {

        int scheduledAppointmentCount = evaluatorHelper.getScheduledAppointmentCount(EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        textViewTodaysScheduledCount.setText(String.valueOf(scheduledAppointmentCount));
        textViewPCSCount.setText(String.valueOf(evaluatorHelper.getPcsScheduledAppointmentCount()));

        int overdueAppointmentCount = evaluatorHelper.getOverdueAppointmentCount();
        textViewOverdueScheduledCount.setText(String.valueOf(overdueAppointmentCount));

        int upcomingAppointmentCount = evaluatorHelper.getUpcomingAppointmentCount();
        textViewUpcomingScheduledCount.setText(String.valueOf(upcomingAppointmentCount));

        int draftEvaluationsCount = evaluatorHelper.getCountOfEvaluations("Draft");
        textViewDraftCount.setText(String.valueOf(draftEvaluationsCount));

        int completedEvaluationsCount = evaluatorHelper.getCountOfEvaluations("Completed");
        textViewCompletedScheduledCount.setText(String.valueOf(completedEvaluationsCount));

        recyclerViewTodaysScheduleVisit = findViewById(R.id.recyclerViewTodaysScheduleVisit);
        recyclerViewTodaysScheduleVisit.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewTodaysScheduleVisit.setEmptyView(findViewById(R.id.list_empty));
        todaysScheduleVisitArrayList = evaluatorHelper.getScheduleAppointmentDetails(2, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        scheduledAppointmentDashboardAdapter = new ScheduleAppointmentDashboardAdapter(this, todaysScheduleVisitArrayList);
        recyclerViewTodaysScheduleVisit.setAdapter(scheduledAppointmentDashboardAdapter);

        //Pending Visits
        recyclerViewPendingVisits = findViewById(R.id.recyclerViewPendingVisits);
        LinearLayoutManager linearLayoutManagerPending = new LinearLayoutManager(getApplicationContext());
        recyclerViewPendingVisits.setLayoutManager(linearLayoutManagerPending); // set LayoutManager to RecyclerView
        recyclerViewPendingVisits.setEmptyView(findViewById(R.id.list_no_pending_visits));
        overdueScheduleVisitArrayList = evaluatorHelper.getOverdueAppointmentDetails(2, EvaluatorConstants.FOLLOWUP_CUSTOMER_STATUS);
        evaluationOverdueAdapter = new EvaluationOverdueAdapter(this, overdueScheduleVisitArrayList, "Overdue", "Dashboard");
        recyclerViewPendingVisits.setAdapter(evaluationOverdueAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 300) {
            displayUnreadNotificationIcon();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            LogUtil.getInstance().logE("downloadEnquiryMaster", "onNewIntent()->autoRefreshDownloadMasters()");
            if (intent.getBooleanExtra(EvaluatorConstants.FROM_SCHEDULE_APPOINTMENT_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_NEW_EVALUTION_SCREEN, false)) {
                if (intent.getBooleanExtra(ChampionConstants.IS_DRAFT_SAVED, false))
                    getDataFromLocalDb();

            } else if (intent.getBooleanExtra(EvaluatorConstants.FROM_APPOINTMENT_DETAILS_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_LOGIN_SCREEN, false)) {
                //TODO
            } else if (intent.getBooleanExtra(ChampionConstants.FROM_DRAFT_LIST_ACTIVITY, false)) {
                getDataFromLocalDb();
            } else
                autoRefreshDownloadMasters();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                LogUtil.getInstance().logE("downloadEnquiryMaster", "BroadcastReceiver()->autoRefreshDownloadMasters()");
                autoRefreshDownloadMasters();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void autoRefreshDownloadMasters() {
        LogUtil.getInstance().logE("downloadEnquiryMaster", "autoRefreshDownloadMasters()- isMasterSync() = " + masterDatabaseHelper.isMasterSync());
        if (CommonHelper.isConnectingToInternet(getBaseContext()) && masterDatabaseHelper.isMasterSync()) {
            showProgressDialog("Downloading dashboard data... Please wait...");
            LogUtil.getInstance().logE("downloadEnquiryMaster", "autoRefreshDownloadMasters()->downloadEnquiryMaster()");
            assignedRequestListPresenter.downloadEnquiryMaster();
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onUpdateNeeded(String updateUrl,boolean isMajorUpdate) {
        new UpdateAppHelper(this, updateUrl,isMajorUpdate).showUpdateDialog();
    }

    @Override
    public void updateNotFound() {
        //TODO
    }

}