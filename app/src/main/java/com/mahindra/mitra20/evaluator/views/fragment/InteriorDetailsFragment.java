package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.LayoutInteriorBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.InteriorDetailsPresenter;
import com.mahindra.mitra20.interfaces.CommonMethods;

import java.util.ArrayList;

import static com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity.randomNumGen;

public class InteriorDetailsFragment extends Fragment implements CommonMethods {

    LayoutInteriorBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private InteriorDetailsPresenter interiorDetailsPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutInteriorBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        interiorDetailsPresenter = new InteriorDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = interiorDetailsPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        binding.spinnerAllWindowCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    binding.spinnerFrontLHWindow.setSelection(0);
                    binding.spinnerFrontLHWindow.setEnabled(false);
                    binding.spinnerFrontRHWindow.setSelection(0);
                    binding.spinnerFrontRHWindow.setEnabled(false);
                    binding.spinnerRearLHWindow.setSelection(0);
                    binding.spinnerRearLHWindow.setEnabled(false);
                    binding.spinnerRearRHWindow.setSelection(0);
                    binding.spinnerRearRHWindow.setEnabled(false);
                } else if (position == 2) {
                    binding.spinnerFrontLHWindow.setEnabled(true);
                    binding.spinnerFrontRHWindow.setEnabled(true);
                    binding.spinnerRearLHWindow.setEnabled(true);
                    binding.spinnerRearRHWindow.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void displayData() {
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.working_notWorking),
                newEvaluation.getOdometer(), binding.spinnerOdometer);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                newEvaluation.getSeatCover(), binding.spinnerSeatCover);
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.good_polishing_requires),
                newEvaluation.getDashboard(), binding.spinnerDashboard);
        SpinnerHelper.setSpinnerValueAirBag(getResources().getStringArray(R.array.airBags),
                newEvaluation.getAirBags(), binding.spinnerAirBags);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapAllWindowCondition.keySet()),
                newEvaluation.getAllWindowCondition(), binding.spinnerAllWindowCondition);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFrontRearRLWindow.keySet()),
                newEvaluation.getFrontLHWindow(), binding.spinnerFrontLHWindow);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFrontRearRLWindow.keySet()),
                newEvaluation.getFrontRHWindow(), binding.spinnerFrontRHWindow);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFrontRearRLWindow.keySet()),
                newEvaluation.getRearLHWindow(), binding.spinnerRearLHWindow);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapFrontRearRLWindow.keySet()),
                newEvaluation.getRearRHWindow(), binding.spinnerRearRHWindow);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin.keySet()),
                newEvaluation.getPlatformPassengerCabin(), binding.spinnerPlatformPassengerCabin);
        SpinnerHelper.setSpinnerValueByCode(new ArrayList<>(SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin.keySet()),
                newEvaluation.getPlatformBootSpace(), binding.spinnerPlatformBootSpace);
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setOdometer(String.valueOf(
                    binding.spinnerOdometer.getSelectedItemPosition()));
            newEvaluation.setSeatCover(
                    binding.spinnerSeatCover.getSelectedItem().toString().equals("Yes") ? "Y" : "N");
            newEvaluation.setDashboard(String.valueOf(
                    binding.spinnerDashboard.getSelectedItemPosition()));
            newEvaluation.setAirBags(binding.spinnerAirBags.getSelectedItem().toString());
            newEvaluation.setAllWindowCondition(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerAllWindowCondition.getSelectedItem().toString(),
                    SpinnerConstants.mapAllWindowCondition));
            if (!binding.spinnerAllWindowCondition.getSelectedItem().toString()
                    .equalsIgnoreCase("Good")) {
                newEvaluation.setFrontLHWindow(SpinnerHelper.getKeyFromMapValues(
                        binding.spinnerFrontLHWindow.getSelectedItem().toString(),
                        SpinnerConstants.mapFrontRearRLWindow));
                newEvaluation.setFrontRHWindow(SpinnerHelper.getKeyFromMapValues(
                        binding.spinnerFrontRHWindow.getSelectedItem().toString(),
                        SpinnerConstants.mapFrontRearRLWindow));
                newEvaluation.setRearLHWindow(SpinnerHelper.getKeyFromMapValues(
                        binding.spinnerRearLHWindow.getSelectedItem().toString(),
                        SpinnerConstants.mapFrontRearRLWindow));
                newEvaluation.setRearRHWindow(SpinnerHelper.getKeyFromMapValues(
                        binding.spinnerRearRHWindow.getSelectedItem().toString(),
                        SpinnerConstants.mapFrontRearRLWindow));
            }
            newEvaluation.setPlatformPassengerCabin(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerPlatformPassengerCabin.getSelectedItem().toString(),
                    SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin));
            newEvaluation.setPlatformBootSpace(SpinnerHelper.getKeyFromMapValues(
                    binding.spinnerPlatformBootSpace.getSelectedItem().toString(),
                    SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        interiorDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.spinnerOdometer.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerOdometer,
                    "Select odometer status");
        }
        if (binding.spinnerSeatCover.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerSeatCover,
                    "Select seat cover status");
        }
        if (binding.spinnerDashboard.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerDashboard,
                    "Select dashboard status");
        }
        if (binding.spinnerAirBags.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerAirBags,
                    "Select air bags status");
        }
        if (binding.spinnerAllWindowCondition.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerAllWindowCondition,
                    "Select All Window Condition status");
        }
        if (binding.spinnerAllWindowCondition.getSelectedItemPosition() == 2) {
            if (binding.spinnerFrontLHWindow.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerFrontLHWindow,
                        "Select Front LH Window status");
            }
            if (binding.spinnerFrontRHWindow.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerFrontRHWindow,
                        "Select Front RH Window status");
            }
            if (binding.spinnerRearLHWindow.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRearLHWindow,
                        "Select Rear LH Window status");
            }
            if (binding.spinnerRearRHWindow.getSelectedItemPosition() == 0) {
                return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerRearRHWindow,
                        "Select Rear RH Window status");
            }
        }
        saveData();
        return true;
    }

    @Override
    public void fillTestDataInForm() {
        binding.spinnerOdometer.setSelection(randomNumGen(2, 1));
        binding.spinnerSeatCover.setSelection(randomNumGen(2, 1));
        binding.spinnerDashboard.setSelection(randomNumGen(2, 1));
        binding.spinnerAirBags.setSelection(randomNumGen(6, 1));
        binding.spinnerAllWindowCondition.setSelection(randomNumGen(2, 1));
        binding.spinnerFrontLHWindow.setSelection(randomNumGen(3, 1));
        binding.spinnerFrontRHWindow.setSelection(randomNumGen(3, 1));
        binding.spinnerRearLHWindow.setSelection(randomNumGen(3, 1));
        binding.spinnerRearRHWindow.setSelection(randomNumGen(3, 1));
        binding.spinnerPlatformPassengerCabin.setSelection(randomNumGen(3, 1));
        binding.spinnerPlatformBootSpace.setSelection(randomNumGen(3, 1));
    }
}