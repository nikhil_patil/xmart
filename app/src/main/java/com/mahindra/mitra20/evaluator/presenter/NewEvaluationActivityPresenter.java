package com.mahindra.mitra20.evaluator.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.AggregateDetails;
import com.mahindra.mitra20.evaluator.models.EvaluationReportDetails;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.models.NewEvaluationRequest;
import com.mahindra.mitra20.evaluator.models.Stepper;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by PATINIK-CONT on 7/12/2019.
 */

public class NewEvaluationActivityPresenter implements VolleySingleTon.VolleyInteractor {

    private NewEvaluationActivityIn newEvaluationActivityIn;
    private Context context;

    public NewEvaluationActivityPresenter(Context context) {
        newEvaluationActivityIn = (NewEvaluationActivityIn) context;
        this.context = context;
    }

    public String prepareEvaluationDataJson(NewEvaluation newEvaluation, String userId) {
        List<AggregateDetails> aggregateDetailsList = new ArrayList<>();
        AggregateDetails aggregateDetails = new AggregateDetails("BDY",
                "BDY", "1",
                MultiSelectDialogHelper.getKeysFromValues(newEvaluation.getStrBodyRemark(),
                        SpinnerConstants.mapBodyRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getBodyCost(),
                newEvaluation.getStrBodyRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("ENG",
                "ENG", "2",
                MultiSelectDialogHelper.getKeysFromValues(newEvaluation.getStrEngineRemark(),
                        SpinnerConstants.mapEngineRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getEngineCost(),
                newEvaluation.getStrEngineRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("FIS",
                "FIS", "3", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrFuelAndIgnitionSystemRemark(),
                SpinnerConstants.mapFuelAndIgnitionRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getFuelAndIgnitionSystemCost(),
                newEvaluation.getStrFuelAndIgnitionSystemRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("TRN",
                "TRN", "4", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrTransmissionRemark(),
                SpinnerConstants.mapTransmissionRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getTransmissionCost(),
                newEvaluation.getStrTransmissionRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("BRS", "BRS",
                "5", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrBreakSystemRemark(),
                SpinnerConstants.mapBrakeSystemRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getBreakSystemCost(),
                newEvaluation.getStrBreakSystemRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("SUS", "SUS",
                "6", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrSuspensionSystemRemarks(),
                SpinnerConstants.mapSuspensionRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getSuspensionCost(),
                newEvaluation.getStrSuspensionSystemRemarksOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("STS", "STS",
                "7", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrSteeringSystemRemark(),
                SpinnerConstants.mapSteeringRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getSteeringSystemCost(),
                newEvaluation.getStrSteeringSystemRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("ACS", "ACS",
                "8", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrACSystemRemark(),
                SpinnerConstants.mapACRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getAcCost(),
                newEvaluation.getStrACSystemRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("ELS", "ELS",
                "9", MultiSelectDialogHelper.getKeysFromValues(
                newEvaluation.getStrElectricalSystemRemark(),
                SpinnerConstants.mapElectricalRemarks, MultiSelectDialogHelper.PIPE),
                newEvaluation.getElectricalSystemCost(),
                newEvaluation.getStrElectricalSystemRemarkOther());
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("GSC", "GSC",
                "10", newEvaluation.getStrGeneralServiceCostRemark(),
                newEvaluation.getGeneralServiceCost(), "");
        aggregateDetailsList.add(aggregateDetails);
        aggregateDetails = new AggregateDetails("FOP", "FOP",
                "11", "", "0", "");
        aggregateDetailsList.add(aggregateDetails);

        List<EvaluationReportDetails> evaluationReportDetailsList = new ArrayList<>();
        EvaluationReportDetails reportDetails = new EvaluationReportDetails(newEvaluation);
        evaluationReportDetailsList.add(reportDetails);

        String evaluatorId;
        if (userId.length() > 0) {
            if (userId.contains(".")) {
                String[] arrUserId = userId.split("\\.");
                evaluatorId = arrUserId[0];
            } else {
                evaluatorId = userId;
            }
        } else {
            evaluatorId = userId;
        }

        NewEvaluationRequest newEvaluationRequest = new NewEvaluationRequest(evaluatorId,
                evaluationReportDetailsList, aggregateDetailsList);
        Gson gson = new GsonBuilder().create();
        return gson.toJson(newEvaluationRequest);
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        newEvaluationActivityIn.onDataSubmissionSuccess(requestId, response);
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            newEvaluationActivityIn.onDataSubmissionFailed(requestId,
                    jsonObject.get("message").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Stepper> getStepperList() {
        List<Stepper> stepperList = new ArrayList<>();
        stepperList.add(new Stepper("Vehicle Evaluation Details", "Lead Det"));
        stepperList.add(new Stepper("Owner Details", "Own Det"));
        stepperList.add(new Stepper("Vehicle Details", "Veh Det"));
        stepperList.add(new Stepper("Accessories", "Accessories"));
        stepperList.add(new Stepper("Interior", "Interior"));
        stepperList.add(new Stepper("Exterior", "Exterior"));
        stepperList.add(new Stepper("Wheel and Tyres", "Wheels"));
        stepperList.add(new Stepper("Aggregate Details", "Refurb Det"));
        stepperList.add(new Stepper("Images", "Images"));
        stepperList.add(new Stepper("Audio & Overall Feedback", "Feedback"));
        return stepperList;
    }

    public void saveApptEvaluationDraft(NewEvaluation newEvaluation) {
        new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                EvaluatorConstants.EvaluationParts.APPT_DATA);
    }

    public void submitEvaluationData(Context context, JSONObject jsonObj) {
        VolleySingleTon.getInstance().connectToPostUrl(context, this,
                WebConstants.submitEvaluationReport.URL, jsonObj,
                WebConstants.submitEvaluationReport.REQUEST_CODE);
    }

    public NewEvaluation getEvaluationObject(Context context, String id) {
        return new EvaluatorHelper(context).getEvaluationDetailsById("", id);
    }

    public boolean validateVehicleEvaluation(NewEvaluation newEvaluation) {
        if (FieldValidator.isEmpty(context, newEvaluation.getOptedNewVehicle(), "") || newEvaluation.getOptedNewVehicle().contains("Select Model")) {
            CommonHelper.toast("Select Opted New Vehicle Model", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, newEvaluation.getEvaluatorName(), "")) {
            CommonHelper.toast("Enter evaluator name", context);
            return false;
        }
        return true;
    }

    public boolean validateOwnerDetails(NewEvaluation evaluation) {
        if (FieldValidator.isEmpty(context, evaluation.getOwnerName(), "Enter owner name")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getAddress(), "Enter owner address")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getEmailId(), "Enter owner's email")) {
            return false;
        }
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (!pattern.matcher(evaluation.getEmailId()).matches()) {
            CommonHelper.toast("Enter valid email id", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhoneNo(), "Enter owner's phone")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getMobileNo(), "Enter owner's mobile number")) {
            return false;
        }
        if (evaluation.getMobileNo().length() != 10) {
            CommonHelper.toast("Enter valid 10 digit mobile number", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getState()) || evaluation.getState().equalsIgnoreCase("Select State")) {
            CommonHelper.toast("Select State", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getCity()) || evaluation.getCity().equalsIgnoreCase("Select City")) {
            CommonHelper.toast("Select District and City", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getPinCode()) || evaluation.getPinCode().equalsIgnoreCase("Select Pincode")) {
            CommonHelper.toast("Select Pincode", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getOccupation()) || evaluation.getOccupation().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select Occupation", context);
            return false;
        }
        return true;
    }

    public boolean validateVehicleDetails(NewEvaluation evaluation) {
        if (FieldValidator.isEmpty(context, evaluation.getMake(), "Select vehicle make")) {
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getModel()) || evaluation.getModel().equalsIgnoreCase("Select Model")) {
            CommonHelper.toast_long("Select vehicle model", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getVariant()) || evaluation.getVariant().equalsIgnoreCase("Select Variant")) {
            CommonHelper.toast_long("Select vehicle Variant", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getYearOfMfg(), "Enter manufacture year")) {
            return false;
        }

        if (evaluation.getYearOfMfg().length() <= 4) {
            try {
                if (Integer.parseInt(new SimpleDateFormat("yyyy", Locale.ENGLISH)
                        .format(new Date())) < Integer.parseInt(evaluation.getYearOfMfg())) {
                    CommonHelper.toast("Manufacture year can't be greater than current year", context);
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (FieldValidator.isEmpty(context, evaluation.getYearOfReg(), "Enter registration year")) {
            return false;
        }

        if (evaluation.getYearOfReg().length() <= 4) {
            try {
                if (Integer.parseInt(new SimpleDateFormat("yyyy", Locale.ENGLISH)
                        .format(new Date())) < Integer.parseInt(
                        evaluation.getYearOfReg())) {
                    CommonHelper.toast("Registration year can't be greater than current year", context);
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            if (Integer.parseInt(evaluation.getYearOfMfg())
                    > Integer.parseInt(evaluation.getYearOfReg())) {
                CommonHelper.toast("Manufacturer year can't be greater than mfg. year", context);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (FieldValidator.isEmpty(context, evaluation.getRegNo(), "Enter registration number")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getMileage(), "Enter Odometer reading")) {
            return false;
        }
//        if (FieldValidator.isEmpty(context, evaluation.getMonthOfReg(), "Select month of registration")) {
//            return false;
//        }
        if (FieldValidator.isEmpty(context, evaluation.getEngineNo(), "Enter engine number")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getChassisNo(), "Enter chassis number")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getNoOfOwners(), "Enter Number of owners")) {
            return false;
        }
        if (Integer.parseInt(evaluation.getNoOfOwners()) == 0) {
            CommonHelper.toast("Number of Owners must be greater than 0", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTransmission(), "Select Transmission")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getServiceBooklet(), "Select Service Booklet")) {
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getColor()) || evaluation.getColor().equalsIgnoreCase("Select Colour")) {
            CommonHelper.toast_long("Select Vehicle Colour", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getFuelType()) || evaluation.getFuelType().equalsIgnoreCase("Select Fuel Type")) {
            CommonHelper.toast_long("Select Fuel Type", context);
            return false;
        }
        if (evaluation.getFuelType().equalsIgnoreCase("Ltiquefied Petroleum Gas")
                || evaluation.getFuelType().equalsIgnoreCase("Compressed Natural Gas")) {
            if (FieldValidator.isEmpty(context, evaluation.getCNGKit(), "Select CNG Kit")) {
                return false;
            }
        }
        if (TextUtils.isEmpty(evaluation.getEuroVersion()) || evaluation.getEuroVersion().equals("Select Value")) {
            CommonHelper.toast_long("Select Euro Version", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getVehicleUsage()) || evaluation.getVehicleUsage().equals("Select Vehicle Usage")) {
            CommonHelper.toast_long("Select vehicle usage", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getRCBook()) || evaluation.getRCBook().equals("Select Value")) {
            CommonHelper.toast("Select RC type", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getRcRemarks()) || evaluation.getRcRemarks().equals("Select Value")) {
            CommonHelper.toast("Select RC Remark", context);
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getInsurance()) || evaluation.getInsurance().equalsIgnoreCase("Select insurance")) {
            CommonHelper.toast_long("Select Insurance Type", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getInsuranceDate(), "Select Insurance expiry date")) {
            return false;
        }
        if (TextUtils.isEmpty(evaluation.getNcb()) || evaluation.getNcb().equals("Select NCB")) {
            CommonHelper.toast_long("Select NCB", context);
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPuc(), "Select PUC")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getHypothecation(), "Select Hypothecation")) {
            return false;
        }
        if (evaluation.getHypothecation().equalsIgnoreCase("Y")) {

            if (FieldValidator.isEmpty(context, evaluation.getFinanceCompany(), "Enter Finance Company name"))
                return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getNocStatus(), "Select NOC status")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getCustomerExpectedPrice(), "Enter customer expected price")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTestDrive(), "Select test drive")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getIsAccident(), "Select Accidental Status")) {
            return false;
        }
        if (evaluation.getIsAccident().equalsIgnoreCase("Y")) {
            if (TextUtils.isEmpty(evaluation.getAccidentalType()) || evaluation.getAccidentalType().equalsIgnoreCase("Select Value")) {
                CommonHelper.toast_long("Select Accidental Type", context);
                return false;
            }
        }

        if (FieldValidator.isEmpty(context, evaluation.getFitnessCertificate(), "Select fitness certificate")) {
            return false;
        }

        if (evaluation.getIsAccident().equalsIgnoreCase("Y") ||
                evaluation.getIsAccident().equalsIgnoreCase("Yes")) {
            if (evaluation.getAccidentalType().equalsIgnoreCase("Select Value")) {
                CommonHelper.toast("Select Accidental Type", context);
                return false;
            }

            if (FieldValidator.isEmpty(context, evaluation.getAccidentalRemarks(), "Enter Accidental Remarks")) {
                return false;
            }
        }

//        if (TextUtils.isEmpty(evaluation.getScrapVehicle()) || evaluation.getScrapVehicle().equalsIgnoreCase("Select Value")) {
//            CommonHelper.toast("Select Scrap Vehicle", context);
//            return false;
//        }

        if (TextUtils.isEmpty(evaluation.getRoadTax()) || evaluation.getRoadTax().equalsIgnoreCase("Select Road Tax")) {
            CommonHelper.toast("Select Road Tax", context);
            return false;
        }

        if (evaluation.getRoadTax().equalsIgnoreCase("V")) {
            if (FieldValidator.isEmpty(context, evaluation.getRoadTaxExpiryDate(), "Select road tax expiry date")) {
                return false;
            }
        }
        return true;
    }

    public boolean validateAccessoriesDetails(NewEvaluation evaluation) {
        if (null != evaluation.getStereo() && evaluation.getStereo().length() == 0) {
            CommonHelper.toast("Select stereo", context);
            return false;
        }
        if (null != evaluation.getNoOfKeys() && evaluation.getNoOfKeys().length() == 0) {
            CommonHelper.toast("Select number of keys", context);
            return false;
        }
        if (null != evaluation.getReverseParking() && evaluation.getReverseParking().length() == 0) {
            CommonHelper.toast("Select reverse parking", context);
            return false;
        }
        if (null != evaluation.getPowerSteering() && evaluation.getPowerSteering().length() == 0) {
            CommonHelper.toast("Select power steering", context);
            return false;
        }
        if (null != evaluation.getAlloyWheels() && evaluation.getAlloyWheels().length() == 0) {
            CommonHelper.toast("Select alloy wheels", context);
            return false;
        }
        if (null != evaluation.getFogLamp() && evaluation.getFogLamp().length() == 0) {
            CommonHelper.toast("Select fog lamp", context);
            return false;
        }
        if (null != evaluation.getCentralLocking() && evaluation.getCentralLocking().length() == 0) {
            CommonHelper.toast("Select central locking", context);
            return false;
        }
        if (null != evaluation.getToolKitJack() && evaluation.getToolKitJack().length() == 0) {
            CommonHelper.toast("Select Tool kit and Jack", context);
            return false;
        }
        if (null != evaluation.getTubelessTyres() && evaluation.getTubelessTyres().length() == 0) {
            CommonHelper.toast("Select tubeless tyre", context);
            return false;
        }
        if (null != evaluation.getRearWiper() && evaluation.getRearWiper().length() == 0) {
            CommonHelper.toast("Select rear wiper", context);
            return false;
        }
        if (null != evaluation.getMusicSystem() && evaluation.getMusicSystem().length() == 0) {
            CommonHelper.toast("Select music system", context);
            return false;
        }
        if (null != evaluation.getFMRadio() && evaluation.getFMRadio().length() == 0) {
            CommonHelper.toast("Select fm radio", context);
            return false;
        }
        if (null != evaluation.getPowerWindow() && evaluation.getPowerWindow().length() == 0) {
            CommonHelper.toast("Select power window", context);
            return false;
        }
        if (null != evaluation.getPowerWindowType() && evaluation.getPowerWindowType().isEmpty() &&
                evaluation.getPowerWindow().equalsIgnoreCase("Y")) {
            CommonHelper.toast("Select power window types", context);
            return false;
        }
        if (null != evaluation.getAC() && evaluation.getAC().length() == 0) {
            CommonHelper.toast("Select AC", context);
            return false;
        }
        if (null != evaluation.getPowerAdjustableSeats() && evaluation.getPowerAdjustableSeats().length() == 0) {
            CommonHelper.toast("Select adjustable seats", context);
            return false;
        }
        if (null != evaluation.getKeyLessEntry() && evaluation.getKeyLessEntry().length() == 0) {
            CommonHelper.toast("Select keyless entry", context);
            return false;
        }
        if (null != evaluation.getPedalShifter() && evaluation.getPedalShifter().length() == 0) {
            CommonHelper.toast("Select paddle shifter", context);
            return false;
        }
        if (null != evaluation.getElectricallyAdjustableORVM() && evaluation.getElectricallyAdjustableORVM().length() == 0) {
            CommonHelper.toast("Select electrically adjustable ORVM", context);
            return false;
        }
        if (null != evaluation.getRearDefogger() && evaluation.getRearDefogger().length() == 0) {
            CommonHelper.toast("Select defogger", context);
            return false;
        }
        if (null != evaluation.getSunRoof() && evaluation.getSunRoof().length() == 0) {
            CommonHelper.toast("Select sun roof", context);
            return false;
        }
        if (null != evaluation.getAutoClimateTechnologies() && evaluation.getAutoClimateTechnologies().length() == 0) {
            CommonHelper.toast("Select auto climate technologies", context);
            return false;
        }
        if (null != evaluation.getLeatherSeats() && evaluation.getLeatherSeats().length() == 0) {
            CommonHelper.toast("Select leather seats", context);
            return false;
        }
        if (null != evaluation.getBluetoothAudioSystems() && evaluation.getBluetoothAudioSystems().length() == 0) {
            CommonHelper.toast("Select bluetooth audio systems", context);
            return false;
        }
        if (null != evaluation.getGPSNavigationSystems() && evaluation.getGPSNavigationSystems().length() == 0) {
            CommonHelper.toast("Select navigation systems", context);
            return false;
        }
        return true;
    }

    public boolean validateInteriorDetails(NewEvaluation evaluation) {
        if (evaluation.getOdometer().isEmpty() || evaluation.getOdometer().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select odometer status", context);
            return false;
        }
        if (evaluation.getSeatCover().isEmpty() || evaluation.getSeatCover().equalsIgnoreCase("Select Value")) {
            CommonHelper.toast("Select seat cover status", context);
            return false;
        }
        if (evaluation.getDashboard().isEmpty() || evaluation.getDashboard().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select dashboard status", context);
            return false;
        }
        if (evaluation.getAirBags().isEmpty() || evaluation.getAirBags().equalsIgnoreCase("Select Value")) {
            CommonHelper.toast("Select air bags status", context);
            return false;
        }
        if (evaluation.getAllWindowCondition().isEmpty()) {
            CommonHelper.toast("Select All Window Condition status", context);
            return false;
        }
        if (evaluation.getAllWindowCondition().equalsIgnoreCase("BAD")) {
            if (evaluation.getFrontLHWindow().isEmpty()) {
                CommonHelper.toast("Select Front LH Window status", context);
                return false;
            }
            if (evaluation.getFrontRHWindow().isEmpty()) {
                CommonHelper.toast("Select Front RH Window status", context);
                return false;
            }
            if (evaluation.getRearLHWindow().isEmpty()) {
                CommonHelper.toast("Select Rear LH Window status", context);
                return false;
            }
            if (evaluation.getRearRHWindow().isEmpty()) {
                CommonHelper.toast("Select Rear RH Window status", context);
                return false;
            }
        }
        return true;
    }

    public boolean validateExteriorDetails(NewEvaluation evaluation) {
        if (evaluation.getFrontFenderRightSide().isEmpty()) {
            CommonHelper.toast("Select front fender right side status", context);
            return false;
        }
        if (evaluation.getFrontDoorRightSide().isEmpty()) {
            CommonHelper.toast("Select front door right side status", context);
            return false;
        }
        if (evaluation.getBackDoorRightSide().isEmpty()) {
            CommonHelper.toast("Select back door right side status", context);
            return false;
        }
        if (evaluation.getBackFenderRightSide().isEmpty()) {
            CommonHelper.toast("Select back fender right side status", context);
            return false;
        }
        if (evaluation.getBonnet().isEmpty()) {
            CommonHelper.toast("Select bonnet status", context);
            return false;
        }
        if (evaluation.getTailGate().isEmpty()) {
            CommonHelper.toast("Select tail gate status", context);
            return false;
        }
        if (evaluation.getFrontFenderLeftHandSide().isEmpty()) {
            CommonHelper.toast("Select front fender left side status", context);
            return false;
        }
        if (evaluation.getFrontDoorLeftHandSide().isEmpty()) {
            CommonHelper.toast("Select front door left side status", context);
            return false;
        }
        if (evaluation.getBackDoorLeftHandSide().isEmpty()) {
            CommonHelper.toast("Select back door left side status", context);
            return false;
        }
        if (evaluation.getBackFenderLeftHandSide().isEmpty()) {
            CommonHelper.toast("Select back fender left side status", context);
            return false;
        }
        if (evaluation.getWindShield().isEmpty() || evaluation.getWindShield().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select wind shield status", context);
            return false;
        }

        if (evaluation.getDickyDoor().isEmpty()) {
            CommonHelper.toast("Select Dicky Door status", context);
            return false;
        }

        if (evaluation.getPillars().isEmpty() || evaluation.getPillars().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select pillars status", context);
            return false;
        }
        if (evaluation.getPillars().equalsIgnoreCase("2") || evaluation.getPillars().equalsIgnoreCase("3")) {
            if (evaluation.getPillarType().isEmpty()) {
                CommonHelper.toast("Select pillar type", context);
                return false;
            }
        }
        if (evaluation.getMirrors().isEmpty() || evaluation.getMirrors().equalsIgnoreCase("0")) {
            CommonHelper.toast("Select mirrors status", context);
            return false;
        }
        if (evaluation.getHeadLightLH().isEmpty()) {
            CommonHelper.toast("Select Head Light LH status", context);
            return false;
        }
        if (evaluation.getHeadLightRH().isEmpty()) {
            CommonHelper.toast("Select Head Light RH status", context);
            return false;
        }
        if (evaluation.getTailLightLH().isEmpty()) {
            CommonHelper.toast("Select Tail Light LH status", context);
            return false;
        }
        if (evaluation.getTailLightRH().isEmpty()) {
            CommonHelper.toast("Select Tail Light RH status", context);
            return false;
        }
        if (evaluation.getFrontBumper().isEmpty()) {
            CommonHelper.toast("Select Front Bumper status", context);
            return false;
        }
        if (evaluation.getBackBumper().isEmpty()) {
            CommonHelper.toast("Select Back Bumper status", context);
            return false;
        }
        if (evaluation.getOutsideRearViewMirrorLH().isEmpty()) {
            CommonHelper.toast("Select OutSide RearView Mirror LH status", context);
            return false;
        }
        if (evaluation.getOutsideRearViewMirrorRH().isEmpty()) {
            CommonHelper.toast("Select OutSide RearView Mirror RH status", context);
            return false;
        }
        if (evaluation.getApronFrontLH().isEmpty()) {
            CommonHelper.toast("Select Apron Front LH status", context);
            return false;
        }
        if (evaluation.getApronFrontRH().isEmpty()) {
            CommonHelper.toast("Select Apron Front RH status", context);
            return false;
        }
        if (evaluation.getStrutMountingArea().isEmpty()) {
            CommonHelper.toast("Select Strut Mounting Area status", context);
            return false;
        }
        if (evaluation.getFirewall().isEmpty()) {
            CommonHelper.toast("Select Firewall status", context);
            return false;
        }
        if (evaluation.getRunningBoardLH().isEmpty()) {
            CommonHelper.toast("Select Running Board LH status", context);
            return false;
        }
        if (evaluation.getRunningBoardRH().isEmpty()) {
            CommonHelper.toast("Select Running Board RH status", context);
            return false;
        }
        return true;
    }

    public boolean validateWheelsAndTyresDetails(NewEvaluation evaluation) {
        if (FieldValidator.isEmpty(context, evaluation.getWheels(), "Select wheels type")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTyreFrontLeft(), "Select tyre front left status")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTyreFrontRight(), "Select tyre front right status")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTyreRearLeft(), "Select tyre rear left status")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getTyreRearRight(), "Select tyre rear right status")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getSpareTyre(), "Select spare tyre status")) {
            return false;
        }
        return true;
    }

    public boolean validateAggregateDetails(NewEvaluation evaluation) {
        if (!TextUtils.isEmpty(evaluation.getBodyCost()) && !evaluation.getBodyCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrBodyRemark(), "Enter body cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getEngineCost()) && !evaluation.getEngineCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrEngineRemark(), "Enter engine cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getFuelAndIgnitionSystemCost()) && !evaluation.getFuelAndIgnitionSystemCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrFuelAndIgnitionSystemRemark(), "Enter fuel and ignition cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getTransmissionCost()) && !evaluation.getTransmissionCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrTransmissionRemark(), "Enter transmission cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getBreakSystemCost())  && !evaluation.getBreakSystemCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrBreakSystemRemark(), "Enter break system cost")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getSuspensionCost())  && !evaluation.getSuspensionCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrSuspensionSystemRemarks(), "Enter suspension system cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getSteeringSystemCost())  && !evaluation.getSteeringSystemCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrSteeringSystemRemark(), "Enter steering system cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getAcCost())  && !evaluation.getAcCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrACSystemRemark(), "Enter AC cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getElectricalSystemCost())  && !evaluation.getElectricalSystemCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrElectricalSystemRemark(), "Enter electrical system cost remark")) {
                return false;
            }
        }
        if (!TextUtils.isEmpty(evaluation.getGeneralServiceCost())  && !evaluation.getGeneralServiceCost().equalsIgnoreCase("0")) {
            if (FieldValidator.isEmpty(context, evaluation.getStrGeneralServiceCostRemark(), "Enter general service cost remark")) {
                return false;
            }
        }
        return true;
    }

    public boolean validateImagesDetails(NewEvaluation evaluation) {
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathFrontImage(), "Select Front Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRearImage(), "Select Rear Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPath45DegreeLeftView(), "Select 45 Degree Left Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPath45DegreeRightView(), "Select 45 Degree Right Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathDashboardView(), "Select Dashboard Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathOdometerImage(), "Capture Odometer Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathChassisNoImage(), "Capture Chassis Number Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRoofTopImage(), "Capture Roof Top Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathInteriorView(), "Capture Interior Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRCCopyImage(), "Capture RC Image")) {
            return false;
        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRCCopy1(), "Capture RC Copy1 Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRCCopy2(), "Capture RC Copy2 Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRCCopy3(), "Capture RC Copy3 Image")) {
//            return false;
//        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathInsuranceImage(), "Capture Insurance Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRearLeftTyre(), "Capture Rear Left Tyre Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathRearRightTyre(), "Capture Rear Right Tyre Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathFrontLeftTyre(), "Capture Front Left Tyre Image")) {
            return false;
        }
        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathFrontRightTyre(), "Capture Front Right Tyre Image")) {
            return false;
        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathEngineNoImage(), "Capture Engine Number Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathDamagedPartImage1(), "Capture Damage 1 Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathDamagedPartImage2(), "Capture Damage 2 Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathDamagedPartImage3(), "Capture Damage 3 Image")) {
//            return false;
//        }
//        if (FieldValidator.isEmpty(context, evaluation.getPhotoPathDamagedPartImage4(), "Capture Damage 4 Image")) {
//            return false;
//        }


        return true;
    }

    public boolean validateAudioAndFeedbackDetails(NewEvaluation evaluation) {
        if (TextUtils.isEmpty(evaluation.getOverallFeedbackRating()) || evaluation.getOverallFeedbackRating().equals("0")) {
            CommonHelper.toast("Please Select Rating", context);
            return false;
        }
        return true;
    }
}