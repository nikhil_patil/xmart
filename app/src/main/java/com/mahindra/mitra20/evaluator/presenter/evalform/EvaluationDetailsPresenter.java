package com.mahindra.mitra20.evaluator.presenter.evalform;

import android.content.Context;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

import java.util.List;

/**
 * Created by PATINIK-CONT on 30/12/2019.
 */

public class EvaluationDetailsPresenter {

    private Context context;

    public EvaluationDetailsPresenter(Context context) {
        this.context = context;
    }

    public List<MasterModel> getModelMaster(String make) {
        MasterDatabaseHelper masterDatabaseHelper = new MasterDatabaseHelper(context);
        return masterDatabaseHelper.getModelMaster(make);
    }

    public void saveData(NewEvaluation newEvaluation) {
        try {
            new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                    EvaluatorConstants.EvaluationParts.VEHICLE_EVALUATION_DETAILS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NewEvaluation getEvaluationData(String evaluationId) {
        return new NewEvaluationHelper(context).getEvaluationDetails(evaluationId,
                        EvaluatorConstants.EvaluationParts.VEHICLE_EVALUATION_DETAILS);
    }
}