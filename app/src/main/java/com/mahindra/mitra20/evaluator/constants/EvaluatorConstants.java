package com.mahindra.mitra20.evaluator.constants;

/**
 * Created by BADHAP-CONT on 8/12/2018.
 */

public interface EvaluatorConstants {
    int DASHBOARD_DATA_LIMIT = 5;

    String OPEN_STATUS = "OPEN";
    String OPEN_STATUS_KEY = "OPN";

    String RESERVE_PRICE_STATUS_KEY = "RES";

    String OFFER_PRICE_STATUS_KEY = "OFFR";

    String PROCUREMENT_STATUS_KEY = "PCS";
    String PROCUREMENT_STATUS = "Procurement Status Completed";

    String FOLLOWUP_CUSTOMER_STATUS_KEY = "FOL";
    String FOLLOWUP_CUSTOMER_STATUS = "Followup custmer";

    String EVALUATION_DB_STATUS_DRAFT = "0";
    String EVALUATION_DB_STATUS_COMPLETED = "1";
    String EVALUATION_DB_STATUS_ONLINE = "2";
    String FROM_SCHEDULE_APPOINTMENT_SCREEN = "from_schedule_appointment_screen";
    String FROM_NEW_EVALUTION_SCREEN = "from_new_evalution_screen";
    String IS_DRAFT_SAVED = "is_draft_saved";
    String FROM_APPOINTMENT_DETAILS_SCREEN = "from_appointment_details_screen";
    String FROM_LOGIN_SCREEN = "from_login_screen";

    //Extras Constants
    String EXTRA_EVAL_ID = "ID";

    interface EvaluationParts {
        int VEHICLE_EVALUATION_DETAILS = 0;
        int OWNER_DETAILS = 1;
        int VEHICLE_DETAILS = 2;
        int ACCESSORIES = 3;
        int INTERIOR = 4;
        int EXTERIOR = 5;
        int WHEELS_AND_TYRES = 6;
        int AGGREGATE_DETAILS = 7;
        int IMAGES = 8;
        int AUDIO_AND_OVERALL_FEEDBACK = 9;
        int APPT_DATA = 10;
    }

    interface EvaluationFragmentTags {
        String VEHICLE_EVALUATION_DETAILS = "VEHICLE_EVALUATION_DETAILS";
        String OWNER_DETAILS = "OWNER_DETAILS";
        String VEHICLE_DETAILS = "VEHICLE_DETAILS";
        String ACCESSORIES = "ACCESSORIES";
        String INTERIOR = "INTERIOR";
        String EXTERIOR = "EXTERIOR";
        String WHEELS_AND_TYRES = "WHEELS_AND_TYRES";
        String AGGREGATE_DETAILS = "AGGREGATE_DETAILS";
        String IMAGES = "IMAGES";
        String AUDIO_AND_OVERALL_FEEDBACK = "AUDIO_AND_OVERALL_FEEDBACK";
    }
}