package com.mahindra.mitra20.evaluator.models;

import java.util.List;

public class NewEvaluationRequest {

    private String EvaluatorID;

    private List<EvaluationReportDetails> EvaluationReportDetails;

    private List<AggregateDetails> AggregateDetails;

    public NewEvaluationRequest(String evaluatorID,
                                List<EvaluationReportDetails> evaluationReportDetails,
                                List<AggregateDetails> aggregateDetails) {
        EvaluatorID = evaluatorID;
        EvaluationReportDetails = evaluationReportDetails;
        AggregateDetails = aggregateDetails;
    }
}
