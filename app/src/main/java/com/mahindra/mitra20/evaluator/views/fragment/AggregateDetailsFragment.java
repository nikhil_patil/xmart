package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.LayoutAggregateDetailsBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.AggregateDetailsPresenter;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.interfaces.CommonMethods;

public class AggregateDetailsFragment extends Fragment implements CommonMethods, View.OnClickListener {

    LayoutAggregateDetailsBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private AggregateDetailsPresenter aggregateDetailsPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutAggregateDetailsBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        aggregateDetailsPresenter = new AggregateDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = aggregateDetailsPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        binding.editTextBody.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextEngine.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextFuelAndIgnitionSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextTransmission.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextBreakSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextSuspensionSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextSteeringSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextACSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextElectricalSystem.addTextChangedListener(countAggregateTextWatcher);
        binding.editTextGeneralServiceCost.addTextChangedListener(countTotalTextWatcher);
        binding.editTextTotalOfAggregate.addTextChangedListener(countTotalTextWatcher);
        binding.editTextBodyRemark.setOnClickListener(this);
        binding.editTextEngineRemark.setOnClickListener(this);
        binding.editTextFuelAndIgnitionSystemRemark.setOnClickListener(this);
        binding.editTextTransmissionRemark.setOnClickListener(this);
        binding.editTextBreakSystemRemark.setOnClickListener(this);
        binding.editTextSuspensionSystemRemark.setOnClickListener(this);
        binding.editTextSteeringSystemRemark.setOnClickListener(this);
        binding.editTextACSystemRemark.setOnClickListener(this);
        binding.editTextElectricalSystemRemark.setOnClickListener(this);
    }

    @Override
    public void displayData() {
        binding.editTextBody.setText(
                newEvaluation.getBodyCost().replace(".0", ""));
        binding.editTextEngine.setText(
                newEvaluation.getEngineCost().replace(".0", ""));
        binding.editTextFuelAndIgnitionSystem.setText(
                newEvaluation.getFuelAndIgnitionSystemCost().replace(".0", ""));
        binding.editTextTransmission.setText(
                newEvaluation.getTransmissionCost().replace(".0", ""));
        binding.editTextBreakSystem.setText(
                newEvaluation.getBreakSystemCost().replace(".0", ""));
        binding.editTextSuspensionSystem.setText(
                newEvaluation.getSuspensionCost().replace(".0", ""));
        binding.editTextSteeringSystem.setText(
                newEvaluation.getSteeringSystemCost().replace(".0", ""));
        binding.editTextACSystem.setText(
                newEvaluation.getAcCost().replace(".0", ""));
        binding.editTextElectricalSystem.setText(
                newEvaluation.getElectricalSystemCost().replace(".0", ""));
        binding.editTextTotalOfAggregate.setText(
                newEvaluation.getTotalOfAggregateCost().replace(".0", ""));
        binding.editTextGeneralServiceCost.setText(
                newEvaluation.getGeneralServiceCost().replace(".0", ""));
        binding.editTextTotalRefurbishmentCost.setText(
                newEvaluation.getTotalRefurbishmentCost().replace(".0", ""));

        binding.editTextBodyRemark.setText(newEvaluation.getStrBodyRemark());
        binding.editTextEngineRemark.setText(newEvaluation.getStrEngineRemark());
        binding.editTextFuelAndIgnitionSystemRemark.setText(newEvaluation.getStrFuelAndIgnitionSystemRemark());
        binding.editTextTransmissionRemark.setText(newEvaluation.getStrTransmissionRemark());
        binding.editTextBreakSystemRemark.setText(newEvaluation.getStrBreakSystemRemark());
        binding.editTextSuspensionSystemRemark.setText(newEvaluation.getStrSuspensionSystemRemarks());
        binding.editTextSteeringSystemRemark.setText(newEvaluation.getStrSteeringSystemRemark());
        binding.editTextACSystemRemark.setText(newEvaluation.getStrACSystemRemark());
        binding.editTextElectricalSystemRemark.setText(newEvaluation.getStrElectricalSystemRemark());

        if (newEvaluation.getStrBodyRemark().contains("Other")) {
            binding.editTextBodyRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrEngineRemark().contains("Other")) {
            binding.editTextEngineRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrFuelAndIgnitionSystemRemark().contains("Other")) {
            binding.editTextFuelAndIgnitionSystemRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrTransmissionRemark().contains("Other")) {
            binding.editTextTransmissionRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrBreakSystemRemark().contains("Other")) {
            binding.editTextBreakSystemRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrSuspensionSystemRemarks().contains("Other")) {
            binding.editTextSuspensionSystemRemarksOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrSteeringSystemRemark().contains("Other")) {
            binding.editTextSteeringSystemRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrACSystemRemark().contains("Other")) {
            binding.editTextACSystemRemarkOther.setVisibility(View.VISIBLE);
        }
        if (newEvaluation.getStrElectricalSystemRemark().contains("Other")) {
            binding.editTextElectricalSystemRemarkOther.setVisibility(View.VISIBLE);
        }

        binding.editTextBodyRemarkOther.setText(newEvaluation.getStrBodyRemarkOther());
        binding.editTextEngineRemarkOther.setText(newEvaluation.getStrEngineRemarkOther());
        binding.editTextFuelAndIgnitionSystemRemarkOther.setText(newEvaluation.getStrFuelAndIgnitionSystemRemarkOther());
        binding.editTextTransmissionRemarkOther.setText(newEvaluation.getStrTransmissionRemarkOther());
        binding.editTextBreakSystemRemarkOther.setText(newEvaluation.getStrBreakSystemRemarkOther());
        binding.editTextSuspensionSystemRemarksOther.setText(newEvaluation.getStrSuspensionSystemRemarksOther());
        binding.editTextSteeringSystemRemarkOther.setText(newEvaluation.getStrSteeringSystemRemarkOther());
        binding.editTextACSystemRemarkOther.setText(newEvaluation.getStrACSystemRemarkOther());
        binding.editTextElectricalSystemRemarkOther.setText(newEvaluation.getStrElectricalSystemRemarkOther());
        binding.editTextGeneralServiceCostRemark.setText(newEvaluation.getStrGeneralServiceCostRemark());
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setBodyCost((
                    binding.editTextBody.getText().length() > 0) ?
                    binding.editTextBody.getText().toString().trim() : "0");
            newEvaluation.setEngineCost((
                    binding.editTextEngine.getText().length() > 0) ?
                    binding.editTextEngine.getText().toString().trim() : "0");
            newEvaluation.setFuelAndIgnitionSystemCost((
                    binding.editTextFuelAndIgnitionSystem.getText().length() > 0) ?
                    binding.editTextFuelAndIgnitionSystem.getText().toString().trim() : "0");
            newEvaluation.setTransmissionCost((
                    binding.editTextTransmission.getText().length() > 0) ?
                    binding.editTextTransmission.getText().toString().trim() : "0");
            newEvaluation.setBreakSystemCost((
                    binding.editTextBreakSystem.getText().length() > 0) ?
                    binding.editTextBreakSystem.getText().toString().trim() : "0");
            newEvaluation.setSuspensionCost((
                    binding.editTextSuspensionSystem.getText().length() > 0) ?
                    binding.editTextSuspensionSystem.getText().toString().trim() : "0");
            newEvaluation.setSteeringSystemCost((
                    binding.editTextSteeringSystem.getText().length() > 0) ?
                    binding.editTextSteeringSystem.getText().toString().trim() : "0");
            newEvaluation.setAcCost((
                    binding.editTextACSystem.getText().length() > 0) ?
                    binding.editTextACSystem.getText().toString().trim() : "0");
            newEvaluation.setElectricalSystemCost((
                    binding.editTextElectricalSystem.getText().length() > 0) ?
                    binding.editTextElectricalSystem.getText().toString().trim() : "0");
            newEvaluation.setTotalOfAggregateCost((
                    binding.editTextTotalOfAggregate.getText().length() > 0) ?
                    binding.editTextTotalOfAggregate.getText().toString().trim() : "0");
            newEvaluation.setGeneralServiceCost((
                    binding.editTextGeneralServiceCost.getText().length() > 0) ?
                    binding.editTextGeneralServiceCost.getText().toString().trim() : "0");
            newEvaluation.setTotalRefurbishmentCost((
                    binding.editTextTotalRefurbishmentCost.getText().length() > 0) ?
                    binding.editTextTotalRefurbishmentCost.getText().toString().trim() : "0");

            newEvaluation.setStrBodyRemark(
                    binding.editTextBodyRemark.getText().toString().trim());
            newEvaluation.setStrEngineRemark(
                    binding.editTextEngineRemark.getText().toString().trim());
            newEvaluation.setStrFuelAndIgnitionSystemRemark(
                    binding.editTextFuelAndIgnitionSystemRemark.getText().toString().trim());
            newEvaluation.setStrTransmissionRemark(
                    binding.editTextTransmissionRemark.getText().toString().trim());
            newEvaluation.setStrBreakSystemRemark(
                    binding.editTextBreakSystemRemark.getText().toString().trim());
            newEvaluation.setStrSuspensionSystemRemarks(
                    binding.editTextSuspensionSystemRemark.getText().toString().trim());
            newEvaluation.setStrSteeringSystemRemark(
                    binding.editTextSteeringSystemRemark.getText().toString().trim());
            newEvaluation.setStrACSystemRemark(
                    binding.editTextACSystemRemark.getText().toString().trim());
            newEvaluation.setStrElectricalSystemRemark(
                    binding.editTextElectricalSystemRemark.getText().toString().trim());
            newEvaluation.setStrGeneralServiceCostRemark(
                    binding.editTextGeneralServiceCostRemark.getText().toString().trim());

            newEvaluation.setStrBodyRemarkOther(
                    binding.editTextBodyRemarkOther.getText().toString().trim());
            newEvaluation.setStrEngineRemarkOther(
                    binding.editTextEngineRemarkOther.getText().toString().trim());
            newEvaluation.setStrFuelAndIgnitionSystemRemarkOther(
                    binding.editTextFuelAndIgnitionSystemRemarkOther.getText().toString().trim());
            newEvaluation.setStrTransmissionRemarkOther(
                    binding.editTextTransmissionRemarkOther.getText().toString().trim());
            newEvaluation.setStrBreakSystemRemarkOther(
                    binding.editTextBreakSystemRemarkOther.getText().toString().trim());
            newEvaluation.setStrSuspensionSystemRemarksOther(
                    binding.editTextSuspensionSystemRemarksOther.getText().toString().trim());
            newEvaluation.setStrSteeringSystemRemarkOther(
                    binding.editTextSteeringSystemRemarkOther.getText().toString().trim());
            newEvaluation.setStrACSystemRemarkOther(
                    binding.editTextACSystemRemarkOther.getText().toString().trim());
            newEvaluation.setStrElectricalSystemRemarkOther(
                    binding.editTextElectricalSystemRemarkOther.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        aggregateDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.editTextBody.getText().length() > 0) {
            if (binding.editTextBodyRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextBodyRemark, "Enter body cost remark");
            }
        }
        if (binding.editTextEngine.getText().length() > 0) {
            if (binding.editTextEngineRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextEngineRemark, "Enter engine cost remark");
            }
        }
        if (binding.editTextFuelAndIgnitionSystem.getText().length() > 0) {
            if (binding.editTextFuelAndIgnitionSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextFuelAndIgnitionSystemRemark, "Enter fuel and ignition cost remark");
            }
        }
        if (binding.editTextTransmission.getText().length() > 0) {
            if (binding.editTextTransmissionRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextTransmissionRemark, "Enter transmission cost remark");
            }
        }
        if (binding.editTextBreakSystem.getText().length() > 0) {
            if (binding.editTextBreakSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextBreakSystemRemark, "Enter break system cost");
            }
        }
        if (binding.editTextSuspensionSystem.getText().length() > 0) {
            if (binding.editTextSuspensionSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextSuspensionSystemRemark, "Enter suspension system cost remark");
            }
        }
        if (binding.editTextSteeringSystem.getText().length() > 0) {
            if (binding.editTextSteeringSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextSteeringSystemRemark, "Enter steering system cost remark");
            }
        }
        if (binding.editTextACSystem.getText().length() > 0) {
            if (binding.editTextACSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextACSystemRemark, "Enter AC cost remark");
            }
        }
        if (binding.editTextElectricalSystem.getText().length() > 0) {
            if (binding.editTextElectricalSystemRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextElectricalSystemRemark, "Enter electrical system cost remark");
            }
        }
        if (binding.editTextGeneralServiceCost.getText().length() > 0) {
            if (binding.editTextGeneralServiceCostRemark.getText().length() == 0) {
                return FieldValidator.focusViewAndDisplayMessage(context,
                        binding.editTextGeneralServiceCostRemark, "Enter general service cost remark");
            }
        }
        saveData();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editTextBodyRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBodyRemark,
                        SpinnerConstants.mapBodyRemarks, "Choose body system remarks",
                        binding.editTextBodyRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextEngineRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextEngineRemark,
                        SpinnerConstants.mapEngineRemarks, "Choose engine system remarks",
                        binding.editTextEngineRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextFuelAndIgnitionSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextFuelAndIgnitionSystemRemark,
                        SpinnerConstants.mapFuelAndIgnitionRemarks, "Choose fuel and ignition remarks",
                        binding.editTextFuelAndIgnitionSystemRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextTransmissionRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextTransmissionRemark,
                        SpinnerConstants.mapTransmissionRemarks, "Choose transmission remarks",
                        binding.editTextTransmissionRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextBreakSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextBreakSystemRemark,
                        SpinnerConstants.mapBrakeSystemRemarks, "Choose break system remarks",
                        binding.editTextBreakSystemRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextSuspensionSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextSuspensionSystemRemark,
                        SpinnerConstants.mapSuspensionRemarks, "Choose suspension remarks",
                        binding.editTextSuspensionSystemRemarksOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextSteeringSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextSteeringSystemRemark,
                        SpinnerConstants.mapSteeringRemarks, "Choose steering remarks",
                        binding.editTextSteeringSystemRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextACSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextACSystemRemark,
                        SpinnerConstants.mapACRemarks, "Choose ac system remarks",
                        binding.editTextACSystemRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
            case R.id.editTextElectricalSystemRemark:
                MultiSelectDialogHelper.showPicker(context, binding.editTextElectricalSystemRemark,
                        SpinnerConstants.mapElectricalRemarks, "Choose electrical system remarks",
                        binding.editTextElectricalSystemRemarkOther, MultiSelectDialogHelper.PIPE);
                break;
        }
    }

    TextWatcher countAggregateTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            binding.editTextTotalOfAggregate.setText(addAggregates());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    TextWatcher countTotalTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            binding.editTextTotalRefurbishmentCost.setText(addTotal());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private String addAggregates() {
        return String.valueOf(getInt(binding.editTextBody.getText().toString()) +
                getInt(binding.editTextEngine.getText().toString()) +
                getInt(binding.editTextFuelAndIgnitionSystem.getText().toString()) +
                getInt(binding.editTextTransmission.getText().toString()) +
                getInt(binding.editTextBreakSystem.getText().toString()) +
                getInt(binding.editTextSuspensionSystem.getText().toString()) +
                getInt(binding.editTextSteeringSystem.getText().toString()) +
                getInt(binding.editTextACSystem.getText().toString()) +
                getInt(binding.editTextElectricalSystem.getText().toString()));
    }

    private String addTotal() {
        return String.valueOf(getInt(binding.editTextTotalOfAggregate.getText().toString())
                + getInt(binding.editTextGeneralServiceCost.getText().toString()));
    }

    int getInt(String intStr) {
        try {
            if (intStr.length() > 0) {
                return Integer.parseInt(intStr);
            } else {
                return 0;
            }
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    @Override
    public void fillTestDataInForm() {
        binding.editTextBody.setText("1");
        binding.editTextEngine.setText("2");
        binding.editTextFuelAndIgnitionSystem.setText("3");
        binding.editTextTransmission.setText("4");
        binding.editTextBreakSystem.setText("5");
        binding.editTextSuspensionSystem.setText("6");
        binding.editTextSteeringSystem.setText("7");
        binding.editTextACSystem.setText("8");
        binding.editTextElectricalSystem.setText("9");
        binding.editTextGeneralServiceCost.setText("10");

        binding.editTextBodyRemark.setText(SpinnerConstants.mapBodyRemarks.get("RP"));
        binding.editTextEngineRemark.setText(SpinnerConstants.mapEngineRemarks.get("EOL"));
        binding.editTextFuelAndIgnitionSystemRemark.setText(SpinnerConstants.mapFuelAndIgnitionRemarks.get("STRD"));
        binding.editTextTransmissionRemark.setText(SpinnerConstants.mapTransmissionRemarks.get("GBLG"));
        binding.editTextBreakSystemRemark.setText(SpinnerConstants.mapBrakeSystemRemarks.get("BRFDLG"));
        binding.editTextSuspensionSystemRemark.setText(SpinnerConstants.mapSuspensionRemarks.get("SBRPMT"));
        binding.editTextSteeringSystemRemark.setText(SpinnerConstants.mapSteeringRemarks.get("PSNW"));
        binding.editTextACSystemRemark.setText(SpinnerConstants.mapACRemarks.get("GLG1"));
        binding.editTextElectricalSystemRemark.setText(SpinnerConstants.mapElectricalRemarks.get("BTDC"));
        binding.editTextGeneralServiceCostRemark.setText("Test Remarks");
    }
}