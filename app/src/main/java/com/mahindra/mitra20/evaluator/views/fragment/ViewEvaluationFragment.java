package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.champion.models.VehiclePhoto;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.databinding.FragmentCompletedEvaluationBinding;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.MultiSelectDialogHelper;
import com.mahindra.mitra20.helper.TouchImageView;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewEvaluationFragment extends Fragment implements View.OnClickListener {

    FragmentCompletedEvaluationBinding binding;
    EvaluatorHelper evaluatorHelper;
    MasterDatabaseHelper masterDatabaseHelper;
    NewEvaluation newEvaluation;
    boolean imagesAreOnline, isBroker = false;

    ArrayList<MasterModel> arrayListColor = new ArrayList<>();
    ArrayList<MasterModel> arrayListState = new ArrayList<>();
    ArrayList<MasterModel> arrayListCity = new ArrayList<>();
    ArrayList<MasterModel> arrayListVariant = new ArrayList<>();
    ArrayList<MasterModel> arrayListMake = new ArrayList<>();
    ArrayList<MasterModel> arrayListModel = new ArrayList<>();
    ArrayList<MasterModel> arrayListModelOptedNewVehicle = new ArrayList<>();
    ArrayList<MasterModel> arrayListVehicleSegment = new ArrayList<>();

    //Audio Variables
    private static boolean isPlaying = false;
    MediaPlayer mMediaPlayer = new MediaPlayer();
    //private static String audioURL = "https://mdwaccess.com/DMSContextREST/rest/xmart/downloadDocuments/VehicleImage?docId=3686";
    private static String audioAuthCode = "YWRtaW46UHJvZDEyMyE=";
    private static String audioContentType = "application/octet-stream";

    private int[] btn_idPurpose = {R.id.buttonExchange, R.id.buttonDirectBuy};
    private int[] btn_idFMRadio = {R.id.buttonFMRadioYes, R.id.buttonFMRadioNo};
    private int[] btn_idPowerWindow = {R.id.buttonPowerWindowYes, R.id.buttonPowerWindowNo};
    private int[] btn_idAC = {R.id.buttonACYes, R.id.buttonACNo};
    private int[] btn_idPowerAdjustableSeats = {R.id.buttonPowerAdjustableSeatsYes,
            R.id.buttonPowerAdjustableSeatsNo};
    private int[] btn_idKeyLessEntry = {R.id.buttonKeyleassEntryYes, R.id.buttonKeyleassEntryNo};
    private int[] btn_idPedalShifter = {R.id.buttonPedalShifterYes, R.id.buttonPedalShifterNo};
    private int[] btn_idElectricallyAdjustableORVM = {R.id.buttonElectricallyAdjustableORVMYes,
            R.id.buttonElectricallyAdjustableORVMNo};
    private int[] btn_idRearDefogger = {R.id.buttonRearDefoggerYes, R.id.buttonRearDefoggerNo};
    private int[] btn_idSunRoof = {R.id.buttonSunRoofYes, R.id.buttonSunRoofNo};
    private int[] btn_idAutoClimateTechnologies = {R.id.buttonAutoClimateTechnologiesYes,
            R.id.buttonAutoClimateTechnologiesNo};
    private int[] btn_idLeatherSeats = {R.id.buttonLeatherSeatsYes, R.id.buttonLeatherSeatsNo};
    private int[] btn_idBluetoothAudioSystems = {R.id.buttonBluetoothAudioSystemsYes,
            R.id.buttonBluetoothAudioSystemsNo};
    private int[] btn_idGPSNavigationSystems = {R.id.buttonGPSNavigationSystemsYes,
            R.id.buttonGPSNavigationSystemsNo};
    private int[] btn_idRearWiper = {R.id.buttonRearWiperYes, R.id.buttonRearWiperNo};
    private int[] btn_idTestDrive = {R.id.buttonTestDriveYes, R.id.buttonTestDriveNo};
    private int[] btn_idAccidentalStatus = {R.id.buttonAccidentalStatusYes, R.id.buttonAccidentalStatusNo};
    private int[] btn_idServiceBooklet = {R.id.buttonServiceBookletYes, R.id.buttonServiceBookletNo};
    private int[] btn_idNocStatus = {R.id.buttonNocStatusYes, R.id.buttonNocStatusNo};
    private int[] btn_idPuc = {R.id.buttonPucYes, R.id.buttonPucNo};
    private int[] btn_idHypothecation = {R.id.buttonHypothecationYes, R.id.buttonHypothecationNo};
    private int[] btn_idStereo = {R.id.buttonStereoYes, R.id.buttonStereoNo};
    private int[] btn_idReverseParking = {R.id.buttonReverseParkingYes, R.id.buttonReverseParkingNo};
    private int[] btn_idPowerSteering = {R.id.buttonPowerSteeringYes, R.id.buttonPowerSteeringNo};
    private int[] btn_idAlloyWheels = {R.id.buttonAlloyWheelsYes, R.id.buttonAlloyWheelsNo};
    private int[] btn_idFogLamp = {R.id.buttonFogLampYes, R.id.buttonFogLampNo};
    private int[] btn_idCentralLocking = {R.id.buttonCentralLockingYes, R.id.buttonCentralLockingNo};
    private int[] btn_idToolKitJack = {R.id.buttonToolKitJackYes, R.id.buttonToolKitJackNo};
    private int[] btn_idMusicSystem = {R.id.buttonMusicSystemYes, R.id.buttonMusicSystemNo};
    private int[] btn_idFitnessCertificate = {R.id.buttonFitnessCertYes, R.id.buttonFitnessCertNo};

    CommonHelper commonHelper;
    AppCompatActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentCompletedEvaluationBinding.inflate(inflater,
                container, false);
        if (getArguments() != null) {
            newEvaluation = getArguments().getParcelable(CommonHelper.EXTRA_EVALUATION);
            imagesAreOnline = getArguments().getBoolean(CommonHelper.EXTRA_ARE_IMAGES_ONLINE);
            isBroker = getArguments().getBoolean(CommonHelper.EXTRA_IS_BROKER);
            init();
        }
        return binding.getRoot();
    }

    private void init() {
        commonHelper = new CommonHelper();
        evaluatorHelper = new EvaluatorHelper(activity);
        masterDatabaseHelper = new MasterDatabaseHelper(activity);


        arrayListColor.addAll(masterDatabaseHelper.getColors());
        //COMMENTED BY PRAVIN DHARAM ON 13-12-2019
//        arrayListState.addAll(evaluatorHelper.getState());
        //arrayListCity.addAll(evaluatorHelper.getCity());
        //ADDED BY PRAVIN DHARAM ON 13-12-2019
        setStateCityAndPincode();
        arrayListMake.add(new MasterModel("Select Make", "Select Make"));
        arrayListMake.addAll(masterDatabaseHelper.getMakeMaster());
        arrayListModelOptedNewVehicle.add(new MasterModel("Select Model", "Select Model"));
        arrayListModelOptedNewVehicle.addAll(masterDatabaseHelper.getModelMaster("MAHINDRA"));
        arrayListVehicleSegment.addAll(masterDatabaseHelper.getVehicleSegmentMaster());

        if (isBroker) {
            binding.tableRowVehicleEvaluationDetails.setVisibility(View.GONE);
            binding.tableRowOwnersDetails.setVisibility(View.GONE);
        } else {
            binding.tableRowVehicleEvaluationDetails.setOnClickListener(this);
            binding.tableRowOwnersDetails.setOnClickListener(this);
        }

        if (newEvaluation.getAudioPathVehicleNoise().length() > 0)
            binding.tableRowViewAudio.setVisibility(View.VISIBLE);
        else
            binding.tableRowViewAudio.setVisibility(View.GONE);

        binding.tableRowVehicleDetails.setOnClickListener(this);
        binding.tableRowAddPhoto.setOnClickListener(this);
        binding.tableRowAggregateDetails.setOnClickListener(this);
        binding.tableAccessoriesStandardFeaturesDetails.setOnClickListener(this);
        binding.tableRowInterior.setOnClickListener(this);
        binding.tableRowExterior.setOnClickListener(this);
        binding.tableRowWheelsAndTyres.setOnClickListener(this);
        binding.tableRowViewAudio.setOnClickListener(this);

        binding.playViewBT.setOnClickListener(this);
        applyDataToView();
    }

    private void setStateCityAndPincode() {
        arrayListState = masterDatabaseHelper.getStateList();
        String stateID = "";

        for (int i = 0; i < arrayListState.size(); i++) {
            if (arrayListState.get(i).getCode().equalsIgnoreCase(newEvaluation.getState())
                    || arrayListState.get(i).getDescription().equalsIgnoreCase(newEvaluation.getState())) {
                binding.ownerDetailsView.textViewState.setText(arrayListState.get(i).getDescription());
                stateID = arrayListState.get(i).getCode();
            }
        }

        String cityID = "";

        String districtID = masterDatabaseHelper.getDistrictID(newEvaluation.getCity());

        arrayListCity = masterDatabaseHelper.getTehsilList(districtID);
        for (int i = 0; i < arrayListCity.size(); i++) {
            if (arrayListCity.get(i).getCode().equalsIgnoreCase(newEvaluation.getCity())
                    || arrayListCity.get(i).getDescription().equalsIgnoreCase(newEvaluation.getCity())) {
                binding.ownerDetailsView.textViewCity.setText(arrayListCity.get(i).getDescription());
                cityID = arrayListCity.get(i).getCode();
            }
        }


//        arrayListPincode = masterDatabaseHelper.getPinCodeList(cityID);
//        for (int i = 0; i < arrayListPincode.size(); i++) {
//            if (arrayListPincode.get(i).getCode().equalsIgnoreCase(newEvaluation.getPinCode())) {
//                binding.ownerDetailsView.textViewPincode.setText(arrayListPincode.get(i).getCode());
//            }
//        }

        binding.ownerDetailsView.textViewPincode.setText(newEvaluation.getPinCode());


    }

    void applyDataToView() {
        if (newEvaluation.getRequestId() != null || newEvaluation.getId() != null) {
            try {
                binding.vehicleDetailsView.textViewMake.setText(newEvaluation.getMake());
                binding.vehicleDetailsView.textViewModel.setText(newEvaluation.getModel());
                binding.vehicleDetailsView.textViewVariant.setText(newEvaluation.getVariant());

                binding.evaluationDetailsView.textViewAgainstTransaction.setText(
                        newEvaluation.getAgainstTransaction());
                binding.evaluationDetailsView.TextViewOptedNewVehicleModel.setText(
                        newEvaluation.getOptedNewVehicle());
                binding.evaluationDetailsView.textViewEvaluationDate.setText(
                        newEvaluation.getDate());
                binding.evaluationDetailsView.textViewEvaluationTime.setText(
                        newEvaluation.getTime());
                binding.evaluationDetailsView.editTextTransactionNo.setText(
                        newEvaluation.getTransactionNo());
                binding.evaluationDetailsView.editTextEvaluatorName.setText(
                        newEvaluation.getEvaluatorName());

                //Owner Details
                binding.ownerDetailsView.textViewOwnerName.setText(newEvaluation.getOwnerName());
                binding.ownerDetailsView.textViewAddress.setText(newEvaluation.getAddress());
                binding.ownerDetailsView.textViewEmailId.setText(newEvaluation.getEmailId());
                binding.ownerDetailsView.textViewPhoneNo.setText(newEvaluation.getPhoneNo());
                binding.ownerDetailsView.textViewMobileNo.setText(newEvaluation.getMobileNo());
                binding.ownerDetailsView.textViewState.setText(newEvaluation.getState());
                binding.ownerDetailsView.textViewCity.setText(newEvaluation.getCity());
                binding.ownerDetailsView.textViewPincode.setText(newEvaluation.getPinCode());
                setOccupationTextViewValueByCode(getResources().getStringArray(R.array.occupation_code),
                        newEvaluation.getOccupation(), binding.ownerDetailsView.textViewOccupation);

                //Vehicle Details
                try {
                    binding.vehicleDetailsView.textViewEvaluationCount.setText(String.valueOf(
                            Integer.parseInt(newEvaluation.getEvaluationCount().trim())));
                    binding.vehicleDetailsView.llEvaluationCount.setVisibility(View.VISIBLE);
                } catch (NumberFormatException e) {
                    binding.vehicleDetailsView.llEvaluationCount.setVisibility(View.GONE);
                }
                binding.vehicleDetailsView.textViewYearOfMfg.setText(newEvaluation.getYearOfMfg());
                binding.vehicleDetailsView.textViewYearOfReg.setText(newEvaluation.getYearOfReg());
                if (isBroker) {
                    int endIndex = newEvaluation.getRegNo().length();
                    if (endIndex > 4) {
                        endIndex = 4;
                    }
                    binding.vehicleDetailsView.textViewRegNo.setText(
                            newEvaluation.getRegNo().substring(0, endIndex));
                } else {
                    binding.vehicleDetailsView.textViewRegNo.setText(newEvaluation.getRegNo());
                }
                binding.vehicleDetailsView.textViewMilege.setText(newEvaluation.getMileage());
                binding.vehicleDetailsView.textViewMonthOfMfg.setText(newEvaluation.getMonthOfMfg());
                binding.vehicleDetailsView.textViewMonthOfReg.setText(newEvaluation.getMonthOfReg());
                binding.vehicleDetailsView.textViewEngineNo.setText(newEvaluation.getEngineNo());
                binding.vehicleDetailsView.textViewNumberOfOwners.setText(newEvaluation.getNoOfOwners());
                if (isBroker) {
                    binding.vehicleDetailsView.trChassisNO.setVisibility(View.GONE);
                    binding.vehicleDetailsView.textViewChassisNo.setVisibility(View.GONE);

                    binding.vehicleDetailsView.trCustomerExpectedPrice.setVisibility(View.GONE);
                    binding.vehicleDetailsView.textViewCustomerExpectedPrice.setVisibility(View.GONE);
                } else {
                    binding.vehicleDetailsView.textViewCustomerExpectedPrice.setText(
                            newEvaluation.getCustomerExpectedPrice());
                    binding.vehicleDetailsView.textViewChassisNo.setText(newEvaluation.getChassisNo());
                }

                if (newEvaluation.getTransmission().equalsIgnoreCase("A")) {
                    binding.vehicleDetailsView.linearLayoutAutomatic.setBackgroundResource(
                            R.drawable.gray_rounded_bg);
                    binding.vehicleDetailsView.imageViewAutomatic.setColorFilter(
                            ContextCompat.getColor(activity, R.color.colorWhite));
                    binding.vehicleDetailsView.textViewAutomatic.setTextColor(
                            getResources().getColor(R.color.colorWhite));
                    binding.vehicleDetailsView.linearLayoutManual.setBackgroundResource(
                            R.drawable.dotted_line_box);
                    binding.vehicleDetailsView.imageViewManual.setColorFilter(
                            ContextCompat.getColor(activity,
                                    R.color.colorGrey));
                    binding.vehicleDetailsView.textViewManual.setTextColor(
                            getResources().getColor(R.color.colorGrey));
                } else {
                    binding.vehicleDetailsView.linearLayoutManual.setBackgroundResource(
                            R.drawable.gray_rounded_bg);
                    binding.vehicleDetailsView.imageViewManual.setColorFilter(
                            ContextCompat.getColor(activity,
                                    R.color.colorWhite));
                    binding.vehicleDetailsView.textViewManual.setTextColor(
                            getResources().getColor(R.color.colorWhite));
                    binding.vehicleDetailsView.linearLayoutAutomatic.setBackgroundResource(
                            R.drawable.dotted_line_box);
                    binding.vehicleDetailsView.imageViewAutomatic.setColorFilter(
                            ContextCompat.getColor(activity,
                                    R.color.colorGrey));
                    binding.vehicleDetailsView.textViewAutomatic.setTextColor(
                            getResources().getColor(R.color.colorGrey));
                }

                binding.vehicleDetailsView.textViewColor.setText(newEvaluation.getColor());
                setSpinnerValue(arrayListColor, newEvaluation.getColor(),
                        binding.vehicleDetailsView.textViewColor);

                if (newEvaluation.getFuelType().equalsIgnoreCase("PET")) {
                    binding.vehicleDetailsView.textViewFuelType.setText("Petrol");
                } else if (newEvaluation.getFuelType().equalsIgnoreCase("DIE")) {
                    binding.vehicleDetailsView.textViewFuelType.setText("Diesel");
                } else if (newEvaluation.getFuelType().equalsIgnoreCase("LPG")) {
                    binding.vehicleDetailsView.textViewFuelType.setText("Liquified Petroleum Gas");
                } else if (newEvaluation.getFuelType().equalsIgnoreCase("CNG")) {
                    binding.vehicleDetailsView.textViewFuelType.setText("Compressed Natural Gas");
                } else if (newEvaluation.getFuelType().equalsIgnoreCase("ELC")) {
                    binding.vehicleDetailsView.textViewFuelType.setText("Electric/Battery");
                }

                if (newEvaluation.getEuroVersion().equalsIgnoreCase("Other")) {
                    binding.vehicleDetailsView.textViewEuroVersion.setText("Other");
                } else {
                    binding.vehicleDetailsView.textViewEuroVersion.setText(
                            newEvaluation.getEuroVersion());
                }
                binding.vehicleDetailsView.textViewBodyType.setText(newEvaluation.getBodyType());
                binding.vehicleDetailsView.textViewNoOfSeats.setText(newEvaluation.getNoOfSeats());
                binding.vehicleDetailsView.textViewRCBook.setText(newEvaluation.getRCBook());
                binding.vehicleDetailsView.textViewRCRemarks.setText(newEvaluation.getRcRemarks());
                binding.vehicleDetailsView.textViewAccidentalRemarks.setText(
                        newEvaluation.getAccidentalRemarks());
                binding.vehicleDetailsView.textViewScrapVehicle.setText(
                        newEvaluation.getScrapVehicle());
                binding.vehicleDetailsView.textViewInsurance.setText(newEvaluation.getInsurance());
                binding.vehicleDetailsView.textViewInsuranceDate.setText(
                        newEvaluation.getInsuranceDate());
                binding.vehicleDetailsView.textViewNcb.setText(newEvaluation.getNcb());

                setUpToggleButton(newEvaluation.getServiceBooklet(), btn_idServiceBooklet);
                setUpToggleButton(newEvaluation.getPuc(), btn_idPuc);
                setUpToggleButton(newEvaluation.getHypothecation(), btn_idHypothecation);
                setUpToggleButton(newEvaluation.getNocStatus(), btn_idNocStatus);

                binding.vehicleDetailsView.textViewFinanceCompany.setText(
                        newEvaluation.getFinanceCompany());

                setUpToggleButton(newEvaluation.getIsAccident(), btn_idAccidentalStatus);
                setUpToggleButton(newEvaluation.getTestDrive(), btn_idTestDrive);
                setUpToggleButton(newEvaluation.getFitnessCertificate(), btn_idFitnessCertificate);
                binding.vehicleDetailsView.textViewRoadTax.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapRoadTax,
                                newEvaluation.getRoadTax()));
                binding.vehicleDetailsView.textViewRoadTaxDate.setText(newEvaluation.getRoadTaxExpiryDate());

                //vehicle details
                setSpinnerValue(getResources().getStringArray(R.array.month),
                        newEvaluation.getMonthOfMfg(), binding.vehicleDetailsView.textViewMonthOfMfg);
                setSpinnerValue(getResources().getStringArray(R.array.month),
                        newEvaluation.getMonthOfReg(), binding.vehicleDetailsView.textViewMonthOfReg);
                setSpinnerValue(getResources().getStringArray(R.array.euro_version),
                        newEvaluation.getEuroVersion(), binding.vehicleDetailsView.textViewEuroVersion);
                setSpinnerValue(getResources().getStringArray(R.array.body_type),
                        newEvaluation.getBodyType(), binding.vehicleDetailsView.textViewBodyType);
                setSpinnerValue(getResources().getStringArray(R.array.no_of_seats),
                        newEvaluation.getNoOfSeats(), binding.vehicleDetailsView.textViewNoOfSeats);
                setSpinnerValue(getResources().getStringArray(R.array.rc_book),
                        newEvaluation.getRCBook(), binding.vehicleDetailsView.textViewRCBook);
                SpinnerHelper.setTextValueFromMap(getResources().getStringArray(R.array.rc_remarks),
                        newEvaluation.getRcRemarks(), binding.vehicleDetailsView.textViewRCRemarks,
                        SpinnerConstants.mapRcRemarks);
                SpinnerHelper.setTextValueFromMap(getResources().getStringArray(R.array.accidental_type),
                        newEvaluation.getAccidentalType(), binding.vehicleDetailsView.textViewAccidentalType,
                        SpinnerConstants.mapAccidentalType);
                SpinnerHelper.setTextValueFromMap(getResources().getStringArray(R.array.cng_kit),
                        newEvaluation.getCNGKit(), binding.vehicleDetailsView.textViewCNGKit,
                        SpinnerConstants.mapCNGKit);
                setSpinnerValue(getResources().getStringArray(R.array.scrap_vehicle),
                        newEvaluation.getScrapVehicle(), binding.vehicleDetailsView.textViewScrapVehicle);
                setSpinnerValue(getResources().getStringArray(R.array.insurance),
                        newEvaluation.getInsurance(), binding.vehicleDetailsView.textViewInsurance);
                setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                        newEvaluation.getNoOfKeys(), binding.accessoriesView.textViewNoOfKeys);
                setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                        newEvaluation.getTubelessTyres(), binding.accessoriesView.textViewTubelessTyres);
                binding.vehicleDetailsView.textViewVehicleUsage.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getVehicleUsage(),
                                SpinnerConstants.mapVehicleUsage, MultiSelectDialogHelper.COMMA));

                //Accessories / standard features details
                binding.accessoriesView.textViewNoOfKeys.setText(newEvaluation.getNoOfKeys());
                binding.accessoriesView.textViewComment.setText(newEvaluation.getComment());
                binding.accessoriesView.textViewPowerWindowType.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getPowerWindowType(),
                                SpinnerConstants.mapPowerWindowType, MultiSelectDialogHelper.COMMA));
                if ((newEvaluation.getTubelessTyres().equalsIgnoreCase("Yes")) ||
                        (newEvaluation.getTubelessTyres().equalsIgnoreCase("Y"))) {
                    binding.accessoriesView.textViewTubelessTyres.setText(R.string.yes);
                } else if ((newEvaluation.getTubelessTyres().equalsIgnoreCase("No"))
                        || (newEvaluation.getTubelessTyres().equalsIgnoreCase("N"))) {
                    binding.accessoriesView.textViewTubelessTyres.setText(R.string.no);
                }
                setUpToggleButton(newEvaluation.getStereo(), btn_idStereo);
                setUpToggleButton(newEvaluation.getReverseParking(), btn_idReverseParking);
                setUpToggleButton(newEvaluation.getPowerSteering(), btn_idPowerSteering);
                setUpToggleButton(newEvaluation.getAlloyWheels(), btn_idAlloyWheels);
                setUpToggleButton(newEvaluation.getFogLamp(), btn_idFogLamp);
                setUpToggleButton(newEvaluation.getCentralLocking(), btn_idCentralLocking);
                setUpToggleButton(newEvaluation.getToolKitJack(), btn_idToolKitJack);
                setUpToggleButton(newEvaluation.getRearWiper(), btn_idRearWiper);
                setUpToggleButton(newEvaluation.getFMRadio(), btn_idFMRadio);
                setUpToggleButton(newEvaluation.getMusicSystem(), btn_idMusicSystem);
                setUpToggleButton(newEvaluation.getPowerWindow(), btn_idPowerWindow);
                setUpToggleButton(newEvaluation.getAC(), btn_idAC);
                setUpToggleButton(newEvaluation.getPowerAdjustableSeats(), btn_idPowerAdjustableSeats);
                setUpToggleButton(newEvaluation.getKeyLessEntry(), btn_idKeyLessEntry);
                setUpToggleButton(newEvaluation.getPedalShifter(), btn_idPedalShifter);
                setUpToggleButton(newEvaluation.getElectricallyAdjustableORVM(),
                        btn_idElectricallyAdjustableORVM);
                setUpToggleButton(newEvaluation.getRearDefogger(), btn_idRearDefogger);
                setUpToggleButton(newEvaluation.getSunRoof(), btn_idSunRoof);
                setUpToggleButton(newEvaluation.getAutoClimateTechnologies(), btn_idAutoClimateTechnologies);
                setUpToggleButton(newEvaluation.getLeatherSeats(), btn_idLeatherSeats);
                setUpToggleButton(newEvaluation.getBluetoothAudioSystems(), btn_idBluetoothAudioSystems);
                setUpToggleButton(newEvaluation.getGPSNavigationSystems(), btn_idGPSNavigationSystems);

                //Interior
                setSpinnerValue(getResources().getStringArray(R.array.working_notWorking),
                        newEvaluation.getOdometer(), binding.interiorView.textViewOdometer);
                setSpinnerValue(getResources().getStringArray(R.array.yes_no),
                        newEvaluation.getSeatCover(), binding.interiorView.textViewSeatCover);
                setSpinnerValue(getResources().getStringArray(R.array.good_polishing_requires),
                        newEvaluation.getDashboard(), binding.interiorView.textViewDashboard);
                binding.interiorView.textViewAirBags.setText(newEvaluation.getAirBags());
                binding.interiorView.textViewAllWindowCondition.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapAllWindowCondition,
                                newEvaluation.getAllWindowCondition()));
                binding.interiorView.textViewFrontLHWindow.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFrontRearRLWindow,
                                newEvaluation.getFrontLHWindow()));
                binding.interiorView.textViewFrontRHWindow.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFrontRearRLWindow,
                                newEvaluation.getFrontRHWindow()));
                binding.interiorView.textViewRearLHWindow.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFrontRearRLWindow,
                                newEvaluation.getRearLHWindow()));
                binding.interiorView.textViewRearRHWindow.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFrontRearRLWindow,
                                newEvaluation.getRearRHWindow()));
                binding.interiorView.textViewPlatformPassengerCabin.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin,
                                newEvaluation.getPlatformPassengerCabin()));
                binding.interiorView.textViewPlatformBootSpace.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapPlatformBootSpaceAndPassengerCabin,
                                newEvaluation.getPlatformBootSpace()));

                //Exterior
                binding.exteriorView.textViewFrontFenderRightSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getFrontFenderRightSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewFrontDoorRightSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getFrontDoorRightSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewBackDoorRightSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getBackDoorRightSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewBackFenderRightSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getBackFenderRightSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewBonnet.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getBonnet(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewTailGate.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getTailGate(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewFrontFenderLeftHandSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getFrontFenderLeftHandSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewFrontDoorLeftHandSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getFrontDoorLeftHandSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewBackDoorLeftHandSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getBackDoorLeftHandSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewBackFenderLeftHandSide.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getBackFenderLeftHandSide(),
                                SpinnerConstants.mapCondition, MultiSelectDialogHelper.COMMA));
                setSpinnerValue(getResources().getStringArray(R.array.good_feded_broken),
                        newEvaluation.getFrontLight(),
                        binding.exteriorView.textViewFrontLight);
                setSpinnerValue(getResources().getStringArray(R.array.good_feded_broken),
                        newEvaluation.getBackLight(),
                        binding.exteriorView.textViewBackLight);
                setSpinnerValue(getResources().getStringArray(R.array.good_broken),
                        newEvaluation.getWindShield(),
                        binding.exteriorView.textViewWindShield);
                setSpinnerValue(getResources().getStringArray(R.array.original_repair_damaged),
                        newEvaluation.getPillars(),
                        binding.exteriorView.textViewPillars);
                binding.exteriorView.textViewPillarType.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getPillarType(),
                                SpinnerConstants.mapPillarType, MultiSelectDialogHelper.COMMA));
                setSpinnerValue(getResources().getStringArray(R.array.good_feded_broken),
                        newEvaluation.getMirrors(),
                        binding.exteriorView.textViewMirrors);
                binding.exteriorView.textViewHeadLightLH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapHeadTailLight,
                                newEvaluation.getHeadLightLH()));
                binding.exteriorView.textViewHeadLightRH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapHeadTailLight,
                                newEvaluation.getHeadLightRH()));
                binding.exteriorView.textViewTailLightLH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapHeadTailLight,
                                newEvaluation.getTailLightLH()));
                binding.exteriorView.textViewTailLightRH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapHeadTailLight,
                                newEvaluation.getTailLightRH()));
                binding.exteriorView.textViewDickyDoor.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapDickyDoor,
                                newEvaluation.getDickyDoor()));
                binding.exteriorView.textViewPillarType.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(newEvaluation.getPillarType(),
                                SpinnerConstants.mapPillarType, MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewFrontBumper.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFrontBumper,
                                newEvaluation.getFrontBumper()));
                binding.exteriorView.textViewBackBumper.setText(
                        MultiSelectDialogHelper.getValuesFromKeys(
                                newEvaluation.getBackBumper(), SpinnerConstants.mapCondition,
                                MultiSelectDialogHelper.COMMA));
                binding.exteriorView.textViewOutSideRearViewMirrorLH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapOutsideRearViewMirrors,
                                newEvaluation.getOutsideRearViewMirrorLH()));
                binding.exteriorView.textViewOutSideRearViewMirrorRH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapOutsideRearViewMirrors,
                                newEvaluation.getOutsideRearViewMirrorRH()));
                binding.exteriorView.textViewBodyShell.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapBodyShell,
                                newEvaluation.getBodyShell()));
                binding.exteriorView.textViewChassis.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapChassis,
                                newEvaluation.getChassis()));
                binding.exteriorView.textViewApronFrontLH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapApronFront,
                                newEvaluation.getApronFrontLH()));
                binding.exteriorView.textViewApronFrontRH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapApronFront,
                                newEvaluation.getApronFrontRH()));
                binding.exteriorView.textViewStrutMountingArea.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapStrutMountingArea,
                                newEvaluation.getStrutMountingArea()));
                binding.exteriorView.textViewFirewall.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapFirewall,
                                newEvaluation.getFirewall()));
                binding.exteriorView.textViewUpperCrossMember.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapUpperLowerCrossMember,
                                newEvaluation.getUpperCrossMember()));
                binding.exteriorView.textViewLowerCrossMember.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapUpperLowerCrossMember,
                                newEvaluation.getLowerCrossMember()));
                binding.exteriorView.textViewCowlTop.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapCowlTop,
                                newEvaluation.getCowlTop()));
                binding.exteriorView.textViewRadiator.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapRadiatorSupport,
                                newEvaluation.getRadiatorSupport()));
                binding.exteriorView.textViewEngineRoomCarrierAssembly.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapEngineRoomCarrierAssembly,
                                newEvaluation.getEngineRoomCarrierAssembly()));
                binding.exteriorView.textViewRunningBoardLH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapRunningBoard,
                                newEvaluation.getRunningBoardLH()));
                binding.exteriorView.textViewRunningBoardRH.setText(
                        SpinnerHelper.getValueByCodeFromMap(SpinnerConstants.mapRunningBoard,
                                newEvaluation.getRunningBoardRH()));

                //Wheels and Tyres
                setSpinnerValue(getResources().getStringArray(R.array.wheels),
                        newEvaluation.getWheels(),
                        binding.wheelsTyresView.textViewWheels);
                setSpinnerValueByIndex(getResources().getStringArray(R.array.tyre_all),
                        newEvaluation.getTyreFrontLeft(),
                        binding.wheelsTyresView.textViewTyreFrontLeft);
                setSpinnerValueByIndex(getResources().getStringArray(R.array.tyre_all),
                        newEvaluation.getTyreFrontRight(),
                        binding.wheelsTyresView.textViewTyreFrontRight);
                setSpinnerValueByIndex(getResources().getStringArray(R.array.tyre_all),
                        newEvaluation.getTyreRearLeft(),
                        binding.wheelsTyresView.textViewTyreRearLeft);
                setSpinnerValueByIndex(getResources().getStringArray(R.array.tyre_all),
                        newEvaluation.getTyreRearRight(),
                        binding.wheelsTyresView.textViewTyreRearRight);
                setSpinnerValueByCode(getResources().getStringArray(R.array.spare_tyres),
                        getResources().getStringArray(R.array.spare_tyres_values),
                        newEvaluation.getSpareTyre(),
                        binding.wheelsTyresView.textViewSpareTyre);

                arrayListModel = masterDatabaseHelper.getModelMaster(newEvaluation.getMake());
//COMMENTED BY PRAVIN DHARAM ON 13-12-2019
//                setSpinnerValue(arrayListState, newEvaluation.getState(),
//                        binding.ownerDetailsView.textViewState);

                setSpinnerValue(arrayListMake, newEvaluation.getMake(),
                        binding.vehicleDetailsView.textViewMake);
                setSpinnerValue(arrayListModel, newEvaluation.getModel(),
                        binding.vehicleDetailsView.textViewModel);
                setSpinnerValue(arrayListVariant, newEvaluation.getVariant(),
                        binding.vehicleDetailsView.textViewVariant);
                try {
                    for (int i = 0; i < arrayListCity.size(); i++) {
                        if ((arrayListCity.get(i).getDescription().equalsIgnoreCase(
                                newEvaluation.getCity())) ||
                                (arrayListCity.get(i).getCode().equalsIgnoreCase(
                                        newEvaluation.getCity()))) {
                            binding.ownerDetailsView.textViewCity.setText(
                                    arrayListCity.get(i).getDescription());
                            break;
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                setUpPurposeToggleButton(newEvaluation.getPurpose(), btn_idPurpose);

                //Aggregate Details
                binding.aggregatesView.textViewBody.setText(newEvaluation.getBodyCost());
                binding.aggregatesView.textViewEngine.setText(newEvaluation.getEngineCost());
                binding.aggregatesView.textViewFuelAndIgnitionSystem.setText(
                        newEvaluation.getFuelAndIgnitionSystemCost());
                binding.aggregatesView.textViewTransmission.setText(
                        newEvaluation.getTransmissionCost());
                binding.aggregatesView.textViewBreakSystem.setText(
                        newEvaluation.getBreakSystemCost());
                binding.aggregatesView.textViewSuspensionSystem.setText(
                        newEvaluation.getSuspensionCost());
                binding.aggregatesView.textViewSteeringSystem.setText(
                        newEvaluation.getSteeringSystemCost());
                binding.aggregatesView.textViewACSystem.setText(
                        newEvaluation.getAcCost());
                binding.aggregatesView.textViewElectricalSystem.setText(
                        newEvaluation.getElectricalSystemCost());
                binding.aggregatesView.textViewTotalOfAggregate.setText(
                        newEvaluation.getTotalOfAggregateCost());
                binding.aggregatesView.textViewGeneralServiceCost.setText(
                        newEvaluation.getGeneralServiceCost());
                binding.aggregatesView.textViewTotalRefurbishmentCost.setText(
                        newEvaluation.getTotalRefurbishmentCost());

                binding.aggregatesView.textViewBodyRemark.setText(
                        newEvaluation.getStrBodyRemark());
                binding.aggregatesView.textViewEngineRemark.setText(
                        newEvaluation.getStrEngineRemark());
                binding.aggregatesView.textViewFuelAndIgnitionSystemRemark.setText(
                        newEvaluation.getStrFuelAndIgnitionSystemRemark());
                binding.aggregatesView.textViewTransmissionRemark.setText(
                        newEvaluation.getStrTransmissionRemark());
                binding.aggregatesView.textViewBreakSystemRemark.setText(
                        newEvaluation.getStrBreakSystemRemark());
                binding.aggregatesView.textViewSuspensionSystemRemarks.setText(
                        newEvaluation.getStrSuspensionSystemRemarks());
                binding.aggregatesView.textViewSteeringSystemRemark.setText(
                        newEvaluation.getStrSteeringSystemRemark());
                binding.aggregatesView.textViewACSystemRemark.setText(
                        newEvaluation.getStrACSystemRemark());
                binding.aggregatesView.textViewElectricalSystemRemark.setText(
                        newEvaluation.getStrElectricalSystemRemark());
                binding.aggregatesView.textViewGeneralServiceCostRemark.setText(
                        newEvaluation.getStrGeneralServiceCostRemark());

                //aggregate remarks if other selected
                if (newEvaluation.getStrBodyRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewBodyRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewBodyRemarkOther.setText(
                            newEvaluation.getStrBodyRemarkOther());
                }
                if (newEvaluation.getStrEngineRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewEngineRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewEngineRemarkOther.setText(
                            newEvaluation.getStrEngineRemarkOther());
                }
                if (newEvaluation.getStrFuelAndIgnitionSystemRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewFuelAndIgnitionSystemRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewFuelAndIgnitionSystemRemarkOther.setText(
                            newEvaluation.getStrFuelAndIgnitionSystemRemarkOther());
                }
                if (newEvaluation.getStrTransmissionRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewTransmissionRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewTransmissionRemarkOther.setText(
                            newEvaluation.getStrTransmissionRemarkOther());
                }
                if (newEvaluation.getStrBreakSystemRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewBreakSystemRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewBreakSystemRemarkOther.setText(
                            newEvaluation.getStrBreakSystemRemarkOther());
                }
                if (newEvaluation.getStrSuspensionSystemRemarksOther().isEmpty()) {
                    binding.aggregatesView.textViewSuspensionSystemRemarksOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewSuspensionSystemRemarksOther.setText(
                            newEvaluation.getStrSuspensionSystemRemarksOther());
                }
                if (newEvaluation.getStrSteeringSystemRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewSteeringSystemRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewSteeringSystemRemarkOther.setText(
                            newEvaluation.getStrSteeringSystemRemarkOther());
                }
                if (newEvaluation.getStrACSystemRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewACSystemRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewACSystemRemarkOther.setText(
                            newEvaluation.getStrACSystemRemarkOther());
                }
                if (newEvaluation.getStrElectricalSystemRemarkOther().isEmpty()) {
                    binding.aggregatesView.textViewElectricalSystemRemarkOther.setVisibility(View.GONE);
                } else {
                    binding.aggregatesView.textViewElectricalSystemRemarkOther.setText(
                            newEvaluation.getStrElectricalSystemRemarkOther());
                }

                List<VehiclePhoto> vehiclePhotos = new ArrayList<>();
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.FrontImage),
                        newEvaluation.getPhotoPathFrontImage(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.RearImage),
                        newEvaluation.getPhotoPathRearImage(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.fortyFivedegreeLeftView),
                        newEvaluation.getPhotoPath45DegreeLeftView(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.fortyFiveDegreeRightView),
                        newEvaluation.getPhotoPath45DegreeRightView(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.DashboardView),
                        newEvaluation.getPhotoPathDashboardView(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.OdometerImage),
                        newEvaluation.getPhotoPathOdometerImage(), true));
                if (!isBroker) {
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.ChasisNoImage),
                            newEvaluation.getPhotoPathChassisNoImage(), true));
                }
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.RoofTopImage),
                        newEvaluation.getPhotoPathRoofTopImage(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.InteriorView),
                        newEvaluation.getPhotoPathInteriorView(), true));
                if (!isBroker) {
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.RCCopyImage),
                            newEvaluation.getPhotoPathRCCopyImage(), true));
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.RCCopyImage1),
                            newEvaluation.getPhotoPathRCCopy1(), true));
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.RCCopyImage2),
                            newEvaluation.getPhotoPathRCCopy2(), true));
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.RCCopyImage3),
                            newEvaluation.getPhotoPathRCCopy3(), true));
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.InsuranceImage),
                            newEvaluation.getPhotoPathInsuranceImage(), true));
                }
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.RearLeftTyre),
                        newEvaluation.getPhotoPathRearLeftTyre(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.RearRightTyre),
                        newEvaluation.getPhotoPathRearRightTyre(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.FrontLeftTyre),
                        newEvaluation.getPhotoPathFrontLeftTyre(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.FrontRightTyre),
                        newEvaluation.getPhotoPathFrontRightTyre(), true));
                if (!isBroker) {
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.EngineNoImage),
                            newEvaluation.getPhotoPathEngineNoImage(), true));
                }
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.DamagedPartImageOne),
                        newEvaluation.getPhotoPathDamagedPartImage1(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.DamagedPartImageTwo),
                        newEvaluation.getPhotoPathDamagedPartImage2(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.DamagedPartImageThree),
                        newEvaluation.getPhotoPathDamagedPartImage3(), true));
                vehiclePhotos.add(new VehiclePhoto(getString(R.string.DamagedPartImageFour),
                        newEvaluation.getPhotoPathDamagedPartImage4(), true));

                int itemsToBeAdded = 3 - vehiclePhotos.size() % 3;
                for (int k = 0; k < itemsToBeAdded; k++) {
                    vehiclePhotos.add(new VehiclePhoto(getString(R.string.DamagedPartImageFive),
                            newEvaluation.getPhotoPathRoofTopImage(), false));
                }
                for (int i = 0; i < vehiclePhotos.size(); i += 3) {
                    LinearLayout linearLayout = new LinearLayout(activity);
                    linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    linearLayout.setPadding(0, 10, 0, 0);
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                    for (int j = i; j < i + 3; j++) {
                        LayoutInflater inflater = LayoutInflater.from(activity);
                        View itemView = inflater.inflate(R.layout.list_item_vehicle_photos_view,
                                linearLayout, false);

                        TextView tvVehiclePhotoName = itemView.findViewById(R.id.tv_vehicle_photo_name);
                        final ProgressBar progressBar = itemView.findViewById(R.id.progress_bar);
                        final ImageView ivVehiclePhoto = itemView.findViewById(R.id.iv_vehicle_photo);

                        tvVehiclePhotoName.setText(vehiclePhotos.get(j).getNameOfImage());
                        if (!vehiclePhotos.get(j).getPath().equalsIgnoreCase("")) {
                            itemView.setTag(vehiclePhotos.get(j).getPath());
                            if (imagesAreOnline) {
                                Glide.with(this)
                                        .load(CommonHelper.getAuthenticatedUrlForGlide(
                                                vehiclePhotos.get(j).getPath()))
                                        .apply(RequestOptions.placeholderOf(R.mipmap.ic_image_not_found)
                                                .error(R.mipmap.ic_image_not_found))
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e,
                                                                        Object model,
                                                                        Target<Drawable> target,
                                                                        boolean isFirstResource) {
                                                progressBar.setVisibility(View.GONE);
                                                ivVehiclePhoto.setImageDrawable(getResources()
                                                        .getDrawable(R.mipmap.ic_image_not_found));
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource,
                                                                           Object model,
                                                                           Target<Drawable> target,
                                                                           DataSource dataSource,
                                                                           boolean isFirstResource) {
                                                progressBar.setVisibility(View.GONE);
                                                return false;
                                            }
                                        })
                                        .into(ivVehiclePhoto);
                            } else {
                                progressBar.setVisibility(View.GONE);
                                Glide.with(activity)
                                        .load(Uri.fromFile(new File(vehiclePhotos.get(j).getPath())))
                                        .apply(new RequestOptions()
                                                .placeholder(R.mipmap.ic_image_not_found))
                                        .into(ivVehiclePhoto);
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                        if (!vehiclePhotos.get(j).isDisplayImage()) {
                            itemView.setVisibility(View.INVISIBLE);
                        }
                        itemView.setTag(vehiclePhotos.get(j).getPath());
                        itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showImageDialog(view);
                            }
                        });
                        linearLayout.addView(itemView);
                    }
                    binding.linearLayoutVehiclePhotos.addView(linearLayout);
                }
            } catch (NullPointerException e) {
                Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private void setOccupationTextViewValueByCode(String[] stringArray, String strOccupation,
                                                  TextView spinnerOccupation) {
        try {
            for (int i = 0; i < stringArray.length; i++) {
                if (stringArray[i].equalsIgnoreCase(strOccupation)) {
                    spinnerOccupation.setText(getResources().getStringArray(R.array.occupation)[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setSpinnerValue(ArrayList<MasterModel> list, String value, TextView textView) {
        try {
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i).getDescription().equalsIgnoreCase(value)) ||
                        (list.get(i).getCode().equalsIgnoreCase(value))) {
                    textView.setText(list.get(i).getDescription());
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    void setSpinnerValue(String[] list, String value, TextView textView) {
        try {
            for (int i = 0; i < list.length; i++) {
                if ((list[i].equalsIgnoreCase(value)) || (String.valueOf(i).equalsIgnoreCase(value))
                        || (list[i].substring(0, 1).equalsIgnoreCase(value))) {
                    textView.setText(list[i]);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    void setSpinnerValueByIndex(String[] list, String indexStr, TextView textView) {
        try {
            if (null != indexStr) {
                int index = Integer.parseInt(indexStr);
                textView.setText(list[index]);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    void setSpinnerValueByCode(String[] list, String[] listCode, String value, TextView textView) {
        try {
            for (int i = 0; i < listCode.length; i++) {
                if ((listCode[i].equalsIgnoreCase(value)) || (String.valueOf(i).equalsIgnoreCase(value))) {
                    textView.setText(list[i]);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tableRowVehicleEvaluationDetails:
                if (binding.linearLayoutVehicleEvaluationDetails.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutVehicleEvaluationDetails.setVisibility(View.GONE);
                    binding.imageViewVehicleEvaluation.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutVehicleEvaluationDetails.setVisibility(View.VISIBLE);
                    binding.imageViewVehicleEvaluation.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowOwnersDetails:
                if (binding.linearLayoutCustomerDetails.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutCustomerDetails.setVisibility(View.GONE);
                    binding.imageViewOwner.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutCustomerDetails.setVisibility(View.VISIBLE);
                    binding.imageViewOwner.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowVehicleDetails:
                if (binding.linearLayoutVehicleDetails.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutVehicleDetails.setVisibility(View.GONE);
                    binding.imageViewVehicle.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutVehicleDetails.setVisibility(View.VISIBLE);
                    binding.imageViewVehicle.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowAddPhoto:
                if (binding.linearLayoutVehiclePhotos.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutVehiclePhotos.setVisibility(View.GONE);
                    binding.imageViewAddPhoto.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutVehiclePhotos.setVisibility(View.VISIBLE);
                    binding.imageViewAddPhoto.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowAggregateDetails:
                if (binding.linearLayoutAggregateDetails.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutAggregateDetails.setVisibility(View.GONE);
                    binding.imageViewAggregateDetails.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutAggregateDetails.setVisibility(View.VISIBLE);
                    binding.imageViewAggregateDetails.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableAccessoriesStandardFeaturesDetails:
                if (binding.linearLayoutStandardFeaturesDetails.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutStandardFeaturesDetails.setVisibility(View.GONE);
                    binding.imageViewStandardFeatureDetails.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutStandardFeaturesDetails.setVisibility(View.VISIBLE);
                    binding.imageViewStandardFeatureDetails.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowInterior:
                if (binding.linearLayoutInterior.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutInterior.setVisibility(View.GONE);
                    binding.imageViewInterior.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutInterior.setVisibility(View.VISIBLE);
                    binding.imageViewInterior.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowExterior:
                if (binding.linearLayoutExterior.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutExterior.setVisibility(View.GONE);
                    binding.imageViewExterior.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutExterior.setVisibility(View.VISIBLE);
                    binding.imageViewExterior.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowWheelsAndTyres:
                if (binding.linearLayoutWheelsAndTyres.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutWheelsAndTyres.setVisibility(View.GONE);
                    binding.imageViewWheelsAndTyres.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutWheelsAndTyres.setVisibility(View.VISIBLE);
                    binding.imageViewWheelsAndTyres.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.tableRowViewAudio:
                if (binding.linearLayoutViewAudio.getVisibility() == View.VISIBLE) {
                    binding.linearLayoutViewAudio.setVisibility(View.GONE);
                    binding.imageViewAudio.setImageResource(R.mipmap.ic_plus);
                } else {
                    setUpDefaults();
                    binding.linearLayoutViewAudio.setVisibility(View.VISIBLE);
                    binding.imageViewAudio.setImageResource(R.mipmap.ic_minus);
                }
                break;
            case R.id.playViewBT:
                if (!isPlaying && !mMediaPlayer.isPlaying()) {
                    binding.playViewBT.setVisibility(View.GONE);
                    binding.audioPB.setVisibility(View.VISIBLE);
                    binding.playTV.setText(R.string.audio_loading);
                    Uri uri = Uri.parse(newEvaluation.getAudioPathVehicleNoise());
                    Map<String, String> headers = new HashMap<>();
                    headers.put("authCd", audioAuthCode);
                    headers.put("Content-Type", audioContentType);
                    // Use java reflection call the hide API:
                    Method method = null;
                    try {
                        method = mMediaPlayer.getClass().
                                getMethod("setDataSource", new Class[]{Context.class, Uri.class, Map.class});
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (method != null) {
                            method.invoke(mMediaPlayer, new Object[]{getActivity(), uri, headers});
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mMediaPlayer.prepareAsync();
                    mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            binding.playViewBT.setImageResource(R.drawable.ic_stop);
                            binding.playViewBT.setVisibility(View.VISIBLE);
                            binding.audioPB.setVisibility(View.GONE);
                            binding.playTV.setText(R.string.stop_playing);
                            mp.start();
                            isPlaying = true;
                        }
                    });
                    mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            binding.playViewBT.setImageResource(R.drawable.ic_play);
                            binding.playTV.setText(R.string.start_playing);
                            mp.reset();
                            isPlaying = false;
                        }
                    });
                } else if (mMediaPlayer.isPlaying() && isPlaying) {
                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    binding.playViewBT.setImageResource(R.drawable.ic_play);
                    binding.playTV.setText(R.string.start_playing);
                    isPlaying = false;
                }
        }
    }

    void setUpDefaults() {
        binding.linearLayoutAggregateDetails.setVisibility(View.GONE);
        binding.linearLayoutVehicleEvaluationDetails.setVisibility(View.GONE);
        binding.linearLayoutCustomerDetails.setVisibility(View.GONE);
        binding.linearLayoutVehicleDetails.setVisibility(View.GONE);
        binding.linearLayoutVehiclePhotos.setVisibility(View.GONE);
        binding.linearLayoutStandardFeaturesDetails.setVisibility(View.GONE);
        binding.linearLayoutInterior.setVisibility(View.GONE);
        binding.linearLayoutExterior.setVisibility(View.GONE);
        binding.linearLayoutWheelsAndTyres.setVisibility(View.GONE);

        binding.imageViewAggregateDetails.setImageResource(R.mipmap.ic_plus);
        binding.imageViewVehicleEvaluation.setImageResource(R.mipmap.ic_plus);
        binding.imageViewOwner.setImageResource(R.mipmap.ic_plus);
        binding.imageViewVehicle.setImageResource(R.mipmap.ic_plus);
        binding.imageViewAddPhoto.setImageResource(R.mipmap.ic_plus);
        binding.imageViewStandardFeatureDetails.setImageResource(R.mipmap.ic_plus);
        binding.imageViewInterior.setImageResource(R.mipmap.ic_plus);
        binding.imageViewExterior.setImageResource(R.mipmap.ic_plus);
        binding.imageViewWheelsAndTyres.setImageResource(R.mipmap.ic_plus);
    }

    private void setUpToggleButton(String value, int[] btnIds) {
        int highlightPos = 1, dimPos = 0;
        if ((value.equalsIgnoreCase("No")) ||
                (value.equalsIgnoreCase("N"))) {
            highlightPos = 0;
            dimPos = 1;
        }

        ((Button) binding.getRoot().findViewById(btnIds[highlightPos])).setTextColor(getResources().getColor(R.color.colorblack));
        (binding.getRoot().findViewById(btnIds[highlightPos])).setBackgroundResource(R.drawable.disable_edit_text_border);

        ((Button) binding.getRoot().findViewById(btnIds[dimPos])).setTextColor(getResources().getColor(R.color.colorWhite));
        (binding.getRoot().
                findViewById(btnIds[dimPos])).setBackgroundResource(R.drawable.gray_rounded_bg);
    }

    private void setUpPurposeToggleButton(String value, int[] btnIds) {
        try {
            int highlightPos = -1, dimPos = 0;
            if (value.equalsIgnoreCase("Direct buy")) {
                highlightPos = 0;
                dimPos = 1;
            } else if (value.equalsIgnoreCase("Exchange") || value.isEmpty()) {
                highlightPos = 1;
                dimPos = 0;
            }

            if (highlightPos != -1) {
                ((Button) binding.getRoot().findViewById(btnIds[highlightPos]))
                        .setTextColor(getResources().getColor(R.color.colorblack));
                (binding.getRoot().findViewById(btnIds[highlightPos]))
                        .setBackgroundResource(R.drawable.disable_edit_text_border);

                ((Button) binding.getRoot().findViewById(btnIds[dimPos]))
                        .setTextColor(getResources().getColor(R.color.colorWhite));
                (binding.getRoot().findViewById(btnIds[dimPos]))
                        .setBackgroundResource(R.drawable.gray_rounded_bg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void showImageDialog(View view) {
        try {
            String imageUrl = (String) view.getTag();
            if (null != imageUrl && !imageUrl.isEmpty()) {
                LayoutInflater factory = LayoutInflater.from(activity);
                final View dialogView = factory.inflate(R.layout.dialog_view_image, null);
                final android.app.AlertDialog alertDialog =
                        new android.app.AlertDialog.Builder(activity,
                                R.style.ThemeViewImageDialog).create();

                alertDialog.setView(dialogView);
                final TouchImageView ivImage = dialogView.findViewById(R.id.iv_image);
                ivImage.setMaxZoom(4f);
                final ImageView ivClose = dialogView.findViewById(R.id.iv_close);
                final ProgressBar progressFrontImage = dialogView.findViewById(R.id.progressFrontImage);
                progressFrontImage.setVisibility(View.GONE);
                if (imagesAreOnline) {
                    Glide.with(this)
                            .load(CommonHelper.getAuthenticatedUrlForGlide(imageUrl))
                            .apply(RequestOptions.placeholderOf(R.mipmap.ic_image_not_found)
                                    .error(R.mipmap.ic_image_not_found))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e,
                                                            Object model,
                                                            Target<Drawable> target,
                                                            boolean isFirstResource) {
                                    ivImage.setImageDrawable(getResources()
                                            .getDrawable(R.mipmap.ic_image_not_found));
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource,
                                                               Object model,
                                                               Target<Drawable> target,
                                                               DataSource dataSource,
                                                               boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .into(ivImage);
                } else {
                    Glide.with(this)
                            .load(Uri.fromFile(new File(imageUrl)))
                            .apply(RequestOptions.placeholderOf(R.mipmap.ic_image_not_found)
                                    .error(R.mipmap.ic_image_not_found))
                            .into(ivImage);
                }
                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                Window window = alertDialog.getWindow();
                assert window != null;
                WindowManager.LayoutParams wlp = window.getAttributes();
                window.setAttributes(wlp);
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}