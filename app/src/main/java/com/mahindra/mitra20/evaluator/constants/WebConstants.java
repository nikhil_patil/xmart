package com.mahindra.mitra20.evaluator.constants;

import com.mahindra.mitra20.constants.GeneralConstants;

/**
 * Created by user on 8/11/2018.
 */

public interface WebConstants {

    String BASE_URL = GeneralConstants.BASE_URL;

    interface ScheduleAppoinmentList {

        int REQUEST_CODE = 1;
        int REQUEST_CODE_PCS = 2;
        String URL = BASE_URL + "xmart/EnquiryRequestList";
        String USER_ID = "UserID";
        String REQUEST_STATUS = "RequestStatus";
        String EVALUATION_REPORT_ID = "EvaluationReportID";
        String COUNT_TO_DISPLAY_DASHBOARD = "CountToDisplayOnDashboard";
        String PINCODE = "Pincode";
    }

    interface appointmentConfirm {
        String URL = BASE_URL + "xmart/ConfirmAppointment";
        String USER_ID = "UserID";
        String REQUEST_ID = "RequestID";//1.Confirmation, 2.Reschedule, 3.Cancel
        String EVALUATIOR_ID = "EvaluatorID";
        String APPOINTMENT_STATUS = "appointmentStatus";
        String APPOINTMENT_DATE_TIME = "appointmentDateTime";
        String CANCELLATION_REASON_ID = "cancellationReasonID";
    }

    interface masterSync {
        int REQUEST_CODE_COLOR_MST = 1;
//        int REQUEST_CODE_STATE_MST = 2;
//        int REQUEST_CODE_CITY_MST = 3;
        int REQUEST_CODE_VARIANT_MST = 4;
        int REQUEST_CODE_MAKE_MST = 5;
        int REQUEST_CODE_MODEL_MST = 7;
        int REQUEST_CODE_VEHCL_SEGMNT_MST = 6;
        int REQUEST_CODE_PINCODE_STATE_DIST_TEHSIL =77;

        String URL = BASE_URL + "BOPIntegration/GetSmallMasterData";
        String URL_PINCODE = BASE_URL + "BOPIntegration/GetPincodeDetails";
        String USER_ID = "UserID";
        String RequestType = "RequestType";
        String ImeiNo = "ImeiNo";
        String LastUpdatedDate = "LastUpdatedDate";

        //ADDED BY PRAVIN DHARAM ON 14-12-2019
        String HEADER_DSCIMEI = "DSCIMEI";
        String FIELD_IMEI_NUMBER = "IMEI number";
        String FIELD_DATE = "Date";

    }

    interface submitEvaluationReport {

        int REQUEST_CODE = 1;
        String URL = BASE_URL + "xmart/SubmitEvaluationReport";
        String USER_ID = "UserID";
        String REQUEST_STATUS = "RequestStatus";
        String EVALUATION_REPORT_ID = "EvaluationReportID";
        String COUNT_TO_DISPLAY_DASHBOARD = "CountToDisplayOnDashboard";
        String PINCODE = "Pincode";
    }

    interface uploadEvaluationDocument {
        String URL = BASE_URL + "xmart/uploadDocument";
        String REQUEST_CODE = "RequestCode";
        String DOC_TYPE = "docType";
        String USER_ID = "userId";
        String USER_TYPE = "userType";
        String EVAL_ID = "evalId";
        String FILE = "file";
    }

    interface dmsMakeMaster {
        String URL = BASE_URL + "BOPIntegration/GetAllExchangeMaster";
        int REQUEST_CODE = 8;

        String IMEI_NO = "ImeiNo";
        String REQUEST_TYPE = "RequestType";
        String LAST_UPDATED_DATE = "LastUpdatedDate";
    }

    interface ServiceHistory {
        int REQUEST_CODE = 9;
        String URL = BASE_URL + "WYH/ROHistory";
        String CHASIS_NUMBER = "chassisNumber";
        String REG_NO = "regNumber";
    }

    interface EvaluatorDetails {
        int REQUEST_CODE = 14;
        String URL = BASE_URL + "xmart/fetchEvaluatorDetails";
    }
}