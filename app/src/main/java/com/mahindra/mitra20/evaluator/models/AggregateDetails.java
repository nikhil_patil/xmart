package com.mahindra.mitra20.evaluator.models;

import com.google.gson.annotations.SerializedName;

public class AggregateDetails {

    private String AggregateCode;

    private String AggregateDesc;

    private String AggregateSrlNo;

    private String Remarks;

    private String SubsystemRefurbishmentCost;

    private String RemarksOthers;


    public AggregateDetails(String aggregateCode, String aggregateDesc, String aggregateSrlNo,
                            String remarks, String subsystemRefurbishmentCost, String other) {
        AggregateCode = aggregateCode;
        AggregateDesc = aggregateDesc;
        AggregateSrlNo = aggregateSrlNo;
        Remarks = remarks;
        SubsystemRefurbishmentCost = subsystemRefurbishmentCost;
        RemarksOthers = other;
    }

    public String getAggregateCode() {
        return AggregateCode;
    }

    public String getRemarks() {
        return Remarks;
    }

    public String getSubsystemRefurbishmentCost() {
        return SubsystemRefurbishmentCost;
    }

    public String getRemarksOthers() {
        return RemarksOthers;
    }
}