package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.databinding.LayoutWheelsAndTyresBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.WheelsAndTyresPresenter;
import com.mahindra.mitra20.interfaces.CommonMethods;

import static com.mahindra.mitra20.evaluator.views.activity.NewEvaluationActivity.randomNumGen;

public class WheelsAndTyresFragment extends Fragment implements CommonMethods {

    LayoutWheelsAndTyresBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private WheelsAndTyresPresenter wheelsAndTyresPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = LayoutWheelsAndTyresBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        wheelsAndTyresPresenter = new WheelsAndTyresPresenter(context);
        String evaluationId;

        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = wheelsAndTyresPresenter.getEvaluationData(evaluationId);
        }

        init();
        displayData();
    }

    @Override
    public void init() {

    }

    @Override
    public void displayData() {
        SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.wheels),
                newEvaluation.getWheels(), binding.spinnerWheels);
        try {
            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.tyre_new),
                    String.valueOf(Integer.parseInt(newEvaluation.getTyreFrontLeft()) - 3),
                    binding.spinnerTyreFrontLeft);
            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.tyre_new),
                    String.valueOf(Integer.parseInt(newEvaluation.getTyreFrontRight()) - 3),
                    binding.spinnerTyreFrontRight);
            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.tyre_new),
                    String.valueOf(Integer.parseInt(newEvaluation.getTyreRearLeft()) - 3),
                    binding.spinnerTyreRearLeft);
            SpinnerHelper.setSpinnerValue(getResources().getStringArray(R.array.tyre_new),
                    String.valueOf(Integer.parseInt(newEvaluation.getTyreRearRight()) - 3),
                    binding.spinnerTyreRearRight);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        SpinnerHelper.setSpinnerValueByCode(
                getResources().getStringArray(R.array.spare_tyres_values),
                newEvaluation.getSpareTyre(), binding.spinnerSpareTyre);
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setWheels(String.valueOf(
                    binding.spinnerWheels.getSelectedItemPosition()));
            newEvaluation.setTyreFrontLeft(String.valueOf(
                    binding.spinnerTyreFrontLeft.getSelectedItemPosition() + 3));
            newEvaluation.setTyreFrontRight(String.valueOf(
                    binding.spinnerTyreFrontRight.getSelectedItemPosition() + 3));
            newEvaluation.setTyreRearLeft(String.valueOf(
                    binding.spinnerTyreRearLeft.getSelectedItemPosition() + 3));
            newEvaluation.setTyreRearRight(String.valueOf(
                    binding.spinnerTyreRearRight.getSelectedItemPosition() + 3));
            newEvaluation.setSpareTyre(SpinnerHelper.getSpinnerValue(
                    binding.spinnerSpareTyre,
                    getResources().getStringArray(R.array.spare_tyres_values)));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        wheelsAndTyresPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        if (binding.spinnerWheels.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerWheels,
                    "Select wheels type");
        }
        if (binding.spinnerTyreFrontLeft.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTyreFrontLeft,
                    "Select tyre front left status");
        }
        if (binding.spinnerTyreFrontRight.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTyreFrontRight,
                    "Select tyre front right status");
        }
        if (binding.spinnerTyreRearLeft.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTyreRearLeft,
                    "Select tyre rear left status");
        }
        if (binding.spinnerTyreRearRight.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerTyreRearRight,
                    "Select tyre rear right status");
        }
        if (binding.spinnerSpareTyre.getSelectedItemPosition() == 0) {
            return FieldValidator.openViewAndDisplayMessage(context, binding.spinnerSpareTyre,
                    "Select spare tyre status");
        }
        saveData();
        return true;
    }

    @Override
    public void fillTestDataInForm() {
        binding.spinnerWheels.setSelection(randomNumGen(2, 1));
        binding.spinnerTyreFrontLeft.setSelection(randomNumGen(5, 1));
        binding.spinnerTyreFrontRight.setSelection(randomNumGen(5, 1));
        binding.spinnerTyreRearLeft.setSelection(randomNumGen(5, 1));
        binding.spinnerTyreRearRight.setSelection(randomNumGen(5, 1));
        binding.spinnerSpareTyre.setSelection(randomNumGen(4, 1));
    }
}