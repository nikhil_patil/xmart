package com.mahindra.mitra20.evaluator.interfaces;

import android.view.View;

/**
 * Created by user on 10/3/2017.
 */

public interface ItemClickListener {
    void onClick(View view, int position);
}
