package com.mahindra.mitra20.evaluator.presenter;

import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;

import java.util.ArrayList;

/**
 * Created by BADHAP-CONT on 8/12/2018.
 */

public interface AssignedRequestListIn {
    public void onDownloadSuccess(int requestId, ArrayList<ScheduleAppoinment> scheduleAppoinments);

    public void onDownloadFail(int requestId, String message);
}
