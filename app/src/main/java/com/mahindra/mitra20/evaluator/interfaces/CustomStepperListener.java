package com.mahindra.mitra20.evaluator.interfaces;

public interface CustomStepperListener {
    void activeStateChange(int position, String title);
}
