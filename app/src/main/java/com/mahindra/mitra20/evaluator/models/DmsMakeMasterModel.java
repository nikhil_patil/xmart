package com.mahindra.mitra20.evaluator.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BADHAP-CONT on 9/9/2018.
 */

public class DmsMakeMasterModel implements Parcelable{

          String Make;
          String MakeCode;
          String ModelCode;
          String ModelDesc;
          String sortOrder;

    public DmsMakeMasterModel() {
    }

    protected DmsMakeMasterModel(Parcel in) {
        Make = in.readString();
        MakeCode = in.readString();
        ModelCode = in.readString();
        ModelDesc = in.readString();
        sortOrder = in.readString();
    }

    public static final Creator<DmsMakeMasterModel> CREATOR = new Creator<DmsMakeMasterModel>() {
        @Override
        public DmsMakeMasterModel createFromParcel(Parcel in) {
            return new DmsMakeMasterModel(in);
        }

        @Override
        public DmsMakeMasterModel[] newArray(int size) {
            return new DmsMakeMasterModel[size];
        }
    };

    public String getMake() {
        return Make;
    }

    public void setMake(String make) {
        Make = make;
    }

    public String getMakeCode() {
        return MakeCode;
    }

    public void setMakeCode(String makeCode) {
        MakeCode = makeCode;
    }

    public String getModelCode() {
        return ModelCode;
    }

    public void setModelCode(String modelCode) {
        ModelCode = modelCode;
    }

    public String getModelDesc() {
        return ModelDesc;
    }

    public void setModelDesc(String modelDesc) {
        ModelDesc = modelDesc;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Make);
        dest.writeString(MakeCode);
        dest.writeString(ModelCode);
        dest.writeString(ModelDesc);
        dest.writeString(sortOrder);
    }
}
