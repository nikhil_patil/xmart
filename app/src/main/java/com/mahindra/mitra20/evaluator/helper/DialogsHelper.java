package com.mahindra.mitra20.evaluator.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.interfaces.OnClickDialog;
import com.mahindra.mitra20.evaluator.views.activity.AppointmentConfirmedActivity;
import com.mahindra.mitra20.evaluator.views.activity.EvaluatorDashboardActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by PURODH-CONT on 10/13/2017.
 */

public class DialogsHelper implements VolleySingleTon.VolleyInteractor {

    EvaluatorHelper evaluatorHelper;
    OnClickDialog onClickDialog;
    Context context;
    String reason = "", requestId1 = "", strNotes1 = "";
    String evaluatorId = "";
    android.app.AlertDialog alertDialog;
    private ProgressDialog dialog;

    public OnClickDialog getOnClickDialog() {
        return onClickDialog;
    }

    public void setOnClickDialog(OnClickDialog onClickDialog) {
        this.onClickDialog = onClickDialog;
    }

    public DialogsHelper(Context _context) {
        context = _context;
        evaluatorHelper = new EvaluatorHelper(context);
        dialog = new ProgressDialog((Activity) context);
        dialog.setCancelable(false);
        dialog.setMessage("Wait while loading");
    }

    public void showCancelAppointmentDialog(int id, final String requestId, String strNotes) {
        try {
            strNotes1 = strNotes;
            requestId1 = requestId;
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(context);
            View promptsView = li.inflate(R.layout.activity_cancel_appointment, null);

            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);

            final ImageView imageViewClose = promptsView.findViewById(R.id.imageViewClose);
            final Spinner spinnerReason = promptsView.findViewById(R.id.spinnerReason);
            final Button buttonCancelAppointmentPopup = promptsView.findViewById(R.id.buttonCancelAppointmentPopup);

            alertDialogBuilder.setView(promptsView);

            // create alert dialog
            alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();


            imageViewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();

                    if (onClickDialog != null) {
                        onClickDialog.onCancel();
                    }
                }
            });

            buttonCancelAppointmentPopup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //if (onClickDialog != null) {

                    reason = spinnerReason.getSelectedItem().toString();
                    String reasonId = String.valueOf(spinnerReason.getSelectedItemPosition());

                    if (spinnerReason.getSelectedItemPosition() == 0) {
                        CommonHelper.toast("Please select reason for cancellation", context);
                    } else {
                        String userId1 = SessionUserDetails.getInstance().getUserID();
                        if (userId1.contains(".")) {
                            String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                            evaluatorId = arrUserId[0].toUpperCase();
                        } else {
                            evaluatorId = userId1;
                        }
                        try {

                            dialog.show();

                            HashMap<String, String> params = new HashMap<>();
                            params.put(WebConstants.appointmentConfirm.REQUEST_ID, requestId1); //"ENQ19A000142"
                            params.put(WebConstants.appointmentConfirm.EVALUATIOR_ID, evaluatorId);// "MV010107");//evaluatorId
                            params.put(WebConstants.appointmentConfirm.APPOINTMENT_STATUS, "CAN");// "CNF");//appointmentStatus
                            params.put(WebConstants.appointmentConfirm.APPOINTMENT_DATE_TIME, "");//"06/08/2018 12:00:00");//appointmentDateTime
                            params.put(WebConstants.appointmentConfirm.CANCELLATION_REASON_ID, reasonId);//"06/08/2018 12:00:00");//appointmentDateTime
                            JSONObject jsonObject = new JSONObject(params);
                            VolleySingleTon.getInstance().connectToPostUrl(context, DialogsHelper.this, WebConstants.appointmentConfirm.URL, params, 3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    // onClickDialog.onSuccess(reason,RequestId,evaluatorId);
                    // }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {

        dialog.hide();

        switch (requestId) {
            case 1:
                CommonHelper.toast(R.string.confirmation_success, context);
                context.startActivity(new Intent(context, AppointmentConfirmedActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                ((Activity) context).finish();
                break;
            case 2:
                CommonHelper.toast(R.string.reschedule_appointment_successfully, context);
                context.startActivity(new Intent(context, EvaluatorDashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                ((Activity) context).finish();
                break;
            case 3:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    if (IsSuccessful.equals("1")) {
                        CommonHelper.toast(R.string.cancel_appointment_successfully, context);

                        evaluatorHelper.cancelAppointment(reason, requestId1, strNotes1);
                        alertDialog.dismiss();

                        if (SessionUserDetails.getInstance().getUserType() == 0) {
                            context.startActivity(new Intent(context, ChampionDashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            ((Activity) context).finish();
                        } else if (SessionUserDetails.getInstance().getUserType() == 1) {
                            context.startActivity(new Intent(context, EvaluatorDashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            ((Activity) context).finish();
                        }
                    } else {
                        CommonHelper.toast(R.string.cancelled_failure, context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonHelper.toast(R.string.cancelled_failure, context);
                }
                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {

        dialog.hide();

        switch (requestId) {
            case 3:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    alertDialog.dismiss();
                    CommonHelper.toast(message, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void callDialog(final String strMobileNo) {
        try {
            new AlertDialog.Builder(context).setIcon(R.mipmap.ic_launcher).setTitle("Call")
                    .setMessage(context.getResources().getString(R.string.make_call_confirmation))
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CommonHelper.makeCall(strMobileNo, context);
                            /*Intent callIntent = new Intent(Intent.ACTION_CALL);
                            // if (callIntent.resolveActivity(getPackageManager()) != null) {
                            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callIntent.setData(Uri.parse("tel:"+strMobileNo));
                            callIntent.setPackage("com.android.server.telecom");
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            context.startActivity(callIntent);*/
                        }
                    }).setNegativeButton("No", null).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}