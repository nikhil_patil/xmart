package com.mahindra.mitra20.evaluator.presenter.evalform;

import android.content.Context;

import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

/**
 * Created by PATINIK-CONT on 30/12/2019.
 */

public class AggregateDetailsPresenter {

    private Context context;

    public AggregateDetailsPresenter(Context context) {
        this.context = context;
    }

    public void saveData(NewEvaluation newEvaluation) {
        new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                EvaluatorConstants.EvaluationParts.AGGREGATE_DETAILS);
    }

    public NewEvaluation getEvaluationData(String evaluationId) {
        return new NewEvaluationHelper(context).getEvaluationDetails(evaluationId,
                EvaluatorConstants.EvaluationParts.AGGREGATE_DETAILS);
    }
}