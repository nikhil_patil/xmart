package com.mahindra.mitra20.evaluator.helper;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by BADHAP-CONT on 8/13/2018.
 */

public class TimePickerHelper implements View.OnFocusChangeListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    private TextView _textView, _textViewInvisible;
    private TableRow _tableRow;
    private Calendar mCalendar;
    private SimpleDateFormat mFormat;
    private SimpleDateFormat mFormat1;
    private Context context;
    DateHelper dateHelper;
/*
    public TimePickerHelper(Context context, int textViewID){
        Activity act = (Activity)context;
        this._textView = (TextView)act.findViewById(textViewID);
        _textView.setOnFocusChangeListener(this);
        _textView.setOnClickListener(this);
    }*/

    public TimePickerHelper(Context context, TextView textView, TextView textViewInvisible) {
        Activity act = (Activity)context;
        this._textView = textView;
        this._textViewInvisible = textViewInvisible;
        _textView.setOnFocusChangeListener(this);
        _textViewInvisible.setOnClickListener(this);
        dateHelper = new DateHelper();
        this.context = context;
    }

    public TimePickerHelper(Context context, int textViewID, int tableRow, int textViewInvisible) {
        try {
        Activity act = (Activity)context;
            this.context = context;
            this._textView = (TextView)act.findViewById(textViewID);
        this._textViewInvisible = (TextView) act.findViewById(textViewInvisible);
            this._tableRow = (TableRow) act.findViewById(tableRow);
        //_tableRow.setOnFocusChangeListener(this);
        _tableRow.setOnClickListener(this);
        _textView.setOnClickListener(this);

            showPicker(context);
        //onClick(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TimePickerHelper(Context context, TextView textViewID, TableRow tableRow, TextView textViewInvisible) {
        try {
            Activity act = (Activity) context;
            this._textView = textViewID;
            this._textViewInvisible = textViewInvisible;
            this._tableRow = tableRow;
            //_tableRow.setOnFocusChangeListener(this);
            _tableRow.setOnClickListener(this);
            _textView.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus){
            showPicker(view);
        }
    }

    @Override
    public void onClick(View view) {
        showPicker(view);
    }

    private void showPicker(View view) {
        if (mCalendar == null)
            mCalendar = Calendar.getInstance();

        int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = mCalendar.get(Calendar.MINUTE);

        new TimePickerDialog(view.getContext(), this, hour, minute, false).show();
    }

    private void showPicker(Context context) {
        if (mCalendar == null)
            mCalendar = Calendar.getInstance();

        int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = mCalendar.get(Calendar.MINUTE);

        new TimePickerDialog(context, this, hour, minute, false).show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);

        if (mFormat == null)
            mFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());

        String time = mFormat.format(mCalendar.getTime());

        this._textView.setText(time.replace("a.m.", "AM").replace("p.m.","PM"));

        if (mFormat1 == null)
            mFormat1 = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

        this._textViewInvisible.setText(mFormat1.format(mCalendar.getTime()));
    }
}