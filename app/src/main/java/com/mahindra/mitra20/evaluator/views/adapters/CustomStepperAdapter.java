package com.mahindra.mitra20.evaluator.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.interfaces.CustomStepperListener;
import com.mahindra.mitra20.evaluator.models.Stepper;
import com.mahindra.mitra20.evaluator.models.Stepper.Status;

import java.util.List;

public class CustomStepperAdapter extends RecyclerView.Adapter<CustomStepperAdapter.StepperViewHolder> {
    private List<Stepper> stepperList;
    private Context context;
    private CustomStepperListener customStepperListener;
    private int currentPosition = 0;

    public CustomStepperAdapter(List<Stepper> stepperList, Context context) {
        this.stepperList = stepperList;
        this.stepperList.get(0).setStatus(Status.ACTIVE);
        this.context = context;
    }

    @NonNull
    @Override
    public StepperViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StepperViewHolder(LayoutInflater.from(context).inflate(R.layout.row_stepper,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StepperViewHolder holder, int position) {
        holder.onBind(stepperList.get(position));
    }

    @Override
    public int getItemCount() {
        return stepperList.size();
    }

    public void setCustomStepperListener(CustomStepperListener customStepperListener) {
        this.customStepperListener = customStepperListener;
    }

    public void nextStep() {
        if (currentPosition < (stepperList.size() - 1)) {
            setActiveState(currentPosition + 1);
        }
    }

    public void previousStep() {
        if (currentPosition > 0) {
            setActiveState(currentPosition - 1);
        }
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setActiveState(int pos) {
        if (customStepperListener != null)
            customStepperListener.activeStateChange(pos, stepperList.get(pos).getTitle());

        stepperList.get(currentPosition).setStatus(Status.IN_ACTIVE);
        currentPosition = pos;
        stepperList.get(currentPosition).setStatus(Status.ACTIVE);
        notifyDataSetChanged();

    }

    class StepperViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_count_and_status;
        private View v_strip;

        StepperViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_count_and_status = itemView.findViewById(R.id.tv_count_and_status);
            v_strip = itemView.findViewById(R.id.v_strip);
            tv_count_and_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (stepperList.get(getLayoutPosition()).getStatus() != Status.ACTIVE) {
                        setActiveState(getLayoutPosition());
                    }
                }
            });
        }


        void onBind(Stepper stepper) {
            tv_count_and_status.setText(stepper.getAbbreviation_title());
            v_strip.setVisibility(((stepperList.size() - 1) == getLayoutPosition()) ? View.GONE : View.VISIBLE);
            setStatus(stepper);
        }

        private void setStatus(Stepper stepper) {
            switch (stepper.getStatus()) {
                case IN_ACTIVE:
                    tv_count_and_status.setBackground(
                            context.getResources().getDrawable(R.drawable.step_in_active_bg));
                    break;
                case ACTIVE:
                    tv_count_and_status.setBackground(
                            context.getResources().getDrawable(R.drawable.step_active_bg));
                    break;
            }
        }
    }
}