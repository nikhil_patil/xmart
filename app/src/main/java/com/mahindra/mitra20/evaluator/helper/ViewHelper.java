package com.mahindra.mitra20.evaluator.helper;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;

import com.mahindra.mitra20.R;

/**
 * Created by PATINIK-CONT on 07-Jun-19.
 */
public class ViewHelper {

    public static void populateLeadView(Context context, TextView textView, int leadPriority) {
        switch (leadPriority) {
            case 1:
                textView.setBackgroundColor(ActivityCompat.getColor(context, R.color.colorRed));
                textView.setText("P1");
                break;
            case 2:
                textView.setBackgroundColor(ActivityCompat.getColor(context, R.color.colorOrange));
                textView.setText("P2");
                break;
            case 3:
            default:
                textView.setBackgroundColor(ActivityCompat.getColor(context, R.color.colorGreen));
                textView.setText("P3");
                break;
        }
    }
}
