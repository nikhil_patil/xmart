package com.mahindra.mitra20.evaluator.presenter;

import com.mahindra.mitra20.evaluator.models.UpcomingAppointmentModel;

import java.util.ArrayList;

/**
 * Created by BADHAP-CONT on 8/17/2018.
 */

public interface UpcomingRequestIn {
    public void onUpcomingDownloadSuccess(int requestId, ArrayList<UpcomingAppointmentModel> scheduleAppoinments);

    public void onUpcomingDownloadFail(int requestId, String message);
}
