package com.mahindra.mitra20.evaluator.views.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DialogsHelper;
import com.mahindra.mitra20.evaluator.models.CancelAppointmentModel;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

public class AppointmentCancelledDetails extends AppCompatActivity {

    TextView textViewReason;
    TextView textViewCustomerName, textViewVehicleName, textViewDate, textViewTime, textViewAddress, textViewMobileNo;
    Button button_confirm_appoinment, button_reschedule_appoinment, buttonCancelAppointment;
    String strOwnerName, strMobileNo, strVehicleName, strDate, strMonth, strAppointmentDate = "", strYear = "", strTime, strLocation, strRequestId;
    ImageView imageViewCall;
    String appointmentStatus = "CNF";
    String evaluatorId = "";
    Dialog dialog;
    CancelAppointmentModel scheduleAppoinment;
    DateHelper dateHelper;
    private DialogsHelper dialogsHelper;
    private Context context;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_appointment_cancelled_details);

        init();
    }

    public void init() {
        CommonHelper.settingCustomToolBarEvaluator(this, "Cancellation Details", View.INVISIBLE);
        context = getApplicationContext();
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper = new MasterDatabaseHelper(context);
        dateHelper = new DateHelper();
        dialogsHelper = new DialogsHelper(this);
        Intent intent = getIntent();
        scheduleAppoinment = intent.getParcelableExtra("CancelAppointmentModel");
        strRequestId = scheduleAppoinment.getRequestID();
        strOwnerName = scheduleAppoinment.getStrCustomerName();
        strMobileNo = scheduleAppoinment.getStrContactNo();
        strVehicleName = scheduleAppoinment.getStrVehicleName();
        strDate = scheduleAppoinment.getStrDate();
        //strMonth = scheduleAppoinment.getStrMonth();
        strTime = scheduleAppoinment.getStrTime();
        strLocation = scheduleAppoinment.getStrLocation();
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());

        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, scheduleAppoinment.getStrMake(), scheduleAppoinment.getStrModel());
        String strMake = makeModel.first;
        String strModel = makeModel.second;
//        String strMake = CommonHelper.getMakeSpinnerData(arrayListMake, scheduleAppoinment.getStrMake());
//        String strModel = CommonHelper.getModelSpinnerData(arrayListMake, scheduleAppoinment.getStrModel());


        ArrayList<CancelAppointmentModel> cancelList = evaluatorHelper.getCancellationDetails(strRequestId);

        textViewCustomerName = (TextView) findViewById(R.id.textViewCustomerName);
        textViewVehicleName = (TextView) findViewById(R.id.textViewVehicleName);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewMobileNo = (TextView) findViewById(R.id.textViewMobileNo);

        imageViewCall = (ImageView) findViewById(R.id.imageViewCall);

        textViewCustomerName.setText(strOwnerName);
        textViewVehicleName.setText(strMake + " " + strModel);
        textViewMobileNo.setText(strMobileNo);

        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        strMonth = dateHelper.getMonthName(appointmentDate);
        strAppointmentDate = dateHelper.getDate(appointmentDate);
        strYear = dateHelper.getYear(appointmentDate);

        textViewDate.setText(strAppointmentDate + " " + strMonth + " " + strYear);
        textViewTime.setText(strTime);
        textViewAddress.setText(strLocation);

        textViewReason = (TextView) findViewById(R.id.textViewReason);
        textViewReason.setText(cancelList.get(0).getStrCancel());

        imageViewCall = (ImageView) findViewById(R.id.imageViewCall);

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogsHelper.callDialog(strMobileNo);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (SessionUserDetails.getInstance().getUserType() == 0) {
            Intent i = new Intent(AppointmentCancelledDetails.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(AppointmentCancelledDetails.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}
