package com.mahindra.mitra20.evaluator.helper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by BADHAP-CONT on 8/13/2018.
 */

public class DatePickerHelper implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    TextView _textView,_textViewInVisible;
    TableRow _tableRow;
    private int _day;
    private int _month;
    private int _birthYear;
    private Context _context;
    String _monthName = "",_dayName="";
    String _dateFormat = "";
    String _maxDate = "";
    DateHelper dateHelper;
    boolean _isSetMinDate;
    boolean _isSetMaxDate;

    public DatePickerHelper(Context context, int textViewID, int textViewInvisible, boolean isSetMinDate) {
        Activity act = (Activity) context;
        this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);
        this._textView.setOnClickListener(this);
        this._context = context;
        this._isSetMinDate = isSetMinDate;
        dateHelper = new DateHelper();

        onClick(null);
    }

    public DatePickerHelper(Context context, TextView textViewID, TextView textViewInvisible, boolean isSetMinDate) {
        Activity act = (Activity) context;
        /*this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);*/
        this._textView = textViewID;
        this._textViewInVisible = textViewInvisible;
        this._textView.setOnClickListener(this);
        this._context = context;
        this._isSetMinDate = isSetMinDate;
        dateHelper = new DateHelper();

        onClick(null);
    }


    public DatePickerHelper(Context context, TextView textViewID, TextView textViewInvisible, String maxDate, boolean isSetMinDate, boolean isSetMaxDate) {
        Activity act = (Activity) context;
        /*this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);*/
        this._textView = textViewID;
        this._textViewInVisible = textViewInvisible;
        this._textView.setOnClickListener(this);
        this._context = context;
        this._isSetMinDate = isSetMinDate;
        this._isSetMaxDate = isSetMaxDate;
        this._maxDate = maxDate;
        dateHelper = new DateHelper();
    }

    public DatePickerHelper(Context context, int textViewID, int textViewInvisible, boolean isSetMinDate, boolean isSetMaxDate) {
        Activity act = (Activity) context;
        /*this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);*/
        this._textView = act.findViewById(textViewID);
        this._textViewInVisible = act.findViewById(textViewInvisible);
//        this._textView.setOnClickListener(this);
        this._context = context;
        this._isSetMinDate = isSetMinDate;
        this._isSetMaxDate = isSetMaxDate;
        dateHelper = new DateHelper();
        onClick(null);
    }

    public DatePickerHelper(Context context, TextView textViewID, TextView textViewInvisible, boolean isSetMinDate, boolean isSetMaxDate) {
        Activity act = (Activity) context;
        /*this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);*/
        this._textView = textViewID;
        this._textViewInVisible = textViewInvisible;
        this._textView.setOnClickListener(this);
        this._context = context;
        this._isSetMinDate = isSetMinDate;
        this._isSetMaxDate = isSetMaxDate;
        dateHelper = new DateHelper();
    }

    public DatePickerHelper(Context context, TextView textViewID, TextView textViewInvisible) {
        Activity act = (Activity) context;
        /*this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);*/
        this._textView = textViewID;
        this._textViewInVisible = textViewInvisible;
        this._textView.setOnClickListener(this);
        this._context = context;
        dateHelper = new DateHelper();
    }

    public DatePickerHelper(Context context, int textViewID, int textViewInvisible, int tableRow) {
        Activity act = (Activity) context;
        this._textView = (TextView) act.findViewById(textViewID);
        this._textViewInVisible = (TextView) act.findViewById(textViewInvisible);
        this._tableRow = (TableRow) act.findViewById(tableRow);
        this._tableRow.setOnClickListener(this);
        this._context = context;
        dateHelper = new DateHelper();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        _birthYear = year;
        _day = dayOfMonth;
        _month = monthOfYear;
        updateDisplay();
    }

    @Override
    public void onClick(View v) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog dialog = new DatePickerDialog(_context, this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        if (_isSetMinDate)
            dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        if (_isSetMaxDate) {
            try {
                Calendar cal = Calendar.getInstance();
//                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                //cal.setTime(sdf.parse(_maxDate));
                cal.add(Calendar.DAY_OF_MONTH, 2);
                dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dialog.show();
    }

    // updates the date in the birth date textView
    private void updateDisplay() {
        if (String.valueOf(_month + 1).length() == 1) {
            _monthName = "0" + (_month + 1);
        } else {
            _monthName = String.valueOf(_month + 1);
        }

        if (String.valueOf(_day).length() == 1) {
            _dayName = "0" + (_day);
        } else {
            _dayName = String.valueOf(_day);
        }
        StringBuilder strBuilder = new StringBuilder()
                // Month is 0 based so add 1
                .append(_dayName).append("/").append(_monthName).append("/").append(_birthYear);

        Date appointmentDate = dateHelper.convertStringToDate(strBuilder.toString());
        String strMonth = dateHelper.getMonthName(appointmentDate);
        String strAppointmentDate = dateHelper.getDate(appointmentDate);
        String strYear = dateHelper.getYear(appointmentDate);

        _textView.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(strAppointmentDate).append(" ").append(strMonth).append(" ").append(strYear));


        _textViewInVisible.setText(strBuilder);
    }
}