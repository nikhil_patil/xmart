package com.mahindra.mitra20.evaluator.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.mahindra.mitra20.R;

/**
 * Created by BADHAP-CONT on 8/14/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected TextView txtHeading;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    protected void setHeading(int resId) {
        if(txtHeading == null)
            txtHeading = findViewById(R.id.textViewHeader);
        if(txtHeading != null)
            txtHeading.setText(resId);
    }
}
