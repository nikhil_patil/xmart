package com.mahindra.mitra20.evaluator.presenter;

/**
 * Created by PATINIK-CONT on 7/12/2019.
 */

public interface NewEvaluationActivityIn {
    void onDataSubmissionSuccess(int requestId, String response);

    void onDataSubmissionFailed(int requestId, String message);
}