package com.mahindra.mitra20.evaluator.presenter.evalform;

import android.content.Context;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.models.VehiclePhoto;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.sqlite.NewEvaluationHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PATINIK-CONT on 30/12/2019.
 */

public class ImagesPresenter {

    private Context context;
    public ImagesPresenter(Context context) {
        this.context = context;
    }

    public List<VehiclePhoto> prepareVehiclePhotosArray(NewEvaluation newEvaluation) {
        List<VehiclePhoto> vehiclePhotos = new ArrayList<>();
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.FrontImage),
                newEvaluation.getPhotoPathFrontImage(), "DOC0001", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RearImage),
                newEvaluation.getPhotoPathRearImage(), "DOC0002", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.fortyFivedegreeLeftView),
                newEvaluation.getPhotoPath45DegreeLeftView(), "DOC0009", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.fortyFiveDegreeRightView),
                newEvaluation.getPhotoPath45DegreeRightView(), "DOC0010", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.DashboardView),
                newEvaluation.getPhotoPathDashboardView(), "DOC0013", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.OdometerImage),
                newEvaluation.getPhotoPathOdometerImage(), "DOC0014", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.ChasisNoImage),
                newEvaluation.getPhotoPathChassisNoImage(), "DOC0011", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RoofTopImage),
                newEvaluation.getPhotoPathRoofTopImage(), "DOC0026", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.InteriorView),
                newEvaluation.getPhotoPathInteriorView(), "DOC0015", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RCCopyImage),
                newEvaluation.getPhotoPathRCCopyImage(), "DOC0016", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RCCopyImage1),
                newEvaluation.getPhotoPathRCCopy1(), "DOC0027", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RCCopyImage2),
                newEvaluation.getPhotoPathRCCopy2(), "DOC0028", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RCCopyImage3),
                newEvaluation.getPhotoPathRCCopy3(), "DOC0029", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.InsuranceImage),
                newEvaluation.getPhotoPathInsuranceImage(), "DOC0017", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RearLeftTyre),
                newEvaluation.getPhotoPathRearLeftTyre(), "DOC0018", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.RearRightTyre),
                newEvaluation.getPhotoPathRearRightTyre(), "DOC0019", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.FrontLeftTyre),
                newEvaluation.getPhotoPathFrontLeftTyre(), "DOC0020", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.FrontRightTyre),
                newEvaluation.getPhotoPathFrontRightTyre(), "DOC0021", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.EngineNoImage),
                newEvaluation.getPhotoPathEngineNoImage(), "DOC0012", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.DamagedPartImageOne),
                newEvaluation.getPhotoPathDamagedPartImage1(), "DOC0022", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.DamagedPartImageTwo),
                newEvaluation.getPhotoPathDamagedPartImage2(), "DOC0023", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.DamagedPartImageThree),
                newEvaluation.getPhotoPathDamagedPartImage3(), "DOC0024", true));
        vehiclePhotos.add(new VehiclePhoto(context.getString(R.string.DamagedPartImageFour),
                newEvaluation.getPhotoPathDamagedPartImage4(), "DOC0025", true));
        vehiclePhotos.add(new VehiclePhoto("None", "", "NONE", false));
        return  vehiclePhotos;
    }

    public NewEvaluation getEvaluationData(String evaluationId) {
        return new NewEvaluationHelper(context).getEvaluationDetails(evaluationId,
                EvaluatorConstants.EvaluationParts.IMAGES);
    }

    public void saveData(NewEvaluation newEvaluation) {
        new NewEvaluationHelper(context).saveAndUpdateNewEvaluationDraft(newEvaluation,
                EvaluatorConstants.EvaluationParts.IMAGES);
    }
}