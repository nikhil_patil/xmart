package com.mahindra.mitra20.evaluator.views.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.helper.DatePickerHelper;
import com.mahindra.mitra20.evaluator.helper.DialogsHelper;
import com.mahindra.mitra20.evaluator.helper.TimePickerHelper;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.models.ScheduleAppoinment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class RescheduleAppointmentActivity extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback, VolleySingleTon.VolleyInteractor {

    private String strRequestId = "", strOwnerName = "", strMobileNo = "", strVehicleName = "", strDate = "",
            strAppointmentDate = "", strYear = "", strMonth = "", strTimeText = "", strTime = "", strLocation = "", strNotes = "";
    private TextView textViewCustomerName, textViewVehicleName, textViewMobileNo, textViewDate,
            textViewDateInvisible, textViewTime, textViewAddress, textViewTimeInvisible;
    private TableRow tableRowTimePicker;
    private Button buttonRescheduleAppointment, buttonCancelAppointment;
    private Dialog dialog;
    private DateHelper dateHelper;
    private ImageView imageViewCall;
    private Context context;
    private DialogsHelper dialogsHelper;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private Date appointmentDate;
    private String evaluatorId = "";
    private GoogleMap map;
    private ScheduleAppoinment scheduleAppoinment;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();
    private String strMake = "", strModel = "";
    private int comparison = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_reschedule_appoinment);

        dateHelper = new DateHelper();
        context = getApplicationContext();
        dialogsHelper = new DialogsHelper(this);

        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper = new MasterDatabaseHelper(context);
        Intent intent = getIntent();
        if (intent != null) {
            scheduleAppoinment = intent.getParcelableExtra("scheduleAppoinment");
            strRequestId = scheduleAppoinment.getRequestID();
            strOwnerName = scheduleAppoinment.getStrCustomerName();
            strMobileNo = scheduleAppoinment.getStrContactNo();
            // strVehicleName = scheduleAppoinment.getStrVehicleName();
            strDate = scheduleAppoinment.getStrDate();
            strLocation = scheduleAppoinment.getStrLocation();
            strTime = scheduleAppoinment.getStrTime();
            strNotes = scheduleAppoinment.getNotes();

            appointmentDate = dateHelper.convertStringToDate(strDate);
            strMonth = dateHelper.getMonthName(appointmentDate);
            strAppointmentDate = dateHelper.getDate(appointmentDate);
            strYear = dateHelper.getYear(appointmentDate);
            strTimeText = dateHelper.convertTwentyFourHourToTweleveHour(scheduleAppoinment.getStrTime());
        }

        CommonHelper.settingCustomToolBarEvaluator(this, "Reschedule Appointment", View.INVISIBLE);
        init();
    }

    public void init() {
        evaluatorId = SessionUserDetails.getInstance().getUserID();
        try {
            String userId1 = SessionUserDetails.getInstance().getUserID();
            if (userId1.contains(".")) {
                String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                evaluatorId = arrUserId[0];
            } else {
                evaluatorId = userId1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageViewCall = (ImageView) findViewById(R.id.imageViewCall);
        imageViewCall.setOnClickListener(this);
        textViewCustomerName = (TextView) findViewById(R.id.textViewCustomerName);
        textViewVehicleName = (TextView) findViewById(R.id.textViewVehicleName);
        textViewMobileNo = (TextView) findViewById(R.id.textViewMobileNo);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewDateInvisible = (TextView) findViewById(R.id.textViewDateInvisible);
        textViewTimeInvisible = (TextView) findViewById(R.id.textViewTimeInvisible);
        textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        buttonRescheduleAppointment = (Button) findViewById(R.id.buttonRescheduleAppoinment);
        buttonRescheduleAppointment.setOnClickListener(this);
        buttonCancelAppointment = (Button) findViewById(R.id.buttonCancelAppointment);
        buttonCancelAppointment.setOnClickListener(this);
        textViewDate.setOnClickListener(this);
        textViewTime.setOnClickListener(this);

        tableRowTimePicker = (TableRow) findViewById(R.id.tableRowTimePicker);

        textViewCustomerName.setText(strOwnerName);
        textViewVehicleName.setText(strVehicleName);
        textViewMobileNo.setText(strMobileNo);

        textViewDateInvisible.setText(strDate);
        textViewTimeInvisible.setText(strTime);

        textViewDate.setText(strAppointmentDate + " " + strMonth + " " + strYear);
        textViewTime.setText(strTimeText);

        textViewAddress.setText(strLocation);

        strMake = scheduleAppoinment.getStrMake();
        strModel = scheduleAppoinment.getStrModel();

        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());

        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, strMake, strModel);
        String strMake1 = makeModel.first;
        String strModel1 = makeModel.second;
//        String strMake1 = CommonHelper.getMakeSpinnerData(arrayListMake, strMake);
//        String strModel1 = CommonHelper.getModelSpinnerData(arrayListMake, strModel);

        textViewVehicleName.setText(strMake1 + " " + strModel1);
        // Getting Reference to SupportMapFragment of activity_map.xml
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        getLatLongFromAddress(strLocation);
    }

    private void getLatLongFromAddress(String fullAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(fullAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                LatLng latLng = new LatLng(latitude, longitude);
                map.addMarker(new MarkerOptions().position(latLng)
                        .title(strLocation));
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                map.animateCamera(CameraUpdateFactory.zoomTo(15), 200, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonRescheduleAppoinment:
                if (validateRescheduling()) {
                    try {

                        if (CommonHelper.isConnectingToInternet(context)) {
                            strTime = textViewTimeInvisible.getText().toString();
                            String userId1 = SessionUserDetails.getInstance().getUserID();
                            if (userId1.contains(".")) {
                                String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                                evaluatorId = arrUserId[0].toUpperCase();
                            } else {
                                evaluatorId = userId1;
                            }
                            HashMap<String, String> params = new HashMap<>();
                            params.put(WebConstants.appointmentConfirm.REQUEST_ID, strRequestId); //"ENQ19A000142"
                            params.put(WebConstants.appointmentConfirm.EVALUATIOR_ID, evaluatorId);// "MV010107");//evaluatorId
                            params.put(WebConstants.appointmentConfirm.APPOINTMENT_STATUS, "RES");// "CNF");//appointmentStatus
                            params.put(WebConstants.appointmentConfirm.APPOINTMENT_DATE_TIME, textViewDateInvisible.getText().toString()
                                    + " " + textViewTimeInvisible.getText().toString());//"06/08/2018 12:00:00");//appointmentDateTime
                            params.put(WebConstants.appointmentConfirm.CANCELLATION_REASON_ID, "");//"06/08/2018 12:00:00");//appointmentDateTime
                            JSONObject jsonObject = new JSONObject(params);
                            VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.appointmentConfirm.URL, jsonObject, 2);
                        } else {
                            CommonHelper.toast(R.string.internet_error, context);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.buttonCancelAppointment:
                String userId1 = SessionUserDetails.getInstance().getUserID();
                if (userId1.contains(".")) {
                    String[] arrUserId = SessionUserDetails.getInstance().getUserID().split("\\.");
                    evaluatorId = arrUserId[0];
                } else {
                    evaluatorId = userId1;
                }
                dialogsHelper.showCancelAppointmentDialog(2, strRequestId, strNotes);
                break;
            case R.id.buttonCancelAppointmentPopup:

                break;
            case R.id.textViewDate:
                new DatePickerHelper(this, R.id.textViewDate, R.id.textViewDateInvisible, true);
                break;
            case R.id.textViewTime:
                new TimePickerHelper(this, R.id.textViewTime, R.id.tableRowTimePicker, R.id.textViewTimeInvisible);
                break;
            case R.id.imageViewClose:
                dialog.dismiss();
                break;
            case R.id.imageViewCall:
                dialogsHelper.callDialog(strMobileNo);
                break;
        }
    }

   /* @Override
    public void onSuccess(String reason, String RequestId, String evaluatorId) {
        HashMap<String,Object> hashMapValues = new HashMap<>();
        hashMapValues.put("reason",reason);
        hashMapValues.put("RequestId", RequestId);
        hashMapValues.put("evaluatorId",evaluatorId);
        JSONObject jsonObject = new JSONObject(hashMapValues);
        VolleySingleTon.getInstance().connectToPostUrl(context, this, WebConstants.ScheduleAppoinmentList.URL, jsonObject, WebConstants.ScheduleAppoinmentList.REQUEST_CODE);

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onNeutral() {

    }*/

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        switch (requestId) {
            case 1:
                CommonHelper.toast(R.string.confirmation_success, getApplicationContext());
                startActivity(new Intent(RescheduleAppointmentActivity.this, AppointmentConfirmedActivity.class));
                finish();
                break;
            case 2:
                try {
                    String strDate = textViewDateInvisible.getText().toString();
                    String strTime = textViewTimeInvisible.getText().toString();
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    if (IsSuccessful.equals("1")) {
                        evaluatorHelper.updateScheduleAppointment(strRequestId, strDate, strTime);
                        CommonHelper.toast(message, getApplicationContext());
                        startActivity(new Intent(RescheduleAppointmentActivity.this, AppointmentRescheduledPopupActivity.class).putExtra("date", textViewDate.getText().toString()).putExtra("time", textViewTime.getText().toString()));
                        finish();
                    } else {
                        CommonHelper.toast(message, getApplicationContext());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                CommonHelper.toast(R.string.cancel_appointment_successfully, getApplicationContext());
                startActivity(new Intent(RescheduleAppointmentActivity.this, EvaluatorDashboardActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        switch (requestId) {
            case 1:
                CommonHelper.toast(R.string.confirmation_failure, getApplicationContext());
                //startActivity(new Intent(AppoinmentDetailsActivity.this, EvaluatorDashboardActivity.class));
                break;
            case 2:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String IsSuccessful = jsonObject.get("IsSuccessful").toString();
                    String message = jsonObject.get("message").toString();
                    CommonHelper.toast(message, getApplicationContext());
                    //finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //startActivity(new Intent(AppoinmentDetailsActivity.this, EvaluatorDashboardActivity.class));
                break;
            case 3:
                CommonHelper.toast(R.string.cancelled_failure, getApplicationContext());
                //startActivity(new Intent(AppoinmentDetailsActivity.this, EvaluatorDashboardActivity.class));
                break;
        }
    }

    public boolean validateRescheduling() {
        try {
            Date currentTime = new Date();
            String strCurrentDate = dateHelper.getCurrentDate();
            String strRescheduledDate = textViewDateInvisible.getText().toString();
            String strRescheduledTime = textViewTimeInvisible.getText().toString();

            if (strDate.equals(strRescheduledDate) && (strTime.equals(strRescheduledTime))) {
                CommonHelper.toast("Please select date and time for reschudling appointment", context);
                return false;
            }

            Date dateCurrentDate = dateHelper.convertStringToDate(strCurrentDate);
            Date dateRescheduledDate = dateHelper.convertStringToDate(strRescheduledDate);

            /*
             *If current date and reschedule date are same then it returns 0,
             *If current date is less than reschedule date then it returns -1,
             *If current date is greater than reschedule date then it return 1
             */
            comparison = dateHelper.compareDates(dateCurrentDate, dateRescheduledDate);

            if (comparison == 1) {
                CommonHelper.toast("Rescheduled date must not be Previous date", RescheduleAppointmentActivity.this);
                return false;
            }
            if (comparison == 0) {
                String strRescheduleTime = strRescheduledDate + " " + strRescheduledTime;
                Date rescheduledTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(strRescheduleTime);
                long elapsed = rescheduledTime.getTime() - currentTime.getTime();

                if (elapsed < 0) {
                    System.out.println(elapsed);
                    CommonHelper.toast("Reschedule time must be greater than current time.", context);
                    return false;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SessionUserDetails.getInstance().getUserType() == 0) {
            Intent i = new Intent(RescheduleAppointmentActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        } else {
            //super.onBackPressed();
            Intent i = new Intent(RescheduleAppointmentActivity.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}
