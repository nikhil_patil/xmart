package com.mahindra.mitra20.evaluator.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.models.SessionUserDetails;

public class EvaluationSubmittedSuccessfully extends AppCompatActivity {

    TextView textViewEvaluationNo, textViewContinue;
    String EvaluationReportID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_evaluation_submitted_successfully);

        textViewEvaluationNo = (TextView) findViewById(R.id.textViewEvaluationNo);
        textViewContinue = (TextView) findViewById(R.id.textViewContinue);

        Intent i = getIntent();
        EvaluationReportID = i.getStringExtra("EvaluationReportID");

        textViewEvaluationNo.setText("Evaluation Report No. " + EvaluationReportID + "\nSubmitted Successfully.!");

        textViewContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (SessionUserDetails.getInstance().getUserType() == 0) {
                    Intent i = new Intent(EvaluationSubmittedSuccessfully.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } else {
                    Intent intent = new Intent(EvaluationSubmittedSuccessfully.this,
                            EvaluatorDashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);/*
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);*/
                    startActivity(intent);
                    finish();
                }
    }
}
