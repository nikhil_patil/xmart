package com.mahindra.mitra20.evaluator.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.DateHelper;
import com.mahindra.mitra20.evaluator.models.CancelAppointmentModel;
import com.mahindra.mitra20.evaluator.models.DmsMakeMasterModel;
import com.mahindra.mitra20.evaluator.views.activity.AppointmentCancelledDetails;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by BADHAP-CONT on 4/9/2018.
 */

public class CancelledAppointmentAdapter extends RecyclerView.Adapter<CancelledAppointmentAdapter.ViewHolder> {
    private ArrayList<CancelAppointmentModel> CancelAppointmentModelContents;
    private Context context;
    private String type;
    private DateHelper dateHelper;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private ArrayList<DmsMakeMasterModel> arrayListMake = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewBackground, textViewMonth, textViewDate, textViewTime, tvEnquirySource,
                textViewCustomerName, textViewVehicleName, textViewLocation, textViewLeadPriority,
                tvEnquiryType;
        LinearLayout linearLayoutOverdueVisits;
        public View layout;
        public ImageView imageViewCall, imageViewMessage;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            textViewMonth = v.findViewById(R.id.textViewMonth);
            textViewDate = v.findViewById(R.id.textViewDate);
            textViewTime = v.findViewById(R.id.textViewTime);
            textViewCustomerName = v.findViewById(R.id.textViewCustomerName);
            textViewVehicleName = v.findViewById(R.id.textViewVehicleName);
            textViewLocation = v.findViewById(R.id.tvAddress);
            textViewBackground = v.findViewById(R.id.textViewBackground);
            imageViewCall = v.findViewById(R.id.imageViewCall);
            imageViewMessage = v.findViewById(R.id.imageViewMessage);
            textViewLeadPriority = v.findViewById(R.id.textViewLeadPriority);
            tvEnquirySource = v.findViewById(R.id.tvEnquirySource);
            tvEnquiryType = v.findViewById(R.id.tvEnquiryType);

            linearLayoutOverdueVisits = v.findViewById(R.id.linearLayoutOverdueVisits);
        }
    }

    public void updateList(ArrayList<CancelAppointmentModel> CancelAppointmentModelContents) {
        this.CancelAppointmentModelContents = CancelAppointmentModelContents;
    }

    public CancelledAppointmentAdapter(Context _context, ArrayList<CancelAppointmentModel> _CancelAppointmentModelContents, String _type) {
        CancelAppointmentModelContents = _CancelAppointmentModelContents;
        context = _context;
        type = _type;
        dateHelper = new DateHelper();
        evaluatorHelper = new EvaluatorHelper(context);
        masterDatabaseHelper = new MasterDatabaseHelper(context);
        arrayListMake.addAll(masterDatabaseHelper.getDmsMasterModel());
    }

    @Override
    public CancelledAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_overdue_visits, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String strDate = CancelAppointmentModelContents.get(position).getStrDate();
        Date appointmentDate = dateHelper.convertStringToDate(strDate);
        String strMonth = dateHelper.getMonthName(appointmentDate);
        String strAppointmentDate = dateHelper.getDate(appointmentDate);
        String strTime = dateHelper.convertTwentyFourHourToTweleveHour(CancelAppointmentModelContents.get(position).getStrTime());

        holder.textViewMonth.setText(strMonth);
        holder.textViewDate.setText(strAppointmentDate);
        holder.textViewTime.setText(strTime);
        holder.textViewCustomerName.setText(CancelAppointmentModelContents.get(position).getStrCustomerName());
        holder.textViewLeadPriority.setVisibility(View.INVISIBLE);
        holder.textViewLocation.setText(CancelAppointmentModelContents.get(position).getStrLocation());

        Pair<String, String> makeModel = CommonHelper.getMakeModelSpinnerData(
                arrayListMake, CancelAppointmentModelContents.get(position).getStrMake(),
                CancelAppointmentModelContents.get(position).getStrModel());
        String strMake = makeModel.first;
        String strModel = makeModel.second;
        holder.textViewVehicleName.setText(String.format("%s %s", strMake, strModel));
        holder.tvEnquirySource.setVisibility(View.GONE);
        holder.tvEnquiryType.setVisibility(View.GONE);

        if (type.equals("Overdue") || type.equals("Draft")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorOrange));
        } else if (type.equals("Upcoming")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        } else if (type.equals("Completed")) {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
        } else {
            holder.textViewBackground.setBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        }

        holder.linearLayoutOverdueVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masterDatabaseHelper.isMasterSync()) {
                    CancelAppointmentModel scheduleAppoinmet = CancelAppointmentModelContents.get(position);
                    String strCustId = CancelAppointmentModelContents.get(position).getStrCustId();
                    Intent intent = new Intent(context, AppointmentCancelledDetails.class);
                    intent.putExtra("CancelAppointmentModel", scheduleAppoinmet);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    CommonHelper.toast(R.string.master_sync_request, context);
                }
            }
        });

        holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new com.mahindra.mitra20.champion.helper.DialogsHelper((Activity) context).callDialog(CancelAppointmentModelContents.get(position).getStrContactNo());
            }
        });

        holder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.
                        sendMessage(CancelAppointmentModelContents.get(position).getStrContactNo(), context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return CancelAppointmentModelContents.size();
    }
}