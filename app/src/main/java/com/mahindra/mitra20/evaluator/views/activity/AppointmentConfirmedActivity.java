package com.mahindra.mitra20.evaluator.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.models.SessionUserDetails;

public class AppointmentConfirmedActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textViewContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_appointment_confirmed);

      //  CommonHelper.settingCustomToolBarEvaluator(this, "Confirm Appointment", View.INVISIBLE);
        textViewContinue = (TextView) findViewById(R.id.textViewContinue);
        textViewContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewContinue:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(SessionUserDetails.getInstance().getUserType()== 0)
        {
            Intent i = new Intent(AppointmentConfirmedActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
        else {
            Intent i = new Intent(AppointmentConfirmedActivity.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }

}
