package com.mahindra.mitra20.evaluator.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.adapters.EvaluationDraftAdapter;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.EmptyRecyclerView;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

public class EvaluationCompletedDraftActivity extends AppCompatActivity implements View.OnClickListener, EvaluationDraftAdapter.DraftListener {

    LinearLayout linearLayoutDraft, linearLayoutCompleted;
    EmptyRecyclerView recyclerViewCompletedDraftEvaluation;
    EvaluatorHelper evaluatorHelper;
    private int[] textView_idEvaluation = {R.id.textViewCompleteLine, R.id.textViewDraftLine};
    private TextView[] textViewEvaluation = new TextView[2];
    TextView textView_unfocusEvaluation, textViewEvaluationType;
    TextView textViewDraft, textViewCompleted;
    String appointmentType = "";
    EvaluationDraftAdapter scheduleAppointmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_evaluation_completed_draft);
        init();
    }

    public void init() {
        appointmentType = "";
        CommonHelper.settingCustomToolBarEvaluator(this, "Evaluation Status", View.INVISIBLE);
        //CommonHelper.setCustomToolBar(this, "Evaluation Status");
        evaluatorHelper = new EvaluatorHelper(getBaseContext());

        try {
            Intent intent = getIntent();
            appointmentType = intent.getStringExtra("appointmentType");
        } catch (Exception e) {
            e.printStackTrace();
        }

        textViewEvaluationType = findViewById(R.id.textViewEvaluationType);
        linearLayoutDraft = findViewById(R.id.linearLayoutDraft);
        linearLayoutCompleted = findViewById(R.id.linearLayoutCompleted);

        linearLayoutDraft.setOnClickListener(this);
        linearLayoutCompleted.setOnClickListener(this);

        textViewDraft = findViewById(R.id.textViewDraft);
        textViewCompleted = findViewById(R.id.textViewCompleted);

        try {
            recyclerViewCompletedDraftEvaluation = findViewById(R.id.recyclerViewCompletedDraftEvaluation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < textViewEvaluation.length; i++) {
            textViewEvaluation[i] = findViewById(textView_idEvaluation[i]);
            textViewEvaluation[i].setBackgroundColor(getResources().getColor(R.color.colorGrey));
        }
        textView_unfocusEvaluation = textViewEvaluation[0];

        setUpLists();
    }

    private void setUpLists() {
        if (appointmentType.equals("Completed")) {
            textViewEvaluationType.setText("Completed evaluation");
            completedEvaluation();
            setFocus(textView_unfocusEvaluation, textViewEvaluation[0]);
            textViewCompleted.setTextColor(getResources().getColor(R.color.colorText));
            textViewDraft.setTextColor(getResources().getColor(R.color.colorTextGray));
        } else if (appointmentType.equals("Draft")) {
            textViewEvaluationType.setText("Pending evaluation");
            setFocus(textView_unfocusEvaluation, textViewEvaluation[1]);
            draftedEvaluation();
            textViewCompleted.setTextColor(getResources().getColor(R.color.colorTextGray));
            textViewDraft.setTextColor(getResources().getColor(R.color.colorText));
        }
    }

    private void setFocus(TextView textView_unfocus, TextView textView_focus) {
        textView_unfocus.setBackgroundColor(getResources().getColor(R.color.colorGrey));
        textView_focus.setBackgroundColor(getResources().getColor(R.color.colorRed));
        this.textView_unfocusEvaluation = textView_focus;
    }

    public void draftedEvaluation() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewCompletedDraftEvaluation.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewCompletedDraftEvaluation.setEmptyView(findViewById(R.id.list_empty_draft));
        findViewById(R.id.list_empty_complelted).setVisibility(View.GONE);
        if (evaluatorHelper.getEvaluationList(EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT).size() > 0) {
            textViewEvaluationType.setVisibility(View.VISIBLE);
        } else {
            textViewEvaluationType.setVisibility(View.GONE);
        }
        scheduleAppointmentAdapter = new EvaluationDraftAdapter(
                this, evaluatorHelper.getEvaluationList(EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT), "Draft");
        scheduleAppointmentAdapter.setDraftListener(this);
        recyclerViewCompletedDraftEvaluation.setAdapter(scheduleAppointmentAdapter);
    }

    public void completedEvaluation() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewCompletedDraftEvaluation.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        recyclerViewCompletedDraftEvaluation.setEmptyView(findViewById(R.id.list_empty_complelted));
        findViewById(R.id.list_empty_draft).setVisibility(View.GONE);
        if (evaluatorHelper.getEvaluationList(EvaluatorConstants.EVALUATION_DB_STATUS_DRAFT).size() > 0) {
            textViewEvaluationType.setVisibility(View.VISIBLE);
        } else {
            textViewEvaluationType.setVisibility(View.GONE);
        }
        scheduleAppointmentAdapter = new EvaluationDraftAdapter(this,
                evaluatorHelper.getEvaluationList(EvaluatorConstants.EVALUATION_DB_STATUS_COMPLETED), "Completed");
        recyclerViewCompletedDraftEvaluation.setAdapter(scheduleAppointmentAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearLayoutDraft:
                textViewEvaluationType.setText("Pending evaluation");
                setFocus(textView_unfocusEvaluation, textViewEvaluation[1]);
                draftedEvaluation();
                textViewCompleted.setTextColor(getResources().getColor(R.color.colorText));
                textViewDraft.setTextColor(getResources().getColor(R.color.colorTextGray));
                break;
            case R.id.linearLayoutCompleted:
                textViewEvaluationType.setText("Completed evaluation");
                setFocus(textView_unfocusEvaluation, textViewEvaluation[0]);
                completedEvaluation();
                textViewCompleted.setTextColor(getResources().getColor(R.color.colorTextGray));
                textViewDraft.setTextColor(getResources().getColor(R.color.colorText));
                break;
        }
    }

    @Override
    public void deleteDraft(NewEvaluation newEvaluation) {
        if (evaluatorHelper.deleteDraftEntry(newEvaluation.getId())) {
            scheduleAppointmentAdapter.removeAndNotifyAdapter(newEvaluation);
        }
    }

    /*@Override
    protected void onRestart() {
        super.onRestart();
        if (appointmentType.equals("Completed")) {
            scheduleAppointmentAdapter.updateItems(
                    evaluatorHelper.getCompletedEvaluationDetails("Completed"));
        } else if (appointmentType.equals("Draft")) {
            scheduleAppointmentAdapter.updateItems(evaluatorHelper.getEvaluationList());
        }
    }*/

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if (SessionUserDetails.getInstance().getUserType() == 0) {
////            COMMENTED BY PRAVIN DHARAM ON 22-11-2019
////            Intent i = new Intent(EvaluationCompletedDraftActivity.this, ChampionDashboardActivity.class);
////            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////            startActivity(i);
////            finish();
//        } else {
////            Intent i = new Intent(EvaluationCompletedDraftActivity.this, EvaluatorDashboardActivity.class);
////            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////            startActivity(i);
////            finish();
//        }
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SessionUserDetails.getInstance().getUserType() == 0) {
            Intent i = new Intent(EvaluationCompletedDraftActivity.this, ChampionDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(ChampionConstants.FROM_DRAFT_LIST_ACTIVITY, true);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(EvaluationCompletedDraftActivity.this, EvaluatorDashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra(ChampionConstants.FROM_DRAFT_LIST_ACTIVITY, true);
            startActivity(i);
            finish();
        }
    }
}
