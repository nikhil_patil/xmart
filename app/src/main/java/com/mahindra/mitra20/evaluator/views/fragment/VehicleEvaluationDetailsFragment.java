package com.mahindra.mitra20.evaluator.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.databinding.LayoutVehicleEvaluationDetailsBinding;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.helper.FieldValidator;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.presenter.evalform.EvaluationDetailsPresenter;
import com.mahindra.mitra20.interfaces.CommonMethods;
import com.mahindra.mitra20.models.SessionUserDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class VehicleEvaluationDetailsFragment extends Fragment implements CommonMethods {

    LayoutVehicleEvaluationDetailsBinding binding;
    private NewEvaluation newEvaluation;
    private Context context;
    private EvaluationDetailsPresenter evaluationDetailsPresenter;
    private ArrayList<MasterModel> arrayListModelOptedNewVehicle = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {

        binding = LayoutVehicleEvaluationDetailsBinding.inflate(inflater,
                container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        evaluationDetailsPresenter = new EvaluationDetailsPresenter(context);
        String evaluationId;
        if (null != getArguments()) {
            evaluationId = getArguments().getString(EvaluatorConstants.EXTRA_EVAL_ID);
            newEvaluation = evaluationDetailsPresenter.getEvaluationData(evaluationId);
        }
        init();
        displayData();
    }

    @Override
    public void init() {
        try {
            arrayListModelOptedNewVehicle.add(new MasterModel("Select Model", "Select Model"));
            arrayListModelOptedNewVehicle.addAll(evaluationDetailsPresenter.getModelMaster("MAHINDRA"));
            ArrayAdapter<MasterModel> OptedNewVehicleModelAdapter = new ArrayAdapter<>(context,
                    R.layout.simple_spinner_item, arrayListModelOptedNewVehicle);
            OptedNewVehicleModelAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
            binding.spinnerOptedNewVehicleModel.setAdapter(OptedNewVehicleModelAdapter);

            if (SessionUserDetails.getInstance().getUserName().length() != 0) {
                binding.editTextEvaluatorName.setText(SessionUserDetails.getInstance().getUserName());
            }

            Date today = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy ", Locale.ENGLISH);
            String formattedDate = dateFormat.format(today);
            binding.textViewEvaluationDate.setText(formattedDate);

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
            String formattedTime = timeFormat.format(today);
            binding.textViewEvaluationTime.setText(formattedTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayData() {
        SpinnerHelper.setSpinnerValue(arrayListModelOptedNewVehicle, newEvaluation.getOptedNewVehicle(),
                binding.spinnerOptedNewVehicleModel);
        if (null != newEvaluation.getRequestId() && newEvaluation.getRequestId().length() > 0) {
            binding.editTextTransactionNo.setText(newEvaluation.getRequestId());
            newEvaluation.setPurpose("Exchange");
            binding.spinnerAgainstTransaction.setSelection(0);
            binding.spinnerAgainstTransaction.setEnabled(false);
            binding.linearLayoutOptedNewVehicle.setVisibility(View.GONE);
            binding.TextViewOptedNewVehicleModel.setVisibility(View.VISIBLE);
            binding.TextViewOptedNewVehicleModel.setText(newEvaluation.getOptedNewVehicle());
        } else {
            newEvaluation.setPurpose("Direct buy");
            binding.spinnerAgainstTransaction.setSelection(1);
            binding.spinnerAgainstTransaction.setEnabled(false);
            binding.linearLayoutOptedNewVehicle.setVisibility(View.VISIBLE);
            binding.TextViewOptedNewVehicleModel.setVisibility(View.GONE);
        }
        setUpToggleButtons(newEvaluation.getPurpose(),
                new Button[]{binding.buttonExchange,
                        binding.buttonDirectBuy});
        if (!newEvaluation.getDate().isEmpty())
            binding.textViewEvaluationDate.setText(newEvaluation.getDate());
        if (!newEvaluation.getTime().isEmpty())
            binding.textViewEvaluationTime.setText(newEvaluation.getTime());
        binding.editTextTransactionNo.setText(newEvaluation.getRequestId());
    }

    @Override
    public NewEvaluation getData() {
        try {
            newEvaluation.setRequestId(binding.editTextTransactionNo.getText().toString().trim());
            String strAgainstTransaction = binding.spinnerAgainstTransaction.getSelectedItem().toString().trim();
            if (strAgainstTransaction.equalsIgnoreCase("Enquiry")) {
                newEvaluation.setAgainstTransaction("ENQ");
                newEvaluation.setOptedNewVehicle(
                        binding.TextViewOptedNewVehicleModel.getText().toString());
            } else {
                newEvaluation.setAgainstTransaction("NONE");
                newEvaluation.setOptedNewVehicle(
                        SpinnerHelper.getSpinnerCodeFromArrayList(binding.spinnerOptedNewVehicleModel,
                                arrayListModelOptedNewVehicle));
            }
            newEvaluation.setDate(binding.textViewEvaluationDate.getText().toString().trim());
            newEvaluation.setTime(binding.textViewEvaluationTime.getText().toString().trim());
            newEvaluation.setTransactionNo(binding.editTextTransactionNo.getText().toString().trim());
            newEvaluation.setEvaluatorName(binding.editTextEvaluatorName.getText().toString().trim());
            newEvaluation.setSourceOfTransaction("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }

    @Override
    public void saveData() {
        getData();
        evaluationDetailsPresenter.saveData(newEvaluation);
    }

    @Override
    public boolean isDataValid() {
        String strAgainstTransaction = binding.spinnerAgainstTransaction.getSelectedItem().toString().trim();
        if (strAgainstTransaction.equalsIgnoreCase("Enquiry")) {
            newEvaluation.setOptedNewVehicle(
                    binding.TextViewOptedNewVehicleModel.getText().toString());
        } else {
            newEvaluation.setOptedNewVehicle(
                    SpinnerHelper.getSpinnerCodeFromArrayList(binding.spinnerOptedNewVehicleModel,
                            arrayListModelOptedNewVehicle));
        }
        if (newEvaluation.getOptedNewVehicle().contains("Select Model")
                || newEvaluation.getOptedNewVehicle().isEmpty()) {
            return FieldValidator.openViewAndDisplayMessage(context,
                    binding.spinnerOptedNewVehicleModel, "Select Opted New Vehicle Model");
        }
        if (binding.editTextEvaluatorName.getText().toString().length() == 0) {
            return FieldValidator.focusViewAndDisplayMessage(context, binding.editTextEvaluatorName,
                    "Enter evaluator name");
        }
        saveData();
        return true;
    }

    @Override
    public void fillTestDataInForm() {
        if (binding.spinnerOptedNewVehicleModel.getVisibility() == View.VISIBLE) {
            binding.spinnerOptedNewVehicleModel.setSelection(3);
        }
    }

    private void setUpToggleButtons(String value, Button[] btns) {
        int highlightPos = -1, dimPos = -1;
        if ((value.equalsIgnoreCase("No")) ||
                (value.equalsIgnoreCase("N"))) {
            highlightPos = 0;
            dimPos = 1;
        } else if ((value.equalsIgnoreCase("Yes")) ||
                (value.equalsIgnoreCase("Y"))) {
            highlightPos = 1;
            dimPos = 0;
        }

        if (highlightPos != -1 && dimPos != -1) {
            btns[highlightPos].setTextColor(getResources().getColor(R.color.colorblack));
            btns[highlightPos].setBackgroundResource(R.drawable.disable_edit_text_border);

            btns[dimPos].setTextColor(getResources().getColor(R.color.colorWhite));
            btns[dimPos].setBackgroundResource(R.drawable.gray_rounded_bg);
        }
    }
}