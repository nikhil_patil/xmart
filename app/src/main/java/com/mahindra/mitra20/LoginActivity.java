package com.mahindra.mitra20;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mahindra.mitra20.champion.constants.ChampionConstants;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.constants.IntentDataConstants;
import com.mahindra.mitra20.evaluator.views.activity.EvaluatorDashboardActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.module_broker.ui.activities.BrokerDashboardActivity;
import com.mahindra.mitra20.presenters.LoginActivityIn;
import com.mahindra.mitra20.presenters.LoginActivityPresenter;

import org.json.JSONObject;

import io.paperdb.Paper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginActivityIn {

    private EditText editTextUsername, editTextPassword;
    private LoginActivityPresenter mLoginActivityPresenter;
    private Button buttonEvaluator;
    private Button buttonBroker, buttonLogin;
    private ProgressBar progressBar;
    private int currentUser = 0;
    ProgressDialog dialog;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SessionUserDetails sessionUserDetailsSP = Paper.book().read(CommonHelper.PAPER_DB_SESSION_USER_DETAILS);
        sharedPref = getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
        boolean isLoginHelpDisplayed = sharedPref.getBoolean(SharedPreferenceKeys.LOGIN_HELP_DISPLAYED, false);
        if (null != sessionUserDetailsSP) {
            SessionUserDetails.getInstance().assignSharedPrefValues(sessionUserDetailsSP);
            redirectToRespectiveDashboard(SessionUserDetails.getInstance());
        } else {
            setContentView(R.layout.xml_login);
            this.setTheme(R.style.SplashTheme);
            init();
            if (!isLoginHelpDisplayed) {
                findViewById(R.id.cl_overlay_xmart).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_help_next).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        findViewById(R.id.cl_overlay_xmart).setVisibility(View.GONE);
                        findViewById(R.id.cl_overlay_associate).setVisibility(View.VISIBLE);

                        findViewById(R.id.tv_help_done).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                findViewById(R.id.cl_overlay_associate).setVisibility(View.GONE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean(SharedPreferenceKeys.LOGIN_HELP_DISPLAYED, true);
                                editor.apply();
                                CommonHelper.showKeyboard(LoginActivity.this, editTextUsername);
                            }
                        });
                    }
                });
            } else {
                editTextUsername.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CommonHelper.showKeyboard(LoginActivity.this, editTextUsername);
                    }
                }, 100);
            }
        }
    }

    protected void init() {
        mLoginActivityPresenter = new LoginActivityPresenter(this, this);
        progressBar = findViewById(R.id.progressBar2);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);
        mLoginActivityPresenter.setLoginTabsClicks();
        setLoginScreenElementsAccordingToUser(currentUser);
        editTextPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    buttonLogin.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonLogin) {
            String token = sharedPref.getString(SharedPreferenceKeys.FCM_TOKEN, "TEST");
            CommonHelper.hideKeyboard(this);

            if (currentUser == 0) {
                String userName = editTextUsername.getText().toString();
                String userPs = editTextPassword.getText().toString();
                if (userName.isEmpty()) {
                    CommonHelper.toast("Enter Username", this);
                    editTextUsername.requestFocus();
                } else if (userPs.isEmpty()) {
                    CommonHelper.toast("Enter password", this);
                    editTextPassword.requestFocus();
                } else {
                    showProgressDialog(1);
                    mLoginActivityPresenter.loginForChamp(currentUser, userName, userPs,
                            "SALS", token);
                }
            } else if (currentUser == 1) {
                String mitraCode = editTextUsername.getText().toString();
                String mitraMobile = editTextPassword.getText().toString();
                if (mitraCode.isEmpty()) {
                    CommonHelper.toast("Enter Username", this);
                    editTextUsername.requestFocus();
                } else if (mitraMobile.isEmpty()) {
                    CommonHelper.toast("Enter password", this);
                    editTextPassword.requestFocus();
                } else {
                    showProgressDialog(1);
                    mLoginActivityPresenter.loginForMitra(currentUser,
                            mitraCode, mitraMobile, token);
                }
            }
        }
    }

    @Override
    public void onLoginSuccess(SessionUserDetails sessionUserDetails) {
        hideProgressDialog(1);
        Paper.book().write(CommonHelper.PAPER_DB_SESSION_USER_DETAILS, sessionUserDetails);
        progressBar.setVisibility(View.GONE);
        redirectToRespectiveDashboard(sessionUserDetails);
    }

    private void redirectToRespectiveDashboard(SessionUserDetails sessionUserDetails) {
        if (sessionUserDetails.getCurrentUser() == 0) {
            Intent intent = new Intent(LoginActivity.this, ChampionDashboardActivity.class);
            intent.putExtra(ChampionConstants.FROM_LOGIN_SCREEN,true);
            intent.putExtra(IntentDataConstants.KEY_USER_DATA, sessionUserDetails);
            startActivity(intent);
        } else if (sessionUserDetails.getCurrentUser() == 1) {
            Intent intent = new Intent(LoginActivity.this, EvaluatorDashboardActivity.class);
            intent.putExtra(IntentDataConstants.KEY_USER_DATA, sessionUserDetails);
            intent.putExtra(ChampionConstants.FROM_LOGIN_SCREEN,true);
            startActivity(intent);
        } else if (sessionUserDetails.getCurrentUser() == 2) {
            Intent intent = new Intent(this, BrokerDashboardActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onLoginFailure(String response) {
        hideProgressDialog(1);
        progressBar.setVisibility(View.GONE);
        //Toast.makeText(this, response, Toast.LENGTH_LONG).show();
        try {
            JSONObject jsonObject1 = new JSONObject(response);
            String IsSuccessful = jsonObject1.get("IsSuccessful").toString();
            String message = jsonObject1.get("message").toString();

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            if(null != response && response.contains("com.android.volley.NoConnectionError")) {
                Toast.makeText(this, "Please check if your internet is working and try again", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Please Enter valid userId and password", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void settingLoginElementsClicks() {
        buttonEvaluator = findViewById(R.id.buttonEvaluator);
        buttonBroker = findViewById(R.id.buttonBroker);

        buttonEvaluator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoginScreenElementsAccordingToUser(0);
            }
        });

        buttonBroker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoginScreenElementsAccordingToUser(1);
            }
        });
    }

    public void showProgressDialog(int requestId) {
        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Logging in. Please wait");
        dialog.show();
    }

    public void hideProgressDialog(int requestId) {
        dialog.dismiss();
    }

    private void setLoginScreenElementsAccordingToUser(int user) {
        if (user == 0) {
            buttonEvaluator.setTextColor(Color.WHITE);
            buttonBroker.setTextColor(getResources().getColor(R.color.lightRed));
            editTextUsername.setText(getResources().getString(R.string.blank));
            editTextUsername.setHint(getResources().getString(R.string.user_name));
            editTextPassword.setText(getResources().getString(R.string.blank));
            editTextPassword.setHint(getResources().getString(R.string.pass_word));
            currentUser = user;
        } else if (user == 1) {
            buttonEvaluator.setTextColor(getResources().getColor(R.color.lightRed));
            buttonBroker.setTextColor(Color.WHITE);
            editTextUsername.setText(getResources().getString(R.string.blank));
            editTextUsername.setHint(getResources().getString(R.string.mitra_code));
            editTextPassword.setText(getResources().getString(R.string.blank));
            editTextPassword.setHint(getResources().getString(R.string.mitra_mobile));
            currentUser = user;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}