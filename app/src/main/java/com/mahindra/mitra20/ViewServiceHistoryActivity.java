package com.mahindra.mitra20;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.interfaces.ViewServiceHistoryIn;
import com.mahindra.mitra20.models.RoDetail;
import com.mahindra.mitra20.models.ServiceHistory;
import com.mahindra.mitra20.models.ServiceHistoryStats;
import com.mahindra.mitra20.presenters.ViewServiceHistoryPresenter;

public class ViewServiceHistoryActivity extends AppCompatActivity implements ViewServiceHistoryIn {

    ViewServiceHistoryPresenter viewServiceHistoryPresenter;
    String chassisNo, regNo;
    ProgressBar progressBar;
    LinearLayout llMain, llSubmit, llError;
    EditText editTextChassisNo, editTextRegNo;
    TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_service_history);

        chassisNo = getIntent().getStringExtra("chassis_no");
        regNo = getIntent().getStringExtra("reg_no");
//        chassisNo = "G6D11475";
        init();
        viewServiceHistoryPresenter = new ViewServiceHistoryPresenter(this);
        viewServiceHistoryPresenter.getServiceHistory(chassisNo, regNo);
    }

    public void init() {
        CommonHelper.settingCustomToolBarEvaluator(this,
                "Service History", View.INVISIBLE);
        progressBar = findViewById(R.id.progress_bar);
        llMain = findViewById(R.id.ll_main);
        llSubmit = findViewById(R.id.llSubmit);
        llError = findViewById(R.id.ll_error);
        tvError = findViewById(R.id.tv_error);
        editTextChassisNo = findViewById(R.id.editTextChassisNo);
        if (null != chassisNo) editTextChassisNo.setText(chassisNo);
        editTextRegNo = findViewById(R.id.editTextRegNo);
        if (null != regNo) editTextRegNo.setText(regNo);
        llSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextChassisNo.getText().toString().isEmpty()
                        && editTextRegNo.getText().toString().isEmpty()) {
                    CommonHelper.toast("Enter either Chassis number or Registration number",
                            ViewServiceHistoryActivity.this);
                } else {
                    chassisNo = editTextChassisNo.getText().toString().trim();
                    regNo = editTextRegNo.getText().toString().trim();
                    viewServiceHistoryPresenter.getServiceHistory(chassisNo, regNo);
                    llMain.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    llError.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onServiceHistory(ServiceHistory serviceHistory) {
        llMain.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        llError.setVisibility(View.GONE);
        populateData(serviceHistory);
    }

    private void populateData(ServiceHistory serviceHistory) {
        try {
            TextView textViewCustomerName = findViewById(R.id.textViewCustomerName);
            TextView textViewRegNo = findViewById(R.id.textViewRegNo);
            TextView textViewModel = findViewById(R.id.textViewModel);
            TextView textViewModelFamily = findViewById(R.id.textViewModelFamily);
            TextView textViewDos = findViewById(R.id.textViewDos);
            TextView textViewSerialNo = findViewById(R.id.textViewSerialNo);
            TextView textViewAvgRun = findViewById(R.id.textViewAvgRun);
            TextView textViewLastRepKms = findViewById(R.id.textViewLastRepKms);
            TextView textViewRsPerKm = findViewById(R.id.textViewRsPerKm);
            TextView textViewMTV = findViewById(R.id.textViewMTV);

            TextView scheduleCount = findViewById(R.id.scheduleCount);
            TextView scheduleAmount = findViewById(R.id.scheduleAmount);
            TextView rrCount = findViewById(R.id.rrCount);
            TextView rrAmount = findViewById(R.id.rrAmount);
            TextView bodyshopCount = findViewById(R.id.bodyshopCount);
            TextView bodyshopAmount = findViewById(R.id.bodyshopAmount);
            TextView saCount = findViewById(R.id.saCount);
            TextView saAmount = findViewById(R.id.saAmount);
            TextView presaleCount = findViewById(R.id.presaleCount);
            TextView presaleAmount = findViewById(R.id.presaleAmount);
            TextView salesCount = findViewById(R.id.salesCount);
            TextView salesAmount = findViewById(R.id.salesAmount);
            TextView otherCount = findViewById(R.id.otherCount);
            TextView otherAmount = findViewById(R.id.otherAmount);
            TextView totalCount = findViewById(R.id.totalCount);
            TextView totalAmount = findViewById(R.id.totalAmount);
            LinearLayout llRoDetails = findViewById(R.id.llRoDetails);

            textViewCustomerName.setText(serviceHistory.getCustomerName());
            textViewRegNo.setText(serviceHistory.getRegistrationNo());
            textViewModel.setText(serviceHistory.getModel());
            textViewModelFamily.setText(serviceHistory.getModelFamily());
            textViewDos.setText(serviceHistory.getSoldDate());
            textViewSerialNo.setText(serviceHistory.getChassisNo());

            ServiceHistoryStats serviceHistoryStats = viewServiceHistoryPresenter.calculateAvgRunDay(
                    serviceHistory.getRoDetail(), serviceHistory.getSoldDate());

            textViewAvgRun.setText(String.valueOf(serviceHistoryStats.getAverageRun()));
            textViewLastRepKms.setText(String.valueOf(serviceHistoryStats.getLastReportedKms()));
            textViewRsPerKm.setText(String.valueOf(serviceHistoryStats.getServiceTypeRsPerKm()));
            textViewMTV.setText(String.valueOf(serviceHistoryStats.getServiceTypeMTV()));

            scheduleCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeScheduleCount()));
            scheduleAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeScheduleTotalAmount()));
            rrCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeRRCount()));
            rrAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeRRTotalAmount()));
            bodyshopCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeACCCount()));
            bodyshopAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeACCTotalAmount()));
            saCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeSVCCount()));
            saAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeSVCTotalAmount()));
            presaleCount.setText(String.valueOf(serviceHistoryStats.getServiceTypePRSLCount()));
            presaleAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypePRSLTotalAmount()));
            salesCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeSACCount()));
            salesAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeSACTotalAmount()));
            otherCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeOthersCount()));
            otherAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeOthersTotalAmount()));
            totalCount.setText(String.valueOf(serviceHistoryStats.getServiceTypeAllCount()));
            totalAmount.setText(String.valueOf(serviceHistoryStats.getServiceTypeAllTotalAmount()));

            llRoDetails.removeAllViews();
            for (int i = 0; i < serviceHistory.getRoDetail().size(); i++) {
                RoDetail roDetail = serviceHistory.getRoDetail().get(i);
                View view = getLayoutInflater().inflate(R.layout.ro_details_item, null);
                TextView tvColumnOne = view.findViewById(R.id.tvColumnOne);
                TextView tvColumnTwo = view.findViewById(R.id.tvColumnTwo);
                TextView tvColumnThree = view.findViewById(R.id.tvColumnThree);

                tvColumnOne.setText(String.format("%s\n\n%s\n\n%s", roDetail.getRoNumber(),
                        roDetail.getRoDate(), roDetail.getServiceType()));
                tvColumnTwo.setText(String.format("%s\n\n%s\n\n%s", roDetail.getKm(),
                        roDetail.getDealerName(), roDetail.getBranchName()));
                tvColumnThree.setText(String.format("%s\n\n%s\n\n%s", roDetail.getTotalAmount(),
                        roDetail.getPartAmount(), roDetail.getLaborAmount()));

                llRoDetails.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDownloadFail(String msg) {
        progressBar.setVisibility(View.GONE);
        llMain.setVisibility(View.GONE);
        tvError.setText(msg);
        llError.setVisibility(View.VISIBLE);
    }
}