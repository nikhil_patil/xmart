package com.mahindra.mitra20.module_broker.interfaces;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public interface AuctionsTabInteractor {
    ArrayList<SingleAuction> auctionList(int pageNum);
    int getAuctionsActiveItemIndex();
    int getAuctionsActivePageNum();
}
