package com.mahindra.mitra20.module_broker.ui.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.Activity_Profile_Pic;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.champion.helper.LoginHelper;
import com.mahindra.mitra20.champion.helper.UpdateAppHelper;
import com.mahindra.mitra20.constants.AuctionType;
import com.mahindra.mitra20.constants.GeneralConstants;
import com.mahindra.mitra20.evaluator.views.activity.NotificationsActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.helper.MasterSyncHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.constants.SharedPreferenceKeys;
import com.mahindra.mitra20.module_broker.helper.CustomSwipeToRefresh;
import com.mahindra.mitra20.module_broker.helper.MastersHelper;
import com.mahindra.mitra20.module_broker.interfaces.AuctionActivityJobs;
import com.mahindra.mitra20.module_broker.interfaces.AuctionsTabInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.DashBoardLead;
import com.mahindra.mitra20.module_broker.models.Dealer;
import com.mahindra.mitra20.module_broker.models.Parsers;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.LiveAuctionIP;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;
import com.mahindra.mitra20.module_broker.presenters.BrokerDashBoardIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerDashBoardPresenter;
import com.mahindra.mitra20.module_broker.ui.adapters.ViewPager.BrokerAuctionsDashBoardViewPagerAdapter;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment2;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment3;
import com.mahindra.mitra20.sqlite.BrokerDbHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;
import com.mahindra.mitra20.utils.AuctionIntervalUtils;
import com.mahindra.mitra20.utils.ForceUpdateChecker;
import com.mahindra.mitra20.utils.NetworkUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class BrokerDashboardActivity extends AppCompatActivity
        implements BrokerDashBoardIn, BidNowInteractor, View.OnClickListener,
        AuctionsTabInteractor, AuctionActivityJobs, ForceUpdateChecker.OnUpdateNeededListener {

    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int BID_NOW_CODE = 500;
    private BrokerDashBoardPresenter mitraDashBoardPresenter;
    private Button newLeadsBtn;
    private TextView auctionsViewAllBtn, logoutBtn, leadsViewAllBtn;
    private ViewGroup auctionsFull, leadsFull, leadsHeadingLayout;
    private AllLeadsData allLeadsData;
    private ArrayList<DashBoardLead> leadsViewList;
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private TabLayout dashBoardAuctionsTabLayout;
    private ProgressBar progressBar;
    private ViewPager dashBoardAuctionsViewPager;
    private ImageView ivDashboardProfile, ivProfile, imageViewMenu;
    private CustomSwipeToRefresh swipeRefreshLayout;
    private TableRow tableRowDashboard, tableRowAuctions, tableRowMyLeads, tableRowSyncMaster;
    private String mitraUserId;
    private int currentActiveAuctionPage;
    private ArrayList<SingleAuction> auctionListLive, auctionListBidded, auctionListClosed;
    private int[] auctionsTabList = new int[]{0, 0, 0};
    private Context context;
    private DrawerLayout drawer;
    private SharedPreferences sharedPref;
    private AuctionIntervalUtils intervalUtils = new AuctionIntervalUtils();
    ;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (CommonHelper.isConnectingToInternet(getBaseContext()) && areMastersSynced()) {
                fetchAuctionsData();
                fetchLeadsData();
            }
        }
    };

    private void displayUnreadNotificationIcon() {
        BrokerDbHelper brokerDbHelper = new BrokerDbHelper(this);
        if (brokerDbHelper.checkUnreadNotifications()) {
            findViewById(R.id.iv_notification_available).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.iv_notification_available).setVisibility(View.GONE);
        }
    }

    private void showHelp() {
        try {
            openDrawer();
            findViewById(R.id.cl_overlay_sync).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_sync_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeDrawer();
                    findViewById(R.id.cl_overlay_sync).setVisibility(View.GONE);
                    findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.VISIBLE);

                    findViewById(R.id.tv_pull_to_refresh_next).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            findViewById(R.id.cl_overlay_pull_to_refresh).setVisibility(View.GONE);
                            findViewById(R.id.cl_overlay_notification).setVisibility(View.VISIBLE);

                            findViewById(R.id.tv_notification_done).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    findViewById(R.id.cl_overlay_notification).setVisibility(View.GONE);

                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putBoolean(
                                            SharedPreferenceKeys.BROKER_DASHBOARD_HELP_DISPLAYED, true);
                                    editor.apply();
                                }
                            });
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void VisualElementsReady() {
        TextView userTitleTv = findViewById(R.id.broker_dash_user_title);
        TextView companyTitleTv = findViewById(R.id.broker_dash_company_tv);
        auctionsFull = findViewById(R.id.auctions_full);
        leadsFull = findViewById(R.id.leads_full_in);
        ViewGroup auctionDashBoardVg = findViewById(R.id.blank);
        auctionDashBoardVg.setClickable(true);
        newLeadsBtn = findViewById(R.id.new_leads_btn);
        leadsHeadingLayout = findViewById(R.id.leadHeadingIn);
        leadsViewAllBtn = leadsHeadingLayout.findViewById(R.id.viewAllBtn);

        mitraDashBoardPresenter.setAuctionsSection();
        mitraDashBoardPresenter.setLeadsSection();
        mitraDashBoardPresenter.setClickEvents();

        userTitleTv.setText(SessionUserDetails.getInstance().getMitraName());
        companyTitleTv.setText(SessionUserDetails.getInstance().getParentName());

        setUpSwipeRefreshContainer();

        findViewById(R.id.imageViewNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNotif = new Intent(BrokerDashboardActivity.this,
                        NotificationsActivity.class);
                startActivityForResult(intentNotif, 300);
            }
        });

        fetchAuctionsData();
        fetchLeadsData();
        mitraDashBoardPresenter.getSmallDataForDealerMaster();

        adjustScrollView();
    }

    private void adjustScrollView() {
        final ScrollView scrollView = findViewById(R.id.dash_board_sv);
        ViewTreeObserver.OnScrollChangedListener listener;

        listener = new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();

                if (scrollY < 50) { //threshold
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        };

        scrollView.getViewTreeObserver().addOnScrollChangedListener(listener);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broker_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        evaluatorHelper = new EvaluatorHelper(getBaseContext());
        masterDatabaseHelper = new MasterDatabaseHelper(getBaseContext());
        DrawerLayout drawer = findViewById(R.id.mitra_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        TextView userName = drawer.findViewById(R.id.profile_name);
        userName.setText(SessionUserDetails.getInstance().getMitraName());

        ivProfile = drawer.findViewById(R.id.iv_profile);
        ivDashboardProfile = drawer.findViewById(R.id.iv_dashboard_profile);
        String strProfilePic = SessionUserDetails.getInstance().getUserProfilePic();
        if (null != strProfilePic) {
            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(
                            SessionUserDetails.getInstance().getUserProfilePic()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            ivDashboardProfile.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target, DataSource dataSource,
                                                       boolean isFirstResource) {
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(ivDashboardProfile);
            Glide.with(this)
                    .load(CommonHelper.getAuthenticatedUrlForGlide(
                            SessionUserDetails.getInstance().getUserProfilePic()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            ivProfile.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model,
                                                       Target<Drawable> target, DataSource dataSource,
                                                       boolean isFirstResource) {
                            return false;
                        }
                    })
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .apply(CommonHelper.getGlideProfileErrorImage())
                    .into(ivProfile);
        }

        ivDashboardProfile.setOnClickListener(this);

        tableRowDashboard = navigationView.findViewById(R.id.tableRowDshboard);
        tableRowAuctions = navigationView.findViewById(R.id.tableRowAuctions);
        tableRowMyLeads = navigationView.findViewById(R.id.tableRowMyLeads);
        logoutBtn = navigationView.findViewById(R.id.mitra_logout_btn);
        tableRowSyncMaster = navigationView.findViewById(R.id.tableRowSyncMasters);
        findViewById(R.id.imageViewNewEvaluation).setVisibility(View.GONE);

        init();

        sharedPref = getSharedPreferences(SharedPreferenceKeys.MY_PREFS, Context.MODE_PRIVATE);
        boolean isLoginHelpDisplayed = sharedPref.getBoolean(
                SharedPreferenceKeys.BROKER_DASHBOARD_HELP_DISPLAYED, false);
        if (!isLoginHelpDisplayed) {
            showHelp();
        }
        displayUnreadNotificationIcon();

        //for listening notifications
        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                    new IntentFilter(GeneralConstants.BROADCAST_RECEIVER_KEY));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (NetworkUtilities.isNetworkConnected(this))
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check();

    }

    private void setUpSwipeRefreshContainer() {
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (areMastersSynced()) {
                    fetchAuctionsData();
                    fetchLeadsData();
                }
            }
        });
    }

    void fetchAuctionsData() {
        if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.LIVE))
            fetchLiveAuctionsData("LIV");
        if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.PENDING))
            fetchLiveAuctionsData("PEN");
        if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.COMPLETED))
            fetchLiveAuctionsData("COM");
    }

    private void fetchLeadsData() {
        mitraDashBoardPresenter.getLeadsData();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private void fetchLiveAuctionsData(String type) {
        int auctionPage = 0;
        showProgressBar();
        mitraUserId = SessionUserDetails.getInstance().getMitraCode();
        LiveAuctionIP liveAuctionIP = new LiveAuctionIP();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String toDate = sdf.format(new Date());
        switch (type) {
            case "LIV":
                auctionPage = 0;
                liveAuctionIP.setFromDate(toDate);
                liveAuctionIP.setCountToDisplay("999");
                break;
            case "PEN":
                type = "LIV";
                auctionPage = 1;
                liveAuctionIP.setFromDate(toDate);
                liveAuctionIP.setCountToDisplay("999");
                break;
            case "COM":
                auctionPage = 2;
                Calendar calNow = Calendar.getInstance();
                calNow.add(Calendar.MONTH, -1);
                Date dateBeforeAMonth = calNow.getTime();
                liveAuctionIP.setFromDate(sdf.format(dateBeforeAMonth));
                liveAuctionIP.setCountToDisplay("10");
                break;
        }

        liveAuctionIP.setToDate(toDate);
        liveAuctionIP.setAuctionStatus(type);
        liveAuctionIP.setMake("");
        liveAuctionIP.setModel("");
        liveAuctionIP.setSorting("DATE");
        liveAuctionIP.setUserId(mitraUserId);
        mitraDashBoardPresenter.getAuctionsData(liveAuctionIP, auctionPage);
    }

    private void updateLeadsSingleRow(final DashBoardLead dashBoardLead) {
        ViewGroup leadsVg = findViewById(dashBoardLead.getRelatedViewId());
        TextView leadsNumTv = leadsVg.findViewById(R.id.tvCount);
        TextView leadsDescTv = leadsVg.findViewById(R.id.tvName);

        leadsNumTv.setText(String.valueOf(dashBoardLead.getLeadsCount()));
        leadsNumTv.setTextColor(dashBoardLead.getTextColor());
        leadsDescTv.setText(dashBoardLead.getDescription());

        leadsVg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mitraDashBoardPresenter.showLeadsSection(dashBoardLead.getId());
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (areMastersSynced()) {
            fetchAuctionsData();
            fetchLeadsData();
        }
    }

    private void setAuctionsSection() {
        auctionsViewAllBtn = auctionsFull.findViewById(R.id.viewAllBtn);
        progressBar = findViewById(R.id.progressBar);
        dashBoardAuctionsTabLayout = findViewById(R.id.auctions_dash_board_tab_layout);
        dashBoardAuctionsViewPager = findViewById(R.id.auctions_dash_board_view_pager);
        BrokerAuctionsDashBoardViewPagerAdapter brokerAuctionsDashBoardViewPagerAdapter =
                new BrokerAuctionsDashBoardViewPagerAdapter(getSupportFragmentManager(), this);
        dashBoardAuctionsViewPager.setOffscreenPageLimit(3);
        dashBoardAuctionsViewPager.setAdapter(brokerAuctionsDashBoardViewPagerAdapter);
        dashBoardAuctionsTabLayout.setupWithViewPager(dashBoardAuctionsViewPager);

        dashBoardAuctionsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentActiveAuctionPage = position;
                updatePagerAndTabsData(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setTabs(int liveCount, int biddedCount, int closedCount) {
        try {
            dashBoardAuctionsTabLayout.getTabAt(0)
                    .setText("Live(" + liveCount + ")");
            dashBoardAuctionsTabLayout.getTabAt(1)
                    .setText("Bidded(" + biddedCount + ")");
            dashBoardAuctionsTabLayout.getTabAt(2)
                    .setText("Closed(" + closedCount + ")");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showingAuctionViewAllScreen() {
        openingAuctionsViewAllScreen();
    }

    private void openingAuctionsViewAllScreen() {
        Intent intent = new Intent(this, ActivityBrokerAuctionsViewAll.class);
        startActivity(intent);
    }

    @Override
    public void showingLeadsSection(int pageNum) {
        showLeadsActivity(pageNum);
    }

    @Override
    public void auctionsSectionReady() {
        setAuctionsSection();
    }

    @Override
    public void showingActivityNewLeads() {
        Intent intent = new Intent(BrokerDashboardActivity.this, ActivityNewLeads.class);
        startActivity(intent);
    }

    @Override
    public void settingLeadsSection() {
        leadsHeadingLayout = findViewById(R.id.leadHeadingIn);
        TextView leadsHeadingTv = leadsHeadingLayout.findViewById(R.id.send_to_auction_heading);
        leadsHeadingTv.setText(R.string.my_leads);

        ViewGroup leadsHotVg = leadsFull.findViewById(R.id.hot_btn);
        ViewGroup leadsColdVg = leadsFull.findViewById(R.id.cold_btn);
        ViewGroup leadsWarmVg = leadsFull.findViewById(R.id.warm_btn);
        ViewGroup leadsOrderedVg = leadsFull.findViewById(R.id.ordered_btn);
        ViewGroup leadsInvoicedVg = leadsFull.findViewById(R.id.invoiced_btn);
        ViewGroup leadsLostVg = leadsFull.findViewById(R.id.lost_btn);
        ViewGroup leadsUnassignedVg = leadsFull.findViewById(R.id.unassigned_leads);

        leadsViewList = new ArrayList<>();

        leadsViewList.add(new DashBoardLead(0, 0, DashBoardLead
                .Types.UNASSIGNED, leadsUnassignedVg.getId(), DashBoardLead.Colors.UNASSIGNED_COLOR));
        leadsViewList.add(new DashBoardLead(1, 0, DashBoardLead
                .Types.HOT, leadsHotVg.getId(), DashBoardLead.Colors.HOT_COLOR));
        leadsViewList.add(new DashBoardLead(2, 0, DashBoardLead
                .Types.COLD, leadsColdVg.getId(), DashBoardLead.Colors.COLD_COLOR));
        leadsViewList.add(new DashBoardLead(3, 0, DashBoardLead
                .Types.WARM, leadsWarmVg.getId(), DashBoardLead.Colors.WARM_COLOR));
        leadsViewList.add(new DashBoardLead(4, 0, DashBoardLead
                .Types.ORDERED, leadsOrderedVg.getId(), DashBoardLead.Colors.ORDERED_COLOR));
        leadsViewList.add(new DashBoardLead(5, 0, DashBoardLead
                .Types.INVOICED, leadsInvoicedVg.getId(), DashBoardLead.Colors.INVOICED_COLOR));
        leadsViewList.add(new DashBoardLead(6, 0, DashBoardLead
                .Types.LOST, leadsLostVg.getId(), DashBoardLead.Colors.LOST_COLOR));

        for (int i = 0; i < leadsViewList.size(); i++) {
            updateLeadsSingleRow(leadsViewList.get(i));
        }
    }

    private void updatePagerAndTabsData(int position) {
        switch (position) {
            case 0:
                if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.LIVE)) {
                    fetchAuctionsData("LIV");
                }
                break;
            case 1:
                if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.PENDING)) {
                    fetchAuctionsData("PEN");
                }
                break;
            case 2:
                if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.COMPLETED)) {
                    fetchAuctionsData("COM");
                }
                break;
        }

    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void gotLeadsData(AllLeadsData allLeadsData) {
        swipeRefreshLayout.setRefreshing(false);
        this.allLeadsData = allLeadsData;
        leadsViewList.get(0).setLeadsCount(allLeadsData.getUnassignedCount());
        leadsViewList.get(1).setLeadsCount(allLeadsData.getHotCount());
        leadsViewList.get(2).setLeadsCount(allLeadsData.getColdCount());
        leadsViewList.get(3).setLeadsCount(allLeadsData.getWarmCount());
        leadsViewList.get(4).setLeadsCount(allLeadsData.getOrderedCount());
        leadsViewList.get(5).setLeadsCount(allLeadsData.getInvoicedCount());
        leadsViewList.get(6).setLeadsCount(allLeadsData.getLostCount());
        for (int i = 0; i < leadsViewList.size(); i++) {
            updateLeadsSingleRow(leadsViewList.get(i));
        }
    }

    @Override
    public void gotSmallDataForDealerMaster(JSONObject response) {
        Log.e("response", response.toString());
        MastersHelper.dealerMasterList = new ArrayList<>();

        try {
            JSONArray dealerMaster = response.getJSONArray("DELR_MST");
            JSONObject listEntry = (JSONObject) dealerMaster.get(0);
            JSONArray masterRecordArray = listEntry.getJSONArray("MasterRecord");
            MastersHelper.dealerMasterList = new ArrayList<>();
            for (int i = 0; i < masterRecordArray.length(); i++) {

                Dealer dealer = new Dealer();
                JSONObject jsonObject = masterRecordArray.getJSONObject(i);
                String Code = jsonObject.getString("Code");
                String Description = jsonObject.getString("Description");
                String[] lc = Description.split("-");
                dealer.setParentCode(Code);
                dealer.setLocationCode(lc[0]);
                dealer.setLocationName(lc[1]);

                MastersHelper.dealerMasterList.add(dealer);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("DealerMaster", "dealerMaster" + MastersHelper.dealerMasterList.size());
    }

    @Override
    public void gotNoAuctionsData(int requestId, String response) {
        swipeRefreshLayout.setRefreshing(false);
        if (requestId == 0) {
            auctionListLive = Parsers.parseAuctionsDataByType(new JSONObject(), 0);
            auctionsTabList[0] = auctionListLive.size();
            BrokerAuctionDashBoardSinglePageFragment.getInstance().update(
                    getAuctionList(0));
        } else if (requestId == 1) {
            auctionListBidded = Parsers.parseAuctionsDataByType(new JSONObject(), 1);
            auctionsTabList[1] = auctionListBidded.size();
            BrokerAuctionDashBoardSinglePageFragment2.getInstance().update(
                    getAuctionList(1));
        } else if (requestId == 2) {
            auctionListClosed = Parsers.parseAuctionsData(new JSONObject());
            auctionsTabList[2] = auctionListClosed.size();
            BrokerAuctionDashBoardSinglePageFragment3.getInstance().update(
                    getAuctionList(2));
        }
    }

    @Override
    public void hideProgressBar(ProgressBar progressBar) {
    }

    @Override
    public void showProgressBar(ProgressBar progressBar) {
    }

    @Override
    public void setTabs(int page, int auctionsCount) {
        auctionsTabList[page] = auctionsCount;
        setTabs(auctionsTabList[0], auctionsTabList[1], auctionsTabList[2]);
    }

    @Override
    public void theseItemsAreClickable() {
        tableRowDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
            }
        });

        tableRowAuctions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMastersSynced()) {
                    closeDrawer();
                    openingAuctionsViewAllScreen();
                }
            }
        });

        tableRowMyLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMastersSynced()) {
                    closeDrawer();
                    showLeadsActivity(0);
                }
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginHelper.logoutUser(BrokerDashboardActivity.this);
            }
        });

        tableRowSyncMaster = findViewById(R.id.tableRowSyncMasters);
        tableRowSyncMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonHelper.isConnectingToInternet(context)) {
                    masterDatabaseHelper.truncateMasterTable();
                    new MasterSyncHelper(BrokerDashboardActivity.this).syncMasters();
                } else {
                    CommonHelper.toast(R.string.internet_error,
                            BrokerDashboardActivity.this);
                }
                closeDrawer();
            }
        });

        auctionsViewAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMastersSynced()) {
                    mitraDashBoardPresenter.showAuctionsViewAllScreen();
                }
            }
        });

        newLeadsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMastersSynced()) {
                    mitraDashBoardPresenter.showNewLeadsActivity();
                }
            }
        });

        leadsHeadingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BrokerDashboardActivity.this,
                        BrokerLeadsActivity.class);
                intent.putExtra(ExtrasKeys.LeadsViewAll.DEFAULT_PAGE, 0);
                intent.putExtra(ExtrasKeys.LeadsViewAll.LEADS_DATA, allLeadsData);
                startActivity(intent);
            }
        });

        leadsViewAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mitraDashBoardPresenter.showLeadsSection(0);
            }
        });

        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });
    }

    private void showLeadsActivity(int page) {
        Intent intent = new Intent(BrokerDashboardActivity.this,
                BrokerLeadsActivity.class);
        intent.putExtra(ExtrasKeys.LeadsViewAll.DEFAULT_PAGE, page);
        intent.putExtra(ExtrasKeys.LeadsViewAll.LEADS_DATA, allLeadsData);
        startActivity(intent);
    }

    private void init() {
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE,
                    READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE);
        }
        drawer = findViewById(R.id.mitra_drawer_layout);
        imageViewMenu = drawer.findViewById(R.id.imageViewMenu);
        imageViewMenu.setOnClickListener(this);

        this.mitraDashBoardPresenter = new BrokerDashBoardPresenter(this, this);
        mitraDashBoardPresenter.setViews();
    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onBackPressed() {
        closeDrawer();
    }

    private void openDrawer() {
        if (null != drawer && !drawer.isDrawerOpen(Gravity.START)) {
            drawer.openDrawer(Gravity.START);
        }
    }

    private void closeDrawer() {
        DrawerLayout drawer = findViewById(R.id.mitra_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).setNegativeButton("No", null).show();
        }
    }

    private void callBidNowActivity(SingleAuction singleAuction) {
        if (areMastersSynced()) {
            Intent intent = new Intent(this, ActivityBrokerBidNow.class);
            intent.putExtra(ExtrasKeys.General.AUCTION_SINGLE_DATA, singleAuction);
            startActivityForResult(intent, BID_NOW_CODE);
        }
    }

    private boolean areMastersSynced() {
        if (masterDatabaseHelper.isMasterSync()) {
            return true;
        } else {
            CommonHelper.toast("Please sync masters...", getApplicationContext());
            openDrawer();
            if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_dashboard_profile) {
            startActivity(new Intent(BrokerDashboardActivity.this,
                    Activity_Profile_Pic.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    @Override
    public void clickedOnBidNow(SingleAuction pos) {
        try {
            callBidNowActivity(pos);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<SingleAuction> auctionList(int pageNum) {
        if (pageNum == 0) {
            return auctionListLive;
        } else if (pageNum == 1) {
            return auctionListBidded;
        } else {
            return auctionListClosed;
        }
    }

    @Override
    public int getAuctionsActiveItemIndex() {
        return currentActiveAuctionPage;
    }

    @Override
    public int getAuctionsActivePageNum() {
        return currentActiveAuctionPage;
    }

    @Override
    public void fetchAuctionsData(String type) {
        fetchLiveAuctionsData(type);
    }

    @Override
    public ArrayList<SingleAuction> getAuctionList(int auctionPage) {
        if (auctionPage == 0) {
            return auctionListLive;
        } else if (auctionPage == 1) {
            return auctionListBidded;
        } else if (auctionPage == 2) {
            return auctionListClosed;
        }
        return auctionListLive;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BID_NOW_CODE:
                fetchAuctionsData();
            case 300:
                displayUnreadNotificationIcon();
        }
    }

    @Override
    public void gotAuctionsData(int auctionPage, JSONObject response) {
        swipeRefreshLayout.setRefreshing(false);
        hideProgressBar();
        if (auctionPage == 0) {
            auctionListLive = Parsers.parseAuctionsDataByType(response, 0);
            auctionsTabList[0] = auctionListLive.size();
            BrokerAuctionDashBoardSinglePageFragment.getInstance().update(
                    auctionListLive);
        } else if (auctionPage == 1) {
            auctionListBidded = Parsers.parseAuctionsDataByType(response, 1);
            auctionsTabList[1] = auctionListBidded.size();
            BrokerAuctionDashBoardSinglePageFragment2.getInstance().update(
                    auctionListBidded);
        } else if (auctionPage == 2) {
            auctionListClosed = Parsers.parseAuctionsData(response);
            auctionsTabList[2] = auctionListClosed.size();
            BrokerAuctionDashBoardSinglePageFragment3.getInstance().update(
                    auctionListClosed);
        }
        setTabs(auctionsTabList[0], auctionsTabList[1], auctionsTabList[2]);
    }

    @Override
    public void onUpdateNeeded(String updateUrl, boolean isMajorUpdate) {
        new UpdateAppHelper(this, updateUrl, isMajorUpdate).showUpdateDialog();
    }

    @Override
    public void updateNotFound() {

    }


}