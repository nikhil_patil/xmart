package com.mahindra.mitra20.module_broker.ui.adapters.ViewPager;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.models.BrokerAuctionPages;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment2;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionDashBoardSinglePageFragment3;

import java.util.ArrayList;

public class BrokerAuctionsDashBoardViewPagerAdapter extends FragmentStatePagerAdapter {
    public ArrayList<BrokerAuctionPages> brokerAuctionPagesList;
    private BidNowInteractor bidNowInteractor ;

    public BrokerAuctionsDashBoardViewPagerAdapter(FragmentManager fm, BidNowInteractor bidNowInteractor) {
        super(fm);
        this.bidNowInteractor = bidNowInteractor;
        brokerAuctionPagesList = new ArrayList<>();
        brokerAuctionPagesList.add(new BrokerAuctionPages("Live",0 ));
        brokerAuctionPagesList.add(new BrokerAuctionPages("Bidded",1 ));
        brokerAuctionPagesList.add(new BrokerAuctionPages("Closed",2 ));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return BrokerAuctionDashBoardSinglePageFragment.getInstance(bidNowInteractor);
            case 1: return BrokerAuctionDashBoardSinglePageFragment2.getInstance(bidNowInteractor);
            case 2: return BrokerAuctionDashBoardSinglePageFragment3.getInstance(bidNowInteractor);
        }
        return null;
    }

    @Override
    public int getCount() {
        return brokerAuctionPagesList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return brokerAuctionPagesList.get(position).getPageTitle();
    }
}