package com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.helper.StringHelper;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public class AuctionRecyclerViewDashBoardAdapter extends
        RecyclerView.Adapter<AuctionRecyclerViewDashBoardAdapter.AuctionViewHolder> {

    public ArrayList<SingleAuction> auctionArrayList;
    private BidNowInteractor interactor;
    public int AUCTION_TYPE = 0;

    public AuctionRecyclerViewDashBoardAdapter(BidNowInteractor interactor, ArrayList<SingleAuction> auctionArrayList) {
        this.auctionArrayList = auctionArrayList;
        this.interactor = interactor;
    }

    @NonNull
    @Override
    public AuctionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_mitra_auctions_single_dashboard, parent, false);
        return new AuctionViewHolder(v);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull final AuctionViewHolder holder, int position) {
        SingleAuction auction = auctionArrayList.get(position);
        int seconds = Integer.parseInt(auction.getRemainingTimeInSeconds());
        int milli = seconds * 1000;
        holder.vehicleNameTv.setText(auction.getVehicleName());
        holder.totalBidsCountTv.setText(String.valueOf(auction.getCountOfBidders()));
        holder.highestBidValueTv.setText("");

        if (AUCTION_TYPE == 0) {
            holder.descr_cl_live.setVisibility(View.VISIBLE);
            holder.descr_cl_bidded.setVisibility(View.INVISIBLE);
            holder.descr_cl_closed.setVisibility(View.INVISIBLE);
            holder.isHighestBidderTv.setVisibility(View.GONE);
            holder.isHighestBidderTv2.setVisibility(View.GONE);
        } else if (AUCTION_TYPE == 1) {
            holder.descr_cl_live.setVisibility(View.INVISIBLE);
            holder.descr_cl_bidded.setVisibility(View.VISIBLE);
            holder.descr_cl_closed.setVisibility(View.INVISIBLE);
            holder.isHighestBidderTv2.setText("highest bidder");

            if (auction.isHasWon()) {
                holder.isHighestBidderTv.setText("You are the");
                Glide.with(holder.arrow.getContext())
                        .load(R.mipmap.download_arrow_green)
                        .into(holder.arrow);
            } else {
                holder.isHighestBidderTv.setText("You are not");
                Glide.with(holder.arrow.getContext())
                        .load(R.mipmap.download_arrow)
                        .into(holder.arrow);
            }
            holder.myTimer.setVisibility(View.VISIBLE);
            holder.yourPriceTv.setText("Rs " + auction.getBidderPriceStr());

            if (auction.HasMitraBidded()) {
                holder.bidNowBtn.setText("Rebid");
            } else {
                holder.bidNowBtn.setText("Bid Now");
            }
        } else if (AUCTION_TYPE == 2) {
            holder.descr_cl_live.setVisibility(View.INVISIBLE);
            holder.descr_cl_bidded.setVisibility(View.INVISIBLE);
            holder.descr_cl_closed.setVisibility(View.VISIBLE);
            holder.bidNowBtn.setVisibility(View.INVISIBLE);
            boolean bidderNotBidded = true;
            if (null == auction.getBidderPriceStr())
                holder.biddersPriceTv.setText("Not bidded");
            else if (auction.getBidderPriceStr().equalsIgnoreCase(""))
                holder.biddersPriceTv.setText("Not bidded");
            else {
                holder.biddersPriceTv.setText(String.format("Rs %s", auction.getBidderPriceStr()));
                bidderNotBidded = false;
            }

            if (!bidderNotBidded) {
                if (auction.getHighestBid() <= 0)
                    holder.highestBidValueTv.setText("Not bidded");
                else
                    holder.highestBidValueTv.setText(String.format("Rs %s", auction.getHighestBidStr()));
            } else {
                holder.highestBidValueTv.setText("NA");
            }
        }

        long biddedPriceLng = 0;
        long highestPriceLng = 0;
        if (auction.getBidderPriceStr() != null &&
                !auction.getBidderPriceStr().equalsIgnoreCase("")) {
            holder.biddersPriceTv.setText(String.format("Rs %s", auction.getBidderPriceStr()));
            biddedPriceLng = Long.parseLong(auction.getBidderPriceStr());
        }

        if (auction.getBidderPriceStr() != null &&
                !auction.getBidderPriceStr().equalsIgnoreCase("")) {
            biddedPriceLng = Long.parseLong(auction.getBidderPriceStr());
        }

        if (auction.getHighestBidStr() != null &&
                !auction.getHighestBidStr().equalsIgnoreCase("")) {
            highestPriceLng = Long.parseLong(auction.getHighestBidStr());
        }

        if((highestPriceLng > 0 && biddedPriceLng > 0 && biddedPriceLng >= highestPriceLng)){
            holder.highestBidValueTv.setText(String.format("Rs %d", biddedPriceLng));
        }

        String builder = auction.getMake() + " " + auction.getModel();
        holder.vehicleSpecsTv.setText(builder);
        holder.textViewVehicleUsage.setText(String.format("Vehicle Usage - %s",
                auction.getVehicleUsage()));

        if (null != auction.getImageUrl()) {
            if (!auction.getImageUrl().isEmpty()) {
                Glide.with(holder.vehicleIV.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(auction.getImageUrl()))
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.vehicleIV);
            } else {
                Glide.with(holder.vehicleIV.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.vehicleIV);
            }
        }

        if (holder.countDownTimer != null) {
            holder.countDownTimer.cancel();
        }
        holder.countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                String time = StringHelper.getDoubleDigit(String.valueOf(hours % 24)) + ":" +
                        StringHelper.getDoubleDigit(String.valueOf(((minutes % 60) > 9)
                                ? minutes % 60 : "0" + (minutes % 60)) + ":" +
                                StringHelper.getDoubleDigit(String.valueOf(seconds % 60)));
                holder.timeTv.setText(time);
                holder.myTimer.setText(time);
            }

            @Override
            public void onFinish() {
                holder.countDownTimer.cancel();
            }
        };
        holder.countDownTimer.start();
    }

    @Override
    public int getItemCount() {
        if (auctionArrayList != null) {
            return auctionArrayList.size();
        }
        return 0;
    }

    class AuctionViewHolder extends RecyclerView.ViewHolder {
        private ImageView vehicleIV;
        private TextView timeTv;
        private TextView bidNowBtn;
        private TextView vehicleNameTv;
        private TextView vehicleSpecsTv;
        private TextView highestBidValueTv;
        private TextView totalBidsCountTv;
        private ViewGroup descr_cl_live;
        private ViewGroup descr_cl_bidded;
        private ViewGroup descr_cl_closed;
        private TextView myTimer;
        private TextView biddersPriceTv;
        private TextView isHighestBidderTv, isHighestBidderTv2;
        private TextView yourPriceTv;
        private TextView textViewVehicleUsage;
        private ImageView arrow;
        private CountDownTimer countDownTimer;

        AuctionViewHolder(View itemView) {
            super(itemView);
            vehicleIV = itemView.findViewById(R.id.imageView17);
            timeTv = itemView.findViewById(R.id.time_remaining_value);
            bidNowBtn = itemView.findViewById(R.id.bid_now_btn);
            vehicleNameTv = itemView.findViewById(R.id.vehicle_name_tv);
            vehicleSpecsTv = itemView.findViewById(R.id.vehicle_specs_tv);
            myTimer = itemView.findViewById(R.id.timer_tv_2);
            myTimer.setVisibility(View.GONE);
            highestBidValueTv = itemView.findViewById(R.id.highest_bid_tv2);
            descr_cl_live = itemView.findViewById(R.id.descr_cl_live);
            descr_cl_bidded = itemView.findViewById(R.id.descr_cl_bidded);
            descr_cl_closed = itemView.findViewById(R.id.descr_cl_closed);

            biddersPriceTv = itemView.findViewById(R.id.your_price_tv_2);
            isHighestBidderTv = itemView.findViewById(R.id.isHighest_bidder_tv);
            yourPriceTv = itemView.findViewById(R.id.your_pricr_tv);
            isHighestBidderTv2 = itemView.findViewById(R.id.isHighest_bidder_tv2);

            arrow = itemView.findViewById(R.id.down_arrow);
            textViewVehicleUsage = itemView.findViewById(R.id.textViewVehicleUsage);

            totalBidsCountTv = itemView.findViewById(R.id.total_bid_count);
            bidNowBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interactor.clickedOnBidNow(auctionArrayList.get(getAdapterPosition()));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interactor.clickedOnBidNow(auctionArrayList.get(getAdapterPosition()));
                }
            });
        }
    }
}