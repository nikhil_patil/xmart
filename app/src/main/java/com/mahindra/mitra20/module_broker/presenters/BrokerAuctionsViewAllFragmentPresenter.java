package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

public class BrokerAuctionsViewAllFragmentPresenter{
    private BrokerAuctionsViewAllFragmentIn brokerAuctionsViewAllFragmentView;
    private Context context;

    public BrokerAuctionsViewAllFragmentPresenter(Context context, BrokerAuctionsViewAllFragmentIn brokerLeadsViewAllView){
        this.brokerAuctionsViewAllFragmentView = brokerLeadsViewAllView;
        this.context = context;
    }

    public void setViews(){
        brokerAuctionsViewAllFragmentView.VisualElementsReady();
    }



}
