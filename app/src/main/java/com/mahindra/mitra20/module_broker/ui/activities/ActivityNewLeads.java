package com.mahindra.mitra20.module_broker.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.models.MasterModel;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.models.Dealer;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.NewLeadIN;
import com.mahindra.mitra20.module_broker.presenters.BrokerNewLeadIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerNewLeadPresenter;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;
import com.mahindra.mitra20.sqlite.MasterDatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ActivityNewLeads extends AppCompatActivity implements BrokerNewLeadIn, View.OnClickListener {

    private BrokerNewLeadPresenter brokerNewLeadPresenter;
    private LinearLayout llSubmitButton;
    private AppCompatSpinner parentSpinner;
    private AppCompatSpinner parentSpinnerModel;
    private EditText customerNameTv, mobileNoTv, pinCodeTv, remarkTv, parentNameTv;
    private ArrayList<Dealer> locationList = SessionUserDetails.getInstance().getLocationList();
    private ArrayList<String> modelGroupCdList;
    private ArrayList<String> modelGroupDescList;
    private ArrayList<String> combinedModelList;
    private String locationCodeSelected;
    private ArrayList<MasterModel> arrayListMake = new ArrayList<>();
    private ArrayList<MasterModel> arrayListModel = new ArrayList<>();
    private EvaluatorHelper evaluatorHelper;
    private MasterDatabaseHelper masterDatabaseHelper;
    private Spinner spinnerModel, spinnerMake;
    private String modelCodeSelected;
    private Button btn_unfocusExchange;
    private Button[] btnExchange = new Button[2];
    private int[] btn_idServiceBooklet = {R.id.buttonExchangeYes, R.id.buttonExchangeNo};
    private String strIsExchange;
    String strMakeCode = "", strModelCode = "";
    private HashMap<String, String> makeModelCombinedNewList;
    private String vehicleModelSelected;
    private String parentCodeSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_leads);
        init();
    }

    private HashMap<String, String> getMakeModelMap() {
        HashMap<String, String> makeModelMap = new HashMap<>();

        makeModelMap.put("ECOM", "ECOMOBILE");
        makeModelMap.put("LOGN", "VERITO");
        makeModelMap.put("BOL", "BOLERO");
        makeModelMap.put("CHMP", "CHAMPION");
        makeModelMap.put("GIO", "GIO LOAD");
        makeModelMap.put("HTOP", "HARD TOP");
        makeModelMap.put("MAXI", "MAXI TRUCK");
        makeModelMap.put("MXMO", "MAXXIMO LOAD");
        makeModelMap.put("PUP", "PICK UP");
        makeModelMap.put("SCR", "SCORPIO");
        makeModelMap.put("STOP", "SOFT TOP");
        makeModelMap.put("VYGR", "VOYAGER");
        makeModelMap.put("XYLO", "XYLO");
        makeModelMap.put("ALFL", "ALFA LOAD");
        makeModelMap.put("ALFP", "ALFA PASSENGER");
        makeModelMap.put("XUV5", "XUV500");
        makeModelMap.put("THAR", "THAR");
        makeModelMap.put("GENO", "GENIO");
        makeModelMap.put("GIOP", "GIO PASSENGER");
        makeModelMap.put("MXMP", "MXMO MINIVAN");
        makeModelMap.put("REVA", "REVA");
        makeModelMap.put("REXT", "REXTON");
        makeModelMap.put("ALPL", "ALFA PLUS");
        makeModelMap.put("E2O", "E2O");
        makeModelMap.put("MXMH", "MXMO MINIVAN");
        makeModelMap.put("MXMH", "MAXIMOPASENGERHARDTP");
        makeModelMap.put("QUAN", "QUANTO");
        makeModelMap.put("VIBE", "VIBE");
        makeModelMap.put("TUV3", "TUV300");
        makeModelMap.put("SUPP", "SUPRO Passenger");
        makeModelMap.put("EVRT", "E-SUPRO PASSENGER VA");
        makeModelMap.put("SUPL", "SUPRO Load");
        makeModelMap.put("IMPR", "IMPERIO");
        makeModelMap.put("SSHT", "SUPRO-SHT");
        makeModelMap.put("SUCV", "SUPRO-CV");
        makeModelMap.put("SUMV", "SUPRO-MV");
        makeModelMap.put("JETO", "JEETO");
        makeModelMap.put("KUV1", "KUV100");
        makeModelMap.put("SUMT", "SUPRO-MT");
        makeModelMap.put("ALFE", "E-ALFA");
        makeModelMap.put("TUVP", "TUV300 PLUS");
        makeModelMap.put("BOLP", "BOLERO P");
        makeModelMap.put("E2OP", "E2O Plus");
        makeModelMap.put("NUSP", "NUVOSPORT");
        makeModelMap.put("ESDV", "E SUPRO");
        makeModelMap.put("JETP", "JEETO MINIVAN");
        makeModelMap.put("MRZO", "MARAZZO");
        makeModelMap.put("E20P", "E2O Plus");
        makeModelMap.put("EVRT", "E-VERITO");
        makeModelMap.put("KUVN", "KUV100 NXT");
        makeModelMap.put("SCR9", "SCORPIO 1.99");
        makeModelMap.put("XUVN", "XUV500N");
        makeModelMap.put("XYL9", "XUV 1.99");

        return makeModelMap;
    }

    private void getCombinedList() {
        makeModelCombinedNewList = getMakeModelMap();

        modelGroupCdList = new ArrayList<>();
        modelGroupDescList = new ArrayList<>();

        for (String model : makeModelCombinedNewList.keySet()) {
            String cd = model;
            String desc = makeModelCombinedNewList.get(cd);

            modelGroupCdList.add(cd);
            modelGroupDescList.add(desc);
        }
        combinedModelList = new ArrayList<>();
        for (int i = 0; i < modelGroupCdList.size(); i++) {
            String modelGrupDesc = modelGroupDescList.get(i);
            StringBuilder strBuil = new StringBuilder();
            strBuil.append(modelGrupDesc);
            combinedModelList.add(strBuil.toString());
            Log.e("Combined ", strBuil.toString());
        }
    }

    private void init() {
        evaluatorHelper = new EvaluatorHelper(this);
        masterDatabaseHelper=new MasterDatabaseHelper(this);
        spinnerMake = (Spinner) findViewById(R.id.spinnerMake);
        spinnerModel = (Spinner) findViewById(R.id.spinnerModel);
        brokerNewLeadPresenter = new BrokerNewLeadPresenter(this, this);
        locationList = SessionUserDetails.getInstance().getLocationList();
        getCombinedList();
        brokerNewLeadPresenter.setViews();
        brokerNewLeadPresenter.setClickableEvents();

        arrayListMake.add(new MasterModel("Select Make", "Select Make"));
        arrayListMake.addAll(masterDatabaseHelper.getMakeMaster());
        ArrayAdapter<MasterModel> MakeAdapter = new ArrayAdapter<MasterModel>(this,
                android.R.layout.simple_spinner_dropdown_item, arrayListMake);
        MakeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMake.setAdapter(MakeAdapter);
        spinnerMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strMakeCode = arrayListMake.get(position).getCode();
                arrayListModel.clear();
                arrayListModel.add(new MasterModel("Select Model", "Select Model"));
                arrayListModel.addAll(masterDatabaseHelper.getModelMaster(strMakeCode));
                ArrayAdapter<MasterModel> ModelAdapter = new ArrayAdapter<MasterModel>(ActivityNewLeads.this,
                        android.R.layout.simple_spinner_dropdown_item, arrayListModel);
                ModelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerModel.setAdapter(ModelAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strModelCode = arrayListModel.get(position).getCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //btn_idServiceBooklet
        for (int i = 0; i < btnExchange.length; i++) {
            btnExchange[i] = (Button) findViewById(btn_idServiceBooklet[i]);
            btnExchange[i].setBackgroundResource(R.drawable.disable_edit_text_border);
            btnExchange[i].setTextColor(getResources().getColor(R.color.colorblack));
            btnExchange[i].setOnClickListener(this);
        }
        btn_unfocusExchange = btnExchange[0];
        setFocusExchange(btn_unfocusExchange, btnExchange[0]);
    }

    private void setFocusExchange(Button btn_unfocusAccidentalStatus, Button btn_focus) {
        btn_unfocusAccidentalStatus.setTextColor(getResources().getColor(R.color.colorblack));
        btn_unfocusAccidentalStatus.setBackgroundResource(R.drawable.disable_edit_text_border);
        btn_focus.setBackgroundResource(R.drawable.gray_rounded_bg);
        btn_focus.setTextColor(getResources().getColor(R.color.colorWhite));
        strIsExchange = btn_focus.getText().toString();
        this.btn_unfocusExchange = btn_focus;

        if (strIsExchange.equalsIgnoreCase("YES")) {
            spinnerMake.setEnabled(true);
            spinnerModel.setEnabled(true);
        } else {
            spinnerMake.setEnabled(false);
            spinnerModel.setEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void VisualElementsReady() {
        ViewGroup parentNameVg = setIndBlock(R.id.name_il, "Parent Name");
        parentSpinner = findViewById(R.id.location_codes_spinner);
        parentSpinnerModel = findViewById(R.id.location_models_spinner);

        ArrayList<String> arrayStr = new ArrayList<>();
        if (locationList != null) {
            for (int i = 0; i < locationList.size(); i++) {
                //String parentName = SessionUserDetails.getInstance().getParentName();
                String parentName = locationList.get(i).getParentName();
                String locationCode = parentName + " " + locationList.get(i).getLocationName();
                arrayStr.add(locationCode);
            }
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arrayStr); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        parentSpinner.setAdapter(spinnerArrayAdapter);


        ViewGroup nameVg = setIndBlock(R.id.cd_name_il, "Name");
        ViewGroup mobileNoVg = setIndBlock(R.id.cd_mobile_il, "Mobile.No");
        ViewGroup pincodeVg = setIndBlock(R.id.cd_pincode_il, "Pincode");
        ViewGroup vehicleModelVg = setIndBlock(R.id.cd_vehicle_model_in_il, "Vehicle on Model Interested in");
        ViewGroup remarkVg = setIndBlock(R.id.cd_remark_il, "Remark");

        ArrayAdapter<String> spinnerArrayModelAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        combinedModelList); //selected item will look like a spinner set from XML
        spinnerArrayModelAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        parentSpinnerModel.setAdapter(spinnerArrayModelAdapter);

        // setSpinnerValue();

        parentNameTv = parentNameVg.findViewById(R.id.value_et);
        customerNameTv = nameVg.findViewById(R.id.value_et);
        mobileNoTv = mobileNoVg.findViewById(R.id.value_et);
        pinCodeTv = pincodeVg.findViewById(R.id.value_et);
        remarkTv = remarkVg.findViewById(R.id.value_et);

        ViewGroup headingVg = findViewById(R.id.customer_details_heading_il);

        parentNameTv.setText(SessionUserDetails.getInstance().getParentCode());

        TextView headingTv = headingVg.findViewById(R.id.send_to_auction_heading);
        headingTv.setText("Customer Details");
        TextView viewAllBtn = headingVg.findViewById(R.id.viewAllBtn);
        viewAllBtn.setVisibility(View.GONE);
        brokerNewLeadPresenter.setCustomToolbar();
        llSubmitButton = findViewById(R.id.ll_submit_button);
        brokerNewLeadPresenter.setClickableEvents();
    }

    private ViewGroup setIndBlock(int parent, String label) {
        ViewGroup parentVg = findViewById(parent);
        TextView nameTv = parentVg.findViewById(R.id.label_tv);
        EditText valueEt = parentVg.findViewById(R.id.value_et);

        nameTv.setText(label);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Enter ");
        stringBuilder.append(label);
        valueEt.setHint(stringBuilder.toString());

        if (label.equalsIgnoreCase("Vehicle on Model Interested in")) {
            valueEt.setVisibility(View.GONE);
        }

        return parentVg;
    }

    @Override
    public void theseItemsAreClickable() {
        llSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerNameTv.getText().toString().equals("")) {
                    CommonHelper.toast("Enter Name", ActivityNewLeads.this);
                } else if (mobileNoTv.getText().toString().equals("")) {
                    CommonHelper.toast("Enter mobile", ActivityNewLeads.this);
                } else if (pinCodeTv.getText().toString().equals("")) {
                    CommonHelper.toast("Enter pincode", ActivityNewLeads.this);
                } else if (strIsExchange.equals("Yes")) {
                    if (strMakeCode.equals("Select Make")) {
                        CommonHelper.toast("Select Make", ActivityNewLeads.this);
                    } else if (strModelCode.equals("Select Model")) {
                        CommonHelper.toast("Select Model", ActivityNewLeads.this);
                    } else {
                        NewLeadIN lead = attacheDataFromView();
                        brokerNewLeadPresenter.submitQuickLead(lead);
                    }
                } else {
                    NewLeadIN lead = attacheDataFromView();
                    brokerNewLeadPresenter.submitQuickLead(lead);
                }
            }
        });

        parentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationCodeSelected = String.valueOf(locationList.get(position).getLocationCode());
                parentCodeSelected = String.valueOf(locationList.get(position).getParentCode());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        parentSpinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                modelCodeSelected = modelGroupCdList.get(position);
                vehicleModelSelected = modelGroupDescList.get(position);
                Log.e("Slected", modelCodeSelected + " " + vehicleModelSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private NewLeadIN attacheDataFromView() {
        NewLeadIN newLeadIN = new NewLeadIN();
        newLeadIN.setParentCode(parentCodeSelected);
        newLeadIN.setLocationCode(locationCodeSelected);
        newLeadIN.setEnquiryType("FLD");
        newLeadIN.setEnquirySource("MB");
        newLeadIN.setCustomerName(customerNameTv.getText().toString());
        newLeadIN.setMobileNumber(mobileNoTv.getText().toString());
        newLeadIN.setEmail("");
        newLeadIN.setPinCode(pinCodeTv.getText().toString());
        newLeadIN.setLikelyPurchase("GT15");

        newLeadIN.setModelgroup(modelCodeSelected);

        newLeadIN.setUploadDate(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(new Date()));
        newLeadIN.setEnquiryVerificationType("DWS");
        newLeadIN.setEventcode("");
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        newLeadIN.setFollowupDateTime(sf.format(new Date()));
        newLeadIN.setRemarks(remarkTv.getText().toString());
        newLeadIN.setInterstedInExchange(strIsExchange.equals("Yes") ? "Y" : "N");
        if (strIsExchange.equals("Yes")) {
            newLeadIN.setExchangeMake(strMakeCode);
            newLeadIN.setExchangeModel(strModelCode);
        }
        newLeadIN.setMitraId(SessionUserDetails.getInstance().getMitraCode());
        return newLeadIN;
    }

    @Override
    public void settingCustomToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText("New Leads");

        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void newLeadSubmittedSuccessfully(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("IsSuccessful").equals("1")) {
                CommonHelper.toast("Lead submitted successfully", this);
                finish();
            } else {
                CommonHelper.toast(jsonObject.getString("message"), this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void newLeadNotSubmitted(String response) {
        Log.e("NewLead", "Not Submitted New Lead");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonExchangeYes:
                setFocusExchange(btn_unfocusExchange, btnExchange[0]);
                break;
            case R.id.buttonExchangeNo:
                setFocusExchange(btn_unfocusExchange, btnExchange[1]);
                break;
        }
    }
}
