package com.mahindra.mitra20.module_broker.ui.adapters.ViewPager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.BrokerLeadsPages;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerLeadsFragment;

import java.util.ArrayList;

public class BrokerLeadsViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<BrokerLeadsPages> brokerLeadsPagesList;
    private AllLeadsData allLeadsData;

    public BrokerLeadsViewPagerAdapter(FragmentManager fm, AllLeadsData allLeadsData) {
        super(fm);
        this.allLeadsData = allLeadsData;
        brokerLeadsPagesList = new ArrayList<>();
        brokerLeadsPagesList.add(new BrokerLeadsPages("Unassigned Leads", 0));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Hot", 1));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Cold", 2));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Warm", 3));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Ordered", 4));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Invoiced", 5));
        brokerLeadsPagesList.add(new BrokerLeadsPages("Lost", 6));
    }

    @Override
    public Fragment getItem(int position) {
        BrokerLeadsFragment brokerLeadsFragment;
        Bundle bundle = new Bundle();
        bundle.putInt("type", 1);
        switch (position){
            case 0:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getUnassignedLeads());
                brokerLeadsFragment.setArguments(bundle);
                bundle.putInt("type", 2);
                return brokerLeadsFragment;
            case 1:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getHotEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
            case 2:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getColdEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
            case 3:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getWarmEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
            case 4:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getOrderedEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
            case 5:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getInvoicedEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
            case 6:
                brokerLeadsFragment = new BrokerLeadsFragment();
                bundle.putParcelableArrayList("list", allLeadsData.getLostEnquiries());
                brokerLeadsFragment.setArguments(bundle);
                return brokerLeadsFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return brokerLeadsPagesList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return brokerLeadsPagesList.get(position).getPageTitle();
    }
}