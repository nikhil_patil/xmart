package com.mahindra.mitra20.module_broker.ui.adapters.ViewPager;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mahindra.mitra20.module_broker.models.BrokerAuctionPages;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllPageFragment2;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllPageFragment3;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllSinglePageFragment;

import java.util.ArrayList;

public class BrokerAuctionsViewAllViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<BrokerAuctionPages> brokerAuctionPagesList;

    public BrokerAuctionsViewAllViewPagerAdapter(FragmentManager fm) {
        super(fm);
        brokerAuctionPagesList = new ArrayList<>();
        brokerAuctionPagesList.add(new BrokerAuctionPages("Live", 0));
        brokerAuctionPagesList.add(new BrokerAuctionPages("Bidded", 1));
        brokerAuctionPagesList.add(new BrokerAuctionPages("Closed", 2));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return BrokerAuctionViewAllSinglePageFragment.getInstance(null);
            case 1:
                return BrokerAuctionViewAllPageFragment2.getInstance(null);
            case 2:
                return BrokerAuctionViewAllPageFragment3.getInstance(null);
        }
        return null;
    }

    @Override
    public int getCount() {
        return brokerAuctionPagesList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return brokerAuctionPagesList.get(position).getPageTitle();
    }
}