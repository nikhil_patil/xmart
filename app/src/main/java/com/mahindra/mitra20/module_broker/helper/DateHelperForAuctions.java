package com.mahindra.mitra20.module_broker.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelperForAuctions {
    public static String DDMMYYY = "dd/mm/yyyy";
    public static long MILLIS_IN_ONE_SEC = 86400000;
    public static String getLiveAuctionCurrentDate(String dateFormat){
        //String date = new SimpleDateFormat(dateFormat, Locale.getDefault()).format(new Date());
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

      return "03/09/2018";
    }

    public static String getCompletedAuctionDateStartDate(int days, String dateFormat){
        long  totalMillis = MILLIS_IN_ONE_SEC * days;
        String date = new SimpleDateFormat(dateFormat, Locale.getDefault()).format(totalMillis);
        //return date;
        return "03/08/2018";
    }

    public static long adjustForUTC(String auctionDate, String startTimestamp){
        String dateTime = auctionDate + " " + startTimestamp.split(" ")[0];
        long startTimeInMillis = DateHelperForAuctions.getTimeinMillisForSorting(dateTime);
        String dateTimeStr = String.valueOf(startTimeInMillis) + "00";
        startTimeInMillis = Long.parseLong(dateTimeStr);
        return startTimeInMillis;
    }

    public static long getTimeinMillisForSorting(String dateStr){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");


        Date date = null;
        try {
            date = sdf.parse(dateStr);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(dateStr);
        System.out.println("Date - Time in milliseconds : " + date.getTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();

    }
}
