package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.PlaceBidIP;

import org.json.JSONException;
import org.json.JSONObject;

public class BrokerBidNowPresenter implements VolleySingleTon.VolleyInteractor {
    private BrokerBidNowIn bidNowView;
    private Context context;

    public BrokerBidNowPresenter(Context context, BrokerBidNowIn showAuctionsDetailsView) {
        this.bidNowView = showAuctionsDetailsView;
        this.context = context;
    }

    public void placeBidOnServer(PlaceBidIP placeBidIP) {
        JSONObject jsonObj = new JSONObject();
        try {
            String userid = SessionUserDetails.getInstance().getMitraCode();
            jsonObj.put(RestInputKeys.PlaceBidInputKeys.UserID, userid);
            jsonObj.put(RestInputKeys.PlaceBidInputKeys.AuctionID, placeBidIP.getAuctionId());
            jsonObj.put(RestInputKeys.PlaceBidInputKeys.BIDAmount, placeBidIP.getBidAmount());

            String url = UrlConstants.BASE_URL + UrlConstants.PLACE_BID;
            VolleySingleTon.getInstance().connectToPostUrl3(context, this, url, jsonObj, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setViews() {
        bidNowView.VisualElementsReady();
    }

    public void getAuctionsDataFromServer(String auctionId) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.UserID, SessionUserDetails.getInstance().getMitraCode());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.AuctionID, auctionId);

            String url = UrlConstants.BASE_URL + UrlConstants.LIVE_AUCTION_MASTER_BY_ID;
            VolleySingleTon.getInstance().connectToPostUrl3(context, this, url, jsonObj, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setCustomToolbar() {
        bidNowView.settingCustomToolBar();
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        if (requestId == 0) {
            bidNowView.gotAuctionDataById(response);
        } else if (requestId == 1) {
            bidNowView.placedBidOnServer(response);
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        bidNowView.onErrorResponse(response, requestId);
    }
}