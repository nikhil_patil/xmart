package com.mahindra.mitra20.module_broker.interfaces;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

public interface BidNowInteractor {
    void clickedOnBidNow(SingleAuction pos);
}
