package com.mahindra.mitra20.module_broker.models;

public class Auction {
    private String modelName;
    private String modelNum;
    private String vehicleImageUrl;
    private String modelImage;
    private int bidsTotalNum;
    private String status;
    private long highestBid;
    private long bokerPrice;
    private long timeToCompleteInMilliseconds;
    private String auctionType;

    public Auction(String modelName, String modelNum, int bidstotalNum, String status, long highestBid, long brokerPrice, long timetoComplete){
        setModelName(modelName);
        setModelNum(modelNum);
        setVehicleImageUrl(getVehicleImageUrl());
        setStatus(status);
        setHighestBid(highestBid);
        setBokerPrice(brokerPrice);
        setTimeToCompleteInMilliseconds(timeToCompleteInMilliseconds);
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelNum() {
        return modelNum;
    }

    public void setModelNum(String modelNum) {
        this.modelNum = modelNum;
    }

    public String getModelImage() {
        return modelImage;
    }

    public void setModelImage(String modelImage) {
        this.modelImage = modelImage;
    }

    public int getBidsTotalNum() {
        return bidsTotalNum;
    }

    public void setBidsTotalNum(int bidsTotalNum) {
        this.bidsTotalNum = bidsTotalNum;
    }

    public String getStatus() {
        return status;
    }

    public long getTimeToCompleteInMilliseconds() {
        return timeToCompleteInMilliseconds;
    }

    public void setTimeToCompleteInMilliseconds(long timeToCompleteInMilliseconds) {
        this.timeToCompleteInMilliseconds = timeToCompleteInMilliseconds;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getHighestBid() {
        return highestBid;
    }

    public void setHighestBid(long highestBid) {
        this.highestBid = highestBid;
    }

    public long getBokerPrice() {
        return bokerPrice;
    }

    public void setBokerPrice(long bokerPrice) {
        this.bokerPrice = bokerPrice;
    }

    public String getVehicleImageUrl() {
        return vehicleImageUrl;
    }

    public void setVehicleImageUrl(String vehicleImageUrl) {
        this.vehicleImageUrl = vehicleImageUrl;
    }

    public String getAuctionType() {
        return auctionType;
    }

    public void setAuctionType(String auctionType) {
        this.auctionType = auctionType;
    }
}
