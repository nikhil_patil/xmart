package com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable;

import java.io.Serializable;

public class LeadDetail implements Serializable {
    private String leadId;
    private String leadDate;
    private String mobileNo;
    private String model;
    private String dealerName;
    private String interestedInExchange;
    private String intendedDateOfPurchase;
    private String interestedInWhichMahindraVehicle;
    private String leadStatus;
    private String locationCode;
    private String NameOfProspectiveCustomer;
    private String remarks;
    private String pinCode;
    private String preOwnedVehicleModel;
    private String salesExecutiveName;
    private String salesExecutiveNum;
    private String vehicleSegment;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getIntendedDateOfPurchase() {
        return intendedDateOfPurchase;
    }

    public void setIntendedDateOfPurchase(String intendedDateOfPurchase) {
        this.intendedDateOfPurchase = intendedDateOfPurchase;
    }

    public String getInterestedInExchange() {
        return interestedInExchange;
    }

    public void setInterestedInExchange(String interestedInExchange) {
        this.interestedInExchange = interestedInExchange;
    }

    public String getInterestedInWhichMahindraVehicle() {
        return interestedInWhichMahindraVehicle;
    }

    public void setInterestedInWhichMahindraVehicle(String interestedInWhichMahindraVehicle) {
        this.interestedInWhichMahindraVehicle = interestedInWhichMahindraVehicle;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVehicleSegment() {
        return vehicleSegment;
    }

    public void setVehicleSegment(String vehicleSegment) {
        this.vehicleSegment = vehicleSegment;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPreOwnedVehicleModel() {
        return preOwnedVehicleModel;
    }

    public void setPreOwnedVehicleModel(String preOwnedVehicleModel) {
        this.preOwnedVehicleModel = preOwnedVehicleModel;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getNameOfProspectiveCustomer() {
        return NameOfProspectiveCustomer;
    }

    public void setNameOfProspectiveCustomer(String nameOfProspectiveCustomer) {
        NameOfProspectiveCustomer = nameOfProspectiveCustomer;
    }

    public String getSalesExecutiveName() {
        return salesExecutiveName;
    }

    public void setSalesExecutiveName(String salesExecutiveName) {
        this.salesExecutiveName = salesExecutiveName;
    }

    public String getSalesExecutiveNum() {
        return salesExecutiveNum;
    }

    public void setSalesExecutiveNum(String salesExecutiveNum) {
        this.salesExecutiveNum = salesExecutiveNum;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}