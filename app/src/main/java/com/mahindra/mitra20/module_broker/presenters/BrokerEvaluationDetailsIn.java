package com.mahindra.mitra20.module_broker.presenters;

public interface BrokerEvaluationDetailsIn {
    void VisualElementsReady();

    void settingCustomToolBar();
    void gotEvaluationReport(String response);
    void gotErrorForEvaluationReport(String response);
}
