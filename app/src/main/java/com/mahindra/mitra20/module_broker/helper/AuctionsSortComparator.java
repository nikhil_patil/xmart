package com.mahindra.mitra20.module_broker.helper;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.BrokerInAuction;

import java.util.Comparator;

/**
 * Created by user on 9/15/2018.
 */

public class AuctionsSortComparator implements Comparator<BrokerInAuction> {

    @Override
    public int compare(BrokerInAuction br, BrokerInAuction br1) {
        return br.getBiddingDateTime().compareTo(br1.getBiddingDateTime());
    }
}