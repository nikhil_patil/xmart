package com.mahindra.mitra20.module_broker.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MyAuctionsItemDecoration extends RecyclerView.ItemDecoration {
    private int bottom, left, right;

    public MyAuctionsItemDecoration(int bottom, int left, int right) {
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0)
            outRect.top = bottom;
        outRect.bottom = bottom;
        outRect.left = left;
        outRect.right = right;
    }
}
