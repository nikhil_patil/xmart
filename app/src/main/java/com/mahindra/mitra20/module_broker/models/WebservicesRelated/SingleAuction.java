package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

import java.io.Serializable;
import java.util.ArrayList;

public class SingleAuction implements Serializable {
    private String evaluationReportId;
    private String date;
    private String auctionId;
    private String fromDate;
    private String auctionStatus;
    private String countOfBidders;
    private ArrayList<BrokerInAuction> brokersInAuctionList;
    private String mitraBasePrice;
    private String requestId;
    private String startTimeStamp;
    private String remainingTimeInSeconds;
    private String vehicleOwnerName;
    private String contactNumber;
    private String name;
    private String vehicleName;
    private String endTimeStamp;
    private String winnerId;
    private String imageUrl;
    private long startTimeInMillis;
    private long auctionTimeSpan = 7200000;
    private long highestBid;
    private String isHighestbidder;
    private String highestBidStr;
    private String bidderPriceStr;
    private boolean hasWon;
    private String make;
    private String model;
    private boolean hasMitraBidded = false;
    private String noOfOwners;
    private String odometerReading;
    private String feedbackRating;
    private String vehicleUsage;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getAuctionStatus() {
        return auctionStatus;
    }

    public void setAuctionStatus(String auctionStatus) {
        this.auctionStatus = auctionStatus;
    }

    public String getCountOfBidders() {
        return countOfBidders;
    }

    public void setCountOfBidders(String countOfBidders) {
        this.countOfBidders = countOfBidders;
    }

    public ArrayList<BrokerInAuction> getBrokersInAuctionList() {
        return brokersInAuctionList;
    }

    public void setBrokersInAuctionList(ArrayList<BrokerInAuction> brokersInAuctionList) {
        this.brokersInAuctionList = brokersInAuctionList;
    }

    public String getMitraBasePrice() {
        return mitraBasePrice;
    }

    public void setMitraBasePrice(String mitraBasePrice) {
        this.mitraBasePrice = mitraBasePrice;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(String startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public String getRemainingTimeInSeconds() {
        return remainingTimeInSeconds;
    }

    public void setRemainingTimeInSeconds(String remainingTimeInSeconds) {
        this.remainingTimeInSeconds = remainingTimeInSeconds;
    }

    public String getVehicleOwnerName() {
        return vehicleOwnerName;
    }

    public void setVehicleOwnerName(String vehicleOwnerName) {
        this.vehicleOwnerName = vehicleOwnerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(String endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public long getHighestBid() {
        return highestBid;
    }

    public void setHighestBid(long highestBid) {
        this.highestBid = highestBid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getAuctionTimeSpan() {
        return auctionTimeSpan;
    }

    public void setAuctionTimeSpan(long auctionTimeSpan) {
        this.auctionTimeSpan = auctionTimeSpan;
    }

    public String getEvaluationReportId() {
        return evaluationReportId;
    }

    public void setEvaluationReportId(String evaluationReportId) {
        this.evaluationReportId = evaluationReportId;
    }

    public String getIsHighestbidder() {
        return isHighestbidder;
    }

    public void setIsHighestbidder(String isHighestbidder) {
        this.isHighestbidder = isHighestbidder;
    }

    public String getHighestBidStr() {
        return highestBidStr;
    }

    public void setHighestBidStr(String highestBidStr) {
        this.highestBidStr = highestBidStr;
    }

    public String getBidderPriceStr() {
        return bidderPriceStr;
    }

    public void setBidderPriceStr(String bidderPriceStr) {
        this.bidderPriceStr = bidderPriceStr;
    }

    public boolean isHasWon() {
        return hasWon;
    }

    public void setHasWon(boolean hasWon) {
        this.hasWon = hasWon;
    }

    public boolean HasMitraBidded() {
        return hasMitraBidded;
    }

    public void setHasMitraBidded(boolean hasMitraBidded) {
        this.hasMitraBidded = hasMitraBidded;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public long getStartTimeInMillis() {
        return startTimeInMillis;
    }

    public void setStartTimeInMillis(long startTimeInMillis) {
        this.startTimeInMillis = startTimeInMillis;
    }

    public String getNoOfOwners() {
        return noOfOwners;
    }

    public void setNoOfOwners(String noOfOwners) {
        this.noOfOwners = noOfOwners;
    }

    public boolean isHasMitraBidded() {
        return hasMitraBidded;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getFeedbackRating() {
        return feedbackRating;
    }

    public void setFeedbackRating(String feedbackRating) {
        this.feedbackRating = feedbackRating;
    }

    public String getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }
}