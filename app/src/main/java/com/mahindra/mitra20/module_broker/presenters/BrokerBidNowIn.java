package com.mahindra.mitra20.module_broker.presenters;

public interface BrokerBidNowIn {
    void VisualElementsReady();
    void settingCustomToolBar();
    void placedBidOnServer(String response);
    void gotAuctionDataById(String response);
    void onErrorResponse(String response, int requestId);


}
