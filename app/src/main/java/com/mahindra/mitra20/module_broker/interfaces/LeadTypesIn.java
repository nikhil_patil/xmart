package com.mahindra.mitra20.module_broker.interfaces;

public interface LeadTypesIn {
    String HOT = "Hot";
    String WARM = "Warm";
    String COLD = "Cold";
    String ORDERED = "Order";
    String INVOICED = "Invoiced";
    String LOST = "Lost";
}