package com.mahindra.mitra20.module_broker.constants;

public class RestResponseKeys {

    public static class LiveAuctionOutputKeys{
        public static final String AuctionDetails = "AuctionDetails";
        public static final String AuctionDate = "AuctionDate";
        public static final String AuctionID = "AuctionID";
        public static final String AuctionStatus = "AuctionStatus";
        public static final String CountOfBidders = "CountOfBidders";

        public static final String MITRAprice_aseprice = "MITRAprice(base price)";
        public static final String RequestID = "RequestID";
        public static final String StartTimestamp = "StartTimestamp";
        public static final String remainingTimeInSeconds = "remainingTimeInSeconds";
        public static final String vehOwnerName = "vehOwnerName";
        public static final String vehicleName = "vehicleName";

        public static final String LIST = "LIST";
        public static final String MitraId = "MITRAId";
        public static final String MitraName = "MITRAName";
        public static final String bidPrice = "bidPrice";
        public static final String biddingDateTime = "biddingDateTime";

        public static final String HighestPrice = "HighestPrice";
    }
}