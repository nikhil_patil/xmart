package com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.helper.AuctionHelper;
import com.mahindra.mitra20.module_broker.helper.StringHelper;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public class AuctionRecyclerViewViewAllAdapter extends
        RecyclerView.Adapter<AuctionRecyclerViewViewAllAdapter.AuctionViewHolder> {

    public ArrayList<SingleAuction> auctionArrayList;
    private BidNowInteractor interactor;
    public int AUCTION_TYPE = 0;
    private Context context;

    public AuctionRecyclerViewViewAllAdapter(Context context, BidNowInteractor interactor,
                                             ArrayList<SingleAuction> auctionArrayList) {
        this.auctionArrayList = auctionArrayList;
        this.interactor = interactor;
        this.context = context;
    }

    @NonNull
    @Override
    public AuctionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_mitra_auctions_single_view_all, parent, false);
        return new AuctionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AuctionViewHolder holder, final int position) {
        SingleAuction auction = auctionArrayList.get(position);
        holder.vehicleNameTv.setText(auction.getVehicleName());
        String make = auction.getMake();
        String model = auction.getModel();
        holder.vehicleDescTv.setText(new StringBuilder().append(make).append(" ").append(model).toString());

        holder.timeVg.setVisibility(View.GONE);
        holder.vehicleNameTv.setVisibility(View.GONE);
        holder.bidNowBtn.setVisibility(View.GONE);
        holder.youAreNotTheHighestBidder.setVisibility(View.GONE);
        holder.yourBidVg.setVisibility(View.GONE);
        holder.wonVg.setVisibility(View.GONE);
        holder.lostVg.setVisibility(View.GONE);
        holder.highestBidVg.setVisibility(View.GONE);
        holder.yourPriceVg.setVisibility(View.GONE);

        if (AUCTION_TYPE == 0) {
            holder.timeVg.setVisibility(View.VISIBLE);
            holder.vehicleNameTv.setVisibility(View.VISIBLE);
            holder.vehicleDescTv.setVisibility(View.VISIBLE);
            holder.bidNowBtn.setVisibility(View.VISIBLE);
        } else if (AUCTION_TYPE == 1) {
            holder.vehicleNameTv.setVisibility(View.VISIBLE);
            holder.vehicleDescTv.setVisibility(View.VISIBLE);
            holder.youAreNotTheHighestBidder.setVisibility(View.VISIBLE);
            holder.yourBidVg.setVisibility(View.VISIBLE);
            holder.timeVg.setVisibility(View.VISIBLE);

            holder.isHighestBidderTv2.setText("highest bidder");
            holder.yourPriceTv.setText("Rs " + auction.getBidderPriceStr());

            if (auction.isHasWon()) {
                holder.isHighestBidderTv.setText("You are the");
                Glide.with(holder.arrow.getContext())
                        .load(R.mipmap.download_arrow_green)
                        .into(holder.arrow);
            } else {
                holder.isHighestBidderTv.setText("You are not");
                Glide.with(holder.arrow.getContext())
                        .load(R.mipmap.download_arrow)
                        .into(holder.arrow);
            }

            if (auction.HasMitraBidded()) {
                holder.bidNowBtn.setText("Rebid");
            } else {
                holder.bidNowBtn.setText("Bid Now");
            }
        } else if (AUCTION_TYPE == 2) {
            holder.bidNowBtn.setVisibility(View.GONE);
            holder.highestBidVg.setVisibility(View.VISIBLE);
            holder.yourPriceVg.setVisibility(View.VISIBLE);
            holder.vehicleNameTv.setVisibility(View.VISIBLE);
            holder.vehicleDescTv.setVisibility(View.VISIBLE);
            holder.highestBidVg.setVisibility(View.VISIBLE);
            holder.yourPriceVg.setVisibility(View.VISIBLE);
            holder.ivList.setVisibility(View.VISIBLE);

            boolean bidderNotBidded = true;
            if (null == auction.getBidderPriceStr())
                holder.comYourPriceTv.setText("Not bidded");
            else if (auction.getBidderPriceStr().equalsIgnoreCase(""))
                holder.comYourPriceTv.setText("Not bidded");
            else {
                holder.comYourPriceTv.setText("Rs " + auction.getBidderPriceStr());
                bidderNotBidded = false;
            }

            if (!bidderNotBidded) {
                if (auction.getHighestBid() <= 0)
                    holder.highestBidValueTv.setText("Not bidded");
                else
                    holder.highestBidValueTv.setText("Rs " + auction.getHighestBidStr());
            } else {
                holder.highestBidValueTv.setText("NA");
            }

            long biddedPriceLng = 0;
            long highestPriceLng = 0;

            if (auction.getBidderPriceStr() != null &&
                    !auction.getBidderPriceStr().equalsIgnoreCase("")) {
                biddedPriceLng = Long.parseLong(auction.getBidderPriceStr());
            }

            if (auction.getHighestBidStr() != null &&
                    !auction.getHighestBidStr().equalsIgnoreCase("")) {
                highestPriceLng = Long.parseLong(auction.getHighestBidStr());
            }

            if ((highestPriceLng > 0 && biddedPriceLng > 0 && biddedPriceLng >= highestPriceLng)) {
                holder.highestBidValueTv.setText("Rs " + biddedPriceLng);
            }


            holder.ivList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interactor.clickedOnBidNow(auctionArrayList.get(position));
                }
            });

            boolean hasWon = auction.isHasWon();
            holder.timeVg.setVisibility(View.GONE);

            if (null != auction.getBidderPriceStr()) {
                if (hasWon) {
                    holder.wonVg.setVisibility(View.VISIBLE);
                    holder.lostVg.setVisibility(View.GONE);
                } else {
                    holder.wonVg.setVisibility(View.GONE);
                    holder.lostVg.setVisibility(View.VISIBLE);
                }
            } else {
                holder.wonVg.setVisibility(View.GONE);
                holder.lostVg.setVisibility(View.GONE);
            }
        }

        holder.textViewVehicleUsage.setText(String.format("Vehicle Usage - %s",
                auction.getVehicleUsage()));
        if (null != auction.getImageUrl()) {
            if (!auction.getImageUrl().isEmpty()) {
                Glide.with(holder.vehicleIV.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(auction.getImageUrl()))
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.vehicleIV);
            } else {
                Glide.with(holder.vehicleIV.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(holder.vehicleIV);
            }
        }

        int seconds = Integer.parseInt(auction.getRemainingTimeInSeconds());
        int milli = seconds * 1000;

        if (null != holder.countDownTimer) {
            holder.countDownTimer.cancel();
        }
        holder.countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                String time = StringHelper.getDoubleDigit(String.valueOf(hours % 24)) + ":" +
                        StringHelper.getDoubleDigit((((minutes % 60) > 9)
                                ? minutes % 60 : "0" + (minutes % 60)) + ":" +
                                StringHelper.getDoubleDigit(String.valueOf(seconds % 60)));
                holder.timeTv.setText(time);

                int colorForTimer = AuctionHelper.getColorForTimer(millisUntilFinished);
                Drawable mDrawable = context.getResources().getDrawable(R.drawable.button_half_circular_green);
                mDrawable.setColorFilter(new
                        PorterDuffColorFilter(colorForTimer, PorterDuff.Mode.SRC_IN));
                holder.timeVg.setBackground(mDrawable);
            }

            @Override
            public void onFinish() {
                holder.countDownTimer.cancel();
            }
        };
        holder.countDownTimer.start();
    }

    @Override
    public int getItemCount() {
        if (auctionArrayList != null) {
            return auctionArrayList.size();
        }
        return 0;
    }

    public class AuctionViewHolder extends RecyclerView.ViewHolder {
        ImageView vehicleIV;
        ViewGroup timeVg;
        TextView timeTv;
        TextView bidNowBtn;
        TextView vehicleNameTv;
        TextView vehicleSpecsTv;
        TextView highestBidValueTv;
        TextView totalBidsCountTv;
        TextView vehicleDescTv;
        ViewGroup yourBidVg;
        ViewGroup youAreNotTheHighestBidder;
        ViewGroup yourPriceVg;
        ViewGroup highestBidVg;
        ViewGroup lostVg;
        ViewGroup wonVg;
        TextView yourPriceTv;
        TextView isHighestBidderTv, isHighestBidderTv2;
        TextView comYourPriceTv;
        ImageView ivList, arrow;
        CountDownTimer countDownTimer;
        private TextView textViewVehicleUsage;

        AuctionViewHolder(View itemView) {
            super(itemView);
            vehicleIV = itemView.findViewById(R.id.vehicle_iv);
            timeVg = itemView.findViewById(R.id.time_vg);
            arrow = itemView.findViewById(R.id.down_arrow_iv);
            timeTv = itemView.findViewById(R.id.time_tv);
            ivList = itemView.findViewById(R.id.iv_list);
            bidNowBtn = itemView.findViewById(R.id.bid_now_btn);
            vehicleDescTv = itemView.findViewById(R.id.vehicle_des_tv);
            vehicleNameTv = itemView.findViewById(R.id.vehicle_name_tv);
            highestBidValueTv = itemView.findViewById(R.id.highest_bidder_tv);
            yourBidVg = itemView.findViewById(R.id.your_bid_cl);
            youAreNotTheHighestBidder = itemView.findViewById(R.id.not_the_highest_bid_vg);
            yourPriceVg = itemView.findViewById(R.id.your_price_vg);
            highestBidVg = itemView.findViewById(R.id.highest_bid_vg);
            lostVg = itemView.findViewById(R.id.lost_vg);
            wonVg = itemView.findViewById(R.id.won_vg);
            vehicleSpecsTv = itemView.findViewById(R.id.vehicle_specs_tv);
            totalBidsCountTv = itemView.findViewById(R.id.total_bid_count);
            isHighestBidderTv = itemView.findViewById(R.id.not_highest_bid_tv);
            isHighestBidderTv2 = itemView.findViewById(R.id.not_highest_bid_tv2);
            yourPriceTv = itemView.findViewById(R.id.your_bid_tv);
            comYourPriceTv = itemView.findViewById(R.id.your_pr_tv);
            textViewVehicleUsage = itemView.findViewById(R.id.textViewVehicleUsage);
            bidNowBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    interactor.clickedOnBidNow(auctionArrayList.get(getAdapterPosition()));
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interactor.clickedOnBidNow(auctionArrayList.get(getAdapterPosition()));
                }
            });
        }
    }
}