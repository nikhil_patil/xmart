package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.evaluator.constants.EvaluatorConstants;
import com.mahindra.mitra20.evaluator.models.AggregateDetails;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BrokerEvaluationDetailsPresenter implements VolleySingleTon.VolleyInteractor {
    private BrokerEvaluationDetailsIn brokerEvaluationDetailsView;
    private Context context;

    public BrokerEvaluationDetailsPresenter(Context context, BrokerEvaluationDetailsIn brokerLeadsViewAllView) {
        this.brokerEvaluationDetailsView = brokerLeadsViewAllView;
        this.context = context;
    }

    public void getEvaluationReport(String EvaluationReportId, String RequestId) {
        JSONObject jsonObj = new JSONObject();
        try {
            String userid = SessionUserDetails.getInstance().getMitraCode();
            jsonObj.put(RestInputKeys.PlaceBidInputKeys.UserID, userid);
            jsonObj.put("UserID", userid);
            jsonObj.put("EvaluationReportID", EvaluationReportId);
            jsonObj.put("RequestID", RequestId);

            String url = UrlConstants.BASE_URL + UrlConstants.GET_EVALUATION_REPORT_MASTERBYId;
            VolleySingleTon.getInstance().connectToPostUrl3(context, this, url, jsonObj, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setCustomToolbar() {
        brokerEvaluationDetailsView.settingCustomToolBar();
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            NewEvaluation newEvaluation = getNewEvaluationInstance(jsonObject);
            EvaluatorHelper evaluatorHelper = new EvaluatorHelper(context);
            long evaluationReportID = evaluatorHelper.addUpdateNewEvaluationDetails(newEvaluation,
                    EvaluatorConstants.EVALUATION_DB_STATUS_ONLINE);
            brokerEvaluationDetailsView.gotEvaluationReport(String.valueOf(evaluationReportID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        brokerEvaluationDetailsView.gotErrorForEvaluationReport(response);
    }

    public static NewEvaluation getNewEvaluationInstance(JSONObject jsonObject) {
        NewEvaluation newEvaluation = new NewEvaluation();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("EvaluatorReportIDDetails");
            jsonObject = (JSONObject) jsonArray.get(0);

            newEvaluation.setEvaluationNo(CommonHelper.hasJSONKey(jsonObject, "RequestID"));
            newEvaluation.setRequestId(CommonHelper.hasJSONKey(jsonObject, "TransactionNo."));
            newEvaluation.setAgainstTransaction(CommonHelper.hasJSONKey(jsonObject, "AgainstTransaction"));
            newEvaluation.setPurpose(CommonHelper.hasJSONKey(jsonObject, "Purpose"));
            newEvaluation.setOptedNewVehicle(CommonHelper.hasJSONKey(jsonObject, "OptedNewVehicleModel"));

            if (CommonHelper.hasJSONKey(jsonObject, "EvaluationDate&Time").split(" ").length > 0) {
                newEvaluation.setDate(CommonHelper.hasJSONKey(jsonObject, "EvaluationDate&Time").split(" ")[0]);
                newEvaluation.setTime(CommonHelper.hasJSONKey(jsonObject, "EvaluationDate&Time").split(" ")[1]);
            }

            newEvaluation.setTransactionNo(CommonHelper.hasJSONKey(jsonObject, "TransactionNo."));
            newEvaluation.setEvaluatorName(CommonHelper.hasJSONKey(jsonObject, "EvaluationName"));
            newEvaluation.setFrontDoorRightSide(CommonHelper.hasJSONKey(jsonObject, "FrontDoorRightHandSide"));
            newEvaluation.setVariant(CommonHelper.hasJSONKey(jsonObject, "Variant"));
            newEvaluation.setIsAccident(CommonHelper.hasJSONKey(jsonObject, "Accident"));
            newEvaluation.setTestDrive(CommonHelper.hasJSONKey(jsonObject, "TestDrive"));
            newEvaluation.setFrontDoorLeftHandSide(CommonHelper.hasJSONKey(jsonObject, "FrontDoorLeftHandSide"));
            newEvaluation.setBackBumper(CommonHelper.hasJSONKey(jsonObject, "BackBumper"));
            newEvaluation.setTailGate(CommonHelper.hasJSONKey(jsonObject, "TailGate"));
            newEvaluation.setBonnet(CommonHelper.hasJSONKey(jsonObject, "Bonnet"));
            newEvaluation.setBackDoorLeftHandSide(CommonHelper.hasJSONKey(jsonObject, "BackDoorLeftHandSide"));
            newEvaluation.setBackDoorRightSide(CommonHelper.hasJSONKey(jsonObject, "BackDoorRightHandSide"));
            newEvaluation.setFrontFenderLeftHandSide(CommonHelper.hasJSONKey(jsonObject, "FrontFenderLeftHandSide"));
            newEvaluation.setFrontFenderRightSide(CommonHelper.hasJSONKey(jsonObject, "FrontFenderRightHandSide"));
            newEvaluation.setBackFenderLeftHandSide(CommonHelper.hasJSONKey(jsonObject, "BackFenderLeftHandSide"));
            newEvaluation.setBackFenderRightSide(CommonHelper.hasJSONKey(jsonObject, "BackFenderRightHandSide"));
            newEvaluation.setFrontLight(CommonHelper.hasJSONKey(jsonObject, "FrontLight"));
            newEvaluation.setBackLight(CommonHelper.hasJSONKey(jsonObject, "BackLight"));
            newEvaluation.setWindShield(CommonHelper.hasJSONKey(jsonObject, "WindShield"));
            newEvaluation.setPillars(CommonHelper.hasJSONKey(jsonObject, "Pillars"));
            newEvaluation.setMirrors(CommonHelper.hasJSONKey(jsonObject, "Mirrors"));
            newEvaluation.setFMRadio(CommonHelper.hasJSONKey(jsonObject, "FMRadio"));
            newEvaluation.setPowerWindow(CommonHelper.hasJSONKey(jsonObject, "PowerWindow"));
            newEvaluation.setAC(CommonHelper.hasJSONKey(jsonObject, "AC"));
            newEvaluation.setPowerAdjustableSeats(CommonHelper.hasJSONKey(jsonObject, "PowerAdjustableSeats"));
            newEvaluation.setKeyLessEntry(CommonHelper.hasJSONKey(jsonObject, "KeylessEntry"));
            newEvaluation.setPedalShifter(CommonHelper.hasJSONKey(jsonObject, "PedalShifter"));
            newEvaluation.setElectricallyAdjustableORVM(CommonHelper.hasJSONKey(jsonObject, "ElectricallyAdjustableORVM"));
            newEvaluation.setRearDefogger(CommonHelper.hasJSONKey(jsonObject, "RearDefogger"));
            newEvaluation.setSunRoof(CommonHelper.hasJSONKey(jsonObject, "SunRoof"));
            newEvaluation.setAutoClimateTechnologies(CommonHelper.hasJSONKey(jsonObject, "AutoClimateTechnologies"));
            newEvaluation.setLeatherSeats(CommonHelper.hasJSONKey(jsonObject, "LeatherSeats"));
            newEvaluation.setBluetoothAudioSystems(CommonHelper.hasJSONKey(jsonObject, "BluetoothAudioSystem"));
            newEvaluation.setGPSNavigationSystems(CommonHelper.hasJSONKey(jsonObject, "GPSNavigationSystem"));
            newEvaluation.setDashboard(CommonHelper.hasJSONKey(jsonObject, "Dashboard"));
            newEvaluation.setOdometer(CommonHelper.hasJSONKey(jsonObject, "Odometer"));
            newEvaluation.setSeatCover(CommonHelper.hasJSONKey(jsonObject, "SeatCovers"));
            newEvaluation.setAirBags(CommonHelper.hasJSONKey(jsonObject, "AirBags"));
            newEvaluation.setTubelessTyres(CommonHelper.hasJSONKey(jsonObject, "TubelessTyres"));
            newEvaluation.setTyreRearRight(CommonHelper.hasJSONKey(jsonObject, "BackRightTyre"));
            newEvaluation.setTyreRearLeft(CommonHelper.hasJSONKey(jsonObject, "BackLeftTyre"));
            newEvaluation.setSpareTyre(CommonHelper.hasJSONKey(jsonObject, "SpareTyre"));
            newEvaluation.setTyreFrontRight(CommonHelper.hasJSONKey(jsonObject, "FrontRightTyre"));
            newEvaluation.setTyreFrontLeft(CommonHelper.hasJSONKey(jsonObject, "FrontLeftTyre"));
            newEvaluation.setPhotoPathFrontImage(CommonHelper.hasJSONKey(jsonObject, "FrontViewImage"));
            newEvaluation.setPhotoPathRearImage(CommonHelper.hasJSONKey(jsonObject, "BackViewImage"));
            newEvaluation.setOwnerName(CommonHelper.hasJSONKey(jsonObject, "OwnerName"));
            newEvaluation.setAddress(CommonHelper.hasJSONKey(jsonObject, "OwnerAddress"));
            newEvaluation.setEmailId(CommonHelper.hasJSONKey(jsonObject, "OwnerEmail"));
            newEvaluation.setMobileNo(CommonHelper.hasJSONKey(jsonObject, "OwnerMobileNo."));
            newEvaluation.setState(CommonHelper.hasJSONKey(jsonObject, "OwnerState"));
            newEvaluation.setOccupation(CommonHelper.hasJSONKey(jsonObject, "OwnerOccupation"));
            newEvaluation.setPhoneNo(CommonHelper.hasJSONKey(jsonObject, "OwnerPhoneNo"));
            newEvaluation.setCity(CommonHelper.hasJSONKey(jsonObject, "OwnerCity"));
            newEvaluation.setPinCode(CommonHelper.hasJSONKey(jsonObject, "PinCode"));
            newEvaluation.setMake(CommonHelper.hasJSONKey(jsonObject, "Make"));
            newEvaluation.setModel(CommonHelper.hasJSONKey(jsonObject, "Model"));
            newEvaluation.setYearOfMfg(CommonHelper.hasJSONKey(jsonObject, "YearOfManufacturing"));
            newEvaluation.setYearOfReg(CommonHelper.hasJSONKey(jsonObject, "YearOfRegistration"));
            newEvaluation.setTransmission(CommonHelper.hasJSONKey(jsonObject, "Transmission"));
            newEvaluation.setServiceBooklet(CommonHelper.hasJSONKey(jsonObject, "ServiceBooklet"));
            newEvaluation.setInsurance(CommonHelper.hasJSONKey(jsonObject, "Insurance"));
            newEvaluation.setNocStatus(CommonHelper.hasJSONKey(jsonObject, "FinanceCoNameOrNOCStatus"));
            newEvaluation.setBodyType(CommonHelper.hasJSONKey(jsonObject, "BodyType"));
            newEvaluation.setIsAccident(CommonHelper.hasJSONKey(jsonObject, "AccidentalStatus"));
            newEvaluation.setRegNo(CommonHelper.hasJSONKey(jsonObject, "RegNo"));
            newEvaluation.setMileage(CommonHelper.hasJSONKey(jsonObject, "Mileage(Kms)"));
            newEvaluation.setEngineNo(CommonHelper.hasJSONKey(jsonObject, "EngineNo"));
            newEvaluation.setPuc(CommonHelper.hasJSONKey(jsonObject, "PUC"));
            newEvaluation.setNcb(CommonHelper.hasJSONKey(jsonObject, "NCB"));
            newEvaluation.setNoOfOwners(CommonHelper.hasJSONKey(jsonObject, "NumberOfOwners."));//NumberOfOwners.
            newEvaluation.setCustomerExpectedPrice(CommonHelper.hasJSONKey(jsonObject, "SellingPrice"));
            newEvaluation.setNoOfSeats(CommonHelper.hasJSONKey(jsonObject, "NoOfSeats"));
            newEvaluation.setCustomerExpectedPrice(CommonHelper.hasJSONKey(jsonObject, "CustExptdPrice"));
            newEvaluation.setFuelType(CommonHelper.hasJSONKey(jsonObject, "FuelType"));
            newEvaluation.setColor(CommonHelper.hasJSONKey(jsonObject, "Colour"));
            newEvaluation.setEuroVersion(CommonHelper.hasJSONKey(jsonObject, "EuroVersion"));
            newEvaluation.setChassisNo(CommonHelper.hasJSONKey(jsonObject, "ChassisNo"));
            newEvaluation.setRCBook(CommonHelper.hasJSONKey(jsonObject, "RC"));
            newEvaluation.setHypothecation(CommonHelper.hasJSONKey(jsonObject, "Hypothecation"));
            newEvaluation.setFinanceCompany(CommonHelper.hasJSONKey(jsonObject, "HypothecationBankName"));
            newEvaluation.setInsuranceDate(CommonHelper.hasJSONKey(jsonObject, "InsuranceExpiry"));
            newEvaluation.setStereo(CommonHelper.hasJSONKey(jsonObject, "Stereo"));
            newEvaluation.setNoOfKeys(CommonHelper.hasJSONKey(jsonObject, "NoofKeys(Original)"));
            newEvaluation.setReverseParking(CommonHelper.hasJSONKey(jsonObject, "ReverseParkingSensor"));
            newEvaluation.setPowerSteering(CommonHelper.hasJSONKey(jsonObject, "PowerSteering"));
            newEvaluation.setAlloyWheels(CommonHelper.hasJSONKey(jsonObject, "AlloyWheels"));
            newEvaluation.setFogLamp(CommonHelper.hasJSONKey(jsonObject, "FogLamp"));
            newEvaluation.setCentralLocking(CommonHelper.hasJSONKey(jsonObject, "CentralLocking"));
            newEvaluation.setToolKitJack(CommonHelper.hasJSONKey(jsonObject, "ToolKitAndJack"));
            newEvaluation.setRearWiper(CommonHelper.hasJSONKey(jsonObject, "RearWiper"));
            newEvaluation.setMusicSystem(CommonHelper.hasJSONKey(jsonObject, "MusicSystem"));
            newEvaluation.setWheels(CommonHelper.hasJSONKey(jsonObject, "Wheels"));
            newEvaluation.setMonthOfMfg(CommonHelper.hasJSONKey(jsonObject, "MonthofManufacturing"));
            newEvaluation.setMonthOfReg(CommonHelper.hasJSONKey(jsonObject, "MonthofRegistration"));
            newEvaluation.setComment(CommonHelper.hasJSONKey(jsonObject, "Others"));
            newEvaluation.setToolKitJack(CommonHelper.hasJSONKey(jsonObject, "ToolKit&Jack"));

            newEvaluation.setPhotoPathFrontImage(CommonHelper.hasJSONKey(jsonObject, "FrontViewImage"));
            newEvaluation.setPhotoPathRearImage(CommonHelper.hasJSONKey(jsonObject, "BackViewImage"));
            newEvaluation.setPhotoPath45DegreeLeftView(CommonHelper.hasJSONKey(jsonObject, "Vehicle45degreeleftview"));
            newEvaluation.setPhotoPath45DegreeRightView(CommonHelper.hasJSONKey(jsonObject, "Vehicle45degreerightview"));
            newEvaluation.setPhotoPathDashboardView(CommonHelper.hasJSONKey(jsonObject, "VehicleDashboardView"));
            newEvaluation.setPhotoPathOdometerImage(CommonHelper.hasJSONKey(jsonObject, "OdometerImage"));
            newEvaluation.setPhotoPathChassisNoImage(CommonHelper.hasJSONKey(jsonObject, "ChassisNoImage"));
            newEvaluation.setPhotoPathEngineNoImage(CommonHelper.hasJSONKey(jsonObject, "EngineNoImage"));
            newEvaluation.setPhotoPathInteriorView(CommonHelper.hasJSONKey(jsonObject, "VehicleInteriorView"));
            newEvaluation.setPhotoPathRCCopyImage(CommonHelper.hasJSONKey(jsonObject, "RCCopyImage"));
            newEvaluation.setPhotoPathRCCopy1(CommonHelper.hasJSONKey(jsonObject, "OldRCCopyImage1"));
            newEvaluation.setPhotoPathRCCopy2(CommonHelper.hasJSONKey(jsonObject, "OldRCCopyImage2"));
            newEvaluation.setPhotoPathRCCopy3(CommonHelper.hasJSONKey(jsonObject, "OldRCCopyImage3"));
            newEvaluation.setPhotoPathInsuranceImage(CommonHelper.hasJSONKey(jsonObject, "InsuranceImage"));
            newEvaluation.setPhotoPathRearLeftTyre(CommonHelper.hasJSONKey(jsonObject, "RearLeftTyreImage"));
            newEvaluation.setPhotoPathRearRightTyre(CommonHelper.hasJSONKey(jsonObject, "RearRightTyreImage"));
            newEvaluation.setPhotoPathFrontLeftTyre(CommonHelper.hasJSONKey(jsonObject, "FrontLeftTyreImage"));
            newEvaluation.setPhotoPathFrontRightTyre(CommonHelper.hasJSONKey(jsonObject, "FrontRightTyreImage"));
            newEvaluation.setPhotoPathDamagedPartImage1(CommonHelper.hasJSONKey(jsonObject, "DamagedPartImage1"));
            newEvaluation.setPhotoPathDamagedPartImage2(CommonHelper.hasJSONKey(jsonObject, "DamagedPartImage2"));
            newEvaluation.setPhotoPathDamagedPartImage3(CommonHelper.hasJSONKey(jsonObject, "DamagedPartImage3"));
            newEvaluation.setPhotoPathDamagedPartImage4(CommonHelper.hasJSONKey(jsonObject, "DamagedPartImage4"));
            newEvaluation.setPhotoPathRoofTopImage(CommonHelper.hasJSONKey(jsonObject, "DamagedPartImage5"));

            newEvaluation.setCNGKit(CommonHelper.hasJSONKey(jsonObject, "CNGKit"));
            newEvaluation.setRcRemarks(CommonHelper.hasJSONKey(jsonObject, "RcRemarks"));
            newEvaluation.setAccidentalType(CommonHelper.hasJSONKey(jsonObject, "AccidentalType"));
            newEvaluation.setAccidentalRemarks(CommonHelper.hasJSONKey(jsonObject, "AccidentalRemarks"));
            newEvaluation.setLatitude(CommonHelper.hasJSONKey(jsonObject, "Latitude"));
            newEvaluation.setLongitude(CommonHelper.hasJSONKey(jsonObject, "Longitude"));
            newEvaluation.setScrapVehicle(CommonHelper.hasJSONKey(jsonObject, "ScrapVehicle"));

            newEvaluation.setFitnessCertificate(CommonHelper.hasJSONKey(jsonObject, "FitnessCertificate"));
            newEvaluation.setRoadTax(CommonHelper.hasJSONKey(jsonObject, "RoadTax"));
            newEvaluation.setRoadTaxExpiryDate(CommonHelper.hasJSONKey(jsonObject, "RoadTaxExpiryDate"));
            newEvaluation.setFrontBumper(CommonHelper.hasJSONKey(jsonObject, "FrontBumper"));
            newEvaluation.setOutsideRearViewMirrorLH(CommonHelper.hasJSONKey(jsonObject, "OutsideRearViewMirror(LH)"));
            newEvaluation.setOutsideRearViewMirrorRH(CommonHelper.hasJSONKey(jsonObject, "OutsideRearViewMirror(RH)"));
            newEvaluation.setPillarType(CommonHelper.hasJSONKey(jsonObject, "PillarType"));
            newEvaluation.setDickyDoor(CommonHelper.hasJSONKey(jsonObject, "DickyDoor"));
            newEvaluation.setBodyShell(CommonHelper.hasJSONKey(jsonObject, "BodyShell"));
            newEvaluation.setChassis(CommonHelper.hasJSONKey(jsonObject, "Chassis"));
            newEvaluation.setApronFrontLH(CommonHelper.hasJSONKey(jsonObject, "Apron-FrontLH"));
            newEvaluation.setApronFrontRH(CommonHelper.hasJSONKey(jsonObject, "Apron-FrontRH"));
            newEvaluation.setStrutMountingArea(CommonHelper.hasJSONKey(jsonObject, "StrutMountingArea"));
            newEvaluation.setFirewall(CommonHelper.hasJSONKey(jsonObject, "Firewall"));
            newEvaluation.setCowlTop(CommonHelper.hasJSONKey(jsonObject, "CowlTop"));
            newEvaluation.setUpperCrossMember(CommonHelper.hasJSONKey(jsonObject, "UpperCrossMember"));
            newEvaluation.setLowerCrossMember(CommonHelper.hasJSONKey(jsonObject, "LowerCrossMember"));
            newEvaluation.setRadiatorSupport(CommonHelper.hasJSONKey(jsonObject, "RadiatorSupport"));
            newEvaluation.setEngineRoomCarrierAssembly(CommonHelper.hasJSONKey(jsonObject, "EngineRoomCarrierAssembly"));
            newEvaluation.setHeadLightRH(CommonHelper.hasJSONKey(jsonObject, "HeadLightRH"));
            newEvaluation.setHeadLightLH(CommonHelper.hasJSONKey(jsonObject, "HeadLightLH"));
            newEvaluation.setTailLightLH(CommonHelper.hasJSONKey(jsonObject, "TailLightLH"));
            newEvaluation.setTailLightRH(CommonHelper.hasJSONKey(jsonObject, "TailLightRH"));
            newEvaluation.setRunningBoardRH(CommonHelper.hasJSONKey(jsonObject, "RunningBoardRH"));
            newEvaluation.setRunningBoardLH(CommonHelper.hasJSONKey(jsonObject, "RunningBoardLH"));
            newEvaluation.setPowerWindowType(CommonHelper.hasJSONKey(jsonObject, "PowerWindowType"));
            newEvaluation.setAllWindowCondition(CommonHelper.hasJSONKey(jsonObject, "AllWindowCondition"));
            newEvaluation.setFrontRHWindow(CommonHelper.hasJSONKey(jsonObject, "FrontRHWindow"));
            newEvaluation.setFrontLHWindow(CommonHelper.hasJSONKey(jsonObject, "FrontLHWindow"));
            newEvaluation.setRearRHWindow(CommonHelper.hasJSONKey(jsonObject, "RearRHWindow"));
            newEvaluation.setRearLHWindow(CommonHelper.hasJSONKey(jsonObject, "RearLHWindow"));
            newEvaluation.setPlatformPassengerCabin(CommonHelper.hasJSONKey(jsonObject, "PlatformPassengerCabin"));
            newEvaluation.setPlatformBootSpace(CommonHelper.hasJSONKey(jsonObject, "PlatformBootSpace"));
            newEvaluation.setOverallFeedbackRating(CommonHelper.hasJSONKey(jsonObject, "OverallFeedbackRating"));
            newEvaluation.setAudioPathVehicleNoise(CommonHelper.hasJSONKey(jsonObject, "AudioClipForNoise"));
            newEvaluation.setEvaluationCount(CommonHelper.hasJSONKey(jsonObject, "EvaluationCount"));
            newEvaluation.setVehicleUsage(CommonHelper.hasJSONKey(jsonObject, "VehicleUsage"));
            newEvaluation.setToolKitJack(CommonHelper.hasJSONKey(jsonObject, "ToolKit&Jack"));

            if (!CommonHelper.hasJSONKey(jsonObject, "AggregateDetails").equals("")) {
                JSONArray aggregateDetails = jsonObject.getJSONArray("AggregateDetails");

                for (int i = 0; i < aggregateDetails.length(); i++) {
                    JSONObject jsonAggregateDetails = (JSONObject) aggregateDetails.get(i);
                    AggregateDetails aggregateDetailsChild = new AggregateDetails(
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "AggregateCode"),
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "AggregateDesc"),
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "AggregateSrlNo"),
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "Remarks"),
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "SubsystemRefurbishmentCost"),
                            CommonHelper.hasJSONKey(jsonAggregateDetails, "RemarksOthers")
                    );
                    switch (aggregateDetailsChild.getAggregateCode()) {
                        case "BDY":
                            newEvaluation.setBodyCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrBodyRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrBodyRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "ENG":
                            newEvaluation.setEngineCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrEngineRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrEngineRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "FIS":
                            newEvaluation.setFuelAndIgnitionSystemCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrFuelAndIgnitionSystemRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrFuelAndIgnitionSystemRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "TRN":
                            newEvaluation.setTransmissionCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrTransmissionRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrTransmissionRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "BRS":
                            newEvaluation.setBreakSystemCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrBreakSystemRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrBreakSystemRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "SUS":
                            newEvaluation.setSuspensionCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrSuspensionSystemRemarks(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrSuspensionSystemRemarksOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "STS":
                            newEvaluation.setSteeringSystemCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrSteeringSystemRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrSteeringSystemRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "ACS":
                            newEvaluation.setAcCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrACSystemRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrACSystemRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "ELS":
                            newEvaluation.setElectricalSystemCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrElectricalSystemRemark(aggregateDetailsChild.getRemarks());
                            newEvaluation.setStrElectricalSystemRemarkOther(aggregateDetailsChild.getRemarksOthers());
                            break;
                        case "GSC":
                            newEvaluation.setGeneralServiceCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            newEvaluation.setStrGeneralServiceCostRemark(aggregateDetailsChild.getRemarks());
                            break;
                        case "AGT":
                            newEvaluation.setTotalOfAggregateCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            break;
                        case "TRE":
                            newEvaluation.setTotalRefurbishmentCost(aggregateDetailsChild.getSubsystemRefurbishmentCost());
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newEvaluation;
    }
}