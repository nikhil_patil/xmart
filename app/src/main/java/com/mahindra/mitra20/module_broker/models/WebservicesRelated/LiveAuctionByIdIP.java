package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

public class LiveAuctionByIdIP {
    private String userID;
    private String auctionId;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }
}
