package com.mahindra.mitra20.module_broker.constants;

import com.mahindra.mitra20.constants.GeneralConstants;

public class UrlConstants {

    public static final String BASE_URL = GeneralConstants.BASE_URL;

//    public static final String LEADS_MASTER_LIST = "xmart/leadsMaster";
    public static final String LEADS_MASTER_LIST = "xmart/quickLeadDetails";
    public static final String LIVE_AUCTION_MASTER = "xmart/LiveAuctionMaster";
    public static final String LIVE_AUCTION_MASTER_BY_ID = "xmart/LiveAuctionMasterByID";
    public static final String PLACE_BID = "xmart/PlaceBid";
    public static final String EVAL_CHAM_LOGIN = "common/authentication";
    public static final String MITRA_LOGIN = "Influencer/InflunecerValidation";
    public static final String Lead_SUBMIT = "preSales/quickLeadCreation";
    public static final String GET_SMALL_DATA = "BOPIntegration/GetSmallMasterData";
    public static final String GET_EVALUATION_REPORT_MASTERBYId = "xmart/EvaluationReportMasterByID";
}