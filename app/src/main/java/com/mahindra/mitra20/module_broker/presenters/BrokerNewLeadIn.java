package com.mahindra.mitra20.module_broker.presenters;

public interface BrokerNewLeadIn {
    void VisualElementsReady();
    void theseItemsAreClickable();
    void settingCustomToolBar();
    void newLeadSubmittedSuccessfully(String response);
    void newLeadNotSubmitted(String response);

}
