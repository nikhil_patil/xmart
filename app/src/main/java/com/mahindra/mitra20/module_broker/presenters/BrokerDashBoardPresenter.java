package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.MyLeadsResponse;
import com.mahindra.mitra20.module_broker.models.Parsers;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.LiveAuctionIP;

import org.json.JSONException;
import org.json.JSONObject;

public class BrokerDashBoardPresenter implements VolleySingleTon.VolleyInteractor,
        VolleySingleTon.VolleyInteractorAuctions {
    private BrokerDashBoardIn mainActivityView;
    private int AUCTIONS_DATA_REQUEST = 1;
    public static final int TOTAL_LEADS_REQUEST = 2;
    private final int DELR_MST_REQUEST = 6;
    private Context context;

    public BrokerDashBoardPresenter(Context context, BrokerDashBoardIn mainActivityView) {
        this.mainActivityView = mainActivityView;
        this.context = context;
    }

    public void setViews() {
        mainActivityView.VisualElementsReady();
    }

    public void setClickEvents() {
        mainActivityView.theseItemsAreClickable();
    }

    public void showAuctionsViewAllScreen() {
        mainActivityView.showingAuctionViewAllScreen();
    }

    public void setLeadsSection() {
        mainActivityView.settingLeadsSection();
    }

    public void showLeadsSection(int pageNum) {
        mainActivityView.showingLeadsSection(pageNum);
    }

    public void setAuctionsSection() {
        mainActivityView.auctionsSectionReady();
    }

    public void showNewLeadsActivity() {
        mainActivityView.showingActivityNewLeads();
    }

    public void getAuctionsData(LiveAuctionIP liveAuctionIP, int currentAuctionPage) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.FromDate, liveAuctionIP.getFromDate());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.toDate, liveAuctionIP.getToDate());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.AuctionStatus, liveAuctionIP.getAuctionStatus());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Make, liveAuctionIP.getMake());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Model, liveAuctionIP.getModel());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.countToDisplay, liveAuctionIP.getCountToDisplay());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Sorting, liveAuctionIP.getSorting());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.UserID, liveAuctionIP.getUserId());

            String url = UrlConstants.BASE_URL + UrlConstants.LIVE_AUCTION_MASTER;
            VolleySingleTon.getInstance().connectToPostUrl3ForAuctions(context,
                    this, url, jsonObj, AUCTIONS_DATA_REQUEST, currentAuctionPage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getSmallDataForDealerMaster() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("RequestType", "DELR_MST");
            jsonObj.put("ImeiNo", "");
            jsonObj.put("LastUpdatedDate", "");
            String url = UrlConstants.BASE_URL + UrlConstants.GET_SMALL_DATA;
            VolleySingleTon.getInstance().connectToPostUrlForSmallData(context,
                    this, url, jsonObj, DELR_MST_REQUEST);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getLeadsData() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.LeadsInputKeys.MitraCode,
                    SessionUserDetails.getInstance().getMitraCode());

            String url = UrlConstants.BASE_URL + UrlConstants.LEADS_MASTER_LIST;
            VolleySingleTon.getInstance().connectToPostUrl3(context, this, url, jsonObj,
                    TOTAL_LEADS_REQUEST);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponseForAuctions(int requestId, String response, final int auctionId) {
        JSONObject responseJsonObj = null;
        if (requestId == AUCTIONS_DATA_REQUEST) {
            try {
                responseJsonObj = new JSONObject(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mainActivityView.gotAuctionsData(auctionId, responseJsonObj);
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        JSONObject responseJsonObj = null;
        if (requestId == TOTAL_LEADS_REQUEST) {
            MyLeadsResponse leadsResponses = new Gson().fromJson(response, MyLeadsResponse.class);
//            MyLeadsResponse leadsResponses = new Gson().fromJson("{\"AssignedEnquiries\":4,\"AssignedEnquiryDetails\":[{\"CustomerName\":\"Test445 2\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"EnquiryID\":\"ENQ20A000317\",\"EnquirySource\":\"DWS\",\"EnquiryType\":\"INT\",\"InterestedInVehicle\":\"TUV3\",\"Make\":\"Make2\",\"Model\":\"Model2\",\"SalesConsultantName\":\"ABHISHEK DILIPBHAI FICHADIYA\",\"SalesConsultantNumber\":\"9687557880\",\"Status\":\"Order\"},{\"CustomerName\":\"Test445 5\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"EnquiryID\":\"ENQ20A000318\",\"EnquirySource\":\"DWS\",\"EnquiryType\":\"INT\",\"InterestedInVehicle\":\"TUV3\",\"Make\":\"Skoda\",\"Model\":\"Octavia\",\"SalesConsultantName\":\"ABHISHEK DILIPBHAI FICHADIYA\",\"SalesConsultantNumber\":\"9687557880\",\"Status\":\"Cold\"},{\"CustomerName\":\"Test445 3\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"EnquiryID\":\"ENQ20A000319\",\"EnquirySource\":\"DWS\",\"EnquiryType\":\"INT\",\"InterestedInVehicle\":\"TUV3\",\"Make\":\"Suzuki\",\"Model\":\"Ciaz\",\"SalesConsultantName\":\"ABHISHEK DILIPBHAI FICHADIYA\",\"SalesConsultantNumber\":\"9687557880\",\"Status\":\"Cold\"},{\"CustomerName\":\"Test445 3\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"EnquiryID\":\"ENQ20A000316\",\"EnquirySource\":\"DWS\",\"EnquiryType\":\"INT\",\"InterestedInVehicle\":\"TUV3\",\"Make\":\"Honda\",\"Model\":\"City\",\"SalesConsultantName\":\"ABHISHEK DILIPBHAI FICHADIYA\",\"SalesConsultantNumber\":\"9687557880\",\"Status\":\"Cold\"}],\"IsSuccessful\":\"1\",\"UnassignedLeads\":3,\"UnassignedRequestCountDetails\":[{\"CustomerName\":\"Athira\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"Make\":\"Maruti\",\"Model\":\"Swift\"},{\"CustomerName\":\"Test445 1\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"Make\":\"Volkswagon\",\"Model\":\"Polo\"},{\"CustomerName\":\"Test445\",\"DealerName\":\"M. M. VORA AUTOMOBILES PVT.LTD.\",\"Make\":\"Mercedes\",\"Model\":\"Benz\"}],\"message\":\"Success\",\"messageCode\":\"25\"}", MyLeadsResponse.class);
            AllLeadsData leadsData = Parsers.parseLeadsData(leadsResponses);
            mainActivityView.gotLeadsData(leadsData);
        } else if (requestId == DELR_MST_REQUEST) {
            try {
                responseJsonObj = new JSONObject(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mainActivityView.gotSmallDataForDealerMaster(responseJsonObj);
        }
    }

    public void gotErrorResponse(int requestId, String response) {

    }
}