package com.mahindra.mitra20.module_broker.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.module_broker.helper.MyLeadsItemDecoration;
import com.mahindra.mitra20.module_broker.models.AssignedEnquiryDetails;
import com.mahindra.mitra20.module_broker.models.UnassignedLeadDetails;
import com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView.LeadsRecyclerViewAdapter;

import java.util.List;

public class BrokerLeadsFragment extends Fragment {
    private int type;
    private List<AssignedEnquiryDetails> enquiries;
    private List<UnassignedLeadDetails> leads;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        try {
            if (getArguments() != null) {
                type = getArguments().getInt("type");
                if (type == 1) {
                    enquiries = getArguments().getParcelableArrayList("list");
                } else {
                    leads = getArguments().getParcelableArrayList("list");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inflater.inflate(R.layout.fragment_view_all_leads, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setLeadsRecyclerView(view);
    }

    private void setLeadsRecyclerView(View fragmentView) {
        RecyclerView leadsRv = fragmentView.findViewById(R.id.leadsRv);
        LeadsRecyclerViewAdapter adapter = new LeadsRecyclerViewAdapter(getActivity(),
                enquiries, leads, type);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        leadsRv.setLayoutManager(mLayoutManager);
        MyLeadsItemDecoration dividerItemDecoration = new MyLeadsItemDecoration(
                ScreenDensityHelper.dpToPx(getActivity(),15),
                ScreenDensityHelper.dpToPx(getActivity(), 10),
                ScreenDensityHelper.dpToPx(getActivity(), 10));
        leadsRv.addItemDecoration(dividerItemDecoration);
        leadsRv.setItemAnimator(new DefaultItemAnimator());
        leadsRv.setAdapter(adapter);
    }
}