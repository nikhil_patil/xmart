package com.mahindra.mitra20.module_broker.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.module_broker.presenters.BrokerAuctionsViewAllIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerAuctionsViewAllPresenter;

import org.json.JSONObject;

public class ActivityBrokerBidPlaceNow extends AppCompatActivity implements BrokerAuctionsViewAllIn
        {

    private BrokerAuctionsViewAllPresenter showAuctionDetailsPresenter;
    private TextView userTitleTv, companyTitleTv;
    private ViewGroup auctionDashBoardVg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_place_your_bid);

        init();
    }

    private void init() {
        showAuctionDetailsPresenter = new BrokerAuctionsViewAllPresenter(this, this);
    }


            @Override
    public void VisualElementsReady() {

    }

            @Override
    public void settingCustomToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        //actionBar.setDisplayShowTitleEnabled(false);
        //View customView = getLayoutInflater().inflate(R.layout.temp_tool_bar_mitra, null);
        // actionBar.setCustomView(customView);
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent =(Toolbar) customView.getParent();
        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0,0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText("My Auction Details");

        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

            @Override
            public void gotAuctionsData(int auctionPage, JSONObject response) {

            }

            @Override
            public void gotAuctionAPIError(String message) {

            }


        }
