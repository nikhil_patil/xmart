package com.mahindra.mitra20.module_broker.models;

import android.os.Parcel;
import android.os.Parcelable;

public class UnassignedLeadDetails implements Parcelable {
    private String Model;

    private String Make;

    private String CustomerName;

    private String DealerName;

    protected UnassignedLeadDetails(Parcel in) {
        Model = in.readString();
        Make = in.readString();
        CustomerName = in.readString();
        DealerName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Model);
        dest.writeString(Make);
        dest.writeString(CustomerName);
        dest.writeString(DealerName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UnassignedLeadDetails> CREATOR = new Creator<UnassignedLeadDetails>() {
        @Override
        public UnassignedLeadDetails createFromParcel(Parcel in) {
            return new UnassignedLeadDetails(in);
        }

        @Override
        public UnassignedLeadDetails[] newArray(int size) {
            return new UnassignedLeadDetails[size];
        }
    };

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getDealerName() {
        return DealerName;
    }

    public void setDealerName(String DealerName) {
        this.DealerName = DealerName;
    }

    @Override
    public String toString() {
        return "ClassPojo [Model = " + Model + ", Make = " + Make + ", CustomerName = " + CustomerName + ", DealerName = " + DealerName + "]";
    }
}