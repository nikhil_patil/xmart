package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.MyLeadsResponse;
import com.mahindra.mitra20.module_broker.models.Parsers;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class BrokerLeadsActivityPresenter implements VolleySingleTon.VolleyInteractor {
    private BrokerLeadsViewAllIn brokerLeadsViewAllView;
    private Context context;

    public BrokerLeadsActivityPresenter(Context context, BrokerLeadsViewAllIn brokerLeadsViewAllView) {
        this.brokerLeadsViewAllView = brokerLeadsViewAllView;
        this.context = context;
    }

    public void getLeadsData() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.LeadsInputKeys.MitraCode,
                    SessionUserDetails.getInstance().getMitraCode());

            String url = UrlConstants.BASE_URL + UrlConstants.LEADS_MASTER_LIST;
            VolleySingleTon.getInstance().connectToPostUrl3(context, this, url, jsonObj,
                    BrokerDashBoardPresenter.TOTAL_LEADS_REQUEST);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotSuccessResponse(int requestId, String response) {
        try {
            if (requestId == BrokerDashBoardPresenter.TOTAL_LEADS_REQUEST) {
                MyLeadsResponse leadsResponses = new Gson().fromJson(response, MyLeadsResponse.class);
                AllLeadsData leadsData = Parsers.parseLeadsData(leadsResponses);
                brokerLeadsViewAllView.gotLeadsData(leadsData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {

    }
}