package com.mahindra.mitra20.module_broker.presenters;

import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.MyLeadsResponse;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;

import org.json.JSONObject;

import java.util.List;

public interface BrokerDashBoardIn {
    void VisualElementsReady();
    void theseItemsAreClickable();
    void showingAuctionViewAllScreen();

    void showingLeadsSection(int pageNum);
    void auctionsSectionReady();
    void showingActivityNewLeads();
    void settingLeadsSection();
    void gotAuctionsData(int auctionPage, JSONObject response);
    void gotLeadsData(AllLeadsData allLeadsData);
    void gotSmallDataForDealerMaster(JSONObject response);
    void gotNoAuctionsData(int requestId, String response);
}
