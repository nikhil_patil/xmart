package com.mahindra.mitra20.module_broker.constants;

public class SharedPreferenceKeys {

    public static final String LAST_PINCODE_SYNC_DATE = "last_pincode_sync_date";
    public static String MY_PREFS = "MyPrefs";

    public static String FCM_TOKEN = "fcm_token";
    public static String LOGIN_HELP_DISPLAYED = "login_help_displayed";
    public static String BROKER_DASHBOARD_HELP_DISPLAYED = "broker_dashboard_help_displayed";
    public static String CHAMPION_DASHBOARD_HELP_DISPLAYED = "champion_dashboard_help_displayed";
    public static String EVALUATOR_DASHBOARD_HELP_DISPLAYED = "evaluator_dashboard_help_displayed";
    public static String NOTIFICATION_RECEIVED = "notification_received";

}
