package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.google.gson.Gson;
import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.NewLeadIN;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class BrokerNewLeadPresenter  implements VolleySingleTon.VolleyInteractor{
    private BrokerNewLeadIn brokerNewLeadView;
    private Context context;

    public BrokerNewLeadPresenter(Context context, BrokerNewLeadIn brokerNewLeadView){
        this.brokerNewLeadView = brokerNewLeadView;
        this.context = context;
    }

    public void submitQuickLead(NewLeadIN lead){
        Gson gson = new Gson();
        String json = gson.toJson(lead);// obj is your object
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url  = UrlConstants.BASE_URL + UrlConstants.Lead_SUBMIT;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("DSCIMEI", "");

        VolleySingleTon.getInstance().connectToPostUrl3WithHeaders(context, this, url, jsonObj, 0, headers);
    }

    public void setViews(){
        brokerNewLeadView.VisualElementsReady();
    }

    public void setClickableEvents(){
        brokerNewLeadView.theseItemsAreClickable();
    }

    public void setCustomToolbar(){
        brokerNewLeadView.settingCustomToolBar();
    }


    @Override
    public void gotSuccessResponse(int requestId, String response) {
        brokerNewLeadView.newLeadSubmittedSuccessfully(response);
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        brokerNewLeadView.newLeadNotSubmitted(response);
    }
}