package com.mahindra.mitra20.module_broker.presenters;

import org.json.JSONObject;

public interface BrokerAuctionsViewAllIn {
    void VisualElementsReady();

    void settingCustomToolBar();
    void gotAuctionsData(int auctionPage, JSONObject response);
    void gotAuctionAPIError(String message);
}
