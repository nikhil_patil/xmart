package com.mahindra.mitra20.module_broker.interfaces;

public interface BrokerAuctionViewAllCallback {
    void resetAuctionIntervalValues();
}
