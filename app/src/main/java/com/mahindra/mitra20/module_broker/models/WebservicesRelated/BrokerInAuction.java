package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

import java.io.Serializable;
import java.util.Date;

public class BrokerInAuction implements Serializable{
    private String mitraiD;
    private String mitraName;
    private String bidPrice;
    private String biddingDateTime;
    private Date biddingTime;

    public String getMitraiD() {
        return mitraiD;
    }

    public void setMitraiD(String mitraiD) {
        this.mitraiD = mitraiD;
    }

    public String getMitraName() {
        return mitraName;
    }

    public void setMitraName(String mitraName) {
        this.mitraName = mitraName;
    }

    public String getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public String getBiddingDateTime() {
        return biddingDateTime;
    }

    public void setBiddingDateTime(String biddingDateTime) {
        this.biddingDateTime = biddingDateTime;
    }

    public Date getBiddingTime() {
        return biddingTime;
    }

    public void setBiddingTime(Date biddingTime) {
        this.biddingTime = biddingTime;
    }
}
