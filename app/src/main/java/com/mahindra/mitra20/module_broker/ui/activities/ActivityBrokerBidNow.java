package com.mahindra.mitra20.module_broker.ui.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mahindra.mitra20.R;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.helper.AuctionHelper;
import com.mahindra.mitra20.module_broker.helper.StringHelper;
import com.mahindra.mitra20.module_broker.models.Parsers;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.BrokerInAuction;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.PlaceBidIP;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;
import com.mahindra.mitra20.module_broker.presenters.BrokerBidNowIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerBidNowPresenter;
import com.mahindra.mitra20.module_broker.presenters.BrokerEvaluationDetailsIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerEvaluationDetailsPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityBrokerBidNow extends AppCompatActivity implements BrokerBidNowIn,
        BrokerEvaluationDetailsIn {

    private BrokerBidNowPresenter bidNowPresenter;
    private ViewGroup placeBidVg, closedBidVg;
    private LinearLayout wonVg, lossVg;
    private EditText placeBidEt, enterBiddedTv;
    private ImageView vehicleImage;
    private ImageView evalBtn;
    private ImageView callBtn;
    private SingleAuction singleAuction;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Button placeBidBtn, rebidBtn;
    private TextView highest_bidder, personNameTv, vehicleDetails, rebidPriceLabel,
            tvClosedBidPrice;
    private boolean reportDownloaded, showDetailsClicked;
    private String downloadedReportId, auctionId;
    private CountDownTimer countDownTimer;
    private TextView timeTv;
    private TextView textViewVehicleUsage;
    private TextView textViewNoOfOwners;
    TextView textViewOdometer;
    RatingBar ratingFeedback;
    private ViewGroup timeVg;
    public String contactNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitra_bid_now);
        singleAuction = (SingleAuction) getIntent().getSerializableExtra(ExtrasKeys.General.AUCTION_SINGLE_DATA);
        auctionId = singleAuction.getAuctionId();
        contactNum = singleAuction.getContactNumber();
        init();
        updaeUI();
        setUpTimer();
    }

    private void setCustomToolBar() {
        bidNowPresenter.setCustomToolbar();
    }

    private void init() {
        bidNowPresenter = new BrokerBidNowPresenter(this, this);
        bidNowPresenter.setViews();
        bidNowPresenter.getAuctionsDataFromServer(auctionId);
        setUpSwipeRefreshContainer();
    }

    private void setUpSwipeRefreshContainer() {
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                bidNowPresenter.getAuctionsDataFromServer(auctionId);
            }
        });
    }

    private void showEvalDetailsActivity() {
        if (reportDownloaded) {
            Intent intent = new Intent(this, EvaluationReportDetailsBrokerActivity.class);
            intent.putExtra("NewEvaluationId", downloadedReportId);
            intent.putExtra("status", "Completed");
            startActivity(intent);
        } else {
            CommonHelper.toast("Downloading report. Please wait..", getApplicationContext());
        }
    }

    public void showReBidActivity() {
        placeBidVg.setVisibility(View.VISIBLE);
    }

    public void showPlaceBidActivity() {
        placeBidVg.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (placeBidVg.getVisibility() == View.VISIBLE) {
            closePlaceBid();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void VisualElementsReady() {
        setCustomToolBar();
        placeBidBtn = findViewById(R.id.place_bid_btn);
        evalBtn = findViewById(R.id.list_iv);
        callBtn = findViewById(R.id.phone_iv);
        vehicleImage = findViewById(R.id.vehicle_iv);
        rebidBtn = findViewById(R.id.re_bid_btn);
        closedBidVg = findViewById(R.id.closed_bid);
        wonVg = findViewById(R.id.won_vg);
        lossVg = findViewById(R.id.loss_vg);
        highest_bidder = findViewById(R.id.highest_bidder);
        personNameTv = findViewById(R.id.person_name_tv);
        vehicleDetails = findViewById(R.id.vehicle_details);
        enterBiddedTv = findViewById(R.id.enter_bidded_tv);
        rebidPriceLabel = findViewById(R.id.rebid_price_label);
        tvClosedBidPrice = findViewById(R.id.tv_closed_bid_price);
        textViewNoOfOwners = findViewById(R.id.textViewNoOfOwners);
        textViewOdometer = findViewById(R.id.textViewOdometerReading);
        ratingFeedback = findViewById(R.id.ratingFeedback);
        textViewVehicleUsage = findViewById(R.id.textViewVehicleUsage);
        timeVg = findViewById(R.id.time_vg);
        timeTv = findViewById(R.id.timer_tv);
        timeVg.setVisibility(View.INVISIBLE);

        placeBidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlaceBidActivity();
            }
        });
        rebidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReBidActivity();
            }
        });
        evalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEvalDetailsActivity();
            }
        });
        setUpPlaceBidScreen();

        wonVg.setVisibility(View.GONE);
        lossVg.setVisibility(View.GONE);
    }

    private void setUpTimer() {
        long milli = Long.parseLong(singleAuction.getRemainingTimeInSeconds()) * 1000;
        if (milli > 0) {
            timeVg.setVisibility(View.VISIBLE);
        }
        countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                String time = StringHelper.getDoubleDigit(String.valueOf(hours % 24)) + ":" +
                        StringHelper.getDoubleDigit(String.valueOf(((minutes % 60) > 9)
                                ? minutes % 60 : "0" + (minutes % 60)) + ":" +
                                StringHelper.getDoubleDigit(String.valueOf(seconds % 60)));
                timeTv.setText(time);

                int colorForTimer = AuctionHelper.getColorForTimer(millisUntilFinished);
                Drawable mDrawable = getResources().getDrawable(R.drawable.button_half_circular_green);
                mDrawable.setColorFilter(new
                        PorterDuffColorFilter(colorForTimer, PorterDuff.Mode.SRC_IN));
                timeVg.setBackground(mDrawable);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
            }
        };
        countDownTimer.start();
    }

    private void closePlaceBid() {
        placeBidVg.setVisibility(View.GONE);
    }

    private void setUpPlaceBidScreen() {
        placeBidVg = findViewById(R.id.place_bid_vg);
        placeBidEt = placeBidVg.findViewById(R.id.enter_bid_tv);
        TextView sendToAuctionBtn = placeBidVg.findViewById(R.id.send_to_aution_btn);
        String biddedPrice = null;
        for (int i = 0; i < singleAuction.getBrokersInAuctionList().size(); i++) {
            if (singleAuction.getBrokersInAuctionList().get(i).getMitraiD().equalsIgnoreCase(
                    SessionUserDetails.getInstance().getMitraCode())) {
                biddedPrice = singleAuction.getBrokersInAuctionList().get(i).getBidPrice();
            }
        }
        if (null != biddedPrice) {
            enterBiddedTv.setVisibility(View.VISIBLE);
            rebidPriceLabel.setVisibility(View.VISIBLE);
            enterBiddedTv.setText(biddedPrice);
            sendToAuctionBtn.setText("Place New Bid");
            placeBidEt.setHint("Enter New Bid Price");
        } else {
            sendToAuctionBtn.setText("Place Bid");
            placeBidEt.setHint("Enter Bid Price");
        }

        ImageView closeBtn = placeBidVg.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePlaceBid();
            }
        });
        final String oldBiddedPrice = biddedPrice;
        if (null != singleAuction.getHighestBidStr() && !singleAuction.getHighestBidStr().equalsIgnoreCase(""))
            singleAuction.setHighestBid(Long.parseLong(singleAuction.getHighestBidStr()));
        sendToAuctionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bidPrice = placeBidEt.getText().toString();
                if (bidPrice.length() > 0) {
                    long bidPriceBase = 0;
                    long bidPriceInt = Long.parseLong(bidPrice);
                    if (bidPriceInt > 10000000) {
                        Toast.makeText(ActivityBrokerBidNow.this, "Please enter amount less than 1 Cr",
                                Toast.LENGTH_LONG).show();
                    } else {
                        if (null != oldBiddedPrice && !oldBiddedPrice.equalsIgnoreCase("")) {
                            bidPriceBase = Long.parseLong(oldBiddedPrice);
                        }
                        if (bidPriceInt > bidPriceBase) {
                            placeBidImplementation(placeBidEt.getText().toString().trim());
                        } else {
                            Toast.makeText(ActivityBrokerBidNow.this,
                                    "Enter Amount larger than your old price", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(ActivityBrokerBidNow.this, "Please enter valid Amount",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void placeBidImplementation(String bidAmount) {
        PlaceBidIP placeBidIP = new PlaceBidIP();
        placeBidIP.setUserId(SessionUserDetails.getInstance().getMitraCode());
        placeBidIP.setAuctionId(auctionId);
        placeBidIP.setBidAmount(bidAmount);
        bidNowPresenter.placeBidOnServer(placeBidIP);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putString("aucID", auctionId);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        auctionId = savedInstanceState.getString("aucID");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void settingCustomToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);
        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText("My Auction Details");
        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void gotEvaluationReport(String response) {
        reportDownloaded = true;
        downloadedReportId = response;
        if (showDetailsClicked) {
            showDetailsClicked = false;
            showEvalDetailsActivity();
        }
    }

    @Override
    public void gotErrorForEvaluationReport(String response) {
        Log.v("Evaluation Report error", response);
    }

    @Override
    public void placedBidOnServer(String response) {
        Toast.makeText(this, "Place Bid successfully", Toast.LENGTH_LONG).show();
        finish();
    }

    private void updaeUI() {
        String mitraToken = SessionUserDetails.getInstance().getMitraCode();
        swipeRefreshLayout.setRefreshing(false);
        ArrayList<BrokerInAuction> brokerInAuctionsList = singleAuction.getBrokersInAuctionList();
        boolean bidded = false;
        for (int i = 0; i < brokerInAuctionsList.size(); i++) {
            String mitraId = brokerInAuctionsList.get(i).getMitraiD();
            if (mitraToken.equalsIgnoreCase(mitraId)) {
                bidded = true;
                break;
            }
        }
        if (bidded) {
            if (singleAuction.isHasWon()) {
                highest_bidder.setText("You are the highest Bidder");
            } else {
                highest_bidder.setText("You are not the highest Bidder");
            }
        } else {
            highest_bidder.setVisibility(View.GONE);
        }

        textViewVehicleUsage.setText(String.format("Vehicle Usage - %s",
                singleAuction.getVehicleUsage()));

        if (singleAuction.getAuctionStatus().equalsIgnoreCase("COM")) {
            placeBidBtn.setVisibility(View.GONE);
            rebidBtn.setVisibility(View.GONE);
            highest_bidder.setVisibility(View.GONE);
            closedBidVg.setVisibility(View.VISIBLE);
            evalBtn.setVisibility(View.VISIBLE);
            tvClosedBidPrice.setText(singleAuction.getHighestBidStr());
            if (bidded) {
                if (singleAuction.isHasWon()) {
                    wonVg.setVisibility(View.VISIBLE);
                    lossVg.setVisibility(View.GONE);
                    callBtn.setVisibility(View.VISIBLE);
                    callBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CommonHelper.makeCall(contactNum, ActivityBrokerBidNow.this);
                        }
                    });
                } else {
                    callBtn.setVisibility(View.GONE);
                    wonVg.setVisibility(View.GONE);
                    lossVg.setVisibility(View.VISIBLE);
                }
            }
        } else {
            closedBidVg.setVisibility(View.GONE);
            if (!bidded) {
                placeBidBtn.setVisibility(View.VISIBLE);
                rebidBtn.setVisibility(View.GONE);
            } else {
                placeBidBtn.setVisibility(View.GONE);
                rebidBtn.setVisibility(View.VISIBLE);
            }
        }

        if (null != singleAuction.getImageUrl()) {
            if (!singleAuction.getImageUrl().isEmpty()) {
                Glide.with(vehicleImage.getContext())
                        .load(CommonHelper.getAuthenticatedUrlForGlide(singleAuction.getImageUrl()))
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(vehicleImage);
            } else {
                Glide.with(vehicleImage.getContext())
                        .load(R.mipmap.mahindra_vehicle)
                        .apply(CommonHelper.getGlideErrorImage())
                        .into(vehicleImage);
            }
        }
        if (null != singleAuction.getVehicleName()) {
            personNameTv.setText(singleAuction.getVehicleName());
        }
        if (null != singleAuction.getNoOfOwners()) {
            textViewNoOfOwners.setText(String.format("No of owners : %s", singleAuction.getNoOfOwners()));
            textViewOdometer.setText(String.format("Odometer(kms) : %s", singleAuction.getOdometerReading()));
            try {
                if (singleAuction.getFeedbackRating().trim().length() > 0)
                    ratingFeedback.setRating(Float.parseFloat(singleAuction.getFeedbackRating()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }
        StringBuilder stringBuilder = new StringBuilder();
        if (null != singleAuction.getMake() && null != singleAuction.getModel()) {
            stringBuilder.append(" ").append(singleAuction.getMake())
                    .append(" ").append(singleAuction.getModel());
        }
        vehicleDetails.setText(stringBuilder.toString());
    }

    @Override
    public void gotAuctionDataById(String response) {
        try {
            swipeRefreshLayout.setRefreshing(false);
            JSONObject jsonObject = new JSONObject(response);
            singleAuction = Parsers.parseSingleAuction(jsonObject);
            singleAuction.setAuctionId(auctionId);
            updaeUI();

            BrokerEvaluationDetailsPresenter brokerEvaluationDetailsPresenter = new BrokerEvaluationDetailsPresenter(this, this);
            brokerEvaluationDetailsPresenter.getEvaluationReport(singleAuction.getEvaluationReportId(),
                    singleAuction.getRequestId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(String response, int requestId) {
        swipeRefreshLayout.setRefreshing(false);
        if (requestId == 0) {
            try {
                JSONObject json = new JSONObject(response);
                String message = json.getString("message");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (requestId == 1) {
            try {
                JSONObject json = new JSONObject(response);
                String message = json.getString("message");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}