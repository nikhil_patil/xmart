package com.mahindra.mitra20.module_broker.models.serializable;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.io.Serializable;
import java.util.ArrayList;

public class AuctionsMessengerObject implements Serializable{
    private ArrayList<SingleAuction> filteredAuctionList;

    public ArrayList<SingleAuction> getFilteredAuctionList() {
        return filteredAuctionList;
    }

    public void setFilteredAuctionList(ArrayList<SingleAuction> filteredAuctionList) {
        this.filteredAuctionList = filteredAuctionList;
    }
}
