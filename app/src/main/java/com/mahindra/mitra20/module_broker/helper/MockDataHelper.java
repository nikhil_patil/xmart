package com.mahindra.mitra20.module_broker.helper;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public class MockDataHelper {
    public static ArrayList<SingleAuction>  getLiveAuctionsData(){
        ArrayList<SingleAuction> auctionsList = new ArrayList<>();

        SingleAuction auction1 = new SingleAuction();
        auction1.setDate("23/08/2018");
        auction1.setAuctionId("ACN19A000012");
        auction1.setAuctionStatus("LIVE");
        auction1.setCountOfBidders("3");
        auction1.setMitraBasePrice("32542");
        auction1.setRequestId("ENQ19A000290");
        auction1.setStartTimeStamp("09:55:12 AM");
        auction1.setStartTimeStamp("11:55:12 AM");
        auction1.setRemainingTimeInSeconds("1981");
        auction1.setVehicleOwnerName("Test");
        auction1.setVehicleName("BOL");

        SingleAuction auction2 = new SingleAuction();
        auction2.setDate("23/08/2018");
        auction2.setAuctionId("ACN19A000012");
        auction2.setAuctionStatus("BIDDED");
        auction2.setCountOfBidders("3");
        auction2.setMitraBasePrice("32542");
        auction2.setRequestId("ENQ19A000290");
        auction2.setStartTimeStamp("09:55:12 AM");
        auction2.setStartTimeStamp("11:55:12 AM");
        auction2.setRemainingTimeInSeconds("1981");
        auction2.setVehicleOwnerName("Test");
        auction2.setVehicleName("BOL");


        SingleAuction auction3 = new SingleAuction();
        auction3.setDate("23/08/2018");
        auction3.setAuctionId("ACN19A000012");
        auction3.setAuctionStatus("CLOSED");
        auction3.setCountOfBidders("3");
        auction3.setMitraBasePrice("32542");
        auction3.setRequestId("ENQ19A000290");
        auction3.setStartTimeStamp("09:55:12 AM");
        auction3.setStartTimeStamp("11:55:12 AM");
        auction3.setRemainingTimeInSeconds("1981");
        auction3.setVehicleOwnerName("Test");
        auction3.setVehicleName("BOL");

        auctionsList.add(auction1);
        auctionsList.add(auction2);
        auctionsList.add(auction3);

        return auctionsList;
    }

    public static ArrayList<LeadDetail>  getLeadsData(){
        ArrayList<LeadDetail> leadsList = new ArrayList<>();

        LeadDetail lead  = new LeadDetail();
        lead.setIntendedDateOfPurchase("GT15");
        lead.setInterestedInExchange("Y");
        lead.setInterestedInWhichMahindraVehicle("MAHINDRA TUV300 T4 WHITE");
        lead.setLeadId("ENQ19A000111");
        lead.setLocationCode("MV01");
        lead.setMobileNo("5656564545");
        lead.setPreOwnedVehicleModel("FW-FW5");
        lead.setPinCode("682030");
        lead.setVehicleSegment("Personal");


        LeadDetail lead2  = new LeadDetail();
        lead2.setIntendedDateOfPurchase("GT15");
        lead2.setInterestedInExchange("Y");
        lead2.setInterestedInWhichMahindraVehicle("MAHINDRA TUV300 T4 WHITE");
        lead2.setLeadId("ENQ19A000111");
        lead2.setLocationCode("MV01");
        lead2.setMobileNo("5656564545");
        lead2.setPreOwnedVehicleModel("FW-FW5");
        lead2.setPinCode("682030");
        lead2.setVehicleSegment("Personal");


        leadsList.add(lead);
        leadsList.add(lead2);
        return leadsList;
    }
}
