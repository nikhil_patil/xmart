package com.mahindra.mitra20.module_broker.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyLeadsResponse {
    @SerializedName("AssignedEnquiryDetails")
    private ArrayList<AssignedEnquiryDetails> assignedEnquiryDetails;

    @SerializedName("UnassignedLeads")
    private int unassignedLeadsCount;

    @SerializedName("UnassignedRequestCountDetails")
    private ArrayList<UnassignedLeadDetails> unassignedLeadDetails;

    private String messageCode;

    @SerializedName("AssignedEnquiries")
    private int assignedEnquiriesCount;

    private String message;

    private String IsSuccessful;

    public ArrayList<AssignedEnquiryDetails> getAssignedEnquiryDetails() {
        return assignedEnquiryDetails;
    }

    public void setAssignedEnquiryDetails(ArrayList<AssignedEnquiryDetails> assignedEnquiryDetails) {
        this.assignedEnquiryDetails = assignedEnquiryDetails;
    }

    public int getUnassignedLeadsCount() {
        return unassignedLeadsCount;
    }

    public void setUnassignedLeadsCount(int UnassignedLeads) {
        this.unassignedLeadsCount = UnassignedLeads;
    }

    public ArrayList<UnassignedLeadDetails> getUnassignedLeadDetails() {
        return unassignedLeadDetails;
    }

    public void setUnassignedLeadDetails(ArrayList<UnassignedLeadDetails> unassignedLeadDetails) {
        this.unassignedLeadDetails = unassignedLeadDetails;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public int getAssignedEnquiriesCount() {
        return assignedEnquiriesCount;
    }

    public void setAssignedEnquiriesCount(int AssignedEnquiries) {
        this.assignedEnquiriesCount = AssignedEnquiries;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIsSuccessful() {
        return IsSuccessful;
    }

    public void setIsSuccessful(String IsSuccessful) {
        this.IsSuccessful = IsSuccessful;
    }

}