package com.mahindra.mitra20.module_broker.presenters;

import org.json.JSONObject;

public interface AuctionDashBoardSIngleTabIn {
    void gotParticularAuctionData(JSONObject jsonResponse);
}
