package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

import java.util.ArrayList;

public class    LiveAuctionOP {
    private ArrayList<SingleAuction> auctionDetailsList;
    private String auctionId;
    private String  auctionStatus;
    private String countOfBidders;

    public ArrayList<SingleAuction> getAuctionDetailsList() {
        return auctionDetailsList;
    }

    public void setAuctionDetailsList(ArrayList<SingleAuction> auctionDetailsList) {
        this.auctionDetailsList = auctionDetailsList;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public String getAuctionStatus() {
        return auctionStatus;
    }

    public void setAuctionStatus(String auctionStatus) {
        this.auctionStatus = auctionStatus;
    }

    public String getCountOfBidders() {
        return countOfBidders;
    }

    public void setCountOfBidders(String countOfBidders) {
        this.countOfBidders = countOfBidders;
    }
}
