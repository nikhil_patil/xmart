package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

import java.util.ArrayList;

public class UserBroker {
    private String amountEarned;
    private long amountPending;
    private String category;
    private String mmtSegment;
    private String messageCode;
    private String mitraCode;
    private String mitraName;
    private String mitraSegment;
    private String mitraType;
    private String mitraTypeDesc;
    private String noOfRetailsConverted;
    private String parentCode;
    private String parentName;
    private ArrayList<LocationList> locationList;

    public String getAmountEarned() {
        return amountEarned;
    }

    public void setAmountEarned(String amountEarned) {
        this.amountEarned = amountEarned;
    }

    public long getAmountPending() {
        return amountPending;
    }

    public void setAmountPending(long amountPending) {
        this.amountPending = amountPending;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMmtSegment() {
        return mmtSegment;
    }

    public void setMmtSegment(String mmtSegment) {
        this.mmtSegment = mmtSegment;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMitraCode() {
        return mitraCode;
    }

    public void setMitraCode(String mitraCode) {
        this.mitraCode = mitraCode;
    }

    public String getMitraName() {
        return mitraName;
    }

    public void setMitraName(String mitraName) {
        this.mitraName = mitraName;
    }

    public String getMitraSegment() {
        return mitraSegment;
    }

    public void setMitraSegment(String mitraSegment) {
        this.mitraSegment = mitraSegment;
    }

    public String getMitraType() {
        return mitraType;
    }

    public void setMitraType(String mitraType) {
        this.mitraType = mitraType;
    }

    public String getMitraTypeDesc() {
        return mitraTypeDesc;
    }

    public void setMitraTypeDesc(String mitraTypeDesc) {
        this.mitraTypeDesc = mitraTypeDesc;
    }

    public String getNoOfRetailsConverted() {
        return noOfRetailsConverted;
    }

    public void setNoOfRetailsConverted(String noOfRetailsConverted) {
        this.noOfRetailsConverted = noOfRetailsConverted;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public ArrayList<LocationList> getLocationList() {
        return locationList;
    }

    public void setLocationList(ArrayList<LocationList> locationList) {
        this.locationList = locationList;
    }
}
