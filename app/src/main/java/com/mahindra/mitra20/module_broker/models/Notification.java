package com.mahindra.mitra20.module_broker.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by PATINIK-CONT on 25-Sep-18.
 */
public class Notification implements Parcelable {
    private String _ID = "id";
    private String title = "title";
    private String message = "message";
    private String eventId = "eventId";
    private String payload = "payload";
    private String dateTime = "dateTime";
    private boolean isViewed = false;

    public Notification() {

    }

    protected Notification(Parcel in) {
        _ID = in.readString();
        title = in.readString();
        message = in.readString();
        eventId = in.readString();
        payload = in.readString();
        dateTime = in.readString();
        isViewed = in.readByte() != 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isViewed() {
        return isViewed;
    }

    public void setViewed(boolean viewed) {
        isViewed = viewed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_ID);
        parcel.writeString(title);
        parcel.writeString(message);
        parcel.writeString(eventId);
        parcel.writeString(payload);
        parcel.writeString(dateTime);
        parcel.writeByte((byte) (isViewed ? 1 : 0));
    }
}
