package com.mahindra.mitra20.module_broker.helper;

import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestResponseKeys;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.BrokerInAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BiddersListHelper {

    public static ArrayList<BrokerInAuction> getBiddersList(JSONArray biddersJsonArray ){
        ArrayList<BrokerInAuction> biddersList = new ArrayList<>();
        for(int i=0;i<biddersJsonArray.length();i++ ){
            try {
                JSONObject bidderJsonObj = (JSONObject) biddersJsonArray.get(i);
                String mitraId = (String) bidderJsonObj.get(RestResponseKeys.LiveAuctionOutputKeys.MitraId);
                String mitraName = (String) bidderJsonObj.get(RestResponseKeys.LiveAuctionOutputKeys.MitraName);
                String bidPrice = (String) bidderJsonObj.get(RestResponseKeys.LiveAuctionOutputKeys.bidPrice);
                String biddingDateTime = (String) bidderJsonObj.get(RestResponseKeys.LiveAuctionOutputKeys.biddingDateTime);

                BrokerInAuction brokerInAuction = new BrokerInAuction();
                brokerInAuction.setMitraiD(mitraId);
                brokerInAuction.setMitraName(mitraName);
                brokerInAuction.setBidPrice(bidPrice);
                brokerInAuction.setBiddingDateTime(biddingDateTime);
                biddersList.add(brokerInAuction);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return biddersList;
    }

    public static boolean checkToseeIfBrokerHasBidded(ArrayList<BrokerInAuction> biddersList){
        String mitraId = SessionUserDetails.getInstance().getMitraCode();

        boolean present = false;

        for(int i=0;i<biddersList.size();i++){
            BrokerInAuction brokerInAuction = biddersList.get(i);
            String mitraIdInData = brokerInAuction.getMitraiD();
            if(mitraIdInData.equalsIgnoreCase(mitraId)){
                return true;

            }
        }
        return false;

    }

    public static boolean checkToseeIfBrokerisTheHighestBidder(ArrayList<BrokerInAuction> biddersList){
        String mitraId = SessionUserDetails.getInstance().getMitraCode();

        ArrayList<Long> mitraPrices  = new ArrayList<>();

        long bidPrice =0;

        for(int i=0;i<biddersList.size();i++){
            BrokerInAuction brokerInAuction = biddersList.get(i);
            long bidPriceinData = Long.parseLong(brokerInAuction.getBidPrice());

                if(bidPrice > bidPriceinData){
                    bidPrice = bidPriceinData;

                }
        }
        return false;

    }
}
