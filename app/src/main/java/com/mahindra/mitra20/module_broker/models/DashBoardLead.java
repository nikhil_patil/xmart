package com.mahindra.mitra20.module_broker.models;

public class DashBoardLead {
    private int id;
    private  String description;
    private int relatedViewId;
    private int textColor;
    private int leadsCount = 0;

    public interface Types {
        String UNASSIGNED = "Unassigned Leads";
        String HOT = "Hot";
        String COLD = "Cold";
        String WARM = "Warm";
        String ORDERED = "Ordered";
        String INVOICED = "Invoiced";
        String LOST = "Lost";
    }

    public interface Colors {
        int UNASSIGNED_COLOR = 0xFF000000;
        int HOT_COLOR = 0xFFFF0000;
        int COLD_COLOR = 0xFF87CEEB;
        int WARM_COLOR = 0xFFFE7401;
        int ORDERED_COLOR = 0xFFFEB501;
        int INVOICED_COLOR = 0xFF800080;
        int LOST_COLOR = 0xFFD3D3D3;
    }

    public DashBoardLead(int id, int leadsNum, String description, int relatedViewId, int textColor){
        setId(id);
        setLeadsCount(leadsNum);
        setDescription(description);
        setRelatedViewId(relatedViewId);
        setTextColor(textColor);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRelatedView() {
        return relatedViewId;
    }

    public int getRelatedViewId() {
        return relatedViewId;
    }

    public void setRelatedViewId(int relatedViewId) {
        this.relatedViewId = relatedViewId;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getLeadsCount() {
        return leadsCount;
    }

    public void setLeadsCount(int leadsCount) {
        this.leadsCount = leadsCount;
    }
}