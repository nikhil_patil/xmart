package com.mahindra.mitra20.module_broker.constants;

public class ExtrasKeys {

    public class General {
        public static final String AUCTION_SINGLE_DATA = "auction_single";
        public static final String AUCTION_DEFAULT_PAGE = "auction_default_page";
    }

    public class LeadsViewAll {
        public static final String DEFAULT_PAGE = "deafult_page";
        public static final String LEADS_DATA = "leads_data";
    }

    public class LeadsStatus {
        public static final String ONE_LEAD_DETAIL = "one_lead_detail";
    }
}
