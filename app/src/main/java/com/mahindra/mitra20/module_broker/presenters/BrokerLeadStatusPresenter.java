package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

public class BrokerLeadStatusPresenter {
    private BrokerLeadStatusIn brokerLeadStatusView;
    private Context context;

    public BrokerLeadStatusPresenter(Context context, BrokerLeadStatusIn brokerLeadStatusIn){
        this.brokerLeadStatusView = brokerLeadStatusIn;
        this.context = context;
    }

    public void setCustomToolbar(){
        brokerLeadStatusView.settingCustomToolBar();
    }

    public void setView(){
        brokerLeadStatusView.VisualElementsReady();
    }

    public void attachDataToViews(){
        brokerLeadStatusView.attachingDataToViews();
    }





}
