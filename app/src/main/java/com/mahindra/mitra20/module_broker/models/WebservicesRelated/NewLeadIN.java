package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

public class NewLeadIN {
    private String Parentcode;
    private String Locationcode;
    private String enquiryType;
    private String enquirySource;
    private String customerName;
    private String mobileNumber;
    private String email;
    private String pinCode;
    private String modelgroup;
    private String likelyPurchase;
    private String uploadDate;
    private String EnquiryVerificationType;
    private String Eventcode;
    private String followupDateTime;
    private String remarks;
    private String interstedInExchange;
    private String exchangeMake;
    private String exchangeModel;
    private String mitraId;

    public String getParentCode() {
        return Parentcode;
    }

    public void setParentCode(String parentCode) {
        this.Parentcode = parentCode;
    }

    public String getLocationCode() {
        return Locationcode;
    }

    public void setLocationCode(String locationCode) {
        this.Locationcode = locationCode;
    }

    public String getEnquiryType() {
        return enquiryType;
    }

    public void setEnquiryType(String enquiryType) {
        this.enquiryType = enquiryType;
    }

    public String getEnquirySource() {
        return enquirySource;
    }

    public void setEnquirySource(String enquirySource) {
        this.enquirySource = enquirySource;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getModelgroup() {
        return modelgroup;
    }

    public void setModelgroup(String modelgroup) {
        this.modelgroup = modelgroup;
    }

    public String getLikelyPurchase() {
        return likelyPurchase;
    }

    public void setLikelyPurchase(String likelyPurchase) {
        this.likelyPurchase = likelyPurchase;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getEnquiryVerificationType() {
        return EnquiryVerificationType;
    }

    public void setEnquiryVerificationType(String enquiryVerificationType) {
        EnquiryVerificationType = enquiryVerificationType;
    }

    public String getEventcode() {
        return Eventcode;
    }

    public void setEventcode(String eventcode) {
        Eventcode = eventcode;
    }

    public String getFollowupDateTime() {
        return followupDateTime;
    }

    public void setFollowupDateTime(String followupDateTime) {
        this.followupDateTime = followupDateTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getInterstedInExchange() {
        return interstedInExchange;
    }

    public void setInterstedInExchange(String interstedInExchange) {
        this.interstedInExchange = interstedInExchange;
    }

    public String getExchangeMake() {
        return exchangeMake;
    }

    public void setExchangeMake(String exchangeMake) {
        this.exchangeMake = exchangeMake;
    }

    public String getExchangeModel() {
        return exchangeModel;
    }

    public void setExchangeModel(String exchangeModel) {
        this.exchangeModel = exchangeModel;
    }

    public String getMitraId() {
        return mitraId;
    }

    public void setMitraId(String mitraId) {
        this.mitraId = mitraId;
    }
}
