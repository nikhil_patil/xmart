package com.mahindra.mitra20.module_broker.presenters;

import android.content.Context;

import com.mahindra.mitra20.api.VolleySingleTon;
import com.mahindra.mitra20.module_broker.constants.RestInputKeys;
import com.mahindra.mitra20.module_broker.constants.UrlConstants;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.LiveAuctionIP;

import org.json.JSONException;
import org.json.JSONObject;

public class BrokerAuctionsViewAllPresenter implements VolleySingleTon.VolleyInteractorAuctions {
    private BrokerAuctionsViewAllIn brokerAuctionsViewAllView;
    private Context context;

    public BrokerAuctionsViewAllPresenter(Context context, BrokerAuctionsViewAllIn brokerAuctionsViewAllView) {
        this.brokerAuctionsViewAllView = brokerAuctionsViewAllView;
        this.context = context;
    }

    public void setCustomToolbar() {
        brokerAuctionsViewAllView.settingCustomToolBar();
    }

    public void getAuctionsData(LiveAuctionIP liveAuctionIP, int auctionId) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.FromDate, liveAuctionIP.getFromDate());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.toDate, liveAuctionIP.getToDate());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.AuctionStatus, liveAuctionIP.getAuctionStatus());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Make, liveAuctionIP.getMake());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Model, liveAuctionIP.getModel());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.countToDisplay, liveAuctionIP.getCountToDisplay());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.Sorting, liveAuctionIP.getSorting());
            jsonObj.put(RestInputKeys.LiveAuctionInputKeys.UserID, liveAuctionIP.getUserId());

            String url = UrlConstants.BASE_URL + UrlConstants.LIVE_AUCTION_MASTER;
            VolleySingleTon.getInstance().connectToPostUrl3ForAuctions(context, this, url,
                    jsonObj, 0, auctionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setViews() {
        brokerAuctionsViewAllView.VisualElementsReady();
    }

    @Override
    public void gotSuccessResponseForAuctions(int requestId, String response, final int auctionId) {
        JSONObject responseJsonObj = null;
        try {
            responseJsonObj = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        brokerAuctionsViewAllView.gotAuctionsData(auctionId, responseJsonObj);
    }

    @Override
    public void gotErrorResponse(int requestId, String response) {
        brokerAuctionsViewAllView.gotAuctionAPIError(response);
    }
}
