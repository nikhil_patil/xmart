package com.mahindra.mitra20.module_broker.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AssignedEnquiryDetails implements Parcelable {
    private String Status;

    private String SalesConsultantNumber;

    private String EnquiryType;

    private String EnquirySource;

    private String InterestedInVehicle;

    private String Model;

    private String EnquiryID;

    private String Make;

    private String CustomerName;

    private String SalesConsultantName;

    private String DealerName;

    protected AssignedEnquiryDetails(Parcel in) {
        Status = in.readString();
        SalesConsultantNumber = in.readString();
        EnquiryType = in.readString();
        EnquirySource = in.readString();
        InterestedInVehicle = in.readString();
        Model = in.readString();
        EnquiryID = in.readString();
        Make = in.readString();
        CustomerName = in.readString();
        SalesConsultantName = in.readString();
        DealerName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Status);
        dest.writeString(SalesConsultantNumber);
        dest.writeString(EnquiryType);
        dest.writeString(EnquirySource);
        dest.writeString(InterestedInVehicle);
        dest.writeString(Model);
        dest.writeString(EnquiryID);
        dest.writeString(Make);
        dest.writeString(CustomerName);
        dest.writeString(SalesConsultantName);
        dest.writeString(DealerName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AssignedEnquiryDetails> CREATOR = new Creator<AssignedEnquiryDetails>() {
        @Override
        public AssignedEnquiryDetails createFromParcel(Parcel in) {
            return new AssignedEnquiryDetails(in);
        }

        @Override
        public AssignedEnquiryDetails[] newArray(int size) {
            return new AssignedEnquiryDetails[size];
        }
    };

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getSalesConsultantNumber() {
        return SalesConsultantNumber;
    }

    public void setSalesConsultantNumber(String SalesConsultantNumber) {
        this.SalesConsultantNumber = SalesConsultantNumber;
    }

    public String getEnquiryType() {
        return EnquiryType;
    }

    public void setEnquiryType(String EnquiryType) {
        this.EnquiryType = EnquiryType;
    }

    public String getEnquirySource() {
        return EnquirySource;
    }

    public void setEnquirySource(String EnquirySource) {
        this.EnquirySource = EnquirySource;
    }

    public String getInterestedInVehicle() {
        return InterestedInVehicle;
    }

    public void setInterestedInVehicle(String InterestedInVehicle) {
        this.InterestedInVehicle = InterestedInVehicle;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getEnquiryID() {
        return EnquiryID;
    }

    public void setEnquiryID(String EnquiryID) {
        this.EnquiryID = EnquiryID;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getSalesConsultantName() {
        return SalesConsultantName;
    }

    public void setSalesConsultantName(String SalesConsultantName) {
        this.SalesConsultantName = SalesConsultantName;
    }

    public String getDealerName() {
        return DealerName;
    }

    public void setDealerName(String DealerName) {
        this.DealerName = DealerName;
    }

    @Override
    public String toString() {
        return "ClassPojo [Status = " + Status + ", SalesConsultantNumber = " + SalesConsultantNumber + ", EnquiryType = " + EnquiryType + ", EnquirySource = " + EnquirySource + ", InterestedInVehicle = " + InterestedInVehicle + ", Model = " + Model + ", EnquiryID = " + EnquiryID + ", Make = " + Make + ", CustomerName = " + CustomerName + ", SalesConsultantName = " + SalesConsultantName + ", DealerName = " + DealerName + "]";
    }
}
	