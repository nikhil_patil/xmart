package com.mahindra.mitra20.module_broker.interfaces;

import android.widget.ProgressBar;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public interface AuctionActivityJobs {
    void fetchAuctionsData(String type);

    ArrayList<SingleAuction> getAuctionList(int page);

    void hideProgressBar(ProgressBar progressBar);

    void showProgressBar(ProgressBar progressBar);

    void setTabs(int page, int auctionsCount);
}
