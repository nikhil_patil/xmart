package com.mahindra.mitra20.module_broker.helper;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;

import java.util.ArrayList;
import java.util.Collection;

public class FilterLeadsList {

    public static ArrayList<LeadDetail> filterListByStatus(
            ArrayList<LeadDetail> originalLeadDetailList, String statusToFilter) {
        ArrayList<LeadDetail> filteredList = new ArrayList<>();
        for (int i = 0; i < originalLeadDetailList.size(); i++) {
            LeadDetail leadDetail = originalLeadDetailList.get(i);
            String leadStatus = leadDetail.getLeadStatus();
            if (leadStatus.equalsIgnoreCase(statusToFilter)) {
                filteredList.add(leadDetail);
            }
        }
        return filteredList;
    }

    public interface Predicate<T> {
        boolean apply(T type);
    }

    public static <T> Collection<T> filter(Collection<T> col, Predicate<T> predicate) {
        Collection<T> result = new ArrayList<T>();
        for (T element : col) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }
}