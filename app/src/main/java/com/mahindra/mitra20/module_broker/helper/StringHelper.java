package com.mahindra.mitra20.module_broker.helper;

public class StringHelper {
    public static String getDoubleDigit(String orgStr){
        String newStr = orgStr;
        if(orgStr.length() == 1){
            newStr = "0" + orgStr;
        }
        return newStr;
    }
}
