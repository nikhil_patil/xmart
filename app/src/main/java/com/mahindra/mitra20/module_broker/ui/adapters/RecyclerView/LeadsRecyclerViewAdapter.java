package com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mahindra.mitra20.champion.interfaces.OnCallListener;
import com.mahindra.mitra20.databinding.LeadsListItemNewBinding;
import com.mahindra.mitra20.databinding.LeadsListItemOldBinding;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.models.AssignedEnquiryDetails;
import com.mahindra.mitra20.module_broker.models.UnassignedLeadDetails;

import java.util.List;

public class LeadsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnCallListener {

    private Context context;
    private List<AssignedEnquiryDetails> enquiries;
    private List<UnassignedLeadDetails> leads;
    private int type;

    public LeadsRecyclerViewAdapter(Context context,
                                    List<AssignedEnquiryDetails> enquiryDetails,
                                    List<UnassignedLeadDetails> leadDetails, int type) {
        this.context = context;
        this.enquiries = enquiryDetails;
        this.leads = leadDetails;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (type == 1) {
            LeadsListItemOldBinding binding = LeadsListItemOldBinding.inflate(layoutInflater, parent, false);
            return new EnquiriesViewHolder(binding);
        } else {
            LeadsListItemNewBinding binding = LeadsListItemNewBinding.inflate(layoutInflater, parent, false);
            return new LeadsViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        if (type == 1) {
            EnquiriesViewHolder holder = (EnquiriesViewHolder) holder1;
            final AssignedEnquiryDetails assignedEnquiryDetails = enquiries.get(position);
            holder.binding.setEnquiry(assignedEnquiryDetails);
        } else {
            LeadsViewHolder holder = (LeadsViewHolder) holder1;
            final UnassignedLeadDetails unassignedLeadDetails = leads.get(position);
            holder.binding.setLead(unassignedLeadDetails);
        }
    }

    @Override
    public int getItemCount() {
        if (type == 1 && enquiries != null) {
            return enquiries.size();
        } else if (leads != null) {
            return leads.size();
        }
        return 0 ;
    }

    @Override
    public void onCallPressed(String mobileNumber) {
        CommonHelper.makeCall(mobileNumber, context);
    }

    class LeadsViewHolder extends RecyclerView.ViewHolder {
        LeadsListItemNewBinding binding;

        LeadsViewHolder(LeadsListItemNewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    class EnquiriesViewHolder extends RecyclerView.ViewHolder {
        LeadsListItemOldBinding binding;

        EnquiriesViewHolder(LeadsListItemOldBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setCallListener(LeadsRecyclerViewAdapter.this);
        }
    }
}