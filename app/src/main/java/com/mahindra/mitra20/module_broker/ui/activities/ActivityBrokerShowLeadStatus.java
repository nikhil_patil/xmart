package com.mahindra.mitra20.module_broker.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;
import com.mahindra.mitra20.module_broker.presenters.BrokerLeadStatusIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerLeadStatusPresenter;

public class ActivityBrokerShowLeadStatus extends AppCompatActivity implements BrokerLeadStatusIn {

    private BrokerLeadStatusPresenter brokerLeadStatusPresenter;
    private LeadDetail leadDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitra_lead_status);
        leadDetail = (LeadDetail) getIntent().getSerializableExtra(ExtrasKeys.LeadsStatus.ONE_LEAD_DETAIL);
        init();
    }

    private void init() {
        brokerLeadStatusPresenter = new BrokerLeadStatusPresenter(this, this);
        brokerLeadStatusPresenter.setView();
        brokerLeadStatusPresenter.setCustomToolbar();
        brokerLeadStatusPresenter.attachDataToViews();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void VisualElementsReady() {

    }

    @Override
    public void theseItemsAreClickable() {

    }

    @Override
    public void settingCustomToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        //actionBar.setDisplayShowTitleEnabled(false);
        //View customView = getLayoutInflater().inflate(R.layout.temp_tool_bar_mitra, null);
        // actionBar.setCustomView(customView);
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent =(Toolbar) customView.getParent();
        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0,0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText("Lead Status");

        ImageView backbtn = customView.findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void attachingDataToViews() {
        TextView dealerNameValueTv = findViewById(R.id.dealer_name_value_tv);
        TextView sales_name_value_tv = findViewById(R.id.sales_name_value_tv);
        TextView lead_num_value_tv = findViewById(R.id.lead_num_value_tv);
        TextView status_value_tv = findViewById(R.id.status_value_tv);
        TextView date_lead_value_tv = findViewById(R.id.date_lead_value_tv);
        TextView booking_date_value_tv = findViewById(R.id.booking_date_value_tv);
        TextView Invoiced_date_label_tv = findViewById(R.id.Invoiced_date_label_tv);
        TextView customer_name_value_tv = findViewById(R.id.customer_name_value_tv);
        TextView Interested_in_value_tv = findViewById(R.id.Interested_in_value_tv);
        TextView mob_value_tv = findViewById(R.id.mob_value_tv);

        ImageView phSalesExecutive = findViewById(R.id.phone_iv_1);
        ImageView phCustomer = findViewById(R.id.phone_iv_2);
        ImageView messageCustomer = findViewById(R.id.message_iv);
        final String salesExecutiverNum = leadDetail.getSalesExecutiveNum();
        final String customerMobileNum = leadDetail.getMobileNo();

        phSalesExecutive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.makeCall(salesExecutiverNum, ActivityBrokerShowLeadStatus.this);
            }
        });

        phCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.makeCall(customerMobileNum, ActivityBrokerShowLeadStatus.this);
            }
        });

        messageCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.sendMessage(customerMobileNum, ActivityBrokerShowLeadStatus.this);
            }
        });

        dealerNameValueTv.setText(leadDetail.getDealerName());
        sales_name_value_tv.setText(leadDetail.getSalesExecutiveName());
        lead_num_value_tv.setText(leadDetail.getLeadId());
        status_value_tv.setText(leadDetail.getLeadStatus());
        date_lead_value_tv.setText(leadDetail.getIntendedDateOfPurchase());
        booking_date_value_tv.setText(leadDetail.getIntendedDateOfPurchase());
       customer_name_value_tv.setText(leadDetail.getNameOfProspectiveCustomer());
        Interested_in_value_tv.setText(leadDetail.getInterestedInWhichMahindraVehicle());
        mob_value_tv.setText(leadDetail.getMobileNo());
        Invoiced_date_label_tv.setText("NA");

    }
}
