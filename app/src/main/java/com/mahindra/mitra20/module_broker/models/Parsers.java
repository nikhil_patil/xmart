package com.mahindra.mitra20.module_broker.models;

import com.mahindra.mitra20.champion.helper.SpinnerHelper;
import com.mahindra.mitra20.constants.SpinnerConstants;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.RestResponseKeys;
import com.mahindra.mitra20.module_broker.helper.AuctionHelper;
import com.mahindra.mitra20.module_broker.helper.AuctionsSortComparator;
import com.mahindra.mitra20.module_broker.interfaces.LeadTypesIn;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.BrokerInAuction;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class Parsers {
    private static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);

    public static ArrayList<SingleAuction> parseAuctionsData(JSONObject reponse) {
        ArrayList<SingleAuction> liveAuctionList = new ArrayList<>();
        try {
            JSONArray jsonArray = reponse.optJSONArray(RestResponseKeys.LiveAuctionOutputKeys.AuctionDetails);
            if (null != jsonArray) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                    String auctionDate = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionDate);
                    String auctionId = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionID);
                    String auctionStatus = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionStatus);
                    String countofbidders = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.CountOfBidders);
                    String mitraBasePrice = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.MITRAprice_aseprice);
                    JSONArray biddersListJson = jsonObj.getJSONArray(RestResponseKeys.LiveAuctionOutputKeys.LIST);
                    String HighestPrice = jsonObj.getString("HighestPrice");
                    String Make = jsonObj.getString("Make");
                    String Model = jsonObj.getString("Model");
                    String vehicleImage = jsonObj.getString("vehiclePicURL");

                    String startTimestamp = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.StartTimestamp);
                    String requestID = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.RequestID);
                    String remainingtimeinseconds = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.remainingTimeInSeconds);
                    String vehOwnerName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehOwnerName);
                    String vehicleName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehicleName);
                    String championContactNum = jsonObj.getString("ChampionContactnumber");

                    SingleAuction singleAuction = new SingleAuction();
                    singleAuction.setDate(auctionDate);
                    singleAuction.setAuctionId(auctionId);
                    singleAuction.setAuctionStatus(auctionStatus);
                    singleAuction.setCountOfBidders(countofbidders);
                    singleAuction.setMitraBasePrice(mitraBasePrice);
                    singleAuction.setMake(Make);
                    singleAuction.setModel(Model);
                    singleAuction.setStartTimeStamp(startTimestamp);
                    singleAuction.setAuctionStatus(auctionStatus);
                    singleAuction.setEvaluationReportId(requestID);
                    singleAuction.setRequestId(requestID);
                    singleAuction.setRemainingTimeInSeconds(remainingtimeinseconds);
                    singleAuction.setVehicleOwnerName(vehOwnerName);
                    singleAuction.setVehicleName(vehicleName);
                    singleAuction.setHighestBidStr(HighestPrice);
                    singleAuction.setImageUrl(vehicleImage);
                    singleAuction.setContactNumber(championContactNum);
                    singleAuction.setVehicleUsage(SpinnerHelper.getValueByCodeFromMap(
                            SpinnerConstants.mapVehicleUsage,
                            CommonHelper.hasJSONKey(jsonObj, "VehicleUsage")));
                    if (!singleAuction.getHighestBidStr().isEmpty())
                        singleAuction.setHighestBid(Long.parseLong(singleAuction.getHighestBidStr()));

                    ArrayList<BrokerInAuction> brokerInAuctionsList = new ArrayList<>();
                    for (int d = 0; d < biddersListJson.length(); d++) {
                        BrokerInAuction brokerInAuction = new BrokerInAuction();
                        JSONObject jsonObject = (JSONObject) biddersListJson.get(d);
                        brokerInAuction.setMitraiD(jsonObject.getString("MITRAId"));
                        brokerInAuction.setMitraName(jsonObject.getString("MITRAName"));
                        brokerInAuction.setBidPrice(jsonObject.getString("bidPrice"));
                        brokerInAuction.setBiddingDateTime(jsonObject.getString("biddingDateTime"));
                        Date date1;
                        try {
                            date1 = format.parse(brokerInAuction.getBiddingDateTime());
                            brokerInAuction.setBiddingTime(date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        brokerInAuctionsList.add(brokerInAuction);
                    }
                    try {
                        Collections.sort(brokerInAuctionsList, new AuctionsSortComparator());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    singleAuction.setBrokersInAuctionList(brokerInAuctionsList);

                    for (int d = 0; d < singleAuction.getBrokersInAuctionList().size(); d++) {
                        BrokerInAuction brokerInAuction = singleAuction.getBrokersInAuctionList().get(d);
                        String mitraId = brokerInAuction.getMitraiD();
                        String bidPrice = brokerInAuction.getBidPrice();
                        if (mitraId.equalsIgnoreCase(SessionUserDetails.getInstance().getMitraCode())) {
                            singleAuction.setBidderPriceStr(bidPrice);
                        }
                    }
                    singleAuction.setHasWon(jsonObj.getString("HighestBidder").equals("Y"));
                    singleAuction.setIsHighestbidder(singleAuction.isHasWon() ? "Y" : "N");
                    liveAuctionList.add(singleAuction);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return liveAuctionList;
    }

    public static ArrayList<SingleAuction> parseAuctionsDataByType(JSONObject response, int type) {
        ArrayList<SingleAuction> liveAuctionList = new ArrayList<>();
        ArrayList<SingleAuction> list = new ArrayList<>();
        try {
            JSONArray jsonArray = response.optJSONArray(RestResponseKeys.LiveAuctionOutputKeys.AuctionDetails);
            if (null != jsonArray) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                    String auctionDate = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionDate);
                    String auctionId = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionID);
                    String auctionStatus = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionStatus);
                    String countofbidders = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.CountOfBidders);
                    String mitraBasePrice = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.MITRAprice_aseprice);
                    JSONArray biddersListJson = jsonObj.getJSONArray(RestResponseKeys.LiveAuctionOutputKeys.LIST);
                    String HighestPrice = jsonObj.getString("HighestPrice");
                    String Make = jsonObj.getString("Make");
                    String Model = jsonObj.getString("Model");
                    String vehicleImage = jsonObj.getString("vehiclePicURL");
                    String championContactNum = jsonObj.getString("ChampionContactnumber");

                    String startTimestamp = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.StartTimestamp);
                    String requestID = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.RequestID);

                    String remainingtimeinseconds = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.remainingTimeInSeconds);
                    String vehOwnerName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehOwnerName);
                    String vehicleName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehicleName);

                    SingleAuction singleAuction = new SingleAuction();
                    singleAuction.setDate(auctionDate);
                    singleAuction.setAuctionId(auctionId);
                    singleAuction.setAuctionStatus(auctionStatus);
                    singleAuction.setCountOfBidders(countofbidders);
                    singleAuction.setMitraBasePrice(mitraBasePrice);
                    singleAuction.setMake(Make);
                    singleAuction.setModel(Model);
                    singleAuction.setStartTimeStamp(startTimestamp);
                    singleAuction.setAuctionStatus(auctionStatus);
                    singleAuction.setEvaluationReportId(requestID);
                    singleAuction.setRequestId(requestID);
                    singleAuction.setRemainingTimeInSeconds(remainingtimeinseconds);
                    singleAuction.setVehicleOwnerName(vehOwnerName);
                    singleAuction.setVehicleName(vehicleName);
                    singleAuction.setHighestBidStr(HighestPrice);
                    singleAuction.setImageUrl(vehicleImage);
                    singleAuction.setContactNumber(championContactNum);
                    singleAuction.setVehicleUsage(SpinnerHelper.getValueByCodeFromMap(
                            SpinnerConstants.mapVehicleUsage,
                            CommonHelper.hasJSONKey(jsonObj, "VehicleUsage")));

                    ArrayList<BrokerInAuction> brokerInAuctionsList = new ArrayList<>();
                    for (int d = 0; d < biddersListJson.length(); d++) {
                        BrokerInAuction brokerInAuction = new BrokerInAuction();
                        JSONObject jsonObject = (JSONObject) biddersListJson.get(d);
                        brokerInAuction.setMitraiD(jsonObject.getString("MITRAId"));
                        brokerInAuction.setMitraName(jsonObject.getString("MITRAName"));
                        brokerInAuction.setBidPrice(jsonObject.getString("bidPrice"));
                        brokerInAuction.setBiddingDateTime(jsonObject.getString("biddingDateTime"));
                        Date date1;
                        try {
                            date1 = format.parse(brokerInAuction.getBiddingDateTime());
                            brokerInAuction.setBiddingTime(date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        brokerInAuctionsList.add(brokerInAuction);
                    }
                    try {
                        Collections.sort(brokerInAuctionsList, new AuctionsSortComparator());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    singleAuction.setBrokersInAuctionList(brokerInAuctionsList);
                    for (int d = 0; d < singleAuction.getBrokersInAuctionList().size(); d++) {
                        BrokerInAuction brokerInAuction = singleAuction.getBrokersInAuctionList().get(d);
                        String mitraId = brokerInAuction.getMitraiD();
                        String bidPrice = brokerInAuction.getBidPrice();
                        if (mitraId.equalsIgnoreCase(SessionUserDetails.getInstance().getMitraCode())) {
                            singleAuction.setBidderPriceStr(bidPrice);
                        }
                    }
                    singleAuction.setHasWon(jsonObj.optString("HighestBidder").equals("Y"));
                    singleAuction.setIsHighestbidder(singleAuction.isHasWon() ? "Y" : "N");
                    liveAuctionList.add(singleAuction);
                }
            }
            if (type == 0)
                list.addAll(AuctionHelper.filterListIfMitrahasBiddedForLive(liveAuctionList));
            else if (type == 1)
                list.addAll(AuctionHelper.filterListIfMitrahasBidded(liveAuctionList));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SingleAuction parseSingleAuction(JSONObject orgJsonObj) {
        String auctionDate;
        try {
            JSONArray jsonArray = orgJsonObj.getJSONArray(RestResponseKeys.LiveAuctionOutputKeys.AuctionDetails);
            JSONObject jsonObj = (JSONObject) jsonArray.get(0);
            String auctionId = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionID);
            String auctionStatus = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionStatus);
            String countofbidders = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.CountOfBidders);
            JSONArray biddersListJson = jsonObj.getJSONArray(RestResponseKeys.LiveAuctionOutputKeys.LIST);
            String startTimestamp = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.StartTimestamp);
            String requestID = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.RequestID);
            String remainingtimeinseconds = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.remainingTimeInSeconds);
            String vehOwnerName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehOwnerName);
            String vehicleName = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.vehicleName);
            String isHighestBidder = jsonObj.getString("HighestBidder");
            String basePrice = jsonObj.optString("MITRAPrice(base price)");
            String Make = jsonObj.getString("Make");
            String Model = jsonObj.getString("Model");
            String HighestPrice = jsonObj.getString("HighestPrice");
            SingleAuction singleAuction = new SingleAuction();
            singleAuction.setAuctionId(auctionId);
            singleAuction.setAuctionStatus(auctionStatus);
            singleAuction.setCountOfBidders(countofbidders);
            singleAuction.setMitraBasePrice(basePrice);
            singleAuction.setStartTimeStamp(startTimestamp);
            singleAuction.setAuctionStatus(auctionStatus);
            singleAuction.setRequestId(requestID);
            singleAuction.setHighestBidStr(HighestPrice);
            singleAuction.setEvaluationReportId(auctionId);
            singleAuction.setRemainingTimeInSeconds(remainingtimeinseconds);
            singleAuction.setVehicleOwnerName(vehOwnerName);
            singleAuction.setVehicleName(vehicleName);
            singleAuction.setMake(Make);
            singleAuction.setModel(Model);
            singleAuction.setImageUrl(jsonObj.optString("vehiclePicURL"));
            singleAuction.setContactNumber(jsonObj.optString("ChampionContactnumber"));
            singleAuction.setNoOfOwners(jsonObj.optString("NoOfOwners"));
            singleAuction.setOdometerReading(jsonObj.optString("OdometerReading"));
            singleAuction.setFeedbackRating(jsonObj.optString("OverallFeedbackRating"));
            singleAuction.setVehicleUsage(SpinnerHelper.getValueByCodeFromMap(
                    SpinnerConstants.mapVehicleUsage,
                    CommonHelper.hasJSONKey(jsonObj, "VehicleUsage")));

            singleAuction.setIsHighestbidder(isHighestBidder);
            auctionDate = jsonObj.getString(RestResponseKeys.LiveAuctionOutputKeys.AuctionDate);
            singleAuction.setDate(auctionDate);
            ArrayList<BrokerInAuction> brokerInAuctionsList = new ArrayList<>();

            for (int i = 0; i < biddersListJson.length(); i++) {
                BrokerInAuction brokerInAuction = new BrokerInAuction();
                JSONObject jsonObject = (JSONObject) biddersListJson.get(i);
                brokerInAuction.setMitraiD(jsonObject.getString("MITRAId"));
                brokerInAuction.setMitraName(jsonObject.getString("MITRAName"));
                brokerInAuction.setBidPrice(jsonObject.getString("bidPrice"));
                brokerInAuction.setBiddingDateTime(jsonObject.getString("biddingDateTime"));
                Date date1;
                try {
                    date1 = format.parse(brokerInAuction.getBiddingDateTime());
                    brokerInAuction.setBiddingTime(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                brokerInAuctionsList.add(brokerInAuction);
            }
            try {
                Collections.sort(brokerInAuctionsList, new AuctionsSortComparator());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            singleAuction.setBrokersInAuctionList(brokerInAuctionsList);
            ArrayList<BrokerInAuction> biddersList = singleAuction.getBrokersInAuctionList();

            for (int d = 0; d < biddersList.size(); d++) {
                BrokerInAuction brokerInAuction = biddersList.get(d);
                String mitraId = brokerInAuction.getMitraiD();
                String bidPrice = brokerInAuction.getBidPrice();
                if (mitraId.equalsIgnoreCase(SessionUserDetails.getInstance().getMitraCode())) {
                    singleAuction.setBidderPriceStr(bidPrice);
                }
            }

            singleAuction.setHasWon(jsonObj.getString("HighestBidder").equals("Y"));
            singleAuction.setIsHighestbidder(singleAuction.isHasWon() ? "Y" : "N");
            return singleAuction;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AllLeadsData parseLeadsData(MyLeadsResponse response) {
        AllLeadsData allLeadsData = new AllLeadsData();
        allLeadsData.setUnassignedCount(response.getUnassignedLeadsCount());
        allLeadsData.setUnassignedLeads(response.getUnassignedLeadDetails());
        try {
            for (int i = 0; i < response.getAssignedEnquiryDetails().size(); i++) {
                if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.HOT)) {
                    allLeadsData.getHotEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                } else if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.COLD)) {
                    allLeadsData.getColdEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                } else if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.WARM)) {
                    allLeadsData.getWarmEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                } else if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.ORDERED)) {
                    allLeadsData.getOrderedEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                } else if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.INVOICED)) {
                    allLeadsData.getInvoicedEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                } else if (response.getAssignedEnquiryDetails().get(i).getStatus()
                        .equalsIgnoreCase(LeadTypesIn.LOST)) {
                    allLeadsData.getLostEnquiries().add(response.getAssignedEnquiryDetails().get(i));
                }
            }
            allLeadsData.setHotCount(allLeadsData.getHotEnquiries().size());
            allLeadsData.setWarmCount(allLeadsData.getWarmEnquiries().size());
            allLeadsData.setColdCount(allLeadsData.getColdEnquiries().size());
            allLeadsData.setOrderedCount(allLeadsData.getOrderedEnquiries().size());
            allLeadsData.setInvoicedCount(allLeadsData.getInvoicedEnquiries().size());
            allLeadsData.setLostCount(allLeadsData.getLostEnquiries().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allLeadsData;
    }
}