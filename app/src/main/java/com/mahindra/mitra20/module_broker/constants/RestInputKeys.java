package com.mahindra.mitra20.module_broker.constants;

public class RestInputKeys {

    public static class LeadsInputKeys {
        public static final String MitraCode = "MitraCode";
        public static final String MitraMobile = "MitraMobile";
        public static final String RequestStatus = "RequestStatus";
    }

    public static class LiveAuctionInputKeys {
        public static final String FromDate = "FromDate";
        public static final String toDate = "toDate";
        public static final String AuctionStatus = "AuctionStatus";
        public static final String Make = "Make";
        public static final String Model = "Model";
        public static final String countToDisplay = "countToDisplay";
        public static final String Sorting = "Sorting";
        public static final String UserID = "UserID";
        public static final String AuctionID = "AuctionID";

    }

    public static class PlaceBidInputKeys {
        public static final String UserID = "UserID";
        public static final String AuctionID = "AuctionID";
        public static final String BIDAmount = "BIDAmount";

    }

    public static class EvalChampLoginInputKeys {
        public static final String UserID = "userid";
        public static final String password = "password";
        public static final String authCode = "authCode";
        public static final String FCMTokenId = "FCMTokenId";
    }

    public static class BrokerLoginInputKeys {
        public static final String MitraCode = "MitraCode";
        public static final String MitraMobile = "MitraMobile";
        public static final String FCMTokenId = "FCMTokenId";
    }

    public static class QuickLeadInputKeys {
        public static final String Parentcode = "Parentcode";
        public static final String Locationcode = "Locationcode";
        public static final String enquiryType = "enquiryType";
        public static final String enquirySource = "enquirySource";
        public static final String customerName = "customerName";
        public static final String mobileNumber = "mobileNumber";
        public static final String email = "email";
        public static final String pinCode = "pinCode";
        public static final String modelgroup = "modelgroup";
        public static final String likelyPurchase = "likelyPurchase";
        public static final String uploadDate = "uploadDate";
        public static final String EnquiryVerificationType = "EnquiryVerificationType";
        public static final String Eventcode = "Eventcode";
        public static final String followupDateTime = "followupDateTime";
        public static final String remarks = "remarks";
        public static final String interstedInExchange = "interstedInExchange";
        public static final String exchangeMake = "exchangeMake";
        public static final String exchangeModel = "exchangeModel";
        public static final String mitraId = "mitraId";

    }


}
