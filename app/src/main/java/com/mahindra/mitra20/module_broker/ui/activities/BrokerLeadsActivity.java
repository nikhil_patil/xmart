package com.mahindra.mitra20.module_broker.ui.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.presenters.BrokerLeadsActivityPresenter;
import com.mahindra.mitra20.module_broker.presenters.BrokerLeadsViewAllIn;
import com.mahindra.mitra20.module_broker.ui.adapters.ViewPager.BrokerLeadsViewPagerAdapter;

import org.json.JSONObject;

public class BrokerLeadsActivity extends AppCompatActivity implements BrokerLeadsViewAllIn {
    private int defaultPage = 0;
    private BrokerLeadsActivityPresenter brokerLeadsActivityPresenter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BrokerLeadsViewPagerAdapter brokerLeadsViewPagerAdapter;
    private AllLeadsData allLeadsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broker_leads);
        try {
            defaultPage = getIntent().getIntExtra(
                    ExtrasKeys.LeadsViewAll.DEFAULT_PAGE, 0);
            allLeadsData = getIntent().getParcelableExtra(ExtrasKeys.LeadsViewAll.LEADS_DATA);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        init();
    }

    private void init() {
        brokerLeadsActivityPresenter = new BrokerLeadsActivityPresenter(this, this);
        setCustomToolBar();
        setUpSwipeRefreshContainer();
        tabLayout = findViewById(R.id.auctions_view_all_tabs);
        viewPager = findViewById(R.id.auctions_view_all_view_pager);
        if (null != allLeadsData) {
            populateViewPager();
        } else {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        setListeners();
    }

    void populateViewPager() {
        brokerLeadsViewPagerAdapter = new BrokerLeadsViewPagerAdapter(
                getSupportFragmentManager(), allLeadsData);
        viewPager.setAdapter(brokerLeadsViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(7);
        viewPager.setCurrentItem(defaultPage);
    }

    public void setCustomToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        View customView = getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);

        TextView headingTv = customView.findViewById(R.id.heading_tv);
        headingTv.setText(getString(R.string.my_leads));

        ImageView backButton = customView.findViewById(R.id.back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void gotLeadsData(AllLeadsData leadsData) {
        swipeRefreshLayout.setRefreshing(false);
        if (null != leadsData) {
            allLeadsData = leadsData;
            populateViewPager();
        } else {
            CommonHelper.toast("Leads data not found", this);
        }
    }

    @Override
    public void gotError(JSONObject response) {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void setListeners() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e("TabReselected", "tab position is " + tab.getPosition());
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("TabReselected", "tab position is " + position);
            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = tabLayout.getTabAt(position);
                if (null != tab)
                    tab.select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e("TabReselected", "tab position is ");
            }
        });
    }

    private void setUpSwipeRefreshContainer() {
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                brokerLeadsActivityPresenter.getLeadsData();
            }
        });
    }
}