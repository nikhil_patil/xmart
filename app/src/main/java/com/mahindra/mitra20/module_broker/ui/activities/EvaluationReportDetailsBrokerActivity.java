package com.mahindra.mitra20.module_broker.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.models.NewEvaluation;
import com.mahindra.mitra20.evaluator.views.fragment.ViewEvaluationFragment;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.sqlite.EvaluatorHelper;

public class EvaluationReportDetailsBrokerActivity extends AppCompatActivity {

    CommonHelper commonHelper;
    NewEvaluation newEvaluation;
    private EvaluatorHelper evaluatorHelper;
    private String draftEvaluationId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_details_data);
        init();
    }

    private void init() {
        CommonHelper.settingCustomToolBar(this, "Evaluation Report Details");
        try {
            commonHelper = new CommonHelper();
            evaluatorHelper = new EvaluatorHelper(this);

            Intent intent = getIntent();
            draftEvaluationId = intent.getStringExtra("NewEvaluationId");

            evaluatorHelper = new EvaluatorHelper(EvaluationReportDetailsBrokerActivity.this);

            getFromDraft();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getFromDraft() {
        newEvaluation = evaluatorHelper.getEvaluationDetailsById("", draftEvaluationId);
        if (newEvaluation.getRequestId() != null || newEvaluation.getId() != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(CommonHelper.EXTRA_EVALUATION, newEvaluation);
            bundle.putBoolean(CommonHelper.EXTRA_ARE_IMAGES_ONLINE, true);
            bundle.putBoolean(CommonHelper.EXTRA_IS_BROKER, true);
            ViewEvaluationFragment viewEvaluationFragment =
                    new ViewEvaluationFragment();
            viewEvaluationFragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.main_container, viewEvaluationFragment).commit();
        }
    }
}