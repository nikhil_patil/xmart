package com.mahindra.mitra20.module_broker.helper;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FIlterAuctionssList {

    public static ArrayList<SingleAuction> filterAuctionByStatus(ArrayList<SingleAuction> originalLeadDetailList, String statusToFilter) {

        ArrayList<SingleAuction> filteredList = new ArrayList<>();
        for (int i = 0; i < originalLeadDetailList.size(); i++) {
            SingleAuction leadsDetail = originalLeadDetailList.get(i);
            String auctionStatus = leadsDetail.getAuctionStatus();
            if (auctionStatus.equalsIgnoreCase(statusToFilter)) {
                filteredList.add(leadsDetail);
            }
        }

        return filteredList;
    }

    public interface Predicate<T> {
        boolean apply(T type);
    }


    public static <T> Collection<T> filter(Collection<T> col, Predicate<T> predicate) {
        Collection<T> result = new ArrayList<T>();
        for (T element : col) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }
}
