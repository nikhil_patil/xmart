package com.mahindra.mitra20.module_broker.helper;

import android.graphics.Color;

import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.BrokerInAuction;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;

import java.util.ArrayList;

public class AuctionHelper {

    public static ArrayList<SingleAuction> filterListIfMitrahasBiddedForLive(ArrayList<SingleAuction> originalAuctionList) {
        ArrayList<SingleAuction> filteredAuctionList = new ArrayList<>();
        if (originalAuctionList != null) {
            for (int i = 0; i < originalAuctionList.size(); i++) {
                SingleAuction singleAuction = originalAuctionList.get(i);
                ArrayList<BrokerInAuction> biddersList = singleAuction.getBrokersInAuctionList();
                boolean mitraHasBidded = false;

                for (int d = 0; d < biddersList.size(); d++) {
                    BrokerInAuction brokerInAuction = biddersList.get(d);
                    String mitraId = brokerInAuction.getMitraiD();
                    String bidPrice = brokerInAuction.getBidPrice();
                    if (mitraId.equalsIgnoreCase(SessionUserDetails.getInstance().getMitraCode())) {
                        singleAuction.setBidderPriceStr(bidPrice);
                        mitraHasBidded = true;
                    }
                }

                if (!mitraHasBidded) {
                    singleAuction.setHasMitraBidded(false);
                    filteredAuctionList.add(singleAuction);
                }
            }
        }
        return filteredAuctionList;
    }

    public static ArrayList<SingleAuction> filterListIfMitrahasBidded(ArrayList<SingleAuction> originalAuctionList) {
        ArrayList<SingleAuction> filteredAuctionList = new ArrayList<>();
        if (originalAuctionList != null) {
            for (int i = 0; i < originalAuctionList.size(); i++) {
                SingleAuction singleAuction = originalAuctionList.get(i);
                ArrayList<BrokerInAuction> biddersList = singleAuction.getBrokersInAuctionList();
                boolean mitraHasBidded = false;
                for (int d = 0; d < biddersList.size(); d++) {
                    BrokerInAuction brokerInAuction = biddersList.get(d);
                    String mitraId = brokerInAuction.getMitraiD();
                    String bidPrice = brokerInAuction.getBidPrice();

                    if (mitraId.equalsIgnoreCase(SessionUserDetails.getInstance().getMitraCode())) {
                        singleAuction.setBidderPriceStr(bidPrice);
                        mitraHasBidded = true;
                    }
                }

                if (mitraHasBidded) {
                    singleAuction.setHasMitraBidded(true);
                    filteredAuctionList.add(singleAuction);
                }
            }
        }
        return filteredAuctionList;
    }

    public static int getColorForTimer(long millisUntilFinished) {
        int colorInt = 0;

        long stage1 = 12000000; //2 hrs
        long stage2 = 6000000;  //1 hr
        long stage3 = 3000000;  //30 minutes

        int colorBlue = Color.parseColor("#0084ff");
        int colorRed = Color.parseColor("#ff2154");
        int colorOrange = Color.parseColor("#ff7c00");

        if (millisUntilFinished > stage2) {
            //green color
            colorInt = colorBlue;
        } else if (millisUntilFinished <= stage2 && millisUntilFinished > stage3) {
            //orange color
            colorInt = colorOrange;
        } else if (millisUntilFinished <= stage3) {
            //red color
            colorInt = colorRed;
        }

        return colorInt;
    }
}
