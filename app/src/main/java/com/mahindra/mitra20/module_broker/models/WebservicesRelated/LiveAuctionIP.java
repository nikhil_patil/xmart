package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

public class LiveAuctionIP {
    private String fromDate;
    private String toDate;
    private String auctionStatus;
    private String make;
    private String model;
    private String countToDisplay;
    private String sorting;
    private String userId;


    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getAuctionStatus() {
        return auctionStatus;
    }

    public void setAuctionStatus(String auctionStatus) {
        this.auctionStatus = auctionStatus;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCountToDisplay() {
        return countToDisplay;
    }

    public void setCountToDisplay(String countToDisplay) {
        this.countToDisplay = countToDisplay;
    }

    public String getSorting() {
        return sorting;
    }

    public void setSorting(String sorting) {
        this.sorting = sorting;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
