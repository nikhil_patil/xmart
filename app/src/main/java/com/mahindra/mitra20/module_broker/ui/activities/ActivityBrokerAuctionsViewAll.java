package com.mahindra.mitra20.module_broker.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.constants.AuctionType;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.interfaces.AuctionActivityJobs;
import com.mahindra.mitra20.module_broker.interfaces.AuctionsTabInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BrokerAuctionViewAllCallback;
import com.mahindra.mitra20.module_broker.models.Parsers;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.LiveAuctionIP;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;
import com.mahindra.mitra20.module_broker.presenters.BrokerAuctionsViewAllIn;
import com.mahindra.mitra20.module_broker.presenters.BrokerAuctionsViewAllPresenter;
import com.mahindra.mitra20.module_broker.ui.adapters.ViewPager.BrokerAuctionsViewAllViewPagerAdapter;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllPageFragment2;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllPageFragment3;
import com.mahindra.mitra20.module_broker.ui.fragments.BrokerAuctionViewAllSinglePageFragment;
import com.mahindra.mitra20.utils.AuctionIntervalUtils;
import com.mahindra.mitra20.utils.LogUtil;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActivityBrokerAuctionsViewAll extends AppCompatActivity implements
        BrokerAuctionsViewAllIn, BidNowInteractor, AuctionsTabInteractor, AuctionActivityJobs, BrokerAuctionViewAllCallback {
    private BrokerAuctionsViewAllPresenter showAuctionDetailsPresenter;
    private ArrayList<SingleAuction> auctionListLive, auctionListBidded, auctionListClosed;
    public SwipeRefreshLayout swipeRefreshLayout;
    private AuctionIntervalUtils intervalUtils = new AuctionIntervalUtils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitra_auction_view_all);
        init();
        setUpSwipeRefreshContainer();
    }

    private void callBidNowActivity(SingleAuction singleAuction) {
        try {
            Intent intent = new Intent(this, ActivityBrokerBidNow.class);
            intent.putExtra(ExtrasKeys.General.AUCTION_SINGLE_DATA, singleAuction);
            startActivity(intent);

            resetAuctionIntervalValues();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        showAuctionDetailsPresenter = new BrokerAuctionsViewAllPresenter(this, this);
        showAuctionDetailsPresenter.setViews();
        showAuctionDetailsPresenter.setCustomToolbar();

        TabLayout viewAllTabLayout = findViewById(R.id.auction_view_all_tabs);
        ViewPager viewAllViewPager = findViewById(R.id.auction_view_all_view_pager);
        BrokerAuctionsViewAllViewPagerAdapter brokerAuctionsViewAllViewPagerAdapter =
                new BrokerAuctionsViewAllViewPagerAdapter(getSupportFragmentManager());
        viewAllViewPager.setOffscreenPageLimit(3);
        viewAllViewPager.setAdapter(brokerAuctionsViewAllViewPagerAdapter);
        viewAllTabLayout.setupWithViewPager(viewAllViewPager);
    }

    public void setUpSwipeRefreshContainer() {
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchAuctionsData("LIV");
                fetchAuctionsData("PEN");
                fetchAuctionsData("COM");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void VisualElementsReady() {
        LogUtil.getInstance().logE("1minInterval-Broker", "VisualElementsReady()");
        fetchLiveAuctionsData("LIV");
        fetchLiveAuctionsData("COM");
        fetchLiveAuctionsData("PEN");
    }

    public void fetchLiveAuctionsData(String type) {

        int currentPage = 0;

        String mitraUserId = SessionUserDetails.getInstance().getMitraCode();
        LiveAuctionIP liveAuctionIP = new LiveAuctionIP();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String date = sdf.format(new Date());

        liveAuctionIP.setFromDate(date);
        liveAuctionIP.setToDate(date);
        liveAuctionIP.setCountToDisplay("999");


        switch (type) {
            case "LIV":
                if (!intervalUtils.isFirstAPIOrValidInterval(AuctionType.LIVE)) {
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
                currentPage = 0;
                break;
            case "PEN":
                if (!intervalUtils.isFirstAPIOrValidInterval(AuctionType.PENDING)) {
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
                currentPage = 1;
                type = "LIV";
                break;
            case "COM":
                currentPage = 2;
                if (intervalUtils.isFirstAPIOrValidInterval(AuctionType.COMPLETED)) {
                    Calendar calNow = Calendar.getInstance();
                    calNow.add(Calendar.MONTH, -1);
                    Date dateBeforeAMonth = calNow.getTime();
                    liveAuctionIP.setFromDate(sdf.format(dateBeforeAMonth));
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
                break;
        }


        liveAuctionIP.setAuctionStatus(type);
        liveAuctionIP.setMake("");
        liveAuctionIP.setModel("");
        liveAuctionIP.setSorting("DATE");
        liveAuctionIP.setUserId(mitraUserId);
        showAuctionDetailsPresenter.getAuctionsData(liveAuctionIP, currentPage);
    }

    @Override
    public void settingCustomToolBar() {
        try {
            getSupportActionBar().setCustomView(R.layout.temp_tool_bar_mitra);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                    | ActionBar.DISPLAY_SHOW_HOME);
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            View customView = getSupportActionBar().getCustomView();
            Toolbar parent = (Toolbar) customView.getParent();
            parent.setContentInsetsAbsolute(0, 0);

            TextView headingTv = customView.findViewById(R.id.heading_tv);
            headingTv.setText("My Auction Details");

            ImageView backbtn = customView.findViewById(R.id.back_btn);
            backbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotAuctionsData(int auctionPage, JSONObject response) {
        try {
            swipeRefreshLayout.setRefreshing(false);
            if (auctionPage == 0) {
                auctionListLive = Parsers.parseAuctionsDataByType(response, 0);
                BrokerAuctionViewAllSinglePageFragment.getInstance(this).update(auctionListLive);
            } else if (auctionPage == 1) {
                auctionListBidded = Parsers.parseAuctionsDataByType(response, 1);
                BrokerAuctionViewAllPageFragment2.getInstance(this).update(auctionListBidded);
            } else if (auctionPage == 2) {
                auctionListClosed = Parsers.parseAuctionsData(response);
                BrokerAuctionViewAllPageFragment3.getInstance(this).update(auctionListClosed);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotAuctionAPIError(String message) {
        LogUtil.getInstance().logE("Err", "" + message);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void clickedOnBidNow(SingleAuction pos) {
        callBidNowActivity(pos);
    }

    @Override
    public void fetchAuctionsData(String type) {
        fetchLiveAuctionsData(type);
    }

    @Override
    public ArrayList<SingleAuction> getAuctionList(int auctionPage) {
        if (auctionPage == 0) {
            return auctionListLive;
        } else if (auctionPage == 1) {
            return auctionListBidded;
        } else if (auctionPage == 2) {
            return auctionListClosed;
        }
        return auctionListLive;
    }

    @Override
    public void hideProgressBar(ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTabs(int page, int auctionsCount) {

    }

    @Override
    public ArrayList<SingleAuction> auctionList(int pageNum) {
        return null;
    }

    @Override
    public int getAuctionsActiveItemIndex() {
        return 0;
    }

    @Override
    public int getAuctionsActivePageNum() {
        return 0;
    }

    @Override
    public void resetAuctionIntervalValues() {
        //RESET AUCTION INTERVAL VALUES
        intervalUtils.resetValues();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VisualElementsReady();
    }
}