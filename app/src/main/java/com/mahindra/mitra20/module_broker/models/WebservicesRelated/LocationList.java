package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

public class LocationList {
    private String locationCode;
    private String locationName;

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
