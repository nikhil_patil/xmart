package com.mahindra.mitra20.module_broker.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.evaluator.helper.ScreenDensityHelper;
import com.mahindra.mitra20.module_broker.constants.ExtrasKeys;
import com.mahindra.mitra20.module_broker.helper.MyAuctionsItemDecoration;
import com.mahindra.mitra20.module_broker.interfaces.AuctionActivityJobs;
import com.mahindra.mitra20.module_broker.interfaces.AuctionsTabInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BrokerAuctionViewAllCallback;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;
import com.mahindra.mitra20.module_broker.ui.activities.ActivityBrokerBidNow;
import com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView.AuctionRecyclerViewViewAllAdapter;

import java.util.ArrayList;

public class BrokerAuctionViewAllSinglePageFragment extends Fragment implements BidNowInteractor {
    public static BrokerAuctionViewAllSinglePageFragment instance;
    private View fragmentView;
    private RecyclerView auctionsRv;
    private final int AUCTION_TYPE = 0;
    private AuctionsTabInteractor auctionsTabInteractor;
    private AuctionRecyclerViewViewAllAdapter auctionRecyclerViewViewAllAdapter;
    private ViewGroup noVehicleVg;
    private AuctionActivityJobs auctionActivityJobsInteractor;
    private ProgressBar progressbar;
    private static BrokerAuctionViewAllCallback callback;

    public static BrokerAuctionViewAllSinglePageFragment getInstance(BrokerAuctionViewAllCallback callback) {
        BrokerAuctionViewAllSinglePageFragment.callback = callback;
        if (instance == null) {
            instance = new BrokerAuctionViewAllSinglePageFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_broker_auctions_view_all_single_page, container, false);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        auctionsTabInteractor = (AuctionsTabInteractor) getActivity();
        auctionActivityJobsInteractor = (AuctionActivityJobs) getActivity();
        init();
    }

    private void init() {
        setAuctionsRecyclerView();
        progressbar = fragmentView.findViewById(R.id.progressBar);
        auctionActivityJobsInteractor.showProgressBar(progressbar);
    }

    private void setAuctionsRecyclerView() {
        noVehicleVg = fragmentView.findViewById(R.id.noVehicleVg);
        auctionsRv = fragmentView.findViewById(R.id.auctionsRv);
        auctionRecyclerViewViewAllAdapter = new AuctionRecyclerViewViewAllAdapter(getActivity(),
                this, auctionsTabInteractor.auctionList(auctionsTabInteractor.getAuctionsActivePageNum()));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        auctionsRv.setLayoutManager(mLayoutManager);
        auctionsRv.addItemDecoration(new MyAuctionsItemDecoration(ScreenDensityHelper.dpToPx(getActivity(), 18),
                ScreenDensityHelper.dpToPx(getActivity(), 15), ScreenDensityHelper.dpToPx(getActivity(), 15)));
        auctionsRv.setAdapter(auctionRecyclerViewViewAllAdapter);
    }

    @Override
    public void clickedOnBidNow(SingleAuction singleAuction) {
        if (callback != null)
            callback.resetAuctionIntervalValues();
        Intent intent = new Intent(getActivity(), ActivityBrokerBidNow.class);
//        SingleAuction singleAuction = auctionRecyclerViewViewAllAdapter.auctionArrayList.get(pos);
        intent.putExtra(ExtrasKeys.General.AUCTION_DEFAULT_PAGE, 0);
        intent.putExtra(ExtrasKeys.General.AUCTION_SINGLE_DATA, singleAuction);
        startActivity(intent);
    }

    public void update(ArrayList<SingleAuction> aucList) {
        auctionActivityJobsInteractor = (AuctionActivityJobs) getActivity();
        auctionRecyclerViewViewAllAdapter.auctionArrayList = aucList;
        auctionRecyclerViewViewAllAdapter.AUCTION_TYPE = AUCTION_TYPE;
        auctionRecyclerViewViewAllAdapter.notifyDataSetChanged();

        if (null != aucList) {
            if (aucList.size() > 0) {
                auctionsRv.setVisibility(View.VISIBLE);
                noVehicleVg.setVisibility(View.GONE);
            } else {
                auctionsRv.setVisibility(View.GONE);
                noVehicleVg.setVisibility(View.VISIBLE);
            }
        } else {
            auctionsRv.setVisibility(View.GONE);
            noVehicleVg.setVisibility(View.VISIBLE);
        }
        auctionActivityJobsInteractor.hideProgressBar(progressbar);
    }
}