package com.mahindra.mitra20.module_broker.models.WebservicesRelated;

import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;

import java.util.ArrayList;

public class LeadsMasterListOP {
    private ArrayList<LeadDetail> leadDetailsList;

    public ArrayList<LeadDetail> getLeadDetailsList() {
        return leadDetailsList;
    }

    public void setLeadDetailsList(ArrayList<LeadDetail> leadDetailsList) {
        this.leadDetailsList = leadDetailsList;
    }
}
