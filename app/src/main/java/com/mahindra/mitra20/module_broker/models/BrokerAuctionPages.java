package com.mahindra.mitra20.module_broker.models;

public class BrokerAuctionPages {
    private String pageTitle;
    private int pageNum;

    public BrokerAuctionPages(String pageTitle, int pageNum){
        this.pageTitle = pageTitle;
        this.pageNum = pageNum;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
}
