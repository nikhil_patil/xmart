package com.mahindra.mitra20.module_broker.presenters;

import com.mahindra.mitra20.module_broker.models.AllLeadsData;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.Serializable.LeadDetail;

import org.json.JSONObject;

import java.util.List;

public interface BrokerLeadsViewAllIn {
    void gotLeadsData(AllLeadsData allLeadsData);
    void gotError(JSONObject response);
}