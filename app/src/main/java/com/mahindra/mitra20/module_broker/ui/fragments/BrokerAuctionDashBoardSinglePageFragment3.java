package com.mahindra.mitra20.module_broker.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mahindra.mitra20.R;
import com.mahindra.mitra20.module_broker.interfaces.AuctionActivityJobs;
import com.mahindra.mitra20.module_broker.interfaces.AuctionsTabInteractor;
import com.mahindra.mitra20.module_broker.interfaces.BidNowInteractor;
import com.mahindra.mitra20.module_broker.models.WebservicesRelated.SingleAuction;
import com.mahindra.mitra20.module_broker.ui.adapters.RecyclerView.AuctionRecyclerViewDashBoardAdapter;

import java.util.ArrayList;

public class BrokerAuctionDashBoardSinglePageFragment3 extends Fragment implements BidNowInteractor {
    public static BrokerAuctionDashBoardSinglePageFragment3 instance;
    private View fragmentView;
    private RecyclerView auctionsRv;
    private static BidNowInteractor interactor;
    private final int AUCTION_TYPE = 2;
    private AuctionsTabInteractor auctionsTabInteractor;
    private AuctionRecyclerViewDashBoardAdapter auctionRecyclerViewDashBoardAdapter;
    private ViewGroup noVehicleVg;
    private AuctionActivityJobs auctionActivityJobsInteractor;
    private ProgressBar progressbar;

    public static BrokerAuctionDashBoardSinglePageFragment3 getInstance(BidNowInteractor bidNowInteractor) {
        if (instance == null) {
            instance = new BrokerAuctionDashBoardSinglePageFragment3();
            interactor = bidNowInteractor;
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_broker_auctions_dashboard_single_page,
                container, false);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        auctionsTabInteractor = (AuctionsTabInteractor) getActivity();
        auctionActivityJobsInteractor = (AuctionActivityJobs) getActivity();
        init();
    }

    private void init() {
        noVehicleVg = fragmentView.findViewById(R.id.noVehicleVg);
        progressbar = fragmentView.findViewById(R.id.progressBar);
        auctionActivityJobsInteractor.showProgressBar(progressbar);
        noVehicleVg.setVisibility(View.VISIBLE);
        setAuctionsRecyclerView();
        update(auctionActivityJobsInteractor.getAuctionList(AUCTION_TYPE));
    }

    private void setAuctionsRecyclerView() {
        auctionsRv = fragmentView.findViewById(R.id.auctionsRv);
        auctionRecyclerViewDashBoardAdapter = new AuctionRecyclerViewDashBoardAdapter(this,
                auctionsTabInteractor.auctionList(auctionsTabInteractor.getAuctionsActivePageNum()));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        auctionsRv.setLayoutManager(mLayoutManager);
        auctionsRv.setAdapter(auctionRecyclerViewDashBoardAdapter);
    }

    public void update(ArrayList<SingleAuction> aucList) {
        auctionActivityJobsInteractor = (AuctionActivityJobs) getActivity();
        if (aucList != null) {
            auctionRecyclerViewDashBoardAdapter.auctionArrayList = aucList;
            auctionRecyclerViewDashBoardAdapter.AUCTION_TYPE = AUCTION_TYPE;
            auctionRecyclerViewDashBoardAdapter.notifyDataSetChanged();
            if (null != auctionRecyclerViewDashBoardAdapter.auctionArrayList) {
                auctionActivityJobsInteractor.setTabs(2,
                        auctionRecyclerViewDashBoardAdapter.auctionArrayList.size());
                if (auctionRecyclerViewDashBoardAdapter.auctionArrayList.size() > 0) {
                    noVehicleVg.setVisibility(View.GONE);
                    auctionsRv.setVisibility(View.VISIBLE);
                } else {
                    noVehicleVg.setVisibility(View.VISIBLE);
                    auctionsRv.setVisibility(View.GONE);
                }
            } else {
                noVehicleVg.setVisibility(View.VISIBLE);
                auctionsRv.setVisibility(View.GONE);
            }
        }
        auctionActivityJobsInteractor.hideProgressBar(progressbar);
    }

    public static BrokerAuctionDashBoardSinglePageFragment3 getInstance() {
        if (instance == null) {
            instance = new BrokerAuctionDashBoardSinglePageFragment3();
            // getInstance().update();
        }
        return instance;
    }

    @Override
    public void clickedOnBidNow(SingleAuction pos) {
        if (null != interactor)
            interactor.clickedOnBidNow(pos);
    }
}