package com.mahindra.mitra20.module_broker.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PATINIK-CONT on 16-Oct-19.
 */
public class AllLeadsData implements Parcelable {
    private int unassignedCount;
    private int hotCount;
    private int coldCount;
    private int warmCount;
    private int orderedCount;
    private int invoicedCount;
    private int lostCount;

    private ArrayList<UnassignedLeadDetails> unassignedLeads = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> hotEnquiries = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> coldEnquiries = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> warmEnquiries = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> orderedEnquiries = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> invoicedEnquiries = new ArrayList<>();
    private ArrayList<AssignedEnquiryDetails> lostEnquiries = new ArrayList<>();

    public AllLeadsData() {

    }

    protected AllLeadsData(Parcel in) {
        unassignedCount = in.readInt();
        hotCount = in.readInt();
        coldCount = in.readInt();
        warmCount = in.readInt();
        orderedCount = in.readInt();
        invoicedCount = in.readInt();
        lostCount = in.readInt();
        unassignedLeads = in.createTypedArrayList(UnassignedLeadDetails.CREATOR);
        hotEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
        coldEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
        warmEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
        orderedEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
        invoicedEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
        lostEnquiries = in.createTypedArrayList(AssignedEnquiryDetails.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(unassignedCount);
        dest.writeInt(hotCount);
        dest.writeInt(coldCount);
        dest.writeInt(warmCount);
        dest.writeInt(orderedCount);
        dest.writeInt(invoicedCount);
        dest.writeInt(lostCount);
        dest.writeTypedList(unassignedLeads);
        dest.writeTypedList(hotEnquiries);
        dest.writeTypedList(coldEnquiries);
        dest.writeTypedList(warmEnquiries);
        dest.writeTypedList(orderedEnquiries);
        dest.writeTypedList(invoicedEnquiries);
        dest.writeTypedList(lostEnquiries);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AllLeadsData> CREATOR = new Creator<AllLeadsData>() {
        @Override
        public AllLeadsData createFromParcel(Parcel in) {
            return new AllLeadsData(in);
        }

        @Override
        public AllLeadsData[] newArray(int size) {
            return new AllLeadsData[size];
        }
    };

    public int getUnassignedCount() {
        return unassignedCount;
    }

    public void setUnassignedCount(int unassignedCount) {
        this.unassignedCount = unassignedCount;
    }

    public int getHotCount() {
        return hotCount;
    }

    public void setHotCount(int hotCount) {
        this.hotCount = hotCount;
    }

    public int getColdCount() {
        return coldCount;
    }

    public void setColdCount(int coldCount) {
        this.coldCount = coldCount;
    }

    public int getWarmCount() {
        return warmCount;
    }

    public void setWarmCount(int warmCount) {
        this.warmCount = warmCount;
    }

    public int getOrderedCount() {
        return orderedCount;
    }

    public void setOrderedCount(int orderedCount) {
        this.orderedCount = orderedCount;
    }

    public int getInvoicedCount() {
        return invoicedCount;
    }

    public void setInvoicedCount(int invoicedCount) {
        this.invoicedCount = invoicedCount;
    }

    public int getLostCount() {
        return lostCount;
    }

    public void setLostCount(int lostCount) {
        this.lostCount = lostCount;
    }

    public ArrayList<UnassignedLeadDetails> getUnassignedLeads() {
        return unassignedLeads;
    }

    public void setUnassignedLeads(ArrayList<UnassignedLeadDetails> unassignedLeads) {
        this.unassignedLeads = unassignedLeads;
    }

    public ArrayList<AssignedEnquiryDetails> getHotEnquiries() {
        return hotEnquiries;
    }

    public void setHotEnquiries(ArrayList<AssignedEnquiryDetails> hotEnquiries) {
        this.hotEnquiries = hotEnquiries;
    }

    public ArrayList<AssignedEnquiryDetails> getColdEnquiries() {
        return coldEnquiries;
    }

    public void setColdEnquiries(ArrayList<AssignedEnquiryDetails> coldEnquiries) {
        this.coldEnquiries = coldEnquiries;
    }

    public ArrayList<AssignedEnquiryDetails> getWarmEnquiries() {
        return warmEnquiries;
    }

    public void setWarmEnquiries(ArrayList<AssignedEnquiryDetails> warmEnquiries) {
        this.warmEnquiries = warmEnquiries;
    }

    public ArrayList<AssignedEnquiryDetails> getOrderedEnquiries() {
        return orderedEnquiries;
    }

    public void setOrderedEnquiries(ArrayList<AssignedEnquiryDetails> orderedEnquiries) {
        this.orderedEnquiries = orderedEnquiries;
    }

    public ArrayList<AssignedEnquiryDetails> getInvoicedEnquiries() {
        return invoicedEnquiries;
    }

    public void setInvoicedEnquiries(ArrayList<AssignedEnquiryDetails> invoicedEnquiries) {
        this.invoicedEnquiries = invoicedEnquiries;
    }

    public ArrayList<AssignedEnquiryDetails> getLostEnquiries() {
        return lostEnquiries;
    }

    public void setLostEnquiries(ArrayList<AssignedEnquiryDetails> lostEnquiries) {
        this.lostEnquiries = lostEnquiries;
    }

    public static Creator<AllLeadsData> getCREATOR() {
        return CREATOR;
    }
}