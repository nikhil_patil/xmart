package com.mahindra.mitra20;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mahindra.mitra20.champion.views.activity.ChampionDashboardActivity;
import com.mahindra.mitra20.evaluator.constants.WebConstants;
import com.mahindra.mitra20.evaluator.evaluatorVolley.VolleyMultipartRequest;
import com.mahindra.mitra20.evaluator.helper.BitmapHelper;
import com.mahindra.mitra20.evaluator.views.activity.EvaluatorDashboardActivity;
import com.mahindra.mitra20.helper.CommonHelper;
import com.mahindra.mitra20.models.SessionUserDetails;
import com.mahindra.mitra20.module_broker.ui.activities.BrokerDashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Activity_Profile_Pic extends AppCompatActivity implements View.OnClickListener {

    ImageView imageViewClose, imageViewProfilePic, imageViewCamera, imageViewGallery, imageViewUpload;
    private final int CAMERA_REQUEST_CODE = 100;
    private final int RESULT_LOAD_IMAGE = 101;
    ProgressBar progressProfileImage;
    String userId = "", userType = "";
    int glideFlag = 0;
    String mobNo = "";
    TextView textViewPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile_pic);

        init();
    }

    public void init() {
        progressProfileImage = findViewById(R.id.progressProfileImage);
        imageViewCamera = findViewById(R.id.imageViewCamera);
        imageViewProfilePic = findViewById(R.id.imageViewProfilePic);
        imageViewGallery = findViewById(R.id.imageViewGallery);
        imageViewUpload = findViewById(R.id.imageViewUpload);
        imageViewClose = findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(this);
        imageViewCamera.setOnClickListener(this);
        imageViewGallery.setOnClickListener(this);
        imageViewUpload.setOnClickListener(this);

        textViewPath = findViewById(R.id.textViewPath);


        if (SessionUserDetails.getInstance().getUserType() == 0)//champ
        {
            userId = SessionUserDetails.getInstance().getActualUserID().toLowerCase();
            userType = "CHAMP";
        } else if (SessionUserDetails.getInstance().getUserType() == 1) {//Evaluator
            userId = SessionUserDetails.getInstance().getUserID().toLowerCase();
            userType = "EVAL";
        } else {
            userId = SessionUserDetails.getInstance().getMitraCode();
            mobNo = SessionUserDetails.getInstance().getMitraMobileNum();
            userType = "MITRA";
        }

        String strUserProfilePic = SessionUserDetails.getInstance().getUserProfilePic();
        if(glideFlag == 0) {
            if(strUserProfilePic.length() > 0) {
                //if(filePath != null && !filePath.equals("") ) {
                    Glide.with(this)
                            .load(CommonHelper.getAuthenticatedUrlForGlide(strUserProfilePic))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    progressProfileImage.setVisibility(View.GONE);
                                    imageViewProfilePic.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progressProfileImage.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .apply(CommonHelper.getGlideProfileErrorImage())
                            .into(imageViewProfilePic);
                }
                else
                {
                    progressProfileImage.setVisibility(View.GONE);
                    imageViewProfilePic.setImageDrawable(getResources().getDrawable(R.mipmap.ic_profile));
                }
            //}
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (userType.equalsIgnoreCase("CHAMP")) {
            startActivity(new Intent(Activity_Profile_Pic.this, ChampionDashboardActivity.class));
            finish();
        } else if (userType.equalsIgnoreCase("EVAL")) {
            startActivity(new Intent(Activity_Profile_Pic.this, EvaluatorDashboardActivity.class));
            finish();
        } else {
            startActivity(new Intent(Activity_Profile_Pic.this, BrokerDashboardActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewClose:
                onBackPressed();
                break;
            case R.id.imageViewCamera:
                takePicture();
                break;
            case R.id.imageViewGallery:
                getImageFromGallery();
                break;
            case R.id.imageViewUpload:
                //if (filePath.length() > 0)
               String strPath = textViewPath.getText().toString();
                if(strPath.length() > 0)
                    uploadDocument(strPath);
                break;
        }
    }

    private void takePicture() {
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            Uri photoURI = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        glideFlag = 1;

        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST_CODE:
                    try {
                        glideFlag = 1;
                        imageViewProfilePic.invalidate();
                        imageViewProfilePic.setImageResource(0);
                        imageViewProfilePic.setImageDrawable(null);
                        //File fileToUploadRisk = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                        String filePath = new BitmapHelper().compressImage(this,
                                Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                        File fileToUploadRisk = new File(filePath);
                        textViewPath.setText(fileToUploadRisk.getAbsolutePath());
                        float fileSize = fileToUploadRisk.length() / 1024F / 1024F;
                        if (fileSize < 2.0) {
                            progressProfileImage.setVisibility(View.GONE);
                            //imageViewProfilePic.setImageBitmap(new BitmapHelper().grabImage(fileToUploadRisk.getAbsolutePath()));
                            Glide.with(this)
                                    .load(fileToUploadRisk.getAbsolutePath())
                                    .into(imageViewProfilePic);
                        }
                        else
                        {
                            CommonHelper.toast("Image size should be less than or equal to 200 kb",Activity_Profile_Pic.this);
                        }
                    } catch (Exception e) {
                        CommonHelper.toast("Couldn't get image, please try again", this);
                        e.printStackTrace();
                    }
                    break;
                case RESULT_LOAD_IMAGE:
                    if (resultCode == RESULT_OK) {
                        try {
                            glideFlag = 1;
                            Uri uri = data.getData();
                            String[] projection = {MediaStore.Images.Media.DATA};

                            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(projection[0]);
                            String filePath = cursor.getString(columnIndex);
                            String filePath1 = new BitmapHelper().compressImage(this,
                                    filePath);
                            File fileToUploadRisk = new File(filePath1);
                            textViewPath.setText(fileToUploadRisk.getAbsolutePath());
                            float fileSize = fileToUploadRisk.length() / 1024F / 1024F;
                            if (fileSize < 2.0) {
                                cursor.close();
                                Bitmap image = BitmapFactory.decodeFile(filePath);
                                progressProfileImage.setVisibility(View.GONE);
                                //imageViewProfilePic.setImageBitmap(image);
                                Glide.with(this)
                                        .load(fileToUploadRisk.getAbsolutePath())
                                        .into(imageViewProfilePic);
                            }
                            else
                            {
                                CommonHelper.toast("Image size should be less than or equal to 200 kb",Activity_Profile_Pic.this);
                            }
                        } catch (Exception e) {
                            CommonHelper.toast("Couldn't get image, please try again", this);
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }

    public void getImageFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    void uploadDocument( final String imagePath) {
        try {
            final ProgressDialog dialog = new ProgressDialog(Activity_Profile_Pic.this);
            dialog.setMessage("Wait while uploading profile pic");
            dialog.show();
            //our custom volley request
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(
                    Request.Method.POST, WebConstants.uploadEvaluationDocument.URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            dialog.dismiss();
                            try {
                                JSONObject obj = new JSONObject(new String(response.data));
                                if (obj.getString("IsSuccessful").contains("1")) {

                                    //SessionUserDetails.getInstance().setUserProfilePic(filePath);
                                    //commonHelper.toast("Image upload Success "+documentType ,context);
                                    if (userType.equalsIgnoreCase("CHAMP")) {
                                        startActivity(new Intent(Activity_Profile_Pic.this, ChampionDashboardActivity.class));
                                        finish();
                                    } else if (userType.equalsIgnoreCase("EVAL")) {
                                        startActivity(new Intent(Activity_Profile_Pic.this, EvaluatorDashboardActivity.class));
                                        finish();
                                    }
                                    else {
                                        startActivity(new Intent(Activity_Profile_Pic.this, BrokerDashboardActivity.class));
                                        finish();
                                    }
                                }
                                Toast.makeText(getApplicationContext(), obj.getString("Message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Error : " + error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("docType", "DOC0000");
                    params.put("userId", userId);
                    params.put("userType", userType);
                    params.put("mobNo", mobNo);
                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long imagename = System.currentTimeMillis();
                    // TODO : fetch file and upload.. compress will be done at the time of image capture
//                    params.put("file", new DataPart(imagename + ".jpg", getFileDataFromDrawable(imagePath)));
                    try {
                        params.put("file", new DataPart(imagename + ".jpg", new BitmapHelper().fullyReadFileToBytes(new File(imagePath))));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return params;
                }
            };

            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}